<?php
\date_default_timezone_set('Europe/Berlin');
\setlocale(LC_TIME, 'de_DE', 'de_DE.utf-8');

$rootPath = __DIR__ . \DIRECTORY_SEPARATOR;
$docRootPath = $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR;

// begin - Define
\define('DIR_CorePath', $docRootPath . 'Core' . \DIRECTORY_SEPARATOR);

\define('DIR_Contrib', \DIR_CorePath . 'Contrib' . \DIRECTORY_SEPARATOR);
\define('DIR_Contrib_Pear', \DIR_Contrib . 'Pear' . \DIRECTORY_SEPARATOR);
\define('DIR_Inits', \DIR_CorePath . 'Inits' . \DIRECTORY_SEPARATOR);
\define('DIR_Vendor', \DIR_CorePath . 'Vendor' . \DIRECTORY_SEPARATOR);
\define('DIR_Modules', $docRootPath . 'Modules' . \DIRECTORY_SEPARATOR);

\define('DIR_Uploads', $docRootPath . 'Uploads' . \DIRECTORY_SEPARATOR);
\define('DIR_Downloads', $docRootPath . 'Downloads' . \DIRECTORY_SEPARATOR);
// end - Define

// load Autoloader
try {
	require_once($rootPath . 'Autoloader.php');
	
	$autoloader = new \Autoloader();
	$autoloader->init();
	\Packages\Core\Utility\GeneralUtility::$loggedUserId = 2;
	$autoloader->initErrorReporting();
	$autoloader->startAndCheckClientSession();
} catch (\Exception $e) {
	die($e->getMessage());
}