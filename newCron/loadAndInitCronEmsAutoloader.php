<?php
// cli script
\error_reporting(0);
\ini_set('display_errors', false);


/**
 * initCampaignManager
 * 
 * @param \ClientEntity $clientEntity
 * @param boolean $pdoStatmentDebug
 * @param boolean $pdoStatmentDebug
 * @return \CampaignManager
 */
function initCampaignManager(\ClientEntity $clientEntity, $pdoDebug, $pdoStatmentDebug) {
	$mandant = $clientEntity->getAbkz();
	require_once('config/dbConnect.php');
	
	require_once(DIR_configsInit . 'initCampaignManager.php');
	/* @var $campaignManager \CampaignManager */
	
	return $campaignManager;
}

/*
 * getEmsWorkPath
 * 
 * @return string
 */
function getEmsWorkPath($serverPath) {
	if (\CommandLine::getBoolean('isDev')) {
		$result = $serverPath . 'test/';
	} else {
		$result = $serverPath . 'system/';
	}
	
	return $result;
}

/**
 * initDefaultSessionData
 * 
 * @return void
 */
function initDefaultSessionData() {
	$_SESSION['benutzer_id'] = 1; #344
	$_SESSION['campaign']['actionId'] = 19;
}



require_once(DIR_Cron_library . 'CommandLine.php');

$pdoDebug = $pdoStatmentDebug = false;


// getEmsWorkPath
$emsWorkPath = \getEmsWorkPath($serverPath);
require_once($emsWorkPath . 'Configs/emsConfig.php');
require_once($emsWorkPath . 'EmsAutoloader.php');

/**
 * get cli arguments
 */
$argruments = \CommandLine::parseArgs();

// initDefaultSessionData
\initDefaultSessionData();

/**
 * EmsAutoloader -> init
 */
\EmsAutoloader::init();