<?php
/**
 * initCampaignManager
 * 
 * @param \ClientEntity $clientEntity
 * @param boolean $pdoStatmentDebug
 * @param boolean $pdoStatmentDebug
 * @return \CampaignManager
 */
function initCampaignManager(\ClientEntity $clientEntity, $pdoDebug, $pdoStatmentDebug) {
	$mandant = $clientEntity->getAbkz();
	require_once('config/dbConnect.php');
	
	require_once(DIR_configsInit . 'initCampaignManager.php');
	/* @var $campaignManager \CampaignManager */
	
	return $campaignManager;
}


$pdoDebug = $pdoStatmentDebug = $showDebugData = false;
if (isset($_SERVER['PWD'])) {
	// cli script
	\error_reporting(0);
	\ini_set('display_errors', false);
	
	$serverPath = \str_replace('newCron/', '', DIR_Cron);
} else {
	\error_reporting(E_ALL);
	\ini_set('display_errors', true);
	
	$showDebugData = true;
	$serverPath = \str_replace('newCron/campaign', '', $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR);
}
$emsWorkPath = $serverPath . 'system/';
\define('DIR_Cron_library', $serverPath . 'newCron/library/');



require_once($emsWorkPath . 'Config/emsConfig.php');
require_once(\DIR_library . 'EmsAutoloader.php');


$_SESSION['benutzer_id'] = 1; #344
$_SESSION['campaign']['actionId'] = 19;