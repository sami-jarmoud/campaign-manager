<?php
$dbHostEms = 'localhost';

// campaign - Configs
$kampagne_db = 'kampagnen';
$kunde = 'kunde';
$kunde_ap = 'kunde_ap';
$log = 'log';

// dataManager - Configs
$anrede_db = 'anrede';
$herkunft_db = 'herkunft';
$import_datei_db = 'import_datei';
$partner_db = 'partner';
$metaDB = 'history';

switch ($mandant) {
    case 'demoClient':
        $db_name = 'demo-client_maas_db';
        $db_user = 'dc-maas-user-db';
        $db_pw = 'Q0l9L7k7';
        break;

    case 'skylinePerformance':
        $db_name = 'skyline-performance_maas_db';
        $db_user = 'sk-maas-user-db';
        $db_pw = '6A3x2M9h';
		break;
	
	case 'complead':
        $db_name = 'complead_maas_db';
        $db_user = 'complead-user-db';
        $db_pw = '7I5s5D3d';
		break;
	
	case 'intone':
        $db_name = 'intone_maas_db';
        $db_user = 'intone-user-db';
        $db_pw = '9O8p0H6d';
		break;
	
	case 'stellarPerformance':
        $db_name = 'stellar_maas_db';
        $db_user = 'stellar-user-db';
        $db_pw = 'V1s9S3y6';
		break;
	
        case '4waveMarketing':
        $db_name = '4wave_maas_db';
        $db_user = '4wave-user-db';
        $db_pw = '5U8t9N8j';
		break;
            
         case 'leadWorld':
        $db_name = 'leadworld_maas_db';
        $db_user = 'lead-user-db';
        $db_pw = '4H5d4Q4t';
		break;
	default:
		throw new \InvalidArgumentException('no client set!');
}