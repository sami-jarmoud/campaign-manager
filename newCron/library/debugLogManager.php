<?php
class debugLogManager {
    /**
     * init
     * 
     * @return void
     */
    public function init() {
        // do nothings
    }
    
    
    /**
     * logData
     * 
     * @param string $header
     * @param mixed $data
     * @return void
     */
    public function logData($header, $data) {
        // do nothings
    }
    
    
    /**
     * beginGroup
     * 
     * @param string $title
     * @return void
     */
    public function beginGroup($title) {
        // do nothings
    }
    
    
    /**
     * endGroup
     * 
     * @return void
     */
    public function endGroup() {
        // do nothings
    }
}