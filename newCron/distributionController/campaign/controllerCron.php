<?php


/**
 * getServerPath
 * 
 * @return string
 */
function getServerPath() {
	return \substr(__DIR__, 0, \stripos(__DIR__, 'newCron' . \DIRECTORY_SEPARATOR));
}


/**
 * createCampaignQueryDataArray
 * 
 * @param \DateTime $dateFrom
 * @param \DateTime $dateTo
 * @param \integer $campaignDsdId
 * @return array
 */
function createCampaignQueryDataArray(\DateTime $dateFrom, \DateTime $dateTo) {
	return array(
		'WHERE' => array(
			'mailId' => array(
				'sql' => '`mail_id`',
				'value' => null,
				'comparison' => '<>'
			),
			'dateBegin' => array(
				'sql' => '`datum`',
				'value' => $dateFrom->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldGreaterThan
			),
			'dateEnd' => array(
				'sql' => '`datum`',
				'value' => $dateTo->format('Y-m-d H:i:s'),
				'comparison' => \PdoDbManager::$fieldLessThan
			),
		),
		'ORDER_BY' => '`datum` DESC'
	);
}

/**
 * processUpdateSendEmailCampaign
 * @param \CampaignManager $campaignManager
 * @return void
 */
function processUpdateSendEmailCampaign(\CampaignManager $campaignManager, $campaignId) {
	
	
	$dataArray = array(
		'send_mail_tcm' => array(
			'value' => 1,
			'dataType' => \PDO::PARAM_INT
		)
	);
	$campaignManager->updateCampaignDataRowByKid($campaignId, $dataArray);
}

/**
 * convertirStringToDatum
 * 
 * @param string $stringDatum 
 * @return $datum 
 */
function convertirStringToDatum($stringDatum){
    $date_1 = explode(' ', $stringDatum);
    $datum = explode('-', $date_1[0]);
    $year = $datum[0];
    $month = $datum[1];
    $day = $datum[2];
    
    $zeit_teile = explode(':', $date_1[1]); 
    $h = $zeit_teile[0];
    $m = $zeit_teile[1];
    $s = $zeit_teile[2];
    
    $date = new \DateTime();
    $date->setDate($year, $month, $day);
    $date->setTime($h, $m, $s);
    
    return $date;
}

/**
 * mailContent
 * 
 * @param \CampaignEntity $campaignEntity
 * @param type $mandant
 * @return string
 */
function mailContent(\CampaignEntity $campaignEntity, $mandant){
   
    $messageBody = 'Die Kampagne '. $campaignEntity->getK_name() . ' ist nicht gestartet' . \chr(13);
    $messageBody .= '**********************************************************************' . \chr(13);
    $messageBody .= 'Mandant : ' . $mandant . \chr(13);
    $messageBody .= 'Kampagne ID : ' . $campaignEntity->getK_id() . \chr(13);
    $messageBody .= 'Kampagne Name : ' . $campaignEntity->getK_name() .  \chr(13);
    $messageBody .= 'Kampagne Status : ' . \getStatusName($campaignEntity->getStatus()) . \chr(13);
    $messageBody .= 'Versanddatum : ' . $campaignEntity->getDatum()->format('d.m.Y') . ' um ' . $campaignEntity->getDatum()->format('H:i'). \chr(13);
    $messageBody .= '**********************************************************************' . \chr(13);   
    return $messageBody;   
}

/**
 * getStatusName
 * 
 * @param integer $status
 * @return string
 */
 function getStatusName($status) {

        switch ($status) {
            case 0: $status_bez = "Neu";
                break;
            case 1: $status_bez = "Werbemittel";
                break;
            case 2: $status_bez = "Optimiert";
                break;
            case 3: $status_bez = "Testmails";
                break;
            case 4: $status_bez = "Freigabe";
                break;
            case 5: $status_bez = "versendet";
                break;
            case 6: $status_bez = "senden...";
                break;
            case 7: $status_bez = "abgebr.";
                break;
            case 10: $status_bez = "Anfrage";
                break;
            case 11: $status_bez = "Warte auf Bestätigung";
                break;
            case 12: $status_bez = "Warte auf Werbemittel";
                break;
            case 20: $status_bez = "Warte auf Report Freigabe";
                break;
            case 21: $status_bez = "Erstreporting";
                break;
            case 22: $status_bez = "Endreporting";
                break;
            case 23: $status_bez = "Reportfertig";
                break;
            case 24: $status_bez = "Report freigegeben";
                break;
            case 30: $status_bez = "Rechnungsstellung freigegeben";
                break;
            case 31: $status_bez = "Rechnung bestätigt";
                break;
            case 32: $status_bez = "Rechnung gestellt";
                break;
            case 33: $status_bez = "Rechnung bezahlt";
                break;
        }

        return $status_bez;
    }
  
function sendSmsToTcm(\CampaignEntity $campaignEntity, $mandant, $emfaenger){
    $datum ='';
    if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
        // nur HV
        $datum = ' [HV ' . $campaignEntity->getDatum()->format('d.m') . ']';
    } 
   if(!empty($emfaenger)){ 
    $message ='';
    $message .='Die kampagne ist nicht gestartet ';
    $message .='Mandant : '.$mandant.'. ';
    $message .= 'Kampagne Name : ' .$campaignEntity->getK_name().$datum.'.';

    $smsTrade = new \newCron\distributionController\campaign\smsTrade();
    
   $ergebnis =  $smsTrade->sendMessage($emfaenger, $message);

   }
}
   
// getServerPath
$serverPath = \getServerPath();
\define('DIR_Cron', $serverPath . 'newCron' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_config', DIR_Cron . 'config' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_library', DIR_Cron . 'library' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_dist', DIR_Cron . 'distributionController' . \DIRECTORY_SEPARATOR);
\define('DIR_Cron_campaign', DIR_Cron_dist . 'campaign' . \DIRECTORY_SEPARATOR);
\define('DIR_Core', $serverPath. 'Core' . \DIRECTORY_SEPARATOR);
\define('DIR_Packages', DIR_Core. 'Packages' . \DIRECTORY_SEPARATOR);
\define('DIR_Core2', DIR_Packages. 'Core' . \DIRECTORY_SEPARATOR);
\define('DIR_Utiliy', DIR_Core2. 'Utility' . \DIRECTORY_SEPARATOR);

/**
 * init
 * 
 * loadAndInitCronEmsAutoloader
 * 
 * loadAndInitCronEmsAutoloader
 * $debugLogManager
 * $clientManager
 */
require_once(DIR_Cron . 'loadAndInitCronEmsAutoloader.php');



require_once(DIR_Cron_config . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_Factory . 'DeliverySystemFactory.php');

require_once (DIR_Utiliy . 'MailUtility.php');
require_once (DIR_Cron_campaign. 'smsTrade.php');

$benutzerEntity = $clientManager->getActiveUsersDataItemsByTelefon();

foreach ($benutzerEntity as $benutzer){
       
      $empfaengerArray[$benutzer->getBenutzer_id()] = 
                $benutzer->getTelefon()
        ; 
}
try {
  
   #\DebugAndExceptionUtils::sendDebugData($empfaengerArray, __FILE__ . ': ' . $empfaengerArray);


	$timeStart = time();
	
	$dateNow = new \DateTime('now');
	$dateNow->setTime(0, 0, 0);
	$dateFrom = clone $dateNow;
	$dateTo = clone $dateNow;

	$dateFrom->sub(new \DateInterval('P1D'));
	$dateFrom->setTime(23, 59, 59);
	
	$dateTo->add(new \DateInterval('P1D'));
	$dateTo->setTime(00, 00, 00);
	
	$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
		throw new \DomainException('invalid ClientEntity!', 1424262859);
	}
		
	/**
	 * initCampaignManager
	 */
	$campaignManager = \initCampaignManager(
		$clientEntity,
		$pdoDebug,
		$pdoStatmentDebug
	);
	
        $campaignQueryPartsDataArray = \createCampaignQueryDataArray(
		$dateFrom, 
		$dateTo
		);
         
	/**
	* getCampaignDataItemByQueryParts
	*/
	$campaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
		$campaignQueryPartsDataArray,
		\PDO::FETCH_ASSOC
	);
        foreach ($campaignsDataArray as $campaignDataArray){
            
           $stringDatum = $campaignDataArray['datum'];
           $datum = convertirStringToDatum($stringDatum);
           $datumKontrolleZeit = $datum->add(new \DateInterval('PT1H30M'));
           $dateNow = new \DateTime('now');

           if($datumKontrolleZeit < $dateNow){
               $campaignEntity = $campaignManager->getCampaignDataItemById(
                                $campaignDataArray['k_id']
                                );
               $messageBody = \mailContent(
                               $campaignEntity,
                               $mandant
                           );
                $subject = 'Die kampagne ist nicht gestartet';
               $mailToTcm = new \Packages\Core\Utility\MailUtility();
               
               if(\intval($campaignDataArray['k_id']) === \intval($campaignDataArray['nv_id']
                    )){
                   if(\intval($campaignDataArray['status']) === 4
                        ){
                       if(\intval($campaignEntity->getSend_mail_tcm()) !== 1){
             
                          $mailToTcm::sendMailToTcm(
                                  $subject,
                                  $messageBody
                          ); 
                       \processUpdateSendEmailCampaign(
                               $campaignManager,
                               $campaignEntity->getK_id()
                               );                          
                          foreach ($empfaengerArray as $key => $emfaenger){
                           \sendSmsToTcm(
                                   $campaignEntity,
                                   $mandant,
                                   $emfaenger
                                   );
                              
                          }        
                       }
                   }
               }else{
                    if(\intval($campaignDataArray['status']) !== 0
                       && \intval($campaignDataArray['status']) !== 6
                       && \intval($campaignDataArray['status']) !== 5
                       && \intval($campaignDataArray['status']) !== 7     
                         ){
                       if(\intval($campaignEntity->getSend_mail_tcm()) !== 1){ 
                          $mailToTcm::sendMailToTcm(
                                  $subject,
                                  $messageBody
                          ); 
                       \processUpdateSendEmailCampaign(
                               $campaignManager,
                               $campaignEntity->getK_id()
                               );                          
                          foreach ($empfaengerArray as $key => $emfaenger){
                           \sendSmsToTcm(
                                   $campaignEntity,
                                   $mandant,
                                   $emfaenger
                                   );
                              
                          }                           
                       }
                   }
               }
           }

        }

} catch (Exception $exc) {
	echo $exc->getTraceAsString();
 }


