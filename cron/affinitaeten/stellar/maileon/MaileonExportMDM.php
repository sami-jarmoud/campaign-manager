<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');


$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "98413824-b3ae-4eca-8af0-cfd67b591b07",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);
/*
 * Apikey
 * 
 */
$affinitaeten = array(
    '1f3a4187-07de-4ac5-a5cb-61352915b8f4' => array(
        2520 => 'date',
        2521 => 'inge',
        2522 => 'trawell',
        2523 => 'eso',
        2524 => 'fam',
        2525 => 'game',
        2526 => 'char',
        2527 => 'win',
        2528 => 'immo',
        2529 => 'love',
        2530 => 'limod',
        2531 => 'lotto',
        2532 => 'luxgou',
        2533 => 'reiwell',
        2534 => 'sonstiges',
        2535 => 'sozi',
        2536 => 'toto',
        2537 => 'spofi',
        2538 => 'techcar',       
        2539 => 'telko',
        2540 => 'tiere',     
        2541 => 'unthob',               
        2542 => 'versi',
        2543 => 'wifa',  
        2544 => 'wipo'            
    ),
    '4d682dc3c-2937-4946-b231-3bc63890a8f9' => array(
        2520 => 'date',
        2521 => 'inge',
        2522 => 'trawell',
        2523 => 'eso',
        2524 => 'fam',
        2525 => 'game',
        2526 => 'char',
        2527 => 'win',
        2528 => 'immo',
        2529 => 'love',
        2530 => 'limod',
        2531 => 'lotto',
        2532 => 'luxgou',
        2533 => 'reiwell',
        2534 => 'sonstiges',
        2535 => 'sozi',
        2536 => 'toto',
        2537 => 'spofi',
        2538 => 'techcar',       
        2539 => 'telko',
        2540 => 'tiere',     
        2541 => 'unthob',               
        2542 => 'versi',
        2543 => 'wifa',  
        2544 => 'wipo'  
    ),
   
    '532ddc97-a5ce-4dce-88da-be441690d0a5' => array(
        2520 => 'date',
        2521 => 'inge',
        2522 => 'trawell',
        2523 => 'eso',
        2524 => 'fam',
        2525 => 'game',
        2526 => 'char',
        2527 => 'win',
        2528 => 'immo',
        2529 => 'love',
        2530 => 'limod',
        2531 => 'lotto',
        2532 => 'luxgou',
        2533 => 'reiwell',
        2534 => 'sonstiges',
        2535 => 'sozi',
        2536 => 'toto',
        2537 => 'spofi',
        2538 => 'techcar',       
        2539 => 'telko',
        2540 => 'tiere',     
        2541 => 'unthob',               
        2542 => 'versi',
        2543 => 'wifa',  
        2544 => 'wipo'  
    ),
    '6ca7acac-a78f-4124-a749-f1424c93e75c' => array(
        2605 => 'date',
        2606 => 'inge',
        2607 => 'trawell',
        2608 => 'eso',
        2609 => 'fam',
        2610 => 'game',
        2611 => 'char',
        2612 => 'win',
        2613 => 'immo',
        2614 => 'love',
        2615 => 'limod',
        2616 => 'lotto',
        2617 => 'luxgou',
        2618 => 'reiwell',
        2619 => 'sonstiges',
        2620 => 'sozi',
        2622 => 'toto',
        2621 => 'spofi',
        2623 => 'techcar',       
        2624 => 'telko',
        2625 => 'tiere',     
        2626 => 'unthob',               
        2627 => 'versi',
        2628 => 'wifa',  
        2629 => 'wipo'            
    ) 
   
);

foreach ($affinitaeten as $apikey => $affinitaet) {

    foreach ($affinitaet as $kontaktFilterId => $aff) {

        //GetContact
        $config2 = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $apikey,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 90000,
            "DEBUG" => "false" // NEVER enable on production
        );

        $debug2 = FALSE;
        $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
        $contactfiltersService->setDebug($debug2);
        $contactfiltersService->refreshContactFilterContacts((int)$kontaktFilterId, time() * 1000);


        $contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
        $contactsService2->setDebug($debug2);

        for ($count = 1; $count < 5; $count++) {

            $response = $contactsService->getContactsByFilterId((int)$kontaktFilterId, $count, 1000);

            foreach ($response->getResult() as $contact) {

                $newContact = new com_maileon_api_contacts_Contact();
                $newContact->anonymous = FALSE;
                $newContact->email = $contact->email;
                $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                $newContact->custom_fields[(string)$aff] = TRUE;

                $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
            }
        }
    }
}