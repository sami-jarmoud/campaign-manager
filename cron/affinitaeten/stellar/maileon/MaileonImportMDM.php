<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');


$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "98413824-b3ae-4eca-8af0-cfd67b591b07",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$affinitaeten = array(
    'wifa' => array(
        2454 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2482 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2628 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2511 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'limod' => array(
        2440 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2468 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2615 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2497 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'telko' => array(
        2450 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2478 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
         2482 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2507 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'techcar' => array(
        2449 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2477 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2623 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2506 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'reiwell' => array(
        2444 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2472 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2618 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2501 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'unthob' => array(
       2452 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
       2480 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
       2626 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
       2509 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'game' => array(
        2434 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2462 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2610 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2491 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'luxgou' => array(
        2442 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2470 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2617 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2499 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'tiere' => array(
        2451 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2479 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2625 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2508 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'sozi' => array(
        2446 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2474 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2620 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2503 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'date' => array(
        2431 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2459 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2605 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2488 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'sonstiges' => array(
        2445 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2473 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2619 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2502 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'versi' => array(
        2453 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2481 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2627 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2510 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'inge' => array(
        2438 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2466 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2606 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2495 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'immo' => array(
        2437 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2465 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2613 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2494 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'wipo' => array(
        2455 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2483 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2629 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2512 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'trawell' => array(
        2443 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2471 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2607 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2500 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'lotto' => array(
        2441 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2469 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2616 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2498 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'toto' => array(
        2448 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2476 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
         2482 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2505 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'eso' => array(
        2432 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2460 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2608 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2489 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'win' => array(
        2436 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2464 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2612 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2493 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'fam' => array(
        2433 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2461 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2609 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2463 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'char' => array(
        2435 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2463 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2611 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2492 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'love' => array(
        2438 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2467 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2614 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2496 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    ),
    'spofi' => array(
        2447 => '1f3a4187-07de-4ac5-a5cb-61352915b8f4',
        2475 => '4d682dc3c-2937-4946-b231-3bc63890a8f9',
        2621 => '6ca7acac-a78f-4124-a749-f1424c93e75c',
        2504 => '4532ddc97-a5ce-4dce-88da-be441690d0a5'
    )
);

foreach ($affinitaeten as $aff => $affinitaet) {

    foreach ($affinitaet as $kontaktFilterId => $apikey) {

        //GetContact
        $config2 = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $apikey,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 90000,
            "DEBUG" => "false" // NEVER enable on production
        );

        $debug2 = FALSE;
        $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config2);
        $contactfiltersService->setDebug($debug2);
        $contactfiltersService->refreshContactFilterContacts((int)$kontaktFilterId, time() * 1000);


        $contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
        $contactsService2->setDebug($debug2);

        for ($count = 1; $count < 5; $count++) {

            $response = $contactsService2->getContactsByFilterId((int)$kontaktFilterId, $count, 1000);

            foreach ($response->getResult() as $contact) {

                $newContact = new com_maileon_api_contacts_Contact();
                $newContact->anonymous = FALSE;
                $newContact->email = $contact->email;
                $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                $newContact->custom_fields[(string)$aff] = TRUE;

                $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
            }
        }
    }
}