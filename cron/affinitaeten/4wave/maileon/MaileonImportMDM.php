<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');


$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "db1757ee-52bf-4179-b936-bebd403ba669",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$affinitaeten = array(
    'wifa' => array(
        1008 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1035 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1149 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1060 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'limod' => array(
        994 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1021 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1135 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1046 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'telko' => array(
        1004 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1031 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1145 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1056 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'techcar' => array(
        1003 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1030 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1144 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1055 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'reiwell' => array(
        998 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1025 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1139 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1050 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'unthob' => array(
        1006 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1033 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1147 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1058 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'game' => array(
        988 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1015 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1129 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1040 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'luxgou' => array(
        996 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1023 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1137 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1048 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'tiere' => array(
        1005 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1032 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1146 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1057 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'sozi' => array(
        1000 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1027 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1141 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1052 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'date' => array(
        985 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
       1012 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
       1126 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
       1037 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'sonstiges' => array(
        999 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1026 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1140 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1051 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'versi' => array(
        1007 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1034 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1148 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1059 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'inge' => array(
        992 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1019 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1133 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1044 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'immo' => array(
       991 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
       1018 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
       1132 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
       1043 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'wipo' => array(
        1009 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1036 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1150 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1061 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'trawell' => array(
        997 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1024 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1138 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1049 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'lotto' => array(
       995 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
       1022 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
       1136 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
       1047 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'toto' => array(
        1002 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1029 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1136 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1054 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'eso' => array(
        986 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1013 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1127 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1038 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'win' => array(
        990 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1017 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1131 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1042 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'fam' => array(
        987 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1014 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1128 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1039 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'char' => array(
        989 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1016 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1130 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1041 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'love' => array(
       993 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
       1020 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
       1134 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
       1045 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    ),
    'spofi' => array(
        1001 => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e',
        1028 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        1143 => '69a7bab6-8e27-4d73-b544-5fb8635b3db9',
        1053 => '40d15f98-2023-45fd-8842-ca16d8b2680f'
    )
);

foreach ($affinitaeten as $aff => $affinitaet) {

    foreach ($affinitaet as $kontaktFilterId => $apikey) {

        //GetContact
        $config2 = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $apikey,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 90000,
            "DEBUG" => "false" // NEVER enable on production
        );

        $debug2 = FALSE;
        $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config2);
        $contactfiltersService->setDebug($debug2);
        $contactfiltersService->refreshContactFilterContacts((int)$kontaktFilterId, time() * 1000);


        $contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
        $contactsService2->setDebug($debug2);

        for ($count = 1; $count < 5; $count++) {

            $response = $contactsService2->getContactsByFilterId((int)$kontaktFilterId, $count, 1000);

            foreach ($response->getResult() as $contact) {

                $newContact = new com_maileon_api_contacts_Contact();
                $newContact->anonymous = FALSE;
                $newContact->email = $contact->email;
                $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                $newContact->custom_fields[(string)$aff] = TRUE;

                $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
            }
        }
    }
}