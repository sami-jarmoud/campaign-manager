<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');


$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "db1757ee-52bf-4179-b936-bebd403ba669",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);
/*
 * Apikey
 * 4Wave => 'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e'
 * 4Wave 2018 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32' 
 * 4Wave Basis => '40d15f98-2023-45fd-8842-ca16d8b2680f'
 */
$affinitaeten = array(
    'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e' => array(
        1075 => 'date',
        1076 => 'eso',
        1077 => 'fam',
        1078 => 'game',
        1079 => 'char',
        1080 => 'win',
        1081 => 'immo',
        1082 => 'inge',
        1083 => 'love',
        1085 => 'limod',
        1084 => 'lotto',
        1086 => 'luxgou',
        1087 => 'trawell',
        1088 => 'reiwell',
        1089 => 'sonstiges',
        1090 => 'sozi',
        1091 => 'spofi',
        1092 => 'toto',
        1093 => 'techcar',       
        1094 => 'telko',
        1095 => 'tiere',     
        1096 => 'unthob',               
        1097 => 'versi',
        1098 => 'wifa',  
        1099 => 'wipo'            
    ),
    '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32' => array(
        1075 => 'date',
        1076 => 'eso',
        1077 => 'fam',
        1078 => 'game',
        1079 => 'char',
        1080 => 'win',
        1081 => 'immo',
        1082 => 'inge',
        1083 => 'love',
        1085 => 'limod',
        1084 => 'lotto',
        1086 => 'luxgou',
        1087 => 'trawell',
        1088 => 'reiwell',
        1089 => 'sonstiges',
        1090 => 'sozi',
        1091 => 'spofi',
        1092 => 'toto',
        1093 => 'techcar',       
        1094 => 'telko',
        1095 => 'tiere',     
        1096 => 'unthob',               
        1097 => 'versi',
        1098 => 'wifa',  
        1099 => 'wipo'
    ),
   
    '69a7bab6-8e27-4d73-b544-5fb8635b3db9' => array(
        1126 => 'date',
        1127 => 'eso',
        1128 => 'fam',
        1129 => 'game',
        1130 => 'char',
        1131 => 'win',
        1132 => 'immo',
        1133 => 'inge',
        1134 => 'love',
        1135 => 'limod',
        1136 => 'lotto',
        1137 => 'luxgou',
        1138 => 'trawell',
        1139 => 'reiwell',
        1140 => 'sonstiges',
        1141 => 'sozi',
        1143 => 'spofi',
        1142 => 'toto',
        1144 => 'techcar',       
        1145 => 'telko',
        1146 => 'tiere',     
        1147 => 'unthob',               
        1148 => 'versi',
        1149 => 'wifa',  
        1150 => 'wipo'
    )
   
   
);

foreach ($affinitaeten as $apikey => $affinitaet) {

    foreach ($affinitaet as $kontaktFilterId => $aff) {

        //GetContact
        $config2 = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $apikey,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 90000,
            "DEBUG" => "false" // NEVER enable on production
        );

        $debug2 = FALSE;
        $contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
        $contactfiltersService->setDebug($debug2);
        $contactfiltersService->refreshContactFilterContacts((int)$kontaktFilterId, time() * 1000);


        $contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
        $contactsService2->setDebug($debug2);

        for ($count = 1; $count < 5; $count++) {

            $response = $contactsService->getContactsByFilterId((int)$kontaktFilterId, $count, 1000);

            foreach ($response->getResult() as $contact) {

                $newContact = new com_maileon_api_contacts_Contact();
                $newContact->anonymous = FALSE;
                $newContact->email = $contact->email;
                $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
                $newContact->custom_fields[(string)$aff] = TRUE;

                $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
            }
        }
    }
}