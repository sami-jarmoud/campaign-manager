<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "d35fd0b2-8a95-421b-bb77-6e666a9fbd1e",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 100000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(44, time()*1000);  


 $mandanten = array(
               801 => '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
               805 => '41034668-eb18-4301-bb65-13d853e83261',
               804 => '40d15f98-2023-45fd-8842-ca16d8b2680f',
               832 =>'534c3a3b-b465-4225-b8ea-2c44f715dd16'
            );
foreach ($mandanten as $key => $apikey) {

$config2 = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => $apikey,
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 10000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug2 = FALSE;
$contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
$contactsService2->setDebug($debug2);

$start = time();

    $response = $contactsService->getContactsByFilterId(44, 1, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            )
    );

    foreach ($response->getResult() as $contact) {

        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->anonymous = FALSE;
        $newContact->email = $contact->email;
        $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $contact->standard_fields['SALUTATION'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $contact->standard_fields['FIRSTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $contact->standard_fields['LASTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ADDRESS] = $contact->standard_fields['ADDRESS'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ZIP] = $contact->standard_fields['ZIP'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$CITY] = $contact->standard_fields['CITY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$COUNTRY] = $contact->standard_fields['COUNTRY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$BIRTHDAY] = $contact->standard_fields['BIRTHDAY'];
        $newContact->custom_fields["Testliste-4wave"] = TRUE;

        $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
        
    }
    
        $contactfiltersService2 = new com_maileon_api_contactfilters_ContactfiltersService($config2);
        $contactfiltersService2->setDebug($debug);
        $contactfiltersService2->refreshContactFilterContacts($key, time()*1000);
}

$end = time();

echo "TestListe: " . ($end - $start) . " seconds";

