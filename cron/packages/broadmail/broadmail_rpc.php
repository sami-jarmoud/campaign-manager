<?php

    /**
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * redistribute any part of this software.
     */

    /**
     * Main entry point for the broadmail NuSOAP RPC interface. Currently uses the
     * webservices deployed with "RPC:Encoded".
     *
     * @author Peter Romianowski
     * @version 1.0 2005-05-06
     */

    require_once('lib/nusoap/nusoap.php');

    /**
     * Utilities.
     */
    class _BroadmailRpcUtils {

        /**
         * Parse the given ISO8601 compliant date string into a unix timestamp.
         */
        function parseIso8601($dateString) {
            return iso8601_to_timestamp($dateString);
        }

        /**
         * Format the given unix timestamp into a ISO8601 compliant date string.
         */
        function formatIso8601($timestamp) {
            return timestamp_to_iso8601($timestamp);
        }

    }

    $BroadmailRpcUtils = new _BroadmailRpcUtils();

    /**
     * System class.
     *
     * Base class for all SOAP stubs.
     */
    class _BroadmailRpcBase {

        var $customsoapClient;
        var $sessionId;
        var $_types;
        var $_typeConversionIndex;
      

        function _init($sessionId, $endPoint, $serviceName, $proxyHost = false,
                       $proxyPort = false, $proxyUsername = false,
                       $proxyPassword = false, $timeout = 0,
                       $responseTimeout = 500) {

            // Initialize the internal type mapping
            $this->_types = array(
                'java.util.Date' => 'dateTime',
                'java.util.Date[]' => '[]dateTime',
                'java.sql.Date' => 'dateTime',
                'java.sql.Date[]' => '[]dateTime',
                'java.sql.Timestamp' => 'dateTime',
                'java.sql.Timestamp[]' => '[]dateTime',
                'java.lang.Long' => 'long',
                'java.lang.Long[]' => '[]long',
                'java.lang.Double[]' => '[]double',
                'java.lang.Byte[]' => 'base64Binary'
            );

            $this->customsoapClient = new customsoapclient($endPoint."/Rpc".$serviceName, false,
                $proxyHost, $proxyPort, $proxyUsername, $proxyPassword,
                $timeout, $responseTimeout);

            $this->sessionId = $sessionId;
            $this->_typeConversionIndex = 0;
        }

        function _call($method, $params) {
            $result = $this->customsoapClient->call($method, $params);
            $this->fault = $this->customsoapClient->fault;
            return $result;
        }

        /**
         * Delegation to customsoapClient.getError().
         */
        function getError() {
            return $this->customsoapClient->getError();
        }

        function _convert($javaClass, $value) {
            $result = $value;
            $type = $this->_types[$javaClass];
            if ($type) {
                if (strstr($type, "[]")) {
                    // This must be an array
                    $type = substr($type, 2);
                    $result = array();
                    for ($a = 0; $a < count($value); $a++) {
                        array_push($result, new soapval("wrapped".$this->_typeConversionIndex, $type, $value[$a], false, "http://www.w3.org/2001/XMLSchema"));
                        $this->_typeConversionIndex++;
                    }
                } else {
                    // Fix for BM-2545
                    if ($type == "base64Binary") {
                        $value = base64_encode($value);
                    }
                    $result = new soapval("wrapped".$this->_typeConversionIndex, $type, $value, false, "http://www.w3.org/2001/XMLSchema");
                    $this->_typeConversionIndex++;
                }
            }
            return $result;
        }
    }

    require_once('impl/rpc_factory.php');
?>