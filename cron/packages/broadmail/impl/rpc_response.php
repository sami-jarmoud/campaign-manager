<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface ResponseWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:08 CET 2010
     */

    class BroadmailRpcResponseWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcResponseWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Response', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getRecipientResponseCount( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getRecipientResponseCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Long', $p4)));
        }

        function getRecipientResponseCount2( $p1,  $p2,  $p3) {
            return $this->_call('getRecipientResponseCount2', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.Long', $p3)));
        }

        function getAllRecipientResponseCounts( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getAllRecipientResponseCounts', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3), 'p5' => $this->_convert('java.lang.Long', $p4)));
        }

        function getAllRecipientResponseCounts2( $p1,  $p2,  $p3) {
            return $this->_call('getAllRecipientResponseCounts2', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2), 'p4' => $this->_convert('java.lang.Long', $p3)));
        }

        function getMailingResponseCount( $p1,  $p2) {
            return $this->_call('getMailingResponseCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function getBounceCounter( $p1,  $p2) {
            return $this->_call('getBounceCounter', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function resetBounceCounter( $p1) {
            return $this->_call('resetBounceCounter', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getBounceCounterThreshold( $p1) {
            return $this->_call('getBounceCounterThreshold', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function isBounceCounterThresholdExeeded( $p1) {
            return $this->_call('isBounceCounterThresholdExeeded', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
