<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface FolderWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:20:53 CET 2010
     */

    class BroadmailRpcFolderWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcFolderWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Folder', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getParent( $p1) {
            return $this->_call('getParent', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getChildren( $p1) {
            return $this->_call('getChildren', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function createFolder( $p1,  $p2,  $p3) {
            return $this->_call('createFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function renameFolder( $p1,  $p2) {
            return $this->_call('renameFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function removeFolder( $p1) {
            return $this->_call('removeFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFolderName( $p1) {
            return $this->_call('getFolderName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function moveFolder( $p1,  $p2) {
            return $this->_call('moveFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function assignFolder( $p1,  $p2,  $p3) {
            return $this->_call('assignFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getAssignedFolder( $p1) {
            return $this->_call('getAssignedFolder', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getRootFolders( $p1) {
            return $this->_call('getRootFolders', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
