<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface RecipientListWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:05 CET 2010
     */

    class BroadmailRpcRecipientListWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcRecipientListWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'RecipientList', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getName( $p1) {
            return $this->_call('getName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setName( $p1,  $p2) {
            return $this->_call('setName', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function copy( $p1) {
            return $this->_call('copy', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getDescription( $p1) {
            return $this->_call('getDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setDescription( $p1,  $p2) {
            return $this->_call('setDescription', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getCount( $p1) {
            return $this->_call('getCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Boolean', $p1)));
        }

        function getAttributeNames( $p1,  $p2) {
            return $this->_call('getAttributeNames', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getAllIds() {
            return $this->_call('getAllIds', array('p1' => $this->sessionId));
        }

        function getDataSet() {
            return $this->_call('getDataSet', array('p1' => $this->sessionId));
        }

        function isTestRecipientList( $p1) {
            return $this->_call('isTestRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setTestRecipientList( $p1,  $p2) {
            return $this->_call('setTestRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
