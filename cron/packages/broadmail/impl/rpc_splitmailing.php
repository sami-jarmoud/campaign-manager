<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface SplitMailingWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:20:57 CET 2010
     */

    class BroadmailRpcSplitMailingWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcSplitMailingWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'SplitMailing', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function addSplit( $p1,  $p2) {
            return $this->_call('addSplit', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function getSplitMasterId( $p1) {
            return $this->_call('getSplitMasterId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getSplitChildIds( $p1) {
            return $this->_call('getSplitChildIds', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function removeSplit( $p1) {
            return $this->_call('removeSplit', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function setFinalSplitSelectionCriterion( $p1,  $p2) {
            return $this->_call('setFinalSplitSelectionCriterion', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function setMasterStartDelay( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('setMasterStartDelay', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Integer', $p2), 'p4' => $this->_convert('java.lang.Integer', $p3), 'p5' => $this->_convert('java.lang.Integer', $p4)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
