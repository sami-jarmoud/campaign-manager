<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface SystemWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:11 CET 2010
     */

    class BroadmailRpcSystemWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcSystemWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'System', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getSerialNumber() {
            return $this->_call('getSerialNumber', array('p1' => $this->sessionId));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
