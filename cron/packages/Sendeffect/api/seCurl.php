<?php

class seCurl {

    private $username;
    private $token;
    private $url;

    public function __construct($username, $pwd, $url) {

        $this->username = $username;
        $this->token = $pwd;
        $this->url = $url;
    }

    function AddSubscriberToListCronjob($email, $customfields = array(), $listId) {
        $xmlrequest = new SimpleXMLElement("<xmlrequest />");
        $xmlrequest->addChild('username', $this->username);
        $xmlrequest->addChild('usertoken', $this->token);
        $xmlrequest->addChild('requesttype', 'subscribers');
        $xmlrequest->addChild('requestmethod', 'AddSubscriberToList');
        $xmlrequest->addChild('details');
        $xmlrequest->details->addChild('emailaddress', $email);
        $xmlrequest->details->addChild('mailinglist', $listId);
        $xmlrequest->details->addChild('format', 'html');
        $xmlrequest->details->addChild('confirmed', '1');
        if (!empty($customfields) && is_array($customfields)) {
            $xmlrequest->details->addChild('customfields');
            foreach ($customfields as $fieldid => $fieldvalue) {
                $lastItem = $xmlrequest->details->customfields->addChild('item');
                $lastItem->addChild('fieldid', $fieldid);
                $lastItem->addChild('value', $fieldvalue);
            }
        }
        // Send Request to API
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlrequest->asXML());
        $result = curl_exec($ch);
        curl_close($ch);
        /* if ($result === false) {
          return "Fehler bei Abfrage.<br />";
          } else {
          $xml_doc = simplexml_load_string($result);
          if ($xml_doc->status != 'SUCCESS') {
          return 'Fehler: ' . $xml_doc->errormessage . '<br/>';
          }
          } */
    }

    function deleteContact($email, $listId) {
        $xml = '<xmlrequest> 
<username>' . $this->username . '</username>
<usertoken>' . $this->token . '</usertoken>
<requesttype>subscribers</requesttype>
<requestmethod>DeleteSubscriber</requestmethod>
<details>
<emailaddress>' . $email . '</emailaddress>
<list>' . $listId . '</list>
</details>
</xmlrequest>';
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $result = @curl_exec($ch);
        if ($result === false) {
            echo "Fehler bei Abfrage";
        } else {
            $xml_doc = simplexml_load_string($result);          
            if ($xml_doc->status == 'SUCCESS') {
                #print_r($result);
            } else {
                echo 'Fehler: ', $xml_doc->errormessage, '-',$listId;
            }
        }
    }

}
