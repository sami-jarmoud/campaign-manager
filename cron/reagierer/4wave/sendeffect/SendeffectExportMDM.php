<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

$kontaktFilterId = 0;
$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "98413824-b3ae-4eca-8af0-cfd67b591b07",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$apikeylist4Wave = array(
    '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
    '41034668-eb18-4301-bb65-13d853e83261',
    '40d15f98-2023-45fd-8842-ca16d8b2680f',
    '534c3a3b-b465-4225-b8ea-2c44f715dd16',
    'db1757ee-52bf-4179-b936-bebd403ba669'
);


$debug2 = FALSE;
$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug2);
$contactfiltersService->refreshContactFilterContacts((int) $kontaktFilterId, time() * 1000);


for ($count = 1; $count < 5; $count++) {

    $response = $contactsService->getContactsByFilterId((int) $kontaktFilterId, $count, 1000);

    foreach ($response->getResult() as $contact) {

        foreach ($apikeylist4Wave as $apikey) {

            //GetContact
            $config2 = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $apikey,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 600,
                "DEBUG" => "false" // NEVER enable on production
            );

            $contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
            $contactsService2->setDebug($debug2);

            $newContact = new com_maileon_api_contacts_Contact();
            $newContact->anonymous = FALSE;
            $newContact->email = $contact->email;
            $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
            $newContact->custom_fields['reagierer'] = TRUE;

            $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
        }
    }
}
