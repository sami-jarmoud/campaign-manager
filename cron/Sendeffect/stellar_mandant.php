<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Sendeffect' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'seCurl.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "98413824-b3ae-4eca-8af0-cfd67b591b07",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(2298, time()*1000);  


//Sendeffect Settings
$sendeffectWebservice = new seCurl(
        $username = 'stellar', 
        $token = '17d1ad0963205db79f597b0e0350651712a506fa', 
        $url ='https://sf14.sendsfx.com/xml.php'      
        );

$start = time();
for ($count = 1; $count < 100; $count++) {
    //Gesamtverteiler_Deutschland 4wave
    //Neu_Verteiler_Deutschland Stellar
    $response = $contactsService->getContactsByFilterId(2298, $count, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            ), array('PartnerID',
        'DeliveryID',
        'Neu_Verteiler_Deutschland',
        'Gesamtverteiler_Oesterreich',
        'Gesamtverteiler_Premium',
        'Gesamtverteiler_Schweiz')
    );

    foreach ($response->getResult() as $contact) {

        $customfields = array();
        $dateSE ='';
        $xqDate = $contact->standard_fields['BIRTHDAY'];
        if(!empty($xqDate)){            
        $xqDateArray = explode('-', $xqDate);
        $year = (int)$xqDateArray[0];
        $month = (int)$xqDateArray[1];
        $day = (int)$xqDateArray[2];
        $dateTime = new DateTime();
        $dateTime->setDate($year, $month, $day);
        $dateSE = $dateTime->format('d/m/Y');
        }
        
        $email = $contact->email;
        $customfields[16] = $contact->standard_fields['SALUTATION'];
        $customfields[17] = $contact->standard_fields['FIRSTNAME'];
        $customfields[30] = $contact->standard_fields['LASTNAME'];
        $customfields[18] = $contact->standard_fields['ADDRESS'];
        $customfields[19] = $contact->standard_fields['ZIP'];
        $customfields[20] = $contact->standard_fields['CITY'];
        $customfields[22] = $contact->standard_fields['COUNTRY'];
        $customfields[59] = $dateSE;
        $customfields[26] = $contact->custom_fields['PartnerID'];
        $customfields[27] = $contact->custom_fields['DeliveryID'];
        
        if((int)$contact->custom_fields['Neu_Verteiler_Deutschland'])
        {
          $listid = 25;  
        }
        if((int)$contact->custom_fields['Gesamtverteiler_Oesterreich']&& ($contact->standard_fields['COUNTRY'] == 'AT' || $contact->standard_fields['COUNTRY'] == 'at'))
        {
            $listid = 5;
        }
        if((int)$contact->custom_fields['Gesamtverteiler_Premium'])
        {
            $listid = 7;
        }
       if((int)$contact->custom_fields['Gesamtverteiler_Schweiz'] && ($contact->standard_fields['COUNTRY'] == 'ch' || $contact->standard_fields['COUNTRY'] == 'CH'))
       {
           $listid = 8;
       }

      $sendeffectWebservice->AddSubscriberToListCronjob($email, $customfields,$listid);
    }
}

$end = time();

echo "Mandant: Stellar SE Duration: " . ($end - $start) . " seconds";