<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Sendeffect' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'seCurl.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "db1757ee-52bf-4179-b936-bebd403ba669",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 90000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(890, time()*1000);  

//Sendeffect Settings
$sendeffectWebservice = new seCurl(
        $username = '4wave', 
        $token = '0211a820809c5f6a1452eb59a2dd41aa7071b82b', 
        $url = 'https://sf21.sendsfx.com/xml.php'      
        );

$start = time();
for ($count = 1; $count < 70; $count++) {
    //Gesamtverteiler_Deutschland 4wave
    //Neu_Verteiler_Deutschland Stellar
    $response = $contactsService->getContactsByFilterId(890, $count, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            ), array('PartnerID',
        'DeliveryID',
        'Gesamtverteiler_Deutschland',
        'Gesamtverteiler_Oesterreich',
        'Gesamtverteiler_Premium',
        'Gesamtverteiler_Schweiz')
    );

    foreach ($response->getResult() as $contact) {

        $customfields = array();
        $dateSE ='';
        $xqDate = $contact->standard_fields['BIRTHDAY'];
        if(!empty($xqDate)){            
        $xqDateArray = explode('-', $xqDate);
        $year = (int)$xqDateArray[0];
        $month = (int)$xqDateArray[1];
        $day = (int)$xqDateArray[2];
        $dateTime = new DateTime();
        $dateTime->setDate($year, $month, $day);
        $dateSE = $dateTime->format('d/m/Y');
        }

        
        $email = $contact->email;
        $customfields[1193] = $contact->standard_fields['SALUTATION'];
        $customfields[1192] = $contact->standard_fields['FIRSTNAME'];
        $customfields[1187] = $contact->standard_fields['LASTNAME'];
        $customfields[1191] = $contact->standard_fields['ADDRESS'];
        $customfields[1199] = $contact->standard_fields['ZIP'];
        $customfields[1188] = $contact->standard_fields['CITY'];
        $customfields[1186] = $contact->standard_fields['COUNTRY'];
        $customfields[1210] = $dateSE;
        $customfields[1189] = $contact->custom_fields['PartnerID'];
        $customfields[1195] = $contact->custom_fields['DeliveryID'];
        
        if((int)$contact->custom_fields['Gesamtverteiler_Deutschland'])
        {
          $listid = 1176;  
        }
         if((int)$contact->custom_fields['Gesamtverteiler_Oesterreich']&& ($contact->standard_fields['COUNTRY'] == 'AT' || $contact->standard_fields['COUNTRY'] == 'at'))
        {
            $listid = 1177;
        }
        if((int)$contact->custom_fields['Gesamtverteiler_Premium'])
        {
            $listid = 1179;
        }
       if((int)$contact->custom_fields['Gesamtverteiler_Schweiz'] && ($contact->standard_fields['COUNTRY'] == 'ch' || $contact->standard_fields['COUNTRY'] == 'CH'))
       {
           $listid = 1178;
       }

       $sendeffectWebservice->AddSubscriberToListCronjob($email, $customfields,$listid);
    }
}

$end = time();

echo "Mandant: 4Wave SE Duration: " . ($end - $start) . " seconds";