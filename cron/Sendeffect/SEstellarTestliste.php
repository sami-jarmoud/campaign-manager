<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Sendeffect' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'seCurl.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "d35fd0b2-8a95-421b-bb77-6e666a9fbd1e",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 100000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(44, time() * 1000);


$mandanten = array(
    array(
        'username' => 'stellar2',
        'token' => '82bc671d9822554e29e070d78b01049ea83fefed',
        'url' => 'https://sf14.sendsfx.com/xml.php',
        'listid' => 42,
        'SALUTATION' => 16,
        'FIRSTNAME' => 17,
        'LASTNAME' => 30,
        'BIRTHDAY' => 59
    ),
    array(
        'username' => 'stellar',
        'token' => '17d1ad0963205db79f597b0e0350651712a506fa',
        'url' => 'https://sf14.sendsfx.com/xml.php',
        'listid' => 9,
        'SALUTATION' => 16,
        'FIRSTNAME' => 17,
        'LASTNAME' => 30,
        'BIRTHDAY' => 59
    ),
);
foreach ($mandanten as $user) {

//Sendeffect Settings
    $sendeffectWebservice = new seCurl(
            $username = $user['username'], $token = $user['token'], $url = $user['url']
    );

    $start = time();

    $response = $contactsService->getContactsByFilterId(44, 1, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            )
    );

    foreach ($response->getResult() as $contact) {

        $customfields = array();
        $dateSE = '';
        $xqDate = $contact->standard_fields['BIRTHDAY'];
        if (!empty($xqDate)) {
            $xqDateArray = explode('-', $xqDate);
            $year = (int) $xqDateArray[0];
            $month = (int) $xqDateArray[1];
            $day = (int) $xqDateArray[2];
            $dateTime = new DateTime();
            $dateTime->setDate($year, $month, $day);
            $dateSE = $dateTime->format('d/m/Y');
        }

        $email = $contact->email;
        $customfields[(int)$user['SALUTATION']] = $contact->standard_fields['SALUTATION'];
        $customfields[(int)$user['FIRSTNAME']] = $contact->standard_fields['FIRSTNAME'];
        $customfields[(int)$user['LASTNAME']]= $contact->standard_fields['LASTNAME'];
        $customfields[(int)$user['BIRTHDAY']] = $dateSE;

        $sendeffectWebservice->AddSubscriberToListCronjob($email, $customfields, $user['listid']);
    }
}

$end = time();



