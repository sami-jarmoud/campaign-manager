<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');

$config = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => "1f3a4187-07de-4ac5-a5cb-61352915b8f4",
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 100000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug = FALSE;
$contactsService = new com_maileon_api_contacts_ContactsService($config);
$contactsService->setDebug($debug);

$contactfiltersService = new com_maileon_api_contactfilters_ContactfiltersService($config);
$contactfiltersService->setDebug($debug);
$contactfiltersService->refreshContactFilterContacts(59, time()*1000);  


 $mandanten = array(
                2092 =>'532ddc97-a5ce-4dce-88da-be441690d0a5',
                2177 =>'d682dc3c-2937-4946-b231-3bc63890a8f9',
                2178 =>'bddce743-e107-4f85-922a-b36a9e1d511d',
                2216 =>'50ff829c-f425-461b-ae05-aafa3e634ed3'
            );
foreach ($mandanten as $key => $apikey) {

$config2 = array(
    "BASE_URI" => "https://api.maileon.com/1.0",
    "API_KEY" => $apikey,
    "PROXY_HOST" => "",
    "PROXY_PORT" => "",
    "THROW_EXCEPTION" => true,
    "TIMEOUT" => 10000,
    "DEBUG" => "false" // NEVER enable on production
);

$debug2 = FALSE;
$contactsService2 = new com_maileon_api_contacts_ContactsService($config2);
$contactsService2->setDebug($debug2);

$start = time();

    $response = $contactsService->getContactsByFilterId(59, 1, 1000, array('SALUTATION',
        'FIRSTNAME',
        'LASTNAME',
        'ADDRESS',
        'ZIP',
        'CITY',
        'COUNTRY',
        'BIRTHDAY'
            )
    );

    foreach ($response->getResult() as $contact) {

        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->anonymous = FALSE;
        $newContact->email = $contact->email;
        $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $contact->standard_fields['SALUTATION'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $contact->standard_fields['FIRSTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $contact->standard_fields['LASTNAME'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ADDRESS] = $contact->standard_fields['ADDRESS'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$ZIP] = $contact->standard_fields['ZIP'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$CITY] = $contact->standard_fields['CITY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$COUNTRY] = $contact->standard_fields['COUNTRY'];
        $newContact->standard_fields[com_maileon_api_contacts_StandardContactField::$BIRTHDAY] = $contact->standard_fields['BIRTHDAY'];
        $newContact->custom_fields["Testliste-Stellar"] = TRUE;

        $contactsService2->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
        
    }
    
        $contactfiltersService2 = new com_maileon_api_contactfilters_ContactfiltersService($config2);
        $contactfiltersService2->setDebug($debug);
        $contactfiltersService2->refreshContactFilterContacts($key, time()*1000);
}

$end = time();

echo "TestListe Stellar: " . ($end - $start) . " seconds";

