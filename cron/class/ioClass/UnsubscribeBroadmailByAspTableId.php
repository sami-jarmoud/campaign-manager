<?php
class UnsubscribeBroadmailByAspTableId {
	/**
	 * @var PDO
	 */
	protected $dbh = null;
	
	/**
	 * @var PDO
	 */
	protected $emsDbh = null;
	
	/**
	 * @var \BroadmailRpcFactory
	 */
	protected $bmConnection = null;
	protected $aspTable = null;
	
	
	
	public function __construct() {
		try {
			$this->setDbh(
				new \PDO(
					'mysql:host=localhost;dbname=abmelder_ems',
					'abmelder',
					'2X6w5W9a'
				)
			);
			$this->getDbh()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			
			$this->setEmsDbh(
				new \PDO(
					'mysql:host=localhost;dbname=peppmt',
					'dbo49323410',
					'5C4i7E9v'
				)
			);
			$this->getDbh()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {
			throw new \Exception('connection Error!', 1424432963);
		}
	}
	
	
	/**
	 * unsubscribeAll
	 * 
	 * @param array $rcpIds
	 * @param integer $clientId
	 * @throws \DomainException
	 * @throws \Exception
	 * @return void
	 */
	public function unsubscribeAll(array $rcpIds, $clientId) {
		$query = 'SELECT `m_login`, `api_user`, `pw`'
			. ' FROM `mandant_asp`'
			. ' WHERE `m_id` = :clientId'
				. ' AND `asp_id` = 1'
		;

		$loginStmt = $this->getEmsDbh()->prepare($query);
		try {
			$loginStmt->bindParam(':clientId', $clientId, PDO::PARAM_INT);
			$loginStmt->execute();
			
			if (($row = $loginStmt->fetch(\PDO::FETCH_ASSOC)) === false) {
				throw new \DomainException('no deliverySystems data found!', 1424433072);
			}
			$loginStmt->closeCursor();

			$this->setBmConnection(
				new \BroadmailRpcFactory(
					$row['m_login'],
					$row['api_user'],
					$row['pw']
				)
			);
			if ($this->getBmConnection()->getError()) {
				throw new \Exception('Login Fehler oder Server nicht erreichbar', 1424433115);
			}
			
			// unsubscribeRecipients
			$this->unsubscribeRecipients($rcpIds);
		} catch (\Exception $e) {
			\MailUtils::sendMail(__FUNCTION__ . ': ' . $e->getMessage());
			
			// logUnsubscribeRecipentsError
			$this->logUnsubscribeRecipentsError(
				$rcpIds,
				$e->getMessage()
			);
		}
	}
	
	/**
	 * unsubscribeRecipients
	 * 
	 * @param array $rcpIds
	 * @param string $sqlQuery
	 * @return void
	 */
	protected function unsubscribeRecipients(array $rcpIds) {
		$sqlQuery = 'UPDATE ' . $this->getAspTable()
			. ' SET `status` = :status'
				. ', `processed` = NOW()'
			. ' WHERE `teilnehmer_id` = :participantId'
		;
		
		$stmp = $this->getDbh()->prepare($sqlQuery);
		try {
			$rcpUnsubscribe = $this->getBmConnection()->newUnsubscribeWebservice();

			$status = 1;
			foreach ($rcpIds as $rcpItem) {
				$rcpUnsubscribe->add(0, $rcpItem['email']);

				$stmp->bindParam(':status', $status, \PDO::PARAM_INT);
				$stmp->bindParam(':participantId', $rcpItem['id'], \PDO::PARAM_INT);
				$stmp->execute();
			}
		} catch (\PDOException $e) {
			\MailUtils::sendMail(__FUNCTION__ . ': ' . $e->getMessage());
		}
	}
	
	/**
	 * logUnsubscribeRecipentsError
	 * 
	 * @param array $rcpIds
	 * @param string $errorMessage
	 * @return void
	 */
	protected function logUnsubscribeRecipentsError(array $rcpIds, $errorMessage) {
		$sqlQuery = 'UPDATE ' . $this->getAspTable()
			. ' SET `reply` = :errorMessage'
				. ', `processed` = NOW()'
			. ' WHERE `teilnehmer_id` = :participantId'
		;

		$stmt = $this->getDbh()->prepare($sqlQuery);
		try {
			foreach ($rcpIds as $rcpItem) {
				$stmt->bindParam(':errorMessage', $errorMessage, \PDO::PARAM_STR);
				$stmt->bindParam(':participantId', $rcpItem['id'], \PDO::PARAM_INT);
				$stmt->execute();
			}
		} catch (\PDOException $e) {
			\MailUtils::sendMail(__FUNCTION__ . ': ' . $e->getMessage());
		}
	}

	public function __destruct() {
		if ($this->getBmConnection() !== null) {
			$this->getBmConnection()->logout();
		}
	}
	
	
	
	public function getDbh() {
		return $this->dbh;
	}
	public function setDbh($dbh) {
		$this->dbh = $dbh;
	}
	
	public function getEmsDbh() {
		return $this->emsDbh;
	}
	public function setEmsDbh($emsDbh) {
		$this->emsDbh = $emsDbh;
	}
	
	public function getBmConnection() {
		return $this->bmConnection;
	}
	public function setBmConnection($bmConnection) {
		$this->bmConnection = $bmConnection;
	}
	
	public function getAspTable() {
		return $this->aspTable;
	}
	public function setAspTable($aspTable) {
		$this->aspTable = $aspTable;
	}

}