<?php

class updateKajomiManager {

    public function getMailingData($url, $moduser, $modpassword, $mailing_id) {

        $hash = md5($modpassword . $mailing_id);

        $curl_url = 'http://' . $url . '/toolz/getdata.php?mod=' . $moduser . '&hash=' . $hash . '&msgid=' . $mailing_id . '&typ=mailinfo';

        $c = curl_init($curl_url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($c);

        $xml = simplexml_load_string($result);

        $start = $xml->versandDatum . " " . $xml->versandZeit;

        $mailingDataArr = array(
            "start" => $start,
            "versendet" => $xml->gesamt->menge->brutto,
            "softbounces" => $xml->gesamt->menge->softbounces,
            "hardbounces" => $xml->gesamt->menge->hardbounces,
            "openings_all" => $xml->gesamt->oeffner->gesamt,
            "openings" => $xml->gesamt->oeffner->unique,
            "klicks_all" => $xml->gesamt->klicker->gesamt,
            "klicks" => $xml->gesamt->klicker->unique
        );

        return $mailingDataArr;
    }

}

?>