<?php

class UnsubscribeMaileonStandard {

    private $authentification = NULL;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    public function unsubscribeAll($rcpIds, $mID) {
        $data = array();
        $err = NULL;
        $apikeylist = array();
        if ((int)$mID == 7) {
            $apikeylist = array(
                '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
                '41034668-eb18-4301-bb65-13d853e83261',
                '40d15f98-2023-45fd-8842-ca16d8b2680f',
                '534c3a3b-b465-4225-b8ea-2c44f715dd16',
                'db1757ee-52bf-4179-b936-bebd403ba669',
                '69a7bab6-8e27-4d73-b544-5fb8635b3db9'
            );
        }
        if ((int)$mID == 6) {
            $apikeylist = array(
                '532ddc97-a5ce-4dce-88da-be441690d0a5',
                'd682dc3c-2937-4946-b231-3bc63890a8f9',
                'bddce743-e107-4f85-922a-b36a9e1d511d',
                '50ff829c-f425-461b-ae05-aafa3e634ed3',
                '98413824-b3ae-4eca-8af0-cfd67b591b07',
                '6ca7acac-a78f-4124-a749-f1424c93e75c'
            );
        }
        if ((int)$mID == 8) {
            $apikeylist = array(
                '8bf22890-eb19-4fdf-bac9-22e28a253a93'
            );
        }        

        foreach ($apikeylist as $apikey) {
            $this->authentification = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $apikey,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 10000, // 5 seconds
                "DEBUG" => "false" // NEVER enable on production
            );
            $contactsService = new \com_maileon_api_contacts_ContactsService($this->authentification);
            $contactsService->setDebug(FALSE);
            try {
                foreach ($rcpIds as $rcpId) {

                    $response = $contactsService->unsubscribeContactByEmail(
                            $rcpId['email'], null, null
                    );
                    
                    if (!$response->isSuccess()) {
                        #throw \DebugAndExceptionUtils::sendDebugData($response->getBodyData());
                    }
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();
            }
        }
        return $data;
    }

}

?>
