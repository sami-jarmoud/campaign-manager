<?php

class UnsubscribeMailsolution
{
	private $curlOptions;
	private $curlConnection = null;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		$data = array();
		$err = NULL;
		$AID = NULL;
		$red = NULL;
		
		$qry_login = ' SELECT * '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 5 ';
			  
		$qry = ' UPDATE `asp_5` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_5` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
            $url = $row['m_login'];
			$AID = $row['AID'];
			$red = $row['red'];
        }
		
		$curl_url = 'http://'.$url.'/c/n?AID='.$AID.'&ACTION=DELETE&EMMAIL=&RED='.$red;
		$c = curl_init($curl_url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER,true);
		
		try {
			$err = curl_exec($c);
			if (strpos('EMID',$err) !== false) {
				throw new Exception($err);
			} else {
		
				foreach($rcpIds as $rcpId)
				{
					$status = 1;
					$email = $rcpId['email'];
					$id = $rcpId['id'];
					$output = '';
					$curl_url = 'http://'.$url.'/c/n?AID='.$AID.'&ACTION=DELETE&EMMAIL='.$email.'&RED='.$red;
					$c = curl_init($curl_url);
					$output = curl_exec($c);
					
					$stmt = $this->dbh->prepare($qry);
					$stmt->bindParam(':status', $status, PDO::PARAM_INT);
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);
					$stmt->execute();
				}
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return $data;
	}

}
?>
