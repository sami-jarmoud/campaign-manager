<?php

/**
 * Description of ImportUnsubscribeSendEffectStandard
 *
 * @author SamiMohamedJarmoud
 */
class ImportUnsubscribeSendEffectStandard {
     
    	private $curlOptions;
	private $curlConnection = null;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}
        
        public function importUnsubscribeAll($row){
            
            $data = array();
            $err = NULL;
		  
            $dateNow = new \DateTime('now');
	    $dateNow->setTime(23, 59, 59);
            $dateFrom = clone $dateNow;
	    $dateFrom->sub(new \DateInterval('P1D'));
	    $fromDateTime = $dateFrom->format('Y-m-d H:i:s');
         
                  $mandant_id = $row['m_login'];
                  $username = $row['api_user'];
                  $token = $row['pw'];
           
               try{
                    $post_data['module'] = 'subscribers';
		    $post_data['action'] = 'getunsubscribes';
                    $post_data['username'] = $username;
                    $post_data['token'] = $token;
                    $post_data['fromdatetime'] = $fromDateTime;
 
                   $curl_url = 'http://sf14.sendsfx.com/middleware/api/';
                   $curlConnection = curl_init($curl_url);
                    curl_setopt($curlConnection, CURLOPT_CONNECTTIMEOUT, 60);
                    curl_setopt($curlConnection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
                    curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curlConnection, CURLOPT_FOLLOWLOCATION, 1);                   
                        foreach ($post_data as $key => $value)
                        {
                            $post_items[] = $key . '=' . $value;
                        }
                        $post_string = implode ('&', $post_items);
                        curl_setopt($curlConnection, CURLOPT_POSTFIELDS, $post_string);

                        $result = curl_exec($curlConnection);
                        curl_close($curlConnection);
                         $xml = \simplexml_load_string($result);
                            foreach($xml->data->unsubscribes->unsubscribe as $unsubscribe){
                                    $data[]= array(
                                    'email' => $unsubscribe->emailaddress
                            );

                            }     

               } catch (Exception $ex) {
                      \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
               }
                
           return $data;
        }
}
