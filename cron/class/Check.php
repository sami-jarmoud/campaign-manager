<?php
require_once("Validate.php");

class Check {

	private $email;
	
	function __construct($email) {
		$this->email = trim($email);
	}
	
	function isEmail() {
		return Validate::email($this->email);
	}
}
?>