<?php
class UnsubscribeBroadmailFake
{

	private $bmConnection = null;
	private $bmList;
	private $bmLogin;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
			$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
			$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		
	    $qry_login = ' SELECT `m_login`, `api_user`, `pw` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
				.' AND `asp_id` = 6 '
				.' AND active = 1 ';
		
		$qry = ' UPDATE `asp_6` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_6` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  	  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
            $mandant_id = $row['m_login'];
            $api_user = $row['api_user'];
            $pw = $row['pw'];
        }

		try {
			$this->bmConnection = new BroadmailRpcFactory($mandant_id, $api_user, $pw);
			if($this->bmConnection->getError()) {
				throw new Exception('Login Fehler oder Server nicht erreichbar');
			} else {
				foreach ($rcpIds as $rcpId) {
						$rcpEmails[] = $rcpId['email'];
				}
				
				$rcpWs = $this->bmConnection->newRecipientWebservice();
				$rcpList = $this->bmConnection->newRecipientListWebservice();
				
				$listArr = $rcpList->getAllIds();
								
				foreach($listArr as $lid) {
					$status = 0;
					$isTestList = $rcpList->isTestRecipientList($lid);
					if(!$isTestList) {
						$check = $rcpWs->removeAll($lid, $rcpEmails);
					}
					
					if($check) {
						$status = 1;
					}
				}
				
				foreach($rcpIds as $rcpId) {
							$id = $rcpId['id'];
							$stmt = $this->dbh->prepare($qry);
							$stmt->bindParam(':status', $status, PDO::PARAM_INT);
							$stmt->bindParam(':id', $id, PDO::PARAM_INT);
							$stmt->execute();
				}
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		return true;
	}

	public function __destruct()
	{
		if($this->bmConnection !== null)
			$this->bmConnection->logout();
	}
}
?>