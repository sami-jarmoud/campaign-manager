<?php

class UnsubscribeElaine
{
	private $soapClient = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		$result = array();
		$task = '';
		
		$qry_login = ' SELECT `m_login`, `api_user`, `pw` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 3 ';
			  
		$qry = ' UPDATE `asp_3` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_3` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
            $mandant_id = $row['m_login'];
            $api_user = $row['api_user'];
            $pw = $row['pw'];
        }
		
		$curl_url = 'http://'.$api_user.':'.$pw.$mandant_id;
		
		$this->curlOptions = array (
			CURLOPT_URL				=> $curl_url,
			CURLOPT_HEADER 			=> false,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_FOLLOWLOCATION	=> false,
			CURLOPT_PORT			=> 80,
			CURLOPT_CONNECTTIMEOUT	=> 5,
			CURLOPT_TIMEOUT			=> 30,
			CURLOPT_POST			=> true
		);
		
		$c = curl_init($curl_url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		
		try {
			if (curl_exec($c) === false) {
				throw new Exception(curl_error($c));
			} else {
				foreach($rcpIds as $rcpId) {
						$rcpEmail = $rcpId['email'];
						
						$task .= '<task>';
						$task .= '<job>UPDATE_OR_INSERT</job>';
						$task .= '<user_data>';
						$task .= '<c_email>'.$rcpEmail.'</c_email>';
						$task .= '</user_data>';
						$task .= '<group_ids mode="replace">';
						$task .= '<id>RECYCLEBIN</id>';
						$task .= '</group_ids>';
						$task .= '</task>';
				}
				
					$xml_data = '<?xml version="1.0" encoding="UTF-8" ?>';
					$xml_data .= '<batch precedence="new" importEmpty="false" ignoreGroups="RECYCLEBIN,BLACKLIST,BOUNCES" defaultGroups="" key="c_email">';
					$xml_data .= '<return>';
					$xml_data .= '<pid job="[effective_job]">[p_id]</pid>';
					$xml_data .= '</return>';
					$xml_data .= '<tasks>';
					$xml_data .= $task;
					$xml_data .= '</tasks>';
					$xml_data .= '</batch>';
					
					$this->curlConnection = curl_init();
					curl_setopt_array($this->curlConnection, $this->curlOptions);
					curl_setopt($this->curlConnection, CURLOPT_POSTFIELDS, "data=".$xml_data);
					$result = curl_exec($this->curlConnection);
					
					foreach($rcpIds as $rcpId) {
						$id = $rcpId['id'];
						$status = 1;
						$stmt = $this->dbh->prepare($qry);
						$stmt->bindParam(':status', $status, PDO::PARAM_INT);
						$stmt->bindParam(':id', $id, PDO::PARAM_INT);
						$stmt->execute();
					}
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		return $result;
	}
}

?>