<?php

class UnsubscribeKajomi
{
	private $curlOptions;
	private $curlConnection = null;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		$data = array();
		$err = NULL;
		
		$qry_login = ' SELECT `m_login`, `api_user`, `pw` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 4 ';
			  
		$qry = ' UPDATE `asp_4` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_4` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
            $mandant_id = $row['m_login'];
        }
		
		$curl_url = 'http://'.$mandant_id.'/toolz/remove.php';
		$c = curl_init($curl_url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER,true);
		
		try {
			$err = curl_exec($c);
			if (strpos('no email',$err) !== false) {
				throw new Exception($err);
			} else {
		
				foreach($rcpIds as $rcpId)
				{
					$status = 1;
					$email = $rcpId['email'];
					$id = $rcpId['id'];
					$output = '';
					$curl_url = 'http://'.$mandant_id.'/toolz/remove.php?email='.$email;
					$c = curl_init($curl_url);
					$output = curl_exec($c);
					#print $email."\n";
					
					$stmt = $this->dbh->prepare($qry);
					$stmt->bindParam(':status', $status, PDO::PARAM_INT);
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);
					$stmt->execute();
				}
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
                        
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return $data;
	}

}
?>
