<?php

class UnsubscribeBackclick
{
	private $curlOptions;
	private $curlConnection = null;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
		$this->dbh = new PDO('mysql:host=localhost;dbname=abmelder_ems', 'abmelder', '2X6w5W9a');
		$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
	}

	public function unsubscribeAll($rcpIds, $mID)
	{
		$data = array();
		$err = NULL;
		
		$qry_login = ' SELECT `m_login`, `api_user`, `pw` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 2 ';
			  
		$qry = ' UPDATE `asp_2` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
		$qry_error = ' UPDATE `asp_2` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `teilnehmer_id` = :id ';
			  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
            $mandant_id = $row['m_login'];
            $api_user = $row['api_user'];
            $pw = $row['pw'];
        }
		
		$this->curlOptions = array (
			CURLOPT_URL				=> $mandant_id,
			CURLOPT_HEADER 			=> false,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_FOLLOWLOCATION	=> false,
			CURLOPT_PORT			=> 80,
			CURLOPT_CONNECTTIMEOUT	=> 5,
			CURLOPT_TIMEOUT			=> 30,
			CURLOPT_POST			=> true
		);
		
		$c = $this->curlConnection = curl_init($mandant_id);
		
		try {
			curl_exec($c);
			$err = curl_error($c);
			if ($err) {
				throw new Exception($err);
			} else {
				curl_setopt_array($this->curlConnection, $this->curlOptions);
		
				foreach($rcpIds as $rcpId)
				{
					$status = 1;
					$email = $rcpId['email'];
					$id = $rcpId['id'];
					$output = '';
					curl_setopt($this->curlConnection, CURLOPT_POSTFIELDS, 'EMAIL=' .urlencode($email). '&DELETE=1&MID='.$api_user);
		
					$output = curl_exec($this->curlConnection);
					$data[$email] = trim($output);
					$stmt = $this->dbh->prepare($qry);
					$stmt->bindParam(':status', $status, PDO::PARAM_INT);
					$stmt->bindParam(':id', $id, PDO::PARAM_INT);
					$stmt->execute();
					usleep(1000);
				}
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return $data;
	}

	public function __destruct()
	{
		if($this->curlConnection !== null)
			curl_close($this->curlConnection);
	}
}
?>
