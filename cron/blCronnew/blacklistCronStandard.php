<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/vhosts/dev.campaign-treaction.de/httpdocs/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/packages/broadmail/broadmail_rpc.php');

require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/BlacklistWebservice.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBroadmail.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/AddRobinsonRQ.obj');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBackclick.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blElaine.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blKajomi.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMailsolution.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBroadmailFake.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blSendEffect.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMaileon.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMaileonStandard.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$limit = 250;
$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

try {
    $blacklistWs = new blacklistWebservice();
    $cnt = $blacklistWs->countUnProcessed();
    $messageDataArray[] = 'count: ' . $cnt . \chr(13);
    if ($cnt > 0) {
        #$m_aspArr = $blacklistWs->getMandantAsps($mID);
        #$messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);
        $rcpIds = $blacklistWs->getAll($limit);
        $messageDataArray[] = 'count rcpIds: ' . count($rcpIds) . \chr(13);
        $blacklistML = new blacklistMaileonStandard();
        $ml_processed = $blacklistML->blacklistAll(
                $rcpIds, $mID
        );

        $messageDataArray[] = 'blacklistAll: ' . $ml_processed . \chr(13);
    }

    $messageDataArray[] = 'end: ' . \time() . \chr(13);
    //\DebugAndExceptionUtils::sendDebugData($messageDataArray, __FILE__ . ': ' . $mID);
} catch (\Exception $e) {
    \DebugAndExceptionUtils::sendDebugData($e, __FILE__ . ': ' . $mID);
}
unset($messageDataArray);
