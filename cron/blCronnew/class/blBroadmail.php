<?php
class blacklistBroadmail {
    private $bmConnection = null;
    private $bmList;
    private $bmLogin;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    public function blacklistAll($rcpIds, $mID) {
        $rcpEmails = array();
        $rcpReasons = array();

        $qry_login = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `masp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `m_id` = :mID '
                . ' AND `asp_id` = 1 ';

        $qry = ' UPDATE `asp_blacklist` '
                . ' SET `status` = :status, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        $qryUnprocessed = ' SELECT * '
                . ' FROM `asp_blacklist` as `asp` '
                . ' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID` '
                . ' WHERE `asp`.`mandant_asp_id` = :masp_id '
                . ' AND `asp`.`status` = 0 ';

        $qry_error = ' UPDATE `asp_blacklist` '
                . ' SET `reply` = :error_message, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach ($result_login as $row) {
            $masp_id = $row['masp_id'];
            $mandant_id = $row['m_login'];
            $api_user = $row['api_user'];
            $pw = $row['pw'];
        }

        try {
            $this->bmConnection = new BroadmailRpcFactory($mandant_id, $api_user, $pw);
            if ($this->bmConnection->getError()) {
                throw new \Exception('Login Fehler oder Server nicht erreichbar');
            } else {
                $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
                $stmtUnprocessed->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                $stmtUnprocessed->execute();
                $resultUnprocessed = $stmtUnprocessed->fetchAll();

                if (count($resultUnprocessed) > 0) {
                    foreach ($resultUnprocessed as $row) {
                        $rcpEmails[] = $row['Email'];
                        $rcpReasons[] = $row['Grund'];
                    }
					
                    $blacklistWs = $this->bmConnection->newBlacklistWebservice();

                    $blacklistWs->addAll($rcpEmails, $rcpReasons);
                    $check = $blacklistWs->areBlacklisted($rcpEmails);

                    if (count($check) > 0) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    
                    foreach ($resultUnprocessed as $row) {
                        $id = $row['ID'];
                        $stmt = $this->dbh->prepare($qry);
                        $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                        $stmt->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                }
            }
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
			
            foreach ($rcpIds as $rcpId) {
                $id = $rcpId['id'];
                $stmt_err = $this->dbh->prepare($qry_error);
                $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                $stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
                $stmt_err->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                $stmt_err->execute();
            }
        }
		
        return true;
    }

    public function __destruct() {
        if ($this->bmConnection !== null) {
			$this->bmConnection->logout();
		}
    }

}

?>