<?php
class CURLConnect {

	public $curlConn;

	public function __construct($url)
	{
		$this->curlConn = curl_init();

		$curlOptions = array (
			CURLOPT_URL				=> $url,
			CURLOPT_HEADER 			=> false,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_POST			=> true,
			CURLOPT_FOLLOWLOCATION	=> false,
			CURLOPT_PORT			=> 80,
			CURLOPT_CONNECTTIMEOUT	=> 2,
			CURLOPT_TIMEOUT			=> 10
		);
		curl_setopt_array($this->curlConn, $curlOptions);
	}

	function disconnect() {
		if($this->curlConn) {
			curl_close($this->curlConn);
		}
	}
}
?>