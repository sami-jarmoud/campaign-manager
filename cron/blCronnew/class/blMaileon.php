<?php

class blacklistMaileon {

    private $authentification = NULL;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
        $this->dbh->exec("set names utf8");
        $this->dbh2->exec("set names utf8");
    }

    public function blacklistAll($rcpIds, $mID, $maspId = 0) {

        $qry_login = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `masp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `m_id` = :mID ';
        #   .' AND `asp_id` = 8 ';
        
        $qry_login2 = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `asp_id` '
                . ' FROM `mandant_asp` '
                . ' WHERE `masp_id` = :maspId ';
        #   .' AND `asp_id` = 8 ';
        
        $qry = ' UPDATE `asp_blacklist` '
                . ' SET `status` = :status, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        $qryUnprocessed = ' SELECT * '
                . ' FROM `asp_blacklist` as `asp` '
                . ' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID` '
                . ' WHERE `asp`.`mandant_asp_id` = :masp_id '
                . ' AND `asp`.`status` = 0 ';

        $qry_error = ' UPDATE `asp_blacklist` '
                . ' SET `reply` = :error_message, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        
        if($maspId == 0){
            #var_dump("qry_login");
            $stmt_login = $this->dbh2->prepare($qry_login);
            $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);           
        }else {
      
            $stmt_login = $this->dbh2->prepare($qry_login2);
            $stmt_login->bindParam(':maspId', $maspId, PDO::PARAM_INT);              
        }
    #    $stmt_login = $this->dbh2->prepare($qry_login);
    #    $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();
        foreach ($result_login as $row) {
            $masp_id = $row['masp_id'];
            $mandant_id = $row['m_login'];
            $username = $row['api_user'];
            $pw = $row['pw'];

            if($maspId == 0){
                $mandantid = $masp_id;
            }else{
                $mandantid = $maspId;
            }
            
            switch ($mandantid) {
                
                case 12:  # Stellar Performance
                    $blId = 35910;
                    $apikey = "1f3a4187-07de-4ac5-a5cb-61352915b8f4";
                    break;                     

                case 15:  # 4Wave
                    $blId = 36260;
                    #$blId = 38110; #test_cronjobs_4W
                    $apikey = "d35fd0b2-8a95-421b-bb77-6e666a9fbd1e";
                    break;                    

                case 16 : # 4Wave 2018
                    $blId = 37357;
                   # $blId = 38111; #test_cronjobs_4W2018
                    $apikey = "44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32";
                    break;
                
                #case 10: # 4Wave Marketing Sandbox
                case 17: # 4Wave Marketing Sandbox
                    $blId = 37358;
                   # $blId = 38109; # test_cronjobs_4MS
                    $apikey = "41034668-eb18-4301-bb65-13d853e83261";
                    break;
                
                #case 11: # Stellar 2018
                case 18: # Stellar 2018
                    $blId = 37359;
                   # $blId = 38113; # test_cronjobs_S2018
                    $apikey = "532ddc97-a5ce-4dce-88da-be441690d0a5";
                    break;   

                #case 12: # Stellar Basis
                case 19: # Stellar Basis
                    $blId = 37360;
                   # $blId = 38114; #test_cronjobs_SBa
                    $apikey = "d682dc3c-2937-4946-b231-3bc63890a8f9";
                    break;  
                
                #case 13: # Stellar Performnace Sandbox
                case 20: # Stellar Performnace Sandbox
                   $blId = 37361;
                  #  $blId = 38115; # test_cronjobs_SPS
                    $apikey = "bddce743-e107-4f85-922a-b36a9e1d511d";
                    break;                
                
                #case 14: # 4Wave Basis
                case 21: # 4Wave Basis
                   $blId = 37451;
                  #  $blId = 38112; #test_cronjobs_4WBa
                    $apikey = "40d15f98-2023-45fd-8842-ca16d8b2680f";
                    break;              
                
                #case 15: # 4Wave Black
                case 22: # 4Wave Black    
                    $blId = 37471;
                  #  $blId = 38119; #test_cronjobs_4WB
                    $apikey = "534c3a3b-b465-4225-b8ea-2c44f715dd16";
                    break;

                #case 16: # Stellar Black
                case 23: # Stellar Black
                    $blId = 37472;
                  #  $blId = 38116; #test_cronjobs_SB
                    $apikey = "50ff829c-f425-461b-ae05-aafa3e634ed3";
                    break;                
 
                #case 19: # Stellar Reserve
                case 26: # Stellar Reserve    
                    $blId = 37875;
                  #  $blId = 38117; # test_cronjobs_SR
                    $apikey = "6ca7acac-a78f-4124-a749-f1424c93e75c";
                    break;       
               
                #case 20: # 4Wave Extra
                case 27: # 4Wave Extra
                    $blId = 37883;
                   # $blId = 38118; #test_cronjobs_4WE
                    $apikey = "69a7bab6-8e27-4d73-b544-5fb8635b3db9";
                    break; 
                
                case 28: # LeadWorld
                    $blId = 38011;
                    $apikey = "d682dc3c-2937-4946-b231-3bc63890a8f9";
                    break; 

                case 29: # LeadWorldBlack
                    $blId = 38122;
                    $apikey = "8bf22890-eb19-4fdf-bac9-22e28a253a93";
                    break;       
            
                case 30: # 4Wave MDM
                    $blId = 37552;
                    $apikey = "db1757ee-52bf-4179-b936-bebd403ba669";
                    break;                 
                
                case 31: # Stellar MDM
                    $blId = 37550;
                    $apikey = "98413824-b3ae-4eca-8af0-cfd67b591b07";
                    break;                  
/*                
                 case 29: # Taelish Angebot
                    #$blId = 38068;
                    $blId = 38130;
                    $apikey = "60accbe1-7dd4-4840-a9a7-96e85769ef2e ";
                    break;         
 */          
                 
                 
            }
            
            #var_dump($mandantid);
            #var_dump($blId);
            #var_dump($apikey);

            $this->authentification = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $pw,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 100, // 5 seconds
                "DEBUG" => "false" // NEVER enable on production
            );

            $blacklistsService = new \com_maileon_api_blacklists_BlacklistsService($this->authentification);
            $blacklistsService->setDebug(FALSE);
            try {
                #var_dump($id);
                $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
               # $stmtUnprocessed->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                $stmtUnprocessed->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                $stmtUnprocessed->execute();
                #var_dump($stmtUnprocessed->execute());
                $resultUnprocessed = $stmtUnprocessed->fetchAll();
                
                $importName = date("Y-m-d H:i:s") . ' Backlisten Sync';
                #var_dump("resultUnprocessed", $resultUnprocessed);
                if (count($resultUnprocessed) > 0) {
                    foreach ($resultUnprocessed as $row) {
                        $emails[] = $row['Email'];
                    }

                    $response = $blacklistsService->addEntriesToBlacklist(
                            $blId, $emails, $importName
                    );
                    #var_dump($response->isSuccess());

                    if ($response->isSuccess()) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    foreach ($resultUnprocessed as $row) {
                        $id = $row['ID'];
                        $stmt = $this->dbh->prepare($qry);
                        $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                        $stmt->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                    print $response->getResult();
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();

                foreach ($rcpIds as $rcpId) {
                    $id = $rcpId['id'];
                    $stmt_err = $this->dbh->prepare($qry_error);
                    $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                    $stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
                    $stmt_err->bindParam(':masp_id', $mandantid, PDO::PARAM_INT);
                    $stmt_err->execute();
                }
            }
        }

        return true;
    }

}

?>