<?php
\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');


require_once($workPath . 'cron/packages/broadmail/broadmail_rpc.php');

require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/BlacklistWebservice.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBroadmail.php');
#require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/AddRobinsonRQ.obj');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBackclick.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blElaine.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blKajomi.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMailsolution.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blBroadmailFake.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blSendEffect.php');
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'class/blMaileon.php');


require_once($workPath . 'cron/packages/'. \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');

$limit = 250;
#var_dump("BlacklistCron");
$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

try {
	$blacklistWs = new blacklistWebservice();
	$cnt = $blacklistWs->countUnProcessed();
	$messageDataArray[] = 'count: ' . $cnt . \chr(13);
    #var_dump($cnt);    
	if ($cnt >= 0) {
        if($maspId == 0){
            $m_aspArr = $blacklistWs->getMandantAsps($mID);    
        }else {
            $m_aspArr = $blacklistWs->getMandantAspsFromMaspId($maspId);  
        }    
        $messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);
		$rcpIds = $blacklistWs->getAll($limit);
		$messageDataArray[] = 'count rcpIds: ' . count($rcpIds) . \chr(13);
       # var_dump("m_aspArr", $m_aspArr);
		foreach ($m_aspArr as $m_asp) {

			$messageDataArray[] = \chr(13) . 'asp: ' . $m_asp . \chr(13);
            #var_dump("m_asp",$m_asp);
			switch ((int) $m_asp) {
				#case 1:
					#$blacklistBM = new blacklistBroadmail();
					#$bm_processed = $blacklistBM->blacklistAll(
						#$rcpIds,
						#$mID
					#);
					
					#$messageDataArray[] = 'blacklistAll: ' . $bm_processed . \chr(13);
					#break;
				
				case 2:
					$blacklistBackclick = new blacklistBackclick();
					$bc_processed = $blacklistBackclick->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $bc_processed . \chr(13);
					break;
				
				case 3:
					$blacklistElaine = new blacklistElaine();
					$el_processed = $blacklistElaine->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $el_processed . \chr(13);
					break;
				
				case 4:
					$blacklistKajomi = new blacklistKajomi();
					$k_processed = $blacklistKajomi->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $k_processed . \chr(13);
					break;
				
				case 5:
					$blacklistMS = new blacklistMailsolution();
					$ms_processed = $blacklistMS->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $ms_processed . \chr(13);
					break;
				
				case 6:
					$blacklistBMFake = new blacklistBroadmailFake();
					$bmFake_processed = $blacklistBMFake->blacklistAll(
						$rcpIds,
						$mID
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $bmFake_processed . \chr(13);
					break;
                                    
				case 7:
                case 17: #Stellar2
                case 18: #4Wave2       
                   
               # case 18:  # 4Wave2 
					$blacklistSE = new blacklistSendEffect();
					$se_processed = $blacklistSE->blacklistAll(
						$rcpIds,
						$mID,
                        $maspId
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $k_processed . \chr(13);
					break;  
                case 8:  # 4Wave
                case 9:  # 4Wave 2018
                case 10: # 4Wave Marketing Sandbox
                case 14: # 4Wave Basis
                case 15: # 4Wave Black
                case 20: # 4Wave Extra
                #case 22: # 4Wave MDM
                    
                case 11: # Stellar 2018
                case 12: # Stellar Basis
                case 13: # Stellar PerBlackformanceSandbox
                case 16: # Stellar Black
                case 19: # Stellar Reserve
                #case 23: # Stellar MDM
                    
                case 21: # LeadWorld Black
                    #var_dump("m_asp", $m_asp);
					$blacklistML = new blacklistMaileon();
					$ml_processed = $blacklistML->blacklistAll(
						$rcpIds,
						$mID,
                        $maspId
					);
					
					$messageDataArray[] = 'blacklistAll: ' . $k_processed . \chr(13);
					break;                                     		                                 
			}
		}
	}
	
	$messageDataArray[] = 'end: ' . \time() . \chr(13);
	//\DebugAndExceptionUtils::sendDebugData($messageDataArray, __FILE__ . ': ' . $mID);
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e, __FILE__ . ': ' . $mID);
}
unset($messageDataArray);