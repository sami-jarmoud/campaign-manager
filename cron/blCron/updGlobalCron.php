<?php
\error_reporting(null);
\ini_set('display_errors', false);

\set_time_limit(1200);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');


require(__DIR__ . \DIRECTORY_SEPARATOR. 'class/BlacklistWebservice.php');

require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$limit = 250;

$messageDataArray = array();
$messageDataArray[] = 'start: ' . \time() . \chr(13);

try {
	$blacklistWs = new blacklistWebservice();
	$cnt = $blacklistWs->countUnProcessed();
	$messageDataArray[] = 'count: ' . $cnt . \chr(13);
	
	if ($cnt > 0) {
		$blacklistWs->insert_asp_blacklist_table();
		$messageDataArray[] = 'insert_asp_blacklist_table: ' . 1 . \chr(13);
		
		$mIDsArr = $blacklistWs->getMandantID();
		$messageDataArray[] = 'mIDsArr: ' . \implode('', $mIDsArr) . \chr(13);
		
		$rcpIdsUnsub = $blacklistWs->getAllUnsub($limit);
		$messageDataArray[] = 'getAllUnsub: ' . count($rcpIdsUnsub) . \chr(13);

		/*foreach ($mIDsArr as $mID) {
			$blacklistWs->insertUnsub(
				$rcpIdsUnsub,
				$mID
			);
			$messageDataArray[] = 'insertUnsub: ' . 1 . \chr(13);
		}*/

		$rcpIds = $blacklistWs->getAll($limit);
		$messageDataArray[] = 'getAll: ' . count($rcpIds) . \chr(13);
		
		$blacklistWs->logUnsub($rcpIds);
		$messageDataArray[] = 'logUnsub: ' . 1 . \chr(13);
		
		$asp_allArr = $blacklistWs->getAllAsps();
		$messageDataArray[] = 'getAllAsps: ' . count($asp_allArr) . \chr(13);
		
		$blacklistWs->logProcessed(
			$rcpIds,
			$asp_allArr
		);
		$messageDataArray[] = 'logProcessed: ' . 1 . \chr(13);
	}
	
	$messageDataArray[] = 'end: ' . \time() . \chr(13);
	//\DebugAndExceptionUtils::sendDebugData($messageDataArray, __FILE__ . ': ' . $mID);
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e, __FILE__);
}
unset($messageDataArray);