<?php

class blacklistMaileonStandard {

    private $authentification = NULL;
    private $dbh = null;
    private $dbh2 = null;

    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
        $this->dbh->exec("set names utf8");
        $this->dbh2->exec("set names utf8");
    }

    public function blacklistAll($rcpIds, $mID) {


        $qry = ' UPDATE `asp_blacklist` '
                . ' SET `status` = :status, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';

        $qryUnprocessed = ' SELECT * '
                . ' FROM `asp_blacklist` as `asp` '
                . ' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID` '
                . ' WHERE `asp`.`mandant_asp_id` = :masp_id '
                . ' AND `asp`.`status` = 0 ';

        $qry_error = ' UPDATE `asp_blacklist` '
                . ' SET `reply` = :error_message, '
                . ' `processed` = NOW() '
                . ' WHERE `participient_id` = :id '
                . ' AND `mandant_asp_id` = :masp_id ';




        $masp_id = '0';
        $blacklist = array(
            '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32' => '37357', 
            '41034668-eb18-4301-bb65-13d853e83261' => '37358', 
            '532ddc97-a5ce-4dce-88da-be441690d0a5' => '37359', 
            'd682dc3c-2937-4946-b231-3bc63890a8f9' => '37360', 
            'bddce743-e107-4f85-922a-b36a9e1d511d' => '37361', 
            '40d15f98-2023-45fd-8842-ca16d8b2680f' => '37451', 
            '534c3a3b-b465-4225-b8ea-2c44f715dd16' => '37471', 
            '50ff829c-f425-461b-ae05-aafa3e634ed3' => '37472',
            '98413824-b3ae-4eca-8af0-cfd67b591b07' => '37550',
            'db1757ee-52bf-4179-b936-bebd403ba669' => '37552',
            '69a7bab6-8e27-4d73-b544-5fb8635b3db9' => '37842',
            '6ca7acac-a78f-4124-a749-f1424c93e75c' => '37843',
			'8bf22890-eb19-4fdf-bac9-22e28a253a93' => '38122', # LeadWorld Blacklist
			'6ca7acac-a78f-4124-a749-f1424c93e75c '=> '37875', # Stellar Reserve 
			'69a7bab6-8e27-4d73-b544-5fb8635b3db9 '=> '37883'  # 4Wave Extra

        );

        foreach ($blacklist as $apikey => $blId) {

            $this->authentification = array(
                "BASE_URI" => "https://api.maileon.com/1.0",
                "API_KEY" => $apikey,
                "PROXY_HOST" => "",
                "PROXY_PORT" => "",
                "THROW_EXCEPTION" => true,
                "TIMEOUT" => 100, // 5 seconds
                "DEBUG" => "false" // NEVER enable on production
            );
            $blacklistsService = new \com_maileon_api_blacklists_BlacklistsService($this->authentification);
            $blacklistsService->setDebug(FALSE);

            try {

                $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
                $stmtUnprocessed->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                $stmtUnprocessed->execute();
                $resultUnprocessed = $stmtUnprocessed->fetchAll();
                $importName = date("Y-m-d H:i:s") . ' Backlisten Sync';
                if (count($resultUnprocessed) > 0) {
                    foreach ($resultUnprocessed as $row) {
                        $emails[] = $row['Email'];
                    }
                    $response = $blacklistsService->addEntriesToBlacklist(
                            $blId, $emails, $importName
                    );
                   
                    #print $response->getResult();
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();

                foreach ($rcpIds as $rcpId) {
                    $id = $rcpId['id'];
                    $stmt_err = $this->dbh->prepare($qry_error);
                    $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                    $stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
                    $stmt_err->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                    $stmt_err->execute();
                }
            }
        }
                if ($response->isSuccess()) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                    foreach ($resultUnprocessed as $row) {
                        $id = $row['ID'];
                        $stmt = $this->dbh->prepare($qry);
                        $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                        $stmt->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
                        $stmt->execute();
                    }


        return true;
    }

}

?>