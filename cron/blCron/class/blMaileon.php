<?php
class blacklistMaileon
{
        private $authentification = NULL;
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
			$this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
			$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
			$this->dbh->exec("set names utf8");
			$this->dbh2->exec("set names utf8");
	}

	public function blacklistAll($rcpIds, $mID)
	{
		
	    $qry_login = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `masp_id` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 8 ';
		
		$qry = ' UPDATE `asp_blacklist` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `participient_id` = :id '
			  .' AND `mandant_asp_id` = :masp_id ';
		
		$qryUnprocessed = ' SELECT * '
			  .' FROM `asp_blacklist` as `asp` '
			  .' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID` '
			  .' WHERE `asp`.`mandant_asp_id` = :masp_id '
			  .' AND `asp`.`status` = 0 ';
			  
		$qry_error = ' UPDATE `asp_blacklist` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `participient_id` = :id '
			  .' AND `mandant_asp_id` = :masp_id ';
			  	  
        $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
	        $masp_id = $row['masp_id'];
            $mandant_id = $row['m_login'];
            $username = $row['api_user'];
            $pw = $row['pw'];
        }
        $this->authentification = array(
       "BASE_URI" => "https://api.maileon.com/1.0",
       "API_KEY" => $pw,
       "PROXY_HOST" => "",
       "PROXY_PORT" => "",
       "THROW_EXCEPTION" => true,
       "TIMEOUT" => 100, // 5 seconds
       "DEBUG" => "false" // NEVER enable on production
   );		
        
        $blacklistsService = new \com_maileon_api_blacklists_BlacklistsService($this->authentification);
	$blacklistsService->setDebug(FALSE);
		try {
                          if($masp_id == '12'){
                              $blId = 35910;                             
                          }
                          if($masp_id == '15'){
                              $blId = 36260;                             
                          }
                          if($masp_id == '28'){
                              $blId = 38011;                             
                          }
					  
			  $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
			  $stmtUnprocessed->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
			  $stmtUnprocessed->execute();
			  $resultUnprocessed = $stmtUnprocessed->fetchAll();
                          $importName = date("Y-m-d H:i:s") . ' Backlisten Sync';
			  if(count($resultUnprocessed) > 0) {
                              foreach($resultUnprocessed as $row) {
                                  $emails[] = $row['Email'];
                              }
                              $response = $blacklistsService->addEntriesToBlacklist(
                                      $blId,
                                      $emails,
                                      $importName
                                      );
                              if ($response->isSuccess()) {
                                  $status = 1;
                              }else{
                                  $status = 0;
                              }
				  foreach($resultUnprocessed as $row) {						
						$id = $row['ID'];    
						$stmt = $this->dbh->prepare($qry);
						$stmt->bindParam(':status', $status, PDO::PARAM_INT);
						$stmt->bindParam(':id', $id, PDO::PARAM_INT);
						$stmt->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
						$stmt->execute();
				}
                                #print $response->getResult();
			  }
				
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return true;
	}
}
?>