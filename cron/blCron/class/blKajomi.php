<?php
class blacklistKajomi
{
	private $dbh = null;
	private $dbh2 = null;

	public function __construct()
	{
			$this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
			$this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
			$this->dbh->exec("set names utf8");
			$this->dbh2->exec("set names utf8");
	}

	public function blacklistAll($rcpIds, $mID)
	{
		
	    $qry_login = ' SELECT `m_login`, `api_user`, `pw`, `verteiler`, `masp_id` '
	          .' FROM `mandant_asp` '
              .' WHERE `m_id` = :mID '
              .' AND `asp_id` = 4 ';
		
		$qry = ' UPDATE `asp_blacklist` '
			  .' SET `status` = :status, '
			  .' `processed` = NOW() '
			  .' WHERE `participient_id` = :id '
			  .' AND `mandant_asp_id` = :masp_id ';
		
		$qryUnprocessed = ' SELECT * '
			  .' FROM `asp_blacklist` as `asp` '
			  .' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID` '
			  .' WHERE `asp`.`mandant_asp_id` = :masp_id '
			  .' AND `asp`.`status` = 0 ';
			  
		$qry_error = ' UPDATE `asp_blacklist` '
			  .' SET `reply` = :error_message, '
			  .' `processed` = NOW() '
			  .' WHERE `participient_id` = :id '
			  .' AND `mandant_asp_id` = :masp_id ';
			  	  
	    $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $result_login = $stmt_login->fetchAll();

        foreach($result_login as $row) {
			$masp_id = $row['masp_id'];
            $mandant_id = $row['m_login'];
            $api_user = $row['api_user'];
            $pw = $row['pw'];
        }
		
		$curl_url = 'http://'.$mandant_id.'/toolz/remove.php';
		$c = curl_init($curl_url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER,true);
		
		try {
			$err = curl_exec($c);
						if (strpos('no email',$err) !== false) {
				throw new Exception($err);
			} else {
				
			  $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
			  $stmtUnprocessed->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
			  $stmtUnprocessed->execute();
			  $resultUnprocessed = $stmtUnprocessed->fetchAll();
			  if(count($resultUnprocessed) > 0) {
				  foreach($resultUnprocessed as $row) {
						$status = 1;
						$email = '';
						$email = $row['Email'];
						$id = $row['ID'];
						$output = '';
						$hash = '';
						
						if (strpos($email,'*') !== false) {
							$domainArr = explode('@',$email);
							$email = $domainArr[1];
						}
						
						$hash = md5($pw.$email);
						$curl_url = 'http://'.$mandant_id.'/toolz/remove.php?mod='.$api_user.'&hash='.$hash.'&email='.$email.'&typ=black';
						$c = curl_init($curl_url);
						$output = curl_exec($c);
						
						$stmt = $this->dbh->prepare($qry);
						$stmt->bindParam(':status', $status, PDO::PARAM_INT);
						$stmt->bindParam(':id', $id, PDO::PARAM_INT);
						$stmt->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
						$stmt->execute();
				}
			  }
				
			}
		} catch (Exception $e) {
			$error_message = $e->getMessage();
			
			foreach($rcpIds as $rcpId) {
				$id = $rcpId['id'];
				$stmt_err = $this->dbh->prepare($qry_error);
				$stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
				$stmt_err->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt_err->bindParam(':masp_id', $masp_id, PDO::PARAM_INT);
				$stmt_err->execute();
			}
		}
		
		return true;
	}

}
?>