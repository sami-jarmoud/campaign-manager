<?php
class blacklistMailsolution {
    private $dbh = null;
    private $dbh2 = null;
    private $debug = false;
    
    
    
    
    
    public function __construct() {
        $this->dbh = new PDO('mysql:host=localhost;dbname=blacklist', 'blackuser', 'ati21coyu09t');
        $this->dbh2 = new PDO('mysql:host=localhost;dbname=peppmt', 'dbo49323410', '5C4i7E9v');
    }

    
    public function blacklistAll($rcpIds, $mID) {
        $task = null;
        $status = 0;

        $qry_login = 'SELECT *' 
            . ' FROM `mandant_asp`' 
            . ' WHERE `m_id` = :mID' 
                . ' AND `asp_id` = 5'
        ;
        $stmt_login = $this->dbh2->prepare($qry_login);
        $stmt_login->bindParam(':mID', $mID, PDO::PARAM_INT);
        $stmt_login->execute();
        $loginDataArray = $stmt_login->fetch(PDO::FETCH_ASSOC);
        
        $qryUnprocessed = 'SELECT *' 
            . ' FROM `asp_blacklist` as `asp`' 
                . ' INNER JOIN `Blacklist` as `b` ON `asp`.`participient_id` = `b`.`ID`' 
            . ' WHERE `asp`.`mandant_asp_id` = :masp_id' 
                . ' AND `asp`.`status` = 0'
        ;
        $stmtUnprocessed = $this->dbh->prepare($qryUnprocessed);
        $stmtUnprocessed->bindParam(':masp_id', $loginDataArray['masp_id'], PDO::PARAM_INT);
        $stmtUnprocessed->execute();
        $resultUnprocessed = $stmtUnprocessed->fetchAll(PDO::FETCH_ASSOC);
        
        echo nl2br('resultUnprocessed: ' . count($resultUnprocessed) . chr(13));
        if (count($resultUnprocessed) > 0) {
            foreach ($resultUnprocessed as $row) {
                $task .= '<blacksheep><![CDATA[' . $row['Email'] . ']]></blacksheep>';
            }
            
            $xml_data = '<?xml version="1.0" encoding="ISO-8859-1" ?>';
            $xml_data .= '<!DOCTYPE import SYSTEM "https://xml.fagms.net/XMLInterface.dtd">';
            $xml_data .= '<import>';
            $xml_data .= '<transaction_id>1234</transaction_id>';
            $xml_data .= '<respond_to>NOW</respond_to>';
            $xml_data .= '<response_type>SHORT</response_type>';
            $xml_data .= '<login>';
            $xml_data .= '<net>' . $loginDataArray['verteiler'] . '</net>';
            $xml_data .= '<uid>' . $loginDataArray['api_user'] . '</uid>';
            $xml_data .= '<pwd>' . $loginDataArray['pw'] . '</pwd>';
            $xml_data .= '</login>';
            $xml_data .= '<orders>';
            $xml_data .= '<insert_blacksheep>';
            $xml_data .= '<prof_id>' . $loginDataArray['profileID'] . '</prof_id>';
            $xml_data .= $task;
            $xml_data .= '</insert_blacksheep>';
            $xml_data .= '</orders>';
            $xml_data .= '</import>';
            
            
            if ((boolean) $_GET['debug'] === true && $_SERVER['REMOTE_ADDR'] == '87.138.67.216') {
                $this->debug = true;
            }
            
            try {
                $ch = curl_init('http://xml.fagms.net/cgi-bin/xmlinterface');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                #curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($ch);

                if (curl_errno($ch)) {
                    throw new Exception(curl_error($ch));
                } else {
                    $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    if ($resultStatus != 200) {
                        throw new Exception(curl_error($ch));
                    } else {
                        $status = 1;
                    }
                }
                curl_close($ch);
                unset($result);

                $qry = 'UPDATE `asp_blacklist`' 
                    . ' SET `status` = :status' 
                        . ', `processed` = NOW()' 
                    . ' WHERE `participient_id` = :id' 
                        . ' AND `mandant_asp_id` = :masp_id'
                ;
                foreach ($resultUnprocessed as $row) {
                    $stmt = $this->dbh->prepare($qry);
                    $stmt->bindParam(':status', $status, PDO::PARAM_INT);
                    $stmt->bindParam(':id', $row['ID'], PDO::PARAM_INT);
                    $stmt->bindParam(':masp_id', $loginDataArray['masp_id'], PDO::PARAM_INT);
                    $stmt->execute();
                }
            } catch (Exception $e) {
                $error_message = $e->getMessage();
                
                $qryError = 'UPDATE `asp_blacklist`' 
                    . ' SET `reply` = :error_message' 
                        . ', `processed` = NOW()' 
                    . ' WHERE `participient_id` = :id' 
                        . ' AND `mandant_asp_id` = :masp_id'
                ;
                foreach ($rcpIds as $rcpId) {
                    $stmt_err = $this->dbh->prepare($qryError);
                    $stmt_err->bindParam(':error_message', $error_message, PDO::PARAM_STR);
                    $stmt_err->bindParam(':id', $rcpId['id'], PDO::PARAM_INT);
                    $stmt_err->bindParam(':masp_id', $loginDataArray['masp_id'], PDO::PARAM_INT);
                    $stmt_err->execute();
                }
                
                if ($this->debug === true) {
                    echo nl2br($error_message);
                }
            }
        }
        
        return true;
    }
}
?>