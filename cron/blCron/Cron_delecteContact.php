<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Sendeffect' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'seCurl.php');

require_once(DIR_Utils . 'DebugAndExceptionUtils.php');

function getContactForDelete($bl_db) {

    $dateNow = new DateTime('now');
    $dateNow->sub(new DateInterval('P30D'));
    //Test ein Tag
    #$dateNow->sub(new DateInterval('P1D'));
    $dateNow->setTime(00, 00, 00);
    $dateTime = $dateNow->format('Y-m-d H:i:s');
    $contactDataArray = array();
    $sql = 'SELECT * FROM `DeleteContact` WHERE `Create_Datum` < \''. $dateTime .'\'' ;
    #$sql = 'SELECT * FROM `DeleteContact` ';
    if (($res = mysql_query($sql, $bl_db))) {
        while (($row = mysql_fetch_array($res)) !== false) {
            $contactDataArray[$row['id']] = $row;
        }
        mysql_free_result($res);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
    }

    return $contactDataArray;
}

// blacklist db
$bl_db = mysql_connect('localhost', 'blackuser', 'ati21coyu09t') or die('Verbindungsversuch fehlgeschlagen, Black');
mysql_select_db('blacklist', $bl_db);

// ems db
$ems_db = mysql_connect('localhost', 'dbo49323410', '5C4i7E9v') or die('Verbindungsversuch fehlgeschlagen, Ems');
mysql_select_db('peppmt', $ems_db);

//Kontakte nach 30 Tage vom Löschauftrag aus DB aufrufen
$contactDataArray = getContactForDelete($bl_db);
#var_dump($contactDataArray);
foreach ($contactDataArray as $contactData) {
    $mandant = $contactData['db_abkz'];
    $email = $contactData['Email'];
    switch ($mandant) {

        case 'stellarPerformance':
            $db_name = 'stellar_maas_db';
            $db_user = 'stellar-user-db';
            $db_pw = 'V1s9S3y6';
            $apikeylist = array(
                '1f3a4187-07de-4ac5-a5cb-61352915b8f4', // stellar
                'd682dc3c-2937-4946-b231-3bc63890a8f9', //basis
                '532ddc97-a5ce-4dce-88da-be441690d0a5', //18
                '6ca7acac-a78f-4124-a749-f1424c93e75c', //reserve
                '98413824-b3ae-4eca-8af0-cfd67b591b07', //MDM
                '50ff829c-f425-461b-ae05-aafa3e634ed3', //Black
                'bddce743-e107-4f85-922a-b36a9e1d511d' //Sandbox
            );
            $se_login = array(
                'client_1' => array(
                    'm_login' => 'http://sf14.sendsfx.com/xml.php',
                    'api_user' => 'stellar',
                    'pw' => '17d1ad0963205db79f597b0e0350651712a506fa',
                    'listID' => array(
                        '25',
                        '5',
                        '8',
                        '7'
                    )
                ),
                'client_2' => array(
                    'm_login' => 'http://sf14.sendsfx.com/xml.php',
                    'api_user' => 'stellar2',
                    'pw' => '82bc671d9822554e29e070d78b01049ea83fefed',
                    'listID' => array(
                        '38',
                        '39',
                        '40',
                        '41'
                    )
                )
            );
            break;

        case '4waveMarketing':
            $db_name = '4wave_maas_db';
            $db_user = '4wave-user-db';
            $db_pw = '5U8t9N8j';
            $apikeylist = array(
                '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32', //18
                '40d15f98-2023-45fd-8842-ca16d8b2680f', //basis
                '69a7bab6-8e27-4d73-b544-5fb8635b3db9', //extra
                'd35fd0b2-8a95-421b-bb77-6e666a9fbd1e', //4wave
                'db1757ee-52bf-4179-b936-bebd403ba669', //MDM
                '534c3a3b-b465-4225-b8ea-2c44f715dd16', //Black
                '41034668-eb18-4301-bb65-13d853e83261' //Sandbox
            );
            $se_login = array(
                'client_1' => array(
                    'm_login' => 'http://sf21.sendsfx.com/xml.php',
                    'api_user' => '4wave',
                    'pw' => '0211a820809c5f6a1452eb59a2dd41aa7071b82b',
                    'listID' => array(
                        '1176',
                        '1177',
                        '1178',
                        '1179'
                    )
                ),
                'client_2' => array(
                    'm_login' => 'http://sf14.sendsfx.com/xml.php',
                    'api_user' => '4wave2',
                    'pw' => '17d6df85da1bedf492da0d181789e500c8c3bb5f',
                    'listID' => array(
                        '45',
                        '46',
                        '47',
                        '48'
                    )
                )
            );
            break;

        case 'leadWorld':
            $db_name = 'leadworld_maas_db';
            $db_user = 'lead-user-db';
            $db_pw = '4H5d4Q4t';
            $apikeylist = array(
                '229fe8f6-4b46-459e-a287-bf3f0841f358', //lead
                '8bf22890-eb19-4fdf-bac9-22e28a253a93'  //Black
            );
            break;
        default:
            throw new \InvalidArgumentException('no client set!');
    }

    //Kontakte im VS XQ löschen

    foreach ($apikeylist as $apikey) {

        //GetContact
        $config = array(
            "BASE_URI" => "https://api.maileon.com/1.0",
            "API_KEY" => $apikey,
            "PROXY_HOST" => "",
            "PROXY_PORT" => "",
            "THROW_EXCEPTION" => true,
            "TIMEOUT" => 600,
            "DEBUG" => "false" // NEVER enable on production
        );
        $debug = FALSE;
        $contactsService = new com_maileon_api_contacts_ContactsService($config);
        $contactsService->setDebug($debug);

        $response = $contactsService->deleteContactByEmail($email);
        if ($response->isSuccess()) {
            var_dump($email . ' ist gelöscht. Mandant API KEY: '.$apikey);
        } else {
            var_dump($email . ' ist nicht gelöscht Mandant API KEY: '.$apikey);
        }
    }
    //Kontakte im VS SE löschen
    foreach ($se_login as $client) {
        //SE function delete Contact
        $sendeffectWebservice = new seCurl(
                $username = $client['api_user'], $token = $client['pw'], $url = $client['m_login']
        );
        $listIDs = $client['listID'];
        foreach ($listIDs as $listId) {
            $sendeffectWebservice->deleteContact($email, $listId);
        }
    }

    //Kontakte im CM löschen
    $mandant_db = mysql_connect('localhost', $db_user, $db_pw) or die('Verbindungsversuch fehlgeschlagen, ' . $db_name);
    mysql_select_db($db_name, $mandant_db) or die(\mysql_error($mandant_db));
    //SQL Delete Contact
    $sql = 'DELETE FROM `history` WHERE `EMAIL` = \'' . $email . '\'';
    $res = mysql_query($sql, $mandant_db);
    if($res){
        $sql2 = 'DELETE FROM `DeleteContact` WHERE `EMAIL` = \'' . $email . '\'';
        $res2 = mysql_query($sql2, $bl_db);
        if(!$res2){
             throw new \InvalidArgumentException('Der Kontakt '.$email.' ist nicht gelöscht aus DB');
        }
    }
} 


