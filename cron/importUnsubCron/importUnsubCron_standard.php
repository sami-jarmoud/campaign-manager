<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/class/UnsubscribeWebservice.php');
require_once($workPath . 'cron/class/ImportUnsubscribeBroadmail.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffect.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileon.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileonStandard.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffectStandard.php');

require_once($workPath . 'cron/packages/' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

$limit = 250;

try {
    $unsubWs = new UnsubscribeWebservice();
    #$m_aspArr = $unsubWs->getMandantAsps($mID);
    #$messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);


   $apikeylistWave = array(
        '44f4089f-c6bc-4cce-bfbc-29fc7f4f0b32',
        '41034668-eb18-4301-bb65-13d853e83261',
        '40d15f98-2023-45fd-8842-ca16d8b2680f',
        '534c3a3b-b465-4225-b8ea-2c44f715dd16',
        'db1757ee-52bf-4179-b936-bebd403ba669',
        '69a7bab6-8e27-4d73-b544-5fb8635b3db9'
    );

    $apikeylistStellar = array(
        '532ddc97-a5ce-4dce-88da-be441690d0a5',
        'd682dc3c-2937-4946-b231-3bc63890a8f9',
        'bddce743-e107-4f85-922a-b36a9e1d511d',
        '50ff829c-f425-461b-ae05-aafa3e634ed3',
        '98413824-b3ae-4eca-8af0-cfd67b591b07',
        '6ca7acac-a78f-4124-a749-f1424c93e75c'
    );
   $result_login = array(
                array(
                   'm_login' => 'http://sf14.sendsfx.com/middleware/api/',
                   'api_user' => 'stellar2',
                   'pw' => '82bc671d9822554e29e070d78b01049ea83fefed',
                   'mid' => 6
                ),
                 array(
                   'm_login' => 'http://sf14.sendsfx.com/middleware/api/',
                   'api_user' => '4wave2',
                   'pw' => '17d6df85da1bedf492da0d181789e500c8c3bb5f',
                   'mid' => 7
                )
            );
    foreach ($apikeylistWave as $apikeyWave) {
        $impUnsubMLWave = new \ImportUnsubscribeMaileonStandard();
        $rcpIds_mlWave = $impUnsubMLWave->importUnsubscribeAll(
                $apikeyWave
        );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_mlWave) . \chr(13);

        if (count($rcpIds_mlWave) > 0) {
            $mID = 7;
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_mlWave, $mID
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
            #var_dump($ml_processed);
        }
    }

    foreach ($apikeylistStellar as $apikeyStellar) {

        $impUnsubMLStellar = new \ImportUnsubscribeMaileonStandard();
        $rcpIds_mlStellar = $impUnsubMLStellar->importUnsubscribeAll(
                $apikeyStellar
        );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_mlStellar) . \chr(13);

        if (count($rcpIds_mlStellar) > 0) {
            $mID = 6;
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_mlStellar, $mID
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
           # var_dump($ml_processed);
        }
    }
    foreach ($result_login as $row) {

        $impUnsubSEMandant = new \ImportUnsubscribeSendEffectStandard();
        $rcpIds_seMandant = $impUnsubSEMandant->importUnsubscribeAll(
                $row
                );
        $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_seMandant) . \chr(13);
       
        if (count($rcpIds_seMandant) > 0) {
            $mID = $row['mid'];
            $ml_processed = $unsubWs->addRecipients(
                    $rcpIds_seMandant, $row['mid']
            );

            $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
           # var_dump($ml_processed);
        }
    }
} catch (Exception $ex) {
    \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
}

unset($messageDataArray);
