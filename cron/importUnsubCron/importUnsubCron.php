<?php

\error_reporting(null);
\ini_set('display_errors', false);

$workPath = '/var/www/maas/data/www/maas.treaction.de/';
\define('DIR_Utils', $workPath . 'system/library/Utils/');

require_once($workPath . 'cron/class/UnsubscribeWebservice.php');
require_once($workPath . 'cron/class/ImportUnsubscribeBroadmail.php');
require_once($workPath . 'cron/class/ImportUnsubscribeSendEffect.php');
require_once($workPath . 'cron/class/ImportUnsubscribeMaileon.php');

require_once($workPath . 'cron/packages/'. \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');


$messageDataArray = array();
$messageDataArray[] = 'clientId: ' . $mID . \chr(13);
$messageDataArray[] = 'start: ' . \time() . \chr(13);

$limit = 250;


try{
    	$unsubWs = new UnsubscribeWebservice();
	$m_aspArr = $unsubWs->getMandantAsps($mID);

	$messageDataArray[] = 'asps: ' . \implode('', $m_aspArr) . \chr(13);
        
           foreach ($m_aspArr as $m_asp) {
			$messageDataArray[] = \chr(13) . 'asp: ' . $m_asp . \chr(13);
                        
			switch ((int) $m_asp) {

                            case 7:
                                $impUnsubSE = new \ImportUnsubscribeSendEffect();
                                $rcpIds_se = $impUnsubSE->importUnsubscribeAll(
                                        $mID
                                        );
                                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_se) . \chr(13);

                                if (count($rcpIds_se) > 0) {
                                    
                                    $se_processed = $unsubWs->addRecipients(
                                            $rcpIds_se, 
                                            $mID
                                            );

                                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                                    var_dump($se_processed);
                                }
                                break; 
      
                            case 8:
                                
                                $impUnsubML = new \ImportUnsubscribeMaileon();
                                $rcpIds_ml = $impUnsubML->importUnsubscribeAll(
                                        $mID
                                        );

                                $messageDataArray[] = 'rcpCount: ' . \count($rcpIds_ml) . \chr(13);

                                if (count($rcpIds_ml) > 0) {
                                    
                                    $ml_processed = $unsubWs->addRecipients(
                                            $rcpIds_ml, 
                                            $mID
                                            );

                                    $messageDataArray[] = 'ImportUnsubscribeAll: ' . 1 . \chr(13);
                                    var_dump($ml_processed);
                                }
                                break;                                 

			}
		}
    
} catch (Exception $ex) {
     \DebugAndExceptionUtils::sendDebugData($ex, __FILE__ . ': ' . $mID);
}

unset($messageDataArray);