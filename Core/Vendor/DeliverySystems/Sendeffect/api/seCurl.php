<?php

class seCurl {

    private $username;
    private $token;
    private $useragent;
    private $handle;
    public $url;
    public $mailingId;
    public $newsletterUrl;
    public $subject;
    public $title;
    public $createdate;

    public function __construct($username, $pwd, $clientId,$useragent) {

        $this->username = $username;
        $this->token = $pwd;
        $this->useragent = $useragent;
        $this->url = $clientId;
    }

    public function create($subject, $name, $html, $text, $newsletterUrl, $aff) {
        try {
            $this->setOpt(
                    $this->url
            );
            $data['module'] = 'newsletter';
            $data['action'] = 'insert';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = '';
            if (!empty($newsletterUrl)) {
                $data['url'] = $newsletterUrl;
            }
            if (!empty($html)) {
                $data['html'] = \urlencode($html);
            }
            if (!empty($text)) {
                $data['text'] = \urlencode($text);
            }
            if (!empty($subject)) {
                $data['subject'] = \urlencode($subject);
            }
            if (!empty($name)) {
                $data['name'] = '[AFF=' . $aff . '] ' . \urlencode($name);
            }

            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }
            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);
            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
            $xml_doc = \simplexml_load_string($result);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }
      
        } catch (\Exception $ex) {
            throw $ex;
        }
        
        return $xml_doc->data;
    }

    public function update($mailingId, $html, $text, $subject, $name, $aff) {
        try {
            $this->setOpt(
                    $this->url
            );
            $data['module'] = 'newsletter';
            $data['action'] = 'update';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = $mailingId;
            if (!empty($html)) {
                $data['html'] = \urlencode($html);
            }
            if (!empty($text)) {
                 $data['text'] = \urlencode($text);
               }
            if (!empty($subject)) {
                $data['subject'] = \urlencode(\htmlspecialchars_decode($subject,ENT_QUOTES));
            }
            if (!empty($name)) {
                $data['name'] = '[AFF=' . $aff . '] ' . \urlencode(\htmlspecialchars_decode($name,ENT_QUOTES));
            }
            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);
            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
             $xml_doc = \simplexml_load_string($result);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $xml_doc->data;
    }

    public function delete($mailingId) {
        try {
            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'newsletter';
            $data['action'] = 'delete';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = $mailingId;

            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
             $xml_doc = \simplexml_load_string($result);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }           
        } catch (Exception $ex) {
            throw $ex;
        }

        return $xml_doc->data;
    }
    
     public function getHTMLContent($mailingId) {
        try {
            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'newsletter';
            $data['action'] = 'view';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = $mailingId;

            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
             return $result;
             
        } catch (Exception $ex) {
            throw $ex;
        }

        return $xml_doc->data;
    }
    
    public function actionJob($externalJobId, $action){
     try {
            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'newsletter';
            $data['action'] = $action;
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = $externalJobId;

            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);

             $xml_doc = \simplexml_load_string($result);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }           
        } catch (Exception $ex) {
            throw $ex;
        }

        return $xml_doc->data;
    }
    
    public function send($mailingId, $datetime, $segmentIds, $listIds, $senderName, $maxSubscribers, $create, $sendermail, $replytomail) {
        try {
            $this->setOpt(
                    $this->url
            );
            $data['module'] = 'newsletter';
            $data['action'] = 'send';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['datetime'] = \strtotime($datetime);
            $data['externalid'] = $mailingId;
            $data['sendermail'] = $sendermail;
            $data['replytomail'] = $replytomail;
            $data['sendername'] = $senderName;
            $data['maxsubscribers'] = $maxSubscribers;
            
            if (!empty($segmentIds)) {
                #$data['segmentids'] = $segmentIds;
            }
            
            if (!empty($listIds)) {
                $data['listids'] = implode(",",$listIds); // Versand entweder an Listen oder an Segmente
            }
            
            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
            $xml_doc = \simplexml_load_string($result);
            
             if($xml_doc->success == 0){
                if($create === TRUE){ 
                 //Kampagne löschen
                 $this->delete(
                         $mailingId
                         );
                }
                 throw new \Exception($xml_doc->data);
             }
        } catch (\Exception $ex) {
            throw $ex;
        }
        
        return $xml_doc->data->externaljobid;
    }
    public function sendTestMail($mailingId, $segmentIds, $listIds, $senderMail, $senderName,$emails,$sendermail,$replytomail) {
       
        try {
            $this->setOpt(
                    $this->url
            );
            $data['module'] = 'newsletter';
            $data['action'] = 'send';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['datetime'] = '';
            $data['externalid'] = $mailingId;
            $data['sendermail'] = $sendermail;
            $data['replytomail'] = $replytomail;
            $data['sendername'] = $senderName;
            $data['emails'] = $emails;
             $data['listids'] = $listIds;
            if (!empty($segmentIds)) {
                #$data['segmentids'] = $segmentIds;
            }
                       
            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);
   
            $result = curl_exec($this->handle);
            curl_close($this->handle);

        } catch (\Exception $ex) {
            throw $ex;
        }
        
        return $result;
    }
   
    public function getPerformance($mailingId) {

        try {

            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'stats';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['externalid'] = $mailingId;
            $data['type'] = 'xml';
            $data['minsendsize'] = 50;
            $data['minuniqueopens'] = 0;

            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
            $xml_doc = \simplexml_load_string($result);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }            
        } catch (Exception $ex) {
            throw $ex;
        }
        return $xml_doc->data;
    }

    public function GetListSubscribers($recipientId) {

        try {

            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'stats';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['type'] = 'custom';
            $data['name'] = 'GetListSubscribers';
            $data['listid'] = $recipientId;
            
            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
            $xml_doc = \simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }            
        } catch (Exception $ex) {
            throw $ex;
        }
        return $xml_doc->data->row;
    }
    
    public function AddSubscriberToList($email, $listid, $url, $customfields = array()) {

    $xmlrequest = new SimpleXMLElement("<xmlrequest />");
    $xmlrequest->addChild('username', $this->username);
    $xmlrequest->addChild('usertoken', $this->token);
    $xmlrequest->addChild('requesttype', 'subscribers');
    $xmlrequest->addChild('requestmethod', 'AddSubscriberToList');
    $xmlrequest->addChild('details');
    $xmlrequest->details->addChild('emailaddress', $email);
    $xmlrequest->details->addChild('mailinglist', $listid);
    $xmlrequest->details->addChild('format', 'html');
    $xmlrequest->details->addChild('confirmed', '1');
    	if(!empty($customfields) && is_array($customfields)) {
		$xmlrequest->details->addChild('customfields');
		foreach($customfields as $fieldid => $fieldvalue) {
			$lastItem = $xmlrequest->details->customfields->addChild('item');
			$lastItem->addChild('fieldid', $fieldid);
			$lastItem->addChild('value', $fieldvalue);
		}
	}
    // Send Request to API
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlrequest->asXML());
    $result = curl_exec($ch);
    curl_close($ch);
 
    return $result;
}
    
    function ActivateSubscriber($email, $listid, $url) {
    $xmlrequest = new SimpleXMLElement("<xmlrequest />");
    $xmlrequest->addChild('username', $this->username);
    $xmlrequest->addChild('usertoken', $this->token);
    $xmlrequest->addChild('requesttype', 'subscribers');
    $xmlrequest->addChild('requestmethod', 'ActivateSubscriber');
    $xmlrequest->addChild('details');
    $xmlrequest->details->addChild('emailaddress', $email);
    $xmlrequest->details->addChild('listid', $listid);
    // Send Request to API
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlrequest->asXML());
    $result = curl_exec($ch);
    curl_close($ch);
     return $result;
}
    public function GetCountListSubscribers($recipientId) {

        try {

            $this->setOpt(
                    $this->url
            );

            $data['module'] = 'stats';
            $data['username'] = $this->username;
            $data['token'] = $this->token;
            $data['type'] = 'custom';
            $data['activeonly'] = '1';
            $data['name'] = 'GetListSubscriberCount';
            $data['listid'] = $recipientId;
            
            foreach ($data as $key => $value) {
                $items[] = $key . '=' . $value;
            }

            $post_string = implode('&', $items);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $post_string);

            $result = curl_exec($this->handle);
            curl_close($this->handle);
            
            $xml_doc = \simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);

              if($xml_doc->success == 0){
                throw new \Exception($xml_doc->data);
               }            
        } catch (Exception $ex) {
            throw $ex;
        }
        return $xml_doc->data->row->Count;
        
    }
   
    private function setOpt($url) {

        $this->handle = curl_init($url);

        curl_setopt($this->handle, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($this->handle, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->handle, CURLOPT_SSL_VERIFYPEER, false);
        #curl_setopt($this->handle, CURLOPT_FOLLOWLOCATION, 1);
    }

}
