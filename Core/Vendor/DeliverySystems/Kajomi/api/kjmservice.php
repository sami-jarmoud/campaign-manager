<?php

require_once 'classes.php';

/**
 * 
 * basic interface to kajomi api
 * @author winand appelhoff
 * @code
 * 		// example usage
 * 		$http=new httpconnector('test','secret'); 
 *		$http->connect("api.kajomimail.de/srv",0);	
 *		$client=new kajomiclient($http);
 *		try
 *		{
 *			$res=$client->getRules();
 *		}
 *		catch (wexception $e) 
 *		{
 *			echo $e->message . $e->src;
 *		}	
 * @endcode
 */
class kajomiclient
{
	private $con;
	
	/**
	 * 
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}
	
	/**
	 * Get stored User Lists
	 * 
	 * @return 
	 * Array of list
	 */
        public function getLists()
        {
            $cc = call::create("api", "basic/kjmservice/getLists");
            return $this->con->invoke($cc);
        }

        /**
         * 
         * Get stored rules
         * 
         * @return 
         * Array of rule
         */
        public function getRules()
        {
            $cc = call::create("api", "basic/kjmservice/getRules");
            return $this->con->invoke($cc);
        }
        	
        /**
         * 
         * Duplicates existing list
         * 
         * @param list $l
         * existing list obtained through 
         * @see kajomiclient::getLists()
         * @param string $title 
         * new name of duplicated list
         */
        public function duplicateList($l,$title)
        {
            $cc = call::create("api", "basic/kjmservice/duplicateList")->addParam("l", $l)->addParam("title", $title);
            return $this->con->invoke($cc);
        }
        /**
         * 
         * returns users 
         * @param int $start 
         * starting point
         * @param int $end 
         * number of entries
         * @param list $l
         * optional: select users from list $l only
         * @return 
         * users as table class
         */
        public function exportUsers($start,$end,$l=NULL)
        {
            $cc = call::create("api", "basic/kjmservice/exportUsers")->addParam("start", $start)->addParam("end", $end)->addParam("l", $l);
            return $this->con->invoke($cc);
        }
		/**
		 * 
		 * imports list of users into list $l
		 * @param table $t 
		 * table containing user data
		 * @param list $l 
		 * user list 
		 * @see kajomiclient::getLists()
		 * @return 
		 * table containing users failed to import
		 */        	          
        public function importUsers(&$t,$l)
        {
            $cc = call::create("api", "basic/kjmservice/importUsers")->addParam("t", $t)->addParam("l", $l);
            return $this->con->invoke($cc);
        }
        /**
         * 
         * removes users
         * @param table $t 
         * table containing user data
         */
        public function removeUsers($t)
        {
            $cc = call::create("api", "basic/kjmservice/removeUsers")->addParam("t", $t);
            return $this->con->invoke($cc);
        }

        /**
         * 
         * updates users
         * @param table $t 
         * table containing user data
         */
        public function updateUsers($t)
        {
            $cc = call::create("api", "basic/kjmservice/updateUsers")->addParam("t", $t);
            return $this->con->invoke($cc);
        }        
        
        /**
         * 
         * queries users
         * @param string $field 
         * field name. e.g. email
         * @param string $value
         * value of field
         * @return
         * table containing users
         */
        public function queryUsers($field,$value,$l)
        {
            $cc = call::create("api", "basic/kjmservice/queryUsers")->addParam("field", $field)->addParam("value", $value)->addParam("l", $l);
            return $this->con->invoke($cc);
        }        
		/**
		 * 
		 * get list of blacklisted users/domains
		 * @return 
		 * array of blacklist
		 */                        
        public  function getBlacklist()
        {
            $cc = call::create("api", "basic/kjmservice/getBlacklist");
            return $this->con->invoke($cc);        	
        }
		/**
		 * 
		 * imports blacklisted users/domains
		 * @param blacklist &$_b 
		 * array of blacklist
		 */        	      
        public  function importBlacklist(&$_b)
        {
            $cc = call::create("api", "basic/kjmservice/importBlacklist")->addParam("_b",$_b);
            return $this->con->invoke($cc);        	
        }
        /**
         * 
         * removes blacklisted users/domains
         * @param blacklist $_b 
         * array of blacklist
         * @see kajomiclient::getBlacklist()
         */
        public  function removeBlacklist(&$_b)
        {
            $cc = call::create("api", "basic/kjmservice/removeBlacklist")->addParam("_b",$_b);
            return $this->con->invoke($cc);        	
        }	   
        /**
         * 
         * Enter description here ...
         * @param message $m
         * message containing data
         * @see message
         * @param lists $l
         * list of recepients
         * @see lists
         * @param selection $s
         * selection : null empty
         * @see selection
         * @param array $_pos
         * array of rules to apply to list
         * @see rule
         * @param $_exclude
         * array of queues to exclude from receiving message 
         * @param  string $datetime
         * datetime string to start delivering message
         * @return
         * array of queue[] objects containing pending messages
         */
        
        public function send($m,$l,$s,$_pos,$_exclude,$datetime)
        {
        	$cc=call::create("api","basic/kjmservice/send")->addParam("m",$m)->addParam("l", $l)->addParam("s", $s)->addParam("_pos", $_pos)->addParam("_exclude",$_exclude)->addParam("datetime",$datetime);
        	return $this->con->invoke($cc);
        }

        /**
         * 
         * relays message; simple mail sender
         * @param string $sender
         * @param string $recepient
         * @param message $m
         * @see message
         */
        public function relay($sender,$recepient,$m,$_attach=NULL)
        {
        	$cc=call::create("api","basic/mailrelay/relay")->addParam("sender",$sender)->addParam("_m", array(new mailaddress($recepient,"TO")))->addParam("m", $m);
        	if($_attach!=NULL)
        		$cc->addParam("_attach", $_attach);
        	return $this->con->invoke($cc);
        }        
        
        /**
         * 
         * relays message; simple mail sender
         * @param string $sender
         * @param mailaddress $_m
         * mailaddress[] list
         * @param message $m
         * @see message
         */
        public function relayMulti($sender,$_m,$m,$_attach=NULL)
        {
        	$cc=call::create("api","basic/mailrelay/relay")->addParam("sender",$sender)->addParam("_m", $_m)->addParam("m", $m);
        	if($_attach!=NULL)
        	{
        		$cc->addParam("_attach", $_attach);
        	}
        	return $this->con->invoke($cc);
        }          
        
        /**
         * 
         * retrieves queues
         * pending=true : queues not processed
         * @param boolean $pending
         * @return
         * array of queue[] objects
         */
        public function getQueues($pending)
        {
        	$cc=call::create("api","basic/kjmservice/getQueues")->addParam("pending", $pending);
        	return $this->con->invoke($cc);
        }
        /**
         * 
         * retrieves specific queue
         * queuenum
         * @param int $queuenum
         * @return
         * queue object
         */
        public function getQueue($queuenum)
        {
        	$cc=call::create("api","basic/kjmservice/getQueue")->addParam("queuenum", $queuenum);
        	return $this->con->invoke($cc);
        }        
        /**
         * 
         * cancels pending queue 
         * @param $q
         * pending queue
         * @see kajomiclient::getQueues(true)
         */
        public function cancelQueue($q)
        {
        	$cc=call::create("api","basic/kjmservice/cancelQueue")->addParam("q", $q);
        	return $this->con->invoke($cc);        	
        }
        /**
         * 
         * sets active queue status (active/inactive) 
         * @param $q
         * queue
         * @see kajomiclient::getQueues(true)
         */
        public function setQueueStatus($q,$bactive)
        {
        	$cc=call::create("api","basic/kjmservice/setQueueStatus")->addParam("q", $q)->addParam("bactive", $bactive);
        	return $this->con->invoke($cc);        	
        }        
        /**
         * 
         * checks html data for parser errors
         * @param string $data 
         * html code as UTF8 string
         * @return 
         * string[] containing errors/warnings
         */
        public  function checkHTML(&$data)
        {
            $cc = call::create("api", "basic/kjmservice/checkHTML")->addParam("data",mb_convert_encoding($data, 'UTF8'));
            return $this->con->invoke($cc);        	
        }	    
        
        /**
         * 
         * checks html data for parser errors
         * @param string $data 
         * html code as UTF8 string
         * @return 
         * string[] containing errors/warnings
         */
        public  function fixHTML(&$data)
        {
            $cc = call::create("api", "basic/kjmservice/fixHTML")->addParam("html",$data);
            return $this->con->invoke($cc);        	
        }        

        /**
         * 
         * tries to fix html documents created with word
         * @param string $data 
         * html code as UTF8 string
         * @return 
         * array of parameter containing all <a href links
         */
        public function getHrefTags(&$data)
        {
            $cc = call::create("api", "basic/kjmservice/getHrefTags")->addParam("html",mb_convert_encoding($data, 'UTF8'));
            return $this->con->invoke($cc);        	
        }        
        
        /**
         * 
         * replaces all <a href links
         * @param string $data
         * html code as UTF8 string 
         * @param parameter[] $_urls
         * array of parameter with links to replace ... send NULL to replace all links
         */
        public function replaceHREF(&$data,$_urls)
        {
            $cc = call::create("api", "basic/kjmservice/replaceHREF")->addParam("html",mb_convert_encoding($data, 'UTF8'))->addParam("_urls", $_urls);
            return $this->con->invoke($cc);        	
        }         
}
