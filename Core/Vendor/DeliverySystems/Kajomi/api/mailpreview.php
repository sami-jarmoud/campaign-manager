<?php
require_once 'classes.php';


class mailpreview
{
	private $con;
	
	/**
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}
	/**
	 * 
	 * retrieves available clients (mail targets)
	 */
	public function getClients()
	{
		$cc = call::create("api", "basic/mailpreview/getClients");
		return $this->con->invoke($cc);
	}
	/**
	 * 
	 * retrieves available Previews matching [$msgid]
	 */
	public function getPreviews($msgid)
	{
		$cc = call::create("api", "basic/mailpreview/getPreviews")->addParam("msgid", $msgid);
		return $this->con->invoke($cc);
	}	
	/**
	 * 
	 * retrieves preview images as array of base64 strings
	 * @param string $jobid
	 * @param string $_client
	 * @param string $thumbs
	 */
	public function getBase64Previews($jobid,$_client,$thumbs)
	{
		$cc = call::create("api", "basic/mailpreview/getBase64Previews")->addParam("jobid", $jobid)->addParam("_client", $_client)->addParam("thumbs", $thumbs);
		return $this->con->invoke($cc);
	}	
	/**
	 * 
	 * creates preview for message m
	 * @param message $m
	 */
	public function createPreview($m)
	{
		$cc = call::create("api", "basic/mailpreview/createPreview")->addParam("m", $m);
		return $this->con->invoke($cc);		
	}
	/**
	 * 
	 * creates link for preview image
	 * @param string $storagekey
	 * @param string $type
	 * jpeg or gif
	 * @param int $n
	 * client code fpr jpeg: @see getClients
	 * @return
	 * string with url to image
	 */
	public function resolve($storagekey,$type,$client) 
	{
		$cc=call::create("api","basic/mailpreview/resolve")->addParam("storagekey", $storagekey)->addParam("type", $type)->addParam("n", $client);
		return $this->con->invoke($cc);
	}	
}
	
?>