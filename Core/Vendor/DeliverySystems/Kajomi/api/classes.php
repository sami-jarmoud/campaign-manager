<?php

class parameter
{
	public $n,$v;
	
	public function __construct($name,$value) 	{ $this->n=$name;$this->v=$value; }
	
	public static function fromArray($_arr)
	{
		$ret=array();
		foreach($_arr as $k=>$v)
			array_push($ret, new parameter($k, $v));

		return $ret;
	} 
	
	public static  function toArray($_p)
	{
		$ret=array();
		foreach($_p as $p)
			$ret[$p->n] =$p->v;
		return $ret;	
	}
}


class lists
{
	/**
	 * 
	 * List number
	 * @var int $listnum
	 */
	public $listnum;
	/**
	 * 
	 * List title
	 * @var string $title
	 */
	public $title;
	public $sende;
	public $sendn;
}

/**
 * 
 * blacklist
 */
class blacklist
{
	/**
	 * 
	 * @var int $list
	 * listnum of list
	 * @see lists
	 * 
	 */
    public $list;
    /**
     * 
     * e-mail address of user or blacklisted base domain name
     * @var string $email
     */
    public $email;
}
/**
 * 
 * message : mail message description
 */
class message
{
	/**
	 * @var string $sendername
	 * displayed sendername in mail message
	 */	
	public $sendername = "";
	/**
	 * 	 
	 * @var string $senderemail
	 * currently reserved for future use
	 */
    public $senderemail = "";

    /**
     * @var string $description
     * description
     */
    public $description = "";
    	
    /**
     * @var string $subject
     * mail subject
     */
    public $subject = "";
    /**
     * @var string $content
     * text body of mail
     */    
    public $content = "";
    /**
     * @var string $htcontent
     * html body of mail
     */
    public $htcontent = "";
    public $fattach = "";
    
    public $replyto="";
    
    public $_custom_header=array();
}

class mailaddress
{
	public $type;
	public $address;
	
	public function __construct($a,$t="TO")
	{
		$this->address=$a;
		$this->type=$t;
	}
}

/**
 * 
 * attachment
 *
 */
class attachment
{
	public $storagekey;
	public $name;
	public $mime_type; 
	
	public function __construct($key,$n,$mime) 
	{
		$this->storagekey=$key;
		$this->name=$n;
		$this->mime_type=$mime;
	}
}

/**
 * 
 * selection
 */
class selection
{
    /**
     * @var int $msex
     * recepient sex
     * 0: undefined
     * 1: male
     * 2: female
     */
    public $msex=0;
    /**
     * @var int $mlimit
     * selection amount limit
     */    
    public $mlimit=1000000;
    /**
     * @var string morder
     * sort order
     * asc : ascending
     * desc : descending
     * empty field : random
     */    
    public $morder="";
}

/**
 * 
 * queue
 */
class queue
{
    /**
     * @var int $id
     * queue id
     */	
	public $id;
    /**
     * @var int $queuenum
     * currently assigned queue number
     * 0 : queue is still unprocessed
     *  > 0 : queue already processed
     */	
    public $queuenum;
    /**
     * @var message $m
     * mail message
     * @see message
     */    
    public $m=NULL;
    /**
     * @var lists $l
     * queue selection list
     * @see lists
     */    
    public $l;
}

class category
{
	public $id=0;
	public $value="";
	public $ident="";
	public $isleaf=1;
	
	public $_meta=array();
	
	public function  __construct($i){
		$this->id=$i;
	}
}

class link
{
	public $id=0;
	public $url="";
	public $description="";
	
	public function __construct($i=0){
		$this->id=intval($i);
	}
}

class table
{
	/**
	 * 
	 * array of string containing column names
	 * @var array $_columns
	 */
	public $_columns=array();
	/**
	 * 
	 * array of array of string containing individual rows
	 * @var array $_rows
	 */
	public $_rows=array();
	
	/**
	 * 
	 * adds column; row data remains unchanged 
	 * @param string $value
	 * columname
	 */
	public function addColumn($value){ array_push($this->_columns,$value); }
	/**
	 * 
	 * adds row to end of _rows
	 * @param array $r
	 * array of string containing row data
	 */
	public function addRow(array $r){ array_push($this->_rows,$r); }
	
	public function getColumnIndex($name)
	{
		for ($i=0;$i<count($this->_columns);++$i )
			if($this->_columns[$i]==$name)
				return $i;
		return -1;
	}
	
	public static function fromObj($o)
	{
		$ret=new self($o);
		$ret->_columns=$o->_columns;
		$ret->_rows=$o->_rows;
		$ret->name=$o->name;		
		return $ret;
	}
	
	/**
	 * 
	 * removes column and rowentries 
	 * @param string $name
	 * columnname
	 */
	public function removeColumn($name)
	{
		$n=$this->getColumnIndex($name);
		if($n==-1)return ;
		unset($this->_columns[$n]);
		foreach($this->_rows as &$v)
			unset($v[$n]);
	}
	
	/**
	 * 
	 * writes table $tab to CSV file
	 * @param table $tab
	 * @param string $fname
	 * filename
	 */
	public static function exportCSV($tab,$fname)
	{
		$fp=fopen($fname,'w+');
		fputcsv($fp, $tab->_columns,';','"');
		foreach ($tab->_rows as $f)
			fputcsv($fp, $f,';','"');			
	}
	
	/**
	 * 
	 * reads table from csv
	 * @param string $fname
	 * filename
	 * @return table $t
	 * new table containing data
	 */
	public static function importCSV($fname,$seperator=';')
	{
		$fp=fopen($fname, 'r');
		$ret=new table();
		$ret->_columns=fgetcsv($fp,1000,$seperator,'"');
		while( ($dat=fgetcsv($fp,1000,$seperator,'"'))!== FALSE)
		{
			foreach ($dat as &$val)
				$val=mb_convert_encoding($val,'UTF8');
			array_push($ret->_rows, $dat);
		}
		return $ret;
	}
	
	public static function toObject($t)
	{
		$_ret=array();

		foreach($t->_rows as $r)
		{
			$ret=array();
			for($i=0;$i<count($t->_columns);++$i){
				$ret[$t->_columns[$i]]=$r[$i];
			}
			array_push($_ret, $ret);
		}
		return $_ret;
	}
	
	public function map(array $mapping)
	{
		$rem=array();
		foreach($this->_columns as $c)
		{
			if(!isset($mapping[$c]))
				array_push($rem,$c);
		}	
		
		foreach ($rem as $r)
		{
			$idx=$this->getColumnIndex($r);
			if($idx!=-1)
			{
				unset($this->_columns[$idx]);
				for($i=0;$i<count($this->_rows);++$i)
				{
					unset($this->_rows[$i][$idx]);
				}					
			}
			else{
				$err=1;
			}
		}		
		
		foreach($mapping as $k=>$v)
		{
			$idx=$this->getColumnIndex($k);
			if($idx!=-1)
				$this->columns[$idx]=$v;
		}
	}
}