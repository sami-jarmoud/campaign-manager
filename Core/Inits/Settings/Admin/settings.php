<?php
$repositoryClassDataArray['admin'] = '\\Modules\\Admin\\Domain\\Repository\\AdminRepository';

$moduleSettingsDataArray['admin'] = array(
	'repositorySettings' => array(
		'mandant' => array(
			'mandantTable' => 'mandant_parameters',
			'mandantPrimaryField' => 'id',
			'mandantFetchClass' => '\\Modules\\Admin\\Domain\\Model\\AdminMandantEntity'
			),
		'clickProfiles' => array(
			'clickProfilesTable' => 'click_profiles',
			'clickProfilesPrimaryField' => 'id',
			'clickProfilesFetchClass' => '\\Packages\\Core\\Domain\Model\\ClickProfileEntity'
			),
		'clientClickProfiles' => array(
			'clientClickProfilesTable' => 'client_click_profiles',
			'clientClickProfilesPrimaryField' => 'uid',
			'clientClickProfilesFetchClass' => '\\Packages\\Core\\Domain\\Model\\ClientClickProfileEntity'
			),
		'deliverySystemDistributor' => array(
			'deliverySystemDistributorTable' => 'delivery_system_distributor',
			'deliverySystemDistributorPrimaryField' => 'id',
			'deliverySystemDistributorFetchClass' => '\\Packages\\Core\\Domain\\Model\\DeliverySystemDistributorEntity'
			),
		'recipients' => array(
			'recipientsTable' => 'recipients',
			'recipientsPrimaryField' => 'r_id',
			'recipientsFetchClass' => '\\Modules\\Admin\\Domain\Model\\AdminRecipientsEntitiy'
			),		
		'log' => array(
			'logTable' => 'log',
			'logPrimaryField' => 'logid',
			'logFetchClass' => '\\Modules\\Campaign\\Domain\\Model\\LogEntity'
		),
	),
   'moduleSettings' => array()
);