<?php
$repositoryClassDataArray['statistic'] = '\\Modules\\Statistic\\Domain\\Repository\\StatistikAuswertungRepository';

$moduleSettingsDataArray['statistic'] = array(
	'repositorySettings' => array(
		'auswertung' => array(
			'table' => 'statistik_auswertung',
			'primaryField' => 'id',
			'fetchClass' => '\\Modules\\Statistic\\Domain\Model\\StatistikAuswertungEntity'
			),
		'log' => array(
			'logTable' => 'log',
			'logPrimaryField' => 'logid',
			'logFetchClass' => '\\Modules\\Campaign\\Domain\\Model\\LogEntity'
		),
	),
   'moduleSettings' => array()
);