<?php
/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */

$repositoryClassDataArray = array();
$moduleSettingsDataArray = array();

// DbSettings
$clientDbHost = 'localhost';

// CampaignSettings
$cmVorgabeO = 10; // Default Globale Vorgabe �ffnungsrate
$cmVorgabeK = 1; // Default Globale  Vorgabe Klickrate
$cmVorgabeUsq = 0; // Default Globale Vorgabe �bersendungsquote
$bounceSThreshold = 1;
$bounceHThreshold = 0.5;

// Default MailSettings
$smtpPort = 25;
$smtpEncryption = null;
$cmSenderPrefix = 'news';

$clientConfigFile = \DIR_Inits . 'Connections' . \DIRECTORY_SEPARATOR . 'Clients' . \DIRECTORY_SEPARATOR . $clientEntity->getAbkz() . '.php';
if (\Packages\Core\Utility\FileUtility::isReadable($clientConfigFile)) {
	require($clientConfigFile);
}

$clientConnectionHandle = new \Packages\Core\Manager\Database\PdoConnection();
$clientConnectionHandle->setHost($clientDbHost);
$clientConnectionHandle->setDbName($clientDbName);
$clientConnectionHandle->setDbUser($clientDbUser);
$clientConnectionHandle->setDbPassword($clientDbPassword);
$clientConnectionHandle->init();

$clientSettingsDataArray = array(
	'mailSettings' => array(
		'smtpHost' => $smtpHost,
		'smtpUser' => $smtpUser,
		'smtpPassword' => $smtpPassword,
		'smtpPort' => $smtpPort,
		'smtpEncryption' => $smtpEncryption,
		'serverName' => $serverName,
		'fromEmail' => $fromEmail,
		'fromName' => $fromName,
		'cmSenderPrefix' => $cmSenderPrefix
	)
);