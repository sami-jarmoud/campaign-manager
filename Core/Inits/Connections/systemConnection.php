<?php
$systemDbHost = 'localhost';

if (\Packages\Core\Utility\GeneralUtility::isProdSystem()) {
	// isProdSystem
	$systemDbName = 'peppmt';
	$systemDbUser = 'dbo49323410';
	$systemDbPassword = '5C4i7E9v';
} elseif (\Packages\Core\Utility\GeneralUtility::isTestSystem()) {
	// isTestSystem
	$systemDbName = 'test-maas-db';
	$systemDbUser = 't-maas-user-db';
	$systemDbPassword = 'Q9h3D8a3';
} elseif (\Packages\Core\Utility\GeneralUtility::isDevSystem()) {
	// isDevSystem
	$systemDbName = 'dev-maas-db';
	$systemDbUser = 'd-maas-user-db';
	$systemDbPassword = '7E8t7N9r';
} else {
	throw new \InvalidArgumentException('invalid EnvironmentType', 1436356168);
}

$systemConnectionHandle = new \Packages\Core\Manager\Database\PdoConnection();
$systemConnectionHandle->setHost($systemDbHost);
$systemConnectionHandle->setDbName($systemDbName);
$systemConnectionHandle->setDbUser($systemDbUser);
$systemConnectionHandle->setDbPassword($systemDbPassword);
$systemConnectionHandle->init();