<?php
/* @var $clientConnectionHandle \Packages\Core\Manager\Database\AbstractConnection */

$statistikRepository = new \Modules\Statistic\Domain\Repository\StatistikAuswertungRepository();
$statistikRepository->setDatabaseHandle($clientConnectionHandle);
$statistikRepository->setTable('statistik_auswertung');
$statistikRepository->setPrimaryField('id');
$statistikRepository->setFetchClass('\Modules\Statistic\Domain\Model\StatistikAuswertungEntity');
$statistikRepository->init();
?>