<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\InterfaceAuthentication;


/**
 * Description of AbstractAuthentication
 *
 * @author Cristian.Reus
 */
abstract class AbstractAuthentication implements InterfaceAuthentication {
	
	/**
	 * clientId
	 * 
	 * @var mixed
	 */
	protected $clientId;
	
	/**
	 * username
	 * 
	 * @var string
	 */
	protected $username;
	
	/**
	 * password
	 * 
	 * @var string
	 */
	protected $password;
	
	/**
	 * sharedKey
	 * 
	 * @var mixed
	 */
	protected $sharedKey;
	
	/**
	 * secretKey
	 * 
	 * @var string
	 */
	protected $secretKey;
	
	/**
	 * authentification
	 * 
	 * @var mixed
	 */
	protected $authentification = NULL;
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setClientId($clientId) {
		$this->clientId = $clientId;
	}

	public function setUsername($username) {
		$this->username = $username;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function setSharedKey($sharedKey) {
		$this->sharedKey = $sharedKey;
	}

	public function setSecretKey($secretKey) {
		$this->secretKey = $secretKey;
	}
	
	public function getAuthentification() {
		return $this->authentification;
	}

}
