<?php
namespace Packages\Api\DeliverySystem\Authentication;

/**
 * Description of InterfaceAuthentication
 *
 * @author Cristian.Reus
 */
interface InterfaceAuthentication {
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login();
	
	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout();
}
