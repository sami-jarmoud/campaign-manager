<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\AbstractAuthentication;

/**
 * Description of Broadmail
 *
 * @author Cristian.Reus
 */
class Broadmail extends AbstractAuthentication {
	
	/**
	 * client
	 * 
	 * @var \SoapClient|NULL
	 */
	protected $client = NULL;
	
	
	
	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		try {
			$this->client = new \SoapClient('http://api.broadmail.de/soap11/RpcSession?wsdl');
			
			$this->authentification = $this->client->login(
				$this->clientId,
				$this->username,
				$this->password
			);
		} catch (\Exception $e) {
			// TODO: log message
			throw $e;
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		$this->authentification = $this->client->logout($this->authentification);
		$this->client = NULL;
	}

}