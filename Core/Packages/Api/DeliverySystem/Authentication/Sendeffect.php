<?php
namespace Packages\Api\DeliverySystem\Authentication;

use Packages\Api\DeliverySystem\Authentication\AbstractAuthentication;

/**
 * Description of Sendeffect
 * 
 * @author Sami.Jarmoud
 */
class Sendeffect extends AbstractAuthentication{
    
 	/**
	 * login
	 * 
	 * @return void
	 */
	public function login(){
            if (\is_null($this->authentification)) {
                try {
                    $this->authentification = array(
                        'username' => $this->username,
                        'token' => $this->password,
                        'clientId' => $this->clientId
                    );
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
        }
	
	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout(){
            $this->authentification = NULL;
        }  
}

