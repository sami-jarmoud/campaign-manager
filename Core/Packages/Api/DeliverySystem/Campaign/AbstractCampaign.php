<?php
namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\AbstractDeliverySystem;
use Packages\Api\DeliverySystem\Campaign\InterfaceCampaign;

/**
 * Description of AbstractCampaign
 *
 * @author Cristian.Reus
 */
abstract class AbstractCampaign extends AbstractDeliverySystem implements InterfaceCampaign {
	
	/**
	 * mailingStatusDataArray
	 * 
	 * @var array
	 */
	public static $mailingStatusDataArray = array(
		'NEW' => 'NEW',
		'ALL' => 'ALL'
	);
	
	/**
	 * mailingReadPropertiesMapping
	 * 
	 * @var array
	 */
	protected $mailingReadPropertiesMapping = array();
	
	/**
	 * sortingList
	 * 
	 * @var string
	 */
	protected $sortingList = 'asc';
	
	/**
	 * charset
	 * 
	 * @var string
	 */
	protected $charset = '';
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setSortingList($sortingList) {
		$this->sortingList = $sortingList;
	}
	
	public function setCharset($charset) {
		$this->charset = $charset;
	}

}