<?php

namespace Packages\Api\DeliverySystem\Campaign;

use Packages\Api\DeliverySystem\Campaign\AbstractCampaign;
use Packages\Api\DeliverySystem\Domain\Entity\MailingListEntity;
use Packages\Api\DeliverySystem\Domain\Entity\MailingEntity;
use Packages\Core\Persistence\ObjectStorage;
use Packages\Core\Utility\EncodingUtility;

/**
 * Description of Sendeffect
 * 
 * @author Sami.Jamroud
 */
Class Sendeffect extends AbstractCampaign {

    /**
     * sendeffectClient
     * 
     * @var \sendeffectClient|NULL
     */
    protected $sendeffectClient = NULL;

    /**
     * updateMailingPropertiesMapping
     * 
     * @var array
     */
    protected $updateMailingPropertiesMapping = array(
        'title' => 'name',
        'subject' => 'subject',
        'htmlContent' => 'html',
        'txtContent' => 'text',
    );
    /**
     *  verteilerIdsStellar
     * 
     * @var array 
     */
    protected $verteilerIdsStellar = array(    
            'se-de' => '4',
            'se-at' => '5',
            'se-ch' => '8',
            'se-p' => '7',
            'se-neude' => '25'  
    );
    /**
     *  verteilerIds4wave
     * 
     * @var array 
     */
    protected $verteilerIds4wave = array(    
            'se-de' => '1176',
            'se-at' => '1177',
            'se-ch' => '1178',
            'se-p' => '1179'
    );
    /**
     * getList
     * 
     * @param string $status
     * @param integer $limit
     * @param integer $offset
     * @return \Packages\Core\Persistence\ObjectStorage
     */
    public function getList($status, $limit = 20, $offset = 0) {
        
    }

    /**
     * create
     * 
     * @param MailingEntity $mailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return integer campaignId
     * @throws \Exception
     */
    public function create(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
        try {
            $this->initApiClient();
            switch ($campaignEntity->getCharset()) {
                case 'ISO-8859-1':
                    $htmlContent = EncodingUtility::encodeToUtf8($mailingEntity->getHtmlContent());
                    break;

                case 'UTF-8':
                    $htmlContent = $mailingEntity->getHtmlContent();
                    break;
                default :
                    $htmlContent = $mailingEntity->getHtmlContent();
            } 
            $resultXML = $this->sendeffectClient->create(
                    $mailingEntity->getSubject(), 
                    $campaignEntity->getK_id().'-'.$mailingEntity->getTitle(), 
                    $htmlContent,
                    $mailingEntity->getTxtContent(),
                    '',
                    $this->getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id())
            );
            
            $mailingId = $resultXML->externalid;
            #if(\strlen($mailingId) > 0){
                #$result = $this->sendeffectClient->send(
                        #$mailingId,
                        #$mailingEntity->getScheduleDate()->format('Y-m-d H:i:s'),
                        #'', 
                        #$mailingEntity->getRecipientListIds(), 
                        #$mailingEntity->getFromEmail(), 
                       # $mailingEntity->getFromName(),
                       # $mailingEntity->getMaxRecipients,
                       # TRUE
                     #);
            #}
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $mailingId;
    }

    /**
     * read
     * 
     * @param integer $mailingId
     * @param boolean $getContent
     * @return \Packages\Api\DeliverySystem\Domain\Entity\MailingEntity
     */
    public function read($mailingId, $getContent = false) {
        
    }

    /**
     * update
     * 
     * @param MailingEntity $mailingEntity
     * @param MailingEntity $newMailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @return boolean
     */
    public function update(MailingEntity $mailingEntity, MailingEntity $newMailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
        try {
            #if ($mailingEntity->_isNew()) {
               # throw new \InvalidArgumentException('The domainEntity "(' . \get_class($mailingEntity) . ')" is new.', 1446713194);
           # } 

            $this->processUpdateMailingWebserviceData(
                    $this->updateMailingPropertiesMapping,
                    $mailingEntity->getUidForSE(), 
                    $newMailingEntity,
                    $this->getShortcutClickProfiles($campaignEntity->getCampaign_click_profiles_id()),
                    $campaignEntity->getCharset(),
                    $campaignEntity->getK_id()
              );
            
            if($campaignEntity->getUse_campaign_start_date() === TRUE
                               ){
                 if(\strlen($campaignEntity->getExternal_job_id()) > 1
                         ){
                     
                 //loeschen job id
                $result = $this->sendeffectClient->actionJob(
                                $campaignEntity->getExternal_job_id(), 
                               'pausejob'
                            );      
                $result = $this->sendeffectClient->actionJob(
                                $campaignEntity->getExternal_job_id(), 
                               'deletejob'
                            );
                         }
                            $newMailingEntity->setUseScheduleDate(TRUE);
                //job starten
                $reslut = $this->send(
                        $newMailingEntity,
                        $campaignEntity
                        );
            }
            
            if($campaignEntity->getUse_campaign_start_date() === FALSE
                          && (\strlen($campaignEntity->getExternal_job_id()) > 1)
                               ){
                 //loeschen job id
                $result = $this->sendeffectClient->actionJob(
                                $campaignEntity->getExternal_job_id(), 
                               'pausejob'
                            );   
                $result = $this->sendeffectClient->actionJob(
                                $campaignEntity->getExternal_job_id(), 
                               'deletejob'
                            );    
                $campaignEntity->setExternal_job_id(' ');
              }
              
            $result = TRUE;
        } catch (\Exception $e) {
            throw $e;
        }

        return $result;
    }

    /**
     * delete
     * 
     * @param integer $mailingId mailingId
     * @return mixed|boolean
     */
    public function delete($mailingId) {
        $result = FALSE;
		try {
			if (\strlen($mailingId) <= 0) {
				throw new \InvalidArgumentException('invalid mailingId', 1446813112);
			}			
                         $this->initApiClient();
                          //loeschen job id
                $result = $this->sendeffectClient->actionJob(
                                $mailingId, 
                               'pausejob'
                            );   
                $result = $this->sendeffectClient->actionJob(
                                $mailingId, 
                               'deletejob'
                            );    
                         $this->sendeffectClient->delete(
                                 $mailingId
                                 );
                     $result = TRUE;    
		} catch (\Exception $e) {
			throw new \Exception(__METHOD__ . ': ' . $mailingId, 1454485609, $e);
		}
		
		return $result;
        
    }
    
    /**
     * send
     * 
     * @param MailingEntity $MailingEntity
     * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
     * @param boolean $updateAutoNv
     * @return boolean
     */
    public function send(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity , $sendermail, $replytomail) {
        try {
            #if ($mailingEntity->_isNew()) {
               # throw new \InvalidArgumentException('The domainEntity "(' . \get_class($mailingEntity) . ')" is new.', 1446713194);
           # }           
                if($campaignEntity->getUse_campaign_start_date() === TRUE
                               ){                  
                    $resultjob= $this->processScheduleMailing(
                                             $mailingEntity, 
                                             $campaignEntity,
                                             $sendermail, 
                                             $replytomail
                                              );
                    $externaljobid = \strval($resultjob);
                    $campaignEntity->setExternal_job_id($externaljobid);
                }

            $result = TRUE;
        } catch (\Exception $e) {
            throw $e;
        }

        return $result;
    }    

    /**
     * autoNvMailings
     * 
     * @param integer $oldMailingId mailingId
     * @return integer
     */
    public function autoNvMailings($oldMailingId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
        
    }

    /**
     * resetShippingDate
     * 
     * @param integer $mailingId
     * @return integer
     */
    public function resetShippingDate($mailingId) {
        
    }

    /**
     * sendTestMail
     * @param integer $mailingId
     * @param long $recipientListId
     * @param array $recipient
     * @return integer
     */
    public function sendTestMail($mailingId, $recipientListId, array $recipient) {
         $this->initApiClient();

         try{
             $resultTestMail['senden'] = $this->sendeffectClient->sendTestMail(
                     $mailingId, 
                     '', 
                     $recipientListId, 
                     '', 
                     $recipient['absendername'],
                     $recipient['email'],
                     $recipient['sendermail'],
                     $recipient['replytomail']
                     );
   
         } catch (\Exception $ex) {
                throw $ex;
         }
         
    }
    
        /**
     * getVerteilerZustand
     * @param type $clientId
     * @return array
     * @throws \Exception
     */
    public function getVerteilerZustand($clientId){
        $this->initApiClient();
        try{
           
            switch ($clientId) {
                case 6:
                    $verteilerIds = $this->verteilerIdsStellar;
                    break;
                case 7:
                     $verteilerIds = $this->verteilerIds4wave;
                    break;
                default:
                    $verteilerIds = $this->verteilerIdsStellar;
                    break;
            }
                foreach ($verteilerIds as $verteiler => $id){     
                    
                             $contactsCount[$verteiler] = (int)$this->sendeffectClient->GetCountListSubscribers(
                                                                $id
                                                          );
                               
                          }
            return $contactsCount;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    /**
     * initApiClient
     * @return void
     */
    protected function initApiClient() {
        if (\is_null($this->sendeffectClient)) {
            require_once(\DIR_Vendor . 'DeliverySystems' . \DIRECTORY_SEPARATOR . 'Sendeffect' . \DIRECTORY_SEPARATOR . 'api' . \DIRECTORY_SEPARATOR . 'seCurl.php');

            //create client instance
            $this->sendeffectClient = new \seCurl(
                    $this->authentication->getAuthentification()['username'], $this->authentication->getAuthentification()['token'], $this->authentication->getAuthentification()['clientId'], "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
            );
        }
    }

    /**
     * processUpdateMailingWebserviceData
     * 
     * @param array $processDataArray
     * @param type $mailingId
     * @param MailingEntity $mailingEntity
     * @return void
     */
    protected function processUpdateMailingWebserviceData(array $processDataArray, $mailingId, MailingEntity $mailingEntity, $aff, $charset, $campaignID) {
        foreach ($processDataArray as $key => $item) {
                $mailingDataArray[$item] = '';
            if (
                    (
                    \is_array($mailingEntity->_getProperty($key)) && \count($mailingEntity->_getProperty($key)) > 0
                    ) || (
                    !\is_array($mailingEntity->_getProperty($key)) && \strlen($mailingEntity->_getProperty($key)) > 0
                    )
            ) {
                           $mailingDataArray[$item] = $mailingEntity->_getProperty($key);
            }

        }
          if(\count($mailingDataArray) > 0){
              $this->initApiClient();
            switch ($charset) {
                case 'ISO-8859-1':
                    $htmlContent = EncodingUtility::encodeToUtf8($mailingDataArray['html']);
                    break;

                case 'UTF-8':
                    $htmlContent = $mailingDataArray['html'];
                    break;
                default :
                    $htmlContent = $mailingEntity->getHtmlContent();
            } 
              $this->sendeffectClient->update(
                      $mailingId, 
                      $htmlContent,
                      $mailingDataArray['text'], 
                      $mailingDataArray['subject'], 
                      $campaignID.'-'.$mailingDataArray['name'],
                     $aff
                      );
          }      
    }

    /**
     * getShortcutClickProfiles
     * 
     * @param integer $clickProfileID
     * @return string
     */
    protected function getShortcutClickProfiles($clickProfileID){
        
        if(\Packages\Core\Utility\GeneralUtility::isProdSystem()){
        switch (\intval($clickProfileID)) {
            case 49:
            $shortcut = 'wifa';
                break;
            
            case 50:
            $shortcut = 'limod';
                break;
            
            case 51:
            $shortcut = 'telko';
                break;
            
            case 52:
            $shortcut = 'techcar';
                break;
            
            case 53:
            $shortcut = 'reiwell';
                break;
            
            case 54:
            $shortcut = 'unthob';
                break;
            
            case 55:
            $shortcut = 'game';
                break;
            
            case 56:
            $shortcut = 'luxgou';
                break;
            
            case 57:
            $shortcut = 'tiere';
                break;
            
            case 58:
            $shortcut = 'sozi';
                break;
            
            case 59:
            $shortcut = 'date';
                break;
            
            case 60:
            $shortcut = 'sonstiges';
                break;
            
            case 62:
            $shortcut = 'wifa';
                break;
            
            case 63:
            $shortcut = 'limod';
                break;
            
            case 64:
            $shortcut = 'telko';
                break;
            
            case 65:
            $shortcut = 'techcar';
                break;
            
            case 66:
            $shortcut = 'reiwell';
                break;
            
            case 67:
            $shortcut = 'unthob';
                break;
            
            case 68:
            $shortcut = 'game';
                break;
            
            case 69:
            $shortcut = 'luxgou';
                break;
            
            case 70:
            $shortcut = 'tiere';
                break;
            
            case 71:
            $shortcut = 'sozi';
                break;
            
            case 72:
            $shortcut = 'date';
                break;
            
            case 73:
            $shortcut = 'sonstiges';
                break; 
            
            case 74:
            $shortcut = 'versi';
                break; 
            case 75:
            $shortcut = 'inge';
                break; 
            case 76:
            $shortcut = 'immo';
                break; 
            case 77:
            $shortcut = 'wipo';
                break; 
            case 78:
            $shortcut = 'trawell';
                break; 
            case 79:
            $shortcut = 'lotto';
                break; 
            case 80:
            $shortcut = 'toto';
                break; 
            case 81:
            $shortcut = 'eso';
                break; 
            case 82:
            $shortcut = 'win';
                break; 
            case 83:
            $shortcut = 'fam';
                break; 
            case 84:
            $shortcut = 'char';
                break; 
            case 85:
            $shortcut = 'love';
                break; 
            case 86:
            $shortcut = 'spofi';
                break; 
            case 87:
            $shortcut = 'versi';
                break; 
            case 88:
            $shortcut = 'inge';
                break; 
            case 89:
            $shortcut = 'immo';
                break; 
            case 90:
            $shortcut = 'wipo';
                break; 
            case 91:
            $shortcut = 'trawell';
                break; 
            case 92:
            $shortcut = 'lotto';
                break; 
            case 93:
            $shortcut = 'toto';
                break; 
            case 94:
            $shortcut = 'eso';
                break; 
            case 95:
            $shortcut = 'win';
                break; 
            case 96:
            $shortcut = 'fam';
                break;
            case 97:
            $shortcut = 'char';
                break; 
            case 98:
            $shortcut = 'love';
                break; 
            case 99:
            $shortcut = 'spofi';
                break; 
            
            
            
           }
         }
        if((\Packages\Core\Utility\GeneralUtility::isTestSystem())
                 ||
                 (\Packages\Core\Utility\GeneralUtility::isDevSystem())
                 ){
            switch (\intval($clickProfileID)) {
            case 1:
            $shortcut = 'wifa';
                break;
            
            case 2:
            $shortcut = 'limod';
                break;
            
            case 3:
            $shortcut = 'telko';
                break;
            
            case 4:
            $shortcut = 'techcar';
                break;
            
            case 5:
            $shortcut = 'reiwell';
                break;
            
            case 6:
            $shortcut = 'unthob';
                break;
            
            case 7:
            $shortcut = 'game';
                break;
            
            case 8:
            $shortcut = 'luxgou';
                break;
            
            case 9:
            $shortcut = 'tiere';
                break;
            
            case 10:
            $shortcut = 'sozi';
                break;
            
            case 11:
            $shortcut = 'date';
                break;
            
            case 12:
            $shortcut = 'sonstiges';
                break;  
              case 62:
            $shortcut = 'wifa';
                break;
            
            case 63:
            $shortcut = 'limod';
                break;
            
            case 64:
            $shortcut = 'telko';
                break;
            
            case 65:
            $shortcut = 'techcar';
                break;
            
            case 66:
            $shortcut = 'reiwell';
                break;
            
            case 67:
            $shortcut = 'unthob';
                break;
            
            case 68:
            $shortcut = 'game';
                break;
            
            case 69:
            $shortcut = 'luxgou';
                break;
            
            case 70:
            $shortcut = 'tiere';
                break;
            
            case 71:
            $shortcut = 'sozi';
                break;
            
            case 72:
            $shortcut = 'date';
                break;
            
            case 73:
            $shortcut = 'sonstiges';
                break; 
            case 80:
            $shortcut = 'versi';
                break; 
            case 81:
            $shortcut = 'inge';
                break; 
            case 82:
            $shortcut = 'immo';
                break; 
            case 83:
            $shortcut = 'wipo';
                break; 
            case 84:
            $shortcut = 'trawell';
                break; 
            case 85:
            $shortcut = 'lotto';
                break; 
            case 86:
            $shortcut = 'toto';
                break; 
            case 87:
            $shortcut = 'eso';
                break; 
            case 88:
            $shortcut = 'win';
                break; 
            case 89:
            $shortcut = 'fam';
                break; 
            case 90:
            $shortcut = 'char';
                break; 
            case 91:
            $shortcut = 'love';
                break; 
            case 92:
            $shortcut = 'spofi';
                break; 
            case 93:
            $shortcut = 'versi';
                break; 
            case 94:
            $shortcut = 'inge';
                break; 
            case 95:
            $shortcut = 'immo';
                break; 
            case 96:
            $shortcut = 'wipo';
                break; 
            case 97:
            $shortcut = 'trawell';
                break; 
            case 98:
            $shortcut = 'lotto';
                break; 
            case 99:
            $shortcut = 'toto';
                break; 
            case 100:
            $shortcut = 'eso';
                break; 
            case 101:
            $shortcut = 'win';
                break; 
            case 102:
            $shortcut = 'fam';
                break;
            case 103:
            $shortcut = 'char';
                break; 
            case 104:
            $shortcut = 'love';
                break; 
            case 105:
            $shortcut = 'spofi';
                break; 
            
          }
        }
        return $shortcut;  
    }
    /**
     * processScheduleMailing
     * @param MailingEntity $mailingEntity
     * @return void
     */
    protected function processScheduleMailing(MailingEntity $mailingEntity, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity , $sendermail, $replytomail){
        
        if ($mailingEntity->getScheduleDate() instanceof \DateTime 
			&& $mailingEntity->getUseScheduleDate()
		) {
             $this->initApiClient();
             $externaljobid = $this->sendeffectClient->send(
                                            $campaignEntity->getMail_id(), 
                                            $mailingEntity->getScheduleDate()->format('Y-m-d H:i:s'), 
                                            '', 
                                            $mailingEntity->getRecipientListIds(), 
                                            $mailingEntity->getFromName(),
                                            $mailingEntity->getMaxRecipients(), 
                                            FALSE,
                                            $sendermail, 
                                            $replytomail
                                            );
             return $externaljobid;
      
        }
    }
 }
