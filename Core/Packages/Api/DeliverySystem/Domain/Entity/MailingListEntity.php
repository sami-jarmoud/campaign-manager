<?php
namespace Packages\Api\DeliverySystem\Domain\Entity;

use Packages\Core\DomainObject\AbstractDomainObject;


/**
 * Description of MailingListEntity
 *
 * @author Cristian.Reus
 */
class MailingListEntity extends AbstractDomainObject {
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $title;
	
	/**
	 * status
	 * 
	 * @var string
	 */
	protected $status;
	
	/**
	 * date
	 * 
	 * @var \DateTime
	 */
	protected $date = NULL;
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
	}

	public function getDate() {
		if (!($this->date instanceof \DateTime)) {
			$this->setDate($this->date);
		}
		
		return $this->date;
	}
	public function setDate($date = NULL) {
		$this->createDateTimeFromValueForField(
			'date',
			$date
		);
	}

}
