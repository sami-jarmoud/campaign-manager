<?php
namespace Packages\Api\DeliverySystem\Domain\Entity;


/**
 * Description of StatisticEntity
 * 
 * @author MohamedSamiJarmoud
 *
 */
class StatisticEntity extends AbstractDomainObject{
	
	/**
	 * versandSystem
	 *
	 * @var string
	 */
	protected $versandSystem;
	
	/**
	 * verteilerIds
	 *
	 * @var integer
	 */
	protected $verteilerId  ;
	
	/**
	 * geschlecht
	 *
	 * @var string
	 */
	protected $geschlecht;
	
	/**
	 * selektionsProfil
	 *
	 * @var array
	 */
	protected $selektionsProfil= array();
	
	/**
	 * alter
	 *
	 * @var array
	 */
	protected $alterDataArray = array();
	
	/**
	 * plz
	 *
	 * @var array
	 */
	protected $plzDataArray = array();
	
	
	
		
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setversandSystem($versandSystem){
		$this->versandSystem = $versandSystem;
	}
	
	public function getversandSystem(){
		return $this->versandSystem;
	}
	
	public function setVerteilerId($verteilerId){
		$this->verteilerId = $verteilerId;
	}
	
	public function getVerteilerId(){
		return $this->verteilerId;
	}
	
	public function setGeschlecht($geschlecht){
		$this->geschlecht = $geschlecht;
	}
	
	public function getGeschlecht(){
		return $this->geschlecht;
	}
	
	public function setSelektionsProfil($selektionsProfil){
		$this->selektionsProfil = $selektionsProfil;
	}
	
	public function getSelektionsProfilArray(){
		return $this->selektionsProfil;
	}
	
	public function setAlterDataArray($juenger , $aelter){
		$this->alterDataArray = array(
										'juenger' => $juenger,
										'aelter'  => $aelter
								);
	}
	public function getAlterDataArray(){
		return $this->alterDataArray;
	}
	public function setPlzDataArray($kleiner, $groesser){
		$this->plzDataArray = array(
										'kleiner'   => $kleiner,
										'groesser'  => $groesser
								);
	}
	public function getPlzDataArray(){
		return $this->plzDataArray;
	}
	
}