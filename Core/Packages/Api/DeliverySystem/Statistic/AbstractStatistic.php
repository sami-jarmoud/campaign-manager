<?php
namespace Packages\Api\DeliverySystem\Statistic;

use Packages\Api\DeliverySystem\Statistic\InterfaceStatistic;
use Packages\Core\Domain\Model\ClientDeliveryEntity;
use Packages\Core\Utility\GeneralUtility as CoreGeneralUtility;

/**
 * 
 * @author MohamedSamiJarmoud
 * 
 * TODO: auslagern
 *
 */
abstract class AbstractStatistic implements InterfaceStatistic{
	
	/**
	 * authentication
	 *
	 * @var \Packages\Api\DeliverySystem\Authentication\AbstractAuthentication
	 * @inject
	 */
	protected $authentication = NULL;
	
	/**
	 * authentificationClassName
	 *
	 * @var string
	 */
	protected $authentificationClassName = '\\Packages\\Api\\DeliverySystem\\Authentication\\';
	
	
	/**
	 * init
	 *
	 * @param ClientDeliveryEntity $clientDeliverySystem
	 * @return void
	 */
	public function init(ClientDeliveryEntity $clientDeliverySystem) {
		if (\is_null($this->authentication)) {
			$className = $this->authentificationClassName . $clientDeliverySystem->getDeliverySystemEntity()->getAsp();
	
			$this->authentication = CoreGeneralUtility::makeInstance($className);
			$this->authentication->setClientId($clientDeliverySystem->getM_login());
			$this->authentication->setUsername($clientDeliverySystem->getApi_user());
			$this->authentication->setPassword($clientDeliverySystem->getPw());
			$this->authentication->setSharedKey($clientDeliverySystem->getShared_key());
			$this->authentication->setSecretKey($clientDeliverySystem->getSecret_key());
				
			$this->authentication->login();
		}
	}
	
	public function __destruct() {
		if (!\is_null($this->authentication)) {
			$this->authentication->logout();
			$this->authentication = NULL;
		}
	}
	
}

?>