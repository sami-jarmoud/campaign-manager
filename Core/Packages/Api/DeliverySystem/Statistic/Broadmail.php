<?php
namespace Packages\Api\DeliverySystem\Statistic;

use Packages\Api\DeliverySystem\Factory\Broadmail as BroadmailFactory;
use Packages\Api\DeliverySystem\Statistic\AbstractStatistic;

/**
 * 
 * @author MohamedSamiJarmoud
 *
 */
class Broadmail extends AbstractStatistic {
		
/**
 * processStatisticAuszaelung
 * 
 * @param array $auszaelung
 * @param integer $distributor_id
 * @return integer
 * @throws \Exception
 */	
public function processStatisticAuszaelung(array $auszaelung , $distributor_id ){
		
		try {
 			$recipientFilterWebservice = BroadmailFactory::getInstance('RecipientFilterWebservice');
 			$recipientWebservice = BroadmailFactory::getInstance('RecipientWebservice');
 			
//Geschlect
            $gender = $this->getGender($auszaelung['genderSystem']);
            $results['gender'] = $gender;
			$filterAndModificationIdArray = $this->processGeschlecht(
							               $recipientFilterWebservice,
					                       $gender,
					                       'Anrede'
					               );
		
			   
//Alter	
           
		    if (!empty($auszaelung['alterVon']) || !empty($auszaelung['alterBis'])){
		    	$alterVon = $this->getAlter($auszaelung['alterVon']);
		    	$alterBis = $this->getAlter($auszaelung['alterBis']);
		   	    $this->processAlter(
		   	    		  $recipientFilterWebservice, 
		   	    		  $alterVon,
		   	    		  $alterBis,
		   	    		  $filterAndModificationIdArray['modificationId'],
		   	    		  'Geburtsdatum'
		   	    		);
		    }
// 		   //Selektionsprofil
// 		   $selektionsDataArray = $auszaelung->getSelektionsProfilArray();
// 		   if(!empty($selektionsDataArray)){
// 			$this->processSelektionProfil(
// 					      $recipientFilterWebservice, 
// 					      $selektionsDataArray, 
// 					      $geschlechtDataArray['modificationId'],
// 					      $feldName->getSelektionsFeldName()
// 					);
// 		   }
// 	       //Reagierer
// 		 if (!empty($auszaelung['reagierer'])){
// 		   	    $this->processReagierer(
// 		   	    		  $recipientFilterWebservice, 
// 		   	    		  $auszaelung['reagierer'],
// 		   	    		  $filterAndModificationIdArray['modificationId']
// 		   	    		);
// 		    }
		    //PLZ
		    if (!empty($auszaelung['plzVon']) || !empty($auszaelung['plzBis'] )){
		    	$this->processPlz(
		    			$recipientFilterWebservice,
		    			$auszaelung['plzVon'],
		    			$auszaelung['plzBis'],
		    			$filterAndModificationIdArray['modificationId'],
		    			'Plz'
		    			);
		    }
			//Verteiler		 
			$results['data'] = $this->processVerteiler(
					$recipientFilterWebservice,
					$recipientWebservice,
					$filterAndModificationIdArray['modificationId'],
					$filterAndModificationIdArray['filterId'],
					$distributor_id
					);
					
	  }catch (\Exception $e) {
			throw $e;
		}
	  return $results;
}

/**
 * processStatisticAnlegen
 * 
 * @param array $auszaelung
 * @param integer $distributor_id
 * Qparam integer $client_id
 * @return integer
 * @throws \Exception
 */
public function processStatisticAnlegen(array $auszaelung , $distributor_id, $client_id ){

	try {
		$recipientFilterWebservice = BroadmailFactory::getInstance('RecipientFilterWebservice');
		$recipientWebservice = BroadmailFactory::getInstance('RecipientWebservice');
		$folderWebservice = BroadmailFactory::getInstance('FolderWebservice');
		
//Geschlect
		$gender = $auszaelung['genderSystem'];
		$results['gender'] = $gender;
		$filterAndModificationIdArray = $this->processAnlegenGeschlecht(
				$recipientFilterWebservice,
				$gender,
				'Anrede',
				$auszaelung['funktionName']
				);


//Alter	 
		if (!empty($auszaelung['alterVon']) || !empty($auszaelung['alterBis'])){
			$alterVon = $this->getAlter($auszaelung['alterVon']);
			$alterBis = $this->getAlter($auszaelung['alterBis']);
			$this->processAlter(
					$recipientFilterWebservice,
					$alterVon,
					$alterBis,
					$filterAndModificationIdArray['modificationId'],
					'Geburtsdatum'
					);
		}
// 		   //Selektionsprofil
// 		   $selektionsDataArray = $auszaelung->getSelektionsProfilArray();
// 		   if(!empty($selektionsDataArray)){
// 			$this->processSelektionProfil(
// 					      $recipientFilterWebservice,
// 					      $selektionsDataArray,
// 					      $geschlechtDataArray['modificationId'],
// 					      $feldName->getSelektionsFeldName()
// 					);
// 		   }
// 	       //Reagierer
// 		 if (!empty($auszaelung['reagierer'])){
// 		   	    $this->processReagierer(
// 		   	    		  $recipientFilterWebservice,
// 		   	    		  $auszaelung['reagierer'],
// 		   	    		  $filterAndModificationIdArray['modificationId']
// 		   	    		);
// 		    }
		//PLZ
		if (!empty($auszaelung['plzVon']) || !empty($auszaelung['plzBis'] )){
			$this->processPlz(
					$recipientFilterWebservice,
					$auszaelung['plzVon'],
					$auszaelung['plzBis'],
					$filterAndModificationIdArray['modificationId'],
					'Plz'
					);
		}
		//Verteiler
		$results['data'] = $this->processVerteiler(
				$recipientFilterWebservice,
				$recipientWebservice,
				$filterAndModificationIdArray['modificationId'],
				$filterAndModificationIdArray['filterId'],
				$distributor_id
				);
		
		$results['zielgruppeID'] = $filterAndModificationIdArray['filterId'];
		$results['alerVon'] = $this->getAlter($auszaelung['alterVon']);
		$this->processFolder(
				$folderWebservice,
				$filterAndModificationIdArray['filterId'],
				$client_id
			   );
		
	}catch (\Exception $e) {
		throw $e;
	}
	return $results;
}

/**
 * processGeschlecht
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param string $geschlecht
 * @param string $feldName
 * @return long
 */
protected function processGeschlecht(\SoapClient $recipientFilterWebservice,$geschlecht,$feldName){
    	if(!empty($geschlecht)){
    		$filterId = $recipientFilterWebservice->__soapCall(
    			'createTemporary', 
    				array(
	    				$this->authentication->getAuthentification(),
	    				false,
	    				$feldName,
	    				"==",
	    				array(
	    						$geschlecht
	    				)
    					)
    				);
    			
    		$modificationId = $recipientFilterWebservice->__soapCall(
    			'beginConditionModification', 
    			   array(
    					$this->authentication->getAuthentification(),
    					$filterId
    			       )
    			    );
    	}else{
    		$filterId = $recipientFilterWebservice->__soapCall(
    			'createTemporary', 
    				array(
	    				$this->authentication->getAuthentification(),
	    				false,
	    				$feldName,
	    				"==",
	    				array(
	    						'Herr'
	    						)
    					)
    				);
    	
    		$modificationId = $recipientFilterWebservice->__soapCall(
    			'beginConditionModification', 
    			    array(
    					$this->authentication->getAuthentification(),
    					$filterId
    			        )
    			   );
    		$recipientFilterWebservice->__soapCall(
    			'addOrCondition', 
    			    array(
	    				$this->authentication->getAuthentification(),
	    				$modificationId,
	    				false,
	    				$feldName,
	    				"==",
	    				array(
	    						"Frau"
	    				)
    					)
    				);
    	}
    	$filterAndModificationIdArray = array(
		    			"filterId"       => $filterId,
		    			"modificationId" => $modificationId
    	);
    	return $filterAndModificationIdArray;
    }
	
/**
 * processAnlegenGeschlecht
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param string $geschlecht
 * @param string $feldName
 * @param string $funktionName
 * @return long
 */
protected function processAnlegenGeschlecht(\SoapClient $recipientFilterWebservice,$geschlecht,$feldName,$funktionName){
    	if(!empty($geschlecht)){
    		$filterId = $recipientFilterWebservice->__soapCall(
    				'create',
    				array(
    						$this->authentication->getAuthentification(),
    						$funktionName,
    						false,
    						$feldName,
    						"==",
    						array(
    								$geschlecht
    						)
    				)
    				);
    		 
    		$modificationId = $recipientFilterWebservice->__soapCall(
    				'beginConditionModification',
    				array(
    						$this->authentication->getAuthentification(),
    						$filterId
    				)
    				);
    	}else{
    		$filterId = $recipientFilterWebservice->__soapCall(
    				'create',
    				array(
    						$this->authentication->getAuthentification(),
    						$funktionName,
    						false,
    						$feldName,
    						"==",
    						array(
    								'Herr'
    						)
    				)
    				);
    		 
    		$modificationId = $recipientFilterWebservice->__soapCall(
    				'beginConditionModification',
    				array(
    						$this->authentication->getAuthentification(),
    						$filterId
    				)
    				);
    		$recipientFilterWebservice->__soapCall(
    				'addOrCondition',
    				array(
    						$this->authentication->getAuthentification(),
    						$modificationId,
    						false,
    						$feldName,
    						"==",
    						array(
    								"Frau"
    						)
    				)
    				);
    	}
    	$filterAndModificationIdArray = array(
    			"filterId"       => $filterId,
    			"modificationId" => $modificationId
    	);
    	return $filterAndModificationIdArray;
    } 
	
/**
 * processVerteiler
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param \SoapClient $recipientWebservice
 * @param long $modificationId
 * @param long $filterId
 * @param integer $distributor_id
 * @return long
 */    
protected function processVerteiler(\SoapClient $recipientFilterWebservice, \SoapClient $recipientWebservice,$modificationId, $filterId, $distributor_id){
    	

    	$recipientFilterWebservice->__soapCall(
    			'commitConditionModification', 
    			array(
    					$this->authentication->getAuthentification(),
    					$modificationId
    			)
    			);
    	$result= $recipientWebservice->__soapCall(
    			'getCount',
    			array(
    					$this->authentication->getAuthentification(),
    					$distributor_id,
    					$filterId
    			)
    			);
       
    	return $result;
    }

/**
 * processSelektionProfil
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param array $selektionsDataArray
 * @param long $modificationId
 * @param string $feldName
 */	
protected function processSelektionProfil(\SoapClient $recipientFilterWebservice, array $selektionsDataArray,$modificationId ,$feldName){
    	$ersteSelektion = array_shift($selektionsDataArray);
    	$recipientFilterWebservice->addAndCondition(
    			$modificationId,
    			false,
    			$feldName,
    			"==",
    			$ersteSelektion
    			);
    	
    	if(!empty($selektionsDataArray)){
    		foreach ($selektionsDataArray as $selektion){
    			$recipientFilterWebservice->addOrCondition(
    					$modificationId,
    					false,
    					$feldName,
    					"==",
    					$selektion
    					);
    		}
    	}
    }

/**
 * processAlter
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param string $alterVon
 * @param string $alterBis
 * @param long $modificationId
 * @param string $feldName
 */	
protected function processAlter(\SoapClient $recipientFilterWebservice, $alterVon, $alterBis ,$modificationId,$feldName){
    	if(!empty($alterVon)){
    		$recipientFilterWebservice->__soapCall(
    			'addAndCondition', 
    			array(
    					$this->authentication->getAuthentification(),
    					$modificationId,
    					false,
    					$feldName,
    					'<=',
    					array(
    							$alterVon
    					)
    			)
    			);
    	}
    	if(!empty($alterBis)){
    		$recipientFilterWebservice->__soapCall(
    			'addAndCondition', 
    			array(
    					$this->authentication->getAuthentification(),
    					$modificationId,
    					false,
    					$feldName,
    					'>=',
    					array(
    							$alterBis
    					)
    			)
    			);
    	}
    }
 
/**
 * processPlz
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param string $plzVon
 * @param string $plzBis
 * @param long $modificationId
 * @param string $feldName
 */	
protected function processPlz(\SoapClient $recipientFilterWebservice, $plzVon, $plzBis ,$modificationId,$feldName){
    	if(!empty($plzVon)){
    		$recipientFilterWebservice->__soapCall(
    			'addAndCondition', 
    			array(
    					$this->authentication->getAuthentification(),
    					$modificationId,
    					false,
    					$feldName,
    					'>=',
    					array(
    							$plzVon
    					)
    			)
    			);
    	}
    	if(!empty($plzBis)){
    		$recipientFilterWebservice->__soapCall(
    			'addAndCondition', 
    			array(
    					$this->authentication->getAuthentification(),
    					$modificationId,
    					false,
    					$feldName,
    					'<=',
    					array(
    							$plzBis
    					)
    			)
    			);
    	}
    }
	
/**
 * getGender
 * 
 * @param string $gender
 * @return string
 */
protected function getGender($gender){
	switch ($gender){
		case 0:
			$genderData = '';
		     break;
		case 1:
			$genderData = 'Herr';
			break;
		case 2:
		    $genderData = 'Frau';
		    break;
		default:
			$genderData = '';
	}
	return $genderData;
}

/**
 * processReagierer
 * 
 * @param \SoapClient $recipientFilterWebservice
 * @param string $reagierer
 * @param long $modificationId
 */
protected function processReagierer(\SoapClient $recipientFilterWebservice, $reagierer, $modificationId){
	if(!empty($reagierer)){
		$recipientFilterWebservice->__soapCall(
				'addAndCondition',
				array(
						$this->authentication->getAuthentification(),
						$modificationId,
						false,
						'openedMailing',
						array(
								'0'
						)
				)
				);
	}
}

/**
 * getAlter
 * 
 * @param string $alter
 * @return string
 */
protected function getAlter($alter){
	if(!empty($alter)){
	$datum = new \DateTime('today -'.$alter .'years');
	$geburtsDatum = $datum->format('Y-m-d').' 00:00';
	}else{
		$geburtsDatum ='';
	}
	return $geburtsDatum;
	
}

/**
 * 
 * @param \SoapClient $folderWebservice
 * @param long $filterId
 */
protected function processFolder(\SoapClient $folderWebservice , $filterId, $client_id){
	
	switch ($client_id){
		case 1 :
			$folderId = '121016044574';
			break;
		case 2 :
			$folderId = '123724118966';
			break;
		case 3 :
			$folderId = '123724118972';
			break;
		case 4 :
			$folderId = '123724118952';
			break;
		case 6 :
			$folderId = '123760291007';
			break;
	}
	$folderWebservice->__soapCall(
			'assignFolder', 
			  array(
				  $this->authentication->getAuthentification(),
				  $filterId,
				  $folderId,
				  'filter'
			  )
			);
}
}
