<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Packages\Api\DeliverySystem\Utility;

/**
 * Description of DeliverySystemUtiltiy
 *
 * @author SamiMohamedJarmoud
 */
class DeliverySystemUtiltiy {
    
    
    
    
    
     /**
     * getShortcutClickProfiles
     * 
     * @param integer $clickProfileID
     * @return string
     */
     static public function getShortcutClickProfiles($clickProfileID){
        
        if(\Packages\Core\Utility\GeneralUtility::isProdSystem()){
        switch (\intval($clickProfileID)) {
            case 49:
            $shortcut = 'wifa';
                break;
            
            case 50:
            $shortcut = 'limod';
                break;
            
            case 51:
            $shortcut = 'telko';
                break;
            
            case 52:
            $shortcut = 'techcar';
                break;
            
            case 53:
            $shortcut = 'reiwell';
                break;
            
            case 54:
            $shortcut = 'unthob';
                break;
            
            case 55:
            $shortcut = 'game';
                break;
            
            case 56:
            $shortcut = 'luxgou';
                break;
            
            case 57:
            $shortcut = 'tiere';
                break;
            
            case 58:
            $shortcut = 'sozi';
                break;
            
            case 59:
            $shortcut = 'date';
                break;
            
            case 60:
            $shortcut = 'sonstiges';
                break;    
            case 62:
            $shortcut = 'wifa';
                break;
            
            case 63:
            $shortcut = 'limod';
                break;
            
            case 64:
            $shortcut = 'telko';
                break;
            
            case 65:
            $shortcut = 'techcar';
                break;
            
            case 66:
            $shortcut = 'reiwell';
                break;
            
            case 67:
            $shortcut = 'unthob';
                break;
            
            case 68:
            $shortcut = 'game';
                break;
            
            case 69:
            $shortcut = 'luxgou';
                break;
            
            case 70:
            $shortcut = 'tiere';
                break;
            
            case 71:
            $shortcut = 'sozi';
                break;
            
            case 72:
            $shortcut = 'date';
                break;
            
            case 73:
            $shortcut = 'sonstiges';
                break; 
            case 74:
            $shortcut = 'versi';
                break; 
            case 75:
            $shortcut = 'inge';
                break; 
            case 76:
            $shortcut = 'immo';
                break; 
            case 77:
            $shortcut = 'wipo';
                break; 
            case 78:
            $shortcut = 'trawell';
                break; 
            case 79:
            $shortcut = 'lotto';
                break; 
            case 80:
            $shortcut = 'toto';
                break; 
            case 81:
            $shortcut = 'eso';
                break; 
            case 82:
            $shortcut = 'win';
                break; 
            case 83:
            $shortcut = 'fam';
                break; 
            case 84:
            $shortcut = 'char';
                break; 
            case 85:
            $shortcut = 'love';
                break; 
            case 86:
            $shortcut = 'spofi';
                break; 
            case 87:
            $shortcut = 'versi';
                break; 
            case 88:
            $shortcut = 'inge';
                break; 
            case 89:
            $shortcut = 'immo';
                break; 
            case 90:
            $shortcut = 'wipo';
                break; 
            case 91:
            $shortcut = 'trawell';
                break; 
            case 92:
            $shortcut = 'lotto';
                break; 
            case 93:
            $shortcut = 'toto';
                break; 
            case 94:
            $shortcut = 'eso';
                break; 
            case 95:
            $shortcut = 'win';
                break; 
            case 96:
            $shortcut = 'fam';
                break;
            case 97:
            $shortcut = 'char';
                break; 
            case 98:
            $shortcut = 'love';
                break; 
            case 99:
            $shortcut = 'spofi';
                break; 
            
            case 100:
            $shortcut = 'wifa';
                break;
            
            case 101:
            $shortcut = 'limod';
                break;
            
            case 102:
            $shortcut = 'telko';
                break;
            
            case 103:
            $shortcut = 'techcar';
                break;
            
            case 104:
            $shortcut = 'reiwell';
                break;
            
            case 105:
            $shortcut = 'unthob';
                break;
            
            case 106:
            $shortcut = 'game';
                break;
            
            case 107:
            $shortcut = 'luxgou';
                break;
            
            case 108:
            $shortcut = 'tiere';
                break;
            
            case 109:
            $shortcut = 'sozi';
                break;
            
            case 110:
            $shortcut = 'date';
                break;
            
            case 111:
            $shortcut = 'sonstiges';
                break; 
             case 112:
            $shortcut = 'versi';
                break; 
            case 113:
            $shortcut = 'inge';
                break; 
            case 114:
            $shortcut = 'immo';
                break; 
            case 115:
            $shortcut = 'wipo';
                break; 
            case 116:
            $shortcut = 'trawell';
                break; 
            case 117:
            $shortcut = 'lotto';
                break; 
            case 118:
            $shortcut = 'toto';
                break; 
            case 119:
            $shortcut = 'eso';
                break; 
            case 120:
            $shortcut = 'win';
                break; 
            case 121:
            $shortcut = 'fam';
                break; 
            case 122:
            $shortcut = 'char';
                break; 
            case 123:
            $shortcut = 'love';
                break; 
            case 124:
            $shortcut = 'spofi';
                break; 
           }
           
         }
        if((\Packages\Core\Utility\GeneralUtility::isTestSystem())
                 ||
                 (\Packages\Core\Utility\GeneralUtility::isDevSystem())
                 ){
            switch (\intval($clickProfileID)) {
            case 1:
            $shortcut = 'wifa';
                break;
            
            case 2:
            $shortcut = 'limod';
                break;
            
            case 3:
            $shortcut = 'telko';
                break;
            
            case 4:
            $shortcut = 'techcar';
                break;
            
            case 5:
            $shortcut = 'reiwell';
                break;
            
            case 6:
            $shortcut = 'unthob';
                break;
            
            case 7:
            $shortcut = 'game';
                break;
            
            case 8:
            $shortcut = 'luxgou';
                break;
            
            case 9:
            $shortcut = 'tiere';
                break;
            
            case 10:
            $shortcut = 'sozi';
                break;
            
            case 11:
            $shortcut = 'date';
                break;
            
            case 12:
            $shortcut = 'sonstiges';
                break;  
                 case 80:
            $shortcut = 'versi';
                break; 
            case 81:
            $shortcut = 'inge';
                break; 
            case 82:
            $shortcut = 'immo';
                break; 
            case 83:
            $shortcut = 'wipo';
                break; 
            case 84:
            $shortcut = 'trawell';
                break; 
            case 85:
            $shortcut = 'lotto';
                break; 
            case 86:
            $shortcut = 'toto';
                break; 
            case 87:
            $shortcut = 'eso';
                break; 
            case 88:
            $shortcut = 'win';
                break; 
            case 89:
            $shortcut = 'fam';
                break; 
            case 90:
            $shortcut = 'char';
                break; 
            case 91:
            $shortcut = 'love';
                break; 
            case 92:
            $shortcut = 'spofi';
                break; 
            case 93:
            $shortcut = 'versi';
                break; 
            case 94:
            $shortcut = 'inge';
                break; 
            case 95:
            $shortcut = 'immo';
                break; 
            case 96:
            $shortcut = 'wipo';
                break; 
            case 97:
            $shortcut = 'trawell';
                break; 
            case 98:
            $shortcut = 'lotto';
                break; 
            case 99:
            $shortcut = 'toto';
                break; 
            case 100:
            $shortcut = 'eso';
                break; 
            case 101:
            $shortcut = 'win';
                break; 
            case 102:
            $shortcut = 'fam';
                break;
            case 103:
            $shortcut = 'char';
                break; 
            case 104:
            $shortcut = 'love';
                break; 
            case 105:
            $shortcut = 'spofi';
                break; 
            
               case 107:
            $shortcut = 'wifa';
                break;
            
            case 108:
            $shortcut = 'limod';
                break;
            
            case 109:
            $shortcut = 'telko';
                break;
            
            case 110:
            $shortcut = 'techcar';
                break;
            
            case 111:
            $shortcut = 'reiwell';
                break;
            
            case 112:
            $shortcut = 'unthob';
                break;
            
            case 113:
            $shortcut = 'game';
                break;
            
            case 114:
            $shortcut = 'luxgou';
                break;
            
            case 115:
            $shortcut = 'tiere';
                break;
            
            case 116:
            $shortcut = 'sozi';
                break;
            
            case 117:
            $shortcut = 'date';
                break;
            
            case 118:
            $shortcut = 'sonstiges';
                break; 
             case 119:
            $shortcut = 'versi';
                break; 
            case 120:
            $shortcut = 'inge';
                break; 
            case 121:
            $shortcut = 'immo';
                break; 
            case 122:
            $shortcut = 'wipo';
                break; 
            case 123:
            $shortcut = 'trawell';
                break; 
            case 124:
            $shortcut = 'lotto';
                break; 
            case 125:
            $shortcut = 'toto';
                break; 
            case 126:
            $shortcut = 'eso';
                break; 
            case 127:
            $shortcut = 'win';
                break; 
            case 128:
            $shortcut = 'fam';
                break; 
            case 129:
            $shortcut = 'char';
                break; 
            case 130:
            $shortcut = 'love';
                break; 
            case 131:
            $shortcut = 'spofi';
                break; 
          }
        }
        return $shortcut;  
    }
}
