<?php
namespace Packages\Api\DeliverySystem;

use Packages\Core\Domain\Model\ClientDeliveryEntity;
use Packages\Core\Utility\GeneralUtility as CoreGeneralUtility;

/**
 * Description of AbstractDeliverySystem
 *
 * @author CristianReus
 */
abstract class AbstractDeliverySystem {
	
	/**
	 * authentication
	 * 
	 * @var \Packages\Api\DeliverySystem\Authentication\AbstractAuthentication
	 * @inject
	 */
	protected $authentication = NULL;
	
	/**
	 * authentificationClassName
	 * 
	 * @var string
	 */
	protected $authentificationClassName = '\\Packages\\Api\\DeliverySystem\\Authentication\\';
	
	
	
	/**
	 * init
	 * 
	 * @param ClientDeliveryEntity $clientDeliverySystem
	 * @return void
	 */
	public function init(ClientDeliveryEntity $clientDeliverySystem) {
		if (\is_null($this->authentication)) {
                       $asps = $clientDeliverySystem->getDeliverySystemEntity()->getAsp();
                         switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'Stellar Reserve':
                            case '4Wave Extra':  
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
			$className = $this->authentificationClassName . $asp;

			$this->authentication = CoreGeneralUtility::makeInstance($className);
			$this->authentication->setClientId($clientDeliverySystem->getM_login());
			$this->authentication->setUsername($clientDeliverySystem->getApi_user());
			$this->authentication->setPassword($clientDeliverySystem->getPw());
			$this->authentication->setSharedKey($clientDeliverySystem->getShared_key());
			$this->authentication->setSecretKey($clientDeliverySystem->getSecret_key());
			
			$this->authentication->login();
		}
	}
	
	public function __destruct() {
		if (!\is_null($this->authentication)) {
			$this->authentication->logout();
			$this->authentication = NULL;
		}
	}
}
