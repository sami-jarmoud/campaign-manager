<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Packages\Api\DeliverySystem\Factory;

use Packages\Api\DeliverySystem\Factory\InterfaceWebserviceFactory;

require_once(\DIR_Vendor . 'DeliverySystems' . \DIRECTORY_SEPARATOR . 'Maileon' . \DIRECTORY_SEPARATOR . 'includes' . \DIRECTORY_SEPARATOR . 'MaileonApiClient.php');
/**
 * Description of Maileon
 *
 * @author SamiMohamedJarmoud
 */
class Maileon implements InterfaceWebserviceFactory{
    /**
	 * getWebservice
	 * 
	 * @param string $webserviceType
         * @param mixed $auth 
	 * @return 
	 * @throws \DomainException
	 */
        public static function getWebservice($webserviceType, $auth) {
		try {
			switch ($webserviceType) {
				case 'MailingsWebservice':
					$webservice = new \com_maileon_api_mailings_MailingsService($auth); 
                                        #$webservice->setDebug(FALSE);
					break;
                                case 'ReportsWebservice':
                                        $webservice = new \com_maileon_api_reports_ReportsService($auth);
                                        break;
                                case 'ContactsService':
                                        $webservice = new \com_maileon_api_contacts_ContactsService($auth);
                                        break;
                                case 'ContactFiltersService':
                                        $webservice = new \com_maileon_api_contactfilters_ContactfiltersService;
                                        break;
				default:
					throw new \DomainException('Unknown webserviceObject: ' . $webserviceType);
			}

			return $webservice;
		} catch (\Exception $e) {
			// TODO: log message
			throw $e;
		}
	}
        
        /**
	 * getWebserviceObjectByType
	 * 
	 * @param string $webserviceType
	 * @return \SoapClient
	 * @throws \DomainException
	 */
       public static function getInstance($webserviceType) {
            return 0;
        }
}
