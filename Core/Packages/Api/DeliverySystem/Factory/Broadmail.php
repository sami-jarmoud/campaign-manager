<?php
namespace Packages\Api\DeliverySystem\Factory;

use Packages\Api\DeliverySystem\Factory\InterfaceWebserviceFactory;

/**
 * Description of Broadmail
 *
 * @author Cristian.Reus
 */
class Broadmail implements InterfaceWebserviceFactory {
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * @param string $webserviceType
	 * @return \SoapClient
	 * @throws \DomainException
	 */
	public static function getInstance($webserviceType) {
		try {
			switch ($webserviceType) {
				case 'BlacklistWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcBlacklist?wsdl');
					break;

				case 'MailIdWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailId?wsdl');
					break;

				case 'MailingReportingWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailingReporting?wsdl');
					break;

				case 'MailingWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcMailing?wsdl');
					break;

				case 'OptinProcessWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcOptinProcess?wsdl');
					break;

				case 'RecipientListWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipientList?wsdl');
					break;

				case 'RecipientWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipient?wsdl');
					break;

				case 'ResponseWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcResponse?wsdl');
					break;

				case 'UnsubscribeWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcUnsubscribe?wsdl');
					break;
				
				case 'RecipientFilterWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcRecipientFilter?wsdl');
					break;
				
				case 'FolderWebservice':
					$webservice = new \SoapClient('http://api.broadmail.de/soap11/RpcFolder?wsdl');
					break;

				default:
					throw new \DomainException('Unknown webserviceObject: ' . $webserviceType, 1445348285);
			}

			return $webservice;
		} catch (\Exception $e) {
			// TODO: log message
			throw $e;
		}
	}
	
        /**
	 * getWebservice
	 * 
	 * @param string $webserviceType
         * @param mixed $auth 
	 * @return 0
	 * @throws \DomainException
	 */
        public static function getWebservice($webserviceType, $auth) {
            return 0;
        }
                }
