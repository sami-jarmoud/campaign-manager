<?php
namespace Packages\Core\Persistence\Generic;

use Packages\Core\Manager\Database\InterfaceConnection;


/**
 * Description of AbstractQuery
 *
 * @author Cristian.Reus
 */
abstract class AbstractQuery {
	
	/**
	 * fieldEqual
	 * 
	 * @var string
	 */
	public static $fieldEqual = ' = ';
	
	/**
	 * fieldNotEqual
	 * 
	 * @var string
	 */
	public static $fieldNotEqual = ' != ';
	
	/**
	 * fieldIn
	 * 
	 * @var string
	 */
	public static $fieldIn = ' IN ';
	
	/**
	 * fieldNotIn
	 * 
	 * @var string
	 */
	public static $fieldNotIn = ' NOT IN ';
	
	/**
	 * fieldGreaterThan 
	 * 
	 * @var string
	 */
	public static $fieldGreaterThan = ' > ';
	
	/**
	 * fieldLessThan 
	 * 
	 * @var string
	 */
	public static $fieldLessThan = ' < ';
	
	/**
	 * fieldGreaterOrEqualThan
	 * 
	 * @var string
	 */
	public static $fieldGreaterOrEqualThan = ' >= ';
	
	/**
	 * fieldLessOrEqualThan
	 * 
	 * @var string
	 */
	public static $fieldLessOrEqualThan = ' <= ';
	
	/**
	 * selectQueryType
	 * 
	 * @var integer
	 */
	public static $selectQueryType = 0;
	
	/**
	 * insertQueryType
	 * 
	 * @var integer
	 */
	public static $insertQueryType = 1;
	
	/**
	 * updateQueryType
	 * 
	 * @var integer
	 */
	public static $updateQueryType = 2;
	
	/**
	 * deleteQueryType
	 * 
	 * @var integer
	 */
	public static $deleteQueryType = 3;
	
	/**
	 * databaseHandle
	 * 
	 * @var InterfaceConnection
	 */
	protected $databaseHandle = NULL;
	
	/**
	 * primaryField
	 * 
	 * @var string
	 */
	protected $primaryField = NULL;
	
	/**
	 * table
	 * 
	 * @var string
	 */
	protected $table = NULL;
	
	
	
	/**
	 * masketField
	 * 
	 * @param string $field
	 * @return string
	 */
	public static function masketField($field) {
		return '`' . \strip_tags($field) . '`';
	}
	
	/**
	 * createPdoInString
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	public static function createPdoInString(array $dataArray) {
		$itemHoldersDataArray = array();
		foreach ($dataArray as $item) {
			$itemHoldersDataArray[] = $item;
		}

		return \implode(',', $itemHoldersDataArray);
	}
	
	/**
	 * createSelectQuery
	 * 
	 * @param array $queryPartsDataArray
	 * @param string $addWhere
	 * @return string
	 */
	public function createSelectQuery(array $queryPartsDataArray, $addWhere = '') {
		$selectFields = '*';
		if (isset($queryPartsDataArray['SELECT'])) {
			if (\is_array($queryPartsDataArray['SELECT']) 
				&& \count($queryPartsDataArray['SELECT']) > 0
			) {
				$selectFields = $this->getSelectFieldsQry($queryPartsDataArray['SELECT']);
			} else {
				$selectFields = $queryPartsDataArray['SELECT'];
			}
		}

		$addWhereQuery = '';
		if (isset($queryPartsDataArray['WHERE']) 
			&& \count($queryPartsDataArray['WHERE']) > 0
		) {
			$addWhereQuery = $this->addWhereQry($queryPartsDataArray['WHERE']);
		}
		
		if (!isset($queryPartsDataArray['FROM'])) {
			$queryPartsDataArray['FROM'] = self::masketField($this->table);
		}

		$query = 'SELECT ' . $selectFields
			. ' FROM ' . $queryPartsDataArray['FROM']
			. ' WHERE 1 = 1'
				. $addWhereQuery
				. $addWhere
			. (isset($queryPartsDataArray['GROUP_BY']) ? ' GROUP BY ' . $queryPartsDataArray['GROUP_BY'] : '')
			. (isset($queryPartsDataArray['ORDER_BY']) ? ' ORDER BY ' . $queryPartsDataArray['ORDER_BY'] : '')
			. (isset($queryPartsDataArray['LIMIT']) ? ' LIMIT ' . $queryPartsDataArray['LIMIT'] : '')
		;

		return $query;
	}
	
	/**
	 * createInsertQuery
	 * 
	 * @param array $dataArray
	 * @param null|string $table
	 * @return string
	 */
	public function createInsertQuery(array $dataArray, $table = NULL) {
		if (\is_null($table) 
			|| \strlen($table) === 0
		) {
			$table = $this->table;
		}
		
		$query = 'INSERT INTO ' . self::masketField($table)
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $query;
	}
	
	/**
	 * createUpdateQuery
	 * 
	 * @param string $primaryField
	 * @param array $dataArray
	 * @param null|string $table
	 * @return string
	 */
	public function createUpdateQuery($primaryField, array $dataArray, $table = NULL) {
		if (\is_null($table) 
			|| \strlen($table) === 0
		) {
			$table = $this->table;
		}
		
		$query = 'UPDATE ' . self::masketField($table)
			. ' SET ' . $this->processAddUpdateQueryFields($dataArray)
			. ' WHERE ' . self::masketField($primaryField) . ' = :_' . $primaryField
		;
		
		return $query;
	}
	
	/**
	 * createDeleteQuery
	 * 
	 * @param string $primaryField
	 * @param null|string $table
	 * @return string
	 */
	public function createDeleteQuery($primaryField, $table = NULL) {
		if (\is_null($table) 
			|| \strlen($table) === 0
		) {
			$table = $this->table;
		}
		
		$query = 'DELETE' 
			. ' FROM ' . self::masketField($table)
			. ' WHERE ' . self::masketField($primaryField) . ' = :_' . $primaryField
		;
		
		return $query;
	}
	
	/**
	 * getSelectFieldsQry
	 * 
	 * @param array $queryPartsSelectDataArray
	 * @return string
	 */
	protected function getSelectFieldsQry(array $queryPartsSelectDataArray) {
		$selectFields = array();
		foreach ($queryPartsSelectDataArray as $key => $item) {
			$selectFields[] = self::masketField($item) . ' as ' . self::masketField($key);
		}

		return \implode(', ', $selectFields);
	}
	
	/**
	 * addWhereQry
	 * 
	 * @param array $queryPartsWhereDataArray
	 * @return string
	 */
	protected function addWhereQry(array $queryPartsWhereDataArray) {
		$addQuery = '';
		foreach ($queryPartsWhereDataArray as $key => $itemDataArray) {
			if (\strlen($itemDataArray['value']) > 0) {
				switch ($itemDataArray['comparison']) {
					case 'fieldEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . \trim($itemDataArray['value']);
						break;

					case 'integerEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . ':' . $key;
						break;

					case 'fieldNotEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldNotEqual . \trim($itemDataArray['value']);
						break;

					default:
						$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' :' . \trim($key);
						break;
				}
			} else {
				if ((string) $itemDataArray['comparison'] === '<>') {
					$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' ""';
				} elseif (\strlen($itemDataArray['comparison']) === 0) {
					$addQuery .= ' AND ' . $itemDataArray['sql'];
				}
			}
		}

		return $addQuery;
	}
	
	/**
	 * processInsertQueryFieldsAndValues
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	protected function processInsertQueryFieldsAndValues(array $dataArray) {
		$queryFields = $queryFieldsValue = array();
		foreach ($dataArray as $field => $item) {
			$queryFields[] = self::masketField($field);
			$queryFieldsValue[] = ':' . $field;
		}

		return ' (' . \implode(', ', $queryFields) . ')'
			. ' VALUES (' . \implode(', ', $queryFieldsValue) . ')'
		;
	}
	
	/**
	 * processAddUpdateQueryFields
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	protected function processAddUpdateQueryFields(array $dataArray) {
		$addQuery = array();
		foreach ($dataArray as $field => $item) {
			$addQuery[] = self::masketField($field) . ' = :' . $field;
		}

		return \implode(', ', $addQuery);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setDatabaseHandle(InterfaceConnection $databaseHandle) {
		$this->databaseHandle = $databaseHandle;
		
		return $this;
	}
	
	public function setPrimaryField($primaryField) {
		$this->primaryField = $primaryField;
		
		return $this;
	}
	
	public function setTable($table) {
		$this->table = $table;
	}

}
