<?php
namespace Packages\Core\Persistence;

use Packages\Core\Persistence\Generic\Query;
use Packages\Core\Manager\Database\InterfaceConnection;
use Packages\Core\Reflection\ClassReflection;
use Packages\Core\Utility\TypeHandlingUtility;
use Packages\Core\Utility\TypeMatchingUtility;


/**
 * Description of AbstractRepository	
 *
 * @author Cristian.Reus
 */
abstract class AbstractRepository {
	
	/**
	 * databaseHandle
	 * 
	 * @var InterfaceConnection
	 */
	protected $databaseHandle = NULL;
	
	/**
	 * query
	 * 
	 * @var Query
	 * @inject
	 */
	protected $query = NULL;
	
	/**
	 * table
	 * 
	 * @var string
	 */
	protected $table = NULL;
	
	/**
	 * primaryField
	 * 
	 * @var string
	 */
	protected $primaryField = NULL;
	
	/**
	 * fetchClass
	 * 
	 * @var string
	 */
	protected $fetchClass = NULL;
	
	/**
	 * useMemoryUsage
	 * 
	 * @var boolean
	 */
	protected $useMemoryUsage = FALSE;
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public function init() {
		if (\is_null($this->databaseHandle)) {
			throw new \InvalidArgumentException('no databaseHandle initialized', 1446043805);
		}
		
		$this->query = new Query();
		$this->query->setPrimaryField($this->primaryField);
		$this->query->setTable($this->table);
		$this->query->setDatabaseHandle($this->databaseHandle);
		$this->query->init();
	}
	
	/**
	 * findAll
	 * 
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAll() {
		return $this->findAllByQueryPartsDataArray(
			array(),
			'',
			$this->fetchClass
		);
	}
	
	/**
	 * findAllByQueryPartsDataArray
	 * 
	 * @param array $queryPartsDataArray
	 * @param string $addWhere
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAllByQueryPartsDataArray(array $queryPartsDataArray, $addWhere = '') {
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass,
			$addWhere
		);
	}
	
	/**
	 * findById
	 * 
	 * @param integer $id
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 * @throws \InvalidArgumentException
	 */
	public function findById($id) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid id.', 1446538291);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	}
	
	/**
	 * findByField
	 * 
	 * @param string $field
	 * @param string $fieldValue
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAllByField($field, $fieldValue) {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $field => array(
					'sql' => Query::masketField($field),
					'value' => $fieldValue,
					'comparison' => Query::$fieldEqual
				),
			),
		);
		
		return $this->findAllByQueryPartsDataArray($queryPartsDataArray);
	}
	
	/**
	 * findOneByField
	 * 
	 * @param string $field
	 * @param string $fieldValue
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 */
	public function findOneByField($field, $fieldValue) {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $field => array(
					'sql' => Query::masketField($field),
					'value' => $fieldValue,
					'comparison' => Query::$fieldEqual
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	}
	
	/**
	 * add
	 * 
	 * @param \Packages\Core\DomainObject\AbstractEntity $domainEntity
	 * @param null|string $table
	 * @return integer
	 * @throws \InvalidArgumentException
	 */
	public function add(\Packages\Core\DomainObject\AbstractEntity $domainEntity, $table = NULL) {
		// TODO: add required parameter
		if (!$domainEntity->_isNew()) {
			throw new \InvalidArgumentException('The domainEntity "(' . \get_class($domainEntity) . ')" is not new.', 1446214635);
		}
		
		$dataArray = $this->createDataArrayFromEntity($domainEntity);
		unset($dataArray[$domainEntity->getPrimaryFieldName()]);
		
		return $this->query->add(
			$dataArray,
			$table
		);
	}
	
	/**
	 * update
	 * 
	 * @param \Packages\Core\DomainObject\AbstractEntity $domainEntity
	 * @param null|string $table
	 * @return boolean
	 */
	public function update(\Packages\Core\DomainObject\AbstractEntity &$domainEntity, $table = NULL) {
		if ($domainEntity->_isNew()) {
			throw new \InvalidArgumentException('The domainEntity "(' . \get_class($domainEntity) . ')" is new.', 1446218087);
		}
		
		$domainEntity->_memorizeModifiedState();
		
		$dataArray = $this->createDataArrayForUpdateFromEntity($domainEntity);
		unset($dataArray[$domainEntity->getPrimaryFieldName()]);
		
		if (\count($dataArray) > 0) {
			$result = $this->query->update(
				(int) $domainEntity->_getProperty($domainEntity->getPrimaryFieldName()),
				$dataArray,
				$domainEntity->getPrimaryFieldName(),
				$table
			);
		} else {
			$result = false;
		}
		
		// reset modified and cleanProperties
		$domainEntity->_resetModifiedProperties();
		$domainEntity->_resetCleanProperties();
		
		return $result;
	}
	
	/**
	 * delete
	 * 
	 * @param integer $primaryFieldValue
	 * @param null|string $primaryField
	 * @param null|string $table
	 * @return boolean
	 */
	public function delete(\Packages\Core\DomainObject\AbstractEntity $domainEntity, $table = NULL) {
		return $this->query->delete(
			(int) $domainEntity->_getProperty($domainEntity->getPrimaryFieldName()),
			$domainEntity->getPrimaryFieldName(),
			$table
		);
	}
	
	/**
	 * executeQuery
	 * 
	 * @param string $query
	 * @return mixed
	 */
	public function executeQuery($query) {
		return $this->query->executeQuery($query);
	}
	
	/**
	 * quoteString
	 * 
	 * @param mixed $string
	 * @return string
	 */
	public function quoteString($string) {
		return $this->databaseHandle->quoteString($string);
	}
	
	/**
	 * _setProperty
	 * 
	 * @param string $propertyName
	 * @param mixed $propertyValue
	 * @return boolean
	 */
	public function _setProperty($propertyName, $propertyValue) {
		if ($this->_hasProperty($propertyName)) {
			$this->{$propertyName} = $propertyValue;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * _hasProperty
	 * 
	 * @param string $propertyName
	 * @return boolean
	 */
	public function _hasProperty($propertyName) {
		return \property_exists($this, $propertyName);
	}
	
	
	/**
	 * createDataArrayFromEntity
	 * 
	 * @param \Packages\Core\DomainObject\AbstractEntity $domainEntity
	 * @return array
	 */
	protected function createDataArrayFromEntity(\Packages\Core\DomainObject\AbstractEntity $domainEntity) {
		$resultDataArray = array();
		
		$reflection = new ClassReflection(\get_class($domainEntity));
		$reflectionClassPropertiesDataArray = $reflection->getProperties(\ReflectionProperty::IS_PROTECTED);
		
		foreach ($reflectionClassPropertiesDataArray as $reflectionPropertie) {
			/* @var $reflectionPropertie \Packages\Core\Reflection\PropertyReflection */
			
			$typeProperties = $reflectionPropertie->getTagsValues();
			if (\array_key_exists('relations', $typeProperties)) {
				continue;
			}
			$type = TypeHandlingUtility::parseType(\current($typeProperties['var']));
			
			if (\is_null($type['elementType'])) {
				/**
				 * elementType erstmal ignorieren
				 * wird sp�ter bei relationen verwendet
				 */
				$propertyValue = $this->getValueByType(
					$domainEntity->_getProperty($reflectionPropertie->getName()),
					$type['type']
				);
				
				if (\strlen($propertyValue)) {
					$resultDataArray[$reflectionPropertie->getName()] = array(
						'value' => $propertyValue,
						'dataType' => TypeMatchingUtility::getPdoType($type['type'])
					);
				}
			}
		}
		
		return $resultDataArray;
	}
	
	/**
	 * createDataArrayForUpdateFromEntity
	 * 
	 * @param \Packages\Core\DomainObject\AbstractEntity $domainEntity
	 * @return array
	 */
	protected function createDataArrayForUpdateFromEntity(\Packages\Core\DomainObject\AbstractEntity $domainEntity) {
		$resultDataArray = array();
		
		$reflection = new ClassReflection(\get_class($domainEntity));
		$reflectionClassPropertiesDataArray = $reflection->getProperties(\ReflectionProperty::IS_PROTECTED);
		
		foreach ($reflectionClassPropertiesDataArray as $reflectionPropertie) {
			/* @var $reflectionPropertie \Packages\Core\Reflection\PropertyReflection */
			
			$typeProperties = $reflectionPropertie->getTagsValues();
			if (\array_key_exists('relations', $typeProperties)) {
				continue;
			}
			$type = TypeHandlingUtility::parseType(\current($typeProperties['var']));
			
			if (\is_null($type['elementType'])) {
				/**
				 * elementType erstmal ignorieren
				 * wird sp�ter bei relationen verwendet
				 */
				$value = $domainEntity->_getModifiedProperty($reflectionPropertie->getName());
				if (!\is_null($value)) {
					$propertyValue = $this->getValueByType(
						$value,
						$type['type']
					);

					if (\strlen($propertyValue)) {
						$resultDataArray[$reflectionPropertie->getName()] = array(
							'value' => $propertyValue,
							'dataType' => TypeMatchingUtility::getPdoType($type['type'])
						);
					}
				}
			}
		}
		
		return $resultDataArray;
	}
	
	/**
	 * getValueByType
	 * 
	 * @param mixed $result
	 * @param string $type
	 * @return string
	 */
	protected function getValueByType($result, $type) {
		switch ($type) {
			case 'integer':
				$propertyValue = (int) $result;
				break;
			
			case 'boolean':
				$propertyValue = (bool) $result;
				break;
			
			case 'float':
				$propertyValue = (double) $result;
				break;
			
			case 'string':
				$propertyValue = (string) $result;
				break;
			
			case 'DateTime':
				if (\gettype($result) === 'object' 
					&& \get_class($result) === 'DateTime'
				) {
					$propertyValue = $result->format('Y-m-d H:i:s');
				} else {
					$propertyValue = NULL;
				}
				break;
			
			default:
				$propertyValue = $result;
				break;
		}
		
		return $propertyValue;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setDatabaseHandle(InterfaceConnection $databaseHandle) {
	   $this->databaseHandle = $databaseHandle;
	}
	
	public function setTable($table) {
		$this->table = $table;
	}
	
	public function setPrimaryField($primaryField) {
		$this->primaryField = $primaryField;
	}
	
	public function setFetchClass($fetchClass) {
		$this->fetchClass = $fetchClass;
	}
	
	public function setUseMemoryUsage($useMemoryUsage) {
		if ((int) $useMemoryUsage === 1) {
			$useMemoryUsage = true;
		} else {
			$useMemoryUsage = false;
		}
		
		$this->useMemoryUsage = $useMemoryUsage;
	}

}
