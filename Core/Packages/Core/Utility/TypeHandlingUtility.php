<?php
namespace Packages\Core\Utility;


/**
 * Description of TypeHandlingUtility
 *
 * @author Cristian.Reus
 */
class TypeHandlingUtility {

	const PARSE_TYPE_PATTERN = '/^\\\\?(?P<type>integer|int|float|double|boolean|bool|string|DateTime|object|array|ArrayObject|SplObjectStorage|Packages\\\\Core\\\\Persistence\\\\ObjectStorage)(?:<\\\\?(?P<elementType>[a-zA-Z0-9\\\\_]+)>)?/';
	const LITERAL_TYPE_PATTERN = '/^(?:integer|int|float|double|boolean|bool|string)$/';
	
	/**
	 * collectionTypes
	 * 
	 * @var array
	 */
	static protected $collectionTypes = array('array', 'ArrayObject', 'SplObjectStorage', 'Packages\\Core\\Persistence\\ObjectStorage');
	
	
	
	/**
	 * parseType
	 *
	 * @param string $type
	 * @return array
	 * @throws \InvalidArgumentException
	 */
	public static function parseType($type) {
		$matches = array();
		
		if (\preg_match(self::PARSE_TYPE_PATTERN, $type, $matches)) {
			$type = self::normalizeType($matches['type']);
			$elementType = isset($matches['elementType']) 
				? self::normalizeType($matches['elementType']) 
				: NULL
			;

			if (!\is_null($elementType) 
				&& !self::isCollectionType($type)
			) {
				throw new \InvalidArgumentException('Found an invalid element type declaration in %s. Type "' . $type . '" must not have an element type hint (' . $elementType . ').', 1446202244);
			}

			return array(
				'type' => $type,
				'elementType' => $elementType
			);
		} else {
			throw new \InvalidArgumentException('Found an invalid element type declaration in %s. A type "' . \var_export($type, TRUE) . '" does not exist.', 1446202282);
		}
	}

	/**
	 * normalizeType
	 *
	 * @param string $type
	 * @return string
	 */
	public static function normalizeType($type) {
		switch ($type) {
			case 'int':
				$type = 'integer';
				break;
			
			case 'bool':
				$type = 'boolean';
				break;
			
			case 'double':
				$type = 'float';
				break;
		}
		
		return $type;
	}

	/**
	 * isLiteral
	 *
	 * @param string $type
	 * @return boolean
	 */
	public static function isLiteral($type) {
		return \preg_match(self::LITERAL_TYPE_PATTERN, $type) === 1;
	}

	/**
	 * isSimpleType
	 *
	 * @param string $type
	 * @return boolean
	 */
	public static function isSimpleType($type) {
		return \in_array(
			self::normalizeType($type),
			array('array', 'string', 'float', 'integer', 'boolean'),
			TRUE
		);
	}

	/**
	 * isCollectionType
	 *
	 * @param string $type
	 * @return boolean
	 */
	public static function isCollectionType($type) {
		if (\in_array($type, self::$collectionTypes, TRUE)) {
			return TRUE;
		}

		if (\class_exists($type) === TRUE 
			|| \interface_exists($type) === TRUE
		) {
			foreach (self::$collectionTypes as $collectionType) {
				if (\is_subclass_of($type, $collectionType) === TRUE) {
					return TRUE;
				}
			}
		}

		return FALSE;
	}
}
