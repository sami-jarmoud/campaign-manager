<?php
namespace Packages\Core\Utility\Html;

use Packages\Core\Utility\Html\AbstractUtility;


/**
 * Description of TableUtility
 *
 * @author Cristian.Reus
 */
final class TableUtility extends AbstractUtility {
	
	/**
	 * createTable
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	public static function createTable(array $configDataArray) {
		return array(
			'begin' => '<table ' . self::processConfigDataArray($configDataArray) . '>' . \chr(13),
			'end' => '</table>'
		);
	}

	/**
	 * createTableHeaderRow
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	public static function createTableHeaderRow(array $configDataArray = array()) {
		$tHeadDataArray = self::createTHeadTag();
		$tmpDataArray = self::createTableRow($configDataArray);

		$resultDataArray = array(
			'begin' => $tHeadDataArray['begin'] . $tmpDataArray['begin'],
			'end' => $tmpDataArray['end'] . $tHeadDataArray['end']
		);
		unset($tmpDataArray);

		return $resultDataArray;
	}

	/**
	 * createTHeadTag
	 * 
	 * @return array
	 */
	public static function createTHeadTag() {
		return array(
			'begin' => '<thead>' . \chr(13),
			'end' => '</thead>' . \chr(13)
		);
	}
	
	/**
	 * createTFootTag
	 * 
	 * @return array
	 */
	public static function createTFootTag() {
		return array(
			'begin' => '<tfoot>' . \chr(13),
			'end' => '</tfoot>' . \chr(13)
		);
	}
	
	/**
	 * createTBodyTag
	 * 
	 * @return array
	 */
	public static function createTBodyTag() {
		return array(
			'begin' => '<tbody>' . \chr(13),
			'end' => '</tbody>' . \chr(13)
		);
	}

	/**
	 * createTableFooterRow
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	public static function createTableFooterRow(array $configDataArray = array()) {
		$tmpDataArray = self::createTableRow($configDataArray);

		$tHeadDataArray = self::createTFootTag();

		$resultDataArray = array(
			'begin' => $tHeadDataArray['begin'] . $tmpDataArray['begin'],
			'end' => $tmpDataArray['end'] . $tHeadDataArray['end']
		);
		unset($tmpDataArray);

		return $resultDataArray;
	}

	/**
	 * createTableRow
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	public static function createTableRow(array $configDataArray = array()) {
		$resultDataArray = array(
			'begin' => '<tr ',
			'end' => '</tr>' . \chr(13)
		);

		if (\count($configDataArray) > 0) {
			$resultDataArray['begin'] .= self::processConfigDataArray($configDataArray);
		}

		$resultDataArray['begin'] .= '>' . \chr(13);

		return $resultDataArray;
	}

	/**
	 * createTableHeaderCellWidthContent
	 * 
	 * @param array $configDataArray
	 * @param string $item
	 * @return string
	 */
	public static function createTableHeaderCellWidthContent(array $configDataArray, $item) {
		return '<th ' . self::processConfigDataArray($configDataArray) . '>' . $item . '</th>' . \chr(13);
	}

	/**
	 * createTableCellWidthContent
	 * 
	 * @param array $configDataArray
	 * @param string $item
	 * @return string
	 */
	public static function createTableCellWidthContent(array $configDataArray, $item) {
		return '<td ' . self::processConfigDataArray($configDataArray) . '>' . $item . '</td>' . \chr(13);
	}

}