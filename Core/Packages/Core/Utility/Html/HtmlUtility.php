<?php
namespace Packages\Core\Utility\Html;

use Packages\Core\Utility\Html\AbstractUtility;


/**
 * Description of HtmlUtility
 *
 * @author Cristian.Reus
 */
final class HtmlUtility extends AbstractUtility {
	
	/**
	 * createHyperlink
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @return string
	 */
	public static function createHyperlink(array $configDataArray, $title) {
		$tmpDataArray = self::createHtmlElement(
			$configDataArray,
			'a'
		);
		
		return $tmpDataArray['begin'] . $title . $tmpDataArray['end'];
	}
	
	/**
	 * createDiv
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	public static function createDiv(array $configDataArray = array()) {
		return self::createHtmlElement(
			$configDataArray,
			'div'
		);
	}
	
	/**
	 * createHtmlElement
	 * 
	 * @param array $configDataArray
	 * @param string $elementType
	 * @return array
	 */
	protected static function createHtmlElement(array $configDataArray, $elementType) {
		return array(
			'begin' => '<' . $elementType . ' ' . self::processConfigDataArray($configDataArray) . '>' . \chr(13),
			'end' => '</' . $elementType . '>'
		);
	}

}