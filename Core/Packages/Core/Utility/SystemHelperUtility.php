<?php
namespace Packages\Core\Utility;

use Packages\Core\Utility\Html\FormUtility;


/**
 * Description of SystemHelperUtility
 *
 * @author Sami Jarmoud
 */
class SystemHelperUtility {
	
	/**
	 * getDeliverySystemsItemsOptionList
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $clientDeliveriesData
	 * @param string $selectedItem
	 * @return string
	 */
	static public function getDeliverySystemsItemsOptionList(\Packages\Core\Persistence\ObjectStorage $clientDeliveriesData, $selectedItem) {
		$result = '';
		
		foreach ($clientDeliveriesData as $clientDeliveryEntity) {
			/* @var $clientDeliveryEntity \Packages\Core\Domain\Model\ClientDeliveryEntity */
			
			$selected = '';
			if ($clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz() === $selectedItem) {
				$selected = 'selected';
			}
			#if( ($clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz() !== 'BM')
                                #&& ($clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz() !== 'K')
                                #){
			$result .= FormUtility::createOptionItem(
				array(
					'value' => $clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz(),
					'selected' => $selected
				),
				$clientDeliveryEntity->getDeliverySystemEntity()->getAsp()
			);
                       #}
		}
		
		return $result;
	}
        
        /**
	 * getDeliverySystemsItemsDomainOptionList
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $domaineDeliverySystemsDataArray
	 * @param integer $selectedItem
	 * @return string
	 */
	static public function getDeliverySystemsItemsDomainOptionList(\Packages\Core\Persistence\ArrayIterator $domaineDeliverySystemsDataArray, $selectedItem) {
		$result = '';
		
		foreach ($domaineDeliverySystemsDataArray as $domaineDeliveryEntity) {
			/* @var $domaineDeliveryEntity \Packages\Core\Domain\Model\DomainDeliveryEntity */
			
			$selected = '';
			if ((int)$domaineDeliveryEntity->getDasp_id() === (int)$selectedItem) {
				$selected = 'selected';
			}
			#if( ($clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz() !== 'BM')
                                #&& ($clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz() !== 'K')
                                #){
			$result .= FormUtility::createOptionItem(
				array(
					'value' => $domaineDeliveryEntity->getDasp_id(),
					'selected' => $selected
				),
				$domaineDeliveryEntity->getDomain_name()
			);
                       #}
		}
		
		return $result;
	}
        
        
	/**
	 * getDeliverySystemsItemsByIdOptionList
	 *
	 * @param \Packages\Core\Persistence\ObjectStorage $clientDeliveriesData
	 * @param string $selectedItem
	 * @return integer 
	 */
	static public function getDeliverySystemsItemsByIdOptionList(\Packages\Core\Persistence\ObjectStorage $clientDeliveriesData, $selectedItem) {
		$result = '';
	
		foreach ($clientDeliveriesData as $clientDeliveryEntity) {
			/* @var $clientDeliveryEntity \Packages\Core\Domain\Model\ClientDeliveryEntity */
				
			$selected = '';
			if ($clientDeliveryEntity->getDeliverySystemEntity()->getId() === $selectedItem) {
				$selected = 'selected';
			}
				
			$result .= FormUtility::createOptionItem(
					array(
							'value' => $clientDeliveryEntity->getDeliverySystemEntity()->getAsp_abkz(),
							'selected' => $selected
					),
					$clientDeliveryEntity->getDeliverySystemEntity()->getAsp()
					);
		}
	
		return $result;
	}
	/**
	 * getUserEntityFromDataArray
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $userDataArray
	 * @param integer $userId
	 * @return NULL|\Packages\Core\Domain\Model\UserEntity
	 */
	static public function getUserEntityFromDataArray(\Packages\Core\Persistence\ArrayIterator $userDataArray, $userId) {
		$result = NULL;
		
		foreach ($userDataArray as $userEntity) {
			/* @var $userEntity \Packages\Core\Domain\Model\UserEntity */
			if ($userEntity->getBenutzer_id() === $userId) {
				$result = $userEntity;
				
				break;
			}
		}
		
		return $result;
	}
	
	/**
	 * getUploadDirForHyperlinks
	 * 
	 * @param string $uploadDir
	 * @return string
	 */
	static public function getUploadDirForHyperlinks($uploadDir) {
		return $_SERVER['HTTP_X_FORWARDED_PROTO'] . '://' .  $_SERVER['HTTP_HOST'] . \DIRECTORY_SEPARATOR . \str_replace($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR, '', $uploadDir);
	}
        
        
          /**
	 * getDeliverySystemDistributorsItems
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray
	 * @param int $dsd_id
	 * @param boolean $disableNotSelectedClientId
	 * @param boolean $selectDefaultValue
	 * @return string
	 */
	public static function getDeliverySystemDistributorsItems(\Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray, $dsd_id, $disableNotSelectedClientId = false, $selectDefaultValue = false) {
		$result = '';
		
		foreach ($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
			/* @var $deliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
			
			$selected = $disabled = '';
			if ($deliverySystemDistributorEntity->getId() === $dsd_id) {
				$selected = 'selected';
			} elseif ($selectDefaultValue 
				&& $deliverySystemDistributorEntity->getSet_as_default()
			) {
				$selected = 'selected';
			}
			
			/*if ($disableNotSelectedClientId 
				&& $deliverySystemDistributorEntity->getM_asp_id() !== $dsd_id
			) {
				$disabled = 'disabled';
			}*/
			
			if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributorEntity->getParent_id()) {
				$optgroupDataArray = FormUtility::createOptgroupDataArray(
					self::getParentClientDeliverySystemDistributorTitle($deliverySystemDistributorEntity->getTitle())
				);
				
				$result .= FormUtility::openSelectOptgroup(
					$key,
					$optgroupDataArray
				);
			}
		
			$result .= FormUtility::createOptionItem(
				array(
					'value' => $deliverySystemDistributorEntity->getId(),
					'selected' => $selected,
					'disabled' => $disabled
				),
				$deliverySystemDistributorEntity->getTitle()
			);
			
			if (\count($deliverySystemDistributorDataArray) === $key) {
				// closeSelectOptgroup
				$result .= FormUtility::closeSelectOptgroup($optgroupDataArray);
				unset($optgroupDataArray);
			}
		}
		
		return $result;
	}
}
