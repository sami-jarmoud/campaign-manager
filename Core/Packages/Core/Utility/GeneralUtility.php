<?php
namespace Packages\Core\Utility;


/**
 * Description of GeneralUtility
 *
 * @author Cristian.Reus
 */
final class GeneralUtility {
	
	/**
	 * namespaceDivider
	 * 
	 * @var string
	 */
	public static $namespaceDivider = '\\';
	
	/**
	 * userId
	 * 
	 * @var integer
	 */
	public static $loggedUserId = 0;
	
	/**
	 * debugUserId
	 * 
	 * @var integer
	 */
	protected static $debugUserId = 2;
	
	/**
	 * devIpAddress
	 * 
	 * @var string
	 */
	protected static $devIpAddress = '87.138.158.223';
	
	/**
	 * finalClassNameCache
	 * 
	 * @var array
	 */
	protected static $finalClassNameCache = array();
	
	/**
	 * singletonInstances
	 * 
	 * @var array
	 */
	protected static $singletonInstances = array();
	
	/**
	 * nonSingletonInstances
	 * 
	 * @var array
	 */
	protected static $nonSingletonInstances = array();
	
	
	
	/**
	 * isProdSystem
	 * 
	 * @return boolean
	 */
	public static function isProdSystem() {
		return (self::getSystemEnvironmentType() === 'Production');
	}
	
	/**
	 * isTestSystem
	 * 
	 * @return boolean
	 */
	public static function isTestSystem() {
		return (self::getSystemEnvironmentType() === 'Testing');
	}
	
	/**
	 * isDevSystem
	 * 
	 * @return boolean
	 */
	public static function isDevSystem() {
		return (self::getSystemEnvironmentType() === 'Development');
	}
	
	/**
	 * getUserIp
	 * 
	 * @return string
	 */
	public static function getUserIp() {
		if (\getenv('HTTP_X_FORWARDED_FOR')) {
			$result = \getenv('HTTP_X_FORWARDED_FOR');
		} else {
			$result = \getenv('REMOTE_ADDR');
		}
		
		return $result;
    }
	
	/**
	 * makeInstance
	 * 
	 * @param string $className
	 * @return object
	 * @throws \InvalidArgumentException
	 */
	public static function makeInstance($className) {
		if (!\is_string($className) 
			|| empty($className)
		) {
			throw new \InvalidArgumentException('className must be a non empty string.', 1446627877);
		}

		if (isset(static::$finalClassNameCache[$className])) {
			$finalClassName = static::$finalClassNameCache[$className];
		} else {
			$finalClassName = self::getClassName($className);
			
			static::$finalClassNameCache[$className] = $finalClassName;
		}

		// Return singleton instance if it is already registered
		if (isset(self::$singletonInstances[$finalClassName])) {
			return self::$singletonInstances[$finalClassName];
		}
		
		// Return instance if it has been injected by addInstance()
		if (isset(self::$nonSingletonInstances[$finalClassName]) 
			&& !empty(self::$nonSingletonInstances[$finalClassName])
		) {
			return \array_shift(self::$nonSingletonInstances[$finalClassName]);
		}
		
		// Create new instance and call constructor with parameters
		$instance = static::instantiateClass($finalClassName, \func_get_args());
		
		// Register new singleton instance
		if ($instance instanceof \Packages\Core\InterfaceSingleton) {
			self::$singletonInstances[$finalClassName] = $instance;
		}
		
		return $instance;
	}
	
	/**
	 * getCorrectFileName
	 * 
	 * @param string $fileName
	 * @return string
	 */
	public static function getCorrectFileName($fileName) {
		return \str_replace(static::$namespaceDivider, \DIRECTORY_SEPARATOR, $fileName);
	}

	/**
	 * getSystemEnvironmentType
	 * 
	 * @return string
	 */
	protected static function getSystemEnvironmentType() {
		return \getenv('MAAS_CONTEXT');
	}
	
	/**
	 * getClassName
	 * 
	 * @param string $className
	 * @return string
	 */
	protected static function getClassName($className) {
		if (\class_exists($className)) {
			while (static::classHasImplementation($className)) {
				$className = static::getImplementationForClass($className);
			}
		}
		
		return \Packages\Core\ClassLoader::getClassNameForAlias($className);
	}
	
	/**
	 * classHasImplementation
	 * 
	 * @param string $className
	 * @return boolean
	 */
	protected static function classHasImplementation($className) {
		// If we are early in the bootstrap, the configuration might not yet be present
		if (!isset($GLOBALS['MAAS_CONF_VARS']['SYS']['Objects'])) {
			return FALSE;
		}
		
		return isset($GLOBALS['MAAS_CONF_VARS']['SYS']['Objects'][$className]) 
			&& \is_array($GLOBALS['MAAS_CONF_VARS']['SYS']['Objects'][$className]) 
			&& !empty($GLOBALS['MAAS_CONF_VARS']['SYS']['Objects'][$className]['className'])
		;
	}
	
	/**
	 * getImplementationForClass
	 * 
	 * @param string $className
	 * @return string
	 */
	protected static function getImplementationForClass($className) {
		return $GLOBALS['MAAS_CONF_VARS']['SYS']['Objects'][$className]['className'];
	}
	
	/**
	 * instantiateClass
	 * 
	 * @param string $className
	 * @param array $arguments
	 * @return object
	 */
	protected static function instantiateClass($className, array $arguments) {
		if (\count($arguments) > 1) {
			$class = new \ReflectionClass($className);
			\array_shift($arguments);
			
			$instance = $class->newInstanceArgs($arguments);
		} else {
			$instance = new $className();
		}
		
		return $instance;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public static function getDebugUserId() {
		return (int) self::$debugUserId;
	}
	
	public static function getDevIpAddress() {
		return self::$devIpAddress;
	}

}