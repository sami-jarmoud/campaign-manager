<?php
namespace Packages\Core\Utility\Format;


/**
 * Description of OthersUtility
 *
 * @author Cristian.Reus
 */
class OthersUtility {
	
	/**
	 * cutString
	 * 
	 * @param string $text
	 * @param integer $length
	 * @return string
	 */
	public static function cutString($text, $length) {
		if (\strlen($text) > $length) {
			$cutText = \substr($text, 0, $length);
			$cutText = $cutText . '...';
		} else {
			$cutText = $text;
		}

		return $cutText;
	}

	/**
	 * cleanUpNotes
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function cleanUpNotes($string) {
		return \str_replace(
			array(
				'&#10;',
				'&#13;'
			),
			array(
				\chr(13),
				\chr(13)
			),
			$string
		);
	}

	/**
	 * nl2Br
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function nl2Br($string) {
		return \str_replace(
			array(
				\chr(13),
				\chr(10)
			),
			array(
				'<br />',
				'<br />'
			),
			\trim($string)
		);
	}
	
	/**
	 * cleanupToAndString
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function cleanupToAndString($string) {
		return \str_replace(
			array(
				'&amp;',
				'&#38;'
			),
			'&',
			$string
		);
	}
	
	/**
	 * cleanupStringForDownload
	 * 
	 * @param string $string
	 * @return string
	 */
	public static function cleanupStringForDownload($string) {
		return \preg_replace('/[^a-zA-Z0-9äöüß_]/u', '_', $string);
	}
}
