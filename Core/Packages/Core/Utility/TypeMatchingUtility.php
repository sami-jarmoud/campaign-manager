<?php
namespace Packages\Core\Utility;


/**
 * Description of TypeMatchingUtility
 *
 * @author Cristian.Reus
 */
class TypeMatchingUtility {

	/**
	 * getPdoType
	 * 
	 * @param string $type
	 * @return integer
	 */
	public static function getPdoType($type) {
		switch ($type) {
			case 'boolean':
				$result = \PDO::PARAM_BOOL;
				break;
			
			case 'null':
				$result = \PDO::PARAM_NULL;
				break;
			
			case 'integer':
				$result = \PDO::PARAM_INT;
				break;
			
			default:
				$result = \PDO::PARAM_STR;
				break;
		}
		
		return $result;
	}
}
