<?php
namespace Packages\Core\Utility;


/**
 * Description of MailUtility
 *
 * @author Cristian.Reus
 */
class MailUtility {
	
	/**
	 * devEmailAddress
	 * 
	 * @var string
	 */
	public static $devEmailAddress = 'dev-maas@treaction.de';
	
	/**
	 * technikContactDataArray
	 * 
	 * @var array
	 */
	public static $technikContactDataArray = array(
           'technik-tcm@treaction.de' => 'Technik Team'
            );
 	
	/**
	 * $technikContactAdress
	 * 
	 * @var string
	 */
	public static $technikContactAdress = 'technik-tcm@treaction.de' ;
	
        
	/**
	 * subject
	 * 
	 * @var string
	 */
	public static $subject = 'MaaS API Error';
	
	/**
	 * headerDataArray
	 * 
	 * @var array
	 */
	protected static $headerDataArray = array(
		'MIME-Version: 1.0',
		'Content-type: text/plain; charset=iso-8859-1',
		'From: Treaction MaaS Mailer <api@maas.treaction.de>'
	);
	
         /**
	 * sendMailToSingle
	 * 
	 * @param string $message
	 * @return void
	 */
	public static function sendMailToSingle($to, $subject, $message) {
		\mail(
			$to,
			$subject,
			$message,
			self::createHeaderData()
		);
	}	
	
	
	
	/**
	 * sendMail
	 * 
	 * @param string $message
	 * @return void
	 */
	public static function sendMail($message) {
		\mail(
			self::$devEmailAddress,
			self::$subject,
			$message,
			self::createHeaderData()
		);
	}
	
	
	/**
	 * sendMailToTcm
	 * 
	 * @param string $subject
         * @param string $message
	 * @return void
	 */
	public static function sendMailToTcm($subject, $message) {
		\mail(
                        self::$technikContactAdress,
			$subject,
			$message,
			self::createHeaderData()
		);
	}
        
	/**
	 * createHeaderData
	 * 
	 * @return string
	 */
	protected static function createHeaderData() {
		$headerDataArray = \array_merge(
			self::$headerDataArray,
			array(
				'X-Mailer: PHP/' . \phpversion()
			)
		);
		
		return \implode(\chr(10), $headerDataArray);
	}
        
}
