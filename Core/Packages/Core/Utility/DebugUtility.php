<?php
namespace Packages\Core\Utility;

use Packages\Core\Utility\GeneralUtility;
use Packages\Core\Utility\FilterUtility;

/**
 * Description of DebugUtility
 *
 * @author Cristian.Reus
 */
class DebugUtility {
	/**
	 * debug
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public static function debug($data, $header = '') {
		$header = ($header ? $header : __METHOD__);
		
		if (GeneralUtility::$loggedUserId === GeneralUtility::getDebugUserId()) {
			if (\is_array($data) 
				|| \is_object($data)
			) {
				echo $header;
				echo '<pre>';
				\print_r($data);
				echo '</pre>' . \nl2br(\chr(13));
			} else {
				echo \nl2br(
					$header . ': '
					. FilterUtility::filterData($data) . \chr(13)
				);
			}
		}
	}

}
