<?php
namespace Packages\Core\Log;


/**
 * Description of InterfaceLog
 *
 * @author Cristian.Reus
 */
interface InterfaceLog {
	/**
	 * log
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function log($data, $header);
	
	/**
	 * info
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function info($data, $header);
	
	/**
	 * warning
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function warning($data, $header);
	
	/**
	 * error
	 * 
	 * @param mixed $data
	 * @param string $header
	 * @return void
	 */
	public function error($data, $header);

	/**
	 * beginGroup
	 * 
	 * @param string $title
	 * @return void
	 */
	public function beginGroup($title);

	/**
	 * endGroup
	 * 
	 * @return void
	 */
	public function endGroup();
}