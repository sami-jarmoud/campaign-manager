<?php
namespace Packages\Core\DomainObject;

use Packages\Core\DomainObject\AbstractObject;

/**
 * AbstractDomainObject
 */
abstract class AbstractDomainObject extends AbstractObject {
	
	/**
	 * uid
	 * 
	 * @var integer
	 */
	protected $uid;
	
	
	
	public function __construct($primaryFieldName = 'uid') {
		$this->setPrimaryFieldName($primaryFieldName);
	}
	
	/**
	 * _isNew
	 * 
	 * @return boolean
	 */
	public function _isNew() {
		return $this->uid === NULL;
	}
	
	/**
	 * __toString
	 * 
	 * @return string
	 */
	public function __toString() {
		return \get_class($this) . ':' . (string) $this->uid;
	}

}