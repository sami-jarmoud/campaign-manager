<?php
namespace Packages\Core\DomainObject;

use Packages\Core\DomainObject\AbstractObject;

/**
 * AbstractEntity
 */
abstract class AbstractEntity extends AbstractObject {
	
	public function __construct($primaryFieldName = 'id') {
		$this->setPrimaryFieldName($primaryFieldName);
	}
}