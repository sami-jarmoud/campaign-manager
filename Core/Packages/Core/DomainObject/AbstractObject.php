<?php
namespace Packages\Core\DomainObject;

use Packages\Core\DomainObject\InterfaceDomainObject;


/**
 * AbstractObject
 */
abstract class AbstractObject implements InterfaceDomainObject {
	
	/**
	 * primaryFieldName
	 * 
	 * @var string
	 */
	private $primaryFieldName = NULL;
	
	/**
	 * _cleanProperties
	 * 
	 * @var array
	 */
	private $_cleanProperties = array();
	
	/**
	 * _modifiedProperties
	 * 
	 * @var array
	 */
	private $_modifiedProperties = array();
	
	/**
	 * _isClone
	 * 
	 * @var boolean 
	 */
	private $_isClone = FALSE;
	
	
	
	/**
	 * _setProperty
	 * 
	 * @param string $propertyName
	 * @param mixed $propertyValue
	 * @return boolean
	 */
	public function _setProperty($propertyName, $propertyValue) {
		if ($this->_hasProperty($propertyName)) {
			$this->{$propertyName} = $propertyValue;
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * _getProperty
	 * 
	 * @param string $propertyName
	 * @return mixed
	 */
	public function _getProperty($propertyName) {
		return $this->{$propertyName};
	}
	
	/**
	 * _getProperties
	 * 
	 * @return array
	 */
	public function _getProperties() {
		$properties = \get_object_vars($this);
		
		foreach ($properties as $propertyName => $propertyValue) {
			if ($propertyName[0] === '_') {
				unset($properties[$propertyName]);
			}
		}
		return $properties;
	}
	
	/**
	 * _hasProperty
	 * 
	 * @param string $propertyName
	 * @return boolean
	 */
	public function _hasProperty($propertyName) {
		return \property_exists($this, $propertyName);
	}
	
	/**
	 * _isNew
	 * 
	 * @return boolean
	 */
	public function _isNew() {
		return $this->{$this->primaryFieldName} === NULL;
	}
	
	/**
	 * _memorizeCleanState
	 * 
	 * @param NULL|string $propertyName
	 * @return void
	 */
	public function _memorizeCleanState($propertyName = NULL) {
		if (!\is_null($propertyName)) {
			$this->_memorizePropertyCleanState($propertyName);
		} else {
			$this->_resetCleanProperties();
			
			$properties = \get_object_vars($this);
			foreach ($properties as $propertyName => $propertyValue) {
				if ($propertyName[0] === '_') {
					continue;
				}
				
				$this->_memorizePropertyCleanState($propertyName);
			}
		}
	}
	
	/**
	 * _memorizePropertyCleanState
	 * 
	 * @param string $propertyName
	 * @return void
	 */
	public function _memorizePropertyCleanState($propertyName) {
		$propertyValue = $this->{$propertyName};
		if (\is_object($propertyValue)) {
			$this->_cleanProperties[$propertyName] = clone $propertyValue;
			
			if ($propertyValue instanceof \Packages\Core\DomainObject\AbstractObject) {
				$this->_cleanProperties[$propertyName]->_setClone(FALSE);
			}
		} else {
			$this->_cleanProperties[$propertyName] = $propertyValue;
		}
	}
	
	/**
	 * _resetCleanProperties
	 * 
	 * @return void
	 */
	public function _resetCleanProperties() {
		$this->_cleanProperties = array();
	}
	
	/**
	 * _getCleanProperties
	 * 
	 * @return array
	 */
	public function _getCleanProperties() {
		return $this->_cleanProperties;
	}
	
	/**
	 * _getCleanProperty
	 * 
	 * @param string $propertyName
	 * @return mixed|NULL
	 */
	public function _getCleanProperty($propertyName) {
		return isset($this->_cleanProperties[$propertyName]) ? $this->_cleanProperties[$propertyName] : NULL;
	}
	
	/**
	 * _memorizeModifiedState
	 * 
	 * @return void
	 */
	public function _memorizeModifiedState() {
		if ($this->_isDirty()) {
			// reset _modifiedProperties
			$this->_resetModifiedProperties();
			
			foreach ($this->_getCleanProperties() as $propertyName => $cleanPropertyValue) {
				if ($this->isPropertyDirty($cleanPropertyValue, $this->_getProperty($propertyName)) === TRUE) {
					$this->_modifiedProperties[$propertyName] = $this->_getProperty($propertyName);
				}
			}
		}
	}
	
	/**
	 * _getModifiedProperty
	 * 
	 * @param mixed $propertyName
	 * @return mixed|NULL
	 */
	public function _getModifiedProperty($propertyName) {
		return isset($this->_modifiedProperties[$propertyName]) ? $this->_modifiedProperties[$propertyName] : NULL;
	}
	
	/**
	 * _resetModifiedProperties
	 * 
	 * @return void
	 */
	public function _resetModifiedProperties() {
		$this->_modifiedProperties = array();
	}
	
	/**
	 * _isDirty
	 * 
	 * @param NULL|string $propertyName
	 * @return boolean
	 * @throws \InvalidArgumentException
	 */
	public function _isDirty($propertyName = NULL) {
		if (!\is_null($this->{$this->primaryFieldName}) 
			&& !\is_null($this->_getCleanProperty($this->primaryFieldName))
			&& $this->{$this->primaryFieldName} != $this->_getCleanProperty($this->primaryFieldName)
		) {
			throw new \InvalidArgumentException('The primaryField "' . $this->primaryFieldName . '" has been modified.', 1446735572);
		}
		
		if (\is_null($propertyName)) {
			foreach ($this->_getCleanProperties() as $propertyName => $cleanPropertyValue) {
				if ($this->isPropertyDirty($cleanPropertyValue, $this->_getProperty($propertyName)) === TRUE) {
					return TRUE;
				}
			}
		} else {
			if ($this->isPropertyDirty($this->_getCleanProperty($propertyName), $this->_getProperty($propertyName)) === TRUE) {
				return TRUE;
			}
		}
		
		return FALSE;
	}
	
	/**
	 * isPropertyDirty
	 * 
	 * @param mixed $previousValue
	 * @param mixed $currentValue
	 * @return boolean
	 */
	protected function isPropertyDirty($previousValue, $currentValue) {
		if (\is_object($currentValue)) {
			if ($currentValue instanceof InterfaceDomainObject) {
				/* @var $currentValue AbstractObject */
				/* @var $previousValue AbstractObject */
				
				$result = !\is_object($previousValue) || \get_class($previousValue) !== \get_class($currentValue) || $currentValue->_getProperty($currentValue->getPrimaryFieldName()) !== $previousValue->_getProperty($previousValue->getPrimaryFieldName());
			} elseif ($currentValue instanceof \Packages\Core\Persistence\ObjectStorage) {
				if ($currentValue->count() > 0) {
					foreach ($currentValue as $item) {
						if ($item instanceof InterfaceDomainObject) {
							/* @var $item AbstractObject */

							$result = self::isPropertyDirty(
								$item->_getCleanProperties(),
								$item->_getProperties()
							);
							if ($result === true) {
								break;
							}
						}
					}
				} else {
					// TODO: check
					$result = false;
				}
			} elseif ($currentValue instanceof \DateTime) {
				if (!($previousValue instanceof \DateTime)) {
					$result = new \DateTime($previousValue) != $currentValue;
				} else {
					$result = $previousValue != $currentValue;
				}
			} else {
				$result = $previousValue != $currentValue;
			}
		} elseif (\is_array($currentValue)) {
			// \Packages\Core\Persistence\ObjectStorage
			// TODO: besser l�sen
			$result = false;
			#$result = $previousValue === $currentValue;
		} else {
			switch (\gettype($currentValue)) {
				case 'integer':
					$result = (int) \trim($previousValue) !== (int) \trim($currentValue);
					break;
				
				case 'double':
					$result = (float) \trim($previousValue) !== (float) \trim($currentValue);
					break;
				
				default:
					// string
					$result = \trim($previousValue) !== \trim($currentValue);
					break;
			}
		}
		
		return $result;
	}
	
	/**
	 * _isClone
	 * 
	 * @return true
	 */
	public function _isClone() {
		return $this->_isClone;
	}
	
	/**
	 * _setClone
	 * 
	 * @param boolean $clone
	 * @return void
	 */
	public function _setClone($clone) {
		$this->_isClone = (boolean) $clone;
	}
	
	/**
	 * __clone
	 * 
	 * @return void
	 */
	public function __clone() {
		$this->_isClone = TRUE;
	}
	
	/**
	 * __toString
	 * 
	 * @return string
	 */
	public function __toString() {
		return \get_class($this) . ':' . (string) $this->{$this->primaryFieldName};
	}
	
	/**
	 * createDateTimeFromValueForField
	 * 
	 * @param string $field
	 * @param string $value
	 * @return void
	 */
	protected function createDateTimeFromValueForField($field, $value) {
		if ((
				!\is_null($value) 
				|| \strlen($value) > 0
			)
			&& ($value != '1970-01-01 02:00:00' 
				|| $value != '0000-00-00 00:00:00'
			)
			&& !($value instanceof \DateTime)
		) {
			$dateTime = new \DateTime($value);
			$dateTime->setTimezone(new \DateTimeZone('Europe/Berlin'));
			
			$this->_setProperty($field, $dateTime);
		} else {
			$this->_setProperty($field, $value);
		}
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function setPrimaryFieldName($primaryFieldName) {
		$this->primaryFieldName = $primaryFieldName;
	}
	public function getPrimaryFieldName() {
		return $this->primaryFieldName;
	}
	
	public function getUid() {
		if ($this->{$this->primaryFieldName} !== NULL) {
			return (int) $this->{$this->primaryFieldName};
		} else {
			return NULL;
		}
	}
        
	public function getUidForSE() {
		if ($this->{$this->primaryFieldName} !== NULL) {
			return  $this->{$this->primaryFieldName};
		} else {
			return NULL;
		}
	}        

}