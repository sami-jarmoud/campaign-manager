<?php
namespace Packages\Core\DomainObject;


/**
 * Description of InterfaceDomainObject
 *
 * @author Cristian.Reus
 */
interface InterfaceDomainObject {
	
	public function getUid();
}
