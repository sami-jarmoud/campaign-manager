<?php
namespace Packages\Core\Manager\Database;

/**
 * Description of InterfaceConnection
 *
 * @author Cristian.Reus
 */
interface InterfaceConnection {
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init();
	
	/**
	 * processFetchQueryByClass
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @param string $fetchClass
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 */
	public function processFetchQueryByClass($query, array $dataArray, $fetchClass);
	
	/**
	 * processFetchAllQueryByClass
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @param string $fetchClass
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function processFetchAllQueryByClass($query, array $dataArray, $fetchClass);
	
	/**
	 * processFetchColumn
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return integer
	 */
	public function processFetchColumn($query, array $dataArray);
	
	/**
	 * processSelectQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return boolean|array
	 */
	public function processSelectQuery($query, array $dataArray = array());
	
	/**
	 * insertQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return integer
	 */
	public function insertQuery($query, array $dataArray);
	
	/**
	 * updateQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return boolean
	 */
	public function updateQuery($query, array $dataArray);
	
	/**
	 * deleteQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return boolean
	 */
	public function deleteQuery($query, array $dataArray);
	
	/**
	 * executeQuery
	 * 
	 * @param string $query
	 * @return mixed
	 */
	public function executeQuery($query);
}
