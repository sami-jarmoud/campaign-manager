<?php
namespace Packages\Core\Manager\Database;

use Packages\Core\Manager\Database\AbstractConnection;
use Packages\Core\Persistence\Generic\AbstractQuery;
use Packages\Core\Utility\ExceptionUtility;

/**
 * Description of PdoConnection
 *
 * @author Cristian.Reus
 */
class PdoConnection extends AbstractConnection {
	
	/**
	 * connection
	 * 
	 * @var \PDO|NULL
	 */
	protected $connection = NULL;
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 * @throws \BadFunctionCallException
	 */
	public function init() {
		try {
			$this->connection = new \PDO(
				'mysql:host=' . $this->host . ';dbname=' . $this->dbName,
				$this->dbUser,
				$this->dbPassword,
				array(
					\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
				)
			);
			$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {
			throw new \BadFunctionCallException('connection Error!', 1445521370, $e);
		}
	}
	
	/**
	 * processFetchQueryByClass
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @param string $fetchClass
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 * @throws \DomainException
	 */
	public function processFetchQueryByClass($query, array $dataArray, $fetchClass) {
		try {
			$stmt = $this->executeQueryStmt(
				$query,
				$dataArray
			);
			
			$result = $this->fetchStmt(
				$fetchClass,
				$stmt
			);
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			throw new \DomainException(__METHOD__, 1445864351, $e);
		}
		
		return $result;
	}
	
	/**
	 * processFetchAllQueryByClass
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @param string $fetchClass
	 * @return \Packages\Core\Persistence\ArrayIterator
	 * @throws \DomainException
	 */
	public function processFetchAllQueryByClass($query, array $dataArray, $fetchClass) {
		try {
			$stmt = $this->executeQueryStmt(
				$query,
				$dataArray
			);
			
			$result = $this->fetchAllStmt(
				$fetchClass,
				$stmt
			);
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			throw new \DomainException(__METHOD__, 1445864379, $e);
		}
		
		return $result;
	}
	
	/**
	 * processFetchColumn
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return integer
	 * @throws \DomainException
	 */
	public function processFetchColumn($query, array $dataArray) {
		try {
			$stmt = $this->executeQueryStmt(
				$query,
				$dataArray
			);
			
			$result = $stmt->fetchColumn();
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			throw new \DomainException(__METHOD__, 1445864379, $e);
		}
		
		return $result;
	}
	
	
	/**
	 * processSelectQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return array
	 * @throws \DomainException
	 */
	public function processSelectQuery($query, array $dataArray = array()) {
		try {
			$stmt = $this->executeQueryStmt(
				$query,
				$dataArray
			);
			
			$result = $stmt->fetchAll();
			$stmt->closeCursor();
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			throw new \DomainException(__METHOD__, 1445864379, $e);
		}
		
		return $result;
	}
	
	/**
	 * insertQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return integer
	 */
	public function insertQuery($query, array $dataArray) {
		$result = $this->processDataQuery(
			$query,
			$dataArray,
			AbstractQuery::$insertQueryType
		);
		
		return $this->lastInsertId;
	}
	
	/**
	 * updateQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return boolean
	 */
	public function updateQuery($query, array $dataArray) {
		return $this->processDataQuery(
			$query,
			$dataArray,
			AbstractQuery::$updateQueryType
		);
	}

	/**
	 * deleteQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return boolean
	 */
	public function deleteQuery($query, array $dataArray) {
		return $this->processDataQuery(
			$query,
			$dataArray,
			AbstractQuery::$deleteQueryType
		);
	}
	
	/**
	 * executeQuery
	 * 
	 * @param string $query
	 * @return mixed
	 * @throws \DomainException
	 */
	public function executeQuery($query) {
		try {
			$result = $this->connection->exec($query);
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			throw new \DomainException(__METHOD__, 1449853035, $e);
		}
		
		return $result;
	}
	
	/**
	 * quoteString
	 * 
	 * @param mixed $string
	 * @return string
	 */
	public function quoteString($string) {
		return $this->connection->quote($string);
	}
	
	
	/**
	 * createStmt
	 * 
	 * @param string $query
	 * @return \PDOStatement
	 */
	protected function createStmt($query) {
		return $this->connection->prepare($query);
	}
	
	/**
	 * processBindWhereParameter
	 * 
	 * @param array $dataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindWhereParameter(array $dataArray, \PDOStatement &$stmt) {
		foreach ($dataArray as $key => &$item) {
			if (\strlen($item['value']) > 0) {
				switch ($item['comparison']) {
					case 'LIKE BINARY':
						$string = '%' . $item['value'] . '%';
						$stmt->bindParam(':' . $key, $string, \PDO::PARAM_STR);
						break;

					case 'fieldEqual':
					case 'fieldNotEqual':
						// do nothings
						break;

					case 'integerEqual':
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_INT);
						break;

					default:
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_STR);
						break;
				}
			}
		}
	}
	
	/**
	 * processBindSetParameter
	 * 
	 * @param array $dataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindSetParameter(array $dataArray, \PDOStatement &$stmt) {
		foreach ($dataArray as $field => &$item) {
			if (isset($item['dataType'])) {
				$stmt->bindParam(':' . $field, $item['value'], $item['dataType']);
			} else {
				$stmt->bindParam(':' . $field, $item['value'], \PDO::PARAM_STR);
			}
		}
	}
	
	/**
	 * processDataQuery
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @param integer $queryType
	 * @return boolean
	 */
	protected function processDataQuery($query, array $dataArray, $queryType = 0) {
		try {
			$this->connection->beginTransaction();
			
			$stmt = $this->executeQueryStmt(
				$query,
				$dataArray
			);
			
			if ((int) $queryType === AbstractQuery::$insertQueryType) {
				$this->lastInsertId = $this->connection->lastInsertId();
			}
			
			$this->connection->commit();
			
			$result = true;
		} catch (\Exception $e) {
			$this->debugPdoException($e);
			
			$this->connection->rollBack();
			
			throw new \DomainException(__METHOD__, 1446217174, $e);
		}
		
		return $result;
	}
	
	/**
	 * executeQueryStmt
	 * 
	 * @param string $query
	 * @param array $dataArray
	 * @return \PDOStatement
	 * @throws \InvalidArgumentException
	 */
	protected function executeQueryStmt($query, array $dataArray) {
		$stmt = $this->createStmt($query);
		
		if (\strpos($query, 'SELECT') !== false) {
			if (isset($dataArray['WHERE'])) {
				$this->processBindWhereParameter(
					$dataArray['WHERE'],
					$stmt
				);
			}
		} else {
			// insert, update, delete
			$this->processBindSetParameter(
				$dataArray,
				$stmt
			);
		}
		
		if (!$stmt->execute()) {
			throw new \InvalidArgumentException('invalid PDOStatement.', 1445863220);
		}
			
		return $stmt;
	}
	
	/**
	 * fetchStmt
	 * 
	 * @param string $fetchClass
	 * @param \PDOStatement $stmt
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 */
	protected function fetchStmt($fetchClass, \PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $fetchClass);
		
		$row = $stmt->fetch();
		
		$stmt->closeCursor();
		
		return $row;
	}
	
	/**
	 * fetchAllStmt
	 * 
	 * @param string $fetchClass
	 * @param \PDOStatement $stmt
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	protected function fetchAllStmt($fetchClass, \PDOStatement &$stmt) {
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $fetchClass);
		
		$resultDataArray = new \Packages\Core\Persistence\ArrayIterator($stmt->fetchAll());
		$stmt->closeCursor();
		
		return $resultDataArray;
	}
	
	/**
	 * debugPdoException
	 * 
	 * @param \Exception $e
	 * @return void
	 */
	protected function debugPdoException(\Exception $e) {
		ExceptionUtility::sendDebugData($e);
	}
}
