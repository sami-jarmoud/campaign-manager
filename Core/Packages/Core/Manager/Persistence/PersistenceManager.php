<?php
namespace Packages\Core\Manager\Persistence;

use Packages\Core\Manager\Persistence\InterfacePersistenceManager;


/**
 * Description of PersistenceManager
 *
 * @author Cristian.Reus
 */
class PersistenceManager implements InterfacePersistenceManager {
	
	protected $connectionManager = null;
	
	protected $query = null;
	
	
	
	public function add($data) {
		
	}

	public function persistAll() {
		
	}

	public function remove($data) {
		
	}

	public function update($data) {
		
	}

}
