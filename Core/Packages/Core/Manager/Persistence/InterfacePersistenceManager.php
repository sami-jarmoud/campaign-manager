<?php
namespace Packages\Core\Manager\Persistence;

/**
 * Description of InterfacePersistenceManager
 *
 * @author Cristian.Reus
 */
interface InterfacePersistenceManager {
	
	public function persistAll();
	
	public function add($data);
	
	public function remove($data);
	
	public function update($data);
}
