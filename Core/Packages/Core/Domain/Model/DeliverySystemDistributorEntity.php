<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of DeliverySystemDistributorEntity
 *
 * @author Cristian.Reus
 */
class DeliverySystemDistributorEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id;
	
	/**
	 * m_asp_id
	 * 
	 * @var integer
	 */
	protected $m_asp_id;
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $title;
	
	/**
	 * distributor_id
	 * 
	 * @var string
	 */
	protected $distributor_id;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	/**
	 * set_as_default
	 * 
	 * @var boolean
	 */
	protected $set_as_default;
	
	/**
	 * parent_id
	 * 
	 * @var integer
	 */
	protected $parent_id;
	
	/**
	 * testlist_distributor_id
	 * 
	 * @var string
	 */
	protected $testlist_distributor_id;
	
	/**
	 * deliverySystemEntity
	 * 
	 * @var \Packages\Core\Domain\Model\ClientDeliveryEntity
	 * @relations 
	 */
	protected $clientDeliveryEntity = NULL;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getM_asp_id() {
		return (int) $this->m_asp_id;
	}
	public function setM_asp_id($m_asp_id) {
		$this->m_asp_id = \intval($m_asp_id);
	}

	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getDistributor_id() {
		return $this->distributor_id;
	}
	public function setDistributor_id($distributorId) {
		$this->distributor_id = $distributorId;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}

	public function getSet_as_default() {
		return (boolean) $this->set_as_default;
	}
	public function setSet_as_default($setAsDefault) {
		if ((int) $setAsDefault === 1) {
			$setAsDefault = true;
		} else {
			$setAsDefault = false;
		}
		
		$this->set_as_default = $setAsDefault;
	}
	
	public function getParent_id() {
		return (int) $this->parent_id;
	}
	public function setParent_id($parentId) {
		$this->parent_id = \intval($parentId);
	}
	
	public function getTestlist_distributor_id() {
		return $this->testlist_distributor_id;
	}
	public function setTestlist_distributor_id($testlistDistributorId) {
		$this->testlist_distributor_id = $testlistDistributorId;
	}
	
	
	public function getClientDeliveryEntity() {
		return $this->clientDeliveryEntity;
	}
	public function setClientDeliveryEntity(\Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity) {
		$this->clientDeliveryEntity = $clientDeliveryEntity;
	}

}