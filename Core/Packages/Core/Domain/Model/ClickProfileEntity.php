<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ClickProfileEntity
 *
 * @author Cristian.Reus
 */
class ClickProfileEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id = NULL;
	
	/**
	 * title
	 * 
	 * @var string
	 */
	protected $title;
	
	/**
	 * shortcut
	 * 
	 * @var string
	 */
	protected $shortcut;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}
	
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getShortcut() {
		return $this->shortcut;
	}
	public function setShortcut($shortcut) {
		$this->shortcut = $shortcut;
	}

}