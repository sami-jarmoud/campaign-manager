<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of DeliverySystemEntity
 *
 * @author Cristian.Reus
 */
class DeliverySystemEntity extends AbstractEntity {
	
	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id;
	
	/**
	 * asp
	 * 
	 * @var string
	 */
	protected $asp;
	
	/**
	 * asp_abkz
	 * 
	 * @var string
	 */
	protected $asp_abkz;
	
	/**
	 * active
	 * 
	 * @var integer
	 */
	protected $active;
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getAsp() {
		return $this->asp;
	}
	public function setAsp($asp) {
		$this->asp = $asp;
	}

	public function getAsp_abkz() {
		return $this->asp_abkz;
	}
	public function setAsp_abkz($asp_abkz) {
		$this->asp_abkz = $asp_abkz;
	}
        public function getActive() {
		return (int) $this->active;
	}
	public function setActive($active) {
		$this->active = \intval($active);
	}

}