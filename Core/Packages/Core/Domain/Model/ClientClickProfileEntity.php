<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of ClientClickProfileEntity
 *
 * @author Cristian.Reus
 */
class ClientClickProfileEntity extends AbstractEntity {
	
	/**
	 * uid
	 * 
	 * @var integer
	 */
	protected $uid;
	
	/**
	 * client_id
	 * 
	 * @var integer
	 */
	protected $client_id;
	
	/**
	 * click_profile_id
	 * 
	 * @var integer
	 */
	protected $click_profile_id;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	/**
	 * clickProfileEntity
	 * 
	 * @var \Packages\Core\Domain\Model\ClickProfileEntity
	 * @relations 
	 */
	protected $clickProfileEntity = NULL;
	
	
	
	public function __construct($primaryFieldName = 'uid') {
		parent::__construct($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter
     *
     *******************************************************************************************/
	public function getUid() {
		return (int) $this->uid;
	}
	public function setUid($uid) {
		$this->uid = \intval($uid);
	}
	
	public function getClient_id() {
		return (int) $this->client_id;
	}
	public function setClient_id($clientId) {
		$this->client_id = \intval($clientId);
	}

	public function getClick_profile_id() {
		return (int) $this->click_profile_id;
	}
	public function setClick_profile_id($clickProfileId) {
		$this->click_profile_id = \intval($clickProfileId);
	}
	
	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		if ((int) $active === 1) {
			$active = true;
		} else {
			$active = false;
		}
		
		$this->active = $active;
	}
	
	
	public function getClickProfileEntity() {
		return $this->clickProfileEntity;
	}
	public function setClickProfileEntity(\Packages\Core\Domain\Model\ClickProfileEntity $clickProfileEntity) {
		$this->clickProfileEntity = $clickProfileEntity;
	}



}