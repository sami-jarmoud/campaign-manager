<?php

namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;

/**
 * Description of RecipientFilterEntity
 *
 * @author SamiMohamedJarmoud
 */
class RecipientFilterEntity extends AbstractEntity{
    
    	/**
	 * id
	 * 
	 * @var integer
	 */
	protected $id;
	
	/**
	 * distributor_id
	 * 
	 * @var integer
	 */
	protected $distributor_id;
	
	/**
	 * recipient_filter_id
	 * 
	 * @var integer
	 */
	protected $recipient_filter_id;

	
	/**
	 * quantity
	 * 
	 * @var string
	 */
	protected $quantity;
        
        
        function getId() {
            return $this->id;
        }

        function getDistributor_id() {
            return $this->distributor_id;
        }

        function getRecipient_filter_id() {
            return $this->recipient_filter_id;
        }

        function getQuantity() {
            return $this->quantity;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setDistributor_id($distributor_id) {
            $this->distributor_id = $distributor_id;
        }

        function setRecipient_filter_id($recipient_filter_id) {
            $this->recipient_filter_id = $recipient_filter_id;
        }

        function setQuantity($quantity) {
            $this->quantity = $quantity;
        }

}
