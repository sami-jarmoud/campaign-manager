<?php
namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * Description of UserEntity
 *
 * @author Cristian.Reus
 */
class UserEntity extends AbstractEntity {
	
	/**
	 * benutzer_id
	 * 
	 * @var integer
	 */
	protected $benutzer_id = NULL;
	
	/**
	 * benutzer_name
	 * 
	 * @var string
	 */
	protected $benutzer_name;
	
	/**
	 * anrede
	 * 
	 * @var string
	 */
	protected $anrede;
	
	/**
	 * vorname
	 * 
	 * @var string
	 */
	protected $vorname;
	
	/**
	 * nachname
	 * 
	 * @var string
	 */
	protected $nachname;
	
	/**
	 * pw
	 * 
	 * @var string
	 */
	protected $pw;
	
	/**
	 * email
	 * 
	 * @var string
	 */
	protected $email;
	
	/**
	 * mandant
	 * 
	 * @var string
	 */
	protected $mandant;
	
	/**
	 * timestamp
	 * 
	 * @var \DateTime
	 */
	protected $timestamp;
	
	/**
	 * rechte
	 * 
	 * @var string
	 */
	protected $rechte;
	
	/**
	 * tab_config
	 * 
	 * @var string
	 */
	protected $tab_config;
	
	/**
	 * zugang_mandant
	 * 
	 * @var string
	 */
	protected $zugang_mandant;
	
	/**
	 * menu
	 * 
	 * @var string
	 */
	protected $menu;
	
	/**
	 * mid
	 * 
	 * @var integer
	 */
	protected $mid;
	
	/**
	 * widget
	 * 
	 * @var string
	 */
	protected $widget;
	
	/**
	 * active
	 * 
	 * @var boolean
	 */
	protected $active;
	
	/**
	 * abteilung
	 * 
	 * @var string
	 */
	protected $abteilung;
        
	/**
	 * telefon
	 * 
	 * @var string
	 */
	protected $telefon;        
	
	
	
	/**
	 * @deprecated
	 */
	protected $name;
	protected $zugang;
	protected $superview;
	
	
	
	public function __construct($primaryFieldName = 'benutzer_id') {
		parent::__construct($primaryFieldName);
	}
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getBenutzer_id() {
		return (int) $this->benutzer_id;
	}
	public function setBenutzer_id($benutzer_id) {
		$this->benutzer_id = \intval($benutzer_id);
	}

	public function getBenutzer_name() {
		return $this->benutzer_name;
	}
	public function setBenutzer_name($benutzer_name) {
		$this->benutzer_name = $benutzer_name;
	}

	public function getAnrede() {
		return $this->anrede;
	}
	public function setAnrede($anrede) {
		$this->anrede = $anrede;
	}

	public function getVorname() {
		return $this->vorname;
	}
	public function setVorname($vorname) {
		$this->vorname = $vorname;
	}

	public function getNachname() {
		return $this->nachname;
	}
	public function setNachname($nachname) {
		$this->nachname = $nachname;
	}

	public function getPw() {
		return $this->pw;
	}
	public function setPw($pw) {
		$this->pw = $pw;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}

	public function getMandant() {
		return $this->mandant;
	}
	public function setMandant($mandant) {
		$this->mandant = $mandant;
	}

	public function getTimestamp() {
		return $this->timestamp;
	}
	public function setTimestamp(\DateTime $timestamp) {
		$this->timestamp = $timestamp;
	}

	public function getRechte() {
		return $this->rechte;
	}
	public function setRechte($rechte) {
		$this->rechte = $rechte;
	}

	public function getTab_config() {
		return $this->tab_config;
	}
	public function setTab_config($tab_config) {
		$this->tab_config = $tab_config;
	}

	public function getZugang_mandant() {
		return $this->zugang_mandant;
	}
	public function setZugang_mandant($zugang_mandant) {
		$this->zugang_mandant = $zugang_mandant;
	}

	public function getMenu() {
		return $this->menu;
	}
	public function setMenu($menu) {
		$this->menu = $menu;
	}

	public function getMid() {
		return (int) $this->mid;
	}
	public function setMid($mid) {
		$this->mid = \intval($mid);
	}

	public function getWidget() {
		return $this->widget;
	}
	public function setWidget($widget) {
		$this->widget = $widget;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		$this->active = (boolean) \intval($active);
	}

	public function getAbteilung() {
		return $this->abteilung;
	}
	public function setAbteilung($abteilung) {
		$this->abteilung = $abteilung;
	}
  
	public function getTelefon() {
		return $this->telefon;
	}
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}        
	
	
	
	/**
	 * @deprecated
	 */
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getZugang() {
		return $this->zugang;
	}
	public function setZugang($zugang) {
		$this->zugang = $zugang;
	}
	
	public function getSuperview() {
		return $this->superview;
	}
	public function setSuperview($superview) {
		$this->superview = $superview;
	}

}