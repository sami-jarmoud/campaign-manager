<?php

namespace Packages\Core\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;

/**
 * Description of DomainDeliveryEntity
 *
 * @author Sami Jarmoud
 */
class DomainDeliveryEntity extends AbstractEntity {

    /**
     * dasp_id
     * 
     * @var integer
     */
    protected $dasp_id;

    /**
     * m_id
     * 
     * @var integer
     */
    protected $m_id;

    /**
     * asp_id
     * 
     * @var integer
     */
    protected $asp_id;

     /**
     * dsd_id
     * 
     * @var integer
     */
    protected $dsd_id;
    
     /**
     * verteiler
     * 
     * @var string
     */
    protected $verteiler;
    
    /**
     * domain_name
     * 
     * @var string
     */
    protected $domain_name;

    /**
     * active
     * 
     * @var integer
     */
    protected $active;

    /**
     * deliverySystemEntity
     * 
     * @var \Packages\Core\Domain\Model\DeliverySystemEntity
     * @relations 
     */
    protected $deliverySystemEntity = NULL;

    /*     * ******************************************************************************************
     *
     *              setter and getter
     *
     * ***************************************************************************************** */

    public function getDasp_id() {
        return $this->dasp_id;
    }

    public function getM_id() {
        return $this->m_id;
    }

    public function getAsp_id() {
        return $this->asp_id;
    }

    public function getDomain_name() {
        return $this->domain_name;
    }

    public function setDasp_id($dasp_id) {
        $this->dasp_id = $dasp_id;
    }

    public function setM_id($m_id) {
        $this->m_id = $m_id;
    }

    public function setAsp_id($asp_id) {
        $this->asp_id = $asp_id;
    }

    public function setDomain_name($domain_name) {
        $this->domain_name = $domain_name;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }
    function getDsd_id() {
        return $this->dsd_id;
    }

    function setDsd_id($dsd_id) {
        $this->dsd_id = $dsd_id;
    }
    function getVerteiler() {
        return $this->verteiler;
    }

    function setVerteiler($verteiler) {
        $this->verteiler = $verteiler;
    }

            public function getDeliverySystemEntity() {
        return $this->deliverySystemEntity;
    }

    public function setDeliverySystemEntity(\Packages\Core\Domain\Model\DeliverySystemEntity $deliverySystemEntity) {
        $this->deliverySystemEntity = $deliverySystemEntity;
    }

}
