<?php
namespace Packages\Core\Domain\Repository;

use Packages\Core\Persistence\AbstractRepository;
use Packages\Core\Persistence\Generic\Query;


/**
 * Description of UserRepository
 *
 * @author Cristian.Reus
 */
class UserRepository extends AbstractRepository {
	
	/**
	 * findAll
	 * 
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findAll() {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => 'vorname ASC, nachname ASC'
		);
		
		return $this->findAllByQueryPartsDataArray($queryPartsDataArray);
	}
	
	/**
	 * findById
	 * 
	 * @param integer $id
	 * @return \Packages\Core\Domain\Model\UserEntity
	 * @throws \InvalidArgumentException
	 */
	public function findById($id) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid id.', 1446538291);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	}
	
	/**
	 * findAllByClientId
	 * 
	 * @param integer $clientId
	 * @param boolean $useUnderMandant
	 * @return \Packages\Core\Persistence\ArrayIterator
	 * @throws \InvalidArgumentException
	 */
	public function findAllByClientId($clientId, $useUnderMandant = false) {
		if (!\is_int($clientId)) {
			throw new \InvalidArgumentException('invalid clientId.', 1447148911);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_mid' => array(
					'sql' => Query::masketField('mid'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => 'vorname ASC, nachname ASC'
		);
		
		$result = $this->findAllByQueryPartsDataArray($queryPartsDataArray);
		if ($result->count() > 0) {
			foreach ($result as $userEntity) {
				/* @var $userEntity \Packages\Core\Domain\Model\UserEntity */
				$userEntity->_resetCleanProperties();
				
				$userEntity->setPw(NULL);
			}
		}
		
		if ((boolean) $useUnderMandant === true) {
			$this->getUnderClientsUsersByClientId(
				$clientId,
				$result
			);
		}
		
		return $result;
	}
	
	
	/**
	 * getUnderClientsUsersByClientId
	 * 
	 * @param integer $clientId
	 * @param \Packages\Core\Persistence\ArrayIterator $resultDataArray
	 * @return void
	 */
	protected function getUnderClientsUsersByClientId($clientId, \Packages\Core\Persistence\ArrayIterator &$resultDataArray) {
		$result = $this->findAll();
		if ($result->count() > 0) {
			foreach ($result as $userEntity) {
				/* @var $userEntity \Packages\Core\Domain\Model\UserEntity */
				
				$userClientsIds = \explode(',', $userEntity->getZugang_mandant());
				if (\in_array((int) $clientId, $userClientsIds)) {
					$userEntity->_resetCleanProperties();
					
					$userEntity->setPw(NULL);
					$userEntity->setZugang(NULL);
					
					$resultDataArray->append($userEntity);
				}
			}
		}
	}
}
