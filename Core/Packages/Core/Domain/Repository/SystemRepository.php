<?php
namespace Packages\Core\Domain\Repository;

use Packages\Core\Persistence\AbstractRepository;
use Packages\Core\Persistence\Generic\Query;


/**
 * Description of SystemRepository
 *
 * @author Cristian.Reus
 */
class SystemRepository extends AbstractRepository {
	
	/**
	 * clientDeliveryTable
	 * 
	 * @var string
	 */
	protected $clientDeliveryTable;
	
	/**
	 * clientDeliveryPrimaryField
	 * 
	 * @var string
	 */
	protected $clientDeliveryPrimaryField;
	
	/**
	 * clientDeliveryFetchClass
	 * 
	 * @var string
	 */
	protected $clientDeliveryFetchClass;
	
	
	/**
	 * deliverySystemTable
	 * 
	 * @var string
	 */
	protected $deliverySystemTable;
	
	/**
	 * deliverySystemPrimaryField
	 * 
	 * @var string
	 */
	protected $deliverySystemPrimaryField;
	
	/**
	 * deliverySystemFetchClass
	 * 
	 * @var string
	 */
	protected $deliverySystemFetchClass;
	
	
	/**
	 * deliverySystemDistributorTable
	 * 
	 * @var string
	 */
	protected $deliverySystemDistributorTable;
	
	/**
	 * deliverySystemDistributorPrimaryField
	 * 
	 * @var string
	 */
	protected $deliverySystemDistributorPrimaryField;
	
	/**
	 * deliverySystemDistributorFetchClass
	 * 
	 * @var string
	 */
	protected $deliverySystemDistributorFetchClass;
	
	
	/**
	 * clientClickProfilesTable
	 * 
	 * @var string
	 */
	protected $clientClickProfilesTable;
	
	/**
	 * clientClickProfilesPrimaryField
	 * 
	 * @var string
	 */
	protected $clientClickProfilesPrimaryField;
	
	/**
	 * clientClickProfilesFetchClass
	 * 
	 * @var string
	 */
	protected $clientClickProfilesFetchClass;
	
	
	/**
	 * clickProfilesTable
	 * 
	 * @var string
	 */
	protected $clickProfilesTable;
	
	/**
	 * clickProfilesPrimaryField
	 * 
	 * @var string
	 */
	protected $clickProfilesPrimaryField;
	
	/**
	 * clickProfilesFetchClass
	 * 
	 * @var string
	 */
	protected $clickProfilesFetchClass;
	
	/**
	 * mandantTable
	 * @var string
	 */
	protected $mandantTable;
	
	/**
	 * mandantPrimaryField
	 * @var string
	 */
	protected $mandantPrimaryField;
	
	/**
	 * mandantFetchClass
	 * @var string
	 */
	protected $mandantFetchClass;
        
        /**
	 * recipientFilterTable
	 * 
	 * @var string
	 */
	protected $recipientFilterTable;
	
	/**
	 * recipientFilterPrimaryField
	 * 
	 * @var string
	 */
	protected $recipientFilterPrimaryField;
	
	/**
	 * recipientFilterFetchClass
	 * 
	 * @var string
	 */
	protected $recipientFilterFetchClass;
        
        /**
	 * domainDelivery
	 * 
	 * @var string
	 */
	protected $domainDeliveryTable;
	
	/**
	 * domainDeliveryPrimaryField
	 * 
	 * @var string
	 */
	protected $domainDeliveryPrimaryField;
	
	/**
	 * domainDeliveryFetchClass
	 * 
	 * @var string
	 */
	protected $domainDeliveryFetchClass;
	
	/**
	 * findById
	 * 
	 * @param integer $id
	 * @return \Packages\Core\Domain\Model\ClientEntity
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function findById($id) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid clientId.', 1446554305);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		$object = $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
		if (!($object instanceof \Packages\Core\Domain\Model\ClientEntity)) {
			throw new \DomainException('invalid clientEntity', 1447155200);
		}
		
		/* @var $object \Packages\Core\Domain\Model\ClientEntity */
		$object->_resetCleanProperties();
		
		// get and add clientDeliveries
		$clientDeliveries = $this->getClientDeliveriesByClientId((int) $object->_getProperty($this->primaryField));
		if ($clientDeliveries->count() > 0) {
			foreach ($clientDeliveries as $clientDeliveryEntity) {
				/* @var $clientDeliveryEntity \Packages\Core\Domain\Model\ClientDeliveryEntity */
				$clientDeliveryEntity->_resetCleanProperties();
				
				// getDeliverySystemEntity
				$deliverySystemEntity = $this->getDeliverySystemById($clientDeliveryEntity->getAsp_id());
				if (!($deliverySystemEntity instanceof \Packages\Core\Domain\Model\DeliverySystemEntity)) {
					throw new \DomainException('invalid deliverySystemEntity', 1446714832);
				}
				$deliverySystemEntity->_resetCleanProperties();
				
				$clientDeliveryEntity->setDeliverySystemEntity($deliverySystemEntity);
				
				$object->addClientDeliveryEntity(
					$clientDeliveryEntity,
					$deliverySystemEntity->getAsp()
				);
			}
		}
		
		// get and add clientClickProfiles
		$clientClickProfiles = $this->getClientClickProfilesByClientId((int) $object->_getProperty($this->primaryField));
		if ($clientClickProfiles->count() > 0) {
			foreach ($clientClickProfiles as $clientClickProfileEntity) {
				/* @var $clientClickProfileEntity \Packages\Core\Domain\Model\ClientClickProfileEntity */
				$clientClickProfileEntity->_resetCleanProperties();
				
				$clickProfileEntity = $this->getClickProfilesById($clientClickProfileEntity->getClick_profile_id());
				if (!($clickProfileEntity instanceof \Packages\Core\Domain\Model\ClickProfileEntity)) {
					throw new \DomainException('invalid clickProfilesEntity', 1447146525);
				}
				$clickProfileEntity->_resetCleanProperties();
				
				$clientClickProfileEntity->setClickProfileEntity($clickProfileEntity);
				
				$object->addClientClickProfilesEntity(
					$clientClickProfileEntity,
					$clickProfileEntity->getShortcut()
				);
			}
		}
		
		if ($this->useMemoryUsage) {
			$object->_memorizeCleanState();
		}
		
		return $object;
	}
	
	/**
	 * findDeliverySystemDistributorById
	 * 
	 * @param integer $dsdId
	 * @return \Packages\Core\Domain\Model\DeliverySystemDistributorEntity
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function findDeliverySystemDistributorById($dsdId) {
		if (!\is_int($dsdId)) {
			throw new \InvalidArgumentException('invalid deliverySystemDistributor id.', 1446648223);
		}
		
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->deliverySystemDistributorTable),
			'WHERE' => array(
				'_' . $this->deliverySystemDistributorPrimaryField => array(
					'sql' => Query::masketField($this->deliverySystemDistributorPrimaryField),
					'value' => $dsdId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		$object = $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->deliverySystemDistributorFetchClass
		);
		/* @var $object \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
		$object->_resetCleanProperties();
		
		// getClientDeliveriesById
		$clientDeliveryEntity = $this->getClientDeliveriesById($object->getM_asp_id());
		if (!($clientDeliveryEntity instanceof \Packages\Core\Domain\Model\ClientDeliveryEntity)) {
			throw new \DomainException('invalid clientDeliveryEntity', 1446714740);
		}
		$clientDeliveryEntity->_resetCleanProperties();
		
		// getDeliverySystemEntity
		$deliverySystemEntity = $this->getDeliverySystemById($clientDeliveryEntity->getAsp_id());
		if (!($deliverySystemEntity instanceof \Packages\Core\Domain\Model\DeliverySystemEntity)) {
			throw new \DomainException('invalid deliverySystemEntity', 1446714832);
		}
		$deliverySystemEntity->_resetCleanProperties();
		
		$clientDeliveryEntity->setDeliverySystemEntity($deliverySystemEntity);
		
		$object->setClientDeliveryEntity($clientDeliveryEntity);
		
		if ($this->useMemoryUsage) {
			$object->_memorizeCleanState();
		}
		
		return $object;
	}
	
	/**
	 * findDeliverySystemDomainById
	 * 
	 * @param integer $dsdnId
	 * @return \Packages\Core\Domain\Model\DomainDeliveryEntity
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function findDeliverySystemDomainById($dsdnId) {
		if (!\is_int($dsdnId)) {
			throw new \InvalidArgumentException('invalid deliverySystemDomain id.', 1446648223);
		}
		
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->domainDeliveryTable),
			'WHERE' => array(
				'_' . $this->domainDeliveryPrimaryField => array(
					'sql' => Query::masketField($this->domainDeliveryPrimaryField),
					'value' => $dsdnId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		$object = $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->domainDeliveryFetchClass
		);
		/* @var $object \Packages\Core\Domain\Model\DomainDeliveryEntity */
		$object->_resetCleanProperties();
				
		return $object;
	}
	
	/**
	 * findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemId
	 * @param array $underClientIds
	 * @return \Packages\Core\Persistence\ArrayIterator
	 * @throws \InvalidArgumentException
	 */
	public function findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId($clientId, $deliverySystemId, array $underClientIds = array()) {
		if (!\is_int($clientId)) {
			throw new \InvalidArgumentException('invalid client id.', 1447423148);
		}
		
		if (!\is_int($deliverySystemId)) {
			throw new \InvalidArgumentException('invalid deliverySystem id.', 1447424929);
		}
		
		$queryPartsDataArray = array(
			'SELECT' => Query::masketField($this->deliverySystemDistributorTable) . '.*',
			'FROM' => Query::masketField($this->deliverySystemDistributorTable)
				. ' JOIN ' . Query::masketField($this->clientDeliveryTable) . ' ON ' 
					. Query::masketField($this->deliverySystemDistributorTable) . '.' . Query::masketField('m_asp_id') . ' = ' . Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('masp_id')
				. ' JOIN ' . Query::masketField($this->deliverySystemTable) . ' ON ' 
					. Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('asp_id') . ' = ' . Query::masketField($this->deliverySystemTable) . '.' . Query::masketField('id')
			,
			'WHERE' => array(
				'_' . $this->clientDeliveryTable . '_active' => array(
					'sql' => Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
				'_' . $this->deliverySystemTable . '_' . $this->deliverySystemPrimaryField => array(
					'sql' => Query::masketField($this->deliverySystemTable) . '.' . Query::masketField($this->deliverySystemPrimaryField),
					'value' => $deliverySystemId,
					'comparison' => 'integerEqual'
				),
				'_' . $this->deliverySystemDistributorTable . '_active' => array(
					'sql' => Query::masketField($this->deliverySystemDistributorTable) . '.' . Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		$additionalWhereDataArray = $this->getAddWhereForUnderClientsIds(
			$clientId,
			$underClientIds
		);
		
		$queryPartsDataArray['WHERE'] += $additionalWhereDataArray;
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->deliverySystemDistributorFetchClass
		);
	}

	/**
	 * findDeliverySystemDistributorsDataArrayByClientId
	 * 
	 * @param integer $clientId
	 * @param integer $deliverySystemId
	 * @param array $underClientIds
	 * @return \Packages\Core\Persistence\ArrayIterator
	 * @throws \InvalidArgumentException
	 */
	public function findDeliverySystemDistributorsDataArrayByClientId($clientId, array $underClientIds = array()) {
		if (!\is_int($clientId)) {
			throw new \InvalidArgumentException('invalid client id.', 1447423148);
		}
		
		$queryPartsDataArray = array(
			'SELECT' => Query::masketField($this->deliverySystemDistributorTable) . '.*',
			'FROM' => Query::masketField($this->deliverySystemDistributorTable)
				. ' JOIN ' . Query::masketField($this->clientDeliveryTable) . ' ON ' 
					. Query::masketField($this->deliverySystemDistributorTable) . '.' . Query::masketField('m_asp_id') . ' = ' . Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('masp_id')
				. ' JOIN ' . Query::masketField($this->deliverySystemTable) . ' ON ' 
					. Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('asp_id') . ' = ' . Query::masketField($this->deliverySystemTable) . '.' . Query::masketField('id')
			,
			'WHERE' => array(
                            '_' . $this->deliverySystemDistributorTable . '_active' => array(
					'sql' => Query::masketField($this->deliverySystemDistributorTable) . '.' . Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
		        	),
		);
		$additionalWhereDataArray = $this->getAddWhereForUnderClientsIds(
			$clientId,
			$underClientIds
		);
		
		$queryPartsDataArray['WHERE'] += $additionalWhereDataArray;
		
		$DeliverySystemDistributors = $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->deliverySystemDistributorFetchClass
		);
                foreach ($DeliverySystemDistributors as $DeliverySystemDistributorEntity){
                    
                /* @var $DeliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
		$DeliverySystemDistributorEntity->_resetCleanProperties();
                    // getClientDeliveriesById
		$clientDeliveryEntity = $this->getClientDeliveriesById($DeliverySystemDistributorEntity->getM_asp_id());
		if (!($clientDeliveryEntity instanceof \Packages\Core\Domain\Model\ClientDeliveryEntity)) {
			throw new \DomainException('invalid clientDeliveryEntity', 1446714740);
		}
		$clientDeliveryEntity->_resetCleanProperties();
		// getDeliverySystemEntity
		$deliverySystemEntity = $this->getDeliverySystemById($clientDeliveryEntity->getAsp_id());
		if (!($deliverySystemEntity instanceof \Packages\Core\Domain\Model\DeliverySystemEntity)) {
			throw new \DomainException('invalid deliverySystemEntity', 1446714832);
		}
		$deliverySystemEntity->_resetCleanProperties();
	
		$clientDeliveryEntity->setDeliverySystemEntity($deliverySystemEntity);
		$DeliverySystemDistributorEntity->setClientDeliveryEntity($clientDeliveryEntity);
		
		if ($this->useMemoryUsage) {
			$DeliverySystemDistributorEntity->_memorizeCleanState();
		   }
                    
                }
		return $DeliverySystemDistributors;
	}	
	
	
	
	/**
	 * getDeliverySystemEntityByDeliverySystem
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $clientDeliverySystems
	 * @param string $deliverySystem
	 * @return \Packages\Core\Domain\Model\ClientDeliveryEntity
	 */
	public function getDeliverySystemEntityByDeliverySystem(\Packages\Core\Persistence\ObjectStorage $clientDeliverySystems, $deliverySystem) {
		$clientDeliverySystems->rewind();
		
		while ($clientDeliverySystems->valid()) {
			$result = $clientDeliverySystems->current();
			/* @var $result \Packages\Core\Domain\Model\ClientDeliveryEntity */
			
			if ($result->getDeliverySystemEntity()->getAsp_abkz() === $deliverySystem) {
				break;
			}
			
			/*
			$objectInfo = $clientDeliverySystems->getInfo();
			if ($objectInfo == $deliverySystem) {
				$result = $clientDeliverySystems->current();
				break;
			}
			 * 
			 */
			
			$clientDeliverySystems->next();
		}
		
		if (!($result instanceof \Packages\Core\Domain\Model\ClientDeliveryEntity)) {
			throw new \InvalidArgumentException('invalid clientDeliveryEntity', 1446623783);
		}
		
		return $result;
	}
	
	/**
	 * getStatistikAuswertung
	 *
	 * @return \Packages\Core\Domain\Model\StatistikAuswertungEntity
	 */
	public function getStatistikAuswertung() {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->statistikAuswertungTable)
		);
	
		return $this->query->getRowByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->statistikAuswertungFetchClass
				);
	}
	

	/**
	 * findDeliverySystemDistributorById
	 *
	 * @param  String $title
	 * @return \Packages\Core\Domain\Model\DeliverySystemDistributorEntity
	 * @throws \InvalidArgumentException
	 * @throws \DomainException
	 */
	public function findDeliverySystemDistributorByTitle($title) {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->deliverySystemDistributorTable),
				'WHERE' => array(
						'_' . $this->deliverySystemDistributorPrimaryField => array(
								'sql' => Query::masketField('title'),
								'value' => $title,
								'comparison' => 'integerEqual'
						),
				),
		);
	
		$object = $this->query->getRowByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->deliverySystemDistributorFetchClass
				);
		/* @var $object \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
		$object->_resetCleanProperties();
	
		// getClientDeliveriesById
		$clientDeliveryEntity = $this->getClientDeliveriesById($object->getM_asp_id());
		if (!($clientDeliveryEntity instanceof \Packages\Core\Domain\Model\ClientDeliveryEntity)) {
			throw new \DomainException('invalid clientDeliveryEntity', 1446714740);
		}
		$clientDeliveryEntity->_resetCleanProperties();
	
		// getDeliverySystemEntity
		$deliverySystemEntity = $this->getDeliverySystemById($clientDeliveryEntity->getAsp_id());
		if (!($deliverySystemEntity instanceof \Packages\Core\Domain\Model\DeliverySystemEntity)) {
			throw new \DomainException('invalid deliverySystemEntity', 1446714832);
		}
		$deliverySystemEntity->_resetCleanProperties();
	
		$clientDeliveryEntity->setDeliverySystemEntity($deliverySystemEntity);
	
		$object->setClientDeliveryEntity($clientDeliveryEntity);
	
		if ($this->useMemoryUsage) {
			$object->_memorizeCleanState();
		}
	
		return $object;
	}
	
		/**
	 * getAdminMandant
	 *
	 * @return \Modules\Admin\Domain\Model\AdminMandantEntity
	 */
	public function getAdminMandant() {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->mandantTable),
				'ORDER_BY' => Query::masketField($this->mandantPrimaryField) . 'DESC'
		);
	
		return $this->query->getRowsByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->mandantFetchClass
				);
	}
	
	/**
	 * findClientClickProfiles
	 * 
	 * @return \Packages\Core\Persistence\ObjectStorage 
	 */
	public function findClientClickProfiles($id) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid clientId.', 1446554305);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),

			),
		);
		
		$object = $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	 if (!($object instanceof \Packages\Core\Domain\Model\ClientEntity)) {
			throw new \DomainException('invalid clientEntity', 1447155200);
		}
		
		/* @var $object \Packages\Core\Domain\Model\ClientEntity */
		$object->_resetCleanProperties();
		
		// get clientClickProfiles
		$clientClickProfiles = $this->getAllClientClickProfilesByClientId((int) $object->_getProperty($this->primaryField));
		if ($clientClickProfiles->count() > 0) {
			foreach ($clientClickProfiles as $clientClickProfileEntity) {
				/* @var $clientClickProfileEntity \Packages\Core\Domain\Model\ClientClickProfileEntity */
				$clientClickProfileEntity->_resetCleanProperties();
				
				$clickProfileEntity = $this->getClickProfilesById($clientClickProfileEntity->getClick_profile_id());
				if (!($clickProfileEntity instanceof \Packages\Core\Domain\Model\ClickProfileEntity)) {
					throw new \DomainException('invalid clickProfilesEntity', 1447146525);
				}
				$clickProfileEntity->_resetCleanProperties();
				
				$clientClickProfileEntity->setClickProfileEntity($clickProfileEntity);
				
				$object->addClientClickProfilesEntity(
					$clientClickProfileEntity,
					$clickProfileEntity->getShortcut()
				);
			}
		}
		
		if ($this->useMemoryUsage) {
			$object->_memorizeCleanState();
		}
		return $object->getClientClickProfiles();
	}

																																													
	/**
	 * getClientClickProfile
	 * 
	 * @return \Packages\Core\Persistence\ObjectStorage 
	 */
	public function getClientClickProfile($id, $clickProfilId) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid clientId.', 1446554305);
		}
		
		$queryPartsDataArray = array(
			'WHERE' => array(
				'_' . $this->primaryField => array(
					'sql' => Query::masketField($this->primaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),

			),
		);
		
		$object = $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	 if (!($object instanceof \Packages\Core\Domain\Model\ClientEntity)) {
			throw new \DomainException('invalid clientEntity', 1447155200);
		}
		
		/* @var $object \Packages\Core\Domain\Model\ClientEntity */
		$object->_resetCleanProperties();
		
		// get clientClickProfiles
		$clientClickProfiles = $this->getAllClientClickProfilesByClientId((int) $object->_getProperty($this->primaryField));
		if ($clientClickProfiles->count() > 0) {
			foreach ($clientClickProfiles as $clientClickProfileEntity) {
				/* @var $clientClickProfileEntity \Packages\Core\Domain\Model\ClientClickProfileEntity */
				$clientClickProfileEntity->_resetCleanProperties();
				if($clickProfilId === $clientClickProfileEntity->getClick_profile_id()
				 ){
				$clickProfileEntity = $this->getClickProfilesById($clientClickProfileEntity->getClick_profile_id());
				if (!($clickProfileEntity instanceof \Packages\Core\Domain\Model\ClickProfileEntity)) {
					throw new \DomainException('invalid clickProfilesEntity', 1447146525);
				}
				$clickProfileEntity->_resetCleanProperties();
				
				$clientClickProfileEntity->setClickProfileEntity($clickProfileEntity);
				
				$object->addClientClickProfilesEntity(
					$clientClickProfileEntity,
					$clickProfileEntity->getShortcut()
				);
			  }
			}
		}
		
		if ($this->useMemoryUsage) {
			$object->_memorizeCleanState();
		}
		return $object->getClientClickProfiles();
	}
	
       /**
	 * findDomainDeliverySystemsDataArrayByClientId
	 * 
	 * @return \Packages\Core\Domain\Model\DomainDeliveryEntity
	 */
	public function findDomainDeliverySystemsDataArrayByClientId($clientId,$aspId, array $underClientIds = array()) {

            
            	if (!\is_int($clientId)) {
			throw new \InvalidArgumentException('invalid client id.', 1447423148);
		}
		
		$queryPartsDataArray = array(
			'SELECT' => Query::masketField($this->domainDeliveryTable) . '.*',
			'FROM' => Query::masketField($this->domainDeliveryTable)				
				. ' JOIN ' . Query::masketField($this->deliverySystemTable) . ' ON ' 
					. Query::masketField($this->domainDeliveryTable) . '.' . Query::masketField('asp_id') . ' = ' . Query::masketField($this->deliverySystemTable) . '.' . Query::masketField('id')
			,
			'WHERE' => array(
                         

                            '_' . $this->domainDeliveryTable . '_active' => array(
					'sql' => Query::masketField($this->domainDeliveryTable) . '.' . Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				  ),

                           '_' . $this->domainDeliveryTable . '_m_id' => array(
					'sql' => Query::masketField($this->domainDeliveryTable) . '.' . Query::masketField('m_id'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				),
                             '_' . $this->domainDeliveryTable . '_asp_id' => array(
					'sql' => Query::masketField($this->domainDeliveryTable) . '.' . Query::masketField('asp_id'),
					'value' => $aspId,
					'comparison' => 'integerEqual'
				),
		       ),
		);
		
		
		$DomainDeliverys = $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->domainDeliveryFetchClass
		);

                     foreach ($DomainDeliverys as $DomainDeliveryEntity){
                    
                /* @var $DomainDeliveryEntity \Packages\Core\Domain\Model\DomainDeliveryEntity */
		$DomainDeliveryEntity->_resetCleanProperties();
 
		// getDeliverySystemEntity
		$deliverySystemEntity = $this->getDeliverySystemById($DomainDeliveryEntity->getAsp_id());
		if (!($deliverySystemEntity instanceof \Packages\Core\Domain\Model\DeliverySystemEntity)) {
			throw new \DomainException('invalid deliverySystemEntity', 1446714832);
		}
		$deliverySystemEntity->_resetCleanProperties();
	
		$DomainDeliveryEntity->setDeliverySystemEntity($deliverySystemEntity);
		
		if ($this->useMemoryUsage) {
			$DomainDeliveryEntity->_memorizeCleanState();
		   }
                    
                }
                
		return $DomainDeliverys;
	}
        
        public function findClickProfileById($clickProfileId){
		
		return $this->getClickProfilesById($clickProfileId);
	}	
        
        /**
         * 
         * @param integer $deliverySystemId
         * @param integer $quantity
         * @return \Packages\Core\Domain\Model\RecipientFilterEntity
         */
        public function findRecipientFilterId($deliverySystemId, $quantity){
            $queryPartsDataArray = array(
			'FROM' => Query::masketField($this->recipientFilterTable),
			'WHERE' => array(
				
                                '_quantity' . $this->recipientFilterPrimaryField => array(
					'sql' => Query::masketField('quantity'),
					'value' => $quantity,
					'comparison' => 'integerEqual'
				),
                                '_distributor_id' . $this->recipientFilterPrimaryField => array(
					'sql' => Query::masketField('distributor_id'),
					'value' => $deliverySystemId,
					'comparison' => 'integerEqual'
				),
			),
		);
            
            return $this->query->getRowsByQueryPartsDataArray(
                   $queryPartsDataArray,
                   $this->recipientFilterFetchClass
		);
        }
	/**
	 * getClientDeliveriesByClientId
	 * 
	 * @param integer $clientId
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	protected function getClientDeliveriesByClientId($clientId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->clientDeliveryTable),
			'WHERE' => array(
				'_m_id' => array(
					'sql' => Query::masketField('m_id'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->clientDeliveryFetchClass
		);
	}
	
	/**
	 * getClientDeliveriesById
	 * 
	 * @param integer $id
	 * @return \Packages\Core\Domain\Model\ClientDeliveryEntity
	 */
	protected function getClientDeliveriesById($id) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->clientDeliveryTable),
			'WHERE' => array(
				'_' . $this->clientDeliveryPrimaryField => array(
					'sql' => Query::masketField($this->clientDeliveryPrimaryField),
					'value' => $id,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->clientDeliveryFetchClass
		);
	}
	
        /**
	 * getDeliverySystemById
	 * 
	 * @param integer $deliverySystemId
	 * @return \Packages\Core\Domain\Model\DeliverySystemEntity
	 */
	protected function getDeliverySystemById($deliverySystemId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->deliverySystemTable),
			'WHERE' => array(
				'_' . $this->deliverySystemPrimaryField => array(
					'sql' => Query::masketField($this->deliverySystemPrimaryField),
					'value' => $deliverySystemId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->deliverySystemFetchClass
		);
	}
	
	
	/**
	 * getClientClickProfilesByClientId
	 * 
	 * @param integer $clientId
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	protected function getClientClickProfilesByClientId($clientId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->clientClickProfilesTable),
			'WHERE' => array(
				'_client_id' => array(
					'sql' => Query::masketField('client_id'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				),
				'_active' => array(
					'sql' => Query::masketField('active'),
					'value' => 1,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->clientClickProfilesFetchClass
		);
	}

	/**
	 * getAllClientClickProfilesByClientId
	 * 
	 * @param integer $clientId
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	protected function getAllClientClickProfilesByClientId($clientId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->clientClickProfilesTable),
			'WHERE' => array(
				'_client_id' => array(
					'sql' => Query::masketField('client_id'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				)
			),
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->clientClickProfilesFetchClass
		);
	}	
	
	/**
	 * getClickProfilesById
	 * 
	 * @param integer $clickProfileId
	 * @return \Packages\Core\Domain\Model\ClickProfileEntity
	 */
	protected function getClickProfilesById($clickProfileId) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->clickProfilesTable),
			'WHERE' => array(
				'_' . $this->clickProfilesPrimaryField => array(
					'sql' => Query::masketField($this->clickProfilesPrimaryField),
					'value' => $clickProfileId,
					'comparison' => 'integerEqual'
				),
			),
		);
		
		return $this->query->getRowByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->clickProfilesFetchClass
		);
	}
	
	/**
	 * getStatistikAuswertunById
	 *
	 * @param integer $auswertungId
	 * @return \Packages\Core\Domain\Model\StatistikAuswertungEntity
	 */
	protected function getStatistikAuswertungById($auswertungId) {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->statistikAuswertungTable),
				'WHERE' => array(
						'_' . $this->statistikAuswertungPrimaryField => array(
								'sql' => Query::masketField($this->statistikAuswertungPrimaryField),
								'value' => $auswertungId,
								'comparison' => 'integerEqual'
						),
				),
		);
	
		return $this->query->getRowByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->statistikAuswertungFetchClass
				);
	}
	
	/**
	 * getAddWhereForUnderClientsIds
	 * 
	 * @param integer $clientId
	 * @param array $underClientIds
	 * @return array
	 */
	private function getAddWhereForUnderClientsIds($clientId, array $underClientIds) {
		if (\count($underClientIds) > 0) {
			$dataArray = array(
				'_' . $this->clientDeliveryTable . '_m_id_in' => array(
					'sql' => Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('m_id') 
						. ' IN (' . Query::createPdoInString($underClientIds) . ')'
					,
					'value' => '',
					'comparison' => ''
				)
			);
		} else {
			$dataArray = array(
				'_' . $this->clientDeliveryTable . '_m_id' => array(
					'sql' => Query::masketField($this->clientDeliveryTable) . '.' . Query::masketField('m_id'),
					'value' => $clientId,
					'comparison' => 'integerEqual'
				),
			);
		}
		
		return $dataArray;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setClientDeliveryTable($clientDeliveryTable) {
		$this->clientDeliveryTable = $clientDeliveryTable;
	}

	public function setClientDeliveryPrimaryField($clientDeliveryPrimaryField) {
		$this->clientDeliveryPrimaryField = $clientDeliveryPrimaryField;
	}

	public function setClientDeliveryFetchClass($clientDeliveryFetchClass) {
		$this->clientDeliveryFetchClass = $clientDeliveryFetchClass;
	}
	
	
	public function setDeliverySystemTable($deliverySystemTable) {
		$this->deliverySystemTable = $deliverySystemTable;
	}

	public function setDeliverySystemPrimaryField($deliverySystemPrimaryField) {
		$this->deliverySystemPrimaryField = $deliverySystemPrimaryField;
	}

	public function setDeliverySystemFetchClass($deliverySystemFetchClass) {
		$this->deliverySystemFetchClass = $deliverySystemFetchClass;
	}


	public function setDeliverySystemDistributorTable($deliverySystemDistributorTable) {
		$this->deliverySystemDistributorTable = $deliverySystemDistributorTable;
	}

	public function setDeliverySystemDistributorPrimaryField($deliverySystemDistributorPrimaryField) {
		$this->deliverySystemDistributorPrimaryField = $deliverySystemDistributorPrimaryField;
	}

	public function setDeliverySystemDistributorFetchClass($deliverySystemDistributorFetchClass) {
		$this->deliverySystemDistributorFetchClass = $deliverySystemDistributorFetchClass;
	}


	public function setClientClickProfilesTable($clientClickProfilesTable) {
		$this->clientClickProfilesTable = $clientClickProfilesTable;
	}

	public function setClientClickProfilesPrimaryField($clientClickProfilesPrimaryField) {
		$this->clientClickProfilesPrimaryField = $clientClickProfilesPrimaryField;
	}

	public function setClientClickProfilesFetchClass($clientClickProfilesFetchClass) {
		$this->clientClickProfilesFetchClass = $clientClickProfilesFetchClass;
	}

	
	public function setClickProfilesTable($clickProfilesTable) {
		$this->clickProfilesTable = $clickProfilesTable;
	}

	public function setClickProfilesPrimaryField($clickProfilesPrimaryField) {
		$this->clickProfilesPrimaryField = $clickProfilesPrimaryField;
	}

	public function setClickProfilesFetchClass($clickProfilesFetchClass) {
		$this->clickProfilesFetchClass = $clickProfilesFetchClass;
	}
	

	public function setMandantTable($mandantTable) {
		$this->mandantTable = $mandantTable;
	}
	
	public function setMandantPrimaryField($mandantPrimaryField) {
		$this->mandantPrimaryField = $mandantPrimaryField;
	}
	
	public function setMandantFetchClass($mandantFetchClass) {
		$this->mandantFetchClass = $mandantFetchClass;
	}

       	public function setRecipientFilterTable($recipientFilterTable) {
		$this->recipientFilterTable = $recipientFilterTable;
	}

	public function setRecipientFilterPrimaryField($recipientFilterPrimaryField) {
		$this->recipientFilterPrimaryField = $recipientFilterPrimaryField;
	}

	public function setRecipientFilterFetchClass($recipientFilterFetchClass) {
		$this->recipientFilterFetchClass = $recipientFilterFetchClass;
	}
        
        public function getDomainDeliveryTable() {
            return $this->domainDeliveryTable;
        }

        public function getDomainDeliveryPrimaryField() {
            return $this->domainDeliveryPrimaryField;
        }

        public function getDomainDeliveryFetchClass() {
            return $this->domainDeliveryFetchClass;
        }

        public function setDomainDeliveryTable($domainDeliveryTable) {
            $this->domainDeliveryTable = $domainDeliveryTable;
        }

        public function setDomainDeliveryPrimaryField($domainDeliveryPrimaryField) {
            $this->domainDeliveryPrimaryField = $domainDeliveryPrimaryField;
        }

        public function setDomainDeliveryFetchClass($domainDeliveryFetchClass) {
            $this->domainDeliveryFetchClass = $domainDeliveryFetchClass;
        }


}
