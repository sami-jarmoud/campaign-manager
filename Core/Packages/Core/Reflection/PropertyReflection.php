<?php
namespace Packages\Core\Reflection;


/**
 * Description of PropertyReflection
 *
 * @author Cristian.Reus
 */
class PropertyReflection extends \ReflectionProperty {

	/**
	 * docCommentParser
	 * 
	 * @var \Packages\Core\Reflection\DocCommentParser
	 */
	protected $docCommentParser;
	
	
	
	/**
	 * isTaggedWith
	 *
	 * @param string $tag
	 * @return boolean
	 */
	public function isTaggedWith($tag) {
		$result = $this->getDocCommentParser()->isTaggedWith($tag);
		
		return $result;
	}

	/**
	 * getTagsValues
	 *
	 * @return array
	 */
	public function getTagsValues() {
		return $this->getDocCommentParser()->getTagsValues();
	}

	/**
	 * getTagValues
	 *
	 * @param string $tag
	 * @return array
	 */
	public function getTagValues($tag) {
		return $this->getDocCommentParser()->getTagValues($tag);
	}

	/**
	 * getValue
	 *
	 * @param object $object
	 * @return mixed
	 * @throws \Packages\Core\Reflection\Exception
	 */
	public function getValue($object = NULL) {
		if (!\is_object($object)) {
			throw new \Packages\Core\Reflection\Exception('object is of type ' . \gettype($object) . ', instance of class ' . $this->class . ' expected.', 1446197344);
		}
		
		if ($this->isPublic()) {
			return parent::getValue($object);
		}
		
		if ($this->isPrivate()) {
			throw new \Packages\Core\Reflection\Exception('Cannot return value of private property "' . $this->name . '.', 1446197386);
		}
		
		parent::setAccessible(TRUE);
		
		return parent::getValue($object);
	}

	/**
	 * getDocCommentParser
	 *
	 * @return \Packages\Core\Reflection\DocCommentParser
	 */
	protected function getDocCommentParser() {
		if (!\is_object($this->docCommentParser)) {
			$this->docCommentParser = new \Packages\Core\Reflection\DocCommentParser();
			$this->docCommentParser->parseDocComment($this->getDocComment());
		}
		
		return $this->docCommentParser;
	}
}