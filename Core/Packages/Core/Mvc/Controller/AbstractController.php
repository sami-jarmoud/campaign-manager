<?php
namespace Packages\Core\Mvc\Controller;

use Packages\Core\ClassLoader;
use Packages\Core\Utility\GeneralUtility;
use Packages\Core\Utility\FileUtility;


/**
 * Description of AbstractController
 *
 * @author Cristian.Reus
 */
abstract class AbstractController {
	
	/**
	 * repositoryParameter
	 * 
	 * @var array
	 */
	protected $settings = array();
	
	/**
	 * moduleRequestDataArray
	 * 
	 * @var array
	 */
	protected $moduleRequestDataArray = array();
	
	/**
	 * mailEngine
	 * 
	 * @var \Packages\Core\Mail\AbstractMail 
	 */
	protected $mailEngine = NULL;
	
	/**
	 * repository
	 * 
	 * @var \Packages\Core\Persistence\AbstractRepository
	 */
	protected $repository = NULL;
	
	/**
	 * systemRepository
	 * 
	 * @var \Packages\Core\Domain\Repository\SystemRepository
	 */
	protected $systemRepository = NULL;
	
	/**
	 * clientEntity
	 * 
	 * @var \Packages\Core\Domain\Model\ClientEntity
	 */
	protected $clientEntity;
	
	/**
	 * loggedUserEntity
	 * 
	 * @var \Packages\Core\Domain\Model\UserEntity
	 */
	protected $loggedUserEntity;
	
	/**
	 * logger
	 * 
	 * @var \Packages\Core\Log\AbstractLog
	 */
	protected $logger = NULL;
	
	/**
	 * contentResultDataArray
	 * 
	 * @var \Packages\Core\Persistence\ArrayIterator
	 */
	protected $contentResultDataArray;
	
	/**
	 * defaultViewDataArray
	 * 
	 * @var array
	 */
	protected $defaultViewDataArray = array();
	
	
	
	/**
	 * __construct
	 * 
	 * @param array $settingsDataArray
	 * @param array $moduleRequestDataArray
	 * @return void
	 */
	public function __construct(array $settingsDataArray, array $moduleRequestDataArray) {
		$this->settings = $settingsDataArray;
		$this->moduleRequestDataArray = $moduleRequestDataArray;
		
		$this->contentResultDataArray = new \Packages\Core\Persistence\ArrayIterator();
	}
	
	/**
	 * init
	 * 
	 * @param string $repositoryClass
	 * @param \Packages\Core\Manager\Database\AbstractConnection $databaseHandle
	 * @param \Packages\Core\Mail\AbstractMail $mailEngine
	 * @param \Packages\Core\Domain\Repository\SystemRepository $systemRepository
	 * @param \Packages\Core\Domain\Model\ClientEntity $clientEntity
	 * @param \Packages\Core\Domain\Model\UserEntity $loggedUserEntity
	 * @return void
	 */
	public function init($repositoryClass, \Packages\Core\Manager\Database\AbstractConnection $databaseHandle, \Packages\Core\Mail\AbstractMail $mailEngine, \Packages\Core\Domain\Repository\SystemRepository $systemRepository, \Packages\Core\Domain\Model\ClientEntity $clientEntity, \Packages\Core\Domain\Model\UserEntity $loggedUserEntity) {
		if (\strlen($repositoryClass) > 0) {
			if (!\class_exists($repositoryClass)) {
				\Autoloader::loadModules();
			}

			$this->repository = GeneralUtility::makeInstance($repositoryClass);

			if (isset($this->settings['repositorySettings']) 
				&& \count($this->settings['repositorySettings'])
			) {
				foreach ($this->settings['repositorySettings'] as $key => $item) {
					if (\is_array($item)) {
						foreach ($item as $name => $value) {
							$this->repository->_setProperty($name, $value);
						}
					}
				}
			}

			$this->repository->setDatabaseHandle($databaseHandle);
			$this->repository->init();
		}
		
		$this->mailEngine = $mailEngine;
		$this->systemRepository = $systemRepository;
		$this->clientEntity = $clientEntity;
		$this->loggedUserEntity = $loggedUserEntity;
		
		$this->logger = GeneralUtility::makeInstance('\\Packages\\Core\\Log\\FirePhpLog');
		$this->logger->init();
		
		/**
		 * postInitAction
		 */
		$this->postInitAction();
	}
	
	/**
	 * initializeAction
	 * 
	 * @return void
	 */
	public function initializeAction() {
		$additionalArgruments = \func_get_args();
		if (\count($additionalArgruments) > 0) {
			foreach ($additionalArgruments as $object) {
				$className = ClassLoader::getClassNameWithoutNamespace(\get_class($object));
				
				if (\strlen($className) > 0) {
					$this->{$className} = $object;
				}
			}
		}
		
		$this->postInitializeAction();
	}
	
	/**
	 * postInitializeAction
	 * 
	 * @return void
	 */
	public function postInitializeAction() {
	}
	
	
	/**
	 * postInitAction
	 * 
	 * @return void
	 */
	abstract protected function postInitAction();
	
	/**
	 * initDefaultViewDataArray
	 * 
	 * @return void
	 */
	abstract protected function initDefaultViewDataArray(array $actionRequestDataArray);
	
	
	/**
	 * initView
	 * 
	 * @param array $viewDataArray
	 * @return void
	 */
	protected function initView(array $viewDataArray) {
		$viewFilename = GeneralUtility::$namespaceDivider . 'Modules' 
			. GeneralUtility::$namespaceDivider . \ucfirst($this->moduleRequestDataArray['_module']) 
			. GeneralUtility::$namespaceDivider . 'View' 
			. \DIRECTORY_SEPARATOR . 'Templates' 
			. GeneralUtility::$namespaceDivider . \ucfirst($this->moduleRequestDataArray['_action']) . \ucfirst($this->moduleRequestDataArray['_module']) . 'View'
		;
		
		$viewFilename = \str_replace('Modules\\', '', $viewFilename);
		$viewFilename = GeneralUtility::getCorrectFileName($viewFilename);
		
		if (FileUtility::isReadable(\DIR_Modules . $viewFilename . '.php')) {
			try {
				if (\count($this->defaultViewDataArray) > 0) {
					foreach ($this->defaultViewDataArray as $key => $item) {
						${$key} = $item;
					}
				}
				
				foreach ($viewDataArray as $key => $item) {
					${$key} = $item;
				}
				unset($viewDataArray);
				
				require_once(\DIR_Modules . $viewFilename . '.php');
			} catch (\Exception $e) {
				throw $e;
			}
		}
	}
	
	/**
	 * addErrorMessages
	 * 
	 * @param array $messageDataArray
	 * @return void
	 */
	protected function addErrorMessages(array $messageDataArray) {
		if ($this->contentResultDataArray->offsetExists('error')) {
			$allValues = $this->contentResultDataArray->offsetGet('error');
			
			$this->contentResultDataArray->offsetSet(
				'error',
				\array_merge($allValues, $messageDataArray)
			);
		} else {
			$this->contentResultDataArray->offsetSet(
				'error',
				$messageDataArray
			);
		}
	}
	
	/**
	 * addSuccessMessages
	 * 
	 * @param array $messageDataArray
	 * @return void
	 */
	protected function addSuccessMessages(array $messageDataArray) {
		if ($this->contentResultDataArray->offsetExists('success')) {
			$allValues = $this->contentResultDataArray->offsetGet('success');
			
			$this->contentResultDataArray->offsetSet(
				'success',
				\array_merge($allValues, $messageDataArray)
			);
		} else {
			$this->contentResultDataArray->offsetSet(
				'success',
				$messageDataArray
			);
		}
	}
	
	/**
	 * viewContentResultDataArray
	 * 
	 * @return void
	 */
	protected function viewContentResultDataArray() {
		\header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		\header('Last-Modified: ' . \gmdate('D, d M Y H:i:s') . ' GMT');
		\header('Cache-Control: no-store, no-cache, must-revalidate');
		\header('Cache-Control: post-check=0, pre-check=0', false);
		\header('Pragma: no-cache');
		
		echo \json_encode(
			array(
				'jsonData' => $this->contentResultDataArray
			)
		);
	}

}