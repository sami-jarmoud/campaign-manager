<?php
namespace Packages\Core\Factory;

use Packages\Core\Utility\GeneralUtility;


/**
 * Description of DeliverySystem
 *
 * @author Cristian.Reus
 */
final class DeliverySystem {
	
	/**
	 * campaignClassName
	 * 
	 * @var string
	 * @deprecated
	 */
	protected static $campaignClassName = '\\Packages\\Api\\DeliverySystem\\Campaign\\';
	
	/**
	 * deliveryClassName
	 * 
	 * @var string
	 */
	protected static $deliveryClassName = '\\Packages\\Api\\DeliverySystem\\';
	
	
	
	/**
	 * getAndInitCampaignInstance
	 * 
	 * @param string $deliverySystemName
	 * @param \Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity
	 * @return \Packages\Api\DeliverySystem\Campaign\AbstractCampaign
	 * @deprecated use: getAndInitDeliverySystemInstance
	 */
	public static function getAndInitCampaignInstance($deliverySystemName, \Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity) {
		$deliverySystem = GeneralUtility::makeInstance(self::$campaignClassName . $deliverySystemName);
		/* @var $deliverySystem \Packages\Api\DeliverySystem\Campaign\AbstractCampaign */
		$deliverySystem->init($clientDeliveryEntity);
		
		return $deliverySystem;
	}
	
	/**
	 * getAndInitDeliverySystemInstance
	 * 
	 * @param string $actionName
	 * @param string $deliverySystemName
	 * @param \Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity
	 * @return \Packages\Api\DeliverySystem\Campaign\AbstractCampaign
	 */
	public static function getAndInitDeliverySystemInstance($actionName, $deliverySystemName, \Packages\Core\Domain\Model\ClientDeliveryEntity $clientDeliveryEntity) {
		$deliverySystem = GeneralUtility::makeInstance(self::$deliveryClassName . $actionName . GeneralUtility::$namespaceDivider . $deliverySystemName);
		$deliverySystem->init($clientDeliveryEntity);
		
		return $deliverySystem;
	}
}
