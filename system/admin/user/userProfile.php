<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

$mandant = $_SESSION['mandant'];

include('../../db_pep.inc.php');
include('../../library/ems_package.php');
include('../../library/mandant.class.php');

$mID = $_SESSION['mID'];
$m = new mandant();
$mandantDataArr = $m->getMandantData($mID);
$mandantName = $mandantDataArr['Mandant'];

if ($_GET['userID']) {
    $userid = $_GET['userID'];
    $qry = mysql_query(
        "SELECT * FROM benutzer WHERE benutzer_id = '$userid'",
        $verbindung_pep
    );

    $z = mysql_fetch_array($qry);
    $benutzername = $z['benutzer_name'];
    $pw = $z['pw'];
    $email = $z['email'];
    $vorname = $z['vorname'];
    $nachname = $z['nachname'];
    $anrede = $z['anrede'];
    $abteilung = $z['abteilung'];
    $telefon = $z['telefon'];

    switch ($abteilung) {
        case 'g': $selected_abt_g = "selected";
            break;
        case 'v': $selected_abt_v = "selected";
            break;
        case 't': $selected_abt_t = "selected";
            break;
        case 'b': $selected_abt_b = "selected";
            break;
        case 's': $selected_abt_s = "selected";
            break;
    }

    $display = "style='display:none'";
} else {
    $display = '';
}

if ($anrede == 'Herr' || $anrede == '') {
    $selected_m = 'checked';
} else {
    $selected_w = 'checked';
}

function zugangMandant() {
    global $userid;
    
    $mID = $_SESSION['mID'];
    $userID = $userid;

    $m = new emsPackage();
    $untermandantArr = $m->getUntermandantID($mID);

    $cnt = count($untermandantArr);
    if ($cnt > 0) {
        $user_untermandantArr = $m->getUserUntermandantID($userID);

        $i = 1;
        foreach ($untermandantArr as $uID) {
            $checked = '';

            if (in_array($uID, $user_untermandantArr)) {
                $checked = 'checked="checked"';
            }

            if ($i == 3) {
                $i = 1;
            }
            
            $m_name = $m->getUntermandantName($uID);
            $tr .= "
                <tr class='bg" . $i . "'>
                    <td align='right' class='info'>" . $m_name . "</td>
                    <td align='left'><input type='checkbox' name='untermandant[]' value='" . $uID . "' " . $checked . "/></td>
                </tr>"
            ;
            $i++;
        }
    } else {
        $tr = "
            <tr class='bg" . $i . "'>
                <td align='center' colspan='2'>Keine Mandanten vorhanden</td>
            </tr>"
        ;
    }

    return $tr;
}

function zugangModul() {
    global $userid;
    
    $mID = $_SESSION['mID'];
    $userID = $userid;

    $m = new emsPackage();
    $packagesArr = $m->getPackageID($mID);

    $user_package = $m->getUserModules($userID);

    $i = 1;
    foreach ($packagesArr as $pID) {
        $checked = '';
        if (in_array($pID, $user_package)) {
            $checked = 'checked="checked"';
        }

        if ($i == 3) {
            $i = 1;
        }
        
        $m_name = $m->getPackageName($pID);
        $tr .= "
            <tr class='bg" . $i . "'>
                <td align='right' class='info'>" . $m_name . "</td>
                <td align='left'><input type='checkbox' name='module[]' value='" . $pID . "' " . $checked . "/></td>
                <td align='left'><a href='#' class='rechteButton'>Rechte zuweisen</a></td>
            </tr>"
        ;
        $i++;
    }

    $checkeda = '';
    if (in_array('999', $user_package)) {
        $checkeda = 'checked="checked"';
    }
    
    $tr .= "
        <tr class='bg" . $i . "'>
            <td align='right' class='info' style='color:red'>Administration</td>
            <td align='left'><input type='checkbox' name='module[]' value='999' " . $checkeda . "/></td>
            <td></td>
        </tr>"
    ;

    return $tr;
}
?>
<input type="hidden" name="userID" value="<?php print $userid; ?>" />
<div id="tab_user" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#userTab1"><em>Allgemein</em></a></li>
        <li><a href="#userTab2"><em>Mandanten</em></a></li>
        <li><a href="#userTab3"><em>Module &amp; Rechte</em></a></li>
    </ul>
    <div class="yui-content" style="margin:0px;padding:0px;">
        <div id="userTab1">
            <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                <tr class='details_hd_info'>
                    <td class='details_hd_info'>Login</td>
                    <td class='details_hd_info'></td>
                </tr>
                <tr>
                    <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                </tr>
                <tr>
                    <td align="right" class="info">Anmeldename<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" name="user_login" id="user_login" style="width:175px" value="<?php print $benutzername; ?>" /></td>
                </tr>
                <tr class="bg2" id="pw_old">
                    <td align="right" class="info">Passwort</td>
                    <td align="left"><input type="password" name="user_pw" id="user_pw" style="width:175px" class="bg2" value="<?php print $pw; ?>" readonly="readonly"/> <a href="#" class="rechteButton" onClick="showPW();return false;" style="margin-left:5px" title="Passwort &auml;ndern">&auml;ndern</a></td>
                </tr>
                <tr id="pwNew" class="bg2">
                    <td align="right" class="info">Passwort<span style="color:red;">*</span></td>
                    <td align="left"><input type="password" name="user_pw1" id="user_pw1" style="width:175px" value="" /></td>
                </tr>
                <tr id="pwNew2" class="bg2">
                    <td align="right" class="info">Passwort best&auml;tigen<span style="color:red;">*</span></td>
                    <td align="left"><input type="password" name="user_pw2" id="user_pw2" style="width:175px" value="" /></td>
                </tr>
                <tr class='details_hd_info'>
                    <td class='details_hd_info'>Details</td>
                    <td class='details_hd_info'></td>
                </tr>
                <tr class="bg2">
                    <td align="right" class="info">Abteilung</td>
                    <td align="left">
                        <select name="abteilung" id="abteilung" style="width:175px">
                            <option value="">-- bitte ausw&auml;hlen --</option>
                            <option value="g" <?php print $selected_abt_g; ?>>Gesch&auml;ftsf&uuml;hrung</option>
                            <option value="v" <?php print $selected_abt_v; ?>>Vertrieb</option>
                            <option value="t" <?php print $selected_abt_t; ?>>Technik</option>
                            <option value="b" <?php print $selected_abt_b; ?>>Buchhaltung</option>
                            <option value="s" <?php print $selected_abt_s; ?>>Support</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="info">Anrede</td>
                    <td align="left"><input type="radio" name="user_anrede" value="Herr" <?php print $selected_m; ?>/> Herr  &nbsp;&nbsp;<input type="radio" name="user_anrede" value="Frau" <?php print $selected_w; ?>/> Frau</td>
                </tr>
                <tr class="bg2">
                    <td align="right" class="info">Vorname</td>
                    <td align="left"><input type="text" name="user_vorname" style="width:175px" value="<?php print $vorname; ?>" /></td>
                </tr>
                <tr>
                    <td align="right" class="info">Nachname</td>
                    <td align="left"><input type="text" name="user_nachname" style="width:175px" value="<?php print $nachname; ?>" /></td>
                </tr>
                <tr class="bg2">
                    <td align="right" class="info">Email</td>
                    <td align="left"><input type="text" name="user_email" style="width:175px" value="<?php print $email; ?>" /></td>
                </tr>
                <tr>
                    <td align="right" class="info">Handy</td>
                    <td align="left"><input type="text" name="user_telefon" style="width:175px" value="<?php print $telefon; ?>" /></td>
                </tr>                
            </table>
        </div>
        <div id="userTab2">
            <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                <tr class='details_hd_info'>
                    <td align="right">Mandant</td>
                    <td align="left" style="width:40%;">Zugang</td>
                </tr>
                <tr class='bg2'>
                    <td align='right' class='info' style="color:#666666"><?php print $mandantName; ?></td>
                    <td align='left'><input type='checkbox' onclick="alert('Dies ist der Hauptmandant und daher nicht ver&auml;nderbar.');this.checked=true" checked/></td>
                </tr>
                <?php print zugangMandant(); ?>
            </table>
        </div>
        <div id="userTab2">
            <table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
                <tr class='details_hd_info'>
                    <td align="right">Modul</td>
                    <td align="left">Zugang</td>
                    <td align="left" style="width:30%;">Rechtevergabe</td>
                </tr>
                <?php print zugangModul(); ?>
            </table>
        </div>
    </div>
</div>

<?php
if (!$userid) {
    print '<script type="text/javascript">document.getElementById(\'pw_old\').style.display = \'none\';</script>';
} else {
    print '<script type="text/javascript">document.getElementById(\'pwNew\').style.display = \'none\';document.getElementById(\'pwNew2\').style.display = \'none\';</script>';
}
?>

<script type="text/javascript">
    function showPW() {
        document.getElementById('pwNew').style.display = '';
        document.getElementById('pwNew2').style.display = '';
    }

    var tabView_user = new YAHOO.widget.TabView('tab_user');
</script>