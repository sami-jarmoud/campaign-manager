<?php
session_start();
$export_ordner = "temp/";
$temp_datei = "test_export_bl.txt";
$datensaetze = '';
$date = date("Y-m-d",time());

if (file_exists($export_ordner.$temp_datei)) {
	unlink($export_ordner.$temp_datei);
}

$filehandle = fopen($export_ordner.$temp_datei, "a");
$datensaetze = $_SESSION['datensaetze'];

fwrite($filehandle, $datensaetze);
fclose($filehandle);

header("Content-type: application/octet-stream"); 
header("Content-disposition: attachment; filename=blacklist_".$date.".csv");
readfile($export_ordner.$temp_datei); 
?>
