<?php
function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}

function datum_de($date_time, $form) {
    if (empty($form)) {
        $form = '';
    }
    
    $date_1 = explode(' ', $date_time);
    $datum = explode('-', $date_1[0]);
    $jahr = $datum[0];
    $monat = $datum[1];
    $tag = $datum[2];

    $zeit = $date_1[1];
    $zeit_teile = explode(':', $date_1[1]);
    if ($form == 'DE' || $zeit_teile[0] == '00' || $form == 'zeit') {
        $h = $zeit_teile[0];
    } else {
        $h = $zeit_teile[0];
    }
    $m = $zeit_teile[1];
    $zeit = $h . ':' . $m;
    $de = $tag . '.' . $monat . '.' . $jahr;

    $dat_zeit = $de . ' ' . $zeit;
    if ($form == 'dat') {
        return $de;
    } elseif ($form == '' || $form == 'DE') {
        return $dat_zeit;
    } elseif ($form == 'zeit') {
        return $zeit;
    } elseif ($form == 'zeit_bm') {
        return $zeit;
    }
}

function getUserDataById(array $userDataArray, $userId) {
    $result = '';
    
    if (array_key_exists($userId, $userDataArray)) {
        $result = utf8_decode($userDataArray[$userId]['name']);
    }
    
    return $result;
}

function getClientDataById(array $allClientsDataArray, $clientId) {
	$result = '';
	if (\array_key_exists($clientId, $allClientsDataArray)) {
		$result = $allClientsDataArray[$clientId];
	}
	
	return $result;
}



header('Content-Type: text/html; charset=ISO-8859-1');


require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$dbHostEms = 'localhost';
$mandant = $_SESSION['mandant'];

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'export') {
        header('Location: export.php');
    }
}


require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

include('../library/mandant.class.php');

$mandantManager = new mandant();
$_SESSION['datensaetze'] = '';


// debug
#$debugLogManager->logData('SESSION', $_SESSION);

if (isset($_SESSION['error'])) {
    print $_SESSION['error'];
}
$_SESSION['error'] = '';

$allClientsDataArray = \array_flip($_SESSION['mandant_all_id_Arr']);
$debugLogManager->logData('allClientsDataArray', $allClientsDataArray);

$verbindung_m = mysql_connect($dbHostEms, 'dbo49323410', '5C4i7E9v') or die("FEHLER: Keine Verbindung zur Abmelder-Datenbank!");
mysql_select_db('peppmt', $verbindung_m) or die("Keine Abmelder-Datenbank!");

$mID_qry = mysql_query(
    "SELECT `id` FROM mandant WHERE abkz = '$mandant' ",
    $verbindung_m
);
$mIDz = mysql_fetch_array($mID_qry);
$mID = $mIDz['id'];
// debug
$debugLogManager->logData('mID', $mID);

$maspIDArr = $mandantManager->getMandantMASP($mID);
$sqlAsp = '';
if (count($maspIDArr) > 0) {
    $sqlAsp .= " AND ( ";
    
    foreach ($maspIDArr as $aspID) {
        $sqlAsp .= " mandant_asp_id = $aspID OR ";
    }
    
    $sqlAsp = substr($sqlAsp, 0, -3);
    $sqlAsp .= " ) ";
}
// debug
$debugLogManager->logData('maspIDArr', $maspIDArr);


include('../db_blacklist_connect.inc.php');


if (isset($_POST['suchwort'])) {
    $s = utf8_decode($_POST['suchwort']);
    if ($s != '') {
        $suchbegriff1 = " AND Email LIKE '%" . $s . "%' ";
    }
} else {
    $suchbegriff1 = '';
}

if (isset($_POST['bearbeiter_bl'])) {
    $sb = $_POST['bearbeiter_bl'];
    if ($sb != '') {
        $suchbegriff4 = " AND Bearbeiter = '$sb' ";
    }
} else {
    $suchbegriff4 = '';
}


if (isset($_POST['beginn'])) {
    $in = $_POST['beginn'];
    if ($in != '') {
        $suchbegriff2 = " AND Datum >= '" . $in . " 00:00:00' ";
    }
} else {
    $suchbegriff2 = '';
}

if (isset($_POST['ende'])) {
    $out = $_POST['ende'];
    if ($out != '') {
        $suchbegriff3 = " AND Datum <= '" . $out . " 23:59:59' ";
    }
} else {
    $suchbegriff3 = '';
}

$suchbegriff = $suchbegriff1 . $suchbegriff2 . $suchbegriff3 . $suchbegriff4;

if ($suchbegriff != '') {
    $_SESSION['suchbegriff'] = $suchbegriff;
}

if (isset($_SESSION['suchbegriff'])) {
    $suchbegriff = $_SESSION['suchbegriff'];
    if ($suchbegriff != '') {
        $_SESSION['suchbegriff'] = $suchbegriff;
    }
}

if ($suchbegriff1 == '' && $suchbegriff2 == '' && $suchbegriff3 == '' && $suchbegriff4 == '') {
    $_SESSION['suchbegriff'] = '';
    $suchbegriff = '';
}
// debug
$debugLogManager->logData('suchbegriff', $suchbegriff);

$anzahl_eintraege = 100;
$limit = "LIMIT 0," . $anzahl_eintraege;

$abmelder_daten_all = mysql_query(
    "SELECT * FROM Blacklist WHERE Email != '' " . $suchbegriff,
    $verbindung_bl
);
$abmeldungen_all = mysql_num_rows($abmelder_daten_all);
$seiten_all = ceil($abmeldungen_all / $anzahl_eintraege);
// debug
$debugLogManager->logData('abmeldungen_all', $abmeldungen_all);


$abmelder_daten = mysql_query(
    "SELECT * FROM Blacklist WHERE Email != '' " . $suchbegriff . " ORDER BY Datum DESC " . $limit,
    $verbindung_bl
);


$userDataArray = array();
$userDataRes = mysql_query(
    "SELECT benutzer_id, CONCAT(`vorname`, ' ', `nachname`) as `name` FROM benutzer",
    $verbindung_m
);
while (($userRow = mysql_fetch_array($userDataRes)) !== false) {
    $userDataArray[$userRow['benutzer_id']] = $userRow;
}
// debug
$debugLogManager->logData('userDataArray', $userDataArray);


$offenGlobal = 0;
if (count($maspIDArr) > 0) {
    foreach ($maspIDArr as $aspID) {
        // debug
        $debugLogManager->beginGroup($aspID);
        
        $abmelder_daten_offen = mysql_query(
            "SELECT COUNT(*) as anzahl FROM asp_blacklist WHERE status = 0 AND mandant_asp_id = $aspID ",
            $verbindung_bl
        );
        $z = mysql_fetch_array($abmelder_daten_offen);
        // debug
        $debugLogManager->logData('abmelder_daten_offen', $z);
        
        $aspNameArr = $mandantManager->getMandantMASPname($aspID);
        // debug
        $debugLogManager->logData('aspNameArr', $aspNameArr);
        
        $abmeldungen_offen .= $aspNameArr['asp'] . ": " . $z['anzahl'] . " offen, ";
        $offenGlobal += $z['anzahl'];
        
        // debug
        $debugLogManager->endGroup();
    }
    
    $abmeldungen_offen = substr($abmeldungen_offen, 0, -2);
}


$eintraege = mysql_num_rows($abmelder_daten);
// debug
$debugLogManager->logData('eintraege', $eintraege);

if ($eintraege == 0) {
    $data_leer = "<tr><td height='30' colspan='30' align='center'><span style='color:#666666'>Keine Eintr&auml;ge gefunden</span</td></tr>";
}

$j = NULL;
$data = NULL;
if ($abmeldungen_all > 0) {
    while ($abmz = mysql_fetch_array($abmelder_daten)) {
        $processed_at = $abmz['processed_at'];
        $created_at = $abmz['Datum'];
        $status = $abmz['status'];

        if ($status == 1) {
            $status_bez = '<img src="img/Tango/16/actions/dialog-apply.png" title="geblacklistet" />';
        } else {
            $status_bez = '<img src="img/Tango/16/actions/appointment-new.png" title="in Warteschleife" />';
        }

        if ($offenGlobal == 0) {
            $status_bez = '<img src="img/Tango/16/actions/dialog-apply.png" title="geblacklistet" />';
        }

        if ($processed_at == 0) {
            $processed_at = 'in Bearbeitung';
            $status_bez = '<img src="img/Tango/16/actions/appointment-new.png" title="in Warteschleife" />';
        } else {
            $processed_at = datum_de($processed_at, '');
            $status_bez = '<img src="img/Tango/16/actions/dialog-apply.png" title="geblacklistet" />';
        }

        if ($j == 3 || !$j) {
            $j = 1;
        }
        
        if ($j == 1) {
            $bg_color = '#FFFFFF';
        } elseif ($j == 2) {
            $bg_color = '#E4E4E4';
        }

        $data .= '
            <tr style="background-color:' . $bg_color . '">
                <td align="center">' . $status_bez . '</td>
                <td style="font-weight:bold">' . $abmz['Email'] . '</td>
                <td>' . datum_de($created_at, "") . '</td>
                <td>' . $processed_at . '</td>
				<td>' . getClientDataById(
					$allClientsDataArray,
					intval($abmz['mandant'])
				) . '</td>
                <td>' . getUserDataById(
                    $userDataArray,
                    $abmz['Bearbeiter']
                ) . '</td>
                <td>' . utf8_decode($abmz['Grund']) . '</td>
                <td>' . utf8_decode($abmz['Notizen']) . '</td>
                <td></td>
            </tr>
        ';
        
        $j++;
    }

    $datensaetze = '';
    $datensaetze = "Email\r\n";
    while ($abmz_all = mysql_fetch_array($abmelder_daten_all)) {
        if (empty($abmz_all['Notiz'])) {
            $abmNotiz = '';
        } else {
            $abmNotiz = $abmz_all['Notiz'];
        }

        $datensaetze .= $abmz_all['Email'] . "\r\n";
    }
}

$_SESSION['datensaetze'] = $datensaetze;
?>
<div id="fakeContainer">
    <table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0" bgcolor="#F2F2F2">
        <tr>
            <th>Status</th>
            <th>Email</th>
            <th>Eingetragen am</th>
            <th>Geblacklistet am</th>
			<th>Mandant</th>
            <th>Bearbeiter</th>
            <th>Grund</th>
            <th>Notiz</th>
            <th style="width:100%"></th>
        </tr>
        <?php print $data; ?>
    </table>
</div>

<div style="background-color:#8e8901;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
    <div style="padding-top:5px;float:left;padding-left:15px">
        <?php print ' Letzten ' . $anzahl_eintraege . ' Eintr&auml;ge'; ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php
        if ($abmeldungen_all > 0) {
            print zahl_format($abmeldungen_all) . ' Blacklisteintr&auml;ge <a href="#" id="show_info" style="color:#9900FF">&nbsp;</a>';
        } else {
            print 'Keine Blacklisteintr&auml;ge vorhanden &nbsp;&nbsp;&nbsp;';
        }
        ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
print $abmeldungen_offen;
?>
    </div>
</div>

<input type="hidden" name="page_sub" id="page_sub" value="unsub" />
<?php
if ($abmeldungen_all > 0) {
    print '
        <script type="text/javascript">';
        if ($_SESSION['domain_found']) {
            print "alert('Folgende Domain(s) dürfen nicht in die Blacklist eingetragen werden:\\n" . $_SESSION['domain_found'] . "');";
        }

        print '
            wi = document.documentElement.clientWidth;
            hi = document.documentElement.clientHeight;

            if (uBrowser == "Firefox") {
                hi = hi-142;
                wi = wi-205;
            } else {
                hi = hi-143;
                wi = wi-205;
            }

            document.getElementById("fakeContainer").style.width = wi+"px";
            document.getElementById("fakeContainer").style.height = hi+"px";

            function Resize() {
                setRequest("blacklist/blacklist_tab.php","bl");
            }

            window.onresize = Resize;

            var mySt = new superTable("scrollTable", {
                cssSkin : "sSky",
                fixedCols : 0,
                headerRows : 1
            });
        </script>
        ';
}
$_SESSION['domain_found'] = '';
?>