<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface SessionWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:10 CET 2010
     */

    class BroadmailRpcSessionWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcSessionWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Session', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function setLocale( $p1) {
            return $this->_call('setLocale', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getLocale() {
            return $this->_call('getLocale', array('p1' => $this->sessionId));
        }

        function login( $p1,  $p2,  $p3) {
            return $this->_call('login', array('p1' => $this->_convert('java.lang.Long', $p1), 'p2' => $p2, 'p3' => $p3));
        }

        function setMediaType( $p1) {
            return $this->_call('setMediaType', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function getMediaType() {
            return $this->_call('getMediaType', array('p1' => $this->sessionId));
        }

        function logout() {
            return $this->_call('logout', array('p1' => $this->sessionId));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
