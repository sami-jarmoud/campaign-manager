<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface BasicReportingWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:20:50 CET 2010
     */

    class BroadmailRpcBasicReportingWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcBasicReportingWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'BasicReporting', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getUnsubscribes( $p1,  $p2) {
            return $this->_call('getUnsubscribes', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function getUnsubscribesByDomain( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getUnsubscribesByDomain', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getUnsubscribesByTime( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('getUnsubscribesByTime', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.util.Date', $p3), 'p5' => $this->_convert('java.util.Date', $p4), 'p6' => $this->_convert('java.lang.String', $p5)));
        }

        function getNumberOfEmails( $p1) {
            return $this->_call('getNumberOfEmails', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1)));
        }

        function getNumberOfEmailsByDomain( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getNumberOfEmailsByDomain', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getClicks( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getClicks', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getClicksByDomain( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getClicksByDomain', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getClicksByTime( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('getClicksByTime', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.util.Date', $p3), 'p5' => $this->_convert('java.util.Date', $p4), 'p6' => $this->_convert('java.lang.String', $p5)));
        }

        function getOpens( $p1,  $p2) {
            return $this->_call('getOpens', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function getOpensByDomain( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getOpensByDomain', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getOpensByTime( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('getOpensByTime', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.util.Date', $p3), 'p5' => $this->_convert('java.util.Date', $p4), 'p6' => $this->_convert('java.lang.String', $p5)));
        }

        function getResponses( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getResponses', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getResponsesByDomain( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getResponsesByDomain', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getResponsesByTime( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('getResponsesByTime', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2), 'p4' => $this->_convert('java.util.Date', $p3), 'p5' => $this->_convert('java.util.Date', $p4), 'p6' => $this->_convert('java.lang.String', $p5)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
