<?php
    /**
     * (C)opyright 2002-2009 optivo GmbH, Wallstraße 16, 10179 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface UnsubscribeWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Feb 24 16:21:13 CET 2010
     */

    class BroadmailRpcUnsubscribeWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcUnsubscribeWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Unsubscribe', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function add( $p1,  $p2) {
            return $this->_call('add', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function contains( $p1) {
            return $this->_call('contains', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function addAll( $p1,  $p2) {
            return $this->_call('addAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function remove( $p1) {
            return $this->_call('remove', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function containsAll( $p1) {
            return $this->_call('containsAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1)));
        }

        function removeAll( $p1) {
            return $this->_call('removeAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String[]', $p1)));
        }

        function getCount() {
            return $this->_call('getCount', array('p1' => $this->sessionId));
        }

        function containsByRecipientList( $p1,  $p2) {
            return $this->_call('containsByRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function containsAllByRecipientList( $p1,  $p2) {
            return $this->_call('containsAllByRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function addByMailId( $p1) {
            return $this->_call('addByMailId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.String', $p1)));
        }

        function removeByRecipientList( $p1,  $p2) {
            return $this->_call('removeByRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function removeAllByRecipientList( $p1,  $p2) {
            return $this->_call('removeAllByRecipientList', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>
