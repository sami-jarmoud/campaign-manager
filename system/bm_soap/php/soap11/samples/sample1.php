<?
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * Example illustrating the use of the BroadmailRpcFactory to access the SOAP 1.1 interface.
     *
     * @author Peter Romianowski
     * @version 1.0 2005-05-06
     */
?>

<html>
    <head>
        <title>optivo&reg; broadmail SOAP1.1 PHP Library Demo 1</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>
    <body>
    
        <h1>optivo&reg; broadmail SOAP1.1 PHP Library Demo 1</h1>
        
        This is a small demonstration on how to use this library to access the optivo&reg; broadmail functionality.<br>
        Please fill in the following fields and hit "submit". After that the optivo&reg; broadmail webservice 
        will be used to login and to retrieve a new unique id and the version number of the webservice.

<?

    // Include the library
    require_once('../broadmail_rpc.php');
    
    $submitted = $_GET['mandatorId'];
    if ($submitted) {
    
        // The form has been submitted.
        // Create a new factory and login.
        $factory = new BroadmailRpcFactory($_GET['mandatorId'], $_GET['username'], $_GET['password'], $_GET['endPoint']);
        
        // This is how error handling works. You should check the result of the getError() method
        // after each(!) call to a webservice.
        if ($factory->getError()) {
            die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
        }
        
        // Now create a SystemWebservice and create a new UUID.
        // Note: Since this library encapsulates all webservice methods you do not
        // need to supply the session id to every method call.
        $systemWs = $factory->newSystemWebservice();
        if ($systemWs->getError()) {
            die ('<h2>Error in SystemWebservice</h2><code>'.$systemWs->getError().'</code>');
        }
        $version = $systemWs->getVersion();
        if ($systemWs->getError()) {
            die ('<h2>Error in SystemWebservice</h2><code>'.$systemWs->getError().'</code>');
        }

        // Finally: Logout
        $factory->logout();

    }

?>
        
        <h2>Login Information</h2>
        <form method="GET">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>Endpoint</td>
                <td><input type="text" name="endPoint" value="http://api.broadmail.de/soap11" size="50"></td>
            </tr>
            <tr>
                <td>Mandator ID</td>
                <td><input type="text" name="mandatorId" size="50"></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username" size="50"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password" size="50"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit"></td>
            </tr>
        </table>

        </form>

        <? if ($submitted) { ?>
        
        <h2>Results</h2>
        
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td><code>Session ID</code></td>
                <td><code><?= $factory->sessionId ?></code></td>
            </tr>
            <tr>
                <td><code>Version</code></td>
                <td><code><?= $version ?></code></td>
            </tr>
        </table>

    </body>
</html>
<? } ?>