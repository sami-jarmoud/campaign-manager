<?
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * Example illustrating the use of the BroadmailRpcFactory to access the SOAP 1.1 interface.
     *
     * @author Peter Romianowski
     * @version 1.0 2005-05-06
     */
?>

<html>
    <head>
        <title>optivo&reg; broadmail SOAP1.1 PHP Library Demo 2</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </head>
    <body>
    
        <h1>optivo&reg; broadmail SOAP1.1 PHP Library Demo 2</h1>
        
        This is a small demonstration on how to use this library to access the optivo&reg; broadmail functionality.<br>
        This demonstration connects to the optivo&reg; webservice and retrieves the first 10 recipients of the
        specified recipient list.

<?

    // Innclude the library
    require_once('../broadmail_rpc.php');
    
    $submitted = $_GET['mandatorId'];
    if ($submitted) {
    
        // The form has been submitted.
        // Create a new factory and login.
        $factory = new BroadmailRpcFactory($_GET['mandatorId'], $_GET['username'], $_GET['password'], $_GET['endPoint']);
        
        // This is how error handling works. You should check the result of the getError() method
        // after each(!) call to a webservice.
        if ($factory->getError()) {
            die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
        }
        
        // Now create a RecipientWebservice and retrieve the email address and the creation date 
        // of the first 10 recipients.
        $recipientWs = $factory->newRecipientWebservice();
        $recipients = $recipientWs->getAllAdvanced(
            $_GET['recipientListId'],       // recipientListId
            array('email', 'created'),      // attributeNames
            0,                              // recipientFilterId (0 means "no filter")
            'created',                      // orderByAttribute 
            false,                          // ascendingSortOrder
            0,                              // pageStart
            10                              // pageSize
        );
        
        if ($recipientWs->getError()) {
            die ('<h2>Error in RecipientWebservice</h2><code>'.$recipientWs->getError().'</code>');
        }

        // Finally: Logout
        $factory->logout();

    } 

?>
        
        <h2>Login Information</h2>
        <form method="GET">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>Endpoint</td>
                <td><input type="text" name="endPoint" value="http://api.broadmail.de/soap11" size="50"></td>
            </tr>
            <tr>
                <td>Mandator ID</td>
                <td><input type="text" name="mandatorId" size="50"></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username" size="50"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password" size="50"></td>
            </tr>
        </table>

        <h2>Recipient Query</h2>
        Enter the id of a recipient list to display the last 10 entries.<br><br>
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td>Recipient List ID</td>
                <td><input type="text" name="recipientListId" size="50"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit"></td>
            </tr>
        </table>

        </form>

        <? if ($submitted) { ?>
        
        <h2>Results</h2>
        
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td><code>Session ID</code></td>
                <td><code><?= $factory->sessionId ?><code></td>
            </tr>
            <tr>
                <td valign="top"><code>Recipients<br>(<?= $_GET['recipientListId'] ?>)</code></td>
                <td valign="top">
                
                    <table cellspacing="0" cellpadding="0" border="1">
                        <tr>
                            <td><code>Email Address</code></td>
                            <td><code>Creation Date (ISO8601)</code></td>
                            <td><code>Creation Date (timestamp)</code></td>
                        </tr>
                        <?  for ($row = 0; $row < count($recipients); $row++) { 
                            
                        ?>
                        <tr>
                            <td><code><?= $recipients[$row][0] ?></code></td>
                            <td><code><?= $recipients[$row][1] ?></code></td>

                        <?  // Demonstrating the use of the $BroadmailRpcUtils class to convert an ISO8601 date ?>
                            <td><code><?= $BroadmailRpcUtils->parseIso8601($recipients[$row][1]) ?></code></td>
                        </tr>
                        <?  } ?>
                    </table>
                
                </td>
            </tr>
        </table>
        <? } ?>

    </body>
</html>
