<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface MailingReportingWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:26:02 CEST 2008
     */

    class BroadmailRpcMailingReportingWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcMailingReportingWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'MailingReporting', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function getClickCount( $p1,  $p2) {
            return $this->_call('getClickCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function getOverallRecipientCount( $p1) {
            return $this->_call('getOverallRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getSentRecipientCount( $p1) {
            return $this->_call('getSentRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getFailedRecipientCount( $p1) {
            return $this->_call('getFailedRecipientCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getOpenCount( $p1,  $p2) {
            return $this->_call('getOpenCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Boolean', $p2)));
        }

        function getLinkUrls( $p1) {
            return $this->_call('getLinkUrls', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getLinkNames( $p1) {
            return $this->_call('getLinkNames', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getClickCountByUrl( $p1,  $p2,  $p3) {
            return $this->_call('getClickCountByUrl', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2), 'p4' => $this->_convert('java.lang.Boolean', $p3)));
        }

        function getResponseCount( $p1,  $p2) {
            return $this->_call('getResponseCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function getUnsubscribeCount( $p1) {
            return $this->_call('getUnsubscribeCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>