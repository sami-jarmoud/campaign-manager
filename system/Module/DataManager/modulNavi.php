<li>
	<h3><img src="img/aesthetica/24/database.png" width="20" class="h3img" /> <span class="mymenuli"><div class="mymenulabel">Data Manager</div></span></h3>
	<div>
		<ul class="sub">
			<li>
				<a href="#" onClick="setRequest('limport/overview.php', 'dm_ov');">
					<img src="img/Oxygen/16/actions/help-about.png" class="liimg" /><div class="mymenulabela">Übersicht</div>
				</a>
			</li>
			<li>
				<a href="#" onClick="setRequest('limport/index.php?testimport=0', 'a');">
					<img src="img/aesthetica/16/database.png" class="liimg" /><div class="mymenulabela">Importe</div>
				</a>
			</li>
			<?php 
			if ($metaDB_test) {
			?>
				<li>
					<a href="#" onClick="setRequest('limport/index.php?testimport=1', 'a');">
						<img src="img/aesthetica/16/database.png" class="liimg" /><div class="mymenulabela">Testimport</div>
					</a>
				</li>
			<?php
			}
			?>
			<li>
				<a href="#" onClick="setRequest('limport/report/', 'a_rep');">
					<img src="img/Tango/16/mimetypes/office-spreadsheet.png" class="liimg" /><div class="mymenulabela">Reporting</div>
				</a>
			</li>
			<li>
				<a href="#" onClick="setRequest('limport/kunde/', 'pa');">
					<img src="img/Tango/16/mimetypes/office-contact.png" class="liimg" /><div class="mymenulabela">Lieferant</div>
				</a>
			</li>
		</ul>
	</div>
</li>