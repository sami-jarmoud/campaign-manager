<div id="menu_admin_user" style="display:none">
	<img src="img/seperator.png" style="float:left;margin-top:-1px;margin-bottom:-1px;" />
	<button class="yui-button" id="button_neu_user">
		<img src="img/icons/user_add.png" class="menu_img" /><div class='button_text'>Neuen Benutzer anlegen</div>
	</button>
</div>
<div id="menu_admin_parameter" style="display:none">
	<img src="img/seperator.png" style="float:left;margin-top:-1px;margin-bottom:-1px;" />
	<button class="yui-button" id="button_neu_parameter">
		<img src="img/icons/application_edit.png" class="menu_img" /><div class='button_text'>Parameter Bearbeiten</div>
	</button>
</div>
<div id="menu_admin_clickProfil" style="display:none">
	<img src="img/seperator.png" style="float:left;margin-top:-1px;margin-bottom:-1px;" />
	<button class="yui-button" id="button_neu_clickProfil">
		<img src="img/icons/application_edit.png" class="menu_img" /><div class='button_text'>Neues Klick Profil anlegen</div>
	</button>
</div>
<div id="menu_admin_dsdn" style="display:none">
	<img src="img/seperator.png" style="float:left;margin-top:-1px;margin-bottom:-1px;" />
	<button class="yui-button" id="button_neu_dsdn">
		<img src="img/icons/application_edit.png" class="menu_img" /><div class='button_text'>Neue Domain anlegen</div>
	</button>
</div>
<div id="menu_admin_dsd" style="display:none">
	<img src="img/seperator.png" style="float:left;margin-top:-1px;margin-bottom:-1px;" />
	<button class="yui-button" id="button_neu_dsd">
		<img src="img/icons/application_edit.png" class="menu_img" /><div class='button_text'>Neue Verteilerliste anlegen</div>
	</button>
</div>
