<div id="container_admin_user_del">
	<div class="panel_label">Administration</div>
	<div class="hd">
		<span class="hd_label">Benutzer l&ouml;schen</span>
		<span class="hd_sublabel"></span>
	</div>
    <div class="bd">
		M&ouml;chten Sie diesen Benutzer l&ouml;schen?
		<form id="userID_del_form" name="userID_del_form" action="admin/user/do_del.php" method="post">
			<input type="hidden" id="userID_del" name="userID_del" value="" />
		</form>
	</div>
</div>


<div id="container_admin_user">
	<div class="panel_label">Administration</div>
    <div class="hd">
		<span class="hd_label">Benutzerverwaltung</span>
		<span class="hd_sublabel"></span>
	</div>
    <div class="bd" style="text-align:left">
		<form id="userProfile" name="userForm" action="admin/user/do.php" method="post">
			<div style="height:300px"></div>
		</form>
	</div>
</div>
<div id="container_admin_clickProfil">
	<div class="panel_label">Administration</div>
    <div class="hd">
		<span class="hd_label">Klickprofilsverwaltung</span>
		<span class="hd_sublabel"></span>
	</div>
    <div class="bd" style="text-align:left">
		<form id="clickProfil" name="clickProfil" action="dispatch.php?_module=Admin&_controller=index&_action=createClickProfil" method="post">
			<div style="height:300px"></div>
		</form>
	</div>
</div>
<div id="container_admin_dsdn">
	<div class="panel_label">Administration</div>
    <div class="hd">
		<span class="hd_label">Domainverwaltung</span>
		<span class="hd_sublabel"></span>
	</div>
    <div class="bd" style="text-align:left">
		<form id="dsdn" name="dsdn" action="dispatch.php?_module=Admin&_controller=index&_action=createDeliverySystemDomain" method="post">
			<div style="height:300px"></div>
		</form>
	</div>
</div>
<div id="container_admin_dsd">
	<div class="panel_label">Administration</div>
    <div class="hd">
		<span class="hd_label">Verteilerlisteverwaltung</span>
		<span class="hd_sublabel"></span>
	</div>
    <div class="bd" style="text-align:left">
		<form id="dsd" name="dsd" action="dispatch.php?_module=Admin&_controller=index&_action=createDeliverySystemDistributor" method="post">
			<div style="height:300px"></div>
		</form>
	</div>
</div>
