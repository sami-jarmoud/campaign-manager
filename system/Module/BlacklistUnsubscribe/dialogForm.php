<?php
/* @var $clientManager \ClientManager */

/**
 * getClientUserSelectOptions
 * 
 * @param \ClientManager $clientManager
 * @return string
 */
function getClientUserSelectOptions(\ClientManager $clientManager) {
	$result = '';
	
	try {
		/**
		 * getActiveUsersDataItemsByClientId
		 */
		$userDataArray = $clientManager->getActiveUsersDataItemsByClientId(\RegistryUtils::get('clientEntity')->getId());
		foreach ($userDataArray as $userEntity) {
			/* @var $userEntity ^\UserEntity */
			if (\strlen($userEntity->getVorname()) > 0 
				|| \strlen($userEntity->getNachname()) > 0
			) {
				$result .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => $userEntity->getBenutzer_id()
					),
					$userEntity->getVorname() . ' ' . $userEntity->getNachname()
				);
			}
		}
		unset($userDataArray);
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}
	
	return $result;
}


// Abmelder
require_once('DialogForm/unsubscribe.php');

// Blacklist
require_once('DialogForm/blacklist.php');