<?php
/* @var $clientManager \ClientManager */
?>
<div id="suche_bl">
    <div class="panel_label">Blacklist-Abmelde-Manager</div>
    <div class="hd">
        <span class="hd_label">Suche</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left">
        <form id="search_bl_form" name="search_bl_form" action="#" method="post">
            <table cellpadding="5" cellspacing="0" border="0" style='font-size:11px;'>
                <tr>
                    <td><span class="info">Email / Domain:</span></td>
                    <td><input type="text" style="width:200px" id="suchwort_bl" name="suchwort_bl" /></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><span class="info">Bearbeiter:</span></td>
                    <td>
						<select style="width:200px" id="bearbeiter_bl" name="bearbeiter_bl">
							<option value=''>-- ausw&auml;hlen --</option>
							<?php
								echo \getClientUserSelectOptions($clientManager);
							?>
						</select>
					</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td><span class="info">Zeitraum:</span></td>
                    <td><input type="text" id="in_bl" name="in_bl" size="6" style="background-color:#F2F2F2;border:1px solid gray" /> - <input type="text" id="out_bl" name="out_bl" size="7" style="background-color:#F2F2F2;border:1px solid gray" /></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><div id="cal1Container_bl"></div></td>
                </tr>
            </table>
        </form>
    </div>
</div>


<div id="container_blacklist">
    <div class="panel_label">Blacklist-Abmelde-Manager</div>
    <div class="hd">
        <span class="hd_label">Neue Blacklisteintr&auml;ge</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <form name="blacklist" method="post" action="blacklist/blacklist.php">
            <div id="bl_new_entry"><div style="height:300px"></div></div>
        </form>
    </div>
</div>


<div id="container_blacklist_status">
    <div class="panel_label">Blacklist-Abmelde-Manager</div>
    <div class="hd">
        <span class="hd_label">Blacklist Status</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <div id="bl_status"><div style="height:300px;"></div></div>
    </div>
</div>