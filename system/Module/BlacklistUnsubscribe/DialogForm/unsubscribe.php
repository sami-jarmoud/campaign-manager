<div id="suche_unsub">
    <div class="panel_label">Blacklist-Abmelde-Manager</div>
    <div class="hd">
        <span class="hd_label">Suche</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left">
        <form id="search_unsub_form" name="search_unsub_form" action="#" method="post">
            <table cellpadding="5" cellspacing="0" border="0" style='font-size:11px;'>
                <tr>
                    <td><span class="info">Zeitraum:</span></td>
                    <td><input type="text" id="in_unsub" name="in_unsub" size="6" style="background-color:#F2F2F2;border:1px solid gray" /> - <input type="text" id="out_unsub" name="out_unsub" size="7" style="background-color:#F2F2F2;border:1px solid gray" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><div id="cal1Container_unsub"></div></td>
                </tr>
            </table>
        </form>
    </div>
</div>


<div id="container_unsubscribe">
    <div class="panel_label">Abmeldung</div>
    <div class="hd">
        <span class="hd_label">Neue Abmeldungen</span>
        <span class="hd_sublabel"></span>
    </div>
    <div class="bd" style="text-align:left;">
        <form name="unsubscribe" method="post" action="abmeldung/unsubscribe.php">
            <table border="0" cellpadding="2">
                <tr>
                    <td>
						Bitte f&uuml;gen Sie die zu abmeldenden Email-Adressen<br />einfach untereinander ein (max. 5000):<br /><br />
                        <textarea name="list" id="list_abmelder" cols="15" rows="15" style="width:325px;border:1px solid #9900FF" class="textfeld"></textarea>
					</td>
                </tr>
            </table>
        </form>
    </div>
</div>