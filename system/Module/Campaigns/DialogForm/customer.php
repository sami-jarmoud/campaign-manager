<div id="container_kunde">
    <div class="panel_label">Kundenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kunde</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="kampagne/View/Customers/action.php">
			<div id="kunde_content">
				<div style="height:400px"></div>
			</div>
		</form>
	</div>
</div>

<div id="container_del_kunde">
    <div class="panel_label">Kundenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kunde l&ouml;schen</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="kampagne/View/Customers/action.php">
			<input type="hidden" name="customer[kunde_id]" id="del_kunde_id" value="" />
			<input type="hidden" name="customerOrContactPerson[action]" id="action" value="deleteCustomerData" />
			Wollen Sie diesen Kunden wirklich l&ouml;schen?
		</form>
	</div>
</div>


<div id="container_kunde_ap">
    <div class="panel_label">Kundenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kontakt</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="kampagne/View/Customers/action.php">
			<div id="kunde_ap_content">
				<div style="height:400px"></div>
			</div>
		</form>
	</div>
</div>

<div id="container_del_kunde_ap">
    <div class="panel_label">Kundenverwaltung</div>
	<div class="hd">
        <span class="hd_label">Kontakt l&ouml;schen</span>
		<span class="hd_sublabel"></span>
    </div>
   	<div class="bd" style="text-align:left;">
		<form method="post" action="kampagne/View/Customers/action.php">
			<input type="hidden" name="contactPerson[ap_id]" id="del_ap_id" value="" />
			<input type="hidden" name="customerOrContactPerson[action]" id="action" value="deleteContactPersonData" />
			Wollen Sie diesen Kontakt wirklich l&ouml;schen?
		</form>
	</div>
</div>