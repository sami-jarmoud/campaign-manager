<?php
/* @var $clientManager \ClientManager */
/* @var $campaignManager \CampaignManager */


/**
 * getAgenturItems
 * 
 * @param \CampaignManager $campaignManager
 * @return string
 */
function getAgenturItems(\CampaignManager $campaignManager) {
	$result = '';
	
	try {
		/**
		 * getCustomersDataItemsByQueryParts
		 */
		$customersDataArray = $campaignManager->getCustomersDataItemsByQueryParts(
			array(
				'ORDER_BY' => '`firma` ASC'
			),
			\PDO::FETCH_CLASS
		);
		if (\count($customersDataArray) > 0) {
			foreach ($customersDataArray as $customerEntity) {
				/* @var $customerEntity \CustomerEntity */

				$result .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => $customerEntity->getKunde_id()
					),
					$customerEntity->getFirma()
				);
			}
		}
		unset($customersDataArray);
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}

	return $result;
}

/**
 * getEditorsItems
 * 
 * @param \ClientManager $clientManager
 * @param string $selectedItem
 * @return string
 */
function getEditorsItems(\ClientManager $clientManager, $selectedItem) {
	$result = '';

	try {
		/**
		 * getDepartmentsUsersDataItemsByMandantId
		 */
		$usersDataArray = $clientManager->getDepartmentsUsersDataItemsByMandantId(
			\RegistryUtils::get('clientEntity')->getId(),
			'v,g,b',
			true
		);
		if (\count($usersDataArray) > 0) {
			foreach ($usersDataArray as $userEntity) {
				/* @var $userEntity \UserEntity */

				$selected = '';
				if ($userEntity->getVorname() . ' ' . $userEntity->getNachname() == $selectedItem) {
					$selected = 'selected';
				}

				$result .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => $userEntity->getVorname() . ' ' . $userEntity->getNachname(),
						'selected' => $selected
					),
					$userEntity->getVorname() . ' ' . $userEntity->getNachname()
				);
			}
		}
		unset($usersDataArray);
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}

	return $result;
}

/**
 * getStatusItems
 * 
 * @param array $statusDataArray
 * @param integer $selectedItem
 * @return string
 */
function getStatusItems(array $statusDataArray, $selectedItem) {
	$result = '';

	foreach ($statusDataArray as $key => $value) {
		$selected = '';
		if ((string) $selectedItem === (string) $key) {
			$selected = 'selected';
		}

		$result .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => $key,
				'selected' => $selected
			),
			$value['label']
		);
	}

	return $result;
}

/**
 * getSalesManagerItems
 * 
 * @param \ClientManager $clientManager
 * @return string
 */
function getSalesManagerItems(\ClientManager $clientManager) {
	$result = '';

	try {
		/**
		 * getDepartmentsUsersDataItemsByMandantId
		 */
		$usersDataArray = $clientManager->getDepartmentsUsersDataItemsByMandantId(
			\RegistryUtils::get('clientEntity')->getId(),
			'v,g',
			true
		);
		if (\count($usersDataArray) > 0) {
			foreach ($usersDataArray as $userEntity) {
				/* @var $userEntity \UserEntity */

				$selected = '';
				if ($userEntity->getBenutzer_id() === \RegistryUtils::get('userEntity')->getBenutzer_id()) {
					$selected = 'selected';
				}

				$result .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => $userEntity->getBenutzer_id(),
						'selected' => $selected
					),
					$userEntity->getVorname() . ' ' . $userEntity->getNachname()
				);
			}
		}
		unset($usersDataArray);
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}

	return $result;
}
?>
<div id="rendertarget"></div>

<?php
// Kampagnen
require_once('DialogForm/campaign.php');

// Kunden
require_once('DialogForm/customer.php');