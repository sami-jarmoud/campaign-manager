<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

?>
<style>
    #yuiDataTable .yui-skin-sam .yui-dt th {
        background-color: #7D960B;
        font-weight:bold;
        color: #FFFFFF;
    }
    .yui-skin-sam .yui-dt-liner {
        margin:0;
        padding:2px 10px 2px 10px;
    }    
    .yui-skin-sam .yui-dt td.Number  {
        text-align:right;
        color: #565656;
        font-size: 11px;
    }
    .yui-skin-sam .yui-dt td.umsatzData  {
        text-align:right;
        font-weight:bold;
        color: #7D960B;
        font-size: 11px;
    }
    .yui-skin-sam .yui-dt td.umsatzKundename {
        font-weight:bold;
        color: #565656;
        font-size: 11px;
    }
</style>
<div style="background-color:#FFFFFF; padding: 10px;">
	<div id="invoiceFilter" style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">
		<?php
			require('Filter/header.php');
		?>
	</div>

    <table class="spacerTopWrap">
		<tr>
			<td style="width:30px;border:0px;background-color:#FFFFFF;padding:2px">
				<img src="img/icon_euro_25.gif" title="Umsatz" />
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Umsatz <?php echo ($month > 0 ? $month . '/' : '') . $year; ?><br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['sum']); ?> Euro</span>
			</td>
			<td style="width:110px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Umsatz (validiert)<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['netSum']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				eff. TKP<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['eTkpSum']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				TKP<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['tkp']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				CPX<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['cpx']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Hybrid<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['hybrid']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Festpreis<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['fixedprice']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Kosten<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['cost']); ?> Euro</span>
			</td>
			<td style="width:100px;border:0px;background-color:#FFFFFF;color:#565656;font-weight:bold;padding:2px">
				Deckungsbeiträgen<br />
				<span style="color:#7D960B"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['cover']); ?> Euro</span>
			</td>			
		</tr>
	</table>
</div>
<div id="umsatz_jahr" style="background-color:#FFFFFF;margin-top:2px;padding:10px">
    <table style="width:60%">
        <tr>
            <td class="label_umsatz_jahr">
				Ums&auml;tze 
				<?php
				echo
					($month > 0 
						? $year . ' ' . \DateUtils::$monthDataArray[$month] 
						: $year
					)
					. (\strlen($editor) > 0 
						? ' von: ' . $editor 
						: ''
					)
					. (\strlen($deliverySystem) > 0 
						? ', ASP: ' . $deliverySystemEntity->getAsp() 
						: ''
					)
					. ($dsdId > 0 
						? ' (' . $deliverySystemDistributorEntity->getTitle() . ')' 
						: (\strlen($deliverySystem) > 0 
							? ' (alle Verteiler)' 
							: ''
						)
					)
				;
				?>
			</td>
        </tr>
    </table>
    <div style="background-color:#7D960B; width:90%;">
        <table style="width:80%;">
            <tr class="umsatz_sum">
                <td align="right">Gesamt</td>
                <td align="right">validiert</td>
				<td align="right">eff. TKP</td>
                <td align="right">TKP</td>
                <td align="right">CPX</td>
                <td align="right">Hybrid</td>
				<td align="right">Festpreis</td>
                <td align="right">Versendet</td>
                <td align="right">Gebucht</td>
				<td align="right">kosten</td>
				<td align="right">Deckungsbeiträgen</td>
            </tr>
            <tr class="umsatz_sum">
                <td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['sum']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
                <td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['netSum']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
				<td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['eTkpSum']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
                <td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['tkp']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
                <td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['cpx']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
                <td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['hybrid']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
				<td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['fixedprice']) . \FormatUtils::euroSignInUtf8Unicode; ?></td>
                <td align="right"><?php echo \FormatUtils::numberFormat($turnoverDataArray['total']['send']); ?></td>
                <td align="right"><?php echo \FormatUtils::numberFormat($turnoverDataArray['total']['booked']); ?></td>
				<td align="right"><?php echo \FormatUtils::sumFormat($turnoverDataArray['total']['cost']) . \FormatUtils::euroSignInUtf8Unicode;; ?></td>
				<td align="right"><?php echo \FormatUtils::sumFormat( $turnoverDataArray['total']['cover']) . \FormatUtils::euroSignInUtf8Unicode;; ?></td>
            </tr>
        </table>
    </div>
    <br />
    <div id="yuiDataTable"></div>
</div>
<script type="text/javascript">
	var deliverySystemShow = <?php echo $deliverySystemShow; ?>;

	if (deliverySystemShow === true) {
		showViewById('tunoverDeliverySystemDistributorCell');
		YAHOO.util.Dom.get('tunoverDeliverySystemDistributorCell').style = 'display: inline-block;';
	} else {
		hideViewById('tunoverDeliverySystemDistributorCell');
	}

	function changeTurnoverDeliverySystemFilter(deliverySystemValue, inputDistributorId) {
		hideViewById(inputDistributorId + 'Cell');
		YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = '';

		if (deliverySystemValue !== '') {
			setRequest_newCampaign(
				'<?php echo \BaseUtils::getAjaxRequestUrl(); ?>',
				'<?php echo \BaseUtils::setAjaxRequestUri('client', 'deliverySystemDistributor'); ?>' 
					+ '&deliverySystem=' + deliverySystemValue 
					+ '&createSelectItem=0'
				,
				'',
				function(deliverySystemDistributorData) {
					if (typeof deliverySystemDistributorData !== 'undefined') {
						if ((deliverySystemDistributorData.indexOf('<span')) !== -1) {
							showViewById(inputDistributorId + 'Content');
							YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = deliverySystemDistributorData;

							YAHOO.util.Dom.get('kDeliverySystemDistributorItems').innerHTML = '';
							hideViewById(inputDistributorId);
						} else {
							YAHOO.util.Dom.get(inputDistributorId + 'Items').innerHTML = '<option value="">-- Alle --</option>' + deliverySystemDistributorData;

							hideViewById(inputDistributorId + 'Content');
							showViewById(inputDistributorId);
							showViewById(inputDistributorId + 'Items');
						}

						showViewById(inputDistributorId + 'Cell');
						YAHOO.util.Dom.get(inputDistributorId + 'Cell').style = 'display: inline-block;';
					}
				}
			);
		} else {
			removeSelectItemsById(inputDistributorId + 'Items');
			hideViewById(inputDistributorId + 'Items');
		}
	}

	function changeView() {
		setRequest(
			'<?php echo \BaseUtils::getScriptFileForRequestData(); ?>'
				+ '?turnover[filter][year]=' + YAHOO.util.Dom.get('tunoverYear').value
				+ '&turnover[filter][month]=' + YAHOO.util.Dom.get('tunoverMonth').value
				+ '&turnover[filter][editor]=' + YAHOO.util.Dom.get('tunoverEditors').value
				+ '&turnover[filter][deliverySystem]=' + YAHOO.util.Dom.get('tunoverDeliverySystem').value
				+ '&turnover[filter][dsdId]=' + YAHOO.util.Dom.get('tunoverDeliverySystemDistributorItems').value
				//+ '&turnover[filter][selection]=' + YAHOO.util.Dom.get('tunoverSelections').value 
			,
			'u'
		);
	}

	YAHOO.example.Basic = function() {
		YAHOO.example.Data = {
			umsatz: <?php echo \json_encode($yuiDataArray); ?>
		};

		var myColumnDefs = [
			{key: 'company', label: 'Agentur / Kunde', sortable: true, resizeable: true, className: 'umsatzKundename'},
			{key: 'salesCustomer', label: 'Umsatz', sortable: true, formatter: 'currency', resizeable: true, className: 'umsatzData', sortOptions: {defaultDir: YAHOO.widget.DataTable.CLASS_DESC}},
			{key: 'salesCustomerNet', label: 'Umsatz (validiert)', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'effTkp', label: 'eff. TKP', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'tkp', label: 'TKP', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'cpx', label: 'CPX', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'hybrid', label: 'Hybrid', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'fixedprice', label: 'Festpreis', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'send', label: 'Versendet', formatter: 'number', sortable: true, resizeable: true, className: 'Number'},
			{key: 'booked', label: 'Gebucht', formatter: 'number', sortable: true, resizeable: true, className: 'Number'},
			{key: 'cost', label: 'Kosten', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'},
			{key: 'cover', label: 'DB', formatter: 'currency', sortable: true, resizeable: true, className: 'umsatzData'}
		];

		var myDataSource = new YAHOO.util.DataSource(YAHOO.example.Data.umsatz);
		myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
		myDataSource.responseSchema = {
			fields: [
				'company',
				'salesCustomer',
				'salesCustomerNet',
				'effTkp',
				'tkp',
				'cpx',
				'hybrid',
				'fixedprice',
				'send',
				'booked',
				'cost',
				'cover'
			]
		};

		var myDataTable = new YAHOO.widget.DataTable(
			'yuiDataTable',
			myColumnDefs,
			myDataSource,
			{
				currencyOptions: {
					suffix: ' &euro;',
					decimalPlaces: 2,
					decimalSeparator: ',',
					thousandsSeparator: '.'
				},
				numberOptions: {
					thousandsSeparator: '.'
				},
				/*
				sortedBy: {
					key: 'salesCustomer',
					dir: YAHOO.widget.DataTable.CLASS_DESC
				}
				*/
			}
		);

		return {
			oDS: myDataSource,
			oDT: myDataTable
		};
	}();
</script>