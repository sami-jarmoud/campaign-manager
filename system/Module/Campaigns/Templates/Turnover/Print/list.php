<?php
/* @var $customerEntity \CustomerEntity */


// createTableRow - Tag
$tableListRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: ' . (($c % 2 == 0) ? '#ffffff' : '#EAEAEA') . ';'
	)
);

$tableContent =
	$tableListRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde company'
			),
			$customerEntity->getFirma()
		) 
		// Umsatz
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde salesCustomer',
				'style' => \CampaignFieldsUtils::alignRight
			),
			 \FormatUtils::sumFormat($customerYuiDataArray['salesCustomer']) . \FormatUtils::euroSignHtmlCode
		) 
		// Umsatz (validiert)
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde salesCustomerNet',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['salesCustomerNet']) . \FormatUtils::euroSignHtmlCode
		) 
		// tkp
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde tkp',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['tkp']) . \FormatUtils::euroSignHtmlCode
		) 
		// eff. TKP
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde effTkp',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['effTkp']) . \FormatUtils::euroSignHtmlCode
		) 
		// CPX
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde cpx',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['cpx']) . \FormatUtils::euroSignHtmlCode
		) 
		// Hybrid
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde hybrid',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['hybrid']) . \FormatUtils::euroSignHtmlCode
		) 
		// Festpreis
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde fixedprice',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['fixedprice']) . \FormatUtils::euroSignHtmlCode
		) 
		// Versendet
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde send',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::numberFormat($customerYuiDataArray['send'])
		) 
		// Gebucht
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'booked',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::numberFormat($customerYuiDataArray['booked'])
		)
		// Kosten
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde fixedprice',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['cost']) . \FormatUtils::euroSignHtmlCode
		) 
		// Deckungsbeitrag
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'umsatz_kunde fixedprice',
				'style' => \CampaignFieldsUtils::alignRight
			),
			\FormatUtils::sumFormat($customerYuiDataArray['cover']) . \FormatUtils::euroSignHtmlCode
		) 		
	. $tableListRowDataArray['end']
;
unset($tableListRowDataArray);