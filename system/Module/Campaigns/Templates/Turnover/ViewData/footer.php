<?php
/**
 * createTFootTag - Tag
 */
$tableFootDataArray = \HtmlTableUtils::createTFootTag();

/**
 * createTableRow - Tag
 */
$tableFootRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'umsatz_sum',
	)
);

$tableFooterContent = 
	$tableFootDataArray['begin'] 
		. $tableFootRowDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'colspan' => $totalColspan,
					'style' => 'height: 10px;'
				),
				''
			) 
		. $tableFootRowDataArray['end'] 
		. $tableFootRowDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'gesamt',
					'style' => \CampaignFieldsUtils::alignLeft . ' font-weight: bold; background-color: #eaeaea;',
					'colspan' => 2,
				),
				'Gesamt'
			)
;

foreach ($tableColumnsDataArray as $key => $itemDataArray) {
	$tableFooterContent .= \HtmlTableUtils::createTableCellWidthContent(
		array(
			'class' => $key,
			'style' => (isset($itemDataArray['style']) ? $itemDataArray['style'] : '') . ' background-color: #eaeaea;',
		),
		$itemDataArray['rowTitle']
	);
}

$tableFooterContent .= 
		$tableFootRowDataArray['end'] 
		. $tableFootRowDataArray['begin']
			// Gesamt
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
					'colspan' => 2,
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['sum']) . \FormatUtils::euroSignHtmlCode
			) 
			// validiert
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['netSum']) . \FormatUtils::euroSignHtmlCode
			) 
			// eff. TKP
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['eTkpSum']) . \FormatUtils::euroSignHtmlCode
			) 
			// TKP
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['tkp']) . \FormatUtils::euroSignHtmlCode
			) 
			// CPX
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['cpx']) . \FormatUtils::euroSignHtmlCode
			) 
			// Hybrid
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['hybrid']) . \FormatUtils::euroSignHtmlCode
			) 
			// Festpreis
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['fixedprice']) . \FormatUtils::euroSignHtmlCode
			) 
			// Versendet
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::numberFormat($turnoverDataArray['total']['send'])
			) 
			// Gebucht
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::numberFormat($turnoverDataArray['total']['booked'])
			)
			// Kosten
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['cost']) . \FormatUtils::euroSignHtmlCode
			) 
			// Deckungsbeitrag
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold; background-color: #eaeaea;',
				),
				\FormatUtils::sumFormat($turnoverDataArray['total']['cover']) . \FormatUtils::euroSignHtmlCode
			) 		
		. $tableFootRowDataArray['end']
	. $tableFootDataArray['end']
;

unset($tableFootDataArray, $tableFootRowDataArray);