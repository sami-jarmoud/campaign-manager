<?php
$tableContent = 
	$tableRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'colspan' => $totalColspan
			),
			'Keine Kampagnen im aktuellen Zeitraum'
		)
	. $tableRowDataArray['end']
;