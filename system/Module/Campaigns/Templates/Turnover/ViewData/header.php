<?php
/* @var $deliverySystemEntity \DeliverySystemEntity */


$tableHeaderContent = 
	$tableRowDataArray['begin']
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $totalColspan,
				'class' => 'label_umsatz_jahr'
			),
			\utf8_encode(
				'Ums�tze ' 
					. ($month > 0 
						? $year . ' ' . \DateUtils::$monthDataArray[$month]
						: $year
					) 
					. (\strlen($editor) > 0 
						? ' von: ' . $editor 
						: ''
					) 
					. (\strlen($deliverySystem) > 0 
						? ', ASP: ' . $deliverySystemEntity->getAsp()  
						: ''
					) 
					. ($dsdId > 0 
						? ' (' . $deliverySystemDistributorEntity->getTitle() . ')'
						: (\strlen($deliverySystem) > 0 
							? ' (alle Verteiler)'
							: ''
						)
					) 
			)
		) 
	. $tableRowDataArray['end'] 
	. $tableRowDataArray['begin'] 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $totalColspan,
				'style' => 'height: 10px; background: none;'
			),
			''
		) 
	. $tableRowDataArray['end']
	. $tableRowDataArray['begin']
;

foreach ($tableHeaderColumnsDataArray as $key => $itemDataArray) {
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'class' => $key,
			'style' => (isset($itemDataArray['style']) ? $itemDataArray['style'] : ''),
		),
		$itemDataArray['rowTitle']
	);
}
$tableHeaderContent .= $tableRowDataArray['end'];