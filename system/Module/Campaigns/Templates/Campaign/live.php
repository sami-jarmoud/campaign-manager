<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="Refresh" content="30; url=/system/Module/Campaigns/View/Campaign/live.php" />

        <title>MaaS - Live Monitoring</title>

        <style type="text/css">
            body {font-family:Arial, Helvetica, sans-serif; font-size:11px;}
            table {font-family:Arial, Helvetica, sans-serif; font-size:11px; width:100%}
            table td {border-right:1px #999999 solid; border-bottom:1px #999999 solid; border-collapse:collapse;white-space: nowrap;}
        </style>

        <script type="text/javascript">
			<!--
            var secs;
			var timerID = null;
			var timerRunning = false;
			var delay = 1000;

			function InitializeTimer() {
				secs = 31;

				StartTheTimer();
			}

			function StopTheClock() {
				if (timerRunning) {
					clearTimeout(timerID);
				}

				timerRunning = false;
			}

			function StartTheTimer() {
				if (secs === 1) {
					StopTheClock();
				} else {
					self.status = secs;
					secs = secs - 1;
					timerRunning = true;
					timerID = self.setTimeout("StartTheTimer()", delay);
					document.getElementById('sek').innerHTML = secs;
				}
			}
			//-->
        </script>
    </head>
    <body onLoad="InitializeTimer()">
        <table cellpadding="5" cellspacing="0">
            <tr>
                <td colspan="14" style="border-right:0px; color:#666666">
                    <strong><i>Live Monitoring (Heute)</i></strong> <span style="font-size:9px">- Diese Seite aktualisiert sich automatisch in <span id='sek'>31</span>sek</span>
                </td>
            </tr>
            <tr style="background-color:#E0E7FC; font-weight:bold;">
                <td>Mandant</td>
                <td>Status</td>
                <td width="230">Kampagne</td>
                <td>Versendet</td>
            </tr>
			<?php echo $liveContentData; ?>
			<tr>
				<td height="35" align="left" colspan="10" style="font-size:10px;color:#666666;border:0px;">
					<img src="/system/img/icons/accept.png" height="10" /> = versendet
				</td>
			</tr>
        </table>
		<?php
			if (\strlen($alertContent) > 0) {
				echo '<script language="javascript" type="text/javascript">alert("' . $alertContent . '");</script>';
			}
		?>
    </body>
</html>