<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */
?>
<table cellpadding="3" id="testmailContentTableFrame" class="table clearTable" style="margin-bottom: 2em;">
	<!--<tr id="testmailTestlisteItems">
		<td style="width: 6em;" class="fontStyle bold">Testliste:</td>
		<td>
			<?php #echo $testlistSelectItem; ?>
		</td>
	</tr>-->
	<tr>
		<td class="fontStyle bold">Kampagne:</td>
		<td class="fontStyle bold">
			<span style="color:#9900FF;"><?php echo $campaignEntity->getCustomerEntity()->getFirma(); ?></span>, <?php echo $campaignEntity->getK_name(); ?>
		</td>
		<!--<td align="left">
			<span class="fontStyle bold">Suchen</span>
			<input name="filter" type="text" id="filter" value=""  onkeypress="handleEnterKey(event, <?php #echo $campaignEntity->getK_id(); ?>)">
			<input type="button"onclick="suchen(<?php #echo $campaignEntity->getK_id(); ?>)" value="Suchen"/> 
			<input type="button"onclick="zuruecksetzen(<?php #echo $campaignEntity->getK_id(); ?>)" value="Zur&uuml;cksetzen"/> 
		</td>-->
	</tr>
</table>
<?php if($an_mich == '0'){?>
<form id="adrForm" name="adrForm">
	<div id="testadressenTable" class="align left"></div>
	<div id="checkBlacklistContent" style="position: absolute; top: 9em; width: 98%; height: 79%; background: #fff;" class="align center"></div>
	
	<input type="hidden" id="testmailRecipient" value="" />
	<input type="hidden" id="testmailTestlistId" value="<?php echo $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id(); ?>" />
	<input type="hidden" id="testmailCampaignId" value="<?php echo $campaignEntity->getK_id(); ?>" />
</form>
<?php }else{?>
<form id="adrForm" name="adrForm">
	<div id="testadressTable" class="align left"></div>	
	<!--<div id="checkBlacklistContent" style="position: absolute; top: 9em; width: 98%; height: 79%; background: #fff;" class="align center"></div>-->
	<input type="hidden" id="testmailRecipient" value="" />
	<input type="hidden" id="testmailTestlistId" value="<?php echo $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id(); ?>" />
	<input type="hidden" id="testmailCampaignId" value="<?php echo $campaignEntity->getK_id(); ?>" />
</form>
<?php } ?>
<script type="text/javascript">
	hideViewById('checkBlacklistContent');
	
	<?php if($an_mich == '0'){?>
		
	getRecipientList(
		<?php echo $campaignEntity->getK_id(); ?>,
		<?php echo $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id(); ?>
	);

	<?php }else{?>
		getRecipient(
		<?php echo $campaignEntity->getK_id(); ?>,
		<?php echo $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id(); ?>
	);
	<?php } ?>	
	testmailRecipients = '';
  
	var e, s = '';
	var i = 0;
	
	function getRecipientList(campaignId, recipientListId) {
		setRequest_default2(
			'<?php echo \BaseUtils::setScriptFileforRequestData('recipientList.php'); ?>'
				+ '?campaign[k_id]=' + campaignId 
				+ '&recipient[listId]=' + recipientListId
			,
			'testadressenTable'
		);
	}
    
		function getRecipientListFilter(campaignId, recipientListId, filter) {
		setRequest_default2(
			'<?php echo \BaseUtils::setScriptFileforRequestData('recipientList.php'); ?>'
				+ '?campaign[k_id]=' + campaignId 
				+ '&recipient[listId]=' + recipientListId
		        + '&filter=' + filter
			,
			'testadressenTable'
		);
	}
	function changeTestlist(campaignId, recipientListId) {
		document.getElementById('testmailRecipient').value = '';
		document.getElementById('testmailTestlistId').value = recipientListId;
		s = '';
		i = 0;
		
		getRecipientList(
			campaignId,
			recipientListId
		);
	}
	
	function getRecipient(campaignId, recipientId) {
		setRequest_default2(
			'<?php echo \BaseUtils::setScriptFileforRequestData('recipient.php'); ?>'
				+ '?campaign[k_id]=' + campaignId 
				+ '&recipient[listId]=' + recipientId
			,
			'testadressTable'
		);
	}
	
	function changeTestlistForMe(campaignId, recipientId) {
		document.getElementById('testmailRecipient').value = '';
		document.getElementById('testmailTestlistId').value = recipientId;
		s = '';
		i = 0;
		
		getRecipient(
			campaignId,
			recipientId
		);
	}
	
	function addRecipientToList(recipientEmail) {
		var addRecipientEmail = '- ' + recipientEmail + "\n";
		var vorhanden = s.search(recipientEmail);

		if (vorhanden !== -1) {
			s = s.replace(addRecipientEmail, '');
			i--;
		} else {
			// checkDeliverySystemBlacklistItem
			//checkDeliverySystemBlacklistItem(recipientEmail);
			
			s += addRecipientEmail;
			i++;
		}
		
		var testmailRecipient = s;
		testmailRecipient = testmailRecipient.replace(/\n/g, '');
		testmailRecipient = testmailRecipient.replace(/\- /g, ';');
		testmailRecipient = testmailRecipient.substr(1, testmailRecipient.length); // erstes ";" entfernen
		
		document.getElementById('testmailRecipient').value = testmailRecipient;
		testmailRecipients = "Testmail wird an folgende Adressen (" + i + ") gesendet:\n\n" + s;
	}
	
	function checkDeliverySystemBlacklistItem(recipient) {
		showViewById('checkBlacklistContent');
		YAHOO.util.Dom.get('checkBlacklistContent').innerHTML = getLoaderImage();
		
		processAjaxRequest(
			'AjaxRequests/ajaxCampaign.php',
			'actionMethod=checkDeliverySystemBlacklistItem' 
				+ '&campaign[k_id]=' +  parseInt(YAHOO.util.Dom.get('testmailCampaignId').value)
				+ '&recipient=' + recipient
			,
			'',
			function (contentResult) {
				hideViewById('checkBlacklistContent');
				
				if (contentResult.itemFound) {
					var message = '';
					
					if (contentResult.success) {
						message = recipient + "\n\n" + 'Email oder Domain befindet sich auf die Blackliste!';
					} else {
						message = 'Es gab einen Problem bei der Blacklist �berpr�fung!';
					}
					
					alert(message);
				}
			}
		);
	}
	function suchen(campaignId){
		var filter = document.getElementById('filter');
						getRecipientListFilter(
		                campaignId,
		              <?php echo $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id(); ?>,
				       filter.value
	             );
			
	}
	function zuruecksetzen(campaignId){
		setRequest_default(
						'Module/Campaigns/View/Campaign/campaignTestmail.php?campaign[k_id]=' + campaignId ,
						'testmail_frame'
					);
			
	}
	function handleEnterKey(e, campaignId){
		var charCode;

    if (e && e.which) {
        charCode = e.which;
    } else if (window.event) {
        e = window.event;
        charCode = e.keyCode;
    }

    if (charCode === 13) {
    	suchen(campaignId);
    }
	}
</script>