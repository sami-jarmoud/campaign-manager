<?php
$tableNoContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: #fff;'
	)
);

$tableNoContent = 
	$tableNoContentRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'text-align:center;padding:10px;color:#666666'
			),
			'Keine Mailings vorhanden'
		)
	. $tableNoContentRowDataArray['end']
;
unset($tableNoContentRowDataArray);