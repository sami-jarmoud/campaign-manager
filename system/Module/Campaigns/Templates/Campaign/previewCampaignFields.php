<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */
?>
<div class="workarea">
    <h3 style="color:#9900FF;font-weight:bold;">- Aktives Fenster -</h3>
    <ul id="ul1" class="draglist" style="border:1px solid #9900FF">
		<?php echo $tab_felder_active; ?>
    </ul>
</div>
<div class="workarea">
    <h3 style="color:#333333;font-weight:bold;">Deaktiviert</h3>
    <ul id="ul2" class="draglist">
		<?php echo $tab_felder_deactive; ?>
    </ul>
</div>
<script type="text/javascript">
	(function() {
		var Dom = YAHOO.util.Dom;
		var Event = YAHOO.util.Event;
		var DDM = YAHOO.util.DragDropMgr;

		YAHOO.example.DDApp = {
			init: function() {
				var rows = 100, cols = 2, i, j;
				for (i = 1; i < cols + 1; i = i + 1) {
					new YAHOO.util.DDTarget('ul' + i);
				}

				for (i = 1; i < cols + 1; i = i + 1) {
					for (j = 1; j < rows + 1; j = j + 1) {
						new YAHOO.example.DDList('li' + i + '_' + j);
					}
				}
			}
		};

		// custom drag and drop implementation
		YAHOO.example.DDList = function(id, sGroup, config) {
			YAHOO.example.DDList.superclass.constructor.call(this, id, sGroup, config);

			var el = this.getDragEl();
			Dom.setStyle(el, 'opacity', 0.67); // The proxy is slightly transparent
			this.goingUp = false;
			this.lastY = 0;
		};

		YAHOO.extend(YAHOO.example.DDList, YAHOO.util.DDProxy, {
			startDrag: function(x, y) {
				// make the proxy look like the source element
				var dragEl = this.getDragEl();
				var clickEl = this.getEl();
				Dom.setStyle(clickEl, 'visibility', 'hidden');

				dragEl.innerHTML = clickEl.innerHTML;

				Dom.setStyle(dragEl, 'color', Dom.getStyle(clickEl, 'color'));
				Dom.setStyle(dragEl, 'backgroundColor', Dom.getStyle(clickEl, 'backgroundColor'));
				Dom.setStyle(dragEl, 'border', '2px solid gray');
			},
			endDrag: function(e) {
				var srcEl = this.getEl();
				var proxy = this.getDragEl();

				// Show the proxy element and animate it to the src element's location
				Dom.setStyle(proxy, 'visibility', '');
				var a = new YAHOO.util.Motion(
					proxy,
					{
						points: {
							to: Dom.getXY(srcEl)
						}
					},
					0.2,
					YAHOO.util.Easing.easeOut
				);
				var proxyid = proxy.id;
				var thisid = this.id;

				// Hide the proxy and show the source element when finished with the animation
				a.onComplete.subscribe(function() {
					Dom.setStyle(proxyid, 'visibility', 'hidden');
					Dom.setStyle(thisid, 'visibility', '');
				});
				a.animate();
			},
			onDragDrop: function(e, id) {
				if (DDM.interactionInfo.drop.length === 1) {
					var pt = DDM.interactionInfo.point;
					var region = DDM.interactionInfo.sourceRegion;

					if (!region.intersect(pt)) {
						var destEl = Dom.get(id);
						var destDD = DDM.getDDById(id);

						destEl.appendChild(this.getEl());
						destDD.isEmpty = false;
						DDM.refreshCache();
					}
				}
			},
			onDrag: function(e) {
				var y = Event.getPageY(e);

				if (y < this.lastY) {
					this.goingUp = true;
				} else if (y > this.lastY) {
					this.goingUp = false;
				}

				this.lastY = y;
			},
			onDragOver: function(e, id) {
				var srcEl = this.getEl();
				var destEl = Dom.get(id);

				if (destEl.nodeName.toLowerCase() === 'li') {
					var orig_p = srcEl.parentNode;
					var p = destEl.parentNode;

					if (this.goingUp) {
						p.insertBefore(srcEl, destEl); // insert above
					} else {
						p.insertBefore(srcEl, destEl.nextSibling); // insert below
					}

					DDM.refreshCache();
				}
			}
		});

		Event.onDOMReady(YAHOO.example.DDApp.init, YAHOO.example.DDApp, true);
	})();
</script>