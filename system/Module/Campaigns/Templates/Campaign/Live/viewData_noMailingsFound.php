<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $clientEntity \ClientEntity */


$liveContentData .= 
	$tableNoContentRowDataArray['begin']
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'text-align:center;'
			),
			'<img src="/system/img/logo/' . $clientEntity->getAbkz() . '.gif" height="15" title="' . $clientEntity->getMandant() . '" />'
		)
		. \HtmlTableUtils::createTableCellWidthContent(
			array(
				'style' => 'text-align:center;border-right:1px #999999 solid; color:#999999;',
				'colspan' => 3
			),
			'Keine Versendungen'
		)
	. $tableNoContentRowDataArray['end']
;