<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $clientEntity \ClientEntity */


foreach ($campaignDataArray as $campaignEntity) {
	/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
	
	$campaignShips = 0;
	$campaignStatus = \RegistryUtils::get(
		'campaign' 
		. \RegistryUtils::$arrayKeyDelimiter . 'statusDataArray' 
		. \RegistryUtils::$arrayKeyDelimiter . 'all' 
		. \RegistryUtils::$arrayKeyDelimiter . $campaignEntity->getStatus()
		. \RegistryUtils::$arrayKeyDelimiter . 'label'
	);
	
	if (\array_key_exists($campaignEntity->getDsd_id(), $deliverySystemDistributorsWidthClientDeliveryDataArray)) {
		$alertContent .= \processDeliverySystemWebserviceForLiveView(
			$campaignEntity,
			$clientEntity,
			$deliverySystemDistributorsWidthClientDeliveryDataArray[$campaignEntity->getDsd_id()],
			$debugLogManager,
			$campaignStatus,
			$campaignShips,
			$mailingIdDone
		);
	}
	
	
	$liveContentData .= 
		$tableContentListRowDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => 'text-align:center;'
				),
				'<img src="/system/img/logo/' . $clientEntity->getAbkz() . '.gif" height="15" alt="versendet" title="' . $clientEntity->getMandant() . '" />'
			)
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => 'text-align:center;',
				),
				$campaignStatus
			)
			. \HtmlTableUtils::createTableCellWidthContent(
				array(),
				$campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name()
			)
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'style' => 'text-align:right;',
				),
				\FormatUtils::numberFormat($campaignShips)
			)
		. $tableContentListRowDataArray['end']
	;
}