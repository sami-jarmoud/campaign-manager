<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */

/* @var $dateNow \DateTime */
/* @var $campaignEntity \CampaignEntity */
/* @var $hvCampaignEntity \CampaignEntity */
/* @var $hvCampaignDate \DateTime */
/* @var $campaignDeliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */

?>
<input type="hidden" name="customer[firma_short]" id="kNewCustomerShortCompany" value="" />
<input type="hidden" name="customer[strasse]" id="kNewCustomerStreet" value="" />
<input type="hidden" name="customer[plz]" id="kNewCustomerZip" value="" />
<input type="hidden" name="customer[ort]" id="kNewCustomerCity" value="" />
<input type="hidden" name="customer[telefon]" id="kNewCustomerPhone" value="" />
<input type="hidden" name="customer[fax]" id="kNewCustomerFax" value="" />
<input type="hidden" name="customer[website]" id="kNewCustomerWebsite" value="" />
<input type="hidden" name="customer[email]" id="kNewCustomerEmail" value="" />
<input type="hidden" name="customer[geschaeftsfuehrer]" id="kNewCustomerCeo" value="" />
<input type="hidden" name="customer[registergericht]" id="kNewCustomerRegistergericht" value="" />
<input type="hidden" name="customer[vat_number]" id="kNewCustomerVatNumber" value="" />
<input type="hidden" name="customer[payment_deadline]" id="kNewCustomerPaymentDeadline" value="" />
<input type="hidden" name="customer[country_id]" id="kNewCustomerCountryId" value="" />
<input type="hidden" name="customer[data_selection]" id="kNewCustomerDataSelection" value="" />

<input type="hidden" name="contactPerson[anrede]" id="kNewContactPersonGender" value=""  />
<input type="hidden" name="contactPerson[vertriebler_id]" id="kNewContactPersonVertriebler" value="" />
<input type="hidden" name="contactPerson[titel]" id="kNewContactPersonTitle" value="" />
<input type="hidden" name="contactPerson[vorname]" id="kNewContactPersonFirstname" value="" />
<input type="hidden" name="contactPerson[nachname]" id="kNewContactPersonLastname" value="" />
<input type="hidden" name="contactPerson[email]" id="kNewContactPersonEmail" value="" />
<input type="hidden" name="contactPerson[telefon]" id="kNewContactPersonPhone" value="" />
<input type="hidden" name="contactPerson[fax]" id="kNewContactPersonFax" value="" />
<input type="hidden" name="contactPerson[mobil]" id="kNewContactPersonMobil" value="" />
<input type="hidden" name="contactPerson[position]" id="kNewContactPersonPosition" value="" />

<input type="hidden" name="campaign[bearbeiter]" value="<?php echo $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname']; ?>" />


<div id="tab_k_new" class="yui-navset" style="padding-top:7px; text-align:left; border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#tab1"><em>Kampagnendetails</em></a></li>
        <li><a href="#tab2"><em>Versanddetails</em></a></li>
        <li><a href="#tab3"><em>Infomailing</em></a></li>
        <li class="<?php echo $performanceContentClass; ?>"><a href="#tab4"><em>Auswertung</em></a></li>
    </ul>
    <div class="yui-content" style="margin:0px; padding:0px;">
        <div id="tab1">
            <input type="hidden" name="action" id="action" value="<?php echo $action; ?>" />
            <input type="hidden" id="kid" name="campaign[kid]" value="<?php echo $kid; ?>" />
            <input type="hidden" id="status" name="campaign[status]" value="<?php echo $campaignEntity->getStatus(); ?>" />
            <input type="hidden" id="rechnung" name="campaign[rechnung]" value="<?php echo $campaignEntity->getRechnung(); ?>" />
            <input type="hidden" id="mail_id" name="campaign[mail_id]" value="<?php echo $campaignEntity->getMail_id(); ?>" />
            <input type="hidden" name="campaign[nv_id]" value="<?php echo $campaignEntity->getNv_id(); ?>" />
            
            <input type="hidden" id="mailingTyp" name="mailingTyp" value="<?php echo $mailing; ?>" />
            <?php echo $campaignIsLock; ?>
            
            <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="2" align="right">
                        <span class="required">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span>
                    </td>
                </tr>
				
				<tr>
                    <td align="right" class="info">Werbemittel:</td>
                    <td align="left">
						<div class="floatLeft">
							<select name="campaign[advertising_materials]" id="campaign_advertising_materials" style="width: 170px;">
								<?php
								echo \HtmlFormUtils::createOptionListItemData(
									\CampaignAndCustomerUtils::$advertisingMaterialsDataArray,
									$campaignEntity->getAdvertising_materials()
								);
								?>
							</select>
						</div>
						<div class="floatLeft spacerLeftWrap" style="margin-top: 5px;">
							<input type="checkbox" name="campaign[premium_campaign]" id="campaign_premium_campaign" value="1" <?php echo $campaignEntity->getPremium_campaign() == 1 ? 'checked="checked"' : ''; ?> /> 
							<label for="campaign_premium_campaign" class="widthAuto">Premium Kampagne</label>
						</div><br class="clearBoth" />
                    </td>
                </tr>
				
                <tr>
                    <td align="right" class="info">Kunde/Agentur: <span class="required">*</span></td>
                    <td align="left">
                        <?php
                        if ($mailing == 'NV') {?>
                            <input type="text" name="agentur" id="agentur" value="<?php echo $campaignEntity->getAgentur(); ?>" class="inactive" readonly="readonly" />
							<input type="hidden" name="campaign[agentur]" value="<?php echo $campaignEntity->getAgentur_id(); ?>" />
                            <input type="hidden" name="campaign[ap]" value="<?php echo $campaignEntity->getAp_id(); ?>" />
                        <?php
                        } else {
                            ?>
                            <select name="campaign[agentur]" id="agentur" onchange="getAP(this.value, '<?php echo $campaignEntity->getAp_id(); ?>');">
                                <option value="">- Bitte ausw&auml;hlen -</option>
                                <?php echo $customerSelectItem; ?>
                            </select>
                            <input type="button" id="k_neuer_kunde" onclick="kNewCustomer();" value="Neu" class="floatRight" />
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                
                <?php
                if ($mailing == 'HV') {?>
                    <tr>
                        <td align="right" class="info">Ansprechpartner:</td>
                        <td align="left">
                            <div id="ap_select" class="floatLeft">
                                <select name="campaign[ap]" id="ap" onchange="changeContactPerson(this.value)">
                                    <option value="">-- keine Kontakte --</option>
                                </select>
                            </div>
                            <input type="button" id="apButton" onclick="kNewContactPerson(<?php echo $_SESSION['benutzer_id']; ?>);" value="Neu" class="floatRight" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="info">Vertriebler:</td>
                        <td align="left">
                            <div id="vertriebler_select">
                                <?php echo $sales_fullname; ?>
                            </div>
                        </td>
                    </tr>
                <?php
                }
                ?>
                
                <tr>
                    <td align="right" class="info">Kampagne: <span class="required">*</span></td>
                    <td align="left">
                        <input type="text" name="campaign[k_name]" id="k_name" value="<?php echo $campaignEntity->getK_name(); ?>" <?php echo $readOnly; ?> />
                    </td>
                </tr>
                <tr>
                    <td align="right" class="info"><span id="v_datum">Versanddatum</span>: <span class="required">*</span></td>
                    <td align="left">
                        <input type="hidden" id="date" name="campaign[date]" value="<?php echo (boolean) $useDateObj ? $campaignEntity->getDatum()->format('Y-m-d') : ''; ?>" <?php echo $readOnly; ?> />
                        <input type="hidden" id="nTag" value="<?php echo ((boolean) $useDateObj ? $campaignEntity->getDatum()->format('d') : ''); ?>" <?php echo $readOnly; ?> />
                        <input type="hidden" id="nMonat" value="<?php echo ((boolean) $useDateObj ? $campaignEntity->getDatum()->format('m') : ''); ?>" <?php echo $readOnly; ?> />
                        <input type="hidden" id="nJahr" value="<?php echo ((boolean) $useDateObj ? $campaignEntity->getDatum()->format('Y') : ''); ?>" <?php echo $readOnly; ?> />
                        
                        <div class="field floatLeft" id="datefields">
                            <select id="month" name="campaign[month]">
                                <?php echo \getOptionListByCount(12); ?>
                            </select>
                            <select id="day" name="campaign[day]">
                                <?php 
									echo \getOptionListByCount(
										\DateUtils::getDayCountByMonthAndYear(
											$dateNow->format('m'),
											$dateNow->format('Y')
										)
									);
								?>
                            </select>
                            <input type="text" id="year" name="campaign[year]" value="" />
                        </div>
                        <div style="margin-left: 10px;" class="floatLeft">
                            <select name="campaign[versand_hour]" id="campaign_versandHour" class="widthAuto" onchange="validateAndCheckNvCampaignDate('<?php echo $mailing; ?>')" <?php echo $disabledCampaignDate; ?>>
                                <?php 
								echo \hourOptionList((boolean) $useDateObj 
									? \intval($campaignEntity->getDatum()->format('H')) 
									: 9
								); 
								?>
                            </select> <b>:</b>
                            <select name="campaign[versand_min]" id="campaign_versandMin" style="margin-left: 2px;" class="widthAuto" onchange="validateAndCheckNvCampaignDate('<?php echo $mailing; ?>');" <?php echo $disabledCampaignDate; ?>>
                                <?php 
								echo \minutesOtionList((boolean) $useDateObj 
									? \intval($campaignEntity->getDatum()->format('i')) 
									: 0
								); 
								?>
                            </select> Uhr
                        </div>
                    </td>
                </tr>
				<tr>
                    <td align="right" class="info">Kampagnen-Klickprofil: <span class="required">*</span></td>
                    <td align="left">
                        <select name="campaign[campaign_click_profiles_id]" id="campaign_click_profiles_id">
							<option value="">- Bitte ausw&auml;hlen -</option>
							<?php
								echo $campaignClickProfileSelectionItems;
							?>
						</select>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="info">Gebuchtes Volumen:</td>
                    <td align="left">
                        <input onkeyup="calcNewUSQ(this.value, <?php echo $cm_vorgabe_usq; ?>);" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');" name="campaign[volumen]" id="volumen" type="text" value="<?php echo \FormatUtils::numberFormat($campaignEntity->getGebucht()); ?>" />
                    </td>
                </tr>
                <tr>
                    <td align="right" class="info" title="zu versendendes Volumen">Zu versend. Volumen:  <span class="required">*</span></td>
                    <td align="left"> 
                        <input title="zu versendendes Volumen" style="width:120px" name="campaign[vorgabe_m]" id="vorgabe_m" type="text" value="<?php echo FormatUtils::numberFormat($campaignEntity->getVorgabe_m()); ?>" onkeyup="calcNewUSQ2(this.value, <?php echo $cm_vorgabe_usq; ?>);" /> 
                        <input type="text" value="<?php echo $cm_vorgabe_usq; ?>" style="width:25px; text-align:right;" id="cm_vorgabe_usq" />% 
                        <input type="button" value="&Uuml;SQ" title="&Uuml;bersendungsquote &uuml;bernehmen" onclick="calcNewV();" />
                        <input type="button" value="Ziel eingeben" title="Zielvorgaben eingeben" onclick="toggleViewById('targetSettings');" class="floatRight" />
                    </td>
                </tr>
                <tr class="toggleHide" id="targetSettings">
                    <td colspan="2">
                        <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                            <tr id="openingTarget">
                                <td align="right" class="info">Ziel &Ouml;ffnungsrate:</td>
                                <td align="left">
                                    <input style="width:100px" name="campaign[vorgabe_o]" id="vorgabe_o" maxlength="5" type="text" value="<?php echo $campaignEntity->getVorgabe_o(); ?>" <?php echo $readOnly; ?> /> % 
                                    <span style="font-style:italic; color:#999999;">(z.B. 10, 11.5)</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Ziel Klickrate:</td>
                                <td align="left">
                                    <input style="width:100px" name="campaign[vorgabe_k]" id="vorgabe_k" maxlength="5" type="text" value="<?php echo $campaignEntity->getVorgabe_k(); ?>" <?php echo $readOnly; ?> /> % 
                                    <span style="font-style:italic; color:#999999;">(z.B. 0.8, 1.5)</span>

                                    <input type="hidden" id="vorgabe_k_old" value="<?php echo $campaignEntity->getVorgabe_k(); ?>" <?php echo $readOnly; ?> />
                                    <input type="hidden" id="vorgabe_o_old" value="<?php echo $campaignEntity->getVorgabe_o(); ?>" <?php echo $readOnly; ?> />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
				<tr class="<?php echo $priceClass; ?>">
					<td align="right" class="info topPaddingWrapper" valign="top">Abrechnungsart: <span class="required">*</span></td>
					<td align="left">
						<select name="campaign[abrechnungsart]" id="abrechnungsart" <?php echo $billingTypeSettings; ?> onchange="showOrChangePriceByBillingType(this.value, 'priceContentBox', true);">
							<option value="">- Bitte ausw&auml;hlen -</option>
							<?php
								echo \HtmlFormUtils::createOptionListItemData(
									\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray,
									$campaignBillingTypeValue
								);
							?>
						</select>
						<div id="priceContentBox">
							<div id="priceByBillingTypeContent"></div>
							
							<input name="campaign[preis]" id="preis" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getPreis()); ?>" />
							<input name="campaign[preis2]" id="preis2" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getPreis2()); ?>" />
                                                        <input name="campaign[hybrid_preis]" id="hybrid_preis" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getHybrid_preis()); ?>" />
                                                        <input name="campaign[hybrid_preis2]" id="hybrid_preis2" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getHybrid_preis2()); ?>" />
							<input name="campaign[unit_price]" id="unit_price" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getUnit_price()); ?>" />
							<input name="campaign[preis_gesamt]" id="preis_gesamt" type="hidden" value="<?php echo \FormatUtils::sumFormat($campaignEntity->getPreis_gesamt()); ?>" />
							<input name="campaign[preis_gesamt_verify]" id="preis_gesamt" type="hidden" value="<?php echo $campaignEntity->getPreis_gesamt_verify(); ?>" />
						</div>
					</td>
				</tr>
                <?php
                if ($mailing == 'NV' 
                    && \strlen($campaignEntity->getMail_id()) > 0
                ) {
					if( ($action == 'add_nv') && ($campaignEntity->getVersandsystem() != 'SE') ){
					?>
					<tr>
                        <td align="right" class="info">Im VS anlegen:</td>
                        <td align="left">
                            <input type="radio" name="campaign[imvs]" id="campaign_imvs_1" onchange="showDeliverySystemStartDate(this, 'campaign_use_campaign_start_date');enableOrDisableDeliverySystemByChangingImvs(this, '<?php echo $campaignEntity->getVersandsystem(); ?>', '<?php echo $campaignEntity->getDsd_id(); ?>');" value="1"/> <label for="campaign_imvs_1" >Ja</label>
							<input type="radio" name="campaign[imvs]" id="campaign_imvs_0" onchange="showDeliverySystemStartDate(this, 'campaign_use_campaign_start_date');enableOrDisableDeliverySystemByChangingImvs(this, '<?php echo $campaignEntity->getVersandsystem(); ?>', '<?php echo $campaignEntity->getDsd_id(); ?>');" value="0" checked /> <label for="campaign_imvs_0" >Nein</label>
                        </td>
                    </tr>
					<?php
					}
                    ?>
					<tr id="campaign_use_campaign_start_date">
                        <td align="right" class="info">Startdatum &uuml;bernehmen?</td>
                        <td align="left">
                            <?php
							echo \createCampaignDeliverySystemStartDateRadios(
								array(
									1 => 'Ja',
									0 => 'Nein'
								),
								$campaignEntity->getUse_campaign_start_date(),
								$campaignStartDate_state
							);
							?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td align="right" class="info" valign="top">Notiz:</td>
                    <td align="left" valign="top" >
                        <textarea name="campaign[note]" id="note" rows="4" ><?php echo $campaignEntity->getNotiz(); ?></textarea>
                        <div id="edit_true"></div>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="tab2">
            <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="headerColumn">Selektion:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0" id="versandInformationen">
                            <tr>
                                <td align="right" class="info">Mandant:</td>
                                <td align="left"><?php echo $_SESSION['mandant']; ?></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Versandsystem: <span class="required">*</span></td>
                                <td align="left" id="deliverySystemItems">
                                    <select name="campaign[versandsystem]" id="deliverySystem" onchange="changeDeliverySystem(this.value, 'deliverySystemDistributor', 0, 0);">
                                        <option value="">- Bitte ausw&auml;hlen -</option>
                                        <?php 
											echo $deliverySystemOptionItems;
										?>
                                    </select>
                                </td>
                            </tr>
                            <tr id="deliverySystemDistributorCell">
                                <td align="right" class="info">Verteiler:</td>
                                <td align="left">
                                    <div id="deliverySystemDistributorContent"></div>
                                    <div id="deliverySystemDistributor" class="floatLeft">
                                        <select name="campaign[deliverySystemDistributor]" id="deliverySystemDistributorItems" class="widthAuto" onchange="changeDeliverySystemDomain(this.value, 'deliverySystemDistributorDomain', 0, 0);">
                                            <?php 
												echo $deliverySystemDistributorItem;
											?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                             <tr id="deliverySystemDistributorDomainCell">
						<td align="right" class="info">Versanddomain: <span class="required">*</span></td>
						<td align="left">
							<div id="deliverySystemDistributorDomainContent"></div>
							<div id="deliverySystemDistributorDomain" class="floatLeft">
								<select name="campaign[dasp_id]" id="deliverySystemDistributorDomainItems" class="widthAuto">
									<?php echo $deliverySystemDistributorDomainItem; ?>
								</select>
							</div>
						</td>
					</tr>
							<tr>
								<td align="right" class="info">Selektion-Klickprofil:</td>
								<td align="left">
									<select name="campaign[selection_click_profiles_id]" id="selection_click_profiles_id">
										<option value="">- Bitte ausw&auml;hlen -</option>
										<?php
											echo $selectionClickProfileSelectionItems;
										?>
									</select>
								</td>
							</tr>
                            <tr>
                                <td align="right" class="info topPaddingWrapper" valign="top">Selektion optional:</td>
                                <td align="left">
									<div class="spacerTopWrap">
										<div id="genderSelection" class="floatLeft ">
											<?php
												echo $genderSelectionItem;
											?>
										</div>
										<div class="floatLeft spacerLeftWrap">
											Alter: <input type="text" name="campaign[alter]" id="alter" style="width:36px" <?php echo $readOnly; ?> />
										</div><br class="clearBoth" />
									</div>
									
									<div class="spacerTopWrap">
										<div id="b2b" class="floatLeft">
											<input type="checkbox" name="campaign[b2b]" id="campaign_b2b" value="1" <?php echo $checkedB2b; ?> /> 
											<label for="campaign_b2b" class="widthAuto">B2B</label>
										</div>
										<div id="zgContent" class="floatLeft spacerLeftWrap">
											Sonstiges:
											<input style="margin-top: -2px;" type="text" name="campaign[zg]" id="zg" class="widthAuto" value="<?php echo $campaignEntity->getZielgruppe(); ?>" <?php echo $readOnly; ?> />
										</div><br class="clearBoth" />
									</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="headerColumn">Email-Parameter:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0" id="emailParameter">
                            <tr>
                                <td align="right" class="info">Betreff:</td>
                                <td align="left">
                                    <input name="campaign[betreff]" id="betreff" type="text" value="<?php echo $campaignEntity->getBetreff(); ?>" <?php echo $readOnly; ?>/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Absender:</td>
                                <td align="left">
                                    <input name="campaign[absendername]" id="absendername" type="text" value="<?php echo $campaignEntity->getAbsendername(); ?>" <?php echo $readOnly; ?> />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Mailformat:</td>
                                <td align="left">
                                    <select name="campaign[mailtyp]" id="mailtyp" onchange="oHide(this.value);">
                                        <?php
                                        echo \HtmlFormUtils::createOptionListItemData(
                                            \CampaignAndCustomerUtils::$mailtypDataArray,
                                            $campaignEntity->getMailtyp()
                                        );
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Blacklist:</td>
                                <td align="left">
                                    <input type="checkbox" name="campaign[bl]" id="bl" value="X" <?php echo $campaignEntity->getBlacklist() == 'X' ? 'checked="checked"' : ''; ?> />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="tab3">
            <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" class="info" valign="top" colspan="2">Infomail "Neues Mailing" an:</td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <div style="height:179px; overflow:auto; background:#FFF;">
                            <?php echo \nl2br($userCheckboxItems); ?>
                        </div>
                        <br />Zus&auml;tzliche Info (optional):<br />
                        <textarea name="campaign[benachrichtigung_notiz]" style="height:40px; width: 98%;"></textarea>
                    </td>
                </tr>
            </table>
        </div>
        
        <div id="tab4" class="<?php echo $performanceContentClass; ?>">
            <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" class="headerColumn">Versanddaten (Menge & Performance):</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td colspan="2" class="info"><?php echo $performanceInfoContent; ?></td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Versendete Menge:</td>
                                <td align="left">
                                    <input name="campaign[versendet]" id="versendet" type="text" value="<?php echo $campaignEntity->getVersendet(); ?>" <?php echo $readOnlyPerformance; ?> />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">&Ouml;ffnungen (unique):</td>
                                <td align="left">
                                    <input name="campaign[openings_u]" id="openings_u" type="text" value="<?php echo $campaignEntity->getOpenings(); ?>" <?php echo $readOnlyPerformance; ?> />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">&Ouml;ffnungen:</td>
                                <td align="left">
                                    <input name="campaign[openings]" id="openings" type="text" value="<?php echo $campaignEntity->getOpenings_all(); ?>" <?php echo $readOnlyPerformance; ?> />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Klicks (unique):</td>
                                <td align="left">
                                    <input name="campaign[klicks_u]" id="klicks_u" type="text" value="<?php echo $campaignEntity->getKlicks(); ?>" <?php echo $readOnlyPerformance; ?> />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="info">Klicks:</td>
                                <td align="left">
                                    <input name="campaign[klicks]" id="klicks" type="text" value="<?php echo $campaignEntity->getKlicks_all(); ?>" <?php echo $readOnlyPerformance; ?> />
                                </td>
                            </tr>
                            <tr class="<?php echo $leadsContentClass; ?>">
                                <td align="right" class="info">Leads:</td>
                                <td align="left">
                                    <input name="campaign[leads]" id="leads" type="text" value="<?php echo $campaignEntity->getLeads(); ?>" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');" />
                                </td>
                            </tr>
                            <tr class="<?php echo $leadsContentClass; ?>">
                                <td align="right" class="info">Leads validiert:</td>
                                <td align="left">
                                    <input name="campaign[leads_u]" id="leads_u" type="text" value="<?php echo $campaignEntity->getLeads_u(); ?>" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="<?php echo $incomeContentClass; ?>">
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr class="<?php echo $incomeContentClass; ?>">
                    <td colspan="2" class="headerColumn">Einnahmen:</td>
                </tr>
                <tr class="<?php echo $incomeContentClass; ?>">
                    <td colspan="2">
                        <div id="volumeAndPerformance">&nbsp;</div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    if (YAHOO.util.Dom.get('apButton') !== null) {
		hideViewById('apButton');
    }
    
    /**
     * toggle - Settings
     */
    toggleViewById('priceContentBox');
    toggleViewById('deliverySystemDistributorContent');
    toggleViewById('deliverySystemDistributorDomainContent');
    
    /* openingTarget */
    oHide('<?php echo $campaignEntity->getMailtyp(); ?>');
	
    /* deliverySystem */
	var deliverySystem = YAHOO.util.Dom.get('deliverySystem');
	
    /* deliverySystemDistributor */
    var deliverySystemDistributor = YAHOO.util.Dom.get('deliverySystemDistributorItems');
    
    var volume = YAHOO.util.Dom.get('volumen').value;
    //resetValueDataIfValueIsNull(volume, 'volumen');
    
    var vz = YAHOO.util.Dom.get('vorgabe_m').value;
    resetValueDataIfValueIsNull(vz, 'vorgabe_m');
    
    <?php
    if ($mailing == 'HV') {
        ?>
        if (volume !== '' && volume !== vz) {
            YAHOO.util.Dom.get('cm_vorgabe_usq').value = ((vz / volume) * 100) - 100;
        }
    <?php
    } else {
        echo "YAHOO.util.Dom.get('cm_vorgabe_usq').value = '';";
		echo 'var hvCampaignDateObject = new Date("' . $hvCampaignDate->format('c') . '");';
    }
    
    echo '
        var showOButton = ' . $jsShowOButton . ';
        var newKampaign = ' . $jsNewKampaign. ';
    ';
    
    require_once(\RegistryUtils::get('emsWorkPath') . 'javascript/Module/Campaign/newCampaign.js');
    
	$jsContent = '';
    switch ($mailing) {
        case 'HV':
            switch ($action) {
                case 'edit':
                case 'copy':
					// toggle - deliverySystemDistributorCell
					$jsContent .= 'showViewById("deliverySystemDistributorCell");' . \chr(13);
                                        $jsContent .= 'showViewById("deliverySystemDistributorDomainCell");' . \chr(13);
                    
                    // showOrChangePriceByBillingType
                    $jsContent .= 'showOrChangePriceByBillingType(' 
                            . '"' . $campaignEntity->getAbrechnungsart() . '", ' 
                            . '"priceContentBox", ' 
                            . 'false'
                        . ');
                    ' . \chr(13);
                    
                    // createAndShowNewPerformanceContent
                    $jsContent .= 'createAndShowNewPerformanceContent("' . $campaignEntity->getAbrechnungsart() . '");' . \chr(13);
					
					$jsContent .= 'deliverySystem.disabled = true;' . \chr(13);
                    break;
            }
            break;
            
        case 'NV':
			// toggle - deliverySystemDistributorCell
			$jsContent .= 'showViewById("deliverySystemDistributorCell");' . \chr(13);
			$jsContent .= 'showViewById("deliverySystemDistributorDomainCell");' . \chr(13);
                                        
			// showOrChangePriceByBillingType
			$jsContent .= 'showOrChangePriceByBillingType(' 
					. '"' . $campaignEntity->getAbrechnungsart() . '", ' 
					. '"priceContentBox", ' 
					. 'false'
				. ');
			' . \chr(13);
			
            switch ($action) {
                case 'edit':
					$jsContent .= 'showViewById("campaign_use_campaign_start_date");' . \chr(13);
					if ((boolean) $jsShowOButton !== true) {
						$jsContent .= 'disableCampaign_useCampaignStartDate();' . \chr(13);
					}
					
                    // createAndShowNewPerformanceContent
                    $jsContent .= 'createAndShowNewPerformanceContent("' . $campaignEntity->getAbrechnungsart() . '");' . \chr(13);
					
					$jsContent .= 'deliverySystem.disabled = true;' . \chr(13);
                    break;
                
                case 'add_nv':
					if ($campaignEntity->getUse_campaign_start_date() === 1) {
						$jsContent .= 'YAHOO.util.Dom.get("campaign_imvs_1").checked = true;' . \chr(13) 
							. 'showViewById("campaign_use_campaign_start_date");' . \chr(13)
						;
					} else {
						$jsContent .= 'hideViewById("campaign_use_campaign_start_date");' . \chr(13);
					}
					
					$jsContent .= 'deliverySystem.disabled = false;' . \chr(13);
                    break;
            }
            break;
    }
	
	echo \chr(13) . $jsContent;
    ?>
</script>