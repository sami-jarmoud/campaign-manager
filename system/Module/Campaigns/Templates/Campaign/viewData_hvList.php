<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */

/* @var $hvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $tmpHvCampaigEntity \CampaignEntity */

$campaignStatusColor = \CampaignAndCustomerUtils::getStatusFieldById(
	$hvCampaignEntity->getStatus(),
	'campaignStatusColor'
);

$status_k = $campaignStatusColor;
$status_r = 'red';
$status_re = 'red';

if ($hvCampaignEntity->getStatus() >= 20 
	&& $hvCampaignEntity->getStatus() < 30
) {
	$status_k = '#5DB00D';
	$status_r = $campaignStatusColor;
} elseif ($hvCampaignEntity->getStatus() >= 30 
	&& $hvCampaignEntity->getStatus() < 33
) {
	$status_k = '#5DB00D';
	$status_r = '#5DB00D';
	$status_re = $campaignStatusColor;
}


$onClick = 'doIt(event'
	. ', \'' . $hvCampaignEntity->getMailtyp() . '\''
	. ', \'' . $hvCampaignEntity->getMail_id() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getCustomerEntity()->getFirma() . ', ' . $hvCampaignEntity->getK_name()) . '\''
	. ' , ' . $hvCampaignEntity->getK_id()
	. ', \'HV\''
	. ', ' . $hvCampaignEntity->getStatus()
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getCustomerEntity()->getFirma()) . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getK_name()) . '\''
	. ' ,\'' . $hvCampaignEntity->getDatum()->format('Y-m-d') . '\''
	. ', \'' . $hvCampaignEntity->getDatum()->format('H:i') . '\''
	. ', \'' . $hvCampaignEntity->getGebucht() . '\''
	. ', \'' . $hvCampaignEntity->getVorgabe_o() . '\''
	. ', \'' . $hvCampaignEntity->getVorgabe_k() . '\''
	. ', \'' . $hvCampaignEntity->getBlacklist() . '\''
	. ', \'' . $hvCampaignEntity->getVersandsystem() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getBearbeiter()) . '\''
	. ', \'' . $hvCampaignEntity->getTkp() . '\''
	. ', \'' . \htmlspecialchars($hvCampaignEntity->getContactPersonEntity()->getVorname() . ' ' . $hvCampaignEntity->getContactPersonEntity()->getNachname()) . '\''
	. ', \'\''
	. ', \'' . $hvCampaignEntity->getOpenings() . '\''
	. ', \'' . $hvCampaignEntity->getOpenings_all() . '\''
	. ', \'' . $hvCampaignEntity->getKlicks() . '\''
	. ', \'' . $hvCampaignEntity->getKlicks_all() . '\''
	. ', \'' . $hvCampaignEntity->getVersendet() . '\''
	. ', \'main\''
	. ', ' . intval($_SESSION['rechte'])
	. ', \'' . $mandant . '\''
	. ');'
;
$tableHvContentRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'id' => 's' . $hvCampaignEntity->getK_id(),
		'class' => $hvCampaignEntity->getK_id() . ', container',
		'onclick' => $onClick,
		'bgcolor' => $bgColor,
		'onMouseOver' => 'this.bgColor=\'#A6CBFF\'',
		'onMouseOut' => 'this.bgColor=\'' . $bgColor . '\''
	)
);


$hvContentList = $tableHvContentRowDataArray['begin'];
if ($hvCampaignEntity->getStatus() >= 33) {
	$hvContentList .= \HtmlTableUtils::createTableCellWidthContent(
		array(
			'style' => 'border-right: 1px solid #DBB7FF; padding: 0; text-align: center;'
		),
		'<img src="img/Tango/16/actions/dialog-apply.png" title="Rechnung bezahlt" />'
	);
} else {
	$hvContentList .= \HtmlTableUtils::createTableCellWidthContent(
		array(
			'style' => 'border-right: 1px solid #DBB7FF; padding: 0;',
			'align' => 'center'
		),
		'<div style="line-height: 11px; margin-top: -1px; width: 79px;">'
			. '<div class="floatLeft">'
				. '<div class="floatLeft" style="border: 1px solid #999999; width: 25px; height: 2px; background-color: ' . $status_k . ';"></div>'
				. '<div class="floatLeft" style="border: 1px solid #999999; width: 25px; height: 2px; margin-left: -1px; background-color: ' . $status_r . ';"></div>'
				. '<div class="floatLeft" style="border: 1px solid #999999; width: 25px; height: 2px; margin-left: -1px; background-color: ' . $status_re . ';"></div>'
			. '</div>'
			. '<div class="floatLeft" style="font-size: 9px; width: 79px; text-align: center; height: 22px; color: #666666;">' 
				. \CampaignAndCustomerUtils::getStatusFieldById(
					$hvCampaignEntity->getStatus(),
					'campaignViewLabel'
				)
			. '</div>'
		. '</div>'
	);
}


$v_zeichen = '';
if ($hvCampaignEntity->getVorgabe_k() != $cm_vorgabe_k || $hvCampaignEntity->getVorgabe_o() != $cm_vorgabe_o
) {
	$v_zeichen = '<a href="#" title="Ziel &Ouml;ffnung: ' . $hvCampaignEntity->getVorgabe_o() . '%, Ziel Klicks: ' . $hvCampaignEntity->getVorgabe_k() . '%" style="color: red;"><b>*</b></a>';
}

$hvContentList .= \HtmlTableUtils::createTableCellWidthContent(
	array(
		'style' => 'border-right: 1px solid #DBB7FF;',
		'title' => \htmlspecialchars($hvCampaignEntity->getK_name() . ', ' . $hvCampaignEntity->getCustomerEntity()->getFirma())
	),
	'<div style="font-weight: bold;">'
		. \FormatUtils::cutString(
			$hvCampaignEntity->getK_name(),
			$_SESSION['campaignView']['textLenght']
		) . $v_zeichen
			. \getSyncContentByDeliverySystem($hvCampaignEntity) . '<br />'
			. '<span style="font-size: 10px; color: #9933FF;">'
				. \FormatUtils::cutString(
					$hvCampaignEntity->getCustomerEntity()->getFirma(),
					$_SESSION['campaignView']['textLenght']
				)
			. '</span>'
		. '</div>'
);


$hvContentList .= 
		\CampaignFieldsUtils::getCellContentByUserTabFieldsDataArray(
			$campaignManager,
			$hvCampaignEntity,
			$tmpHvCampaigEntity,
			$today,
			$userTabFieldsDataArray,
			($nvCampaignsCount > 0 
				? true 
				: false
			),
			'HV'
		) . \HtmlTableUtils::createTableCellWidthContent(
			array(),
			''
		)
	. $tableHvContentRowDataArray['end']
;
$hvkosten = \CampaignFieldsUtils::getKosten(
		   $hvCampaignEntity
		);