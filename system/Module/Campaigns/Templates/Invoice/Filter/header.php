<select id="invoiceYear" name="invoice[filter][year]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
	<?php
		echo \HtmlFormUtils::createOptionListItemDataWithoutKey(
			$_SESSION['campaign']['yearsDataArray'],
			$year
		);
	?>
</select>
<select id="invoiceMonth" name="invoice[filter][month]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
        <option value="">- alle Monate -</option>
	<?php
		echo \HtmlFormUtils::createOptionListItemData(
			$monthDataArray,
			$month
		);
	?>
</select>
<select id="invoiceBillingType" name="invoice[filter][billingType]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
     <option value="">- alle Abr.arten -</option>
	<?php
		echo \HtmlFormUtils::createOptionListItemData(
			\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray,
			$billingType
		);
	?>
</select>
<select id="invoiceCampaignStatus" name="invoice[filter][campaignStatus]" style="font-size:14px;color:#565656;font-weight:bold;" class="spacerRightWrap">
	<option value="99">- alle Rechnungen-Status -</option>
	<?php
		echo \createOrderStatusSelectItems(
			$orderStatusDataArray,
			$campaignStatus
		);
	?>
</select>
<input type="button" id="invoiceFilterChangeView" onclick="invoiceFilterChangeView();" value="anzeigen" style="margin-left: 5px;" /><br class="clearBoth" />