<?php
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * addNewCampaignEntry
 * 
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param string $action
 * @return boolean|integer
 */
function addNewCampaignEntry(\CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $action) {
    $dataArray = array(
		'k_id' => array(
			'value' => $campaignEntity->getK_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'nv_id' => array(
			'value' => $campaignEntity->getNv_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'agentur_id' => array(
			'value' => $campaignEntity->getCustomerEntity()->getKunde_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'ap_id' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getAp_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'vertriebler_id' => array(
			'value' => $campaignEntity->getVertriebler_id(),
			'dataType' => \PDO::PARAM_INT
		),
		'status' => array(
			'value' => $campaignEntity->getStatus(),
			'dataType' => \PDO::PARAM_INT
		),
		'bearbeiter' => array(
			'value' => $campaignEntity->getBearbeiter(),
			'dataType' => \PDO::PARAM_STR
		),
		'hash' => array(
			'value' => $campaignEntity->getHash(),
			'dataType' => \PDO::PARAM_STR
		),
		'use_campaign_start_date' => array(
			'value' => $campaignEntity->getUse_campaign_start_date(),
			'dataType' => \PDO::PARAM_INT
		),
		
		// TODO: deprecated, entfernen
		'agentur' => array(
			'value' => $campaignEntity->getCustomerEntity()->getFirma(),
			'dataType' => \PDO::PARAM_STR
		),
		'ap' => array(
			'value' => $campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname(),
			'dataType' => \PDO::PARAM_STR
		),
	);

	switch ($action) {
		case 'add_nv':
			$dataArray['versendet'] = array(
				'value' => $campaignEntity->getVersendet(),
				'dataType' => \PDO::PARAM_INT
			);
			break;
	}


	/**
	 * createDataArray
	 * 
	 * debug
	 */
	$campaignDataArray = \array_merge(\createCampaignDataArray($campaignEntity), $dataArray);
	$debugLogManager->logData(__FUNCTION__, $campaignDataArray);
	unset($dataArray);

	return $campaignManager->addCampaignItem($campaignDataArray);
}

/**
 * createInTheDeliverySystem
 * 
 * @param \ClientManager $clientManager
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @return void
 */
function createInTheDeliverySystem(\ClientManager $clientManager, \CampaignManager $campaignManager, \debugLogManager $debugLogManager, \CampaignWidthCustomerAndContactPersonEntity $campaignEntity) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
    /**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity');
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);


	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);

	/**
	 * connect to deliveryWebservice
	 */
	$deliverySystemWebservice->login();
          	$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'Stellar Reserve':
                            case '4Wave Extra':    
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }      
	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'copyMailingAndUpdateCampaignData.php')) === true) {
		require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'copyMailingAndUpdateCampaignData.php');
	}

	/**
	 * disconnect from deliveryWebservice
	 */
	$deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}



/**
 * getLatestCampaignId
 * 
 * debug
 */
$latestKid = $campaignManager->getLatestCampaignId();
$debugLogManager->logData('latestKid', $latestKid);

$campaignEntity->setK_id($latestKid + 1);

if ($postAction == 'add_nv') {
	$campaignEntity->setNv_id($_POST['campaign']['kid']);
} else {
	// action: new or copy
	$campaignEntity->setNv_id($campaignEntity->getK_id());
}

$campaignEntity->setHash(\md5($mandant . $campaignEntity->getK_id()));


/**
 * addNewCampaignEntry
 * 
 * debug
 */
$idAddCampaignEntry = \addNewCampaignEntry(
	$campaignManager,
	$debugLogManager,
	$campaignEntity,
	$postAction
);
$debugLogManager->logData('idAddCampaignEntry', $idAddCampaignEntry);


if ((boolean) $imvs === true 
	&& $postAction == 'add_nv'
) {
	// debug
	$debugLogManager->logData('createInTheDeliverySystem', true);

	/**
	 * createInTheDeliverySystem
	 */
	\createInTheDeliverySystem(
		$clientManager,
		$campaignManager,
		$debugLogManager,
		$campaignEntity
	);
}


/**
 * addNewLogEntry
 * 
 * debug
 */
$idAddLogEntry = addNewLogEntry(
	$campaignManager,
	$campaignEntity,
	($_POST['action'] == 'add_nv' ? 3 : 1)
);
$debugLogManager->logData('idAddLogEntry', $idAddLogEntry);