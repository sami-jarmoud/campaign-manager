<?php
/* @var $campaignManager \CampaignManager */
/* @var $userEntity \UserEntity */



/**
 * getYearsDataArrayFromCampaing
 * 
 */
$_SESSION['campaign']['yearsDataArray'] = $campaignManager->getYearsDataArrayFromCampaing();
if (\count($_SESSION['campaign']['yearsDataArray']) === 0) {
	throw new \InvalidArgumentException('no campaigns Found!', 1433749057);
}
?>
<li>
	<h3><img src="img/Tango/22/apps/office-address-book.png" class="h3img" /> <span class="mymenuli"><div class="mymenulabel" onClick="setRequest('kampagne/overview.php', 'cm_ov');">Campaign Manager</div></span></h3>
	<div>
		<ul class="sub">
			<li>
				<a href="#" onClick="setRequest('kampagne/overview.php', 'cm_ov');">
					<img src="img/Oxygen/16/actions/help-about.png" class="liimg" /><div class="mymenulabela">Übersicht</div>
				</a>
			</li>
			<li>
				<a href="#" onClick="setRequest('kampagne/k_main.php?new=1', 'k');">
					<img src="img/Tango/16/apps/office-address-book.png" class="liimg" /><div class="mymenulabela">Kampagnen</div>
				</a>
			</li>
			<li>
				<a href="#" onClick="setRequest('kampagne/planer/index.php?view=month', 'p');">
					<img src="img/Tango/16/apps/office-calendar.png" class="liimg" /><div class="mymenulabela">Planer</div>
				</a>
			</li>
			<?php 
			if ($userEntity->getAbteilung() == 'g' 
				|| $userEntity->getAbteilung() == 'v' 
				|| $userEntity->getAbteilung() == 'b'
			) {
			?>
				<li>
					<a href="#" onClick="setRequest('Module/Campaigns/View/Turnover/index.php', 'u');">
						<img src="img/icon_euro_16.png" class="liimg" /><div class="mymenulabela">Ums&auml;tze</div>
					</a>
				</li>
			<?php
			}
			?>
			<li>
				<a href="#" onClick="setRequest('kampagne/report/', 'k_rep');">
					<img src="img/Tango/16/mimetypes/office-spreadsheet.png" class="liimg" /><div class="mymenulabela">Reporting</div>
				</a>
			</li>
			<?php 
			if ($userEntity->getAbteilung() == 'g' 
				|| $userEntity->getAbteilung() == 'b'
			) {
			?>
				<li>
					<a href="#" onClick="setRequest('Module/Campaigns/View/Invoice/index.php', 'invoice');">
						<img src="img/Tango/16/mimetypes/office-spreadsheet.png" class="liimg" /><div class="mymenulabela">Rechnung</div>
					</a>
				</li>
			<?php
			}
			?>
			<?php 
			// TODO: �ber benutzer rechte l�sen
			if ($_SESSION['intoneGroup']) {
			?>
				<li>
					<a href="#" onClick="window.open('Module/Campaigns/View/Campaign/live.php', '', 'width=600,height=300,scrollbars=yes');">
						<img src="img/Tango/16/apps/utilities-system-monitor.png" class="liimg" /><div class="mymenulabela">Live Monitor</div>
					</a>
				</li>
			<?php
			}
			?>
			<li>
				<a href="#" onClick="setRequest('kampagne/View/Customers/', 'cm_kunde');">
					<img src="img/Tango/16/mimetypes/office-contact.png" class="liimg" /><div class="mymenulabela">Kunden</div>
				</a>
			</li>
			<?php 
			// TODO: �ber benutzer rechte l�sen
			if ($_SESSION['intoneGroup']) {
			?>
				<li>
					<a href="#" onClick="setRequest('kampagne/View/Specialisten/DayList/index.php', 'k_tcm');">
						<img src="img/Tango/16/apps/internet-blog.png" class="liimg" /><div class="mymenulabela">Speziallisten</div>
					</a>
				</li>
			<?php
			}
			?>
			<?php 
			
			if ($userEntity->getAbteilung() == 't'
				|| $userEntity->getAbteilung() == 'v'
				|| $userEntity->getAbteilung() == 'g'
			  ) {
			?>
                                <li>
				    <a href="#" onClick="setRequest('dispatch.php?_module=Campaign&_controller=index&_action=zustand' , 'st');">
						<img src="img/icons/chart.png" class="liimg" /><div class="mymenulabela">Verteiler Stand</div>
					</a>
				</li>
                                
				<!--<li>
				    <a href="#" onClick="setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung&anzeigen=' + true , 'st');">
						<img src="img/icons/chart.png" class="liimg" /><div class="mymenulabela">Statistik</div>
					</a>
				</li>-->
			<?php
			}
			?>
		</ul>
	</div>
</li>