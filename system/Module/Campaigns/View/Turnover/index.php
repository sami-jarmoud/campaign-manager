<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * getTurnoverEditorsItems
 * 
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @param integer $mandantId
 * @param integer $selectedUserFirstAndLastName
 * @return string
 */
function getTurnoverEditorsItems(\ClientManager $clientManager, \debugLogManager $debugLogManager, $mandantId, $selectedUserFirstAndLastName) {
	$result = '';

	try {
		// debug
		$debugLogManager->beginGroup(__FUNCTION__);
		$debugLogManager->logData('mandantId', $mandantId);
		$debugLogManager->logData('selectedUserFirstAndLastName', $selectedUserFirstAndLastName);

		if (\in_array($_SESSION['abteilung'], array('g', 'b'))) {
			/**
			 * getDepartmentsUsersDataItemsByMandantId
			 */
			$usersDataArray = $clientManager->getDepartmentsUsersDataItemsByMandantId(
				$mandantId,
				'v,g',
				true
			);
		} else {
			$editorEntity = new \UserEntity();
			$editorEntity->setBenutzer_id(\RegistryUtils::get('userEntity')->getBenutzer_id());
			$editorEntity->setVorname(\RegistryUtils::get('userEntity')->getVorname());
			$editorEntity->setNachname(\RegistryUtils::get('userEntity')->getNachname());

			$usersDataArray = array(
				\RegistryUtils::get('userEntity')->getBenutzer_id() => $editorEntity
			);
			unset($editorEntity);
		}

		if (\count($usersDataArray) === 0) {
			throw new \InvalidArgumentException('no usersDataArray', 1433244352);
		}

		foreach ($usersDataArray as $userEntity) {
			/* @var $userEntity \UserEntity */

			$selected = '';
			if ($userEntity->getVorname() . ' ' . $userEntity->getNachname() == $selectedUserFirstAndLastName) {
				$selected = 'selected';
			}

			$result .= \HtmlFormUtils::createOptionItem(
				array(
					'value' => $userEntity->getVorname() . ' ' . $userEntity->getNachname(),
					'selected' => $selected
				),
				$userEntity->getVorname() . ' ' . $userEntity->getNachname()
			);
		}
		unset($usersDataArray);

		// debug
		$debugLogManager->endGroup();
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}

	return $result;
}

/**
 * getCampaignsQueryPartsDataArray
 * 
 * @param array $whereQueryPartsDataArray
 * @param ingeger $customerId
 * @return array
 */
function getCampaignsQueryPartsDataArray(array $whereQueryPartsDataArray, $customerId) {
	$campaignsWhereQueryPartsDataArray = array(
		'kId' => array(
			'sql' => '`k_id`',
			'value' => '`nv_id`',
			'comparison' => 'fieldEqual'
		),
		'status' => array(
			'sql' => '`status`',
			'value' => 4,
			'comparison' => \CampaignManager::$fieldGreaterThan
		),
		'customerId' => array(
			'sql' => '`agentur_id`',
			'value' => \intval($customerId),
			'comparison' => 'integerEqual'
		)
	);

	return array(
		'WHERE' => \array_merge($whereQueryPartsDataArray, $campaignsWhereQueryPartsDataArray),
	);
}

/**
 * initSalesByBillingType
 * 
 * @return array
 */
function initSalesByBillingType() {
	$salesByBillingType = array();
	foreach (\CampaignAndOthersUtils::$campaignBillingsTypeDataArray as $billingType) {
		$salesByBillingType[$billingType] = 0;
	}
	$salesByBillingType[\CampaignAndOthersUtils::$campaignBillingsTypeDataArray['Hybri'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndOthersUtils::$campaignBillingsTypeDataArray['CPL'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndOthersUtils::$campaignBillingsTypeDataArray['CPO'] . '_u'] = 0;
	$salesByBillingType[\CampaignAndOthersUtils::$campaignBillingsTypeDataArray['CPC'] . '_u'] = 0;

	return $salesByBillingType;
}

/**
 * initTurnoverData
 * 
 * @return array
 */
function initTurnoverData() {
	return array(
		'total' => array(
			'sum' => 0,
			'tkp' => 0,
			'cpx' => 0,
			'hybrid' => 0,
			'fixedprice' => 0,
			'send' => 0,
			'booked' => 0,
			'cost' => 0,
			'cover' => 0,
		),
		'netSum' => 0,
		'eTkpSum' => 0,
	);
}

/**
 * createYuiData
 * 
 * @param \CustomerEntity $customerEntity
 * @param array $salesByBillingType
 * @param integer $customerTotalSend
 * @param integer $customerTotalBooked
 * @return array
 */
function createYuiData(\CustomerEntity $customerEntity, array $salesByBillingType, $customerTotalSend, $customerTotalBooked, $customerTotalCost) {
	$resultDataArray = array(
		'company' => $customerEntity->getFirma(),
		'salesCustomer' => $salesByBillingType['TKP'] 
			+ (
				$salesByBillingType['CPL'] + $salesByBillingType['CPO'] + $salesByBillingType['CPC']
			) 
			+ $salesByBillingType['Hybrid'] 
			+ $salesByBillingType['Festpreis']
		,
		'salesCustomerNet' => $salesByBillingType['TKP'] 
			+ (
				$salesByBillingType['CPL_u'] + $salesByBillingType['CPO_u'] + $salesByBillingType['CPC_u']
			) 
			+ $salesByBillingType['Hybrid_u'] 
			+ $salesByBillingType['Festpreis']
		,
		'tkp' => $salesByBillingType['TKP'],
		'cpx' => (
			$salesByBillingType['CPL'] + $salesByBillingType['CPO'] + $salesByBillingType['CPC']
		),
		'hybrid' => $salesByBillingType['Hybrid'],
		'fixedprice' => $salesByBillingType['Festpreis'],
		'send' => $customerTotalSend,
		'booked' => $customerTotalBooked,
		'cost' => $customerTotalCost,
		'cover' => ($salesByBillingType['TKP'] 
			+ (
				$salesByBillingType['CPL'] + $salesByBillingType['CPO'] + $salesByBillingType['CPC']
			) 
			+ $salesByBillingType['Hybrid'] 
			+ $salesByBillingType['Festpreis'])
		    - $customerTotalCost,
	);
	
	$resultDataArray['effTkp'] = \FormatUtils::averageCalculation(
		$resultDataArray['salesCustomer'],
		$customerTotalSend
	);
	
	return $resultDataArray;
}

/**
 * calculateAndUpdateSalesByBillingTypeData
 * 
 * @param \CampaignEntity $campaignEntity
 * @param array $salesByBillingType
 * @return void
 */
function calculateAndUpdateSalesByBillingTypeData(\CampaignEntity $campaignEntity, array &$salesByBillingType) {
	switch ($campaignEntity->getAbrechnungsart()) {
		case 'Hybrid':
		case 'CPL':
		case 'CPO':
		case 'CPC':
			$salesByBillingType[$campaignEntity->getAbrechnungsart() . '_u'] += \FormatUtils::validPriceCalculationByBillingType(
				$campaignEntity,
				TRUE
			) + $campaignEntity->getUnit_price();

			$salesByBillingType[$campaignEntity->getAbrechnungsart()] += \FormatUtils::totalPriceCalculationByBillingType(
				$campaignEntity,
				TRUE
			);
			break;

		default:
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				$salesByBillingType[$campaignEntity->getAbrechnungsart()] += \FormatUtils::totalPriceCalculationByBillingType(
					$campaignEntity,
					TRUE
				);
			}
			break;
	}
}

/**
 * calculateAndUpdateTurnoverData
 * 
 * @param array $customerYuiData
 * @param array $turnoverDataArray
 * @return void
 */
function calculateAndUpdateTurnoverData(array $customerYuiData, array &$turnoverDataArray) {
	$turnoverDataArray['total']['sum'] += $customerYuiData['salesCustomer'];
	$turnoverDataArray['total']['tkp'] += $customerYuiData['tkp'];
	$turnoverDataArray['total']['cpx'] += $customerYuiData['cpx'];
	$turnoverDataArray['total']['hybrid'] += $customerYuiData['hybrid'];
	$turnoverDataArray['total']['fixedprice'] += $customerYuiData['fixedprice'];
	$turnoverDataArray['total']['send'] += $customerYuiData['send'];
	$turnoverDataArray['total']['booked'] += $customerYuiData['booked'];
	$turnoverDataArray['total']['cost'] += $customerYuiData['cost'];
	$turnoverDataArray['total']['cover'] += $customerYuiData['cover'];
	
	$turnoverDataArray['netSum'] += $customerYuiData['salesCustomerNet'];
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

/*
 * turnoverFilterSettings
 */
$debugLogManager->beginGroup('filterSettings');

$year = isset($_GET['turnover']['filter']['year']) ? \intval($_GET['turnover']['filter']['year']) : \date('Y');
$debugLogManager->logData('year', $year);

$month = isset($_GET['turnover']['filter']['month']) ? intval($_GET['turnover']['filter']['month']) : \intval(\date('m'));
$debugLogManager->logData('month', $month);

$editor = isset($_GET['turnover']['filter']['editor']) 
	? \DataFilterUtils::filterData(
		$_GET['turnover']['filter']['editor'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: null
;
$debugLogManager->logData('editor', $editor);

$deliverySystem = isset($_GET['turnover']['filter']['deliverySystem']) 
	? \DataFilterUtils::filterData(
		$_GET['turnover']['filter']['deliverySystem'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: null
;
$debugLogManager->logData('deliverySystem', $deliverySystem);

$dsdId = isset($_GET['turnover']['filter']['dsdId']) ? \intval($_GET['turnover']['filter']['dsdId']) : 0;
$debugLogManager->logData('dsdId', $dsdId);

$debugLogManager->endGroup();


/**
 * monthDataArray
 * 
 * debug
 */
$monthDataArray = \DateUtils::$monthDataArray;
$debugLogManager->logData('monthDataArray', $monthDataArray);


$tableColumnsDataArray = array(
	'salesCustomerNet' => array(
		'rowTitle' => 'Umsatz (validiert)',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'tkp' => array(
		'rowTitle' => 'TKP',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'effTkp' => array(
		'rowTitle' => 'eff. TKP',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'cpx' => array(
		'rowTitle' => 'CPX',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'hybrid' => array(
		'rowTitle' => 'Hybrid',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'fixedprice' => array(
		'rowTitle' => 'Festpreis',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'send' => array(
		'rowTitle' => 'Versendet',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'booked' => array(
		'rowTitle' => 'Gebucht',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'cost' => array(
		'rowTitle' => 'Kosten',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
	'cover' => array(
		'rowTitle' => 'Deckungsbeitrag',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),	
);

$tableHeaderColumnsDataArray = array(
	'company' => array(
		'rowTitle' => 'Kunde / Agentur',
		'style' => \CampaignFieldsUtils::alignLeft . ' font-weight: bold;'
	),
	'salesCustomer' => array(
		'rowTitle' => 'Umsatz',
		'style' => \CampaignFieldsUtils::alignRight . ' font-weight: bold;'
	),
);
$tableHeaderColumnsDataArray += $tableColumnsDataArray;
$debugLogManager->logData('tableHeaderColumnsDataArray', $tableHeaderColumnsDataArray);

$totalColspan = \count($tableHeaderColumnsDataArray);
$debugLogManager->logData('totalColspan', $totalColspan);


/**
 * initTurnoverData
 */
$turnoverDataArray = \initTurnoverData();

/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(
	array(
		'class' => 'tcm_table newTableView',
		'width' => '100%'
	)
);

// createTableRow - Tag
$tableRowDataArray = \HtmlTableUtils::createTableRow();


$deliverySystemShow = 'false';
$deliverySystemDistributorItems = $tableData = '';
$yuiDataArray = array();
$tkpTotalSend = 0;
try {
	$whereQueryPartsDataArray = array(
		'year' => array(
			'sql' => 'YEAR(' . $campaignManager->getCampaignTable() . '.`datum`)',
			'value' => $year,
			'comparison' => 'integerEqual'
		)
	);
	
	if (\strlen($editor) > 0) {
		$whereQueryPartsDataArray['bearbeiter'] = array(
			'sql' => $campaignManager->getCampaignTable() . '.`bearbeiter`',
			'value' => $editor,
			'comparison' => '='
		);
	}

	if ($month > 0) {
		$whereQueryPartsDataArray['month'] = array(
			'sql' => 'MONTH(' . $campaignManager->getCampaignTable() . '.`datum`)',
			'value' => $month,
			'comparison' => 'integerEqual'
		);
	}

	if (\strlen($deliverySystem) > 0) {
		/**
		 * CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz
		 * 
		 * debug
		 */
		$deliverySystemEntity = \CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz($deliverySystem);
		if (!($deliverySystemEntity instanceof \DeliverySystemEntity)) {
			throw new \DomainException('no DeliverySystemEntity: ' . $deliverySystem, 1426154158);
		}
		$debugLogManager->logData('deliverySystemEntity', $deliverySystemEntity);

		/**
		 * getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId
		 * 
		 * debug
		 */
		$deliverySystemDistributorDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemId(
			\RegistryUtils::get('clientEntity')->getId(),
			$deliverySystemEntity->getId(),
			$_SESSION['underClientIds']
		);
		$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);

		if ($dsdId > 0) {
			/**
			 * CampaignAndOthersUtils::getDeliverySystemDistributorEntityByDsdId
			 */
			$deliverySystemDistributorEntity = \CampaignAndOthersUtils::getDeliverySystemDistributorEntityByDsdId($dsdId);

			// dsd_id
			$whereQueryPartsDataArray['dsdId'] = array(
				'sql' => $campaignManager->getCampaignTable() . '.`dsd_id`',
				'value' => $deliverySystemDistributorEntity->getId(),
				'comparison' => 'integerEqual'
			);
		} else {
			$deliverySystemDistributorEntity = new \DeliverySystemDistributorEntity();
			
			// versandsystem
			$whereQueryPartsDataArray['deliverySystem'] = array(
				'sql' => $campaignManager->getCampaignTable() . '.`versandsystem`',
				'value' => $deliverySystem,
				'comparison' => '='
			);
		}
		$debugLogManager->logData('deliverySystemDistributorEntity', $deliverySystemDistributorEntity);

		if (\count($deliverySystemDistributorDataArray) > 0) {
			/**
			 * CampaignAndOthersUtils::getDeliverySystemDistributorsItems
			 */
			$deliverySystemDistributorItems = \CampaignAndOthersUtils::getDeliverySystemDistributorsItems(
				$deliverySystemDistributorDataArray,
				$deliverySystemDistributorEntity
			);
		}

		$deliverySystemShow = 'true';
	}
	$debugLogManager->logData('deliverySystemShow', $deliverySystemShow);

	/**
	 * campaignCustomer_queryPartsDataArray
	 * 
	 * debug
	 */
	$campaignCustomer_queryPartsDataArray = array(
		'SELECT' => $campaignManager->getCustomerTable() . '.`kunde_id`'
			. ',' . $campaignManager->getCustomerTable() . '.`firma`'
		,
		'WHERE' => $whereQueryPartsDataArray,
		'GROUP_BY' => $campaignManager->getCustomerTable() . '.`firma`',
		'ORDER_BY' => $campaignManager->getCustomerTable() . '.`firma` ASC'
	);
	$debugLogManager->logData('campaignCustomer_queryPartsDataArray', $campaignCustomer_queryPartsDataArray);


	/**
	 * viewData_header
	 */
	require_once(DIR_Module_Campaigns . 'Templates/Turnover/ViewData/header.php');
	$tableData .= $tableDataArray['begin'] . $tableHeaderContent;

	// debug
	$debugLogManager->beginGroup('turnoverView');


	/**
	 * getCustomerCampaignsDataArrayByQueryParts
	 * 
	 * debug
	 */
	$customersDataArray = $campaignManager->getCustomerCampaignsDataArrayByQueryParts($campaignCustomer_queryPartsDataArray);
	#$debugLogManager->logData('customersDataArray', $customersDataArray);
	if (\count($customersDataArray) > 0) {
		$c = 0;
		foreach ($customersDataArray as $customerEntity) {
			/* @var $customerEntity \CustomerEntity */

			// debug
			$debugLogManager->beginGroup($customerEntity->getKunde_id());
			$debugLogManager->logData('customerEntity', $customerEntity);

			/**
			 * initSalesByBillingType
			 */
			$salesByBillingType = \initSalesByBillingType();
			$customerTotalBooked = $customerTotalSend =$customerTotalCost = 0;

			/**
			 * getCampaignsQueryPartsDataArray
			 * 
			 * debug
			 */
			$campaign_queryPartsDataArray = \getCampaignsQueryPartsDataArray(
				$whereQueryPartsDataArray,
				$customerEntity->getKunde_id()
			);
			#$debugLogManager->logData('campaign_queryPartsDataArray', $campaign_queryPartsDataArray);

			/**
			 * getCampaignsDataItemsByQueryParts
			 * 
			 * debug
			 */
			$campaignsDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
				$campaign_queryPartsDataArray,
				\PDO::FETCH_CLASS
			);
			#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
			if (\count($campaignsDataArray) > 0) {
				require(DIR_Module_Campaigns . 'Templates/Turnover/ViewData/list.php');
			}

			/**
			 * createYuiData
			 * 
			 * debug
			 */
			$customerYuiDataArray = \createYuiData(
				$customerEntity,
				$salesByBillingType,
				$customerTotalSend,
				$customerTotalBooked,
				$customerTotalCost
			);
			$debugLogManager->logData('customerYuiDataArray', $customerYuiDataArray);

			/**
			 * create table rows data for print view
			 */
			require(DIR_Module_Campaigns . 'Templates/Turnover/Print/list.php');
			$tableData .= $tableContent;

			/**
			 * calculateAndUpdateTurnoverData
			 */
			\calculateAndUpdateTurnoverData(
				$customerYuiDataArray,
				$turnoverDataArray
			);

			$yuiDataArray[] = $customerYuiDataArray;
			unset($customerYuiData);

			$c++;

			// debzg
			$debugLogManager->endGroup();
		}

		$debugLogManager->logData('tkpTotalSend', $tkpTotalSend);
		$turnoverDataArray['eTkpSum'] = \FormatUtils::averageCalculation(
			$turnoverDataArray['total']['sum'],
			$tkpTotalSend
		);
	} else {
		require_once(DIR_Module_Campaigns . 'Templates/Turnover/ViewData/noData.php');

		$tableData .= $tableContent;
	}

	// debug
	$debugLogManager->endGroup();


	/**
	 * viewData_footer
	 */
	require_once(DIR_Module_Campaigns . 'Templates/Turnover/ViewData/footer.php');
	$tableData .= $tableFooterContent
		. $tableDataArray['end']
	;
} catch (\Exception $e) {
	require(\DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

$_SESSION['data'] = \utf8_decode($tableData);

// debug
$debugLogManager->logData('yuiDataArray', $yuiDataArray);
$debugLogManager->logData('turnoverDataArray', $turnoverDataArray);
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Turnover/index.php');