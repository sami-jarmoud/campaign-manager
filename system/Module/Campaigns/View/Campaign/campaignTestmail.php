<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * getTestListDataArray
 * 
 * @param \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity
 * @return mixed
 */
function getTestListDataArray(\DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity) {
	return \RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter 
		. $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getId() . \RegistryUtils::$arrayKeyDelimiter 
		. $deliverySystemDistributorWidthClientDeliverySystemEntity->getM_asp_id() . \RegistryUtils::$arrayKeyDelimiter 
		. 'testListDataArray'
	);
}

/**
 * processDeliverySystemWebserviceTestlist
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity
 * @param \debugLogManager $debugLogManager
 * @throws \DomainException
 * @return string
 */
function processDeliverySystemWebserviceTestlist(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity, \debugLogManager $debugLogManager , $an_mich) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$optionsItems = null;
	
	// getTestListDataArray
	$testListDataArray = \getTestListDataArray($deliverySystemDistributorWidthClientDeliverySystemEntity);
	if (!isset($testListDataArray)) {
		$debugLogManager->logData('notFoundTestListDataArray', true);
		$debugLogManager->logData('getClientDeliveryEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
		
		/**
		* DeliverySystemFactory::getAndInitDeliverySystem
		* 
		* debug
		*/
		$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
		$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);


		/**
		 * connect to deliveryWebservice
		 */
		$deliverySystemWebservice->login();
               $asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':
                            case 'Stellar Reserve':
                            case '4Wave Extra':     
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
		if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testlist.php')) === true) {
			require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testlist.php');
		} else {
			$file = __FILE__;
			require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
		}

		/**
		 * disconnect from deliveryWebservice
		 */
		$deliverySystemWebservice->logout();
	} else {
		$debugLogManager->logData('foundTestListDataArray', true);
		$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':  
                            case 'Stellar Reserve':
                            case '4Wave Extra':     
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
		if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testlist.php')) === true) {
			require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testlist.php');
		} else {
			$file = __FILE__;
			require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
		}
	}
	
	// debug
	$debugLogManager->endGroup();
	
	if($an_mich == "0"){
			$result = \HtmlFormUtils::createSelectItem(
		array(
			'onchange' => 'changeTestlist(' . $campaignEntity->getK_id() . ', this.value);'
		),
		$optionsItems
	);
	}  else {
			$result = \HtmlFormUtils::createSelectItem(
		array(
			'onchange' => 'changeTestlistForMe(' . $campaignEntity->getK_id() . ', this.value);'
		),
		$optionsItems
	);
	}

	return $result;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$an_mich = isset($_GET['an_mich']) ? ($_GET['an_mich']) : 0;
$debugLogManager->logData('an_mich', $an_mich);

$filter = isset($_GET['filter']) ? ($_GET['filter']) : 0;
$debugLogManager->logData('filter', $filter);

$defaultRecipientListId = null;

try {
	if ($campaignId > 0) {
		/**
		 * getCampaignDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		$debugLogManager->logData('campaignEntity', $campaignEntity);
		
		/**
		 * getDeliverySystemDistributorWidthDeliverySystemEntityById
		 * 
		 * debug
		 */
		$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
		if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
			throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
		}
		$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);

		
		/**
		 * processDeliverySystemWebserviceTestlist
		 */
		$testlistSelectItem = \processDeliverySystemWebserviceTestlist(
			$campaignEntity,
			$deliverySystemDistributorWidthClientDeliverySystemEntity,
			$debugLogManager,
			$an_mich
		);
	} else {
		throw new \InvalidArgumentException('invalid campaignId', 1424766235);
	}
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
	require(DIR_configs . 'exceptions.php');
	
	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/campaignTestmail.php');