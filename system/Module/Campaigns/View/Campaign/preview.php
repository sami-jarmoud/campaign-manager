<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * processDeliverySystemWebservicePreviewMailing
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param string $mailType
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @throws \DomainException
 * @return void
 */
function processDeliverySystemWebservicePreviewMailing(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $mailType, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
	
	
	/**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	
	if ((\file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'previewMailing.php')) === true) {
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'previewMailing.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	/**
     * disconnect from deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaign', $campaignId);

$mailType = isset($_GET['mailingType']) 
	? \DataFilterUtils::filterData($_GET['mailingType']) 
	: ''
;
$debugLogManager->logData('mailType', $mailType);

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

try {
	if ($campaignId > 0) {
		/**
		 * getCampaignAndCustomerDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		$debugLogManager->logData('campaignEntity', $campaignEntity);
		
		if (\strlen($campaignEntity->getMail_id()) > 0) {
			/**
			 * processDeliverySystemWebservicePreviewMailing
			 */
			\processDeliverySystemWebservicePreviewMailing(
				$campaignEntity,
				$mailType,
				$clientManager,
				$debugLogManager
			);
		}
	} else {
		// keine kampagne id übermittelt
		throw new \InvalidArgumentException('invalid campaignId', 1424766235);
	}
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	$resultDataArray['success'] = false;
	$resultDataArray['content'] = $e->getMessage();
}