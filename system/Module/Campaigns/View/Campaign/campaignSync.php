<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * processDeliverySystemWebserviceSyncTabStatus
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return string
 * @throws \DomainException
 */
function processDeliverySystemWebserviceSyncTabStatus(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	$optionsItems = '';
	if ((\file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'syncTabStatus.php')) === true) {
		\DeliverySystemFactory::loadClassInstance($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp_abkz());
		
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'syncTabStatus.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	// debug
	$debugLogManager->endGroup();
	
	return $optionsItems;
}

/**
 * processDeliverySystemWebserviceAndUpdateCampaignData
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \CampaignManager $campaignManager
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return void
 * 
 * @throws \DomainException
 */
function processDeliverySystemWebserviceAndUpdateCampaignData(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \CampaignManager $campaignManager, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
     * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
     */
    $deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
    $debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
	
	
	/**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	
	if ((\file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php')) === true) {
		$actionId = 16;
		require_once(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'updateSyncCampaign.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	/**
     * disconnect from deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	// debug
	$debugLogManager->endGroup();
}


/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$mailingId = isset($_GET['campaign']['m_id']) 
	? \DataFilterUtils::filterData(
		$_GET['campaign']['m_id'],
		\DataFilterUtils::$validateFilterDataArray['string']
	) 
	: ''
;
$debugLogManager->logData('mailingId', $mailingId);

try {
	if ($campaignId > 0) {
		$_SESSION['campaign']['sync'] = array();
		
		/**
		 * getCampaignAndCustomerDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		
		/**
		 * processDeliverySystemWebserviceSyncTabStatus
		 */
		$syncTabStatusSelectItem = \processDeliverySystemWebserviceSyncTabStatus(
			$campaignEntity,
			$clientManager,
			$debugLogManager
		);
		
		if (\strlen($mailingId) > 0) {
			if ((boolean) \BaseUtils::isProdSystem()
				|| (
					(boolean) \BaseUtils::isTestSystem() 
					&& \BaseUtils::getDebugUserId() !== \RegistryUtils::get('userEntity')->getBenutzer_id()
				)
                || (
					(boolean) \BaseUtils::isDevSystem() 
					&& \BaseUtils::getDebugUserId() !== \RegistryUtils::get('userEntity')->getBenutzer_id()
				)						
			) {

			$countMailing = $campaignManager->getMailCountByMailingId($mailingId);
              if(\intval($countMailing) <= 1){
				$campaignEntity->setMail_id($mailingId);
			
				/**
				* processDeliverySystemWebserviceAndUpdateCampaignData
				*/
				\processDeliverySystemWebserviceAndUpdateCampaignData(
					$campaignEntity,
					$campaignManager,
					$clientManager,
					$debugLogManager
				);
			  }else{
                                       $errorMailing = 'TRUE';
                                       $syncCampagnArray = array();
                                       $syncCampagns = $campaignManager->getSyncCampaignsByMailingId($mailingId);
                                       foreach ($syncCampagns as $syncCampagn){
                                           $syncCampagnArray[] = $syncCampagn->getK_name();             
                                       }
                                       $debugLogManager->logData('syncCampagns', $syncCampagns);
			  }
			} else {
				// sync update deativiert
				$debugLogManager->logData('fakeSyncUpdate', 'fakeSyncUpdate');
			}
			
			unset($_SESSION['campaign']['sync']);
		}
		
		// debug
		$debugLogManager->logData('campaignEntity', $campaignEntity);
	} else {
		throw new \InvalidArgumentException('invalid campaignId', 1424766235);
	}
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
	require(DIR_configs . 'exceptions.php');
	
	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/campaignSync.php');