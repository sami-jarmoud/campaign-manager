<?php
\header('Content-Type: text/html; charset=utf-8');


/**
 * getCampaignQueryPartsByDeliverySystem
 * 
 * @param \DateTime $dateTime
 * @param string $deliverySystem
 * @return array
 */
function getCampaignQueryPartsByDeliverySystem(\DateTime $dateTime, $deliverySystem = 'BM') {
	return array(
		'SELECT' => '*',
		'WHERE' => array(
			'mailId' => array(
				'sql' => '`mail_id`',
				'value' => null,
				'comparison' => '<>'
			),
			'date' => array(
				'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
				'value' => $dateTime->format('Y-m-d'),
				'comparison' => '='
			),
			'deliverySystem' => array(
				'sql' => '`versandsystem`',
				'value' => $deliverySystem,
				'comparison' => \PdoDbManager::$fieldEqual
			),
		),
		'ORDER_BY' => '`datum` ASC'
	);
}

/**
 * processDeliverySystemWebserviceForLiveView
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param \ClientEntity $clientEntity
 * @param \ClientDeliverySystemWidthDeliverySystemEntity $deliverySystemDistributorWidthClientDeliveryEntity
 * @param \debugLogManager $debugLogManager
 * @param string $campaignStatus
 * @param integer $campaignShips
 * @param array $mailingIdDone
 * @return null|string
 */
function processDeliverySystemWebserviceForLiveView(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, \ClientEntity $clientEntity, \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliveryEntity, \debugLogManager $debugLogManager, &$campaignStatus, &$campaignShips, array &$mailingIdDone) {
	$result = null;
	
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
     * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
     */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliveryEntity->getClientDeliveryEntity());
	
	
	/**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
	
	if ((\file_exists(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliveryEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'liveCampaign.php')) === true) {
		include(DIR_deliverySystems . $deliverySystemDistributorWidthClientDeliveryEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp() . \DIRECTORY_SEPARATOR . 'liveCampaign.php');
	} else {
		\DebugAndExceptionUtils::sendDebugData(
			array(
				'clientDeliveryEntity' => $deliverySystemDistributorWidthClientDeliveryEntity->getClientDeliveryEntity(),
				'campaignEntity' => $campaignEntity
			),
			__FUNCTION__ . ': ' . $deliverySystemDistributorWidthClientDeliveryEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp()
		);
		
		$file = __FILE__;
		require(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	/**
     * disconnect from deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
	
	return $result;
}



require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_Factory . 'DeliverySystemFactory.php');


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


$tableNoContentRowDataArray = $tableContentListRowDataArray = \HtmlTableUtils::createTableRow(
	array(
		'style' => 'background: #fff;'
	)
);

$alertContent = $liveContentData = null;
$mailingIdDone = array();

try {
	/**
	 * getClientsAccessDataArrayByLoggedUser
	 * 
	 * debug
	 */
	$userClientsDataArray = \ClientUtils::getClientsAccessDataArrayByLoggedUser();
	$debugLogManager->logData('userClientsDataArray', $userClientsDataArray);
	#\DebugAndExceptionUtils::showDebugData($userClientsDataArray, 'userClientsDataArray');
	
	foreach ($userClientsDataArray as $clientId => $clientName) {
		// debug
		$debugLogManager->beginGroup('client: ' . $clientName);
		
		/**
		 * getClientDataById
		 * 
		 * debug
		 */
		$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new \DomainException('invalid ClientEntity', 1436449286);
		}
		$debugLogManager->logData('clientEntity', $clientEntity);
		
		/**
		 * getUnderClientIdsByClientId
		 * 
		 * debug
		 */
		$underClientIds = $clientManager->getUnderClientIdsByClientId($clientId);
		$debugLogManager->logData('underClientIds', $underClientIds);
		
		/**
		 * getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId
		 * 
		 * debug
		 */
		$deliverySystemDistributorsWidthClientDeliveryDataArray = $clientManager->getDeliverySystemDistributorsAndClientDeliveryDataItemsByClientId(
			$clientId,
			$underClientIds
		);
		$debugLogManager->logData('deliverySystemDistributorsWidthClientDeliveryDataArray', $deliverySystemDistributorsWidthClientDeliveryDataArray);
		#\DebugAndExceptionUtils::showDebugData($deliverySystemDistributorsWidthClientDeliveryDataArray, 'deliverySystemDistributorsWidthClientDeliveryDataArray');
		
		
		/**
		 * init $campaignManager
		 */
		$mandant = $clientEntity->getAbkz();
		require(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');
		require(DIR_configsInit . 'initCampaignManager.php');
		/* @var $campaignManager \CampaignManager */
		
		/**
		 * getCampaignQueryPartsByDeliverySystem
		 * 
		 * debug
		 */
		$queryPartsDataArray = \getCampaignQueryPartsByDeliverySystem(
			\RegistryUtils::get('dateNow'),
			'BM'
		);
		$debugLogManager->logData('queryPartsDataArray', $queryPartsDataArray);
		
		/**
		 * getCampaignsAndCustomerDataItemsByQueryParts
		 * 
		 * debug
		 */
		$campaignDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($queryPartsDataArray);
		#$debugLogManager->logData('campaignDataArray', $campaignDataArray);
		if (\count($campaignDataArray) > 0) {
			include(\DIR_Module_Campaigns . 'Templates/Campaign/Live/viewData_list.php');
		} else {
			include(\DIR_Module_Campaigns . 'Templates/Campaign/Live/viewData_noMailingsFound.php');
		}
		
		
		$debugLogManager->endGroup();
	}
} catch (\Exception $e) {

}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

\RegistryUtils::set(
	'campaign',
	array(
		'mailingIdDone' => \array_unique($mailingIdDone)
	)
);

#\DebugAndExceptionUtils::showDebugData($mailingIdDone, 'mailingIdDone');
#\DebugAndExceptionUtils::showDebugData($alertContent, 'alertContent');
require_once(DIR_Module_Campaigns . 'Templates/Campaign/live.php');