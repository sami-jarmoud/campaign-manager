<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * processDeliverySystemWebserviceTestlistAddressen
 * 
 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
 * @param mixed $recipientListId
 * @param array $attributesDataArray
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @throws DomainException
 * 
 * @return array
 */
function processDeliverySystemWebserviceTestAddresse(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $recipientListId, $attributesDataArray, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	$resultDataArray = array();
	
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getDeliverySystemDistributorWidthDeliverySystemEntityById
	 * 
	 * debug
	 */
	$deliverySystemDistributorWidthClientDeliverySystemEntity = $clientManager->getDeliverySystemDistributorWidthDeliverySystemEntityById($campaignEntity->getDsd_id());
	if (!($deliverySystemDistributorWidthClientDeliverySystemEntity instanceof \DeliverySystemDistributorWidthClientDeliverySystemEntity)) {
		throw new \DomainException('invalid DeliverySystemDistributorWidthClientDeliverySystemEntity', 1424766373);
	}
	$debugLogManager->logData('deliverySystemDistributorWidthClientDeliverySystemEntity', $deliverySystemDistributorWidthClientDeliverySystemEntity);
	
	
	/**
	 * DeliverySystemFactory::getAndInitDeliverySystem
	 * 
	 * debug
	 */
	$deliverySystemWebservice = \DeliverySystemFactory::getAndInitDeliverySystem($deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity());
	$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);
    
    
    /**
     * connect to deliveryWebservice
     */
    $deliverySystemWebservice->login();
		$asps = $deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getAsp();
                 switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black': 
                            case 'Stellar Reserve':
                            case '4Wave Extra':     
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
	if ((\file_exists(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testAddresse.php')) === true) {
		require_once(DIR_deliverySystems . $asp . \DIRECTORY_SEPARATOR . 'testAddresse.php');
	} else {
		$file = __FILE__;
		require_once(DIR_deliverySystems . 'noDeliverySystemImplemented.php');
	}
	
	// debug
	$debugLogManager->logData('resultDataArray', $resultDataArray);
    
    /**
     * disconnect to deliveryWebservice
     */
    $deliverySystemWebservice->logout();
	
	$debugLogManager->endGroup();
	
	return $resultDataArray;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$recipientListId = isset($_GET['recipient']['listId']) 
	? \DataFilterUtils::filterData(
		$_GET['recipient']['listId'],
		 \DataFilterUtils::$validateFilterDataArray['string']
	) 
	: null
;
$debugLogManager->logData('recipientListId', $recipientListId);

try {
	if ($campaignId > 0) {
		/**
		 * getCampaignDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		$debugLogManager->logData('campaignEntity', $campaignEntity);
		
		/**
		 * processDeliverySystemWebserviceTestlistAddressen
		 */
		$resultDataArray = \processDeliverySystemWebserviceTestAddresse(
			$campaignEntity,
            $recipientListId,
            $attributes_testmail,
			$clientManager,
            $debugLogManager
        );
    } else {
        throw new \InvalidArgumentException('invalid campaignId', 1424766235);
    }
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
    require(DIR_configs . 'exceptions.php');
	
	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
echo $resultDataArray[0]['email'];
echo $recipientListId;

?>

<script type="text/javascript">
	var k_id =<?php echo $campaignId;?>;
	var list_id =<?php echo $recipientListId;?>;
	var email ='<?php echo $resultDataArray[0]['email'];?>';
	processAjaxRequest(
					'AjaxRequests/ajax.php',
					'actionMethod=campaign' 
						+ '&actionType=sendTestmail' 
						+ '&campaign[k_id]=' + k_id
				        + '&recipients[]=' + email
						+ '&recipient[listId]=' + list_id
					,
					'',
					function () {}
				);
		alert('Email an ' + email +  ' erfolgreich versendet!');
</script>	