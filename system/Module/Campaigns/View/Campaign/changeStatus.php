<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * createTableContentByStatusDataArray
 * 
 * @param array $tableDataArray
 * @param array $tableRowDataArray
 * @param array $dataArray
 * @param integer $campaignStatus
 * @param mixed $jsOnClickFunction
 * @return string
 */
function createTableContentByStatusDataArray(array $tableDataArray, array $tableRowDataArray, array $dataArray, $campaignStatus, $jsOnClickFunction = null) {
	$result = $tableDataArray['begin'];

	foreach ($dataArray as $item) {
		$checked = '';
		if ((int) $item['status'] === $campaignStatus) {
			$checked = 'checked';
		}

		$result .=
			$tableRowDataArray['begin']
				. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'class' => 'status_inp'
					),
					\HtmlFormUtils::createRadiobox(
						array(
							'name' => 'campaign[newStatus]',
							'value' => $item['status'],
							'id' => 'newStatus_' . $item['status'],
							'class' => 'noBorder',
							'checked' => $checked,
							'onclick' => (\count($jsOnClickFunction) ? $jsOnClickFunction['radioBox'] : null)
						)
					)
				) . \HtmlTableUtils::createTableCellWidthContent(
					array(),
					\HtmlFormUtils::createLabelFieldItem(
						$item['label'],
						array(
							'class' => 'status_label widthFull reset margin padding',
							'for' => 'newStatus_' . $item['status'],
							'onclick' => (\count($jsOnClickFunction) ? $jsOnClickFunction['label'] : null)
						)
					) . $item['sublabel']
				)
			. $tableRowDataArray['end']
		;
	}
	$result .= $tableDataArray['end'];

	return $result;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


// debug
$campaignId = isset($_GET['campaign']['k_id']) ? \intval($_GET['campaign']['k_id']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

try {
	if ($campaignId > 0) {
		/**
		 * getCampaignAndCustomerDataItemById
		 * 
		 * debug
		 */
		$campaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignId);
		if (!($campaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity', 1424766475);
		}
		$debugLogManager->logData('campaignEntity', $campaignEntity);
		
		$campaignRecipientId = ($campaignEntity->getContactPersonEntity()->getVertriebler_id()) 
			? $campaignEntity->getContactPersonEntity()->getVertriebler_id() 
			: $campaignEntity->getVertriebler_id()
		;
		$debugLogManager->logData('campaignRecipientId', $campaignRecipientId);
		
		/**
		 * getUserDataItemById
		 * 
		 * debug
		 */
		$campaignRecipientUserEntity = $clientManager->getUserDataItemById($campaignRecipientId);
		if (!($campaignRecipientUserEntity instanceof \UserEntity)) {
			$campaignRecipientUserEntity = new \UserEntity();
			$campaignRecipientUserEntity->setEmail($_SESSION['u_email']);
			$campaignRecipientUserEntity->setNachname($_SESSION['u_nachname']);
			$campaignRecipientUserEntity->setVorname($_SESSION['u_vorname']);
			
			\MailUtils::sendEmailToUser(
				$_SESSION['u_email'],
				'Mandant: ' . \ClientUtils::getClientNameByClientId($_SESSION['mID']) . \chr(11) 
					. 'Kampagne: ' . $campaignEntity->getK_name() . \chr(11)
					. 'Kunde: ' . $campaignEntity->getCustomerEntity()->getFirma() . \chr(11)
					. 'Ansprechpartner: ' . $campaignEntity->getContactPersonEntity()->getVorname() . ' ' . $campaignEntity->getContactPersonEntity()->getNachname() . \chr(11) . \chr(11)
					. 'Nachricht: ' . 'hat kein g�ltiger Vertriebler mehr!'
			);
		}
		$debugLogManager->logData('campaignRecipientUserEntity', $campaignRecipientUserEntity);


		// createRow - Tag
		$tableDataArray = \HtmlTableUtils::createTable(
			array(
				'class' => 'status_table extendedTable'
			)
		);

		// createTableRow - Tag
		$tableRowDataArray = \HtmlTableUtils::createTableRow(array());


		/**
		 * campaignsStatusDataArray
		 * 
		 * debug
		 */
		if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
			// hvCampaign
			$campaignsStatusDataArray = \CampaignAndCustomerUtils::$hvCampaignsStatusDataArray;
		} else {
			$campaignsStatusDataArray = \CampaignAndCustomerUtils::$nvCampaignsStatusDataArray;
		}
		$debugLogManager->logData('campaignsStatusDataArray', $campaignsStatusDataArray);

		$campaignStatusSelected = $campaignReportingSelected = $campaignOrderSelected = '';
		if ($campaignEntity->getStatus() < 20) {
			$campaignStatusSelected = ' class="selected"';
		} elseif ($campaignEntity->getStatus() >= 20 && $campaignEntity->getStatus() < 30) {
			$campaignReportingSelected = ' class="selected"';
		} elseif ($campaignEntity->getStatus() >= 30) {
			$campaignOrderSelected = ' class="selected"';
		}

		$campaignLiItems = '';
		if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
			// hvCampaign
			$campaignLiItems = '<li' . $campaignReportingSelected . '><a href="#tab2"><em>Reporting</em></a></li>' 
				. '<li' . $campaignOrderSelected . '><a href="#tab3"><em>Rechnung</em></a></li>'
			;
			
			/**
			* campaignsReportingDataArray
			* 
			* debug
			*/
		   $campaignsReportingDataArray = \CampaignAndCustomerUtils::$campaignsReportingStatusDataArray;
		   $debugLogManager->logData('campaignsReportingDataArray', $campaignsReportingDataArray);

		   /**
			* campaignsOrderDataArray
			* 
			* debug
			*/
		   $campaignsOrderDataArray = \CampaignAndCustomerUtils::$campaignsOrderStatusDataArray;
		   $debugLogManager->logData('campaignsOrderDataArray', $campaignsOrderDataArray);
		}
	} else {
		throw new \InvalidArgumentException('invalid campaignId', 1424766235);
	}
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
	require(\DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/changeStatus.php');