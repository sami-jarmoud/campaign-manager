<?php
\header('Content-Type: text/html; charset=utf-8');


/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
require_once(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);


try {
	/**
	 * getTabFieldsDataArrayByIdUser
	 * 
	 * debug
	 */
	$userTabFieldsDataArray = $clientManager->getTabFieldsDataArrayByIdUser(\intval($_SESSION['benutzer_id']));
	if (!\is_array($userTabFieldsDataArray)) {
		throw new \InvalidArgumentException('invalid userTabFieldsDataArray', 1424943681);
	}
	$debugLogManager->logData('userTabFieldsDataArray', $userTabFieldsDataArray);

	/**
	 * campaignTabFieldsDataArray
	 * 
	 * debug
	 */
	$campaignTabFieldsDataArray = \CampaignAndCustomerUtils::$allTabFieldsDataArray;
	$debugLogManager->logData('campaignTabFieldsDataArray', $campaignTabFieldsDataArray);

	$tab_felder_active = $tab_felder_deactive = '';
	$tb = $tb2 = 1;
	foreach ($userTabFieldsDataArray as $userField) {
		if (isset($campaignTabFieldsDataArray[$userField])) {
			$tab_felder_active .=
				'<li class="list1" id="li1_' . $tb . '">'
					. '<div style="width:10px;margin-left:5px;margin-right:5px;margin-top:2px;float:left">'
						. '<div style="background-color:#999999;width:10px;height:2px;"></div>'
						. '<div style="background-color:#999999;width:10px;height:2px;margin-top:2px;"></div>'
						. '<div style="background-color:#999999;width:10px;height:2px;margin-top:2px;"></div>'
					. '</div> '
					. $campaignTabFieldsDataArray[$userField]['label']
					. '<input type="hidden" name="saveTabs[tabs][]" value="' . $userField . '" />'
				. '</li>'
			;
		}

		$tb++;
	}

	foreach ($campaignTabFieldsDataArray as $fieldKey => $feldArray) {
		if ((\in_array($fieldKey, $userTabFieldsDataArray)) !== true) {
			$tab_felder_deactive .=
				'<li class="list2" id="li2_' . $tb2 . '">'
					. '<div style="width:10px;margin-left:5px;margin-right:5px;margin-top:2px;float:left">'
						. '<div style="background-color:#999999;width:10px;height:2px;"></div>'
						. '<div style="background-color:#999999;width:10px;height:2px;margin-top:2px;"></div>'
						. '<div style="background-color:#999999;width:10px;height:2px;margin-top:2px;"></div>'
					. '</div> '
					. $feldArray['label']
					. '<input type="hidden" name="saveTabs[tabs][]" value="' . $fieldKey . '" />'
				. '</li>'
			;
		}

		$tb2++;
	}
} catch (\Exception $e) {
	$debugLogManager->logData('Exception', $e);
	
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

require_once(DIR_Module_Campaigns . 'Templates/Campaign/previewCampaignFields.php');