<?php
namespace EMS\Campaigns\Invoice\Dto;

class CountryDto {
	protected $priceInfo = '';
	protected $additionalInfo = '';
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	****************************************************************************************** */
	public function getPriceInfo() {
		return $this->priceInfo;
	}
	public function setPriceInfo($priceInfo) {
		$this->priceInfo = $priceInfo;
	}

	public function getAdditionalInfo() {
		return $this->additionalInfo;
	}
	public function setAdditionalInfo($additionalInfo) {
		$this->additionalInfo = $additionalInfo;
	}

}
