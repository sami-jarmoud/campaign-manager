<?php
namespace EMS\Campaigns\Invoice\Dto;

class ProductDto {
	/**
	 * @var float
	 */
	protected $netPrice = 0;
	
	/**
	 * @var float
	 */
	protected $countryTaxPrice = 0;
	
	/**
	 * @var float
	 */
	protected $totalPrice = 0;
	
	/**
	 * @var float
	 */
	protected $cashDiscountPrice = 0;
	
	/**
	 * @var float
	 */
	protected $cashDiscountTotalPrice = 0;
	
	protected $dataArray = array();
	
	
	
	
	
	/**
	 * addNetPrice
	 * 
	 * @param float $netPrice
	 * @return void
	 */
	public function addNetPrice($netPrice) {
		$this->netPrice += \floatval($netPrice);
	}
	
	/**
	 * addCountryTaxPrice
	 * 
	 * @param float $countryTaxPrice
	 * @return void
	 */
	public function addCountryTaxPrice($countryTaxPrice) {
		$this->countryTaxPrice += \floatval($countryTaxPrice);
	}
	
	/**
	 * addTotalPrice
	 * 
	 * @param float $totalPrice
	 * @return void
	 */
	public function addTotalPrice($totalPrice) {
		$this->totalPrice += \floatval($totalPrice);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getNetPrice() {
		return (float) $this->netPrice;
	}
	public function setNetPrice($netPrice) {
		$this->netPrice = \floatval($netPrice);
	}
	
	public function getCountryTaxPrice() {
		return (float) $this->countryTaxPrice;
	}
	public function setCountryTaxPrice($countryTaxPrice) {
		$this->countryTaxPrice = \floatval($countryTaxPrice);
	}

	public function getTotalPrice() {
		return (float) $this->totalPrice;
	}
	public function setTotalPrice($totalPrice) {
		$this->totalPrice = \floatval($totalPrice);
	}
	
	public function getCashDiscountPrice() {
		return (float) $this->cashDiscountPrice;
	}
	public function setCashDiscountPrice($cashDiscountPrice) {
		$this->cashDiscountPrice = \floatval($cashDiscountPrice);
	}

	public function getCashDiscountTotalPrice() {
		return (float) $this->cashDiscountTotalPrice;
	}
	public function setCashDiscountTotalPrice($cashDiscountTotalPrice) {
		$this->cashDiscountTotalPrice = \floatval($cashDiscountTotalPrice);
	}
		
	public function getDataArray() {
		return $this->dataArray;
	}
	public function setDataArray($dataArray) {
		$this->dataArray = $dataArray;
	}

}
