<?php
namespace EMS\Campaigns\Invoice\Writer;

abstract class WriterAbstract {
	/**
	 * @var string
	 */
	protected $invoiceResultsPath;
	
	/**
	 * @var string
	 */
	protected $invoiceResourcePath;
	
	/**
	 * @var string
	 */
	protected $templateName;
	
	/**
	 * @var \DateTime
	 */
	protected $dateNow;
	
	
	
	/**
	 * createInvoice
	 * 
	 * @param \DateTime $billingDate
	 * @param \CampaignEntity $campaignEntity
	 * @param \EMS\Campaigns\Invoice\Dto\ProductDto $productDto
	 * @param \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto
	 * @param string $fileExtensions
	 * @param array $summarizedProductDtoDataArray
	 * @return void
	 */
	abstract public function createInvoice(\DateTime $billingDate, \CampaignEntity $campaignEntity, \EMS\Campaigns\Invoice\Dto\ProductDto $productDto, \EMS\Campaigns\Invoice\Dto\CountryDto $countryDto, $fileExtensions, array $summarizedProductDtoDataArray = array());
	
	
	/**
	* getBillingDirectoryByCampaignBillingType
	* 
	* @param \CampaignEntity $campaignEntity
	* @return string
	*/
	protected function getBillingDirectoryByCampaignBillingType(\CampaignEntity $campaignEntity) {
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				$result = 'CPX' . \DIRECTORY_SEPARATOR;
				break;

			default:
				$result = 'Others' . \DIRECTORY_SEPARATOR;
				break;
		}

		return $result;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function setInvoiceResultsPath($invoiceResultsPath) {
		$this->invoiceResultsPath = $invoiceResultsPath;
	}

   	public function setInvoiceResourcePath($invoiceResourcePath) {
		$this->invoiceResourcePath = $invoiceResourcePath;
	}
	
	public function setTemplateName($templateName) {
		$this->templateName = $templateName;
	}
	
	public function setDateNow(\DateTime $dateNow) {
		$this->dateNow = $dateNow;
	}

}