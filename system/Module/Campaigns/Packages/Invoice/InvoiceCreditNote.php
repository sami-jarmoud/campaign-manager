<?php
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'InvoiceAbstract.php');

class InvoiceCreditNote extends \EMS\Campaigns\Invoice\InvoiceAbstract {
	/**
	 * main
	 * 
	 * @param array $invoicePostDataArray
	 * @param string $fileExtensions
	 * @return array
	 * @throws \InvalidArgumentException
	 */
	public function main(array $invoicePostDataArray, $fileExtensions = '') {
		$this->init($invoicePostDataArray);
		
		if (\count($this->processedInvoiceCampaignsIdsDataArray) > 0) {
			$jsonData = $this->processCustomerCreditNote();
		} else {
			throw new \InvalidArgumentException('no invoiceCampaignsCreditNote!', 1434091885);
		}
		
		return $jsonData;
	}
	
	/**
	 * init
	 * 
	 * @param array $invoicePostDataArray
	 * @return void
	 */
	protected function init(array $invoicePostDataArray) {
		$this->processedInvoiceCampaignsIdsDataArray = isset($invoicePostDataArray['campaignIds']) ? $invoicePostDataArray['campaignIds'] : 0;
		$this->debugLogManager->logData('processedInvoiceCampaignsIdsDataArray', $this->processedInvoiceCampaignsIdsDataArray);
	}


	/**
	 * processCustomerCreditNote
	 * 
	 * @return array
	 */
	protected function processCustomerCreditNote() {
		$this->debugLogManager->beginGroup(__FUNCTION__);
		
		$resultDataArray = array(
			'success' => false,
			'message' => ''
		);
		
		/**
		 * updateAndLogData
		 */
		$resultUpdateAndLogData = $this->updateAndLogData(\CampaignAndCustomerUtils::$campaignsOrderStatusDataArray[40]['status']);
		
		/**
		 * auf fehler überprüfen
		 */
		if (!\in_array(false, $resultUpdateAndLogData, true)) {
			$resultDataArray['success'] = true;
		} else {
			$resultDataArray['success'] = false;
			$resultDataArray['message'] = 'Technischer fehler!';
		}
		
		$this->debugLogManager->endGroup();
		
		return $resultDataArray;
	}
}
