<?php
function fillVorlage($text,$d_id) {

	$mandant = $_SESSION['mandant'];
	$mID = $_SESSION['mID'];
	
	include('../limport/db_connect.inc.php');
	
	$sql = mysql_query("SELECT * 
						FROM $import_datei_db as i
						INNER JOIN $partner_db as p ON i.PARTNER = p.PID
						WHERE i.IMPORT_NR = '$d_id' ",$verbindung);
	$z = mysql_fetch_array($sql);
	

	$sql_all = mysql_query("SELECT 	SUM(BRUTTO) as b,
									SUM(NETTO) as n,
									SUM(DUBLETTEN) as d,
									SUM(BLACKLIST) AS bl,
									SUM(BOUNCES) as bo,
									SUM(METADB) AS m,
                                    SUM(FAKE) AS f,
									SUM(DUBLETTE_INDATEI) as di
							FROM $import_datei_db
							WHERE IMPORT_NR2 = '$d_id'",$verbindung);
	$za = mysql_fetch_array($sql_all);
	
	if ($ap_anrede == 'Herr') {
		$b_standard = "Sehr geehrter Herr";
	}
	
	if ($ap_anrede == 'Frau') {
		$b_standard = "Sehr geehrte Frau";
	} 
	
	if ($ap_anrede == '') {
		$b_standard = "Sehr geehrte Damen und Herren";
	} 
	
	
	$l_lieferant_kurz = $z['Pkz'];
	$l_lieferant = $z['Firma'];
	$l_name = $z['IMPORT_DATEI'];
	$l_lieferdatum = $z['GELIEFERT_DATUM'];
	$l_lieferdatum  = util::datum_de($l_lieferdatum,'date');
	
    $l_fake = $za['f'];
	$l_brutto = $za['b'];
	$l_netto = $za['n'];
	$l_netto_ext = $z['NETTO_EXT'];
	
	$l_quote 			= util::getQuote($l_brutto,$l_netto);
	$l_quote_ext		= util::getQuote($l_brutto,$l_netto_ext);
	
	$x_bc_bl = $za['bo']+$za['bl']+$l_fake;
	$x_im_verteiler = $l_brutto-($x_bc_bl+$l_netto_ext);
	
	$text = str_replace("{b_standard}",$b_standard,$text);
	$text = str_replace("{l_lieferant_kurz}",$l_lieferant_kurz,$text);
	$text = str_replace("{l_lieferant}",$l_lieferant,$text);
	
	
	$text = str_replace("{l_name}",$l_name,$text);
	$text = str_replace("{l_lieferdatum}",$l_lieferdatum,$text);
	$text = str_replace("{l_brutto}",util::zahl_format($l_brutto),$text);
	$text = str_replace("{l_netto}",util::zahl_format($l_netto),$text);
	$text = str_replace("{l_netto_ext}",util::zahl_format($l_netto_ext),$text);
	$text = str_replace("{l_quote}",$l_quote,$text);
	$text = str_replace("{l_quote_ext}",$l_quote_ext,$text);
	
	$text = str_replace("{l_dubletten}",util::zahl_format($za['d']),$text);
	$text = str_replace("{l_bereits_vorhanden}",util::zahl_format($za['m']),$text);
	$text = str_replace("{l_bounces}",util::zahl_format($za['bo']),$text);
	$text = str_replace("{l_dubletten_in_datei}",util::zahl_format($za['di']),$text);
	$text = str_replace("{l_blacklist}",util::zahl_format($za['bl']),$text);
        $text = str_replace("{l_fake}",$l_fake,$text);
	
	$text = str_replace("{x_bc_bl}",util::zahl_format($x_bc_bl),$text);
	$text = str_replace("{x_im_verteiler}",util::zahl_format($x_im_verteiler),$text);
	
	
	$text = str_replace("{u_email}",$_SESSION['u_email'],$text);
	$text = str_replace("{u_anrede}",$_SESSION['u_anrede'],$text);
	$text = str_replace("{u_vorname}",$_SESSION['u_vorname'],$text);
	$text = str_replace("{u_nachname}",$_SESSION['u_nachname'],$text);
	
	return $text;
}


function fillVorlageRealtime($text,$d_id) {

	$mandant = $_SESSION['mandant'];
	$mID = $_SESSION['mID'];
	
	include('../limport/db_optdb_connect.inc.php');
	
	$sql = mysql_query("SELECT * FROM Report WHERE id = '$d_id'",$verbindung_optdb);
	$z = mysql_fetch_array($sql);
	
	
	if ($ap_anrede == 'Herr') {
		$b_standard = "Sehr geehrter Herr";
	}
	
	if ($ap_anrede == 'Frau') {
		$b_standard = "Sehr geehrte Frau";
	} 
	
	if ($ap_anrede == '') {
		$b_standard = "Sehr geehrte Damen und Herren";
	} 
	
	$l_name = "Realtime";
	$l_lieferdatum = strtotime($z['zeitraum']);
	$l_lieferdatum = date("Y-m-d", $l_lieferdatum);
	$l_lieferdatum  = util::getMonat($l_lieferdatum,'date');
	
	$l_brutto = $z['brutto'];
	$l_netto = $z['netto_ext'];
	
	$l_quote 			= util::getQuote($l_brutto,$l_netto);
	
	$text = str_replace("{b_standard}",$b_standard,$text);
	
	$text = str_replace("{l_name}",$l_name,$text);
	$text = str_replace("{l_lieferdatum}",$l_lieferdatum,$text);
	$text = str_replace("{l_brutto}",util::zahl_format($l_brutto),$text);
	$text = str_replace("{l_netto_ext}",util::zahl_format($l_netto),$text);
	$text = str_replace("{l_quote_ext}",$l_quote,$text);
	
	$text = str_replace("{u_email}",$_SESSION['u_email'],$text);
	$text = str_replace("{u_anrede}",$_SESSION['u_anrede'],$text);
	$text = str_replace("{u_vorname}",$_SESSION['u_vorname'],$text);
	$text = str_replace("{u_nachname}",$_SESSION['u_nachname'],$text);
	
	return $text;
}
?>