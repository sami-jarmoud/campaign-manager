<?php
class DeliverySystemFactory {
	public static $deliverySystemWebservice = array(
		'BM' => 'BroadmailWebservice',
		'BC' => 'BackclickWebservice',
		'E' => 'ElaineWebservice',
		'K' => 'KajomiWebservice',
		'MS' => 'MailSolutionWebservice',
                'SE' => 'SendeffectWebservice',
                'M' => 'MaileonWebservice',
                '4W18' =>'MaileonWebservice',
                '4WMS'=>'MaileonWebservice',
                'S18'=>'MaileonWebservice',
                'SB'=>'MaileonWebservice',
                'SS'=>'MaileonWebservice',
                '4WB'=>'MaileonWebservice',
                '4WBL'=>'MaileonWebservice',
                'SBL'=>'MaileonWebservice',
                'SRES'=>'MaileonWebservice',
                '4WE'=>'MaileonWebservice',
                'SSE' => 'SendeffectWebservice',
                '4WSE' => 'SendeffectWebservice'
	);
	
	private static $instances;
	
	
	
	
	/**
	 * loadClassInstance
	 * 
	 * @param string $deliverySystem
	 * @return void
	 * 
	 * @throws \DomainException
	 */
	public static function loadClassInstance($deliverySystem) {
		if (!isset(self::$instances[$deliverySystem])) {
			if ((\array_key_exists($deliverySystem, self::$deliverySystemWebservice))) {
				require_once(DIR_deliverySystemWebservice . self::$deliverySystemWebservice[$deliverySystem] . '.php');
			} else {
				throw new \DomainException('Unknown deliverySystemWebservice: ' . $deliverySystem);
			}
		}
	}
	
	/**
	 * getInstance
	 * 
	 * @param string $deliverySystem
	 * @return \DeliverySystemWebserviceAbstract
	 * 
	 * @throws \DomainException
	 */
	public static function getInstance($deliverySystem) {
		self::loadClassInstance($deliverySystem);
		
		self::$instances[$deliverySystem] = new self::$deliverySystemWebservice[$deliverySystem];

		return self::$instances[$deliverySystem];
	}
	
	/**
	 * getAndInitInstance
	 * 
	 * @param string $deliverySystem
	 * @param \ClientDeliveryEntity $clientDeliveryEntity
	 * @return \DeliverySystemWebserviceAbstract
	 * 
	 * @deprecated use getAndInitDeliverySystem
	 */
	public static function getAndInitInstance($deliverySystem, \ClientDeliveryEntity $clientDeliveryEntity) {
		$deliverySystemWebservice = self::getInstance($deliverySystem);
		/* @var $deliverySystemWebservice \DeliverySystemWebserviceAbstract */
		
		$deliverySystemWebservice->setMLogin($clientDeliveryEntity->getM_login());
		$deliverySystemWebservice->setApiUser($clientDeliveryEntity->getApi_user());
		$deliverySystemWebservice->setPw($clientDeliveryEntity->getPw());
		$deliverySystemWebservice->setSharedKey($clientDeliveryEntity->getShared_key());
		$deliverySystemWebservice->setSecretKey($clientDeliveryEntity->getSecret_key());
		
		return $deliverySystemWebservice;
	}
	
	/**
	 * getAndInitDeliverySystem
	 * 
	 * @param \ClientDeliverySystemWidthDeliverySystemEntity $clientDeliverySystemWidthDeliverySystemEntity
	 * @return \DeliverySystemWebserviceAbstract
	 */
	public static function getAndInitDeliverySystem(\ClientDeliverySystemWidthDeliverySystemEntity $clientDeliverySystemWidthDeliverySystemEntity) {
		$deliverySystemWebservice = self::getInstance($clientDeliverySystemWidthDeliverySystemEntity->getDeliverySystem()->getAsp_abkz());
		/* @var $deliverySystemWebservice \DeliverySystemWebserviceAbstract */
		
		$deliverySystemWebservice->setMLogin($clientDeliverySystemWidthDeliverySystemEntity->getM_login());
		$deliverySystemWebservice->setApiUser($clientDeliverySystemWidthDeliverySystemEntity->getApi_user());
		$deliverySystemWebservice->setPw($clientDeliverySystemWidthDeliverySystemEntity->getPw());
		$deliverySystemWebservice->setSharedKey($clientDeliverySystemWidthDeliverySystemEntity->getShared_key());
		$deliverySystemWebservice->setSecretKey($clientDeliverySystemWidthDeliverySystemEntity->getSecret_key());
		
		return $deliverySystemWebservice;
	}

}