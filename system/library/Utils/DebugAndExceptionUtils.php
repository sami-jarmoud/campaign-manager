<?php
// MailUtils
require_once(DIR_Utils . 'MailUtils.php');
require_once(DIR_Utils . 'BaseUtils.php');

final class DebugAndExceptionUtils {
	/**
	 * showDebugData
	 * 
	 * @param array|object $dataArray
	 * @param string $title
	 * @return string
	 */
	public static function showDebugData($dataArray, $title = '') {
		if ((int) $_SESSION['benutzer_id'] === \BaseUtils::getDebugUserId()) {
			if (\is_array($dataArray) 
				|| \is_object($dataArray)
			) {
				echo ($title) ? $title : __FUNCTION__;
				echo '<pre>';
				\print_r($dataArray);
				echo '</pre>' . \nl2br(\chr(13));
			} else {
				echo \nl2br(
					(($title) ? $title : __FUNCTION__) . ': '
						. \DataFilterUtils::filterData($dataArray) . \chr(13)
				);
			}
		}
	}

	/**
	 * sendDebugData
	 * 
	 * @param array|object $dataArray
	 * @param string $title
	 * @return void
	 */
	public static function sendDebugData($dataArray, $title = '') {
		if (\strlen($title) > 0) {
			\MailUtils::$EMAIL_BETREFF = $title;
		} else {
			\MailUtils::$EMAIL_BETREFF = __CLASS__ . ' -> ' . __FUNCTION__;
		}

		/**
		 * MailUtils::sendMail
		 */
		\MailUtils::sendMail(self::createMessageData($dataArray));
	}

	/**
	 * createMessageData
	 * 
	 * @param array|object $data
	 * @param string|integer $key
	 * @return string
	 */
	private static function createMessageData($data, $key = '') {
		$retArr = array();
		foreach ((array) $data as $k => $v) {
			if ((!empty($key)) || ($key === 0)) {
				$k = $key . ' - ' . $k;
			}

			if (\is_array($v) || \is_object($v)) {
				\array_push($retArr, self::createMessageData($v, $k));
			} else {
				\array_push($retArr, $k . ': ' . $v);
			}
		}

		return \implode(chr(11), $retArr);
	}

}