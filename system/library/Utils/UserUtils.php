<?php
final class UserUtils {
	
	protected static $packagesDataArray = array(
		2 => 'campaignManager',
		4 => 'dataManager',
		6 => 'blacklistUnsubscribe',
		7 => 'dais',
		999 => 'admin'
	);

	
	
	/**
	 * checkUserPackagesPermission
	 * 
	 * @param array $userPackages
	 * @param integer $packageId
	 * @return boolean
	 */
	public static function checkUserPackagesPermission(array $userPackages, $packageId) {
		$result = false;

		if (\in_array((int) $packageId, $userPackages)) {
			$result = true;
		}

		return $result;
	}

	/**
	 * getJsUserPackageVarDataArray
	 * 
	 * @param array $userPackages
	 * @return array
	 */
	public static function getJsUserPackageVarDataArray(array $userPackages) {
		$resultDataArray = array();

		// TODO: sp�ter �ber db an die daten kommen
		foreach (self::$packagesDataArray as $packageId => $value) {
			$found = 0;
			if (self::checkUserPackagesPermission($userPackages, $packageId)) {
				$found = 1;
			}
			
			$resultDataArray[$value] = 'var ' . $value . 'View = ' . $found . ';';
		}
		
		return $resultDataArray;
	}

}
