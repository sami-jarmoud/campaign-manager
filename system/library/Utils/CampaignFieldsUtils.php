<?php

 class CampaignFieldsUtils {
	const alignLeft = 'text-align: left;';
	const alignCenter = 'text-align: center;';
	const alignRight = 'text-align: right;';
	
	static public $textLenght;
	static public $bounceSThreshold;
	static public $bounceHThreshold;
	static public $klicksThreshold = 4.5;
	static public $premiumKampagnePreis = 99;
	static public $kampagnePreis = 59;
	static public $nachversand = 9.9;
	static public $werbemittel = 499;
	static public $leadPreis = 0.15;
	static public $versandTkp = 0.075;


                           
	/**
	 * cleanPriceDataForCsv
	 * 
	 * @param string $data
	 * @return string
	 */
	protected static function cleanPriceDataForCsv($data) {
		return \str_replace(\trim(\FormatUtils::euroSignHtmlCode), 'EURO', \strip_tags($data, '<br>'));
	}
	
	/**
	 * getCellStyleType
	 * 
	 * @param integer $quoteValue
	 * @param integer $defaultValue
	 * @param string $mailTyp
	 * @return string
	 */
	protected static function getCellStyleType($quoteValue, $defaultValue, $mailTyp) {
		$result = self::alignRight;
		
		if (\strlen($mailTyp) > 0) {
			if ($quoteValue < $defaultValue 
				&& $quoteValue > 0 
				&& $mailTyp != 't' 
				&& $defaultValue > 0
			) {
				$result .= 'color: red;';
			} elseif ($quoteValue >= $defaultValue 
				&& $mailTyp != 't' 
				&& $defaultValue > 0
			) {
				$result .= 'color: #7D960B;';
			}
		} else {
			if ($quoteValue < $defaultValue 
				&& $quoteValue > 0 
				&& $defaultValue > 0
			) {
				$result .= 'color: red;';
			} elseif ($quoteValue >= $defaultValue 
				&& $defaultValue > 0
			) {
				$result .= 'color: #7D960B;';
			}
		}

		return $result;
	}
	
	/**
	 * getBounceStyle
	 * 
	 * @param mixed $bounceThreshold
	 * @param float $bouncesQuote
	 * @return string
	 */
	protected static function getBounceStyle($bounceThreshold, $bouncesQuote) {
		$bounceStyle = self::alignRight;

		if (\is_numeric($bounceThreshold) === true 
			&& $bouncesQuote > 0
		) {
			if ($bouncesQuote > $bounceThreshold) {
				$bounceStyle .= 'color:red;';
			} else {
				$bounceStyle .= 'color:#7D960B;';
			}
		}

		return $bounceStyle;
	}
	
	/**
	 * getKlicksStyle
	 * 
	 * @param integer $klicks
	 * @param integer $klicksAll
	 * @return string
	 */
	protected static function getKlicksStyle($klicks, $klicksAll) {
		$result = self::alignRight;
		
		if ($klicks > 0) {
			if ($klicksAll >= (\round(($klicks * self::$klicksThreshold)))) {
				$result .= 'color:#9D33FF;';
			}
		}
		
		return $result;
	}

	/**
	 * getCellContentByUserTabFieldsDataArray
	 * 
	 * @param \CampaignManager $campaignManager
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @param \DateTime $today
	 * @param array $userTabFieldsDataArray
	 * @param boolean $campaignHasNv
	 * @param string $campaignType
	 * @param array $csvItemDataArray
	 * @return string
	 */
	public static function  getCellContentByUserTabFieldsDataArray(\CampaignManager $campaignManager, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity, \DateTime $today, array $userTabFieldsDataArray, $campaignHasNv, $campaignType = 'HV', array &$csvItemDataArray = array()) {
		$result = '';

		/**
		* FormatUtils::rateCalculation
		* 
		* openings, openingsAll, klicks, klicksAll
		*/
		$openingsQuote = \FormatUtils::rateCalculation(
			((boolean) $campaignHasNv !== true 
				? $campaignEntity->getOpenings() 
				: $hvAndNvCampaigEntity->getOpenings()
			),
			($campaignType == 'HV' 
				? $campaignEntity->getGebucht() 
				: $campaignEntity->getVersendet()
			)
		);

		$openingsAllQuote = \FormatUtils::rateCalculation(
			((boolean) $campaignHasNv !== true 
				? $campaignEntity->getOpenings_all() 
				: $hvAndNvCampaigEntity->getOpenings_all()
			),
			($campaignType == 'HV' 
				? $campaignEntity->getGebucht() 
				: $campaignEntity->getVersendet()
			)
		);

		$klicksQuote = \FormatUtils::rateCalculation(
			((boolean) $campaignHasNv !== true 
				? $campaignEntity->getKlicks() 
				: $hvAndNvCampaigEntity->getKlicks()
			),
			($campaignType == 'HV' 
				? $campaignEntity->getGebucht() 
				: $campaignEntity->getVersendet()
			)
		);

		$klicksAllQuote = \FormatUtils::rateCalculation(
			((boolean) $campaignHasNv !== true 
				? $campaignEntity->getKlicks_all() 
				: $hvAndNvCampaigEntity->getKlicks_all()
			),
			($campaignType == 'HV' 
				? $campaignEntity->getGebucht() 
				: $campaignEntity->getVersendet()
			)
		);
		foreach ($userTabFieldsDataArray as $field) {
			switch ($field) {
				case 'datum':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field
						),
						self::getDatumResult(
							$campaignType,
							$campaignHasNv,
							$campaignEntity
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getDatum()->format('d.m.Y H:i');
					break;

				case 'beendet':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						self::getBeendetResult(
							$campaignHasNv,
							$campaignEntity
						)
					);
					
					if ($campaignEntity->getBeendet() instanceof \DateTime) {
						$csvItemDataArray[$field] = $campaignEntity->getBeendet()->format('d.m.Y H:i');
					} else {
						$csvItemDataArray[$field] = '';
					}
					break;

				case 'asp':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field
						),
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getVersandsystem() 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getVersandsystem();
					break;

				case 'gebucht':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						((string) $campaignType === 'HV' 
							? self::getGebuchtResult($campaignEntity) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = \FormatUtils::numberFormat($campaignEntity->getGebucht());
					break;

				case 'versendet':
					if ((boolean) $campaignHasNv !== true) {
						$resultData = \FormatUtils::numberFormat($campaignEntity->getVersendet());
					} else {
						$resultData = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getVersendet());
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'openings':
					if ((boolean) $campaignHasNv !== true) {
						$resultData = \FormatUtils::numberFormat($campaignEntity->getOpenings());
					} else {
						$resultData = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getOpenings());
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'open_quote':
					if ($campaignEntity->getMailtyp() == 't') {
						$resultData = '-';
					} else {
						$resultData = $openingsQuote . ' %';
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => ($campaignType == 'HV' 
								? self::getCellStyleType(
									$openingsQuote,
									$campaignEntity->getVorgabe_o(),
									$campaignEntity->getMailtyp()
								) 
								: self::alignRight
							),
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'openings_a':
					if ((boolean) $campaignHasNv !== true) {
						$resultData = \FormatUtils::numberFormat($campaignEntity->getOpenings_all());
					} else {
						$resultData = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getOpenings_all());
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'open_quote_a':
					if ($campaignEntity->getMailtyp() == 't') {
						$resultData = '-';
					} else {
						$resultData = $openingsAllQuote . ' %';
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => ($campaignType == 'HV' 
								? self::getCellStyleType(
									$openingsAllQuote,
									$campaignEntity->getVorgabe_o(),
									$campaignEntity->getMailtyp()
								) 
								: self::alignRight
							),
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'open_ziel':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						self::getOpenZielResult(
							$campaignHasNv,
							$campaignEntity
						)
					);
					
					if ($campaignEntity->getMailtyp() !== 't') {
						$csvItemDataArray[$field] = $campaignEntity->getVorgabe_o() . ' %';
					} else {
						$csvItemDataArray[$field] = '';
					}
					break;

				case 'klicks':
					if ((boolean) $campaignHasNv !== true) {
						$resultData = \FormatUtils::numberFormat($campaignEntity->getKlicks());
						
						$klicksStyle = self::getKlicksStyle(
							$campaignEntity->getKlicks(),
							$campaignEntity->getKlicks_all()
						);
					} else {
						$resultData = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getKlicks());
						
						$klicksStyle = self::getKlicksStyle(
							$hvAndNvCampaigEntity->getKlicks(),
							$hvAndNvCampaigEntity->getKlicks_all()
						);
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => $klicksStyle,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'klicks_quote':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => ($campaignType == 'HV' 
								? self::getCellStyleType(
									$klicksQuote,
									$campaignEntity->getVorgabe_k(),
									''
								) 
								: self::alignRight
							),
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$klicksQuote . ' %'
					);
					
					$csvItemDataArray[$field] = $klicksQuote . ' %';
					break;

				case 'klicks_a':
					if ((boolean) $campaignHasNv !== true) {
						$resultData = \FormatUtils::numberFormat($campaignEntity->getKlicks_all());
						
						$klicksAStyle = self::getKlicksStyle(
							$campaignEntity->getKlicks(),
							$campaignEntity->getKlicks_all()
						);
					} else {
						$resultData = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getKlicks_all());
						
						$klicksAStyle = self::getKlicksStyle(
							$hvAndNvCampaigEntity->getKlicks(),
							$hvAndNvCampaigEntity->getKlicks_all()
						);
					}
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => $klicksAStyle,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'klicks_quote_a':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => ($campaignType == 'HV' 
								? self::getCellStyleType(
									$klicksAllQuote,
									$campaignEntity->getVorgabe_k(),
									''
								) 
								: self::alignRight
							),
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						$klicksAllQuote . ' %'
					);
					
					$csvItemDataArray[$field] = $klicksAllQuote . ' %';
					break;

				case 'klicks_ziel':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getVorgabe_k() . ' %' 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getVorgabe_k() . ' %';
					break;

				case 'menge_ziel':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						\FormatUtils::numberFormat($campaignEntity->getVorgabe_m())
					);
					
					$csvItemDataArray[$field] = \FormatUtils::numberFormat($campaignEntity->getVorgabe_m());
					break;

				case 'zielgruppe':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'title' => ((boolean) $campaignHasNv !== true 
								? \nl2br(\htmlspecialchars($campaignEntity->getZielgruppe()))
								: ''
							)
						),
						self::getCropDataResult(
							$campaignHasNv,
							$campaignEntity->getZielgruppe()
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getZielgruppe();
					break;

				case 'betreff':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => 'font-size:9px;',
							'title' => CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						self::getCropDataResult(
							$campaignHasNv,
							$campaignEntity->getBetreff()
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getBetreff();
					break;

				case 'absender':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => 'font-size:9px;',
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						self::getCropDataResult(
							$campaignHasNv,
							$campaignEntity->getAbsendername()
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getAbsendername();
					break;

				case 'ap':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getContactPersonEntity()->getVorname()
								. ' ' . $campaignEntity->getContactPersonEntity()->getNachname() 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getContactPersonEntity()->getVorname()
						. ' ' . $campaignEntity->getContactPersonEntity()->getNachname()
					;
					break;

				case 'mailformat':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						), 
						((boolean) $campaignHasNv !== true 
							? \CampaignAndCustomerUtils::$mailtypDataArray[$campaignEntity->getMailtyp()] 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = \CampaignAndCustomerUtils::$mailtypDataArray[$campaignEntity->getMailtyp()];
					break;

				case 'bearbeiter':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getBearbeiter() 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getBearbeiter();
					break;

				case 'bl':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['label']
						),
						((boolean) $campaignHasNv !== true 
							? self::getBlacklistResult($campaignEntity) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getBlacklist();
				break;

				case 'notiz':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'title' => ((boolean) $campaignHasNv !== true 
								? \nl2br(\htmlspecialchars($campaignEntity->getNotiz())) 
								: ''
							)
						),
						self::getCropDataResult(
							$campaignHasNv,
							\nl2br($campaignEntity->getNotiz())
						)
					);
					
					$csvItemDataArray[$field] = \FormatUtils::nl2Br($campaignEntity->getNotiz());
					break;

				case 'k_id':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						), 
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getK_id() 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getK_id();
					break;

				case 'abrechnungsart':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						self::getAbrechnungsartResult(
							$campaignType,
							$campaignHasNv,
							$campaignEntity->getAbrechnungsart()
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getAbrechnungsart();
					break;

				case 'leads':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						((boolean) $campaignHasNv !== true 
							? \FormatUtils::numberFormat($campaignEntity->getLeads()) 
							: \FormatUtils::numberFormat($hvAndNvCampaigEntity->getLeads())
						)
					);
					
					if ((boolean) $campaignHasNv !== true) {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($campaignEntity->getLeads());
					} else {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getLeads());
					}
					break;

				case 'leads_u':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						((boolean) $campaignHasNv !== true 
							? \FormatUtils::numberFormat($campaignEntity->getLeads_u()) 
							: \FormatUtils::numberFormat($hvAndNvCampaigEntity->getLeads_u())
						)
					);
					
					if ((boolean) $campaignHasNv !== true) {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($campaignEntity->getLeads_u());
					} else {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getLeads_u());
					}
					break;

				case 'report':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						self::getReportResult(
							$campaignHasNv,
							$campaignEntity
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getReport();
					break;

				case 'erstreporting':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						((boolean) $campaignHasNv !== true 
							? self::getReportingResultByStatus(
								$campaignEntity,
								$today,
								21,
								$field,
								'Erstreporting abgeschlossen'
							) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = '';
					if ($campaignEntity->getErstreporting() instanceof \DateTime) {
						if ($campaignEntity->getErstreporting()->format('dmY') === $today->format('dmY')) {
							$csvItemDataArray[$field] = 'Heute';
						} else {
							$csvItemDataArray[$field] = $campaignEntity->getErstreporting()->format('d.m.Y');
						}
					}
					break;

				case 'endreporting':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						((boolean) $campaignHasNv !== true 
							? self::getReportingResultByStatus(
								$campaignEntity,
								$today,
								22,
								$field,
								'Endreporting abgeschlossen'
							) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = '';
					if ($campaignEntity->getEndreporting() instanceof \DateTime) {
						if ($campaignEntity->getEndreporting()->format('Y-m-d') === $today->format('Y-m-d')) {
							$csvItemDataArray[$field] = 'Heute';
						} else {
							$csvItemDataArray[$field] = $campaignEntity->getEndreporting()->format('d.m.Y');
						}
					}
					break;

				case 'rechnungstellung':
					$resultData = self::getContentResultByActionLog(
						$campaignHasNv,
						$campaignManager,
						$campaignEntity->getK_id(),
						6,
						'Rechnungstellung versendet'
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						$resultData
					);
					
					if ((\stripos('<img', $resultData)) !== false) {
						$csvItemDataArray[$field] = 'Rechnungstellung versendet';
					} else {
						$csvItemDataArray[$field] = $resultData;
					}
					break;

				case 'zustellreport':
					$resultData = self::getContentResultByActionLog(
						$campaignHasNv,
						$campaignManager,
						$campaignEntity->getK_id(),
						7,
						'Zustellreport versendet'
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						$resultData
					);
					
					if ((\stripos('<img', $resultData)) !== false) {
						$csvItemDataArray[$field] = 'Zustellreport versendet';
					} else {
						$csvItemDataArray[$field] = $resultData;
					}
					break;

				case 'infomail':
					$resultData = self::getContentResultByActionLog(
						$campaignHasNv,
						$campaignManager,
						$campaignEntity->getK_id(),
						8,
						'Infomail versendet'
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						$resultData
					);
					
					if ((\stripos('<img', $resultData)) !== false) {
						$csvItemDataArray[$field] = 'Infomail versendet';
					} else {
						$csvItemDataArray[$field] = $resultData;
					}
				break;

				case 'abmelder':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						self::getAbmelderResult(
							$campaignHasNv,
							$campaignType,
							$campaignEntity,
							$hvAndNvCampaigEntity
						)
					);
					
					if ((boolean) $campaignHasNv !== true) {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($campaignEntity->getAbmelder());
					} else {
						$csvItemDataArray[$field] = \FormatUtils::numberFormat($hvAndNvCampaigEntity->getAbmelder());
					}
					break;

				case 'hbounces':
					$hbounces = $versendet = 0;
					self::getBouncesResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity,
						$field,
						$hbounces,
						$versendet
					);
					
					$hBouncesQuote = \FormatUtils::rateCalculation(
						$hbounces,
						$versendet
					);

					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::getBounceStyle(
								self::$bounceHThreshold,
								$hBouncesQuote
							)
						),
						\FormatUtils::numberFormat($hbounces)
							. ' (' . $hBouncesQuote . ' %)'
					);
					
					$csvItemDataArray[$field] = \FormatUtils::numberFormat($hbounces);
					break;

				case 'sbounces':
					$sbounces = $versendet = 0;
					self::getBouncesResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity,
						$field,
						$sbounces,
						$versendet
					);
					
					$sBouncesQuote = \FormatUtils::rateCalculation(
						$sbounces,
						$versendet
					);

					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::getBounceStyle(
								self::$bounceSThreshold,
								$sBouncesQuote
							)
						),
						\FormatUtils::numberFormat($sbounces)
							. ' (' . $sBouncesQuote . ' %)'
					);
					
					$csvItemDataArray[$field] = \FormatUtils::numberFormat($sbounces);
					break;

				case 'hsbounces':
					$hsbounces = $versendet = 0;
					self::getTotalBouncesResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity,
						$hsbounces,
						$versendet
					);
					
					$hsBouncesQuote = \FormatUtils::rateCalculation(
						$hsbounces,
						$versendet
					);

					$bounceHSThreshold = null;
					if (\is_numeric(self::$bounceHThreshold) 
						&& \is_numeric(self::$bounceSThreshold)
					) {
						$bounceHSThreshold = (self::$bounceHThreshold + self::$bounceSThreshold) / 2;
					}

					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::getBounceStyle(
								$bounceHSThreshold,
								$hsBouncesQuote
							)
						),
						\FormatUtils::numberFormat($hsbounces)
							. ' (' . $hsBouncesQuote . ' %)'
					);
					
					$csvItemDataArray[$field] = \FormatUtils::numberFormat($hsbounces);
					break;

				case 'preis':
					$dataResult = self::getPriceResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						$dataResult
					);
					
					$csvItemDataArray[$field] = self::cleanPriceDataForCsv($dataResult);
					break;

				case 'unit_price':
					$dataResult = self::getUnitPriceResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						$dataResult
					);
					
					$csvItemDataArray[$field] = self::cleanPriceDataForCsv($dataResult);
					break;

				case 'preis_gesamt':
					$dataResult = self::getTotalPriceResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						$dataResult
					);
					
					$csvItemDataArray[$field] = self::cleanPriceDataForCsv($dataResult);
					break;
                                case 'preis_broking':
					$preisGesamt = self::getIntegerTotalPriceResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity
					);
                                        $broking_volume = \FormatUtils::numberFormat($campaignEntity->getBroking_volume());
                                        switch ($campaignEntity->getAbrechnungsart()){
                                        case 'CPL':
                                        case 'CPO':
                                        case 'CPC':
                                             $broking_price = ($campaignEntity->getLeads() * $campaignEntity->getBroking_price());
                                            break;
                                        case 'TKP':
                                              $broking_price = ($broking_volume * $campaignEntity->getBroking_price());
                                             break;
                                         case 'Hybri':
                                         case 'Hybrid':
                                              #$broking_tkp = ($broking_volume * $campaignEntity->getHybrid_preis());
                                              #$broking_cpl = ($campaignEntity->getLeads() * $campaignEntity->getHybrid_preis2());
                                              #$broking_price = $broking_tkp + $broking_cpl;
                                             break;
                                         case 'Festp':
                                         case 'Festpreis':
                                              $broking_price = $campaignEntity->getBroking_price();
                                             break;
                                        }

					$dataResult = \FormatUtils::totalPriceFormat($preisGesamt) - \FormatUtils::totalPriceFormat(\FormatUtils::sumFormat($broking_price));
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						\FormatUtils::sumFormat($dataResult). \FormatUtils::euroSignHtmlCode 
					);
					
					$csvItemDataArray[$field] = self::cleanPriceDataForCsv($dataResult);
					break;
				case 'avg_tkp':
					$dataResult = self::getAvgResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight,
							'title' => \CampaignAndCustomerUtils::$allTabFieldsDataArray[$field]['sublabel']
						),
						$dataResult
					);
					
					$csvItemDataArray[$field] = self::cleanPriceDataForCsv($dataResult);
					break;

				case 'rechnung':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						((boolean) $campaignHasNv !== true 
							? self::getRechnungResult($campaignEntity->getStatus()) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = '';
					if ($campaignEntity->getStatus() === 31) {
						$csvItemDataArray[$field] = 'best�tigt';
					} elseif ($campaignEntity->getStatus() === 32) {
						$csvItemDataArray[$field] = 'gestellt';
					}
					break;

				case 'dsd_id':
					$resultData = self::getDsdIdResult(
						$campaignHasNv,
						$campaignEntity->getDsd_id()
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				case 'mail_id':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						self::getMailIdResult(
							$campaignHasNv,
							$campaignEntity->getMail_id()
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getMail_id();
					break;
				
				case 'use_campaign_start_date':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignCenter
						),
						self::getUseCampaignStartDateResult(
							$campaignHasNv,
							$campaignEntity
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getUse_campaign_start_date();
					break;
				
				case 'campaign_click_profile_id':
					$resultData = self::getCampaignClickProfileResult(
						$campaignHasNv,
						$campaignEntity->getCampaign_click_profiles_id()
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;
				
				case 'selection_click_profile_id':
					$resultData = self::getCampaignClickProfileResult(
						$campaignHasNv,
						$campaignEntity->getSelection_click_profiles_id()
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;
				
				case 'broking_volume':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						), 
						((boolean) $campaignHasNv !== true 
							? \FormatUtils::numberFormat($campaignEntity->getBroking_volume()) 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getBroking_volume();
					break;
				
				case 'broking_price':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						), 
						((boolean) $campaignHasNv !== true 
							? \FormatUtils::sumFormat($campaignEntity->getBroking_price()) . \FormatUtils::euroSignHtmlCode 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getBroking_volume();
					break;
				
				case 'kosten':
				$resultData = self::kosten($campaignEntity, $hvAndNvCampaigEntity, $campaignType);
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
                       $resultData
					);
			
					break;
                                    
                                  case 'ctr':
           
                                      if ((boolean) $campaignHasNv !== true) {
						$resultOpenings = $campaignEntity->getOpenings();
					} else {
						$resultOpenings = $hvAndNvCampaigEntity->getOpenings();
					}
                                        
                                        if ((boolean) $campaignHasNv !== true) {
						$resultKlicks = $campaignEntity->getKlicks();
												
					} else {
						$resultKlicks = $hvAndNvCampaigEntity->getKlicks();
						
					}
                                        
                                        $ctr = ($resultKlicks / $resultOpenings);
                                        $resultData = \FormatUtils::sumFormat($ctr);
                                                
                                          $result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						$resultData
					);  
                                      break;
                                      
				case 'deckungsbeitrag':
					$preisGesamt = self::getIntegerTotalPriceResult(
						$campaignType,
						$campaignHasNv,
						$campaignEntity,
						$hvAndNvCampaigEntity
					);
					$kosten = self::kosten($campaignEntity, $hvAndNvCampaigEntity, $campaignType);
					$kosten = \FormatUtils::totalPriceFormat($kosten);
					$preisGesamt = \FormatUtils::totalPriceFormat($preisGesamt);
					$resultData = ($preisGesamt - $kosten);
					if((string)$campaignType === 'HV'){
						$resultData = \FormatUtils::sumFormat($preisGesamt - $kosten).\FormatUtils::euroSignInUtf8Unicode ;
					}else{
						$resultData ='';
					}
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						$resultData	
					);
					break;
				
				// TODO: remove!!!
				case 'crm_order_id':
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
							'style' => self::alignRight
						),
						((boolean) $campaignHasNv !== true 
							? $campaignEntity->getCrm_order_id() 
							: ''
						)
					);
					
					$csvItemDataArray[$field] = $campaignEntity->getCrm_order_id();
					break;
				
				case 'cs_id':
					$resultData = self::getCsIdResult(
						$campaignHasNv,
						$campaignEntity->getCs_id()
					);
					
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						$resultData
					);
					
					$csvItemDataArray[$field] = $resultData;
					break;

				default:
					$result .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'class' => $field,
						),
						''
					);
					
					$csvItemDataArray[$field] = '';
					break;
			}
		}

		return $result;
	}
	
	
	/**
	 * getDatumResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getDatumResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ($campaignType == 'HV' 
			&& (boolean) $campaignHasNv !== true
		) {
			$result = $campaignEntity->getDatum()->format('H:i');
		} elseif ($campaignType == 'NV') {
			$result = $campaignEntity->getDatum()->format('d.m.Y H:i');
		}
		
		return $result;
	}
	
	/**
	 * getBeendetResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getBeendetResult($campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& $campaignEntity->getBeendet() instanceof \DateTime
		) {
			$result = $campaignEntity->getBeendet()->format('d.m.Y H:i');
		}
		
		return $result;
	}
	
	/**
	 * getGebuchtResult
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @return string|float
	 */
	public static function getGebuchtResult(\CampaignEntity $campaignEntity) {
		if ($campaignEntity->getGebucht() === 0 
			&& $campaignEntity->getAbrechnungsart() == 'TKP' 
			&& (
				$campaignEntity->getStatus() === 5 
				|| $campaignEntity->getStatus() >= 20
			)
		) {
			$result = '<img src="img/Oxygen/16/status/dialog-warning.png" title="Gebuchte Menge fehlt!" />';
		} else {
			$result = \FormatUtils::numberFormat($campaignEntity->getGebucht());
		}
		
		return $result;
	}
	
	/**
	 * getOpenZielResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getOpenZielResult($campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& (string) $campaignEntity->getMailtyp() !== 't'
		) {
			$result = $campaignEntity->getVorgabe_o() . ' %';
		}
		
		return $result;
	}
	
	/**
	 * getZielgruppeResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param string $string
	 * @return string
	 */
	public static function getCropDataResult($campaignHasNv, $string) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true) {
			$result = \FormatUtils::cutString(
				$string,
				self::$textLenght
			);
		}
		
		return $result;
	}
	
	/**
	 * getBlacklistResult
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getBlacklistResult(\CampaignEntity $campaignEntity) {
		$result = '';
		
		if (\strlen($campaignEntity->getBlacklist()) > 0) {
			if ($campaignEntity->getStatus() === 5) {
				$result = '<span style="font-weight:bold;color:silver;">X</span>';
			} else {
				$result = '<span style="font-weight:bold;color:red;">X</span>';
			}
		}
		
		return $result;
	}
	
	/**
	 * getAbrechnungsartResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param string $billingType
	 * @return string
	 */
	protected static function getAbrechnungsartResult($campaignType, $campaignHasNv, $billingType) {
		$result = '';
		
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				switch ($billingType) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						$result = $billingType;
						break;
				}
			} else {
				$result = $billingType;
			}
		} else {
			if (\strlen($billingType) > 0) {
				switch ($billingType) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = $billingType;
						break;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * getReportResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getReportResult($campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true) {
			if ($campaignEntity->getStatus() >= 20) {
				$result = '<img src="img/Tango/22/actions/dialog-apply.png" title="Report intern verschickt" />';
			} else {
				$result = $campaignEntity->getReport();
			}
		}
		
		return $result;
	}
	
	/**
	 * getReportingResultByStatus
	 * 
	 * @param \CampaignEntity $campaignEntity
	 * @param \DateTime $today
	 * @param integer $status
	 * @param string $reportingType
	 * @param string $title
	 * @return string
	 */
	protected static function getReportingResultByStatus(\CampaignEntity $campaignEntity, \DateTime $today, $status, $reportingType, $title) {
		$result = '';
		
		$reportingData = ($reportingType == 'endreporting' ? $campaignEntity->getEndreporting() : $campaignEntity->getErstreporting());
		if ($campaignEntity->getStatus() >= $status) {
			$result = '<img src="img/Tango/22/actions/dialog-apply.png" title="' . $title . '" />';
		} elseif ($reportingData instanceof \DateTime) {
			if ($reportingData->format('Y-m-d') === $today->format('Y-m-d')) {
				$result = '<span style="color:red;font-weight:bold">Heute</span>';
			} else {
				$result = $reportingData->format('d.m.Y');
			}
		}
		
		return $result;
	}
	
	/**
	 * getContentResultByActionLog
	 * 
	 * @param boolean $campaignHasNv
	 * @param \CampaignManager $campaignManager
	 * @param integer $campaignId
	 * @param integer $actionLogId
	 * @param string $title
	 * @return string
	 */
	protected static function getContentResultByActionLog($campaignHasNv, \CampaignManager $campaignManager, $campaignId, $actionLogId, $title) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true) {
			/**
			 * getLastLogByAction
			 */
			$invoicingActionLogEntity = $campaignManager->getLastLogByAction(
				$actionLogId,
				$campaignId
			);

			if ($invoicingActionLogEntity instanceof \ActionLogEntity) {
				$result = '<img src="img/Tango/22/actions/dialog-apply.png" title="' . $title . '" />';
			} else {
				$result = 'offen';
			}
		}
		
		return $result;
	}
	
	/**
	 * getAbmelderResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param string $campaignType
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @return string
	 */
	protected static function getAbmelderResult($campaignHasNv, $campaignType, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity) {
		$abmelder = $campaignEntity->getAbmelder();
		$number = $campaignEntity->getVersendet();
		
		if ((string) $campaignType === 'HV' 
			&& (boolean) $campaignHasNv === true
		) {
			// HV has NV
			$abmelder = $hvAndNvCampaigEntity->getAbmelder();
			$number = $hvAndNvCampaigEntity->getVersendet();
		}
		
		return \FormatUtils::numberFormat($abmelder)
			. ' (' . \FormatUtils::rateCalculation(
				$abmelder,
				$number
			) . ' %)'
		;
	}
	
	/**
	 * getBouncesResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @param string $bouncesType
	 * @param integer $bounces
	 * @param integer $send
	 * @return void
	 */
	protected static function getBouncesResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity, $bouncesType, &$bounces, &$send) {
		$bounces = ($bouncesType === 'hbounces' ? $campaignEntity->getHbounces() : $campaignEntity->getSbounces());
		$send = $campaignEntity->getVersendet();
		
		if ((string) $campaignType === 'HV' 
			&& (boolean) $campaignHasNv === true
		) {
			// HV has NV
			$bounces = ($bouncesType === 'hbounces' ? $hvAndNvCampaigEntity->getHbounces() : $hvAndNvCampaigEntity->getSbounces());
			$send = $hvAndNvCampaigEntity->getVersendet();
		}
	}
	
	/**
	 * getTotalBouncesResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @param integer $bounces
	 * @param integer $send
	 * @return void
	 */
	protected static function getTotalBouncesResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity, &$bounces, &$send) {
		$bounces = $campaignEntity->getHbounces() + $campaignEntity->getSbounces();
		$send = $campaignEntity->getVersendet();
		
		if ((string) $campaignType === 'HV' 
			&& (boolean) $campaignHasNv === true
		) {
			// HV has NV
			$bounces = $hvAndNvCampaigEntity->getHbounces() + $hvAndNvCampaigEntity->getSbounces();
			$send = $hvAndNvCampaigEntity->getVersendet();
		}
	}
	
	/**
	 * getPriceResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	public static function getPriceResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity) {
		$contentPrice = '';
		
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'Hybri':
			case 'Hybrid':
				$hybridLabel = 'CPX';
				
				$contentPrice = 'TKP: ' . \FormatUtils::sumFormat($campaignEntity->getPreis()) . \FormatUtils::euroSignHtmlCode . '<br />'
					. $hybridLabel . ': ' . \FormatUtils::sumFormat($campaignEntity->getPreis2()) . \FormatUtils::euroSignHtmlCode
				;
				break;
			
			case 'Festpreis':
			case 'Festp':
				$contentPrice = \FormatUtils::sumFormat($campaignEntity->getPreis_gesamt()) . \FormatUtils::euroSignHtmlCode;
				break;
				
			default:
				if (\strlen($campaignEntity->preis) > 0) {
					$contentPrice = \FormatUtils::sumFormat($campaignEntity->getPreis()) . \FormatUtils::euroSignHtmlCode;
				}
				break;
		}

		$result = '';
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						$result = $contentPrice;
						break;
				}
			} else {
				$result = $contentPrice;
			}
		} else {
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = $contentPrice;
						break;
				}
			}
		}
		unset($contentPrice);
		
		return $result;
	}
	
	/**
	 * getUnitPriceResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string|numeric
	 */
	protected static function getUnitPriceResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						$result = \FormatUtils::sumFormat($campaignEntity->getUnit_price()) . \FormatUtils::euroSignHtmlCode;
						break;
				}
			} else {
				$result = \FormatUtils::sumFormat($campaignEntity->getUnit_price()) . \FormatUtils::euroSignHtmlCode;
			}
		} else {
			// NV
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = \FormatUtils::sumFormat($campaignEntity->getUnit_price()) . \FormatUtils::euroSignHtmlCode;
						break;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * getTotalPriceResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @return string
	 */
	public static function getTotalPriceResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity) {
		$result = '';
		
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						$result = \FormatUtils::getFormatetTotalPriceByBillingType($campaignEntity);
						break;
					
					default:
						$result = \FormatUtils::getFormatetTotalPriceByBillingType(
							$campaignEntity,
							$hvAndNvCampaigEntity->getPreis()
						);
						break;
				}
			} else {
				$result = \FormatUtils::getFormatetTotalPriceByBillingType($campaignEntity);
			}
		} else {
			// NV
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = \FormatUtils::getFormatetTotalPriceByBillingType($campaignEntity);
						break;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * getIntegerTotalPriceResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @return string
	 */
	public static function getIntegerTotalPriceResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity) {
		$result = '';
		
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						$result = \FormatUtils::getIntegerFormatetTotalPriceByBillingType($campaignEntity);
						break;
					
					default:
						$result = \FormatUtils::getIntegerFormatetTotalPriceByBillingType(
							$campaignEntity,
							$hvAndNvCampaigEntity->getPreis()
						);
						break;
				}
			} else {
				$result = \FormatUtils::getIntegerFormatetTotalPriceByBillingType($campaignEntity);
			}
		} else {
			// NV
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = \FormatUtils::getIntegerFormatetTotalPriceByBillingType($campaignEntity);
						break;
				}
			}
		}
		
		return $result;
	}
	/**
	 * getAvgResult
	 * 
	 * @param string $campaignType
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @param \CampaignEntity $hvAndNvCampaigEntity
	 * @return string
	 */
	protected static function getAvgResult($campaignType, $campaignHasNv, \CampaignEntity $campaignEntity, \CampaignEntity $hvAndNvCampaigEntity) {
		$result = '';
		
		if ((string) $campaignType === 'HV') {
			if ((boolean) $campaignHasNv === true) {
				$result = \FormatUtils::getAndCalculateETkpPrice(
					$hvAndNvCampaigEntity,
					$hvAndNvCampaigEntity->getPreis()
				);
			} else {
				$result = \FormatUtils::getAndCalculateETkpPrice($campaignEntity);
			}
		} else {
			// NV
			if (\strlen($campaignEntity->getAbrechnungsart()) > 0) {
				switch ($campaignEntity->getAbrechnungsart()) {
					case 'TKP':
					case 'Festp':
					case 'Festpreis':
					case 'Hybri':
					case 'Hybrid':
						break;
					
					default:
						$result = \FormatUtils::getAndCalculateETkpPrice($campaignEntity);
						break;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * getRechnungResult
	 * 
	 * @param integer $status
	 * @return string
	 */
	protected static function getRechnungResult($status) {
		switch ((int) $status) {
			case 31:
				$result = 'Best&auml;tigt';
				break;
			
			case 32:
				$result = '<img src="img/Tango/22/actions/dialog-apply.png" />';
				break;
			
			default:
				$result = '';
				break;
		}
		
		return $result;
	}
	
	/**
	 * getDsdIdResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param integer $dsdId
	 * @return string
	 */
	public static function getDsdIdResult($campaignHasNv, $dsdId) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& (int) $dsdId > 0
		) {
			/* @var $_SESSION DeliverySystemDistributorEntity */
			if (isset($_SESSION['deliverySystemDistributorEntityDataArray'][$dsdId]) 
				&& $_SESSION['deliverySystemDistributorEntityDataArray'][$dsdId] instanceof \DeliverySystemDistributorEntity
			) {
				$result = $_SESSION['deliverySystemDistributorEntityDataArray'][$dsdId]->getTitle();
			}
		}
		
		return $result;
	}
	
	/**
	 * getMailIdResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param string $mailingId
	 * @return string
	 */
	protected static function getMailIdResult($campaignHasNv, $mailingId) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true) {
			$result = $mailingId;
		}
		
		return $result;
	}
	
	/**
	 * getUseCampaignStartDateResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param \CampaignEntity $campaignEntity
	 * @return string
	 */
	protected static function getUseCampaignStartDateResult($campaignHasNv, \CampaignEntity $campaignEntity) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& (boolean) $campaignEntity->getUse_campaign_start_date() === true
		) {
			$result = '<img src="img/Tango/22/actions/appointment-new.png" title="' . $campaignEntity->getDatum()->format('d.m.Y H:i') . '" />';
		}
		
		return $result;
	}
	
	
	/**
	 * getStaticCountryIso2Result
	 * 
	 * @param \CampaignWidthCustomerAndContactPersonEntity $campaignEntity
	 * @return string
	 */
	public static function getStaticCountryResultByFieldname(\CampaignWidthCustomerAndContactPersonEntity $campaignEntity, $fieldName = 'iso_2') {
		$result = '';
		
		if (\is_object($campaignEntity->getCustomerEntity()->getStaticCountry())) {
			switch ($fieldName) {
				case 'iso_3':
					$result = $campaignEntity->getCustomerEntity()->getStaticCountry()->getCn_iso_3();
					break;
				
				case 'short_de':
					$result = $campaignEntity->getCustomerEntity()->getStaticCountry()->getCn_short_de();
					break;
				
				default:
					$result = $campaignEntity->getCustomerEntity()->getStaticCountry()->getCn_iso_2();
					break;
			}
		}
		
		return $result;
	}
	
	/**
	 * getCampaignClickProfileResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param integer $campaignClickProfileId
	 * @return string
	 */
	protected static function getCampaignClickProfileResult($campaignHasNv, $campaignClickProfileId) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& (int) $campaignClickProfileId > 0
		) {
			$clientClickProfile = \RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'clientClickProfilesDataArray' . \RegistryUtils::$arrayKeyDelimiter  . $campaignClickProfileId);
			/* @var $clientClickProfile \ClientClickProfileWidthClickProfileEntity */
			
			if (isset($clientClickProfile) 
				&& $clientClickProfile instanceof \ClientClickProfileWidthClickProfileEntity
			) {
				$result = $clientClickProfile->getClickProfile()->getShortcut();
			}
		}
		
		return $result;
	}
	
	/**
	 * getCampaignHasDifferentPricesResult
	 * 
	 * @param boolean $campaignHasDifferentPrices
	 * @return string
	 */
	public static function getCampaignHasDifferentPricesResult($campaignHasDifferentPrices) {
		$result = '';
		
		if ($campaignHasDifferentPrices) {
			$result = '<img src="img/Oxygen/16/emblems/emblem-important.png" title="Kampagne hat unterschiedliche Preise!" />';
		}
		
		return $result;
	}
	
	
	/**
	 * getCsIdResult
	 * 
	 * @param boolean $campaignHasNv
	 * @param integer $dsdId
	 * @return string
	 * 
	 * @deprecated
	 */
	public static function getCsIdResult($campaignHasNv, $csId) {
		$result = '';
		
		if ((boolean) $campaignHasNv !== true 
			&& (int) $csId > 0
		) {
			/* @var $_SESSION CountrySelectionEntity */
			$result = $_SESSION['countrySelectionsDataArray'][$csId]->getTitle();
		}
		
		return $result;
	}
	
	public static function getKosten(\CampaignEntity $campaignEntity){
                require_once(DIR_Manager . 'ClientManager.php');
                 /* @var $clientManager \ClientManager */
                   $clientManager = new \ClientManager();
                   $clientManager->init();
                   $versanParameter = $clientManager->getMandantParameter();
		    $kosten = '';
                    
                        if($campaignEntity->getK_id() !== $campaignEntity->getNv_id()
                          ){
			      $treactionService = self::getNvTreactionService($campaignEntity);
                          }else{
			      $treactionService = self::getHvTreactionService($campaignEntity);                              
                          }
                          
			$abmelder = $campaignEntity->getAbmelder();			
                        $versandkosten = self::getVersandKosten($campaignEntity, \floatval($versanParameter->getVersandTkp()));
			
		$kosten = $treactionService + $versandkosten + ($abmelder * \floatval($versanParameter->getLeadPreis()));
		return $kosten;
	}
	
	
	protected static function getHvTreactionService(\CampaignEntity $campaignEntity){
            require_once(DIR_Manager . 'ClientManager.php');
                 /* @var $clientManager \ClientManager */
                   $clientManager = new \ClientManager();
                   $clientManager->init();
                   $versanParameter = $clientManager->getMandantParameter();
		   $result = '';
				
		if( (int)$campaignEntity->getPremium_campaign() === 1){
			$campaignPreis = \floatval($versanParameter->getPremiumKampagne());
		}else{
			$campaignPreis = \floatval($versanParameter->getKampagne());
		}
		switch ((int)$campaignEntity->advertising_materials){
                      //Optimierung
			case 1 :
				$result = $campaignPreis;
				break;
                     //Erstellung       
			case 2 :
				$result = \floatval($versanParameter->getWerbemittel());
				break;
                     //Wechsel      
			case 3:
				$result = $campaignPreis + $campaignPreis; 
				break;
		}
		           
		return $result;
	}
        protected static function getNvTreactionService(\CampaignEntity $campaignEntity){
        require_once(DIR_Manager . 'ClientManager.php');
                 /* @var $clientManager \ClientManager */
                   $clientManager = new \ClientManager();
                   $clientManager->init();
                   $versanParameter = $clientManager->getMandantParameter();
		  $result = '';
                if( (int)$campaignEntity->getPremium_campaign() === 1
                   ){
			$campaignPreis = \floatval($versanParameter->getPremiumKampagne());
		   }else{
			$campaignPreis = \floatval($versanParameter->getKampagne());
		    }    
                        if($campaignEntity->getK_id() !== $campaignEntity->getNv_id()
                          ){
                            if((boolean) $campaignEntity->getUse_campaign_start_date() === true
                              ){
                                    switch ((int)$campaignEntity->advertising_materials
                                           ){
                                     //Optimierung
                                       case 1 :
                                               $result = 0;
                                               break;
                                    //Erstellung       
                                       case 2 :
                                               $result = \floatval($versanParameter->getWerbemittel());
                                               break;
                                    //Wechsel      
                                       case 3:
                                               $result = $campaignPreis;
                                               break;
                                       default :
                                         $result = 0;                                     
                                       }                                                                                       
                            }else{
                                    switch ((int)$campaignEntity->advertising_materials
                                           ){
                                   //Optimierung
                                     case 1 :
                                             $result = \floatval($versanParameter->getNachversand());
                                             break;
                                  //Erstellung       
                                     case 2 :
                                             $result = \floatval($versanParameter->getWerbemittel()) + \floatval($versanParameter->getNachversand());
                                             break;
                                  //Wechsel      
                                     case 3:
                                             $werbemittel = $campaignPreis + \floatval($versanParameter->getNachversand());
                                             break;
                                      default :
                                         $result = \floatval($versanParameter->getNachversand()); 
                                     }
                            }
                        }

		return $result;
	} 
        protected Static function getVersandKosten(CampaignEntity $campaignEntity, $preis){
            $result = 0;

            if($campaignEntity->getVersendet() > 0
               ){
                $versendet = $campaignEntity->getVersendet();
                $result = ($versendet * $preis) / 1000;
            }
          
            return $result;
        }
        protected static function kosten(CampaignEntity $campaignEntity ,\CampaignEntity $hvAndNvCampaigEntity , $campaignType){
		if((string)$campaignType === 'HV'){
						$resultkosten = $hvAndNvCampaigEntity->getKosten();
				         if(\strlen($resultkosten)> 0
						    ){
				               $resultData = \FormatUtils::sumFormat($resultkosten).\FormatUtils::euroSignInUtf8Unicode;
			      	       }else{
							   $resultData = self::getKosten($campaignEntity);
							   $resultData = \FormatUtils::sumFormat($resultData).\FormatUtils::euroSignInUtf8Unicode;
						   }
					}else{
						$resultData ='';
					}
		return $resultData;			
		}
       
                /**
	 * totalPriceFormat
	 * 
	 * @param mixed $price
	 * @return mixed
	 */
	public static function totalPriceFormat($price){
		$result = \str_replace('.','', $price);
		
		return str_replace(',','.', $result);
	}
                
                
                                        }  