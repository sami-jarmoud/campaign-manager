<?php
final class HtmlTableUtils {
	/**
	 * createTable
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	static public function createTable(array $configDataArray) {
		return array(
			'begin' => '<table ' . self::processConfigDataArray($configDataArray) . '>' . \chr(13),
			'end' => '</table>'
		);
	}

	/**
	 * createTableHeaderRow
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static public function createTableHeaderRow(array $configDataArray = array()) {
		$tmpDataArray = self::createTableRow($configDataArray);

		$tHeadDataArray = self::createTHeadTag();

		$resultDataArray = array(
			'begin' => $tHeadDataArray['begin'] . $tmpDataArray['begin'],
			'end' => $tmpDataArray['end'] . $tHeadDataArray['end']
		);
		unset($tmpDataArray);

		return $resultDataArray;
	}

	/**
	 * createTHeadTag
	 * 
	 * @return array
	 */
	static public function createTHeadTag() {
		return array(
			'begin' => '<thead>' . \chr(13),
			'end' => '</thead>' . \chr(13)
		);
	}
	
	/**
	 * createTFootTag
	 * 
	 * @return array
	 */
	static public function createTFootTag() {
		return array(
			'begin' => '<tfoot>' . \chr(13),
			'end' => '</tfoot>' . \chr(13)
		);
	}

	/**
	 * createTableFooterRow
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	static public function createTableFooterRow(array $configDataArray = array()) {
		$tmpDataArray = self::createTableRow($configDataArray);

		$tHeadDataArray = self::createTFootTag();

		$resultDataArray = array(
			'begin' => $tHeadDataArray['begin'] . $tmpDataArray['begin'],
			'end' => $tmpDataArray['end'] . $tHeadDataArray['end']
		);
		unset($tmpDataArray);

		return $resultDataArray;
	}

	/**
	 * createTableRow
	 * 
	 * @param array $configDataArray
	 * @return array
	 */
	static public function createTableRow(array $configDataArray = array()) {
		$resultDataArray = array(
			'begin' => '<tr ',
			'end' => '</tr>' . \chr(13)
		);

		if (\count($configDataArray) > 0) {
			$resultDataArray['begin'] .= self::processConfigDataArray($configDataArray);
		}

		$resultDataArray['begin'] .= '>' . \chr(13);

		return $resultDataArray;
	}

	/**
	 * createTableHeaderCellWidthContent
	 * 
	 * @param array $configDataArray
	 * @param string $item
	 * @return string
	 */
	static public function createTableHeaderCellWidthContent(array $configDataArray, $item) {
		return '<th ' . self::processConfigDataArray($configDataArray) . '>' . $item . '</th>' . \chr(13);
	}

	/**
	 * createTableCellWidthContent
	 * 
	 * @param array $configDataArray
	 * @param string $item
	 * @return string
	 */
	static public function createTableCellWidthContent(array $configDataArray, $item) {
		return '<td ' . self::processConfigDataArray($configDataArray) . '>' . $item . '</td>' . \chr(13);
	}

	/**
	 * processConfigDataArray
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static private function processConfigDataArray(array $configDataArray) {
		$content = '';

		foreach ($configDataArray as $key => $value) {
			$content .= $key . '="' . $value . '" ';
		}

		return \rtrim($content);
	}

}