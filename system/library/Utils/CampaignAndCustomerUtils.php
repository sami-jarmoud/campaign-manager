<?php
final class CampaignAndCustomerUtils {
	static public $hvCampaignsStatusDataArray = array(
		0 => array(
			'status' => 0,
			'label' => 'Neu',
			'sublabel' => 'Neue Kampagne',
			'campaignViewLabel' => 'Neu',
			'campaignStatusColor' => 'red',
		),
		1 => array(
			'status' => 1,
			'label' => 'Werbemittel erhalten',
			'sublabel' => 'Werbemittel wurden geliefert',
			'campaignViewLabel' => 'Werbemittel',
			'campaignStatusColor' => '#FF9900',
		),
		2 => array(
			'status' => 2,
			'label' => 'Optimiert',
			'sublabel' => 'Werbemittel wurden optimiert und sind versandfertig',
			'campaignViewLabel' => 'Optimiert',
			'campaignStatusColor' => '#FF9900',
		),
		4 => array(
			'status' => 4,
			'label' => 'Freigabe',
			'sublabel' => 'Freigabe wurde erteilt',
			'campaignViewLabel' => 'Freigabe',
			'campaignStatusColor' => '#5DB00D',
		),
		5 => array(
			'status' => 5,
			'label' => 'Versendet',
			'sublabel' => 'Mailing wurde versendet',
			'campaignViewLabel' => 'versendet',
			'campaignStatusColor' => '#5DB00D',
		),
	);
	
	static public $nvCampaignsStatusDataArray = array(
		0 => array(
			'status' => 0,
			'label' => 'Neu',
			'sublabel' => 'Neue Kampagne',
			'campaignViewLabel' => 'Neu',
			'campaignStatusColor' => 'red',
		),
		1 => array(
			'status' => 1,
			'label' => 'Werbemittel erhalten',
			'sublabel' => 'Werbemittel wurden geliefert',
			'campaignViewLabel' => 'Werbemittel',
			'campaignStatusColor' => '#FF9900',
		),
		2 => array(
			'status' => 2,
			'label' => 'Optimiert',
			'sublabel' => 'Werbemittel wurden optimiert und sind versandfertig',
			'campaignViewLabel' => 'Optimiert',
			'campaignStatusColor' => '#FF9900',
		),
		4 => array(
			'status' => 4,
			'label' => 'Freigabe',
			'sublabel' => 'Freigabe wurde erteilt',
			'campaignViewLabel' => 'Freigabe',
			'campaignStatusColor' => '#5DB00D',
		),
		13 => array(
			'status' => 13,
			'label' => 'Programiert',
			'sublabel' => 'Programiert',
			'campaignViewLabel' => 'programiert',
			'campaignStatusColor' => '#FF9900',
		),
		5 => array(
			'status' => 5,
			'label' => 'Versendet',
			'sublabel' => 'Mailing wurde versendet',
			'campaignViewLabel' => 'versendet',
			'campaignStatusColor' => '#5DB00D',
		),
	);
	
	static public $campaignsReportingStatusDataArray = array(
		23 => array(
			'status' => 23,
			'label' => 'Reportfertig',
			'sublabel' => 'Das Mailing ist abgeschlossen und kann reportet werden.',
			'campaignViewLabel' => 'Reportfertig',
			'campaignStatusColor' => '#FF9900',
		),
		20 => array(
			'status' => 20,
			'label' => 'Report intern verschickt',
			'sublabel' => 'Das Reporting wurde intern verschickt',
			'campaignViewLabel' => 'Report intern<br />verschickt',
			'campaignStatusColor' => '#FF9900',
		),
		21 => array(
			'status' => 21,
			'label' => 'Erstreporting abgeschlossen',
			'sublabel' => 'Das Erstreporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Erstreporting<br />abgeschlossen',
			'campaignStatusColor' => '#FF9900',
		),
		22 => array(
			'status' => 22,
			'label' => 'Endreporting abgeschlossen',
			'sublabel' => 'Das finale Reporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Endreporting<br />abgeschlossen',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	static public $campaignsOrderStatusDataArray = array(
		30 => array(
			'status' => 30,
			'label' => 'Rechnungsstellung freigegeben',
			'sublabel' => 'Die Rechnungsstellungsmail wurde an Buchhaltung verschickt.',
			'campaignViewLabel' => 'Rechnungsstellung<br />freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		32 => array(
			'status' => 32,
			'label' => 'Rechnung gestellt',
			'sublabel' => 'Die Rechnung wurde an den Kunden verschickt.',
			'campaignViewLabel' => 'Rechnung<br />gestellt',
			'campaignStatusColor' => '#FF9900',
		),
		33 => array(
			'status' => 33,
			'label' => 'Rechnung bezahlt',
			'sublabel' => 'Die Rechung wurde bezahlt.',
			'campaignViewLabel' => 'Rechnung<br />bezahlt',
			'campaignStatusColor' => '#5DB00D',
		),
		40 => array(
			'status' => 40,
			'label' => 'Gutschrift erhalten',
			'sublabel' => 'Gutschrift erhalten.',
			'campaignViewLabel' => 'Gutschrift erhalten',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	static public $campaignsOthersStatusDataArray = array(
		6 => array(
			'status' => 6,
			'label' => 'senden',
			'sublabel' => 'senden',
			'campaignViewLabel' => 'senden',
			'campaignStatusColor' => 'yellow',
		),
		7 => array(
			'status' => 7,
			'label' => 'abgebrochen',
			'sublabel' => 'Abgebrochen',
			'campaignViewLabel' => 'Abgebrochen',
			'campaignStatusColor' => 'yellow',
		),
		24 => array(
			'status' => 24,
			'label' => 'Report freigegeben',
			'sublabel' => 'Report freigegeben.',
			'campaignViewLabel' => 'Report freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		31 => array(
			'status' => 31,
			'label' => 'Rechnung wird erstellt',
			'sublabel' => 'Der Rechnungserhalt wurde best&auml;tigt',
			'campaignViewLabel' => 'Rechnung<br />wird erstellt',
			'campaignStatusColor' => '#FF9900',
		),
	);
	
	
	static public $campaignBillingsTypeDataArray = array(
		'TKP' => 'TKP',
		'Hybri' => 'Hybrid',
		'CPL' => 'CPL',
		'CPO' => 'CPO',
		'CPC' => 'CPC',
		'Festp' => 'Festpreis'
	);
	
	static public $mailtypDataArray = array(
		'm' => 'Multipart',
		'h' => 'Html',
		't' => 'Text'
	);
	
	static public $genderDataArray = array(
		1 => 'nur Frauen',
		2 => 'nur Herren',
		0 => 'beide'
	);
	
	static public $allTabFieldsDataArray = array(
		'k_id' => array(
			'label' => 'Kampagnen ID',
			'sublabel' => 'ID'
		),
		'datum' => array(
			'label' => 'Versanddatum',
			'sublabel' => 'Start'
		),
		'bearbeiter' => array(
			'label' => 'Bearbeiter',
			'sublabel' => 'Bearbeiter'
		),
		'betreff' => array(
			'label' => 'Betreff',
			'sublabel' => 'Betreff'
		),
		'absender' => array(
			'label' => 'Absendername',
			'sublabel' => 'Absendername'
		),
		'gebucht' => array(
			'label' => 'Gebucht',
			'sublabel' => 'Gebucht'
		),
		'versendet' => array(
			'label' => 'Versendet',
			'sublabel' => 'Versendet'
		),
		'beendet' => array(
			'label' => 'Versandendzeit',
			'sublabel' => 'Beendet'
		),
		'openings' => array(
			'label' => '&Ouml;ffnungen (unique)',
			'sublabel' => '&Ouml; (u)'
		),
		'openings_a' => array(
			'label' => '&Ouml;ffnungen (alle)',
			'sublabel' => '&Ouml;'
		),
		'open_quote_a' => array(
			'label' => '&Ouml;ffnungsrate (alle in %)',
			'sublabel' => '&Ouml;R'
		),
		'open_ziel' => array(
			'label' => '&Ouml;ffnungszielrate',
			'sublabel' => '&Ouml; Ziel'
		),
		'open_quote' => array(
			'label' => '&Ouml;ffnungsrate (unique in %)',
			'sublabel' => '&Ouml;R (u)'
		),
		'klicks' => array(
			'label' => 'Klicks (unique)',
			'sublabel' => 'K (u)'
		),
		'klicks_a' => array(
			'label' => 'Klicks (alle)',
			'sublabel' => 'K'
		),
		'klicks_quote' => array(
			'label' => 'Klickrate (unique in %)',
			'sublabel' => 'KR (u)'
		),
		'klicks_ziel' => array(
			'label' => 'Klickzielrate',
			'sublabel' => 'K Ziel'
		),
		'klicks_quote_a' => array(
			'label' => 'Klickrate (alle in %)',
			'sublabel' => 'KR'
		),
		'hbounces' => array(
			'label' => 'Hardbounces (abs. und %)',
			'sublabel' => 'Hardbounces'
		),
		'sbounces' => array(
			'label' => 'Softbounces (abs. und %)',
			'sublabel' => 'Softbounces'
		),
		'hsbounces' => array(
			'label' => 'Bounces gesamt (abs. und %)',
			'sublabel' => 'Bounces gesamt'
		),
		'zielgruppe' => array(
			'label' => 'Zielgruppe',
			'sublabel' => 'Zielgruppe'
		),
		'bl' => array(
			'label' => 'Blacklist',
			'sublabel' => 'Blacklist'
		),
		'mailformat' => array(
			'label' => 'Mailformat',
			'sublabel' => 'Mime'
		),
		'asp' => array(
			'label' => 'Versandsystem (ASP)',
			'sublabel' => 'ASP'
		),
		'notiz' => array(
			'label' => 'Notiz',
			'sublabel' => 'Notiz'
		),
		'ap' => array(
			'label' => 'Ansprechpartner',
			'sublabel' => 'Ansprechpartner'
		),
		'abrechnungsart' => array(
			'label' => 'Abrechnungsart',
			'sublabel' => 'Abr.rt'
		),
		'preis' => array(
			'label' => 'Preis',
			'sublabel' => 'Preis'
		),
		'preis_gesamt' => array(
			'label' => 'Einnahmen',
			'sublabel' => 'Einnahmen'
		),
               'preis_broking' => array(
			'label' => 'Einnahmen nach Broking',
			'sublabel' => 'E.Broking'
		),
		'avg_tkp' => array(
			'label' => 'eTKP',
			'sublabel' => 'eTKP'
		),
		'leads' => array(
			'label' => 'Leads',
			'sublabel' => 'Leads'
		),
		'leads_u' => array(
			'label' => 'Leads validiert',
			'sublabel' => 'Leads validiert'
		),
		'report' => array(
			'label' => 'Report intern',
			'sublabel' => 'Report intern'
		),
		'rechnung' => array(
			'label' => 'Rechnung',
			'sublabel' => 'Rechnung'
		),
		'erstreporting' => array(
			'label' => 'Erstreporting f&auml;llig am',
			'sublabel' => 'Erstrep. f&auml;llig am'
		),
		'endreporting' => array(
			'label' => 'Endreporting f&auml;llig am',
			'sublabel' => 'Endrep. f&auml;llig am'
		),
		'menge_ziel' => array(
			'label' => 'Vorgabe Menge',
			'sublabel' => 'Menge Ziel'
		),
		'abmelder' => array(
			'label' => 'Abmelder',
			'sublabel' => 'Abmelder'
		),
		'rechnungstellung' => array(
			'label' => 'Rechnungstellung',
			'sublabel' => 'Re-St'
		),
		'zustellreport' => array(
			'label' => 'Zustellreport',
			'sublabel' => 'Z-Rep'
		),
		'infomail' => array(
			'label' => 'Infomail - Neues Mailing',
			'sublabel' => 'Infomail'
		),
		'unit_price' => array(
			'label' => 'Preis pauschal',
			'sublabel' => 'Preis pauschal'
		),
		'anfrage' => array(
			'label' => 'Anfrage details',
			'sublabel' => 'Anfrage details'
		),
		'dsd_id' => array(
			'label' => 'Verteiler',
			'sublabel' => 'Verteiler'
		),
		'mail_id' => array(
			'label' => 'MailId',
			'sublabel' => 'MailId'
		),
		'use_campaign_start_date' => array(
			'label' => 'in Asp programiert',
			'sublabel' => 'in Asp prog.'
		),
		'actionLogInfo' => array(
			'label' => 'LogInfo',
			'sublabel' => 'LogInfo'
		),
		'campaign_click_profile_id' => array(
			'label' => 'Kampagnen-Klickprofil',
			'sublabel' => 'K.-Klickprofil'
		),
		'selection_click_profile_id' => array(
			'label' => 'Selektion-Klickprofil',
			'sublabel' => 'S.-Klickprofil'
		),
		'broking_volume' => array(
			'label' => 'Broking Volumen',
			'sublabel' => 'B.Volumen'
		),
		'broking_price' => array(
			'label' => 'Broking Preis',
			'sublabel' => 'B.Preis'
		),
                'ctr' => array(
			'label' => 'Click-Through-Rate',
			'sublabel' => 'CTR'
		),
		'kosten' => array(
			'label' => 'Kosten',
			'sublabel' => 'Kosten'
		),
		'deckungsbeitrag' => array(
			'label' => 'Deckungsbeitrag',
			'sublabel' => 'DB'
		),
               'monatsrechnung' => array(
			'label' => 'M.Rechnung',
			'sublabel' => 'M.Rechnung'
		),
		// TODO: deprecated, entfernen
		#'crm_order_id' => array(
		#	'label' => 'CRM - Id',
		#	'sublabel' => 'CRM - Id'
		#),
		'cs_id' => array(
			'label' => 'Selektion Land',
			'sublabel' => 'Selektion Land'
		),
	);
	
	static public $customerDataSelections = array(
		1 => 'DOI',
		2 => 'SOI',
		3 => 'DOI/SOI',
		0 => 'Unbekannt'
	);
	
	static public $advertisingMaterialsDataArray = array(
		1 => 'Optimierung',
		2 => 'Erstellung',
		3 => 'Wechsel'
	);
	
	
	
	
	
	/**
	 * getStatusFieldById
	 * 
	 * @param integer $statusId
	 * @param string $fieldLabel
	 * @return string
	 */
	static public function getStatusFieldById($statusId, $fieldLabel) {
		$result = '';
		foreach (\RegistryUtils::get('campaign' . \RegistryUtils::$arrayKeyDelimiter . 'statusDataArray' . \RegistryUtils::$arrayKeyDelimiter . 'all') as $key => $dataArray) {
			if ($key === (int) $statusId) {
				$result = $dataArray[$fieldLabel];
				break;
			}
		}
		unset($statusDataArray);
		
		return $result;
	}
	
}