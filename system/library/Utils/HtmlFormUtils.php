<?php
final class HtmlFormUtils {
	/**
	 * createOptionListItemData
	 * 
	 * @param array $dataArray
	 * @param string|integer $actualValue
	 * @return string
	 */
	static public function createOptionListItemData(array $dataArray, $actualValue) {
		$result = '';

		foreach ($dataArray as $key => $value) {
			$selected = '';
			if ((string) $actualValue === (string) $key) {
				$selected = 'selected';
			}

			$result .= self::createOptionItem(
				array(
					'value' => $key,
					'selected' => $selected
				),
				$value
			);
		}

		return $result;
	}

	/**
	 * createOptionListItemDataWithoutKey
	 * 
	 * @param array $dataArray
	 * @param mixed $actualValue
	 * @return string
	 */
	static public function createOptionListItemDataWithoutKey(array $dataArray, $actualValue) {
		$result = '';

		foreach ($dataArray as $value) {
			$selected = '';
			if ((string) $actualValue === (string) $value) {
				$selected = 'selected';
			}

			$result .= self::createOptionItem(
				array(
					'value' => $value,
					'selected' => $selected
				),
				$value
			);
		}

		return $result;
	}

	/**
	 * createSelectItem
	 * 
	 * @param array $configDataArray
	 * @param string $optionsItems
	 * @param boolean $addDefaultItem
	 * @param string $defaultTitle
	 * @return string
	 */
	static public function createSelectItem(array $configDataArray, $optionsItems, $addDefaultItem = false, $defaultTitle = '- Bitte ausw&auml;hlen -') {
		$result = '<select ';
		foreach ($configDataArray as $key => $value) {
			$result .= $key . '="' . $value . '" ';
		}
		$result .= '/>' . \chr(11);

		if ((boolean) $addDefaultItem === true) {
			$result .= self::createOptionItem(
				array(
					'value' => ''
				),
				$defaultTitle
			);
		}

		$result .= $optionsItems . \chr(11) . '</select>';

		return $result;
	}

	/**
	 * createOptgroupDataArray
	 * 
	 * @param string $labelTitle
	 * @return array
	 */
	static public function createOptgroupDataArray($labelTitle) {
		return array(
			'begin' => '<optgroup label="' . $labelTitle . '">',
			'end' => '</optgroup>'
		);
	}
	
	/**
	 * openSelectOptgroup
	 * 
	 * @param integer $c
	 * @param array $optgroupDataArray
	 * @return string
	 */
	static public function openSelectOptgroup($c, array $optgroupDataArray) {
		$result = '';
		
		if ($c > 1) {
			/**
			 * closeSelectOptgroup
			 */
			$result .= self::closeSelectOptgroup($optgroupDataArray);
		}
		
		// openSelectOptgroup
		$result .= $optgroupDataArray['begin'];
		
		return $result;
	}
	
	/**
	 * closeSelectOptgroup
	 * 
	 * @param array $optgroupDataArray
	 * @return string
	 */
	static public function closeSelectOptgroup(array $optgroupDataArray) {
		return $optgroupDataArray['end'];
	}
	
	/**
	 * createOptionItem
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @return string
	 */
	static public function createOptionItem(array $configDataArray, $title) {
		$result = '<option ';
		foreach ($configDataArray as $key => $value) {
			if ($key == 'value') {
				if (\strlen($value) > 0) {
					$result .= $key . '="' . $value . '" ';
				} else {
					$result .= $key . '="" ';
				}
			} elseif (\strlen($value) > 0) {
				$result .= $key . '="' . $value . '" ';
			}
		}
		$result .= '>' . $title . '</option>' . \chr(11);

		return $result;
	}

	/**
	 * createHiddenField
	 * 
	 * @param string $name
	 * @param string $value
	 * @return string
	 */
	static public function createHiddenField($name, $value) {
		return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
	}

	/**
	 * createCheckbox
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static public function createCheckbox(array $configDataArray) {
		$result = '<input type="checkbox" ';

		foreach ($configDataArray as $key => $item) {
			if (\strlen($item) > 0) {
				$result .= $key . '="' . $item . '" ';
			}
		}

		$result .= '/>';

		return $result;
	}

	/**
	 * createCheckboxItem
	 * 
	 * @param array $configDataArray
	 * @param string $title
	 * @return string
	 */
	static public function createCheckboxItem(array $configDataArray, $title) {
		return self::createCheckbox($configDataArray) . ' ' . $title . \chr(13);
	}

	/**
	 * createCheckboxItemWidthLabel
	 * 
	 * @param array $checkboxConfigDataArray
	 * @param string $labelTitle
	 * @param array $checkboxConfigDataArray
	 * @return string
	 */
	static public function createCheckboxItemWidthLabel(array $checkboxConfigDataArray, $labelTitle, array $labelConfigDataArray = array()) {
		return self::createCheckbox($checkboxConfigDataArray)
			. self::createLabelFieldItem(
				$labelTitle,
				$labelConfigDataArray
			) . \chr(13);
	}

	/**
	 * createLabelFieldItem
	 * 
	 * @param string $title
	 * @param array $configDataArray
	 * @return string
	 */
	static public function createLabelFieldItem($title, array $configDataArray = array()) {
		$result = '<label ';

		if (\count($configDataArray) > 0) {
			foreach ($configDataArray as $key => $value) {
				$result .= $key . '="' . $value . '" ';
			}
		}
		$result .= '>' . $title . '</label>' . \chr(11);

		return $result;
	}

	/**
	 * createRadioboxItemWidthLabel
	 * 
	 * @param array $radioboxConfigDataArray
	 * @param string $labelTitle
	 * @param array $labelConfigDataArray
	 * @return string
	 */
	static public function createRadioboxItemWidthLabel(array $radioboxConfigDataArray, $labelTitle, array $labelConfigDataArray = array()) {
		return self::createRadiobox($radioboxConfigDataArray)
			. ' ' . self::createLabelFieldItem(
				$labelTitle,
				$labelConfigDataArray
			) . \chr(13);
	}

	/**
	 * createRadiobox
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static public function createRadiobox(array $configDataArray) {
		$result = '<input type="radio" ';

		foreach ($configDataArray as $key => $item) {
			if (\strlen($item) > 0) {
				$result .= $key . '="' . $item . '" ';
			}
		}

		$result .= '/>';

		return $result;
	}

	/**
	 * createButton
	 * 
	 * @param array $configDataArray
	 * @return string
	 */
	static public function createButton(array $configDataArray) {
		$result = '<input type="button" ';

		foreach ($configDataArray as $key => $item) {
			if (\strlen($item) > 0) {
				$result .= $key . '="' . $item . '" ';
			}
		}

		$result .= '/>';

		return $result;
	}

}