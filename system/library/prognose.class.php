<?php
class prognose {

	public static function getHour($datum) {
	
		$heuteY = date("Y");
		$heuteM = date("m");
		$heuteD = date("d");
		$passedDays = 0;
		
		$datumParts = explode(" ",$datum);
		
		$datumPartsDate = explode("-",$datumParts[0]);
		
		if ($datumPartsDate[0] == $heuteY && $datumPartsDate[1] == $heuteM && $datumPartsDate[2] == $heuteD ) {
			$datumPartsTime = explode(":",$datumParts[1]);
		} 
		
			$heute = time();
			$datum_timestamp = mktime(0,0,0,$datumPartsDate[1],$datumPartsDate[2],$datumPartsDate[0]);
			
			$passedDays = $heute-$datum_timestamp;
			$passedDays = floor($passedDays/86400);
		
		$passedTime = array("h" => $datumPartsTime[0],"passedDays" => $passedDays);
		return $passedTime;

	}
	
	
	public static function getMailCat($versendet,$datum,$openings,$klicks) {
		
		$heuteH = date("H");
		
		$time = prognose::getHour($datum);
		
		if ($time['passedDays'] == 0) {
			$Hour = $time['h'];
			$hourPassed = $heuteH-$Hour;
			$Hour = 0;
			
		} else {
			$Hour = $time['passedDays'];
			$hourPassed = 24;
		}
		
		
		$openingRate = round((($openings/$versendet)*100),2);
		$klickRate = round((($klicks/$versendet)*100),2);

		if ($hourPassed>=0 && $hourPassed<=5) {		
			
			if ($hourPassed == 0) {
			
				if ($openingRate < 0.05) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.05 && $openingRate <= 0.1) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 0.1 && $openingRate <= 0.4) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 0.4 && $openingRate <= 0.8) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 0.9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.05) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.05 && $klickRate <= 0.08) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.09 && $klickRate <= 0.5) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 0.6) {
					$klickCat = 5;
				}
			
				
			}
			
			if ($hourPassed == 1) {
			
				if ($openingRate <= 0.05) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.1 && $openingRate <= 0.4) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 0.4 && $openingRate <= 0.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 1 && $openingRate <= 1.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 2) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.05) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.06 && $klickRate <= 0.08) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.09 && $klickRate <= 0.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 1) {
					$klickCat = 5;
				}
				
			}
			
			if ($hourPassed == 2) {
			
				if ($openingRate < 0.5) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.5 && $openingRate <= 0.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 1 && $openingRate <= 1.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 2 && $openingRate <= 3.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 4) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.02) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.02 && $klickRate <= 0.09) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.1 && $klickRate <= 0.19) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.2 && $klickRate <= 1.4) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 1.5) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($hourPassed == 3) {
			
				if ($openingRate < 0.8) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 0.9 && $openingRate <= 1.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 2 && $openingRate <= 2.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 3 && $openingRate <= 6.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 7) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.04) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.04 && $klickRate <= 0.14) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.16 && $klickRate <= 0.25) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.26 && $klickRate <= 1.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 2) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($hourPassed == 4) {
			
				if ($openingRate < 1) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 1 && $openingRate <= 1.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 2 && $openingRate <= 3.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 4 && $openingRate <= 7.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 8) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.04) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.04 && $klickRate <= 0.19) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.2 && $klickRate <= 0.29) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.3 && $klickRate <= 2.4) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 2.5) {
					$klickCat = 5;
				}
			
			}
			
			
			if ($hourPassed == 5) {
			
				if ($openingRate < 1.5) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 1.6 && $openingRate <= 3.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 4 && $openingRate <= 5.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 6 && $openingRate <= 8.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 9) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.09) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.1 && $klickRate <= 0.24) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.25 && $klickRate <= 0.49) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.5 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 3) {
					$klickCat = 5;
				}
				
			}
			
			
				
		}
		
		if ($time['passedDays']>=1 && $time['passedDays']<=4) {
			
			if ($time['passedDays'] == 1) {
				
				if ($openingRate < 1.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 2 && $openingRate <= 5.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 6 && $openingRate <= 9.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 10 && $openingRate <= 17.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 18) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.1) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.2 && $klickRate <= 0.4) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.5 && $klickRate <= 0.7) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 0.8 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 3) {
					$klickCat = 5;
				}
				
			}
			
			
			if ($time['passedDays']== 2) {
				
				if ($openingRate < 2.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 3 && $openingRate <= 6.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 7 && $openingRate <= 10.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 11 && $openingRate <= 19.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 20) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.3) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.4 && $klickRate <= 0.6) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.7 && $klickRate <= 1.1) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 1.2 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 3) {
					$klickCat = 5;
				}
				
			}
			
			
			if ($time['passedDays'] >= 3) {
				
				if ($openingRate < 2.9) {
					$openingCat = 1;
				}
				
				if ($openingRate >= 3 && $openingRate <= 7.9) {
					$openingCat = 2;
				}
				
				if ($openingRate >= 8 && $openingRate <= 12.9) {
					$openingCat = 3;
				}
				
				if ($openingRate >= 13 && $openingRate <= 19.9) {
					$openingCat = 4;
				}
				
				if ($openingRate >= 20) {
					$openingCat = 5;
				}
				
				if ($klickRate < 0.3) {
					$klickCat = 1;
				}
				
				if ($klickRate >= 0.4 && $klickRate <= 0.6) {
					$klickCat = 2;
				}
				
				if ($klickRate >= 0.7 && $klickRate <= 1.2) {
					$klickCat = 3;
				}
				
				if ($klickRate >= 1.3 && $klickRate <= 2.9) {
					$klickCat = 4;
				}
				
				if ($klickRate >= 3) {
					$klickCat = 5;
				}
			}
			
				
			
		}
		
		$catArr = array("Tag" => $time['passedDays'], "Stunde" => $hourPassed, "oCat" => $openingCat, "kCat" => $klickCat);
		return $catArr;
	}
	
	
	
	public static function getPrognose($gebucht,$versendet,$klicks,$openings,$day,$hour,$oCat,$kCat) {

		if ($hour >= 0 && $hour <= 23 && $day == 0) {

			if ($hour == 0) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.5; break;
					case 2: $okoeff = 1.7; break;
					case 3: $okoeff = 2; break;
					case 4: $okoeff = 2.5; break;
					case 5: $okoeff = 4; break;
				}
				
				switch ($kCat) {
					case 1: $kkoeff = 1.5; break;
					case 2: $kkoeff = 2; break;
					case 3: $kkoeff = 3; break;
					case 4: $kkoeff = 3.5; break;
					case 5: $kkoeff = 4; break;
				}
				
			}
			
			
			if ($hour == 1) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.1; break;
					case 2: $okoeff = 1.2; break;
					case 3: $okoeff = 1.3; break;
					case 4: $okoeff = 1.5; break;
					case 5: $okoeff = 1.5; break;
				}
				
				switch ($kCat) {
					case 1: $kkoeff = 1.3; break;
					case 2: $kkoeff = 1.3; break;
					case 3: $kkoeff = 1.8; break;
					case 4: $kkoeff = 2; break;
					case 5: $kkoeff = 2.5; break;
				}
				
			}
			
			
			if ($hour == 2) {
				
				switch ($oCat) {
					case 1: $okoeff = 1.2; break;
					case 2: $okoeff = 1.2; break;
					case 3: $okoeff = 1.3; break;
					case 4: $okoeff = 1.4; break;
					case 5: $okoeff = 1.4; break;
				}
				
					$kkoeff = 1.2;
				
			}
			
			
			if ($hour > 2 && $hour <= 5) {
				
					$okoeff = 1.1;
					$kkoeff = 1.1;
				
			}
			
			if ($hour > 5) {
				
					$okoeff = 1.05;
					$kkoeff = 1.05;
				
			}
		
		}
		
		
		if ($day >= 1) {

			if ($day == 1 && $oCat ) { 
				
				switch ($oCat) {
					case 1: $okoeff = 1.05; break;
					case 2: $okoeff = 1.1; break;
					case 3: $okoeff = 1.3; break;
					case 4: $okoeff = 1.5; break;
					case 5: $okoeff = 1.5; break;
					default : $okoeff = 1.1; break;
				}
				
				switch ($kCat) {
					case 1: $kkoeff = 1.1; break;
					case 2: $kkoeff = 1.2; break;
					case 3: $kkoeff = 1.3; break;
					case 4: $kkoeff = 1.4; break;
					case 5: $kkoeff = 1.5; break;
					default : $kkoeff = 1.3; break;
				}
								
			}
			
			if ($day == 1 && !$oCat) {
				$okoeff = 1.05;
				$kkoeff = 1.03;
			}
			
			if ($day == 2) { 
			
				$okoeff = 1.04;
				$kkoeff = 1.03;
				
			}
			
			if ($day == 3) { 
			
				$okoeff = 1.03;
				$kkoeff = 1.03;
				
			}
			
			if ($day == 4) { 
			
				$okoeff = 1.02;
				$kkoeff = 1.02;
				
			}
			
			if ($day >= 5) { 
			
				$okoeff = 1.01;
				$kkoeff = 1.01;
				
			}
		
		}
		
		$koeffArr = array("okoeff" => $okoeff, "kkoeff" => $kkoeff);
		
		return $koeffArr;
	
	}
	
	
	public static function getPrognoseData ($gebucht,$versendet,$versanddatum,$oeffner,$klicks) {
	
		global $rateHoursArr;
		global $rateLeftHoursArr;
		global $rateLeftDaysArr;
		global $lefth;
		
		$rateHoursArr = array();
		$rateLeftHoursArr = array();
		$rateLeftDaysArr = array();
		
		$t = prognose::getMailCat($versendet,$versanddatum,$oeffner,$klicks);
		if ($t['Stunde'] >= 0 && $t['Stunde']<5) {
		
			$lefth = 4-$t['Stunde'];
			
			for($i=1; $i <= $lefth; $i++) {
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,$t['Tag'],$lefth,$t['oCat'],$t['kCat']);
				
				$klicks = $klicks*$t2["kkoeff"];
				$oeffner = $oeffner*$t2["okoeff"];
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
			}
			
			$rateHoursArr = array("kRate" => $krate, "oRate" => $orate);
						
		}
		
		
		if ($t['Stunde'] >= 5 && $t['Stunde']<23) {
		
			$d = date("Y-m-d H:m:s");
			$time = prognose::getHour($d);
			
			$j = 24-$time['h'];
			
			for($i=1; $i < $j; $i++) {
			
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,0,$j,-1,-1);
				
				$klicks = $klicks*$t2["kkoeff"];
				$oeffner = $oeffner*$t2["okoeff"];
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
				$h = $i;
				
				$rateLeftHoursArr[] = array("kRate" => $krate, "oRate" => $orate);
			}
			
		}
		
		
		if ($t['Tag'] >= 0 && $t['Tag']<5) {

			$d = $t['Tag'];
			if ($d == 0) {$d = 1;}
			$j = 4-$t['Tag'];
			for($i=1; $i <= $j; $i++) {
			
				$t2 = prognose::getPrognose($gebucht,$versendet,$klicks,$oeffner,$d,$t['oCat'],$t['kCat']);
				
				$klicks = $klicks*$t2["kkoeff"];
				$oeffner = $oeffner*$t2["okoeff"];
				
				$krate = round((($klicks/$gebucht)*100),2);
				$orate = round((($oeffner/$gebucht)*100),2);
				
				$d++;
				
				$rateLeftDaysArr[] = array("kRate" => $krate, "oRate" => $orate);
			}
		}
		
	}
	
}
?>