<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/system/Vendor/FirePHPCore/FirePHP.class.php');

/**
 * @deprecated use Packages\DebugLogManager
 */
class debugLogManager {
    protected $userId = 0;
    
    /**
     * @var FirePHP
     */
    protected $firePhp = null;
    
    
    
    
    /**
     * init
     * 
     * @return  void
     */
    public function init() {
        if ($this->getUserId() === \BaseUtils::getDebugUserId()) {
            $this->setFirePhp(FirePHP::getInstance(true));
        }
    }
    
    
    /**
     * logData
     * 
     * @param   string  $header
     * @param   mixed   $data
     * 
     * @return  void
     */
    public function logData($header, $data) {
        if ($this->getFirePhp() instanceof FirePHP) {
            $this->getFirePhp()->log($data, $header . ':');
        }
    }
    
    
    /**
     * beginGroup
     * 
     * @param   string  $title
     * 
     * @return  void
     */
    public function beginGroup($title) {
        if ($this->getFirePhp() instanceof FirePHP) {
            $this->getFirePhp()->group($title);
        }
    }
    
    
    /**
     * endGroup
     * 
     * @return  void
     */
    public function endGroup() {
        if ($this->getFirePhp() instanceof FirePHP) {
            $this->getFirePhp()->groupEnd();
        }
    }
    
    
    
    
    
    /********************************************************************************************
     *
     *              setter and getter - functions
     *
     *******************************************************************************************/
    public function getUserId() {
        return (int) $this->userId;
    }
    public function setUserId($userId) {
        $this->userId = intval($userId);
    }

    public function getFirePhp() {
        return $this->firePhp;
    }
    public function setFirePhp($firePhp) {
        $this->firePhp = $firePhp;
    }
}
?>