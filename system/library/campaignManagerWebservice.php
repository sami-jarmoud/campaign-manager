<?php
class campaignManagerWebservice {
    public function __construct() {
        $mandant = $_SESSION['mandant'];
        include($_SERVER['DOCUMENT_ROOT'] . '/system/db_connect.inc.php');
        
        $this->dbh = new PDO('mysql:host=' . $dbHostEms . ';dbname=' . $db_name, $db_user, $db_pw);
        #$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

    protected function isInt($value) {
        return (preg_match( '/^\d*$/', $value) == 1);
    }
    
    public function getKundenData($table, $type) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' ORDER BY `firma` ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'id' => $row['kunde_id'],
                'firma' => '<span style="font-weight:bold">' . $row['firma'] . '</span>',
                'firma_short' => $row['firma_short'],
                'status' => $row['status']
            );
        }

        if ($type == 'json') {
            $dataArr = '{"Result": ' . json_encode($dataArr) . '}';
        }

        return $dataArr;
    }

    public function getKundeExist($table, $id) {
        $result = 0;
        
        if ($this->isInt($id) === true) {
            $qry = 'SELECT COUNT(*)' . 
                ' FROM `' . $table . '`' . 
                ' WHERE `kunde_id` = :id'
            ;
            
            $stmt = $this->dbh->prepare($qry);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $result = $stmt->fetchColumn();
        }
        
        return $result;
    }

    public function getUpdateDate($table) {
        $qry = 'SELECT `gestartet`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `k_id` = "1"'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['gestartet'];
    }

    public function getKundeExistByName($table, $firma) {
        $qry = ' SELECT `kunde_id`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `firma` = :firma'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':firma', $firma, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row['kunde_id'];
    }

    public function setKunde($table, $name, $short, $street, $zip, $city, $fon, $fax, $www, $email, $ceo, $register, $crmCustomerId) {
        $kundeID = $this->getKundeExistByName(
            $table,
            $name
        );

        if ($kundeID > 0) {
            $kunde_id = $kundeID;
        } else {
            $qry = 'INSERT INTO `' . $table . '`' . 
                ' (`firma`, `status`, `firma_short`, `strasse`, `plz`, `ort`, `telefon`, `fax`, `website`, `email`, `geschaeftsfuehrer`, `registergericht`, `crm_customer_id`)' . 
                ' VALUES (:name, "1", :short, :street, :zip, :city, :fon, :fax, :www, :email, :ceo, :register, :crmCustomerId)'
            ;
            $stmt = $this->dbh->prepare($qry);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':short', $short, PDO::PARAM_STR);
            $stmt->bindParam(':street', $street, PDO::PARAM_STR);
            $stmt->bindParam(':zip', $zip, PDO::PARAM_STR);
            $stmt->bindParam(':city', $city, PDO::PARAM_STR);
            $stmt->bindParam(':fon', $fon, PDO::PARAM_STR);
            $stmt->bindParam(':fax', $fax, PDO::PARAM_STR);
            $stmt->bindParam(':www', $www, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':ceo', $ceo, PDO::PARAM_STR);
            $stmt->bindParam(':register', $register, PDO::PARAM_STR);
            $stmt->bindParam(':crmCustomerId', $crmCustomerId, PDO::PARAM_STR);
            $stmt->execute();
            
            $kunde_id = $this->dbh->lastInsertId();
        }

        return $kunde_id;
    }

    public function setKontakt($table, $kunde_id, $ap_anrede, $ap_titel, $ap_vorname, $ap_nachname, $ap_email, $ap_vertrieblerId, $ap_crmId) {
        $qry = 'INSERT INTO `' . $table . '`' . 
            ' (`kunde_id`, `anrede`, `titel`, `vorname`, `nachname`, `email`, `status`, `vertriebler_id`, `crm_ap_id`)' . 
            ' VALUES (:kunde_id, :ap_anrede, :ap_titel, :ap_vorname, :ap_nachname, :ap_email, "1", :ap_vertrieblerId, :crmApId)'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':kunde_id', $kunde_id, PDO::PARAM_INT);
        $stmt->bindParam(':ap_anrede', $ap_anrede, PDO::PARAM_STR);
        $stmt->bindParam(':ap_titel', $ap_titel, PDO::PARAM_STR);
        $stmt->bindParam(':ap_vorname', $ap_vorname, PDO::PARAM_STR);
        $stmt->bindParam(':ap_nachname', $ap_nachname, PDO::PARAM_STR);
        $stmt->bindParam(':ap_email', $ap_email, PDO::PARAM_STR);
        $stmt->bindParam(':ap_vertrieblerId', $ap_vertrieblerId, PDO::PARAM_INT);
        $stmt->bindParam(':crmApId', $ap_crmId, PDO::PARAM_STR);
        $stmt->execute();

        return $this->dbh->lastInsertId();
    }

    public function getKundeData($table, $id) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `kunde_id` = :id'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $dataArr = array(
            'id' => $row['kunde_id'],
            'firma' => $row['firma'],
            'firma_short' => $row['firma_short'],
            'strasse' => $row['strasse'],
            'plz' => $row['plz'],
            'ort' => $row['ort'],
            'telefon' => $row['telefon'],
            'fax' => $row['fax'],
            'website' => $row['website'],
            'email' => $row['email'],
            'geschaeftsfuehrer' => $row['geschaeftsfuehrer'],
            'registergericht' => $row['registergericht'],
            'temp1' => $row['temp1'],
            'temp2' => $row['temp2'],
            'status' => $row['status']
        );

        return $dataArr;
    }

    public function getAllAP($table, $id) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `kunde_id` = :id ' . 
                ' AND `status` = "1"' . 
            ' ORDER BY `email` ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'ap_id' => $row['ap_id'],
                'anrede' => $row['anrede'],
                'titel' => $row['titel'],
                'vorname' => utf8_decode($row['vorname']),
                'nachname' => utf8_decode($row['nachname']),
                'email' => $row['email'],
                'telefon' => $row['telefon'],
                'fax' => $row['fax'],
                'mobil' => $row['mobil'],
                'position' => $row['position'],
                'status' => $row['status'],
                'vertriebler_id' => $row['vertriebler_id']
            );
        }

        return $dataArr;
    }

    public function getAP($table, $id) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `ap_id` = :id'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $dataArr = array(
            'ap_id' => $row['ap_id'],
            'anrede' => $row['anrede'],
            'titel' => $row['titel'],
            'vorname' => utf8_decode($row['vorname']),
            'nachname' => utf8_decode($row['nachname']),
            'email' => $row['email'],
            'telefon' => $row['telefon'],
            'fax' => $row['fax'],
            'mobil' => $row['mobil'],
            'position' => $row['position'],
            'status' => $row['status'],
            'vertriebler_id' => $row['vertriebler_id']
        );

        return $dataArr;
    }

    public function countCampaignsByYear($table, $year, $abrTypSql) {
        $qry = ' SELECT MONTH(`datum`) as `monat`, COUNT(*) as `anzahl`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE YEAR(`datum`) = :year' . 
                ' AND `k_id` = `nv_id`' . 
                ' AND `k_id` > "1"' . 
                ' ' . trim($abrTypSql) . 
            ' GROUP BY MONTH(`datum`)' . 
            ' ORDER BY MONTH(`datum`) ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[$row['monat']] = array(
                'monat' => $row['monat'],
                'anzahl' => $row['anzahl']
            );
        }

        return $dataArr;
    }

    public function countCampaignsSepByYear($table, $year, $abrTypSql) {
        $qry = ' SELECT MONTH(`datum`) as `monat`, COUNT(*) as `anzahl`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE YEAR(`datum`) = :year' . 
                ' AND `k_id` > "1"' . 
                ' AND `status` != "10"' . 
                ' ' . trim($abrTypSql) . 
            ' GROUP BY MONTH(`datum`)' . 
            ' ORDER BY MONTH(`datum`) ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':year', $year, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[$row['monat']] = array(
                'monat' => $row['monat'],
                'anzahl' => $row['anzahl']
            );
        }

        return $dataArr;
    }

    public function getSumCampaignsByMonth($table, $kid) {
        $qryGebucht = ' SELECT SUM(`gebucht`) as `gebucht`, SUM(`versendet`) as `versendet`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `nv_id` = :kid'
        ;
        $stmt = $this->dbh->prepare($qryGebucht);
        $stmt->bindParam(':kid', $kid, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $dataArr = array(
            'k_id' => $kid,
            'gebucht' => $row['gebucht'],
            'versendet' => $row['versendet']
        );

        return $dataArr;
    }

    public function getCampaignsByMonthKunde($table, $year, $month, $agentur_id, $abrTyp = '') {
        $monthSql = '';
        if ($month != '') {
            $monthSql = ' AND MONTH(`datum`) = :month';
        }
        
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE YEAR(`datum`) = :year' . 
                ' AND `status` > "4"' . 
                ' AND `agentur_id` = :agentur_id' . 
                ' AND `k_id` = `nv_id`' . 
                $monthSql
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':year', $year, PDO::PARAM_INT);
        
        if ($month != '') {
            $stmt->bindParam(':month', $month, PDO::PARAM_INT);
        }
        
        $stmt->bindParam(':agentur_id', $agentur_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'kid' => $row['k_id'],
                'gebucht' => $row['gebucht']
            );
        }

        return $dataArr;
    }

    public function getCampaignYear($table) {
        $qry = 'SELECT YEAR(`datum`) as `y`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE YEAR(`datum`) != "0000"' . 
            ' GROUP BY `y`' . 
            ' ORDER BY `y` DESC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'jahr' => $row['y']
            );
        }

        return $dataArr;
    }

    public function getCampaignKundeByYear($table, $kunde, $year, $month) {
        $m = '';
        if ($month != '') {
            $m = ' AND MONTH(`' . $table . '`.`datum`) = "' . $month . '"';
        }

        $qry = 'SELECT `' . $kunde . '`.`kunde_id`, `' . $kunde . '`.`firma`' . 
            ' FROM `' . $kunde . '`,`' . $table . '`' . 
            ' WHERE `' . $kunde . '`.`kunde_id` = `' . $table . '`.`agentur_id`' . 
                ' AND YEAR(`' . $table . '`.`datum`) = "' . $year . '"' . 
                ' AND `' . $table . '`.`status` > "4"' . 
                $m . 
            ' GROUP BY `' . $kunde . '`.`firma`' . 
            ' ORDER BY `' . $kunde . '`.`firma` ASC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'kunde_id' => $row['kunde_id'],
                'firma' => $row['firma'],
                'anzahl' => $row['anzahl']
            );
        }

        return $dataArr;
    }

    public function getCampaignKundeByAbr($table, $year, $month, $kunde_id, $abrechnungsart) {
        $m = '';
        if ($month != '') {
            $m = ' AND MONTH(`datum`) = "' . $month . '"';
        }

        if ($abrechnungsart == 'Hybri') {
            $preis = '`preis2`';
        } else {
            $preis = '`preis`';
        }
        
        $qry = 'SELECT ((`gebucht`/1000)*`preis`) as `tkp_gesamt`, (`leads`*' . $preis . ') as `l_gesamt`, (`leads_u`*' . $preis . ') as `lu_gesamt`, `preis_gesamt`, `preis_gesamt_verify`' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `agentur_id` = "' . $kunde_id . '"' . 
                ' AND `abrechnungsart` = "' . $abrechnungsart . '"' . 
                ' AND YEAR(`datum`) = "' . $year . '"' . 
                $m . 
                ' AND `k_id` = `nv_id`' . 
                ' AND `status` > "4"' . 
                ' AND `k_id` > "1"'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'tkp_gesamt' => $row['tkp_gesamt'],
                'l_gesamt' => $row['l_gesamt'],
                'lu_gesamt' => $row['lu_gesamt'],
                'preis_gesamt' => $row['preis_gesamt'],
                'preis_gesamt_verify' => $row['preis_gesamt_verify']
            );
        }

        return $dataArr;
    }

    public function getCampaignData($table, $id) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `k_id` = :id'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $row = $stmt->fetch();

        $dataArr = array(
            'kid' => $row['k_id'],
            'k_name' => utf8_decode($row['k_name']),
            'agentur' => utf8_decode($row['agentur']),
            'mime' => $row['mailtyp'],
            'hash' => $row['hash'],
            'notiz' => utf8_decode($row['notiz']),
            'absendername' => utf8_decode($row['absendername']),
            'versanddatum' => $row['datum'],
            'vorgabe_m' => $row['vorgabe_m'],
            'versandsystem' => $row['versandsystem'],
            'zielgruppe' => utf8_decode($row['zielgruppe']),
            'betreff' => utf8_decode($row['betreff']),
            'blacklist' => $row['blacklist'],
            'ap_id' => $row['ap_id']
        );

        return $dataArr;
    }

    public function getCampaignDataByYear($table, $year) {
        $qry = 'SELECT *' . 
            ' FROM :table' . 
            ' WHERE YEAR(`datum`) = :year' . 
                ' AND `k_id` > "1"' . 
                ' AND `status` > "4"' . 
            ' ORDER BY `datum` DESC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':year', $year, PDO::PARAM_INT);
        $stmt->bindParam(':table', $table, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'kid' => $row['k_id'],
                'k_name' => utf8_decode($row['k_name']),
                'agentur' => utf8_decode($row['agentur']),
                'mime' => $row['mailtyp'],
                'hash' => $row['hash'],
                'notiz' => utf8_decode($row['notiz']),
                'absendername' => utf8_decode($row['absendername']),
                'versanddatum' => $row['datum'],
                'vorgabe_m' => $row['vorgabe_m'],
                'versandsystem' => $row['versandsystem'],
                'zielgruppe' => utf8_decode($row['zielgruppe']),
                'betreff' => utf8_decode($row['betreff']),
                'blacklist' => $row['blacklist']
            );
        }

        return $dataArr;
    }

    public function getCmReporting($table, $kid) {
        $qry = 'SELECT *' . 
            ' FROM `' . $table . '`' . 
            ' WHERE `kid` = :kid' . 
            ' ORDER BY `created` DESC'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':kid', $kid, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = array(
                'kid' => $row['kid'],
                'created' => $row['created'],
                'empfaenger' => $row['empfaenger'],
                'absender' => $row['absender'],
                'betreff' => $row['betreff'],
                'report' => $row['report'],
                'typ' => $row['typ']
            );
        }

        return $dataArr;
    }
}
?>