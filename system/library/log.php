<?php
class logManager {


	public function __construct() {
			$mandant = $_SESSION['mandant'];
			
			include($_SERVER['DOCUMENT_ROOT'] . '/system/db_connect.inc.php');
			$this->dbh = new PDO('mysql:host='.$dbHostEms.';dbname='.$db_name, $db_user, $db_pw);
	}
	
	
	public function setLog($table,$userID,$actionID,$kID,$nvID,$importNr,$status) {
		
		$qry = ' INSERT INTO '.$table
	          .' (userid, action, kid, nv_id, import_nr, timestamp, status) '
			  .' VALUES '
			  .' ( '.$userID.', '.$actionID.', '.$kID.', '.$nvID.', '.$importNr.', NOW(), '.$status.' ) ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
		
	}
	
	
	public function getLastLogByAction($table,$actionID,$kid) {
		
		$qry = ' SELECT * '
		 	  .' FROM '.$table
	          .' WHERE action = '.$actionID
			  .' AND kid = '.$kid
			  .' ORDER BY timestamp DESC '
			  .' LIMIT 0,1 ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array(
							 'userid' 		=> $row['userid'],
							 'timestamp' 	=> $row['timestamp']
							);
		}
		
		return $dataArr;
		
	}
	
	
	public function getLastLogData($table,$importNr,$actionID) {
		
		$qry = ' SELECT * '
		 	  .' FROM '.$table
	          .' WHERE import_nr = '.$importNr
			  .' AND action = '.$actionID
			  .' ORDER BY timestamp DESC '
			  .' LIMIT 0,1 ';
			  
	    $stmt = $this->dbh->prepare($qry);
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		$dataArr = array();
		
		foreach($result as $row) {
			$dataArr = array(
							 'userid' 		=> $row['userid'],
							 'timestamp' 	=> $row['timestamp'],
							 'status' 		=> $row['status']
							);
		}
		
		return $dataArr;
		
	}
	
}

?>