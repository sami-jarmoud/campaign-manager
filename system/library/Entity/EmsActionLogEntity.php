<?php
class EmsActionLogEntity {
	public $id;
	public $client_id;
	public $user_id;
	public $action_id;
	public $record_id;
	public $record_table;
	
	/**
	 * @var DateTime
	 */
	public $crdate;
	
	public $log_data;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getClient_id() {
		return (int) $this->client_id;
	}
	public function setClient_id($client_id) {
		$this->client_id = \intval($client_id);
	}

	public function getUser_id() {
		return (int) $this->user_id;
	}
	public function setUser_id($user_id) {
		$this->user_id = \intval($user_id);
	}

	public function getAction_id() {
		return (int) $this->action_id;
	}
	public function setAction_id($action_id) {
		$this->action_id = \intval($action_id);
	}

	public function getRecord_id() {
		return (int) $this->record_id;
	}
	public function setRecord_id($record_id) {
		$this->record_id = \intval($record_id);
	}

	public function getRecord_table() {
		return $this->record_table;
	}
	public function setRecord_table($record_table) {
		$this->record_table = $record_table;
	}

	public function getCrdate() {
		return $this->crdate;
	}
	public function setCrdate(\DateTime $crdate) {
		$this->crdate = new \DateTime($crdate);
	}

	public function getLog_data() {
		return $this->log_data;
	}
	public function setLog_data($log_data) {
		$this->log_data = $log_data;
	}

}