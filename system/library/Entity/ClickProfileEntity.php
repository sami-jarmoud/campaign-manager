<?php
class ClickProfileEntity {
	public $id;
	public $title;
	public $shortcut;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}
	
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getShortcut() {
		return $this->shortcut;
	}
	public function setShortcut($shortcut) {
		$this->shortcut = $shortcut;
	}

}