<?php
class DeliverySystemDistributorWidthClientDeliverySystemEntity extends \DeliverySystemDistributorEntity {
	/**
	 * @var \ClientDeliverySystemWidthDeliverySystemEntity
	 */
	public $clientDeliveryEntity;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
  public function getClientDeliveryEntity() {
	  return $this->clientDeliveryEntity;
  }
  public function setClientDeliveryEntity(ClientDeliveryEntity $clientDeliveryEntity) {
	  $this->clientDeliveryEntity = $clientDeliveryEntity;
  }

}