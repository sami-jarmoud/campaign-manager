<?php
require_once(DIR_Entity . 'ClientDeliveryEntity.php');
/**
 * @deprecated use ClientDeliverySystemWidthDeliverySystemEntity
 */

class ClientAndDeliverySystemEntity extends ClientDeliveryEntity {
	public $asp;
	public $asp_abkz;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getAsp() {
		return $this->asp;
	}
	public function setAsp($asp) {
		$this->asp = $asp;
	}

	public function getAsp_abkz() {
		return $this->asp_abkz;
	}
	public function setAsp_abkz($asp_abkz) {
		$this->asp_abkz = $asp_abkz;
	}

}