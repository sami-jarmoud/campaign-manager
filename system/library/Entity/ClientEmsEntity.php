<?php
class ClientEmsEntity {
	public $id;
	public $mandant;
	public $abkz;
	public $active;
	public $dais_client_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getMandant() {
		return $this->mandant;
	}
	public function setMandant($mandant) {
		$this->mandant = $mandant;
	}

	public function getAbkz() {
		return $this->abkz;
	}
	public function setAbkz($abkz) {
		$this->abkz = $abkz;
	}

	public function getActive() {
		return (boolean) $this->active;
	}
	public function setActive($active) {
		$this->active = (boolean) \intval($active);
	}

	public function getDais_client_id() {
		return (int) $this->dais_client_id;
	}
	public function setDais_client_id($dais_client_id) {
		$this->dais_client_id = \intval($dais_client_id);
	}

}