<?php
class ClientEntity {
	public $id;
	public $mandant;
	public $package;
	public $abkz;
	public $unsubkey;
	public $dm_rep_mail;
	public $dm_rep_betreff;
	public $untermandant;
	public $cm_rep_mail;
	public $cm_rep_betreff;
	public $cm_zustellung_betreff;
	public $cm_zustellung_mail;
	public $cm_rechnung_betreff;
	public $cm_rechnung_mail;
	public $crm_hash;
	public $intone_group;
	public $broadmail_import;
	public $client_base_path;
	public $has_multiple_distributors;
	public $parent_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getMandant() {
		return $this->mandant;
	}
	public function setMandant($mandant) {
		$this->mandant = $mandant;
	}

	public function getPackage() {
		return $this->package;
	}
	public function setPackage($package) {
		$this->package = $package;
	}

	public function getAbkz() {
		return $this->abkz;
	}
	public function setAbkz($abkz) {
		$this->abkz = $abkz;
	}

	public function getUnsubkey() {
		return $this->unsubkey;
	}
	public function setUnsubkey($unsubkey) {
		$this->unsubkey = $unsubkey;
	}

	public function getDm_rep_mail() {
		return $this->dm_rep_mail;
	}
	public function setDm_rep_mail($dm_rep_mail) {
		$this->dm_rep_mail = $dm_rep_mail;
	}

	public function getDm_rep_betreff() {
		return $this->dm_rep_betreff;
	}
	public function setDm_rep_betreff($dm_rep_betreff) {
		$this->dm_rep_betreff = $dm_rep_betreff;
	}

	public function getUntermandant() {
		return $this->untermandant;
	}
	public function setUntermandant($untermandant) {
		$this->untermandant = $untermandant;
	}

	public function getCm_rep_mail() {
		return $this->cm_rep_mail;
	}
	public function setCm_rep_mail($cm_rep_mail) {
		$this->cm_rep_mail = $cm_rep_mail;
	}

	public function getCm_rep_betreff() {
		return $this->cm_rep_betreff;
	}
	public function setCm_rep_betreff($cm_rep_betreff) {
		$this->cm_rep_betreff = $cm_rep_betreff;
	}

	public function getCm_zustellung_betreff() {
		return $this->cm_zustellung_betreff;
	}
	public function setCm_zustellung_betreff($cm_zustellung_betreff) {
		$this->cm_zustellung_betreff = $cm_zustellung_betreff;
	}

	public function getCm_zustellung_mail() {
		return $this->cm_zustellung_mail;
	}
	public function setCm_zustellung_mail($cm_zustellung_mail) {
		$this->cm_zustellung_mail = $cm_zustellung_mail;
	}

	public function getCm_rechnung_betreff() {
		return $this->cm_rechnung_betreff;
	}
	public function setCm_rechnung_betreff($cm_rechnung_betreff) {
		$this->cm_rechnung_betreff = $cm_rechnung_betreff;
	}

	public function getCm_rechnung_mail() {
		return $this->cm_rechnung_mail;
	}
	public function setCm_rechnung_mail($cm_rechnung_mail) {
		$this->cm_rechnung_mail = $cm_rechnung_mail;
	}

	public function getCrm_hash() {
		return $this->crm_hash;
	}
	public function setCrm_hash($crm_hash) {
		$this->crm_hash = $crm_hash;
	}

	public function getIntone_group() {
		return (boolean) $this->intone_group;
	}
	public function setIntone_group($intone_group) {
		$this->intone_group = (boolean) \intval($intone_group);
	}

	public function getBroadmail_import() {
		return (boolean) $this->broadmail_import;
	}
	public function setBroadmail_import($broadmail_import) {
		$this->broadmail_import = (boolean) \intval($broadmail_import);
	}

	public function getClient_base_path() {
		return $this->client_base_path;
	}
	public function setClient_base_path($client_base_path) {
		$this->client_base_path = $client_base_path;
	}

	public function getHas_multiple_distributors() {
		return (boolean) $this->has_multiple_distributors;
	}
	public function setHas_multiple_distributors($has_multiple_distributors) {
		$this->has_multiple_distributors = (boolean) \intval($has_multiple_distributors);
	}

	public function getParent_id() {
		return (int) $this->parent_id;
	}
	public function setParent_id($parent_id) {
		$this->parent_id = \intval($parent_id);
	}


}