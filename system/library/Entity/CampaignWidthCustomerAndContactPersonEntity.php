<?php
require_once(DIR_Entity . 'CampaignEntity.php');

class CampaignWidthCustomerAndContactPersonEntity extends CampaignEntity {
	/**
	 * @var CustomerEntity
	 */
	public $customerEntity;

	/**
	 * @var ContactPersonEntity
	 */
	public $contactPersonEntity;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getCustomerEntity() {
		return $this->customerEntity;
	}
	public function setCustomerEntity($customerEntity) {
		$this->customerEntity = $customerEntity;
	}

	public function getContactPersonEntity() {
		return $this->contactPersonEntity;
	}
	public function setContactPersonEntity($contactPersonEntity) {
		$this->contactPersonEntity = $contactPersonEntity;
	}

}