<?php
class ClientDeliverySystemWidthDeliverySystemEntity extends \ClientDeliveryEntity {
	/**
	 * @var \DeliverySystemEntity
	 */
	public $deliverySystem;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
  public function getDeliverySystem() {
	  return $this->deliverySystem;
  }
  public function setDeliverySystem(\DeliverySystemEntity $deliverySystem) {
	  $this->deliverySystem = $deliverySystem;
  }

}