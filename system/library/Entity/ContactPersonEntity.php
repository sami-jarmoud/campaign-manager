<?php
class ContactPersonEntity {
	public $ap_id;
	public $kunde_id;
	public $anrede;
	public $titel;
	public $vorname;
	public $nachname;
	public $email;
	public $telefon;
	public $fax;
	public $mobil;
	public $position;
	public $status;
	public $vertriebler_id;
	
	// TODO: entfernen
	public $crm_ap_id;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getAp_id() {
		return (int) $this->ap_id;
	}
	public function setAp_id($ap_id) {
		$this->ap_id = \intval($ap_id);
	}

	public function getKunde_id() {
		return (int) $this->kunde_id;
	}
	public function setKunde_id($kunde_id) {
		$this->kunde_id = \intval($kunde_id);
	}

	public function getAnrede() {
		return $this->anrede;
	}
	public function setAnrede($anrede) {
		$this->anrede = $anrede;
	}

	public function getTitel() {
		return $this->titel;
	}
	public function setTitel($titel) {
		$this->titel = $titel;
	}

	public function getVorname() {
		return $this->vorname;
	}
	public function setVorname($vorname) {
		$this->vorname = $vorname;
	}

	public function getNachname() {
		return $this->nachname;
	}
	public function setNachname($nachname) {
		$this->nachname = $nachname;
	}

	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}

	public function getTelefon() {
		return $this->telefon;
	}
	public function setTelefon($telefon) {
		$this->telefon = $telefon;
	}

	public function getFax() {
		return $this->fax;
	}
	public function setFax($fax) {
		$this->fax = $fax;
	}

	public function getMobil() {
		return $this->mobil;
	}
	public function setMobil($mobil) {
		$this->mobil = $mobil;
	}

	public function getPosition() {
		return $this->position;
	}
	public function setPosition($position) {
		$this->position = $position;
	}

	public function getStatus() {
		return (int) $this->status;
	}
	public function setStatus($status) {
		$this->status = \intval($status);
	}

	public function getVertriebler_id() {
		return (int) $this->vertriebler_id;
	}
	public function setVertriebler_id($vertriebler_id) {
		$this->vertriebler_id = \intval($vertriebler_id);
	}

	// TODO: remove
	public function getCrm_ap_id() {
		return $this->crm_ap_id;
	}
	public function setCrm_ap_id($crm_ap_id) {
		$this->crm_ap_id = $crm_ap_id;
	}

}