<?php
class DeliverySystemEntity {
	public $id;
	public $asp;
	public $asp_abkz;
	
	
	
	
	
	/********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}

	public function getAsp() {
		return $this->asp;
	}
	public function setAsp($asp) {
		$this->asp = $asp;
	}

	public function getAsp_abkz() {
		return $this->asp_abkz;
	}
	public function setAsp_abkz($asp_abkz) {
		$this->asp_abkz = $asp_abkz;
	}

}