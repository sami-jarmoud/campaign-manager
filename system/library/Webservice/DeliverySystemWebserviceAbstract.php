<?php
require_once(DIR_Webservice . 'DeliverySystemWebserviceInterface.php');

abstract class DeliverySystemWebserviceAbstract implements DeliverySystemWebserviceInterface {
	protected $mLogin;
	protected $apiUser;
	protected $pw;
	protected $sharedKey;
	protected $secretKey;
	
	
	
	
	/**
	 * getMailingStatusDataArray
	 * 
	 * @return array
	 */
	public static function getMailingStatusDataArray() {
		return static::$mailingStatusDataArray;
	}
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getMLogin() {
		return $this->mLogin;
	}
	public function setMLogin($mLogin) {
		$this->mLogin = $mLogin;
	}

	public function getApiUser() {
		return $this->apiUser;
	}
	public function setApiUser($apiUser) {
		$this->apiUser = $apiUser;
	}

	public function getPw() {
		return $this->pw;
	}
	public function setPw($pw) {
		$this->pw = $pw;
	}
	
	public function getSharedKey() {
		return $this->sharedKey;
	}
	public function setSharedKey($sharedKey) {
		$this->sharedKey = $sharedKey;
	}

	public function getSecretKey() {
		return $this->secretKey;
	}
	public function setSecretKey($secretKey) {
		$this->secretKey = $secretKey;
	}

}