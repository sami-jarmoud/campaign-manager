<?php
require_once(\DIR_Webservice . 'DeliverySystemWebserviceAbstract.php');

/**
 * Description of SendeffectWebservice
 *
 * @author SamiMohamedJarmoud
 */
 class SendeffectWebservice extends \DeliverySystemWebserviceAbstract{

	protected static $mailingStatusDataArray = array(
	        1 => 'NEW',
		2 => 'SENDING',
		3 => 'DONE',
		4 => 'CANCELED',
		100 => 'Alle'
	);
        
        /**
         * sendeffectClient
         * 
         * @var \sendeffectClient|NULL
         */
        protected $sendeffectClient = NULL;  
        
	protected $curlHandle;
        
     	/**
	 * login
	 * 
	 * @return void
	 */
	public function login() {
		try {
			$this->setCurlHandle(\curl_init());
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * logout
	 * 
	 * @return void
	 */
	public function logout() {
		\curl_close($this->getCurlHandle());
	}


	/**
	 * getMailingData
	 * 
	 * @param mixed $mailingId
	 * @return \SimpleXMLElement
	 * 
	 * @throws \DomainException
	 */
	public function getMailingData($mailingId) {

		try {

            require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                    $curlResult = $this->sendeffectClient->getPerformance(
                            $mailingId
                            );

			return $curlResult;
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

        
        
	/**
	 * deleteMailing
	 * 
	 * @param integer $mailingId
	 * @return boolean
	 */
        public function deleteMailing($mailingId,$external_job){
            $result = FALSE;
            
            if(\strlen($mailingId) > 0){
                try{
                    require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                  if((\strlen($external_job) > 1) &&
                          !\is_null($external_job)
                    ){
                 //loeschen job id
                 $this->sendeffectClient->actionJob(
                                $external_job, 
                               'pausejob'
                            );  
               $this->sendeffectClient->actionJob(
                                $external_job, 
                               'deletejob'
                            );   
              }
      
                    $this->sendeffectClient->delete(
                            $mailingId
                            );
                    
                    $result = TRUE;
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
            
           return $result; 
        }     
        
        /**
	 * GetListSubscribers
	 * 
	 * @param integer $recipientId
	 * @return boolean
	 */
        public function GetListSubscribers($recipientId){
            
            if(\strlen($recipientId) > 0){
                try{
                    
                    require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                    $result = $this->sendeffectClient->GetListSubscribers(
                            $recipientId
                            );
                                       
                } catch (Exception $ex) {
                    throw $ex;
                }
            }
            
           return $result; 
        }
       
          /**
	 * SendTestMail
	 * 
         * @param string $mailingId
	 * @param integer $recipientListId
         * @param string $absendername
         * @param string $email
	 * @return boolean
	 */
        public function SendTestMail($mailingId,$recipientListId,$absendername,$email,$sendermail,$replytomail){
            $result = FALSE;
            
                try{                   
                    require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                   $this->sendeffectClient->sendTestMail(
                     $mailingId, 
                     '', 
                     $recipientListId, 
                     '', 
                     $absendername,
                     $email,
                     $sendermail,
                     $replytomail           
                     );
                    
                    $result = TRUE;
                } catch (Exception $ex) {
                    throw $ex;
                }
           
            
           return $result; 
        }
        
         /**
	 * AddTestMail
	 * 
         * @param string $email
	 * @param integer $recipientId
         * @param string $url
         * @param array $customfields
	 * @return boolean
	 */
        public function AddTestMail($email,$recipientId,$url,$customfields){

                try{                   
                    require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                  $result = $this->sendeffectClient->AddSubscriberToList(         
                     $email,
                     $recipientId,
                     $url,
                     $customfields
                     );
                    
                    
                } catch (Exception $ex) {
                    throw $ex;
                }
           
            
           return $result; 
        }
        
        /**
	 * ActivateSubscriber
	 * 
         * @param string $email
	 * @param integer $recipientId
         * @param string $url
	 * @return boolean
	 */
        public function ActivateSubscriber($email, $recipientId, $url) {

                try{                   
                    require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                  $result = $this->sendeffectClient->ActivateSubscriber(
                          $email, 
                          $recipientId, 
                          $url
                          );
                    
                    
                } catch (Exception $ex) {
                    throw $ex;
                }
           
            
           return $result; 
        }
        
        
        /**
	 * getMailingData
	 * 
	 * @param mixed $mailingId
	 * @return \SimpleXMLElement
	 * 
	 * @throws \DomainException
	 */
	public function getHTMLContent($mailingId) {

		try {

            require_once(DIR_packages . 'sendeffect/api/seCurl.php');
                    $this->initApiClient();
                     
                    $curlResult = $this->sendeffectClient->getHTMLContent(
                            $mailingId
                            );

			return $curlResult;
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}
        
    protected function initApiClient() {
        if (\is_null($this->sendeffectClient)) {            
            //create client instance
            $this->sendeffectClient = new \seCurl(
                    $this->apiUser, 
                    $this->pw,
                    $this->mLogin,
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
            );
            
        }
    }        
        
        
    /********************************************************************************************
     *
     *              setter and getter - Functions
     *
     *******************************************************************************************/
	public function getCurlHandle() {
		return $this->curlHandle;
	}
	public function setCurlHandle($curlHandle) {
		$this->curlHandle = $curlHandle;
	}        
    
}
