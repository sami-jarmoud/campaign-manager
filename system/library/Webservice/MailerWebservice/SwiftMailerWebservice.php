<?php
require_once(DIR_Webservice . 'MailerWebserviceAbstract.php');
require_once(DIR_Vendor . 'swiftmailer-5.4.1/lib/swift_required.php');

class SwiftMailerWebservice extends MailerWebserviceAbstract {
	protected $transportTyp = 'mail';

	/**
	 * @var Swift_Mailer
	 */
	protected $mailer;
	protected $transport = null;
	protected $failedRecipients = array();
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init() {
		// set global charset
		\Swift_Preferences::getInstance()->setCharset($this->getCharset());

		/**
		 * createTransport
		 */
		$this->createTransport();

		// Create the Mailer using your created Transport
		$this->setMailer(\Swift_Mailer::newInstance($this->getTransport()));
	}

	/**
	 * sendMessage
	 * 
	 * @param \Swift_Message $message
	 * @return integer
	 */
	public function sendMessage($message) {
		$failedRecipients = array();
		
		$result = $this->getMailer()->send(
			$message,
			$failedRecipients
		);
		
		$this->failedRecipients = $failedRecipients;
		
		return $result;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              helpers - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * createTransport
	 * 
	 * @return void
	 */
	protected function createTransport() {
		if ($this->getTransport() === null) {
			try {
				switch ($this->getTransportTyp()) {
					case 'smtp':
						$this->setTransport(
							\Swift_SmtpTransport::newInstance(
								$this->getSmtpHost(),
								$this->getSmtpPort(),
								$this->encryption
							)
								->setUsername($this->getSmtpUsername())
								->setPassword($this->getSmtpPassword())
						);
						break;

					case 'sendmail':
						$this->setTransport(\Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -t'));
						break;

					default:
						$this->setTransport(\Swift_MailTransport::newInstance());
						break;
				}

				if (\strlen($this->getServerName()) > 0) {
					$this->getTransport()->setLocalDomain($this->getServerName());
				}
			} catch (\Swift_TransportException $e) {
				\DebugAndExceptionUtils::sendDebugData(__CLASS__ . ' -> ' . __FUNCTION__ . ': CreateTransport Fehler: ' . $e->getMessage());
			}
		}
	}

	/**
	 * createMessage
	 * 
	 * @return Swift_Message
	 */
	public function createMessage() {
		$message = \Swift_Message::newInstance();
		/* @var $message Swift_Message */

		// set Message ID
		if (\strlen($this->getServerName()) > 0) {
			$message->setId(\time() . '.' . \uniqid() . '@' . $this->getServerName());
		}

		// setFrom
		$message->setFrom(
			$this->getFromEmail(),
			$this->getFromName()
		);

		// setSender
		$message->setSender(
			$this->getFromEmail(),
			$this->getFromName()
		);

		return $message;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getTransportTyp() {
		return $this->transportTyp;
	}
	public function setTransportTyp($transportTyp) {
		$this->transportTyp = $transportTyp;
	}

	public function getMailer() {
		return $this->mailer;
	}
	public function setMailer(Swift_Mailer $mailer) {
		$this->mailer = $mailer;
	}

	public function getTransport() {
		return $this->transport;
	}
	public function setTransport($transport) {
		$this->transport = $transport;
	}

	public function getFailedRecipients() {
		return $this->failedRecipients;
	}
	public function setFailedRecipients($failedRecipients) {
		$this->failedRecipients = $failedRecipients;
	}

}