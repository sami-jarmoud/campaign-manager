<?php
require_once(DIR_Webservice . 'MailerWebserviceInterface.php');

abstract class MailerWebserviceAbstract implements MailerWebserviceInterface {
	protected $smtpHost;
	protected $smtpUsername;
	protected $smtpPassword;
	protected $smtpPort = 25;
	protected $contentType = 'text/plain';
	protected $charset = 'utf-8';
	protected $fromEmail;
	protected $fromName;
	protected $serverName = '';
	protected $encryption = null;
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getSmtpPort() {
		return (int) $this->smtpPort;
	}
	public function setSmtpPort($smtpPort) {
		$this->smtpPort = intval($smtpPort);
	}

	public function getSmtpHost() {
		return $this->smtpHost;
	}
	public function setSmtpHost($smtpHost) {
		$this->smtpHost = $smtpHost;
	}

	public function getSmtpUsername() {
		return $this->smtpUsername;
	}
	public function setSmtpUsername($smtpUsername) {
		$this->smtpUsername = $smtpUsername;
	}

	public function getSmtpPassword() {
		return $this->smtpPassword;
	}
	public function setSmtpPassword($smtpPassword) {
		$this->smtpPassword = $smtpPassword;
	}

	public function getContentType() {
		return $this->contentType;
	}
	public function setContentType($contentType) {
		$this->contentType = $contentType;
	}

	public function getCharset() {
		return $this->charset;
	}
	public function setCharset($charset) {
		$this->charset = $charset;
	}

	public function getFromEmail() {
		return $this->fromEmail;
	}
	public function setFromEmail($fromEmail) {
		$this->fromEmail = $fromEmail;
	}

	public function getFromName() {
		return $this->fromName;
	}
	public function setFromName($fromName) {
		$this->fromName = $fromName;
	}

	public function getServerName() {
		return $this->serverName;
	}
	public function setServerName($serverName) {
		$this->serverName = $serverName;
	}
	
	public function getEncryption() {
		return $this->encryption;
	}
	public function setEncryption($encryption) {
		$this->encryption = $encryption;
	}


}