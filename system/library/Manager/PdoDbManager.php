<?php
// DebugAndExceptionUtils
require_once(DIR_Utils . 'DebugAndExceptionUtils.php');

abstract class PdoDbManager {
	static public $fieldEqual = ' = ';
	static public $fieldNotEqual = ' != ';
	static public $fieldIn = ' IN ';
	static public $fieldNotIn = ' NOT IN ';
	static public $fieldGreaterThan = ' > ';
	static public $fieldLessThan = ' < ';
	static public $fieldGreaterOrEqualThan = ' >= ';
	static public $fieldLessOrEqualThan = ' <= ';
	
	protected $host;
	protected $dbName;
	protected $dbUser;
	protected $dbPassword;
	protected $pdoDebug = false;
	protected $statementsDebug = false;

	/**
	 * @var PDO 
	 */
	protected $dbh = null;
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return  void
	 */
	public function init() {
		try {
			$this->setDbh(
				new \PDO(
					'mysql:host=' . $this->getHost() . ';dbname=' . $this->getDbName(),
					$this->getDbUser(),
					$this->getDbPassword(),
					array(
						\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
					)
				)
			);
			$this->getDbh()->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {
			if ($this->getPdoDebug() === true) {
				throw new \Exception(
					\nl2br(
						$e->getMessage() . chr(13) 
						. $e->getCode()
					),
					1424432922
				);
			} else {
				throw new \Exception('connection Error!', 1424432963);
			}
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              debug - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * debugPdoStatement
	 * 
	 * @param \PDOStatement $stmt
	 * @param array $debugDataArray
	 * @return string
	 */
	protected function debugPdoStatement(\PDOStatement &$stmt, array $debugDataArray = array()) {
		if ((boolean) $this->getStatementsDebug() === true) {
			$stmt->debugDumpParams();

			if (\count($debugDataArray) > 0) {
				/**
				 * showDebugData
				 */
				\DebugAndExceptionUtils::showDebugData(
					$debugDataArray,
					__FUNCTION__
				);
			}
		}
	}

	/**
	 * sendDebugDataToEmail
	 * 
	 * @param string $functionName
	 * @param array $debugDataArray
	 * @return void
	 */
	protected function sendDebugDataToEmail($functionName, array $debugDataArray) {
		if ((boolean) $this->getStatementsDebug() === true) {
			\DebugAndExceptionUtils::sendDebugData(
				$debugDataArray,
				$functionName
			);
		}
	}

	/**
	 * debugPdoException
	 * 
	 * @param \PDOException $e
	 * @param string $qry
	 * @param array $debugDataArray
	 * @return string
	 */
	protected function debugPdoException(\PDOException $e, $qry, array $debugDataArray = array()) {
		/**
		 * sendDebugData
		 */
		// TODO: trace implementieren
		\DebugAndExceptionUtils::sendDebugData(
			array(
				'qry' => $qry,
				'debugDataArray' => $debugDataArray,
				'client' => $_SESSION['mandant'],
				'clientId' => $_SESSION['mID'],
				'userId' => $_SESSION['benutzer_id']
			),
			__CLASS__ . ' -> ' . __FUNCTION__
		);

		if ($this->getPdoDebug() === true) {
			$result = \nl2br(
				$qry . \chr(11)
				. $e->getMessage()
			);
		} else {
			$result = 'Sql Error';
		}
		
		die($result);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              helper - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * createPdoInString
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	protected function createPdoInString(array $dataArray) {
		$itemHoldersDataArray = array();
		foreach ($dataArray as $item) {
			$itemHoldersDataArray[] = ':' . $item;
		}

		return \implode(',', $itemHoldersDataArray);
	}

	/**
	 * processBindWhereParameter
	 * 
	 * @param array $queryPartsWhereDataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindWhereParameter(array $queryPartsWhereDataArray, \PDOStatement &$stmt) {
		foreach ($queryPartsWhereDataArray as $key => &$item) {
			if (\strlen($item['value']) > 0) {
				switch ($item['comparison']) {
					case 'LIKE BINARY':
						$string = '%' . $item['value'] . '%';
						$stmt->bindParam(':' . $key, $string, \PDO::PARAM_STR);
						break;

					case 'fieldEqual':
					case 'fieldNotEqual':
						// do nothings
						break;

					case 'integerEqual':
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_INT);
						break;

					default:
						$stmt->bindParam(':' . $key, $item['value'], \PDO::PARAM_STR);
						break;
				}
			}
		}
	}

	/**
	 * addWhereQry
	 * 
	 * @param array $queryPartsWhereDataArray
	 * @return string
	 */
	protected function addWhereQry(array $queryPartsWhereDataArray) {
		$addQuery = '';
		foreach ($queryPartsWhereDataArray as $key => $itemDataArray) {
			if (\strlen($itemDataArray['value']) > 0) {
				switch ($itemDataArray['comparison']) {
					case 'fieldEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . \trim($itemDataArray['value']);
						break;

					case 'integerEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldEqual . ':' . $key;
						break;

					case 'fieldNotEqual':
						$addQuery .= ' AND ' . $itemDataArray['sql'] . self::$fieldNotEqual . \trim($itemDataArray['value']);
						break;

					default:
						$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' :' . \trim($key);
						break;
				}
			} else {
				if ($itemDataArray['comparison'] == '<>') {
					$addQuery .= ' AND ' . $itemDataArray['sql'] . ' ' . $itemDataArray['comparison'] . ' ""';
				} elseif (\strlen($itemDataArray['comparison']) === 0) {
					$addQuery .= ' AND ' . $itemDataArray['sql'];
				}
			}
		}

		return $addQuery;
	}

	/**
	 * addUpdateQueryFields
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	protected function addUpdateQueryFields(array $dataArray) {
		$addQuery = array();
		foreach ($dataArray as $field => $item) {
			$addQuery[] = $field . ' = :' . $field;
		}

		return \implode(', ', $addQuery);
	}

	/**
	 * processInsertQueryFieldsAndValues
	 * 
	 * @param array $dataArray
	 * @return string
	 */
	protected function processInsertQueryFieldsAndValues(array $dataArray) {
		$queryFields = $queryFieldsValue = array();
		foreach ($dataArray as $field => $item) {
			$queryFields[] = $field;
			$queryFieldsValue[] = ':' . $field;
		}

		return ' (' . \implode(', ', $queryFields) . ')'
			. ' VALUES (' . \implode(', ', $queryFieldsValue) . ')'
		;
	}

	/**
	 * processBindSetParameter
	 * 
	 * @param array $dataArray
	 * @param \PDOStatement $stmt
	 * @return void
	 */
	protected function processBindSetParameter(array $dataArray, \PDOStatement &$stmt) {
		foreach ($dataArray as $field => &$item) {
			$stmt->bindParam(':' . $field, $item['value'], $item['dataType']);
		}
	}

	/**
	 * getSelectFieldsQry
	 * 
	 * @param array $queryPartsSelectDataArray
	 * @return string
	 */
	protected function getSelectFieldsQry(array $queryPartsSelectDataArray) {
		$selectFields = '';
		foreach ($queryPartsSelectDataArray as $key => $item) {
			$selectFields .= $item . ' as `' . $key . '`, ';
		}

		return \rtrim($selectFields, ', ');
	}

	/**
	 * processDataArrayByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param string $table
	 * @param string $addWhere
	 * @return false|\PDOStatement
	 */
	protected function processDataArrayByQueryParts(array $queryPartsDataArray, $table, $addWhere = '') {
		$selectFields = '*';
		if (isset($queryPartsDataArray['SELECT'])) {
			if (\is_array($queryPartsDataArray['SELECT']) 
				&& \count($queryPartsDataArray['SELECT']) > 0
			) {
				$selectFields = $this->getSelectFieldsQry($queryPartsDataArray['SELECT']);
			} else {
				$selectFields = $queryPartsDataArray['SELECT'];
			}
		}

		$addWhereQuery = '';
		if (isset($queryPartsDataArray['WHERE']) 
			&& \count($queryPartsDataArray['WHERE']) > 0
		) {
			$addWhereQuery = $this->addWhereQry($queryPartsDataArray['WHERE']);
		}

		$qry = 'SELECT ' . $selectFields
			. ' FROM ' . $table
			. ' WHERE 1 = 1'
				. $addWhereQuery
				. $addWhere
			. (isset($queryPartsDataArray['GROUP_BY']) ? ' GROUP BY ' . $queryPartsDataArray['GROUP_BY'] : '')
			. (isset($queryPartsDataArray['ORDER_BY']) ? ' ORDER BY ' . $queryPartsDataArray['ORDER_BY'] : '')
			. (isset($queryPartsDataArray['LIMIT']) ? ' LIMIT ' . $queryPartsDataArray['LIMIT'] : '')
		;

		$debugDataArray = array(
			'queryPartsDataArray' => $queryPartsDataArray
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);

			if (isset($queryPartsDataArray['WHERE']) 
				&& \count($queryPartsDataArray['WHERE']) > 0
			) {
				$this->processBindWhereParameter(
					$queryPartsDataArray['WHERE'],
					$stmt
				);
			}

			$stmt->execute();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt;
	}

	/**
	 * processInsertItemByDataArray
	 * 
	 * @param string $qry
	 * @param array $dataArray
	 * @return boolean|integer
	 */
	protected function processInsertItemByDataArray($qry, array $dataArray) {
		try {
			$this->getDbh()->beginTransaction();

			$stmt = $this->getDbh()->prepare($qry);

			// processBindSetParameter
			$this->processBindSetParameter(
				$dataArray,
				$stmt
			);

			$stmt->execute();

			$result = $this->getDbh()->lastInsertId();

			$this->getDbh()->commit();

			// debugPdoStatement
			$this->debugPdoStatement(
				$stmt,
				$dataArray
			);

			// sendDebugDataToEmail
			$this->sendDebugDataToEmail(
				__CLASS__ . ' -> ' . __FUNCTION__,
				array(
					'query' => $qry,
					'dataArray' => $dataArray
				)
			);
		} catch (\PDOException $e) {
			$result = false;
			$this->getDbh()->rollBack();

			$this->debugPdoException(
				$e,
				$qry,
				$dataArray
			);
		}

		return $result;
	}
	
	/**
	 * processUpdateItemByDataArray
	 * 
	 * @param string $qry
	 * @param array $dataArray
	 * @return boolean
	 */
	protected function processUpdateItemByDataArray($qry, array $dataArray) {
		try {
			$this->getDbh()->beginTransaction();

			$stmt = $this->getDbh()->prepare($qry);

			// processBindSetParameter
			$this->processBindSetParameter(
				$dataArray,
				$stmt
			);

			$stmt->execute();

			$this->getDbh()->commit();

			// debugPdoStatement
			$this->debugPdoStatement(
				$stmt,
				$dataArray
			);

			// sendDebugDataToEmail
			$this->sendDebugDataToEmail(
				__CLASS__ . ' -> ' . __FUNCTION__,
				array(
					'query' => $qry,
					'dataArray' => $dataArray
				)
			);
			
			$result = true;
		} catch (\PDOException $e) {
			$result = false;
			$this->getDbh()->rollBack();

			$this->debugPdoException(
				$e,
				$qry,
				$dataArray
			);
		}

		return $result;
	}
	
	/**
	 * processDeleteItemByDataArray
	 * 
	 * @param string $qry
	 * @param array $dataArray
	 * @return boolean
	 */
	protected function processDeleteItemByDataArray($qry, array $dataArray) {
		try {
			$this->getDbh()->beginTransaction();

			$stmt = $this->getDbh()->prepare($qry);

			// processBindSetParameter
			$this->processBindSetParameter(
				$dataArray,
				$stmt
			);

			$stmt->execute();

			$this->getDbh()->commit();

			// debugPdoStatement
			$this->debugPdoStatement(
				$stmt,
				$dataArray
			);

			// sendDebugDataToEmail
			$this->sendDebugDataToEmail(
				__CLASS__ . ' -> ' . __FUNCTION__,
				array(
					'query' => $qry,
					'dataArray' => $dataArray
				)
			);
			
			$result = true;
		} catch (\PDOException $e) {
			$result = false;
			$this->getDbh()->rollBack();

			$this->debugPdoException(
				$e,
				$qry,
				$dataArray
			);
		}

		return $result;
	}
	
	
	/**
	 * fetchStmt
	 * 
	 * @param \PDOStatement $stmt
	 * @param string $fetchClass
	 * @param boolean $throwException
	 * @return mixed
	 * @throws \DomainException
	 */
	protected function fetchStmt(\PDOStatement &$stmt, $fetchClass, $throwException = true) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $fetchClass);
		
		if (($row = $stmt->fetch()) !== false) {
			$stmt->closeCursor();
			
			return $row;
		} else {
			if ((boolean) $throwException) {
				throw new \DomainException('no ' . $fetchClass, 1424433041);
			}
		}
	}
	
	/**
	 * fetchAllStmt
	 * 
	 * @param \PDOStatement $stmt
	 * @param type $fetchClass
	 * @return false|array
	 * @throws \DomainException
	 */
	protected function fetchAllStmt(\PDOStatement &$stmt, $fetchClass) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $fetchClass);
		
		if (($resultDataArray = $stmt->fetchAll()) !== false) {
			$stmt->closeCursor();
			
			return $resultDataArray;
		} else {
			throw new \DomainException('no ' . $fetchClass, 1424433053);
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getHost() {
		return $this->host;
	}
	public function setHost($host) {
		$this->host = $host;
	}

	public function getDbName() {
		return $this->dbName;
	}
	public function setDbName($dbName) {
		$this->dbName = $dbName;
	}

	public function getDbUser() {
		return $this->dbUser;
	}
	public function setDbUser($dbUser) {
		$this->dbUser = $dbUser;
	}

	public function getDbPassword() {
		return $this->dbPassword;
	}
	public function setDbPassword($dbPassword) {
		$this->dbPassword = $dbPassword;
	}

	public function getPdoDebug() {
		return (boolean) $this->pdoDebug;
	}
	public function setPdoDebug($pdoDebug) {
		$this->pdoDebug = (boolean) $pdoDebug;
	}

	public function getStatementsDebug() {
		return (boolean) $this->statementsDebug;
	}
	public function setStatementsDebug($statementsDebug) {
		$this->statementsDebug = (boolean) $statementsDebug;
	}

	public function getDbh() {
		return $this->dbh;
	}
	public function setDbh($dbh) {
		$this->dbh = $dbh;
	}

}