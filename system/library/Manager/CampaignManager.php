<?php
require_once(DIR_Manager . 'PdoDbManager.php');

// Entity
require_once(DIR_Entity . 'CampaignEntity.php');
require_once(DIR_Entity . 'CustomerEntity.php');
require_once(DIR_Entity . 'ContactPersonEntity.php');
require_once(DIR_Entity . 'CampaignWidthCustomerAndContactPersonEntity.php');
require_once(DIR_Entity . 'ActionLogEntity.php');

class CampaignManager extends \PdoDbManager {
	protected $campaignTable;
	protected $customerTable;
	protected $contactPersonTable;
	protected $logTable;
	protected $campaignFetchClass = 'CampaignEntity';
	protected $customerFetchClass = 'CustomerEntity';
	protected $contactPersonFetchClass = 'ContactPersonEntity';
	protected $logFetchClass = 'ActionLogEntity';
	
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              campaign - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getCampaignDataById
	 * 
	 * @param integer $kid
	 * @return false|CampaignEntity
	 */
	public function getCampaignDataItemById($kid) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'kId' => array(
						'sql' => '`k_id`',
						'value' => $kid,
						'comparison' => 'integerEqual'
					),
				),
			),
			$this->getCampaignTable(),
			''
		);
		
		return $this->processCampaignStmp($stmt);
	}

	/**
	 * getCampaignAndCustomerDataItemById
	 * 
	 * @param integer $kid
	 * @return false|CampaignWidthCustomerAndContactPersonEntity
	 * 
	 * @throws \DomainException
	 */
	public function getCampaignAndCustomerDataItemById($kid) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'kId' => array(
						'sql' => '`k_id`',
						'value' => $kid,
						'comparison' => 'integerEqual'
					)
				)
			),
			$this->getCampaignTable(),
			''
		);

		return $this->processCampaignAndCustomerStmt($stmt);
	}

	/**
	 * getCampaignsDataItemsByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array
	 */
	public function getCampaignsDataItemsByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1'
		);

		return ($fetchMode === \PDO::FETCH_CLASS ? $this->processCampaignsStmp($stmt) : $stmt->fetchAll($fetchMode));
	}

	/**
	 * getCampaignDataItemByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array|CampaignEntity
	 * 
	 * @throws \DomainException
	 */
	public function getCampaignDataItemByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1'
		);

		return ($fetchMode === \PDO::FETCH_CLASS ? $this->processCampaignStmp($stmt) : $stmt->fetch($fetchMode));
	}

	/**
	 * getCampaignsCountByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @return false|integer
	 */
	public function getCampaignsCountByQueryParts(array $queryPartsDataArray) {
		$queryPartsDataArray['SELECT'] = array(
			'count' => 'COUNT(`k_id`)'
		);
		unset(
			$queryPartsDataArray['ORDER_BY'],
			$queryPartsDataArray['LIMIT']
		);

		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1'
		);

		return $stmt->fetchColumn();
	}

	/**
	 * getLatestCampaignId
	 * 
	 * @return integer
	 */
	public function getLatestCampaignId() {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'SELECT' => array(
					'k_id' => 'k_id'
				),
				'ORDER_BY' => '`k_id` DESC'
			),
			$this->getCampaignTable()
		);
		
		$campaignEntity = $this->processCampaignStmp($stmt);
		/* @var $campaignEntity \CampaignEntity */

		return $campaignEntity->getK_id();
	}
	
	/**
	 * getMailCountByMailingId
	 * 
	 * @return integer
	 */
	public function getMailCountByMailingId($mailingId) {	
		return $this->getCampaignsCountByQueryParts(
			array(
				'WHERE' => array(
					'mail_id' => array(
						'sql' => '`mail_id`',
						'value' => $mailingId,
						'comparison' => 'integerEqual'
					)
				)
			)
		);
		
	}
        
        /**
         * getSyncCampaignsByMailingId
         * 
         * @return array
         */
        public function getSyncCampaignsByMailingId($mailingId){
            $queryPartsDataArray = array(
				'WHERE' => array(
					'mail_id' => array(
						'sql' => '`mail_id`',
						'value' => $mailingId,
						'comparison' => 'integerEqual'
					)
				),
                               'ORDER_BY' => '`k_id` ASC'
			);
            $stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1'
		);

		return $this->processCampaignsStmp($stmt);;
        }
	
	/**
	 * getAllCampaignsDataItemsByKid
	 * 
	 * @param integer $kid
	 * @param array $addWhereQueryPartsDataArray
	 * @param boolean $getCustomerDataItem
	 * @return array
	 */
	public function getAllCampaignsDataItemsByKid($kid, array $addWhereQueryPartsDataArray, $getCustomerDataItem = true) {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'nvId' => array(
					'sql' => '`nv_id`',
					'value' => $kid,
					'comparison' => 'integerEqual'
				)
			),
			'ORDER_BY' => '`datum` ASC'
		);
		
		if (\count($addWhereQueryPartsDataArray) > 0) {
			$queryPartsDataArray['WHERE'] += $addWhereQueryPartsDataArray;
		}
		
		return ($getCustomerDataItem 
				? $this->getCampaignsAndCustomerDataItemsByQueryParts($queryPartsDataArray) 
				: $this->getCampaignsDataItemsByQueryParts($queryPartsDataArray, \PDO::FETCH_CLASS)
			)
		;
	}
	

	/**
	 * getNvCampaignDataItemsByKid
	 * 
	 * @param integer $kid
	 * @return false|array
	 */
	public function getNvCampaignDataItemsByKid($kid) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'nvId' => array(
						'sql' => '`nv_id`',
						'value' => $kid,
						'comparison' => 'integerEqual'
					),
					'kId' => array(
						'sql' => '`nv_id`',
						'value' => 'k_id',
						'comparison' => 'fieldNotEqual'
					)
				)
			),
			$this->getCampaignTable(),
			''
		);

		return $this->processCampaignsStmp($stmt);
	}

	/**
	 * getNvCampaignsCountByKid
	 * 
	 * @param integer $kid
	 * @return false|integer
	 */
	public function getNvCampaignsCountByKid($kid) {
		return $this->getCampaignsCountByQueryParts(
			array(
				'WHERE' => array(
					'nvId' => array(
						'sql' => '`nv_id`',
						'value' => $kid,
						'comparison' => 'integerEqual'
					),
					'kId' => array(
						'sql' => '`nv_id`',
						'value' => 'k_id',
						'comparison' => 'fieldNotEqual'
					)
				)
			)
		);
	}

	/**
	 * getNvCampaignsCountByKidAndFilterData
	 * 
	 * @param integer $kid
	 * @param array $addWhereQueryPartsDataArray
	 * @return false|integer
	 */
	public function getNvCampaignsCountByKidAndFilterData($kid, array $addWhereQueryPartsDataArray) {
		return $this->getCampaignsCountByQueryParts(
			array(
				'WHERE' => array(
					'nvId' => array(
						'sql' => '`nv_id`',
						'value' => $kid,
						'comparison' => 'integerEqual'
					),
					'kId' => array(
						'sql' => '`nv_id`',
						'value' => 'k_id',
						'comparison' => 'fieldNotEqual'
					),
					$addWhereQueryPartsDataArray
				)
			)
		);
	}

	/**
	 * getCampaignDataItemsForNvIds
	 * 
	 * @param array $fieldsDataArray
	 * @param array $nvIdsDataArray
	 * @param string $nvIdComparison
	 * @param integer $fetchMode
	 * @return false|array
	 * 
	 * @throws \PDOException
	 */
	public function getCampaignDataItemsForNvIds(array $fieldsDataArray, array $nvIdsDataArray, $nvIdComparison = ' IN ', $fetchMode = \PDO::FETCH_ASSOC) {
		$qry = 'SELECT ' . $this->getSelectFieldsQry($fieldsDataArray)
			. ' FROM ' . $this->getCampaignTable()
			. ' WHERE 1 = 1'
				. ' AND `k_id` > 1'
				. ' AND `nv_id` ' . $nvIdComparison . ' (' . $this->createPdoInString($nvIdsDataArray) . ')'
		;

		$debugDataArray = array(
			'fieldsDataArray' => $fieldsDataArray,
			'nvIdsDataArray' => $this->createPdoInString($nvIdsDataArray),
			'nvIdComparison' => $nvIdComparison
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);

			foreach ($nvIdsDataArray as $item) {
				$stmt->bindValue(':' . $item, $item, \PDO::PARAM_STR);
			}

			$stmt->execute();

			if ($fetchMode === \PDO::FETCH_CLASS) {
				$resultDataArray = $this->processCampaignsStmp($stmt);
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return ($fetchMode === \PDO::FETCH_CLASS ? $resultDataArray : $stmt->fetchAll($fetchMode));
	}

	/**
	 * getYearsDataArrayFromCampaing
	 * 
	 * @return false|array
	 * 
	 * @throws \PDOException
	 */
	public function getYearsDataArrayFromCampaing() {
		$qry = 'SELECT YEAR(`datum`) as `year`'
			. ' FROM ' . $this->getCampaignTable()
			. ' WHERE YEAR(`datum`) != 0000'
			. ' GROUP BY `year`'
			. ' ORDER BY `year` DESC'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->execute();

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $stmt->fetchAll(\PDO::FETCH_COLUMN);
	}

	/**
	 * getCampaignsAndCustomerDataItemsByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @return array
	 * 
	 * @throws \DomainException
	 */
	public function getCampaignsAndCustomerDataItemsByQueryParts(array $queryPartsDataArray) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1'
		);

		return $this->processCampaignsAndCustomerStmt($stmt);
	}
	
	/**
	 * getCampaignsAndCustomerDataItemsByQueryPartsAndDsdIds
	 * 
	 * @param array $queryPartsDataArray
	 * @param array $dsdIdsDataArray
	 * @param string $dsdIdComparison
	 * @return array
	 */
	public function getCampaignsAndCustomerDataItemsByQueryPartsAndDsdIds(array $queryPartsDataArray, array $dsdIdsDataArray, $dsdIdComparison = ' IN ') {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCampaignTable(),
			' AND `k_id` > 1' 
				. ' AND `dsd_id` ' . $dsdIdComparison . ' (' . \implode(',', $dsdIdsDataArray) . ')'
		);
		
		return $this->processCampaignsAndCustomerStmt($stmt);
	}
	
	/**
	 * getNVCampaignsDataItemsByKid
	 * 
	 * @param integer $kid
	 * @return array
	 */
	public function getNVCampaignsDataItemsByKid($kid) {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'nvId' => array(
					'sql' => '`nv_id`',
					'value' => $kid,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => '`k_id` ASC'
		);

		return $this->getCampaignsDataItemsByQueryParts(
			$queryPartsDataArray,
			\PDO::FETCH_CLASS
		);
	}

	/**
	 * getMonthCampaignsCountDataArray
	 * 
	 * @param integer $year
	 * @param string $addWhere
	 * @return false|array
	 * 
	 * @throws \PDOException
	 */
	public function getMonthCampaignsCountDataArray($year, $addWhere = '') {
		$qry = 'SELECT MONTH(`datum`) as `month`, COUNT(*) as `count`'
			. ' FROM ' . $this->getCampaignTable()
			. ' WHERE `k_id` > 1'
				. ' AND YEAR(`datum`) = :year'
				. ' AND `status` != 10'
				. $addWhere
			. ' GROUP BY `month`'
			. ' ORDER BY `month` ASC'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':year', $year, \PDO::PARAM_INT);
			$stmt->execute();

			$this->debugPdoStatement($stmt);

			$resultDataArray = array();
			while (($row = $stmt->fetch()) !== false) {
				$resultDataArray[$row['month']] = $row['count'];
			}
			$stmt->closeCursor();
		} catch (\PDOException $e) {
			$resultDataArray = false;

			$this->debugPdoException(
				$e,
				$qry
			);
		}

		return $resultDataArray;
	}
	
	
	/**
	 * deleteCampaignDataRowById
	 * 
	 * @param integer $campaignId
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function deleteCampaignDataRowById($campaignId) {
		$qry = 'DELETE '
			. ' FROM ' . $this->getCampaignTable()
			. ' WHERE `k_id` = :kid'
		;
		
		return $this->processDeleteItemByDataArray(
			$qry,
			array(
				'kid' => array(
					'value' => $campaignId,
					'dataType' => \PDO::PARAM_INT
				),
			)
		);
	}

	/**
	 * updateCampaignDataRowByKid
	 * 
	 * @param integer $campaignId
	 * @param array $dataArray
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function updateCampaignDataRowByKid($campaignId, array $dataArray) {
		$qry = 'UPDATE ' . $this->getCampaignTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `k_id` = :kid'
		;
		
		$dataArray['kid'] = array(
			'value' => (int) $campaignId,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}

	/**
	 * updateCampaignDataRowByNvId
	 * 
	 * @param integer $nvId
	 * @param array $dataArray
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function updateCampaignDataRowByNvId($nvId, array $dataArray) {
		$qry = 'UPDATE ' . $this->getCampaignTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `nv_id` = :nvId'
		;
		
		$dataArray['nvId'] = array(
			'value' => (int) $nvId,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}

	/**
	 * addCampaignItem
	 * 
	 * @param array $dataArray
	 * @return boolean|integer
	 */
	public function addCampaignItem(array $dataArray) {
		$qry = 'INSERT INTO ' . $this->getCampaignTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry, 
			$dataArray
		);
	}
	
	/**
	 * updateCampaignAndAddLogItem
	 * 
	 * @param integer $campaignId
	 * @param integer $campaignNvId
	 * @param integer $userId
	 * @param integer $actionId
	 * @param integer $status
	 * @param array $campaignDataArray
	 * @param boolean $logCampaignData
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function updateCampaignAndAddLogItem($campaignId, $campaignNvId, $userId, $actionId, $status, array $campaignDataArray, $logCampaignData = true) {
		/**
		 * createDataArrayForInsertQueryFields
		 */
		$logDataArray = $this->createDataArrayForInsertQueryFields(
			$userId,
			$actionId,
			$campaignId,
			$campaignNvId,
			$status,
			((boolean) $logCampaignData 
				? \serialize($this->getCampaignDataItemById($campaignId))
				: null
			)
		);
		
		$updateQry = 'UPDATE ' . $this->getCampaignTable()
			. ' SET ' . $this->addUpdateQueryFields($campaignDataArray)
			. ' WHERE `k_id` = :kid'
		;
		$insertQry = 'INSERT INTO ' . $this->getLogTable()
			. $this->processInsertQueryFieldsAndValues($logDataArray)
		;

		try {
			$this->getDbh()->beginTransaction();

			$updateStmt = $this->getDbh()->prepare($updateQry);
			$insertStmt = $this->getDbh()->prepare($insertQry);
			
			/**
			 * update Campaign
			 */
			$updateStmt->bindValue(':kid', $campaignId, \PDO::PARAM_INT);
			// processBindSetParameter
			$this->processBindSetParameter(
				$campaignDataArray,
				$updateStmt
			);
			
			/**
			 * insert ActionLog
			 */
			$this->processBindSetParameter(
				$logDataArray,
				$insertStmt
			);

			$updateStmt->execute();
			$insertStmt->execute();

			$this->getDbh()->commit();

			// debugPdoStatement
			$this->debugPdoStatement(
				$updateStmt,
				$campaignDataArray
			);
			$this->debugPdoStatement(
				$insertStmt,
				$logDataArray
			);

			// sendDebugDataToEmail
			$this->sendDebugDataToEmail(
				__CLASS__ . ' -> ' . __FUNCTION__,
				array(
					'updateQry' => $updateQry,
					'insertQry' => $insertQry,
					'kid' => $campaignId,
					'campaignDataArray' => $campaignDataArray,
					'logDataArray' => $logDataArray
				)
			);

			$result = true;
		} catch (\PDOException $e) {
			$result = false;
			
			$this->debugPdoException(
				$e,
				$updateQry . \chr(11) 
					. $insertQry
				,
				array(
					'kid' => $campaignId,
				)
			);
		}

		return $result;
	}
	
			
	/**
	 * updateFetchedObject
	 * 
	 * @param \CampaignEntity $object
	 * @return void
	 */
	protected function updateFetchedObject(\CampaignEntity &$object) {
		$object->setDatum($object->getDatum());
		$object->setGestartet($object->getGestartet());
		$object->setBeendet($object->getBeendet());
		$object->setErstreporting($object->getErstreporting());
		$object->setEndreporting($object->getEndreporting());
		$object->setLast_update($object->getLast_update());
	}
	
	
	/**
	 * createDataArrayForInsertQueryFields
	 * 
	 * @param integer $userId
	 * @param integer $actionId
	 * @param integer $campaignId
	 * @param integer $campaignNvId
	 * @param integer $status
	 * @param mixed $status
	 * @return array
	 */
	protected function createDataArrayForInsertQueryFields($userId, $actionId, $campaignId, $campaignNvId, $status, $logData) {
		return array(
			'userid' => array(
				'value' => $userId,
				'dataType' => PDO::PARAM_INT
			),
			'action' => array(
				'value' => $actionId,
				'dataType' => PDO::PARAM_INT
			),
			'kid' => array(
				'value' => $campaignId,
				'dataType' => PDO::PARAM_INT
			),
			'nv_id' => array(
				'value' => $campaignNvId,
				'dataType' => PDO::PARAM_INT
			),
			'timestamp' => array(
				'value' => date('Y-m-d H:i:s'),
				'dataType' => PDO::PARAM_STR
			),
			'status' => array(
				'value' => $status,
				'dataType' => PDO::PARAM_INT
			),
			'log_data' => array(
				'value' => $logData,
				'dataType' => PDO::PARAM_STR
			),
		);
	}
	
	/**
	 * processCampaignsAndCustomerStmt
	 * 
	 * @param \PDOStatement $stmt
	 * @return array
	 * 
	 * @throws \DomainException
	 * 
	 * TODO: verbessern, auch ->processCampaignAndCustomerStmt
	 */
	protected function processCampaignsAndCustomerStmt(\PDOStatement &$stmt) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, 'CampaignWidthCustomerAndContactPersonEntity');

		$resultDataArray = array();
		while (($row = $stmt->fetch()) !== false) {
			/* @var $row CampaignWidthCustomerAndContactPersonEntity */

			// updateFetchedObject
			$this->updateFetchedObject($row);

			$resultDataArray[$row->getK_id()] = $row;

			/**
			 * getCustomerDataItemById
			 */
			$customerEntity = $this->getCustomerDataItemById($row->getAgentur_id());
			if ($customerEntity instanceof \CustomerEntity) {
				/**
				 * updateCustomerEntityWidthStaticCountryEntity
				 */
				$this->updateCustomerEntityWidthStaticCountryEntity($customerEntity);
				
				$resultDataArray[$row->getK_id()]->setCustomerEntity($customerEntity);
				unset($customerEntity);
			} else {
				$resultDataArray[$row->getK_id()]->setCustomerEntity(new \CustomerEntity());
			}

			if ($row->getAp_id() > 0) {
				$contactPersonEntity = $this->getContactPersonDataItemById($row->getAp_id());
				if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
					throw new \DomainException('no ' . $this->getContactPersonFetchClass());
				}
				
				$resultDataArray[$row->getK_id()]->setContactPersonEntity($contactPersonEntity);
				unset($contactPersonEntity);
			} else {
				$resultDataArray[$row->getK_id()]->setContactPersonEntity(new \ContactPersonEntity());
			}
		}
		$stmt->closeCursor();

		return $resultDataArray;
	}
	
	/**
	 * processCampaignAndCustomerStmt
	 * 
	 * @param \PDOStatement $stmt
	 * @return \CampaignWidthCustomerAndContactPersonEntity
	 * 
	 * @throws \DomainException
	 */
	protected function processCampaignAndCustomerStmt(\PDOStatement &$stmt) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, 'CampaignWidthCustomerAndContactPersonEntity');

		if (($row = $stmt->fetch()) !== false) {
			/* @var $row CampaignWidthCustomerAndContactPersonEntity */

			// updateFetchedObject
			$this->updateFetchedObject($row);
			
			$stmt->closeCursor();

			/**
			 * getCustomerDataItemById
			 */
			$customerEntity = $this->getCustomerDataItemById($row->getAgentur_id());
			if ($customerEntity instanceof \CustomerEntity) {
				/**
				 * updateCustomerEntityWidthStaticCountryEntity
				 */
				$this->updateCustomerEntityWidthStaticCountryEntity($customerEntity);
				
				$row->setCustomerEntity($customerEntity);
				unset($customerEntity);
			} else {
				$row->setCustomerEntity(new \CustomerEntity());
			}

			if ($row->getAp_id() > 0) {
				$contactPersonEntity = $this->getContactPersonDataItemById($row->getAp_id());
				if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
					throw new \DomainException('no ' . $this->getContactPersonFetchClass());
				}

				$row->setContactPersonEntity($contactPersonEntity);
				unset($contactPersonEntity);
			} else {
				$row->setContactPersonEntity(new \ContactPersonEntity());
			}
		} else {
			throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity');
		}

		return $row;
	}
	
	/**
	 * processCampaignsStmp
	 * 
	 * @param \PDOStatement $stmt
	 * @return array
	 */
	protected function processCampaignsStmp(\PDOStatement &$stmt) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCampaignFetchClass());

		$resultDataArray = array();
		while (($row = $stmt->fetch()) !== false) {
			/* @var $row CampaignEntity */

			// updateFetchedObject
			$this->updateFetchedObject($row);

			$resultDataArray[$row->getK_id()] = $row;
		}
		$stmt->closeCursor();

		return $resultDataArray;
	}
	
	/**
	 * processCampaignStmp
	 * 
	 * @param \PDOStatement $stmt
	 * @return false|CampaignEntity
	 * 
	 * @throws \DomainException
	 */
	protected function processCampaignStmp(\PDOStatement &$stmt) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCampaignFetchClass());

		if (($row = $stmt->fetch()) !== false) {
			// updateFetchedObject
			$this->updateFetchedObject($row);

			$stmt->closeCursor();
		} else {
			throw new \DomainException('no ' . $this->getCampaignFetchClass());
		}
		
		return $row;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              customer - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getCustomerDataItemById
	 * 
	 * @param integer $customerId
	 * @return null|\CustomerEntity
	 * 
	 * @throws \PDOException
	 */
	public function getCustomerDataItemById($customerId) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getCustomerTable()
			. ' WHERE 1 = 1'
			. ' AND `kunde_id` = :customerId'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':customerId', $customerId, \PDO::PARAM_INT);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCustomerFetchClass());

			$this->debugPdoStatement($stmt);
			
			$customerEntity = $stmt->fetch();
			if ($customerEntity instanceof \CustomerEntity) {
				/**
				* updateCustomerEntityWidthStaticCountryEntity
				*/
			   $this->updateCustomerEntityWidthStaticCountryEntity($customerEntity);
			}
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				array(
					'customerId' => $customerId
				)
			);
		}
		
		return $customerEntity;
	}

	/**
	 * getCustomerDataItemByCompany
	 * 
	 * @param string $company
	 * @return null|\CustomerEntity
	 * 
	 * @throws \PDOException
	 */
	public function getCustomerDataItemByCompany($company) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getCustomerTable()
			. ' WHERE 1 = 1'
				. ' AND `firma` = :company'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':company', $company, \PDO::PARAM_STR);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCustomerFetchClass());

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				array(
					'company' => $company
				)
			);
		}

		return $stmt->fetch();
	}

	/**
	 * getCustomersDataItemsByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array
	 */
	public function getCustomersDataItemsByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCustomerTable(),
			' AND `status` = 1'
		);

		// setFetchMode
		if ($fetchMode === \PDO::FETCH_CLASS) {
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCustomerFetchClass());
		} else {
			$stmt->setFetchMode($fetchMode);
		}

		return $stmt->fetchAll();
	}

	/**
	 * getCustomerDataItemByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return null|array|\CustomerEntity
	 */
	public function getCustomerDataItemByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCustomerTable(),
			' AND `status` = 1'
		);

		// setFetchMode
		if ($fetchMode === \PDO::FETCH_CLASS) {
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCustomerFetchClass());
		} else {
			$stmt->setFetchMode($fetchMode);
		}

		return $stmt->fetch();
	}
	
	
	/**
	 * getCustomerCampaignsDataArrayByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @return null|array
	 */
	public function getCustomerCampaignsDataArrayByQueryParts(array $queryPartsDataArray) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getCustomerTable() 
                . ' JOIN ' . $this->getCampaignTable() . ' ON ' . $this->getCustomerTable() . '.`kunde_id` = ' . $this->getCampaignTable() . '.`agentur_id`',
			' AND (' 
				. $this->getCampaignTable() . '.`status` > 19' 
				. ' OR ' . $this->getCampaignTable() . '.`status` = 5' 
			. ')'
		);

		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getCustomerFetchClass());

		return $stmt->fetchAll();
	}
	

	/**
	 * addCustomerItem
	 * 
	 * @param array $dataArray
	 * @return boolean|integer
	 */
	public function addCustomerItem(array $dataArray) {
		$qry = 'INSERT INTO ' . $this->getCustomerTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	/**
	 * updateCustomerDataById
	 * 
	 * @param integer $customerId
	 * @param array $dataArray
	 * @return boolean
	 */
	public function updateCustomerDataById($customerId, array $dataArray) {
		$qry = 'UPDATE ' . $this->getCustomerTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `kunde_id` = :kundeId'
		;
		
		$dataArray['kundeId'] = array(
			'value' => (int) $customerId,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}
        /**
	 * updateCampaignDataInvoiceByCustomerid
	 * 
	 * @param integer $ap_id
	 * @param array $dataArray
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function updateCampaignDataInvoiceByCustomerid($ap_id, array $dataArray) {
            
		$qry = 'UPDATE ' . $this->getCampaignTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `ap_id` = :ap_id'
		;
		
		$dataArray['ap_id'] = array(
			'value' => (int) $ap_id,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}
	/**
	 * deleteCustomerDataById
	 * 
	 * @param integer $customerId
	 * @return boolean
	 */
	public function deleteCustomerDataById($customerId) {
		$qry = 'DELETE '
			. ' FROM ' . $this->getCustomerTable()
			. ' WHERE `kunde_id` = :kundeId'
		;
		
		return $this->processDeleteItemByDataArray(
			$qry,
			array(
				'kundeId' => array(
					'value' => $customerId,
					'dataType' => \PDO::PARAM_INT
				),
			)
		);
	}
	
	
	/**
	 * updateCustomerEntityWidthStaticCountryEntity
	 * 
	 * @param \CustomerEntity $customerEntity
	 * @return void
	 * 
	 * @throws \DomainException
	 */
	protected function updateCustomerEntityWidthStaticCountryEntity(\CustomerEntity &$customerEntity) {
		if ($customerEntity->getCountry_id() > 0) {
			if (\count($_SESSION['staticCountryDataArray']) === 0) {
				throw new \DomainException('no StaticCountryEntity');
			}
			
			$customerEntity->setStaticCountry($_SESSION['staticCountryDataArray'][$customerEntity->getCountry_id()]);
		} else {
			$customerEntity->setStaticCountry(new \StaticCountryEntity());
		}
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              contactPerson - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getContactPersonDataItemById
	 * 
	 * @param integer $apId
	 * @return false|\ContactPersonEntity
	 * 
	 * @throws \PDOException
	 */
	public function getContactPersonDataItemById($apId) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getContactPersonTable()
			. ' WHERE 1 = 1'
				. ' AND `ap_id` = :apId'
		;

		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':apId', $apId, \PDO::PARAM_INT);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getContactPersonFetchClass());

			$this->debugPdoStatement($stmt);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				array(
					'apId' => $apId
				)
			);
		}

		return $stmt->fetch();
	}

	/**
	 * getContactPersonDataItemsByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array
	 */
	public function getContactPersonDataItemsByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getContactPersonTable(), 
			' AND `status` = 1'
		);

		// setFetchMode
		if ($fetchMode === \PDO::FETCH_CLASS) {
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getContactPersonFetchClass());
		} else {
			$stmt->setFetchMode($fetchMode);
		}

		return $stmt->fetchAll();
	}

	/**
	 * getContactPersonDataItemByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @param integer $fetchMode
	 * @return false|array|\ContactPersonEntity
	 */
	public function getContactPersonDataItemByQueryParts(array $queryPartsDataArray, $fetchMode = \PDO::FETCH_ASSOC) {
		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getContactPersonTable(),
			' AND `status` = 1'
		);

		// setFetchMode
		if ($fetchMode === \PDO::FETCH_CLASS) {
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getContactPersonFetchClass());
		} else {
			$stmt->setFetchMode($fetchMode);
		}

		return $stmt->fetch();
	}
	
	/**
	 * getContactPersonsCountByQueryParts
	 * 
	 * @param array $queryPartsDataArray
	 * @return false|integer
	 */
	public function getContactPersonsCountByQueryParts(array $queryPartsDataArray) {
		$queryPartsDataArray['SELECT'] = array(
			'count' => 'COUNT(`ap_id`)'
		);
		unset(
			$queryPartsDataArray['ORDER_BY'],
			$queryPartsDataArray['LIMIT']
		);

		$stmt = $this->processDataArrayByQueryParts(
			$queryPartsDataArray,
			$this->getContactPersonTable(), 
			' AND `status` = 1'
		);

		return $stmt->fetchColumn();
	}

	/**
	 * addContactPersonItem
	 * 
	 * @param array $dataArray
	 * @return boolean|integer
	 */
	public function addContactPersonItem(array $dataArray) {
		$qry = 'INSERT INTO ' . $this->getContactPersonTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	/**
	 * updateContactPersonDataById
	 * 
	 * @param integer $contactPersonId
	 * @param array $dataArray
	 * @return boolean
	 */
	public function updateContactPersonDataById($contactPersonId, array $dataArray) {
		$qry = 'UPDATE ' . $this->getContactPersonTable()
			. ' SET ' . $this->addUpdateQueryFields($dataArray)
			. ' WHERE `ap_id` = :apId'
		;
		
		$dataArray['apId'] = array(
			'value' => (int) $contactPersonId,
			'dataType' => \PDO::PARAM_INT
		);

		return $this->processUpdateItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	/**
	 * deleteContactPersonDataById
	 * 
	 * @param integer $contactPersonId
	 * @return boolean
	 */
	public function deleteContactPersonDataById($contactPersonId) {
		$qry = 'DELETE '
			. ' FROM ' . $this->getContactPersonTable()
			. ' WHERE `ap_id` = :apId'
		;
		
		return $this->processDeleteItemByDataArray(
			$qry,
			array(
				'apId' => array(
					'value' => $contactPersonId,
					'dataType' => \PDO::PARAM_INT
				),
			)
		);
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              log - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * addLogItem
	 * 
	 * @param array $dataArray
	 * @return boolean|integer
	 */
	public function addLogItem(array $dataArray) {
		$qry = 'INSERT INTO ' . $this->getLogTable()
			. $this->processInsertQueryFieldsAndValues($dataArray)
		;

		return $this->processInsertItemByDataArray(
			$qry,
			$dataArray
		);
	}
	
	/**
	 * deleteAllCampaignsLogByKid
	 * 
	 * @param integer $campaignId
	 * @return boolean
	 * 
	 * @throws \PDOException
	 */
	public function deleteAllCampaignsLogByKid($campaignId) {
		$qry = 'DELETE'
			. ' FROM ' . $this->getLogTable()
			. ' WHERE `kid` = :kid'
		;
		
		return $this->processDeleteItemByDataArray(
			$qry,
			array(
				'kid' => array(
					'value' => $campaignId,
					'dataType' => \PDO::PARAM_INT
				),
			)
		);
	}
	
	/**
	 * getLastLogByAction
	 * 
	 * @param integer $actionId
	 * @param integer $campaignId
	 * @return null|\ActionLogEntity
	 * 
	 * @throws \PDOException
	 */
	public function getLastLogByAction($actionId, $campaignId) {
		$qry = 'SELECT *'
			. ' FROM ' . $this->getLogTable()
			. ' WHERE action = :actionId'
				. ' AND kid = :kId'
			. ' ORDER BY timestamp DESC'
		;

		$debugDataArray = array(
			'actionId' => $actionId,
			'kId' => $campaignId
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindParam(':actionId', $actionId, \PDO::PARAM_INT);
			$stmt->bindParam(':kId', $campaignId, \PDO::PARAM_INT);
			$stmt->execute();

			// setFetchMode
			$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getLogFetchClass());

			if (($row = $stmt->fetch()) !== false) {
				/* @var $row ActionLogEntity */
				$row->setTimestamp($row->getTimestamp());
			} else {
				$row = null;
			}

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $row;
	}
	
	/**
	 * getCampaignLastLogItemByStatus
	 * 
	 * @param integer $campaignId
	 * @param integer $statusId
	 * @return false|ActionLogEntity
	 */
	public function getCampaignLastLogItemByStatus($campaignId, $statusId) {
		$stmt = $this->processDataArrayByQueryParts(
			array(
				'WHERE' => array(
					'kid' => array(
						'sql' => '`kid`',
						'value' => $campaignId,
						'comparison' => 'integerEqual'
					),
					'status' => array(
						'sql' => '`status`',
						'value' => $statusId,
						'comparison' => 'integerEqual'
					)
				),
				'ORDER_BY' => 'timestamp DESC',
				'LIMIT' => 1
			),
			$this->getLogTable(),
			''
		);
		
		return $this->processLogStmp($stmt);
	}
	
	
	/**
	 * processLogStmp
	 * 
	 * @param \PDOStatement $stmt
	 * @return \ActionLogEntity
	 */
	protected function processLogStmp(\PDOStatement &$stmt) {
		// setFetchMode
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $this->getLogFetchClass());

		if (($row = $stmt->fetch()) !== false) {
			/* @var $row ActionLogEntity */
			$row->setTimestamp($row->getTimestamp());

			$stmt->closeCursor();
		}
		
		return $row;
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              deprecated - Functions
	 *
	 ****************************************************************************************** */
	/**
	 * getNVCampaignsAndCustomerDataItemsByKid
	 * 
	 * @param integer $kid
	 * @return array
	 * @deprecated use ->getAllCampaignsDataItemsByKid
	 */
	public function getNVCampaignsAndCustomerDataItemsByKid($kid) {
		$queryPartsDataArray = array(
			'WHERE' => array(
				'nvId' => array(
					'sql' => '`nv_id`',
					'value' => $kid,
					'comparison' => 'integerEqual'
				),
			),
			'ORDER_BY' => '`datum` ASC'
		);

		return $this->getCampaignsAndCustomerDataItemsByQueryParts($queryPartsDataArray);
	}
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function setCampaignTable($campaignTable) {
		$this->campaignTable = '`' . $campaignTable . '`';
	}
	public function getCampaignTable() {
		return $this->campaignTable;
	}

	public function getCustomerTable() {
		return $this->customerTable;
	}
	public function setCustomerTable($customerTable) {
		$this->customerTable = $customerTable;
	}

	public function getContactPersonTable() {
		return $this->contactPersonTable;
	}
	public function setContactPersonTable($contactPersonTable) {
		$this->contactPersonTable = $contactPersonTable;
	}
	
	public function getLogTable() {
		return $this->logTable;
	}
	public function setLogTable($logTable) {
		$this->logTable = $logTable;
	}
	
	public function getCampaignFetchClass() {
		return $this->campaignFetchClass;
	}
	public function setCampaignFetchClass($campaignFetchClass) {
		$this->campaignFetchClass = $campaignFetchClass;
	}

	public function getCustomerFetchClass() {
		return $this->customerFetchClass;
	}
	public function setCustomerFetchClass($customerFetchClass) {
		$this->customerFetchClass = $customerFetchClass;
	}

	public function getContactPersonFetchClass() {
		return $this->contactPersonFetchClass;
	}
	public function setContactPersonFetchClass($contactPersonFetchClass) {
		$this->contactPersonFetchClass = $contactPersonFetchClass;
	}
	
	public function getLogFetchClass() {
		return $this->logFetchClass;
	}
	public function setLogFetchClass($logFetchClass) {
		$this->logFetchClass = $logFetchClass;
	}

}