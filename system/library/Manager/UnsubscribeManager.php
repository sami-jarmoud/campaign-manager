<?php
require_once(DIR_Manager . 'PdoDbManager.php');

class UnsubscribeManager extends \PdoDbManager {
	protected $clientTable = 'mandant_1';
	
	
	
	
	
	/**
	 * init
	 * 
	 * @return void
	 */
	public function init() {
		$this->setHost('localhost');
		$this->setDbName('abmelder_ems');
		$this->setDbUser('abmelder');
		$this->setDbPassword('2X6w5W9a');

		parent::init();
	}

	/**
	 * checkIfTableExist
	 * 
	 * @return false|integer
	 */
	public function checkIfTableExist() {
		$qry = 'SHOW TABLES LIKE "' . $this->getClientTable() . '"';

		try {
			$stmt = $this->getDbh()->query($qry);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				array(
					'clientTable' > $this->getClientTable(),
				)
			);
		}

		return $stmt->rowCount();
	}

	/**
	 * getClientCountDataByDateAndType
	 * 
	 * @param \DateTime $dataTime
	 * @param boolean $getByCscGroup
	 * @return false|integer
	 */
	public function getClientCountDataByDateAndType(\DateTime $dataTime, $getByCscGroup = false) {
		$qry = 'SELECT COUNT(1) as `count`'
			. ' FROM ' . $this->getClientTable()
			. ' WHERE 1 = 1'
				. ' AND MONTH(`created_at`) = :month'
				. ' AND YEAR(`created_at`) = :year'
		;

		if ((boolean) $getByCscGroup === true) {
			$qry .= ' AND `benutzer` > 0';
		} else {
			$qry .= ' AND `benutzer` = 0';
		}

		$debugDataArray = array(
			'clientTable' > $this->getClientTable(),
			'date' => $dataTime,
			'type' => $getByCscGroup
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':month', $dataTime->format('m'), \PDO::PARAM_INT);
			$stmt->bindValue(':year', $dataTime->format('Y'), \PDO::PARAM_INT);
			$stmt->execute();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchColumn();
	}

	/**
	 * getClientCountDataForEmsByMonthAndYear
	 * 
	 * @param integer $month
	 * @param integer $year
	 * @return false|integer
	 */
	public function getClientCountDataForEmsByMonthAndYear($month, $year) {
		$qry = 'SELECT COUNT(1) as `count`'
			. ' FROM ' . $this->getClientTable()
			. ' WHERE 1 = 1'
				. ' AND MONTH(`created_at`) = :month'
				. ' AND YEAR(`created_at`) = :year'
				. ' AND (`ip` != "" OR `benutzer` > 0)'
			. ' GROUP BY `email`'
		;

		$debugDataArray = array(
			'clientTable' > $this->getClientTable(),
				'month' => $month,
				'year' => $year
		);
		
		try {
			$stmt = $this->getDbh()->prepare($qry);
			$stmt->bindValue(':month', $month, \PDO::PARAM_INT);
			$stmt->bindValue(':year', $year, \PDO::PARAM_INT);
			$stmt->execute();

			$this->debugPdoStatement(
				$stmt,
				$debugDataArray
			);
		} catch (\PDOException $e) {
			$this->debugPdoException(
				$e,
				$qry,
				$debugDataArray
			);
		}

		return $stmt->fetchColumn();
	}
	
	
	
	
	
	/********************************************************************************************
	 *
	 *              setter and getter - Functions
	 *
	 ****************************************************************************************** */
	public function getClientTable() {
		return $this->clientTable;
	}
	public function setClientTable($clientTable) {
		$this->clientTable = $clientTable;
	}

}