<?php
    /**
     * (C)opyright 2002-2007 optivo GmbH, Stralauer Allee 2, 10247 Berlin (www.optivo.de)
     * All rights reserved. You are granted the right to use the software regarding to the
     * terms and conditions only. Especially you are not allowed to copy, sell, change or
     * redistribute any part of this software.
     */

    /**
     * GENERATED FILE - DO NOT ALTER!
     *
     * Rpc implementation of the webservice interface RecipientWebservice.
     *
     * @author Peter Romianowski
     * @version 1.0 Wed Oct 08 15:26:11 CEST 2008
     */

    class BroadmailRpcRecipientWebserviceImpl extends _BroadmailRpcBase {

        function BroadmailRpcRecipientWebserviceImpl($sessionId, $endPoint,
            $proxyHost = false, $proxyPort = false, $proxyUsername = false,
            $proxyPassword = false, $timeout = 0,
            $responseTimeout = 30) {

            $this->_init($sessionId, $endPoint, 'Recipient', $proxyHost, $proxyPort,
                $proxyUsername, $proxyPassword, $timeout, $responseTimeout);
        }



        function add( $p1,  $p2,  $p3,  $p4,  $p5,  $p6) {
            return $this->_call('add', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.String', $p4), 'p6' => $this->_convert('java.lang.String[]', $p5), 'p7' => $this->_convert('java.lang.String[]', $p6)));
        }

        function clear( $p1) {
            return $this->_call('clear', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function contains( $p1,  $p2) {
            return $this->_call('contains', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function addAll( $p1,  $p2,  $p3,  $p4,  $p5,  $p6) {
            return $this->_call('addAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4), 'p6' => $this->_convert('java.lang.String[]', $p5), 'p7' => $this->_convert('java.lang.String[][]', $p6)));
        }

        function remove( $p1,  $p2) {
            return $this->_call('remove', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function removeAll( $p1,  $p2) {
            return $this->_call('removeAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function getAttributes( $p1,  $p2,  $p3) {
            return $this->_call('getAttributes', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3)));
        }

        function setAttributes( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('setAttributes', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4)));
        }

        function getCount( $p1,  $p2) {
            return $this->_call('getCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2)));
        }

        function getAll( $p1,  $p2) {
            return $this->_call('getAll', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2)));
        }

        function getAllAdvanced( $p1,  $p2,  $p3,  $p4,  $p5,  $p6,  $p7) {
            return $this->_call('getAllAdvanced', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String[]', $p2), 'p4' => $this->_convert('java.lang.Long', $p3), 'p5' => $this->_convert('java.lang.String', $p4), 'p6' => $this->_convert('java.lang.Boolean', $p5), 'p7' => $this->_convert('java.lang.Integer', $p6), 'p8' => $this->_convert('java.lang.Integer', $p7)));
        }

        function containsValid( $p1,  $p2) {
            return $this->_call('containsValid', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function containsMultiple( $p1,  $p2) {
            return $this->_call('containsMultiple', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function containsValidMultiple( $p1,  $p2) {
            return $this->_call('containsValidMultiple', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long[]', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function removeNotOptined( $p1,  $p2) {
            return $this->_call('removeNotOptined', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2)));
        }

        function removeBlacklisted( $p1) {
            return $this->_call('removeBlacklisted', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function removeUnsubscribed( $p1) {
            return $this->_call('removeUnsubscribed', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function removeBounced( $p1) {
            return $this->_call('removeBounced', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1)));
        }

        function getDistinctValues( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getDistinctValues', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Integer', $p4)));
        }

        function getDistinctValuesCount( $p1,  $p2,  $p3,  $p4) {
            return $this->_call('getDistinctValuesCount', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String', $p3), 'p5' => $this->_convert('java.lang.Integer', $p4)));
        }

        function setAttributesByFilter( $p1,  $p2,  $p3,  $p4,  $p5) {
            return $this->_call('setAttributesByFilter', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.Long', $p2), 'p4' => $this->_convert('java.lang.String[]', $p3), 'p5' => $this->_convert('java.lang.String[]', $p4), 'p6' => $this->_convert('java.lang.String', $p5)));
        }

        function changeRecipientId( $p1,  $p2,  $p3) {
            return $this->_call('changeRecipientId', array('p1' => $this->sessionId, 'p2' => $this->_convert('java.lang.Long', $p1), 'p3' => $this->_convert('java.lang.String', $p2), 'p4' => $this->_convert('java.lang.String', $p3)));
        }

        function getVersion() {
            return $this->_call('getVersion', array());
        }
    }

?>