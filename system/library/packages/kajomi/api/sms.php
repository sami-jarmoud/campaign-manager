<?php

namespace kjm\api;

use kjm\api\iconnector; 
use kjm\api\call;
use kjm\api\classes\parameter; 


require_once 'api/classes.php';
require_once DIR_Pear . 'HTTP/Request2.php';

	/**
	 * 
	 * \cond
	 */

class sms
{
	private $con;
	
	/**
	 * constructor
	 * @param iconnection $icon
	 */
	public function __construct($icon)
	{
		$this->con=$icon;
	}
	
	public  function relay($sender,$_recepient,$m)
	{
		  $cc=call::create("api","basic/sms/relay")->addParam("sender",$sender)->addParam("_recepient", $_recepient)->addParam("m", $m);
		  return $this->con->invoke($cc);
	}	
}

	/**
	 * 
	 * \endcond
	 */
?>	