function pagePlanerRequest() {
	var reloadUrl = createCampaignReloadUrl();
	
	setRequest(reloadUrl, 'p');
}

function createCampaignReloadUrl() {
	var reloadUrl;
	
	if (YAHOO.util.Dom.get('page_planer') !== null) {
		// TODO: remove, use: YAHOO.util.Dom.get('page').value
		reloadUrl = createPlanerReloadUrl();
	} else {
		// default page url
		reloadUrl = YAHOO.util.Dom.get('page').value;
	}
	
	if (YAHOO.util.Dom.get('kid') !== null) {
		if (YAHOO.util.Dom.get('mailingTyp').value === 'NV') {
			reloadUrl += '&campaignAnchor=snv' + parseInt(YAHOO.util.Dom.get('kid').value);
		} else {
			reloadUrl += '&campaignAnchor=s' + parseInt(YAHOO.util.Dom.get('kid').value);
		}
	}
	
	return reloadUrl;
}

function createPlanerReloadUrl() {
	// TODO: remove
	return 'kampagne/planer/index.php' 
		+ '?view=' + YAHOO.util.Dom.get('planer_view').value 
		+ '&jahr=' + parseInt(YAHOO.util.Dom.get('planer_jahr').value) 
		+ '&monat=' + parseInt(YAHOO.util.Dom.get('planer_monat').value) 
		+ '&sort=' + YAHOO.util.Dom.get('planer_sort').value 
		+ '&view=' + YAHOO.util.Dom.get('planer_view').value 
		+ '&abrTyp=' + YAHOO.util.Dom.get('abrTyp').value
                + '&deliverySystem=' + YAHOO.util.Dom.get('deliverySystem').value
                + '&deliverySystemDistributor=' + parseInt (YAHOO.util.Dom.get('deliverySystemDistributor').value)
	;
}

function pageReload(page, subPage) {
	setTimeout(function() {
		if (YAHOO.util.Dom.get('page_planer') !== null) {
			// TODO: remove, use: setRequest(page, subPage);
			pagePlanerRequest();
		} else {
			setRequest(
				page,
				subPage
			);
		}
	}, 1000);
}

// Instantiate a Panel from markup
var handleCancel_default = function() {
	this.cancel();
};

var handleSubmit_default = function() {
	this.submit();
};

var handleUpdate = function() {
	var p = YAHOO.util.Dom.get('page').value;
	var p_sub = YAHOO.util.Dom.get('page_sub').value;
	
	this.cancel();
	
	pageReload(p, p_sub);
};


YAHOO.namespace('example.container');