function initBlacklistUnsibscribe() {
	var handleSubmit_blacklist = function() {
		this.submit();

		setRequest('blacklist/blacklist_tab.php', 'b');

		document.search_bl_form.reset();
	};
	
	var handleSubmit_unsubscribe = function() {
		this.submit();

		setRequest('abmeldung/abmeldung.php', 'unsub');

		document.search_unsub_form.reset();
	};
	
	
	
	// blacklist
	YAHOO.example.container.suche_bl = new YAHOO.widget.Dialog('suche_bl', {
		width: '367px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: false,
		constraintoviewport: true,
		buttons: [{
			text: 'suchen',
			handler: handleSubmit_blacklist,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.suche_bl.render();
	YAHOO.util.Event.addListener('suchen_bl', 'click', YAHOO.example.container.suche_bl.show, YAHOO.example.container.suche_bl, true);
	
	YAHOO.example.container.blacklist = new YAHOO.widget.Dialog('container_blacklist', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'Eintragen',
			handler: handleSubmit_blacklist,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.blacklist.validate = function() {
		var blfehler = '';

		var grund = YAHOO.util.Dom.get('grund').value;
		var grund_new = YAHOO.util.Dom.get('grund_new').value;
		var list_blacklist = YAHOO.util.Dom.get('list_blacklist').value;

		if (grund === '' 
			&& grund_new === ''
		) {
			blfehler += "- Bitte einen Grund angeben\n";
		}

		if (list_blacklist === '') {
			blfehler += "- Bitte Adressen einfuegen\n";
		}

		if (blfehler === '') {
			return true;
		} else {
			alert(blfehler);

			return false;
		}
	};
	YAHOO.example.container.blacklist.render();
	YAHOO.util.Event.addListener('nl_blacklist', 'click', YAHOO.example.container.blacklist.show, YAHOO.example.container.blacklist, true);
	YAHOO.util.Event.addListener('su_blacklist', 'click', YAHOO.example.container.blacklist.show, YAHOO.example.container.blacklist, true);
	
	YAHOO.example.container.blacklist_status = new YAHOO.widget.Dialog('container_blacklist_status', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'OK',
			handler: handleCancel_default,
			isDefault: true
		}]
	});
	YAHOO.example.container.blacklist_status.render();
	YAHOO.util.Event.addListener('status_blacklist_button', 'click', YAHOO.example.container.blacklist_status.show, YAHOO.example.container.blacklist_status, true);
	

	// unsubscribe
	YAHOO.example.container.suche_unsub = new YAHOO.widget.Dialog('suche_unsub', {
		width: '367px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: false,
		constraintoviewport: true,
		buttons: [{
			text: 'suchen',
			handler: handleSubmit_unsubscribe,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.suche_unsub.render();
	YAHOO.util.Event.addListener('suchen_unsub', 'click', YAHOO.example.container.suche_unsub.show, YAHOO.example.container.suche_unsub, true);
	
	YAHOO.example.container.unsubscribe = new YAHOO.widget.Dialog('container_unsubscribe', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'Abmelden',
			handler: handleSubmit_unsubscribe,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.unsubscribe.render();
	YAHOO.util.Event.addListener('nl_unsubscribe', 'click', YAHOO.example.container.unsubscribe.show, YAHOO.example.container.unsubscribe, true);
	YAHOO.util.Event.addListener('su_unsubscribe', 'click', YAHOO.example.container.unsubscribe.show, YAHOO.example.container.unsubscribe, true);
}
YAHOO.util.Event.addListener(window, 'load', initBlacklistUnsibscribe);