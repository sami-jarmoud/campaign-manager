function changeValueToNumber(value, elementId) {
	var newValue = convertValue2Integer(value);
	
	YAHOO.util.Dom.get(elementId).value = Trenner(newValue);
}

function calcNewUSQ(v, cmVorgabeUsq) {
    v = convertValue2Integer(v);
	var newV = parseInt((v * (cmVorgabeUsq / 100)) + v);

	YAHOO.util.Dom.get('vorgabe_m').value = Trenner(newV);
	YAHOO.util.Dom.get('cm_vorgabe_usq').value = ((newV / v) * 100) - 100;
	YAHOO.util.Dom.get('volumen').value = Trenner(v);
}

function calcNewV() {
    var volume = convertValue2Integer(YAHOO.util.Dom.get('volumen').value);
	var cmVorgabeUsq = convertValue2Float(YAHOO.util.Dom.get('cm_vorgabe_usq').value);

	if (volume > 0) {
		YAHOO.util.Dom.get('vorgabe_m').value = Trenner(parseInt((volume * (cmVorgabeUsq / 100)) + volume));
	} else {
		alert('Bitte zuerst das gebuchte Volumen eintragen');

		YAHOO.util.Dom.get('cm_vorgabe_usq').value = cmVorgabeUsq;
	}
}

function calcNewUSQ2(v, cmVorgabeUsq) {
    var v_old = convertValue2Integer(YAHOO.util.Dom.get('volumen').value);
	v = convertValue2Integer(v);

	if (v_old === 0) {
		alert('Bitte zuerst das gebuchte Volumen eintragen');

		YAHOO.util.Dom.get('vorgabe_m').value = '';
		YAHOO.util.Dom.get('cm_vorgabe_usq').value = cmVorgabeUsq;
	} else {
		YAHOO.util.Dom.get('cm_vorgabe_usq').value = ((v / v_old) * 100) - 100;
	}
}

function kNewCustomer() {
    YAHOO.util.Dom.get('cmCustomerCompany').value = '';
	YAHOO.util.Dom.get('cmCustomerShortCompany').value = '';
	YAHOO.util.Dom.get('cmCustomerStreet').value = '';
	YAHOO.util.Dom.get('cmCustomerZip').value = '';
	YAHOO.util.Dom.get('cmCustomerCity').value = '';
	YAHOO.util.Dom.get('cmCustomerCountrySearch').value = '';
	YAHOO.util.Dom.get('cmCustomerCountryId').value = '';
	YAHOO.util.Dom.get('cmCustomerPhone').value = '';
	YAHOO.util.Dom.get('cmCustomerFax').value = '';
	YAHOO.util.Dom.get('cmCustomerWebsite').value = '';
	YAHOO.util.Dom.get('cmCustomerEmail').value = '';
	YAHOO.util.Dom.get('cmCustomerCeo').value = '';
	YAHOO.util.Dom.get('cmCustomerRegistergericht').value = '';
	YAHOO.util.Dom.get('cmCustomerVatNumber').value = '';
	YAHOO.util.Dom.get('cmCustomerPaymentDeadline').value = '';

	YAHOO.example.container.k_neuer_kunde.show();
}

function kNewContactPerson(userId) {
	resetSelectedValueById('cmContactPersonVertriebler');

	var vertrieblerObj = YAHOO.util.Dom.get('cmContactPersonVertriebler');
	for (var i = 0; i < vertrieblerObj.length; i++) {
		if (parseInt(vertrieblerObj.options[i].value) === userId) {
			vertrieblerObj.options[i].selected = true;
		}
	}

	YAHOO.util.Dom.get('cmContactPersonGender_2').checked = false;
	YAHOO.util.Dom.get('cmContactPersonTitle').value = '';
	YAHOO.util.Dom.get('cmContactPersonFirstname').value = '';
	YAHOO.util.Dom.get('cmContactPersonLastname').value = '';
	YAHOO.util.Dom.get('cmContactPersonEmail').value = '';
	YAHOO.util.Dom.get('cmContactPersonPhone').value = '';
	YAHOO.util.Dom.get('cmContactPersonFax').value = '';
	YAHOO.util.Dom.get('cmContactPersonMobil').value = '';
	YAHOO.util.Dom.get('cmContactPersonPosition').value = '';

	YAHOO.example.container.k_neuer_kontakt.show();
}

function getAP(customerValue, contactPersonId) {
	if (YAHOO.util.Dom.get('apButton') !== null) {
		showViewById('k_neuer_kunde');

		if (isNaN(parseInt(customerValue))) {
			removeSelectItemsById('ap');
			setSelectedValueById('ap', '-- keine Kontakte --');

			if (customerValue.length === 0) {
				hideViewById('apButton');
			}
		} else {
			showViewById('apButton');

			resetSelectedValueById('ap');

			processAjaxRequest(
				'AjaxRequests/ajaxCampaign.php',
				'actionMethod=contactPersons&customerId=' + parseInt(customerValue) 
					+ '&contactPersonId=' + contactPersonId
				,
				'ap_select',
				function() {
					hideViewById('k_neuer_kunde');
				}
			);
		}
	}
}

function changeContactPerson(contactPersonValue) {
	if (isNaN(parseInt(contactPersonValue))) {
		getVertriebler(0);
		
		showViewById('apButton');
	} else {
		hideViewById('apButton');
	}
}

function getVertriebler(salesId) {
	processAjaxRequest(
		'AjaxRequests/ajaxClient.php',
		'actionMethod=salesPerson&salesId=' + parseInt(salesId),
		'vertriebler_select',
		function() {}
	);
}

function oHide(mailtyp) {
	switch (mailtyp) {
		case 't':
			hideViewById('openingTarget');
			break;

		default:
			showViewById('openingTarget');
			break;
	}
}


function toggleViewById(elementId) {
	var force = (YAHOO.util.Dom.hasClass(elementId, 'toggleHide')) ? true : false;

	if (force === true) {
		showViewById(elementId);
	} else {
		hideViewById(elementId);
	}
}

function changeDeliverySystem(deliverySystemValue, inputDistributorId, disableNotSelectedItem, campaignDsdId) {
	hideViewById(inputDistributorId + 'Cell');
	YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = '';

	if (deliverySystemValue !== '') {
		processAjaxRequest(
			'AjaxRequests/ajaxClient.php',
			'actionMethod=deliverySystemDistributor'
				+ '&deliverySystem=' + deliverySystemValue
				+ '&disableNotSelectedItem=' + parseInt(disableNotSelectedItem)
				+ '&dsdId=' + parseInt(campaignDsdId)
			,
			inputDistributorId,
			function(deliverySystemDistributorDataArray) {
				if (typeof deliverySystemDistributorDataArray != 'undefined') {
					showViewById(inputDistributorId + 'Content');
					YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = deliverySystemDistributorDataArray;

					hideViewById(inputDistributorId);
				} else {
					hideViewById(inputDistributorId + 'Content');
					showViewById(inputDistributorId);

					deliverySystemDistributor.disabled = false;
				}

				showViewById(inputDistributorId + 'Cell');
			}
		);
	} else {
		removeSelectItemsById(inputDistributorId + 'Items');

		deliverySystemDistributor.disabled = true;
	}
}

function changeDeliverySystemDomain(deliverySystemDistributorValue, inputDistributorId, disableNotSelectedItem, campaignDsdId) {
	hideViewById(inputDistributorId + 'Cell');
	YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = '';

	if (deliverySystemDistributorValue !== '') {
		processAjaxRequest(
			'AjaxRequests/ajaxClient.php',
			'actionMethod=deliverySystemDomain'
				+ '&deliverySystemDistributor=' + deliverySystemDistributorValue
				+ '&disableNotSelectedItem=' + parseInt(disableNotSelectedItem)
				+ '&dsdId=' + parseInt(campaignDsdId)
			,
			inputDistributorId,
			function(deliverySystemDistributorDataArray) {
				if (typeof deliverySystemDistributorDataArray != 'undefined') {
					showViewById(inputDistributorId + 'Content');
					YAHOO.util.Dom.get(inputDistributorId + 'Content').innerHTML = deliverySystemDistributorDataArray;

					hideViewById(inputDistributorId);
				} else {
					hideViewById(inputDistributorId + 'Content');
					showViewById(inputDistributorId);

					deliverySystemDistributorValue.disabled = false;
				}

				showViewById(inputDistributorId + 'Cell');
			}
		);
	} else {
		removeSelectItemsById(inputDistributorId + 'Items');

		deliverySystemDistributorValue.disabled = true;
	}
}
function changePriceValue(priceValue, priceId, billingType, inputPriceIdAndName) {
	priceValue = convertValue2Float(priceValue);

	if (isNaN(priceValue)) {
		alert('Falsche preis Angabe!');

		return false;
	} else {
		priceValue = convertFloat2Number(priceValue);

		YAHOO.util.Dom.get(priceId).value = priceValue;
		if (YAHOO.util.Dom.get(inputPriceIdAndName)) {
			YAHOO.util.Dom.get(inputPriceIdAndName).value = priceValue;
		}

		/* createAndShowNewPerformanceContent */
		if (billingType.length > 0) {
			createAndShowNewPerformanceContent(billingType);
		}
	}
}

function createElement(elementType, elementSettings) {
	var newNode = document.createElement(elementType);

	if (elementSettings !== '') {
		for (var key in elementSettings) {
			newNode = setAttributeToNode(
				key,
				elementSettings[key],
				newNode
			);
		}
	}

	return newNode;
}

function setAttributeToNode(attributeKey, attributeValue, nodeElement) {
	var attribute = document.createAttribute(attributeKey);
	attribute.nodeValue = attributeValue;

	nodeElement.setAttributeNode(attribute);

	return nodeElement;
}

function createPriceElement(inputPriceIdAndName, priceId, priceValue, billingType) {
	return createElement('input', {
		id: inputPriceIdAndName,
		type: 'text',
		name: inputPriceIdAndName,
		style: 'width: 100px',
		onchange: 'changePriceValue(this.value, "' + priceId + '", "' + billingType + '", "' + inputPriceIdAndName + '")',
		value: priceValue
	});
}

function showOrChangePriceByBillingType(billingType, toggleId, changePrice) {
	YAHOO.util.Dom.get('priceByBillingTypeContent').innerHTML = '';

	if (billingType !== '') {
		showViewById('volumeAndPerformance');

		showViewById(toggleId);

		if (changePrice) {
			/* resetAllPriceData */
			resetAllPriceData();

			/* createAndShowNewPerformanceContent */
			createAndShowNewPerformanceContent(billingType);
		}

		var priceElement = createPriceElement(
			'campaignPrice',
			'preis',
			YAHOO.util.Dom.get('preis').value,
			billingType
		);
		var price2Element = createPriceElement(
			'campaignPrice2',
			'preis2',
			YAHOO.util.Dom.get('preis2').value,
			billingType
		);
               var priceElementHyprid = createPriceElement(
			'campaignPriceHyprid',
			'hybrid_preis',
			YAHOO.util.Dom.get('hybrid_preis').value,
			billingType
		);
		var price2ElementHyprid = createPriceElement(
			'campaignPriceHyprid2',
			'hybrid_preis2',
			YAHOO.util.Dom.get('hybrid_preis2').value,
			billingType
		);
		var unitPriceElement = createPriceElement(
			'campaignUnitPrice',
			'unit_price',
			YAHOO.util.Dom.get('unit_price').value,
			billingType
		);
		var priceTotalElement = createPriceElement(
			'campaignPriceTotal',
			'preis_gesamt',
			YAHOO.util.Dom.get('preis_gesamt').value,
			billingType
		);

		var priceBeginNode = document.createTextNode('Preis pro ');
		var priceEndNode = document.createTextNode(' \u20AC');

		var unitPriceBeginNode = document.createTextNode('Pauschale (optional): ');
		var unitPriceEndNode = document.createTextNode(' \u20AC');

		switch (billingType) {
			case 'Hybri':
			case 'Hybrid':
				var priceHybridBeginNode = document.createTextNode('Preis pro TKP: ');
				var priceHybridEndNode = document.createTextNode(' \u20AC');
                                
                                var priceBrokingHybridTKPBeginNode = document.createTextNode('Broking pro TKP: ');
				var priceBrokingHybridTKPEndNode = document.createTextNode(' \u20AC');
                                
                                var priceBrokingHybridCPXBeginNode = document.createTextNode('Broking pro CPX: ');
				var priceBrokingHybridCPXEndNode = document.createTextNode(' \u20AC');

				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceHybridBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceElement);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceHybridEndNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(createElement('br', ''));

				priceBeginNode.appendData('CPX: ');
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(price2Element);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceEndNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(createElement('br', ''));
                                                             
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBrokingHybridTKPBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceElementHyprid);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBrokingHybridTKPEndNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(createElement('br', ''));
                                                     
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBrokingHybridCPXBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(price2ElementHyprid);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBrokingHybridCPXEndNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(createElement('br', ''));

				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceElement);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceEndNode);
				break;

			case 'Festp':
			case 'Festpreis':
				var priceFestpreisBeginNode = document.createTextNode('Gesamtpreis: ');

				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceFestpreisBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceTotalElement);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceEndNode);
				break;

			default:
				priceBeginNode.appendData(billingType + ': ');

				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceElement);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(priceEndNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(createElement('br', ''));

				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceBeginNode);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceElement);
				YAHOO.util.Dom.get('priceByBillingTypeContent').appendChild(unitPriceEndNode);
				break;
		}

		var vo = 0, vk = 0;
		if (billingType === 'TKP' || billingType === 'Hybrid' || billingType === 'Hybri') {
			vo = YAHOO.util.Dom.get('vorgabe_o_old').value;
			vk = YAHOO.util.Dom.get('vorgabe_k_old').value;
		}

		YAHOO.util.Dom.get('vorgabe_o').value = vo;
		YAHOO.util.Dom.get('vorgabe_k').value = vk;
	} else {
		hideViewById(toggleId);

		resetAllPriceData();

		toggleViewById('volumeAndPerformance');
	}
}

function resetAllPriceData() {
	changePriceValue(0, 'preis', '', 'campaignPrice');
	changePriceValue(0, 'preis2', '', 'campaignPrice2');
        changePriceValue(0, 'hybrid_preis', '', 'campaignPriceHyprid');
	changePriceValue(0, 'hybrid_preis2', '', 'campaignPriceHyprid2');
	changePriceValue(0, 'unit_price', '', 'campaignUnitPrice');
	changePriceValue(0, 'preis_gesamt', '', 'campaignPriceTotal');
}

function createAndShowNewPerformanceContent(billingType) {
	if (newKampaign !== true) {
		var campaignPrice = YAHOO.util.Dom.get('preis').value;
		var campaignUnitPrice = YAHOO.util.Dom.get('unit_price').value;
		var campaignBooked = convertValue2Integer(YAHOO.util.Dom.get('volumen').value);
		var otherAttributes = '';

		switch (billingType) {
			case 'CPL':
			case 'CPO':
			case 'CPC':
				var campaignLeads = parseInt(YAHOO.util.Dom.get('leads').value);
				var campaignLeadsUnique = parseInt(YAHOO.util.Dom.get('leads_u').value);

				otherAttributes += '&campaignLeads=' + campaignLeads + '&campaignLeadsUnique=' + campaignLeadsUnique;
				break;

			case 'Hybri':
			case 'Hybrid':
				var campaignPrice2 = YAHOO.util.Dom.get('preis2').value;
				var campaignLeads = parseInt(YAHOO.util.Dom.get('leads').value);
				var campaignLeadsUnique = parseInt(YAHOO.util.Dom.get('leads_u').value);

				otherAttributes += '&campaignPrice2=' + campaignPrice2 + '&campaignLeads=' + campaignLeads + '&campaignLeadsUnique=' + campaignLeadsUnique;
				break;

			case 'Festp':
			case 'Festpreis':
				var campaignPriceTotal = YAHOO.util.Dom.get('preis_gesamt').value;

				otherAttributes += '&campaignPriceTotal=' + campaignPriceTotal;
				break;

			default:
				break;
		}

		processAjaxRequest(
			'AjaxRequests/ajaxCampaign.php',
			'actionMethod=getPerformanceContent&campaignPrice=' + campaignPrice 
				+ '&campaignUnitPrice=' + campaignUnitPrice 
				+ '&campaignBooked=' + campaignBooked 
				+ '&billingType=' + billingType 
				+ otherAttributes
			,
			'volumeAndPerformance',
			function() {}
		);
	}
}

function resetValueDataIfValueIsNull(value, inputId) {
	if (value === 0) {
		YAHOO.util.Dom.get(inputId).value = '';
	}
}

function showDeliverySystemStartDate(element, elementId) {
	if (parseInt(element.value) === 1) {
		
		hideViewById('campaignAdvertisings');
	} else {
		YAHOO.util.Dom.get(elementId + '_0').checked = true;

		showViewById('campaignAdvertisings');
	}
}

function validateAndCheckNvCampaignDate(mailingTyp) {
	validateCampaignDeliveryDate('date', 'campaign_versandHour', 'campaign_versandMin');

	if (mailingTyp === 'NV') {
		/**
		 * 
		 * @returns {Boolean}checkNvSendCampaignDate
		 */
		if (!checkNvSendCampaignDate(hvCampaignDateObject)) {
			alert(unescape("Versanddatum der NV mu%DF gr%F6%DFer sein als das HV Versanddatum: " + hvCampaignDateObject.toLocaleString() + " Uhr"));

			return false;
		}
	}
}

function enableOrDisableDeliverySystemByChangingImvs(element, campaignDs, campaignDsdId) {
	if (deliverySystem.value !== campaignDs) {
		// versandsystem wieder auf campaignDeliverySystem setzen
		var deliverySystemLenght = deliverySystem.length;
		for (var i = 0; i < deliverySystemLenght; i++) {
			if (deliverySystem[i].value === campaignDs) {
				deliverySystem[i].selected = true;
			}
		}

		changeDeliverySystem(
			campaignDs,
			'deliverySystemDistributor',
			1,
			campaignDsdId
		);
		deliverySystem.disabled = true;
	} else {
		if (parseInt(element.value) === 1) {
			changeDeliverySystem(
				campaignDs,
				'deliverySystemDistributor',
				1,
				campaignDsdId
			);

			deliverySystem.disabled = true;
		} else {
			changeDeliverySystem(
				campaignDs,
				'deliverySystemDistributor',
				0,
				0
			);

			deliverySystem.disabled = false;
		}
	}
}

function selectCampaignAdvertisingDownloadsByClassname(className) {
	processCampaignAdvertisingsDownloads(
		className,
		true
	);
	
	showViewById('compressAndDownloadFile_1454414330');
}
function unselectCampaignAdvertisingDownloadsByClassname(className) {
	processCampaignAdvertisingsDownloads(
		className,
		false
	);
	
	hideViewById('compressAndDownloadFile_1454414330');
}
function processCampaignAdvertisingsDownloads(className, checkedStatus) {
	var campaignAdvertisingElements = YAHOO.util.Dom.getElementsByClassName(className, 'input');
	for (var i = 0; i < campaignAdvertisingElements.length; i++) {
		campaignAdvertisingElements[i].checked = checkedStatus;
	}
}

function toogleCompressAndDownloadFile(element) {
	var campaignAdvertisingElements = YAHOO.util.Dom.getElementsByClassName(element.className, 'input');
	var campaignChecked = 0;
	
	if (element.checked === true) {
		showViewById('compressAndDownloadFile_1454414330');
	} else {
		for (var i = 0; i < campaignAdvertisingElements.length; i++) {
			if (campaignAdvertisingElements[i].checked) {
				campaignChecked++;
			}
		}
		
		if (campaignChecked === 0) {
			hideViewById('compressAndDownloadFile_1454414330');
		}
	}
}
function compressAndDownloadFile(className) {
	var postData = [];
	var campaignAdvertisingElements = YAHOO.util.Dom.getElementsByClassName(className, 'input');
	for (var i = 0; i < campaignAdvertisingElements.length; i++) {
		var checkboxElements = campaignAdvertisingElements[i];
		
		if (checkboxElements.checked) {
			postData.push(checkboxElements.name + '=' + encodeURIComponent(checkboxElements.value));
		}
	}
	postData.push('_module=Campaign');
	postData.push('_controller=Ajax');
	postData.push('_action=compressAndDownloadCampaignFiles');
	postData.push('campaign[k_id]=' + parseInt(YAHOO.util.Dom.get('kid').value));
	
	window.open('dispatch.php?' + postData.join('&'));
}


var tabView_status = new YAHOO.widget.TabView('tab_k_new');

(function() {
	var Event = YAHOO.util.Event, Dom = YAHOO.util.Dom;

	Event.onContentReady('datefields', function() {
		var oCalendarMenu;

		var onButtonClick = function() {
			var oCalendar = new YAHOO.widget.Calendar('buttoncalendar', oCalendarMenu.body.id);
			oCalendar.cfg.setProperty('START_WEEKDAY', [1]);
			oCalendar.cfg.setProperty('MONTHS_LONG', ['Januar', 'Februar', "M\u00E4rz", 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']);
			oCalendar.cfg.setProperty('WEEKDAYS_SHORT', ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']);
			oCalendar.cfg.setProperty('mindate', new Date());
			
			if (Dom.get('nJahr').value !== '') {
				oCalendar.cfg.setProperty('selected', Dom.get('nMonat').value + '/' + Dom.get('nTag').value + '/' + Dom.get('nJahr').value);
			}
			oCalendar.render();

			oCalendar.selectEvent.subscribe(function(p_sType, p_aArgs) {
				var aDate, nMonth, nDay, nYear;

				if (p_aArgs) {
					aDate = p_aArgs[0][0];
					nMonth = aDate[1];
					nDay = aDate[2];
					nYear = aDate[0];

					oButton.set('label', (nDay + '.' + nMonth + '.' + nYear));

					Dom.get('date').value = nYear + '-' + nMonth + '-' + nDay;

					validateCampaignDeliveryDate('date', 'campaign_versandHour', 'campaign_versandMin');

					/*
					 * Sync the Calendar instance's selected date with the date form fields
					 */
					Dom.get('month').selectedIndex = (nMonth - 1);
					Dom.get('day').selectedIndex = (nDay - 1);
					Dom.get('year').value = nYear;

					/**
					 * checkNvSendCampaignDate
					 */
					switch (Dom.get('mailingTyp').value) {
						case 'NV':
							if (!checkNvSendCampaignDate(hvCampaignDateObject)) {
								alert(unescape("Versanddatum der NV mu%DF gr%F6%DFer sein als das HV Versanddatum: " + hvCampaignDateObject.toLocaleString() + " Uhr"));

								return false;
							}
							break;
					}
				}

				oCalendarMenu.hide();
			});

			/**
			 * Pressing the Esc key will hide the Calendar Menu and send focus back to
			 * its parent Button
			 */
			Event.on(oCalendarMenu.element, 'keydown', function(p_oEvent) {
				if (Event.getCharCode(p_oEvent) === 27) {
					oCalendarMenu.hide();
					
					this.focus();
				}
			}, null, this);

			var focusDay = function() {
				var oCalendarTBody = Dom.get('buttoncalendar').tBodies[0],
					aElements = oCalendarTBody.getElementsByTagName('a'),
					oAnchor
				;

				if (aElements.length > 0) {
					Dom.batch(aElements, function(element) {
						if (Dom.hasClass(element.parentNode, 'today')) {
							oAnchor = element;
						}
					});

					if (!oAnchor) {
						oAnchor = aElements[0];
					}

					/**
					 * Focus the anchor element using a timer since Calendar will try 
					 * to set focus to its next button by default
					 */
					YAHOO.lang.later(0, oAnchor, function() {
						try {
							oAnchor.focus();
						} catch (e) {
						}
					});
				}
			};

			/**
			 * Set focus to either the current day, or first day of the month in
			 * the Calendar when it is made visible or the month changes
			 */
			oCalendarMenu.subscribe('show', focusDay);
			oCalendar.renderEvent.subscribe(focusDay, oCalendar, true);

			// Give the Calendar an initial focus
			focusDay.call(oCalendar);

			/**
			 * Re-align the CalendarMenu to the Button to ensure that it is in the correct
			 * position when it is initial made visible
			 */
			oCalendarMenu.align();

			/**
			 * Unsubscribe from the "click" event so that this code is
			 * only executed once
			 */
			this.unsubscribe('click', onButtonClick);
		};

		var oMonthField = Dom.get('month'),
			oDayField = Dom.get('day'),
			oYearField = Dom.get('year')
		;

		/**
		 * Hide the form fields used for the date so that they can be replaced by the
		 * calendar button.
		 */
		oMonthField.style.display = 'none';
		oDayField.style.display = 'none';
		oYearField.style.display = 'none';

		/* Create a Overlay instance to house the Calendar instance */
		oCalendarMenu = new YAHOO.widget.Overlay('calendarmenu', {visible: false});

		/* Create a Button instance of type "menu" */
		var oButton = new YAHOO.widget.Button({
			type: 'menu',
			id: 'calendarpicker',
			label: 'Datum ausw&auml;hlen',
			menu: oCalendarMenu,
			container: 'datefields'
		});

		var nTag = Dom.get('nTag').value;
		var nMonat = Dom.get('nMonat').value;
		var nJahr = Dom.get('nJahr').value;

		if (nTag !== '') {
			oButton.set('label', (nTag + '.' + nMonat + '.' + nJahr));
		}

		oButton.on('appendTo', function() {
			/**
			 * Create an empty body element for the Overlay instance in order
			 * to reserve space to render the Calendar instance into.
			 */
			oCalendarMenu.setBody('&#32;');
			oCalendarMenu.body.id = 'calendarcontainer';
		});

		/**
		 * Add a listener for the "click" event.  This listener will be
		 * used to defer the creation the Calendar instance until the 
		 * first time the Button's Overlay instance is requested to be displayed
		 * by the user.
		 */
		if (showOButton) {
			oButton.on('click', onButtonClick);
		}
	});
}());