function setRequest_preview(url, p) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send();
		request.onreadystatechange = interpretRequest_preview;
	}
}

// Request auswerten
function interpretRequest_preview() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('tab3').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var content = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('tab3').innerHTML = content;
				
				evalScript(content);
			}
			break;
			
		default:
			break;
	}
}