function setRequest_k_st(type) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		var kid_analyse = '', 
			s = ''
		;
		
		if (document.getElementById('kid_analyse')) {
			kid_analyse = document.getElementById('kid_analyse').value;
		}

		if (document.getElementById('search_a')) {
			s = document.getElementById('search_a').value;
		}

		if (type === 'start' || type === 'overview') {
			document.getElementById('analyse_selected').selected = true;
		}

		var url = 'kampagne/k_analyse.php';
		document.getElementById('k_charts').innerHTML = '<div style="height:400px;width:870px;"><table width="870" align="center"><tr><td height="420" align="center"><img src="img/ajax-loader.gif" /></td></tr></table></div>';

		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send('kid_analyse=' + kid_analyse + '&type=' + type + '&suchwort=' + s);
		request.onreadystatechange = interpretRequestka;
	}
}

// Request auswerten
function interpretRequestka() {
	switch (request.readyState){
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById('k_charts').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var content = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('k_charts').innerHTML = content;
				
				evalScript(content);

			}
			break;
			
		default:
			break;
	}
}