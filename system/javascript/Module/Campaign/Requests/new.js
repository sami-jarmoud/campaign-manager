function setRequestK(url) {
	request = createRequest();

	// überprüfen, ob Request erzeugt wurde
	if (!request) {
		alert('Kann keine XMLHTTP-Instanz erzeugen');
		
		return false;
	} else {
		request.open('POST', url, true);
		// Requestheader senden
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		// Request senden
		request.send();
		// Request auswerten
		request.onreadystatechange = interpretRequestK;
	}
}

// Request auswerten
function interpretRequestK() {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				var message = 'Fehler: ' + request.status + ' ' + request.statusText;
				document.getElementById('k_new').innerHTML = message;
				
				console.log(request);
				alert(message);
			} else {
				var contentA = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById('k_new').innerHTML = contentA;
				
				evalScript(contentA);
			}
			break;
			
		default:
			break;
	}

}