var uploader = new plupload.Uploader({
	runtimes: 'html5,flash,silverlight,html4',
	browse_button: 'pickfiles',
	container: document.getElementById('pluploadContainer'),
	url: 'upload.php',
	flash_swf_url: 'javascript/Vendor/plupload/Moxie.swf',
	silverlight_xap_url: 'javascript/Vendor/plupload/Moxie.xap',
	multi_selection: true,
	filters: {
		max_file_size: '10mb'
	},
	init: {
		PostInit: function() {
			document.getElementById('filelist').innerHTML = '';
			document.getElementById('uploadfiles').onclick = function() {
				uploader.start();
				
				return false;
			};
		},
		FilesAdded: function(up, files) {
			plupload.each(files, function(file) {
				document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + wordwrap(file.name, 50, '\n', true) + ' (' + plupload.formatSize(file.size) + ') <a href="javascript:;" id="deleteQueFile_' + file.id + '">Remove from Que</a> <b></b></div>';
				
				document.getElementById('deleteQueFile_' + file.id).onclick = function() {
					document.getElementById(file.id).remove();
					
					up.removeFile(file.id);
					up.refresh();
				};
			});
		},
		BeforeUpload: function(up, file) {
			var uploaderSettings = {
				'multipart_params': {
					'fileId': file.id
				}
			};
			up.setOption(uploaderSettings);
		},
		UploadProgress: function(up, file) {
			document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + '%</span>';
		},
		FileUploaded: function(up, file, responseObject) {
			try {
				if (parseInt(responseObject.status) === 200) {
					var jsonObject = JSON.parse(responseObject.response);
					
					if (jsonObject.result.message === 'success') {
						var uploadCampaignAdvertisingsFile = createElement('input', {
							type: 'hidden',
							name: 'upload[campaignAdvertisings][' + jsonObject.result.fileType + '][' + file.id + ']',
							value: jsonObject.result.fileName,
							id: jsonObject.result.fileName
						});
						
						document.getElementById('deleteQueFile_' + file.id).remove();
						document.getElementById('pluploadContainer').appendChild(uploadCampaignAdvertisingsFile);
						
						// TODO: add remove file from server
					} else {
						alert(jsonObject.result.message);
					}
				}
			} catch (e) {
				console.log(e);
				
				alert('error');
			}
		},
		Error: function(up, err) {
			document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ': ' + err.message));
		}
	}
});

 function wordwrap(str, int_width, str_break, cut) {
	
	  var m = ((arguments.length >= 2) ? arguments[1] : 75);
	  var b = ((arguments.length >= 3) ? arguments[2] : '\n');
	  var c = ((arguments.length >= 4) ? arguments[3] : false);

	  var i, j, l, s, r;

	  str += '';

	  if (m < 1) {
	    return str;
	  }

	  for (i = -1, l = (r = str.split(/\r\n|\n|\r/))
	    .length; ++i < l; r[i] += s) {
	    for (s = r[i], r[i] = ''; s.length > m; r[i] += s.slice(0, j) + ((s = s.slice(j))
	        .length ? b : '')) {
	      j = c == 2 || (j = s.slice(0, m + 1)
	        .match(/\S*(\s)?$/))[1] ? m : j.input.length - j[0].length || c == 1 && m || j.input.length + (j = s.slice(
	          m)
	        .match(/^\S*/))[0].length;
	    }
	  }

	  return r.join('\n');
	}
uploader.init();
