function setRequest_st(a, GenArt) {
    request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
        var monat = document.getElementById('stats_monat').value;
        var jahr = document.getElementById('stats_jahr').value;
        var import_partner = document.getElementById('stats_partner').value;
        var GenArt = document.getElementById('genart').value;
        var impnr = document.getElementById('impnr').value;
        var url2 = 'limport/statistik.php';
        
        document.getElementById('dm_charts').innerHTML = '<div style="height:400px;width:870px;"><table width="870" align="center"><tr><td height="420" align="center"><img src="img/ajax-loader.gif" /></td></tr></table></div>';
        
        request.open('POST', url2, true);
        // Requestheader senden
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        // Request senden
        request.send('import_monat=' + monat + '&import_jahr=' + jahr + '&GenArt=' + GenArt + '&analyse_req=' + a + '&import_partner=' + import_partner + '&impnr=' + impnr);
        // Request auswerten
        request.onreadystatechange = interpretRequest_analyse;
    }
}

// Request auswerten
function interpretRequest_analyse() {
    switch (request.readyState) {
        // wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
        case 4:
            if (request.status !== 200) {
                document.getElementById('dm_charts').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
            } else {
                var content_analyse = request.responseText;
                
                // den Inhalt des Requests in das <div> schreiben
                document.getElementById('dm_charts').innerHTML = content_analyse;
                
                evalScript(content_analyse);
            }
            break;
            
        default:
            break;
    }
}