function initDataManager() {
	var handleCancelStatistik = function() {
		document.selA.selAopt.options[0].selected = true;

		this.cancel();
	};
	
	var handleYes = function() {
		this.submit();

		setRequest('limport/index.php', 'a');

		this.hide();
	};

	var handleNo = function() {
		this.hide();
	};
	
	var handleSubmit_a_rep = function() {
		this.submit();

		var id_rep_edit = YAHOO.util.Dom.get('id_rep_edit').value;
		setRequest_default('limport/report/rep_details.php?id=' + id_rep_edit, 'center_content');
	};

	var handleSubmitStatus = function() {
		this.submit();

		setRequest('limport/index.php', 'a');
	};

	var handleSubmitDMKunde = function() {
		var kfehler = '';

		var firma_name = YAHOO.util.Dom.get('firma').value;
		if (firma_name == '') {
			kfehler = "- Bitte Firmennamen angeben\n";
		}

		if (kfehler == '') {
			this.submit();

			var kunde_id = YAHOO.util.Dom.get('dm_kunde_id').value;
			if (kunde_id != '') {
				setRequest_default('limport/kunde/details.php?id=' + kunde_id, 'basic_content');
			} else {
				setRequest('limport/kunde/', 'pa');
			}
		} else {
			alert(kfehler);

			return false;
		}
	};

	var handleSubmitDMKunde_ap = function() {
		this.submit();

		var kunde_id2 = YAHOO.util.Dom.get('dm_kunde_id2').value;
		setRequest_default('limport/kunde/details.php?id=' + kunde_id2, 'basic_content');
	};

	var handleDMDel_ap = function() {
		YAHOO.util.Dom.get('dm_del_ap_id').value = YAHOO.util.Dom.get('dm_ap_id_def').value;

		this.submit();

		YAHOO.util.Dom.get('dm_del_ap_id').value = '';
		YAHOO.util.Dom.get('dm_ap_id_def').value = '';

		var kunde_id2 = YAHOO.util.Dom.get('dm_kunde_id2').value;
		setRequest_default('limport/kunde/details.php?id=' + kunde_id2, 'basic_content');
	};

	var handleDMDel_kunde = function() {
		YAHOO.util.Dom.get('dm_del_kunde_id').value = YAHOO.util.Dom.get('dm_kunde_id_def').value;

		this.submit();

		YAHOO.util.Dom.get('dm_del_kunde_id').value = '';
		setRequest('limport/kunde/', 'dm_kunde');
	};
	
	
	
	YAHOO.example.container.container_dm_kunde = new YAHOO.widget.Dialog('container_dm_kunde', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmitDMKunde,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_dm_kunde.render();
	
	YAHOO.example.container.container_dm_kunde_ap = new YAHOO.widget.Dialog('container_dm_kunde_ap', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmitDMKunde_ap,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_dm_kunde_ap.validate = function() {
		var kapfehler = '';

		var ap_vorname = YAHOO.util.Dom.get('vorname').value;
		var ap_nachname = YAHOO.util.Dom.get('nachname').value;
		var ap_email = YAHOO.util.Dom.get('email').value;

		if (ap_vorname == '') {
			kapfehler = "- Bitte Vorname angeben\n";
		}

		if (ap_nachname == '') {
			kapfehler += "- Bitte Nachname angeben\n";
		}

		if (ap_email == '') {
			kapfehler += "- Bitte Email angeben\n";
		}

		if (kapfehler == '') {
			return true;
		} else {
			alert(kapfehler);

			return false;
		}
	};
	YAHOO.example.container.container_dm_kunde_ap.render();
	
	YAHOO.example.container.container_dm_del_kunde_ap = new YAHOO.widget.Dialog('container_dm_del_kunde_ap', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleDMDel_ap,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_dm_del_kunde_ap.render();
	
	YAHOO.example.container.container_dm_del_kunde = new YAHOO.widget.Dialog('container_dm_del_kunde', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleDMDel_kunde,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_dm_del_kunde.render();
	
	YAHOO.example.container.container_a_rep_editieren = new YAHOO.widget.Dialog('container_a_rep_editieren', {
		width: '450px',
		fixedcenter: true,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleSubmit_a_rep,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.container_a_rep_editieren.render();
	
	YAHOO.example.container.container_info_adr = new YAHOO.widget.Dialog('container_info_adr', {
		width: '500px',
		y: 10,
		x: 600,
		fixedcenter: false,
		visible: false,
		modal: false,
		close: true,
		constraintoviewport: true
	});
	YAHOO.example.container.container_info_adr.render();
	YAHOO.util.Event.addListener('button_details_adr', 'click', YAHOO.example.container.container_info_adr.show, YAHOO.example.container.container_info_adr, true);
	
	YAHOO.example.container.container_a_rep_vorlage = new YAHOO.widget.Dialog('container_a_rep_vorlage', {
		width: '450px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: 'speichern',
			handler: handleUpdate,
			isDefault: true
		}]
	});
	YAHOO.example.container.container_a_rep_vorlage.render();
	YAHOO.util.Event.addListener('a_rep_vorlage_button', 'click', YAHOO.example.container.container_a_rep_vorlage.show, YAHOO.example.container.container_a_rep_vorlage, true);
	
	YAHOO.example.container.del_datei = new YAHOO.widget.Dialog('del_datei', {
		width: '300px',
		fixedcenter: true,
		visible: false,
		constraintoviewport: true,
		buttons: [{
			text: 'l&ouml;schen',
			handler: handleYes,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleNo
		}]
	});
	YAHOO.example.container.del_datei.render();
	YAHOO.util.Event.addListener('button_del_a', 'click', YAHOO.example.container.del_datei.show, YAHOO.example.container.del_datei, true);
	
	YAHOO.example.container.statistik = new YAHOO.widget.Dialog('container_statistik', {
		width: '930px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		buttons: [{
			text: 'schliessen',
			handler: handleCancelStatistik
		}]
	});
	YAHOO.example.container.statistik.render();
	YAHOO.util.Event.addListener('button_import_analyse', 'click', YAHOO.example.container.statistik.show, YAHOO.example.container.statistik, true);
	
	YAHOO.example.container.adr_status = new YAHOO.widget.Dialog('adr_status', {
		width: '350px',
		fixedcenter: true,
		visible: false,
		modal: true,
		close: true,
		constraintoviewport: true,
		buttons: [{
			text: '&auml;ndern',
			handler: handleSubmitStatus,
			isDefault: true
		}, {
			text: 'abbrechen',
			handler: handleCancel_default
		}]
	});
	YAHOO.example.container.adr_status.render();
	YAHOO.util.Event.addListener('import_status_aendern', 'click', YAHOO.example.container.adr_status.show, YAHOO.example.container.adr_status, true);
}
YAHOO.util.Event.addListener(window, 'load', initDataManager);

var tabView_r = new YAHOO.widget.TabView('neu_rep_tab');
var tabView_p = new YAHOO.widget.TabView('neu_p_tab');