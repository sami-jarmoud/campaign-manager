function show(pid, monat) {
	var img = 'img' + pid + monat;
	var obj;

	obj = document.getElementById(img);

	var plus = new Image();
	plus.src = 'img/arrow_right.png';

	var minus = new Image();
	minus.src = 'img/arrow_down.png';

	obj.src = (obj.src === plus.src) ? minus.src : plus.src;

	for (i = 1; i <= 100; i++) {
		var feld = pid + monat + i;

		var vista = (document.getElementById(feld).style.display === 'none') ? '' : 'none';
		document.getElementById(feld).style.display = vista;
	}
}

function select_datei(evt, pid, monat, jahr) {
	document.getElementById('stats_monat').value = monat;
	document.getElementById('stats_jahr').value = jahr;
	document.getElementById('stats_partner').value = pid;
	document.getElementById('genart').value = 'Import';
	document.getElementById('impnr').value = 'all';

	var selectedRow = (evt.target || evt.srcElement);

	while (selectedRow.tagName !== 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';

		if (oldRow === selectedRow) {
			return true;
		}
	}

	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}

function doIt_adressen(evt, details_adressen, impnr, pid, art, partner, lieferdatum, brutto, netto, netto_ext, rep_info, notiz_rep, ap_anrede, ap, ap_email, status, abschlag, monat, jahr) {
	getXY(evt, 'a');

	var details_adressen = '';

	document.getElementById('import_status_aendern').style.display = 'none';
	document.getElementById('a_rep').style.display = 'none';
	document.getElementById('button_del_a').style.display = '';
	document.getElementById('button_del_a_deactive').style.display = 'none';

	document.getElementById('button_details_adr').onclick = function() {
		setRequest_default('limport/bottom_details.php?impnr=' + impnr, 'infocontent_adr');
	};

	document.getElementById('stats_monat').value = monat;
	document.getElementById('stats_jahr').value = jahr;
	document.getElementById('stats_partner').value = pid;
	document.getElementById('genart').value = 'Import';
	document.getElementById('impnr').value = impnr;
	document.getElementById('impnr_del').value = impnr;

	var impnr;
	var neu_q;
	var mailtext;
	var ext_q;
	var betreff;

	document.getElementById('page').value = 'limport/index.php';
	var page = document.getElementById('page').value;

	if (art != 'S') {
		document.getElementById('button_export2').style.display = '';
		document.getElementById('button_export2').onclick = function() {
			var url = 'datamanager/export/export.php?import_nr=' + impnr;
			window.open(url, '', 'width=800,height=725,scrollbars=1');
		}
	}

	if (art == 'S') {
		document.getElementById('button_del_a').style.display = 'none';
		document.getElementById('button_del_a_deactive').style.display = '';
	}

	if (art == 'H' || art == 'S') {
		document.getElementById('button_del_a').style.display = 'none';
		document.getElementById('import_status_aendern').style.display = '';
		document.getElementById('a_rep').style.display = '';
		document.getElementById('button_import_analyse').style.display = '';

		document.getElementById('import_status_aendern').onclick = function() {
			setRequestDataSt('limport/status.php?import_nr=' + impnr + '&typ=' + art);
		}

		document.getElementById('a_rep').onclick = function() {
			setRequest_default('limport/rep_details.php?import_nr=' + impnr + '&typ=' + art, 'neu_rep_tab');

			setRequest_st('all', 'Import');
		}
	}

	if (art == 'H') {
		document.getElementById('button_del_a').style.display = '';
		document.getElementById('button_del_a_deactive').style.display = 'none';
	}

	var selectedRow = (evt.target || evt.srcElement);
	while (selectedRow.tagName != 'TR') {
		selectedRow = selectedRow.parentNode;
	}

	if (oldRow) {
		oldRow.style.backgroundColor = '';
		oldRow.style.color = '#000000';

		if (oldRow == selectedRow) {
			return true;
		}
	}

	selectedRow.style.backgroundColor = '#bfdaff';
	oldRow = selectedRow;
}