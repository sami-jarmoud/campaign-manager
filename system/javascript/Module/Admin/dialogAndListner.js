function initAdmin() {
    var handleSubmit_user = function () {
        this.submit();

        setRequest('admin/user/index.php', 'admin_user');
    };

    var handleSubmit_clickProfil = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewClickProfil', 'admin_clickProfil');
    };

    var handleSubmit_dsd = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewDeliverySystemDistributor', 'admin_dsd');
    };

    var handleSubmit_dsdn = function () {
        this.submit();

        setRequest('dispatch.php?_module=Admin&_controller=index&_action=viewDeliverySystemDomain', 'admin_dsdn');
    };


    YAHOO.example.container.container_admin_user = new YAHOO.widget.Dialog('container_admin_user', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_user,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_user.validate = function () {
        var ufehler = '';

        var user_login = YAHOO.util.Dom.get('user_login').value;
        var user_pw = YAHOO.util.Dom.get('user_pw').value;
        var user_pw1 = YAHOO.util.Dom.get('user_pw1').value;
        var user_pw2 = YAHOO.util.Dom.get('user_pw2').value;

        if (user_login === '') {
            ufehler = "- Bitte Anmeldename angeben\n";
        }

        if (user_pw === ''
                && user_pw1 === ''
                && user_pw2 === ''
                ) {
            ufehler += "- Bitte Passwort angeben\n";
        }

        if (
                (
                        user_pw1 !== ''
                        || user_pw2 !== ''
                        ) && (
                user_pw1 !== user_pw2
                )
                ) {
            ufehler += "- Passwoerter stimmen nicht ueberein\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_user.render();
    YAHOO.util.Event.addListener('button_edit_user', 'click', YAHOO.example.container.container_admin_user.show, YAHOO.example.container.container_admin_user, true);
    YAHOO.util.Event.addListener('button_neu_user', 'click', YAHOO.example.container.container_admin_user.show, YAHOO.example.container.container_admin_user, true);

    YAHOO.example.container.container_admin_user_del = new YAHOO.widget.Dialog('container_admin_user_del', {
        width: '300px',
        fixedcenter: true,
        visible: false,
        constraintoviewport: true,
        buttons: [{
                text: 'l&ouml;schen',
                handler: handleSubmit_user,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_user_del.render();
    YAHOO.util.Event.addListener('button_del_user', 'click', YAHOO.example.container.container_admin_user_del.show, YAHOO.example.container.container_admin_user_del, true);

    YAHOO.example.container.container_admin_clickProfil = new YAHOO.widget.Dialog('container_admin_clickProfil', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_clickProfil,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_clickProfil.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('name').value;
        var shortcut = YAHOO.util.Dom.get('abkuerzung').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Name angeben\n";
        }

        if (shortcut === ''
                ) {
            ufehler += "- Bitte Abkürzung angeben\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_clickProfil.render();
    YAHOO.util.Event.addListener('button_neu_clickProfil', 'click', YAHOO.example.container.container_admin_clickProfil.show, YAHOO.example.container.container_admin_clickProfil, true);
    YAHOO.util.Event.addListener('button_edit_clickProfil', 'click', YAHOO.example.container.container_admin_clickProfil.show, YAHOO.example.container.container_admin_clickProfil, true);

    YAHOO.example.container.container_admin_dsdn = new YAHOO.widget.Dialog('container_admin_dsdn', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_dsdn,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    
        YAHOO.example.container.container_admin_dsdn.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('domain').value;
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Versanddomain angeben\n";
        }
        
        if (deliverySystem === ''
                ) {
            ufehler += "- Bitte Versandsystem ausw&auml;hlen\n";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };

        

    YAHOO.example.container.container_admin_dsdn.render();
    YAHOO.util.Event.addListener('button_neu_dsdn', 'click', YAHOO.example.container.container_admin_dsdn.show, YAHOO.example.container.container_admin_dsdn, true);
    YAHOO.util.Event.addListener('button_edit_dsdn', 'click', YAHOO.example.container.container_admin_dsdn.show, YAHOO.example.container.container_admin_dsdn, true); 


    YAHOO.example.container.container_admin_dsd = new YAHOO.widget.Dialog('container_admin_dsd', {
        width: '400px',
        fixedcenter: true,
        visible: false,
        modal: true,
        close: true,
        constraintoviewport: true,
        buttons: [{
                text: 'speichern',
                handler: handleSubmit_dsd,
                isDefault: true
            }, {
                text: 'abbrechen',
                handler: handleCancel_default
            }]
    });
    YAHOO.example.container.container_admin_dsd.validate = function () {
        var ufehler = '';

        var name = YAHOO.util.Dom.get('name').value;
        var description = YAHOO.util.Dom.get('description').value;
        var deliverySystem = YAHOO.util.Dom.get('deliverySystem').value;
        var externe_id = YAHOO.util.Dom.get('externe_id').value;

        if (name === ''
                ) {
            ufehler = "- Bitte Name angeben\n";
        }

        if (description === ''
                ) {
            ufehler += "- Bitte Beschreibung angeben\n";
        }

        if (deliverySystem === ''
                ) {
            ufehler += "- Bitte Versandsystem auswählen\n";
        }

        if (externe_id === ''
                ) {
            ufehler += "- Bitte Externe ID angeben\nExternal ID pflegt der Mitarbeiter die Kennung aus Optivo oder Kajomi ein";
        }

        if (ufehler === '') {
            return true;
        } else {
            alert(ufehler);

            return false;
        }
    };
    YAHOO.example.container.container_admin_dsd.render();
    YAHOO.util.Event.addListener('button_neu_dsd', 'click', YAHOO.example.container.container_admin_dsd.show, YAHOO.example.container.container_admin_dsd, true);
    YAHOO.util.Event.addListener('button_edit_dsd', 'click', YAHOO.example.container.container_admin_dsd.show, YAHOO.example.container.container_admin_dsd, true);

}
YAHOO.util.Event.addListener(window, 'load', initAdmin);