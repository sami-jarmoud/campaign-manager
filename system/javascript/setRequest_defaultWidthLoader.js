function setRequest_default2(url, zieldiv) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		document.getElementById(zieldiv).innerHTML = '<table><tr><td style="width:780px;height:370px;text-align:center;border:0px"><img src="img/ajax_loader_3d.gif" /></td></tr></table>';
		
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send();
		request.onreadystatechange = function() {
			interpretRequestdef2(zieldiv);
		};
	}
}


function setRequest_ajax(url, zieldiv) {
	request = createRequest();
    
    // überprüfen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		document.getElementById(zieldiv).innerHTML = '<table><tr><td style="width:780px;height:370px;text-align:center;border:0px"><img src="img/ajax_loader_3d.gif" /></td></tr></table>';
		
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send();
		request.onreadystatechange = function() {
			interpretRequestdef2(zieldiv);
		};
	}
}


// Request auswerten
function interpretRequestdef2(zieldiv) {
	switch (request.readyState) {
		// wenn der readyState 4 und der request.status 200 ist, dann ist alles korrekt gelaufen
		case 4:
			if (request.status !== 200) {
				document.getElementById(zieldiv).innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
			} else {
				var content2 = request.responseText;
				
				// den Inhalt des Requests in das <div> schreiben
				document.getElementById(zieldiv).innerHTML = content2;
				
				evalScript(content2);
			}
			break;
			
		default:
			break;
	}
}