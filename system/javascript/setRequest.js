function setRequest(url, p) {
	request = createRequest();
    
    // �berpr�fen, ob Request erzeugt wurde
    if (!request) {
        alert('Kann keine XMLHTTP-Instanz erzeugen');
        
        return false;
    } else {
		suchwort = '',
			beginn = '',
			ende = '',
			searchExtendendParameter = ''
		;
		
        YAHOO.util.Dom.get('loaderimg').style.visibility="visible";
        YAHOO.util.Dom.get('loaderimg').innerHTML = '<img src="img/k_loader.gif" />';
        YAHOO.util.Dom.get('standard').style.opacity=0.5;
        YAHOO.util.Dom.get('standard').style.filter="progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
        YAHOO.util.Dom.get('menu_buttons').style.opacity=0.5;
        YAHOO.util.Dom.get('menu_buttons').style.filter="progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
		
		console.groupCollapsed(arguments.callee.name);
        
		/**
		 * processCampaignSearch
		 */
		processCampaignSearch(
			p,
			url
		);
        
        
		/**
		 * hideMenuView
		 */
		// campaignManager
		hideCampaignMenuViews();
		
		// dataManager
		hideDataManagerMenuViews();
		
		// blacklistUnsubscribe
		hideBlacklistUnsubscribeMenuView();
		
		// admin
		hideAdminMenuView();
		
		//adminParameter
		hideAdminParameterMenuView();
		
		//admiClickProfil
		hideAdminClickProfilMenuView();

                //admiDeliverySystemDomain
                hideAdminDSDomainMenuView();
                
		//admiDeliverySystemDistributor
		hideAdminDSDMenuView();	
        
		
        YAHOO.util.Dom.get('error').value = '1';
        
		/**
		 * processView
		 */
		// campaignManager
		processCampaignViews(p);
		
		// dataManager
		processDataManagerView(p);
		
		// blacklistUnsubscribe
		processBlacklistUnsubscribeView(p);
		
		// dais
		processDaisView(p);
		
		// admin user
		processAdminView(p);
		
		//admin Parameter
		processAdminParameterView(p);
		
		//admin Click Profil
		processAdminClickProfilView(p);
		
                //admin Delivery System Domain
                processAdminDeliverySystemDomainView(p);
		
              //admin DeliverySystemDistributor
		processAdminDSDView(p);	
			
		
			console.groupCollapsed('arguments');
				console.log('view: ' + p);
				console.log('url: ' + url);
				console.log('suchwort: ' + suchwort);
				console.log('beginn: ' + beginn);
				console.log('ende: ' + ende);
				console.log('searchExtendendParameter: ' + searchExtendendParameter);
			console.groupEnd();
		console.groupEnd();
        
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send('suchwort=' + suchwort + '&beginn=' + beginn + '&ende=' + ende + searchExtendendParameter);
        request.onreadystatechange = interpretRequest;
    }
}

function processCampaignSearch(view, url) {
	if (campaignManagerView) {
		if (YAHOO.util.Dom.get('su').value !== '') {
			YAHOO.util.Dom.get('suchwort').value = YAHOO.util.Dom.get('su').value;
		}

		if (YAHOO.util.Dom.get('suchwort') !== undefined) {
			YAHOO.util.Dom.get('suchwort').onchange = function() {
				YAHOO.util.Dom.get('su').value = YAHOO.util.Dom.get('suchwort').value;
			};
		}

		suchwort = YAHOO.util.Dom.get('suchwort').value
			? encodeURIComponent(YAHOO.util.Dom.get('suchwort').value)
			: (YAHOO.util.Dom.get('su').value
				? encodeURIComponent(YAHOO.util.Dom.get('su').value)
				: ''
			)
		;

		if (YAHOO.util.Dom.get('in')) {
			beginn = YAHOO.util.Dom.get('in').value;
		}

		if (YAHOO.util.Dom.get('out')) {
			ende = YAHOO.util.Dom.get('out').value;
		}

		if (view === 'k'
			|| view === 'kSearch'
		) {
			processKAndKSearch();
		}

		if (url === 'kampagne/k_main.php?new=2') {
			YAHOO.util.Dom.get('su').value = '';

			suchwort = '';
			beginn = '';
			ende = '';
			searchExtendendParameter = '';
		}
		
		console.log(arguments.callee.name);
	}
}

function processKAndKSearch() {
	if (YAHOO.util.Dom.get('kAgentur') !== undefined 
		&& YAHOO.util.Dom.get('kAgentur').value !== ''
	) {
		searchExtendendParameter += '&kAgentur=' + YAHOO.util.Dom.get('kAgentur').value;
	}

	if (YAHOO.util.Dom.get('kAnsprechpartner') !== undefined 
		&& YAHOO.util.Dom.get('kAnsprechpartner').value !== ''
	) {
		searchExtendendParameter += '&kAnsprechpartner=' + YAHOO.util.Dom.get('kAnsprechpartner').value;
	}

	if (YAHOO.util.Dom.get('kEditors') !== undefined 
		&& YAHOO.util.Dom.get('kEditors').value !== ''
	) {
		searchExtendendParameter += '&kEditors=' + YAHOO.util.Dom.get('kEditors').value;
	}

	if (YAHOO.util.Dom.get('kStatus') !== undefined 
		&& YAHOO.util.Dom.get('kStatus').value !== ''
	) {
		searchExtendendParameter += '&kStatus=' + YAHOO.util.Dom.get('kStatus').value;
	}

	if (YAHOO.util.Dom.get('kBillingType') !== undefined 
		&& YAHOO.util.Dom.get('kBillingType').value !== ''
	) {
		searchExtendendParameter += '&kBillingType=' + YAHOO.util.Dom.get('kBillingType').value;
	}

	if (YAHOO.util.Dom.get('kDeliverySystem') !== undefined 
		&& YAHOO.util.Dom.get('kDeliverySystem').value !== ''
	) {
		searchExtendendParameter += '&kDeliverySystem=' + YAHOO.util.Dom.get('kDeliverySystem').value;
	}

	if (YAHOO.util.Dom.get('kDeliverySystemDistributorItems') !== undefined 
		&& YAHOO.util.Dom.get('kDeliverySystemDistributorItems').value !== ''
	) {
		searchExtendendParameter += '&kDsdId=' + YAHOO.util.Dom.get('kDeliverySystemDistributorItems').value;
	}

	if (YAHOO.util.Dom.get('kExtendedView') !== undefined) {
		if (YAHOO.util.Dom.get('kExtendedView').checked) {
			searchExtendendParameter += '&kExtendedView=' + YAHOO.util.Dom.get('kExtendedView').value;

			var hvCampaignView = YAHOO.util.Dom.getElementsByClassName('hvCampaignView', 'input');
			for (var i = 0; i < hvCampaignView.length; i++) {
				if (YAHOO.util.Dom.get(hvCampaignView[i]).checked) {
					searchExtendendParameter += '&kCampaignSettings[hvCampaignView]=' + YAHOO.util.Dom.get(hvCampaignView[i]).value;
				}
			}

			var showNvCampaign = YAHOO.util.Dom.getElementsByClassName('showNvCampaign', 'input');
			for (var i = 0; i < showNvCampaign.length; i++) {
				if (YAHOO.util.Dom.get(showNvCampaign[i]).checked) {
					searchExtendendParameter += '&kCampaignSettings[showNvCampaign]=' + YAHOO.util.Dom.get(showNvCampaign[i]).value;
				}
			}
		}
	}
	
	// TODO: remove, veraltet
	if (YAHOO.util.Dom.get('kOldSelektion') !== undefined 
		&& YAHOO.util.Dom.get('kOldSelektion').value !== ''
	) {
		searchExtendendParameter += '&kOldSelektion=' + YAHOO.util.Dom.get('kOldSelektion').value;
	}
}


function hideCampaignMenuViews() {
	if (campaignManagerView) {
		YAHOO.util.Dom.get('menu_cm_ov').style.display = 'none';
		YAHOO.util.Dom.get('menu_kampagnen').style.display = 'none';
		YAHOO.util.Dom.get('menu_planer').style.display = 'none';
		YAHOO.util.Dom.get('menuInvoice').style.display = 'none';
		YAHOO.util.Dom.get('menu_cm_kunde').style.display = 'none';
		YAHOO.util.Dom.get('menu_umsatz').style.display = 'none';

		YAHOO.util.Dom.get('status_aendern').style.display = 'none';
		YAHOO.util.Dom.get('button_add_k').style.display = 'none';
		YAHOO.util.Dom.get('button_edit_k').style.display = 'none';
		YAHOO.util.Dom.get('button_edit_nv').style.display = 'none';
		YAHOO.util.Dom.get('button_del_k').style.display = 'none';
		YAHOO.util.Dom.get('button_sync_k').style.display = 'none';
		YAHOO.util.Dom.get('button_empfehlung').style.display = 'none';
		YAHOO.util.Dom.get('button_copy').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideDataManagerMenuViews() {
	if (dataManagerView) {
		YAHOO.util.Dom.get('menu_adressen').style.display = 'none';
		YAHOO.util.Dom.get('menu_partner').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideBlacklistUnsubscribeMenuView() {
	if (blacklistUnsubscribeView) {
		YAHOO.util.Dom.get('menu_abmeldung').style.display = 'none';
		YAHOO.util.Dom.get('menu_blacklist').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideAdminMenuView() {
	if (adminView) {
		YAHOO.util.Dom.get('menu_admin_user').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideAdminParameterMenuView() {
	if (adminView) {
		YAHOO.util.Dom.get('menu_admin_parameter').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideAdminClickProfilMenuView() {
	if (adminView) {
		YAHOO.util.Dom.get('menu_admin_clickProfil').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}

function hideAdminDSDomainMenuView() {
	if (adminView) {
		YAHOO.util.Dom.get('menu_admin_dsdn').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}
function hideAdminDSDMenuView() {
	if (adminView) {
		YAHOO.util.Dom.get('menu_admin_dsd').style.display = 'none';
		
		console.log(arguments.callee.name);
	}
}


function processCampaignViews(view) {
	if (campaignManagerView) {
		switch (view) {
			case 'cm_kunde':
				YAHOO.util.Dom.get('menu_cm_kunde').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/office-address-book.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Campaign Manager | Kunden';
				break;

			case 'cm_ov':
				YAHOO.util.Dom.get('menu_cm_ov').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Oxygen/16/actions/help-about.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = '&Uuml;bersicht';
				break;

			case 'k':
			case 'kSearch':
				YAHOO.util.Dom.get('del_k_id').value = '';
				YAHOO.util.Dom.get('menu_kampagnen').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/office-address-book.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Campaign Manager';

				if (view === 'kSearch') {
					resetKSearch();
				}
				break;

			case 'invoice':
			case 'invoiceFilter':
				YAHOO.util.Dom.get('menuInvoice').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/22/apps/office-calendar.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Campaign Manager | Rechnungen';
				break;

			case 'p':
				YAHOO.util.Dom.get('menu_planer').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/22/apps/office-calendar.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Campaign Manager | Planer';
				break;

			case 'u':
				YAHOO.util.Dom.get('menu_umsatz').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/icon_euro_18.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Campaign Manager | Ums&auml;tze';
				break;
		}
		
		console.log(arguments.callee.name);
	}
}

function processDataManagerView(view) {
	if (dataManagerView) {
		switch (view) {
			case 'a':
				YAHOO.util.Dom.get('menu_adressen').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/aesthetica/16/database.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Data Manager | Importe';
				break;

			case 'pa':
				YAHOO.util.Dom.get('menu_partner').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/mimetypes/office-contact.png" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Data Manager | Partner';
				break;
		}

		console.log(arguments.callee.name);
	}
}

function processDaisView(view) {
	if (daisView) {
		if (view === 'dais') {
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/22/actions/help-faq.png" width="16" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Datenauskunftssystem';
		}

		console.log(arguments.callee.name);
	}
}

function processBlacklistUnsubscribeView(view) {
	if (blacklistUnsubscribeView) {
		switch (view) {
			// unsubscribe
			case 'unsub':
				YAHOO.util.Dom.get('menu_abmeldung').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Oxygen/22/actions/mail-mark-important.png" width="16" />';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Blacklist-Abmelde-Manager | Abmeldung';

				resetValues();

				if (YAHOO.util.Dom.get('su_abmeldung').value !== '') {
					suchwort = YAHOO.util.Dom.get('su_abmeldung').value;
				}

				if (YAHOO.util.Dom.get('in_unsub').value !== '') {
					beginn = YAHOO.util.Dom.get('in_unsub').value;
				}

				if (YAHOO.util.Dom.get('out_unsub').value !== '') {
					ende = YAHOO.util.Dom.get('out_unsub').value;
				}
				break;

				// blacklist
			case 'b':
				YAHOO.util.Dom.get('menu_blacklist').style.display = '';
				YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/22/actions/mail-mark-important-deactive.png" width="16"/>';
				YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Blacklist-Abmelde-Manager | Blacklist';

				resetValues();

				if (YAHOO.util.Dom.get('suchwort_bl').value !== '') {
					suchwort = YAHOO.util.Dom.get('suchwort_bl').value;
				}

				if (YAHOO.util.Dom.get('in_bl').value !== '') {
					beginn = YAHOO.util.Dom.get('in_bl').value;
				}

				if (YAHOO.util.Dom.get('out_bl').value !== '') {
					ende = YAHOO.util.Dom.get('out_bl').value;
				}

				if (YAHOO.util.Dom.get('bearbeiter_bl').value !== '') {
					searchExtendendParameter += '&bearbeiter_bl=' + YAHOO.util.Dom.get('bearbeiter_bl').value;
				}
				break;
		}

		console.log(arguments.callee.name);
	}
}

function processAdminView(view) {
	if (adminView) {
		if (view === 'admin_user') {
			YAHOO.util.Dom.get('menu_admin_user').style.display = '';
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/preferences-users.png" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Administration | Benutzerverwaltung';
		}

		console.log(arguments.callee.name);
	}
}

function processAdminParameterView(view){
	if(adminView){
		if (view === 'admin_parameter') {
			YAHOO.util.Dom.get('menu_admin_parameter').style.display = '';
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/preferences-users.png" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Administration | Mandantverwaltung';
		}

		console.log(arguments.callee.name);
	}
}

function processAdminClickProfilView(view){
	if(adminView){
		if (view === 'admin_clickProfil') {
			YAHOO.util.Dom.get('menu_admin_clickProfil').style.display = '';
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/preferences-users.png" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Administration | Klickprofilsverwaltung';
		}

		console.log(arguments.callee.name);
	}
}

function processAdminDeliverySystemDomainView(view){
	if(adminView){
		if (view === 'admin_dsdn') {
			YAHOO.util.Dom.get('menu_admin_dsdn').style.display = '';
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/preferences-users.png" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Administration | Versanddomaenen';
		}

		console.log(arguments.callee.name);
	}
}

function processAdminDSDView(view){
	if(adminView){
		if (view === 'admin_dsd') {
			YAHOO.util.Dom.get('menu_admin_dsd').style.display = '';
			YAHOO.util.Dom.get('menu_top_img').innerHTML = '<img src="img/Tango/16/apps/preferences-users.png" />';
			YAHOO.util.Dom.get('menu_top_label').innerHTML = 'Administration | Verteilerlisteverwaltung';
		}

		console.log(arguments.callee.name);
	}
}



function resetKSearch() {
	YAHOO.util.Dom.get('kAgentur').selectedIndex = '';

	hideViewById('kAp');
	YAHOO.util.Dom.get('kAnsprechpartner').selectedIndex = '';
	YAHOO.util.Dom.get('kEditors').selectedIndex = '';
	YAHOO.util.Dom.get('kStatus').selectedIndex = '';
	YAHOO.util.Dom.get('kBillingType').selectedIndex = '';

	hideViewById('kDeliverySystemDistributorCell');
	YAHOO.util.Dom.get('kDeliverySystemDistributorContent').innerHTML = '';
	YAHOO.util.Dom.get('kDeliverySystemDistributorItems').innerHTML = '';
	YAHOO.util.Dom.get('kDeliverySystem').selectedIndex = '';

	hideViewById('kCampaignSettings');
	YAHOO.util.Dom.get('kExtendedView').checked = false;
	hideViewById('hvCampaignView');
	hideViewById('showNvCampaign');

	YAHOO.util.Dom.get('in').value = '',
		YAHOO.util.Dom.get('out').value = '',
		YAHOO.util.Dom.get('suchwort').value = '',
		YAHOO.util.Dom.get('su').value = ''
	;
	
	// TODO: remove
	YAHOO.util.Dom.get('kOldSelektion').selectedIndex = '';
}

function resetValues() {
	suchwort = '',
		beginn = '',
		ende = ''
	;
	
	if (YAHOO.util.Dom.get('suchwort') !== null) {
		YAHOO.util.Dom.get('suchwort').value = '';
	}
}

// Request auswerten
function interpretRequest() {
    switch (request.readyState) {
        case 4:
            if (request.status !== 200) {
                YAHOO.util.Dom.get('standard').innerHTML = 'Fehler: ' + request.status + ' ' + request.statusText;
            } else {
                YAHOO.util.Dom.get('bottom').innerHTML = '';
                YAHOO.util.Dom.get('loaderimg').style.visibility = 'hidden';
                
                var content = request.responseText;
				
                YAHOO.util.Dom.get('standard').innerHTML = content;
                YAHOO.util.Dom.get('standard').style.opacity = 1;
                YAHOO.util.Dom.get('standard').style.filter = "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                YAHOO.util.Dom.get('menu_buttons').style.opacity = 1;
                YAHOO.util.Dom.get('menu_buttons').style.filter = "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
                
                evalScript(content);
            }
            break;
    }
}