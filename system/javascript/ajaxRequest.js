// depracate, use: processAjaxRequest
function setRequest_newCampaign(url, postData, zielDiv, callbackObject) {
    var callback = {
        success: function(response) {
            //var fixedResponse = response.responseText.replace(/\\"/g, "'");
            var jsonObj = YAHOO.lang.JSON.parse(response.responseText);
            
            if (zielDiv !== '') {
                setTimeout(function() {
                    YAHOO.util.Dom.get(zielDiv).innerHTML = jsonObj.content;
                }, 10);
            }
            
            if (jsonObj.jsonData !== null) {
                callbackObject(jsonObj.jsonData);
            } else {
				if (zielDiv === '') {
					callbackObject(jsonObj.content);
				} else {
					callbackObject();
				}
            }
        },
        failure: function(response) {
            var errorMsg = 'HTTP status code: ' + response.status + "\n" 
                + 'Message: ' + response.statusText
            ;
            
            alert(errorMsg);
        }
    };
    
    // Start the transaction.
    var ajaxRequest = YAHOO.util.Connect.asyncRequest(
        'POST',
        url,
        callback,
        postData
    );
}


function processAjaxRequest(url, postData, zielDiv, callbackObject) {
    var callback = {
        success: function(response) {
            var jsonObj = YAHOO.lang.JSON.parse(response.responseText);
            
            if (zielDiv !== '') {
                setTimeout(function() {
                    YAHOO.util.Dom.get(zielDiv).innerHTML = jsonObj.content;
                }, 10);
            }
            
            if (jsonObj.jsonData !== null) {
                callbackObject(jsonObj.jsonData);
            } else {
				if (zielDiv === '') {
					callbackObject(jsonObj.content);
				} else {
					callbackObject();
				}
            }
        },
        failure: function(response) {
            var errorMsg = 'HTTP status code: ' + response.status + "\n" 
                + 'Message: ' + response.statusText
            ;
            
            alert(errorMsg);
        }
    };
    
    // Start the transaction.
    var ajaxRequest = YAHOO.util.Connect.asyncRequest(
        'POST',
        url,
        callback,
        postData
    );
}

function getLoaderImage() {
	return '<div style="height:100%;text-align:center;"><img src="img/ajax_loader_3d.gif" border="0" style="padding-top: 10em;" /></div>';
}