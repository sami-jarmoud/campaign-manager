<?php
require_once('../emsConfig.php');
require_once('../EmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();

\DebugAndExceptionUtils::sendDebugData(
	array(
		'get' => $_GET,
		'client' => $_SESSION['mandant'],
		'benutzer' => $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname']
	),
	__FILE__
);
die('deaktiviert');



header('Content-Type: text/xml; charset=ISO-8859-1');
session_start();

// debug
function firePhp () {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

function getData($data) {
    $daten = str_replace(' ', ';', $data);
    $pieces = explode(';', $daten);
    
    return $pieces[0];
}

echo '<div style="background-color:#FFFFFF">
        <div id="text" style="font-family:arial;margin:auto;width:230px;"><img src="img/ajax-loader.gif" /></div>
    </div>'
;

$mandant = $_SESSION['mandant'];
$mandantID = $_SESSION['mID'];
$kid = isset($_GET['kid']) ? $_GET['kid'] : '';

include('../db_connect.inc.php');

$firePhp = firePhp();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
    $firePhp->log($_GET, '$_GET');
}

if ($_GET['a_update'] == '1') {
    if ($kid == '') {
        $mail_id_daten = mysql_query(
            'SELECT * FROM `' . $kampagne_db . '` WHERE `mail_id` != "" ORDER BY `datum` DESC LIMIT 0,50',
            $verbindung
        );
    } else {
        $mail_id_daten = mysql_query(
            'SELECT * FROM `' . $kampagne_db . '` WHERE (`k_id` = "' . $kid . '" OR `nv_id` = "' . $kid . '") AND  `mail_id` != ""',
            $verbindung
        );
    }
    if ($mail_id_daten) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('mail_id_daten');
        }
        
        while (($zeile_hv = mysql_fetch_array($mail_id_daten)) !== false) {
            $update_sync = '';
            $update_status = '';
            $abmelderCnt = '';
            
            $m_id = $zeile_hv['mail_id'];
            $versandsystem = $zeile_hv['versandsystem'];
            $datum_versand = $zeile_hv['datum'];
            $status = $zeile_hv['status'];
            $start_datum = $zeile_hv['gestartet'];
            $end_datum = $zeile_hv['beendet'];
            $hash = $zeile_hv['hash'];
            $heute = time();
            
            if ($m_id != '') {
                switch ($versandsystem) {
                    case 'BM':
                        require_once('../bm_soap/broadmail_rpc.php');
                        require_once('../bm_soap/bm_' . $mandant . '.php');
                        
                        $factory = new BroadmailRpcFactory(
                            $bmMID,
                            $bmUser,
                            $bmPass
                        );
                        if ($factory->getError()) {
                            // debug
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($factory->getError(), 'Broadmail: Fehler beim Login');
                            }

                            die('Broadmail: Fehler beim Login <code>' . $factory->getError() . '</code>');
                        }
                        
                        $MailingWebservice = $factory->newMailingWebservice();
                        $MailingReportingWebservice = $factory->newMailingReportingWebservice();
                        $ResponseWebservice = $factory->newResponseWebservice();
                        
                        $y = date('Y', $datum_versand);
                        $m = date('m', $datum_versand);
                        $d = date('d', $datum_versand);
                        $datum_versand = mktime(10, 0, 0, $m, $d, $y);
                        $morgen = $heute + 43000;
                        
                        $openings = $MailingReportingWebservice->getOpenCount(
                            $m_id,
                            true
                        );
                        if ($MailingReportingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingReportingWebservice, 'Error -> getOpenCount: openings');
                            }

                            die('BroadmailError: MailingReportingWebservice');
                        }
                        
                        $openings_all = $MailingReportingWebservice->getOpenCount(
                            $m_id,
                            false
                        );
                        if ($MailingReportingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingReportingWebservice, 'Error -> getOpenCount: openings_all');
                            }

                            die('BroadmailError: MailingReportingWebservice');
                        }
                        
                        $klicks = $MailingReportingWebservice->getClickCount(
                            $m_id,
                            true
                        );
                        if ($MailingReportingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingReportingWebservice, 'Error -> getClickCount: klicks');
                            }

                            die('BroadmailError: MailingReportingWebservice');
                        }
                        
                        $klicks_all = $MailingReportingWebservice->getClickCount(
                            $m_id,
                            false
                        );
                        if ($MailingReportingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingReportingWebservice, 'Error -> getClickCount: klicks_all');
                            }

                            die('BroadmailError: MailingReportingWebservice');
                        }

                        $softbounces = $ResponseWebservice->getMailingResponseCount(
                            'softbounce',
                            $m_id
                        );
                        if ($ResponseWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($ResponseWebservice, 'Error -> getMailingResponseCount: softbounce');
                            }

                            die('BroadmailError: ResponseWebservice');
                        }

                        $hardbounces = $ResponseWebservice->getMailingResponseCount(
                            'hardbounce',
                            $m_id
                        );
                        if ($ResponseWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($ResponseWebservice, 'Error -> getMailingResponseCount: hardbounce');
                            }

                            die('BroadmailError: ResponseWebservice');
                        }
                        
                        if ($urlInternArr) {
                            $getInternClicks = $MailingReportingWebservice->getClickCountByUrl(
                                $m_id,
                                $urlInternArr,
                                true
                            );
                            if ($MailingReportingWebservice->getError()) {
                                if ($firePhp instanceof FirePHP) {
                                    $firePhp->log($MailingReportingWebservice, 'Error -> getClickCountByUrl');
                                }

                                die('BroadmailError: MailingReportingWebservice');
                            }

                            $getInternClicks_all = $MailingReportingWebservice->getClickCountByUrl(
                                $m_id,
                                $urlInternArr,
                                false
                            );
                            if ($MailingReportingWebservice->getError()) {
                                if ($firePhp instanceof FirePHP) {
                                    $firePhp->log($MailingReportingWebservice, 'Error -> getClickCountByUrl');
                                }

                                die('BroadmailError: MailingReportingWebservice');
                            }

                            if (count($getInternClicks) > 0) {
                                $getSumInternClicks = array_sum($getInternClicks);
                                $getSumInternClicks_all = array_sum($getInternClicks_all);

                                $klicks = $klicks - $getSumInternClicks;
                                $klicks_all = $klicks_all - $getSumInternClicks_all;
                            }
                        }
                        
                        $start = $MailingWebservice->getSendingStartedDate($m_id);
                        if ($MailingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingWebservice, 'Error -> getSendingStartedDate');
                            }
                            
                            die('BroadmailError: MailingWebservice');
                        }
                        if (strpos($start, 'T')) {
                            $date_ = explode('T', $start);
                            $datum = $date_[0];
                            $zeit = substr($date_[1], 0, 5);
                            $date_teile = explode('-', $date_[0]);
                            $zeit_teile = explode(':', $zeit);
                            $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                            $z = date('I', $newTime);

                            if ($z == 0) {
                                $newTime += 3600;
                            } else {
                                $newTime += 7200;
                            }

                            $start_neu = date('Y-m-d H:i:s', $newTime);
                            $update_sync .= ', `datum` = "' . $start_neu . '"';
                        }
                        
                        $status_bm = $MailingWebservice->getStatus($m_id);
                        if ($MailingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingWebservice, 'Error -> getStatus');
                            }
                            
                            die('BroadmailError: MailingWebservice');
                        }
                        switch ($status_bm) {
                            case 'DONE':
                            case 'CANCELED':
                                if ($status_bm == 'DONE') {
                                    $status = 5;
                                }
                                
                                if ($status_bm == 'CANCELED') {
                                    $status = 7;
                                }
                                
                                $update_status .= ', `status` = ' . $status;
                                
                                $ende = $MailingWebservice->getSendingFinishedDate($m_id);
                                if ($MailingWebservice->getError()) {
                                    if ($firePhp instanceof FirePHP) {
                                        $firePhp->log($MailingWebservice, 'Error -> getSendingFinishedDate');
                                    }
                                    
                                    die('BroadmailError: MailingWebservice');
                                }
                                
                                if (strpos($ende, 'T')) {
                                    $date_e = explode('T', $ende);
                                    $datum = $date_e[0];
                                    $zeit = substr($date_e[1], 0, 5);
                                    $date_teile = explode('-', $date_e[0]);
                                    $zeit_teile = explode(':', $zeit);
                                    $newTime = mktime($zeit_teile[0], $zeit_teile[1], 0, $date_teile[1], $date_teile[2], $date_teile[0]);
                                    $z = date('I', $newTime);

                                    if ($z == 0) {
                                        $newTime += 3600;
                                    } else {
                                        $newTime += 7200;
                                    }

                                    $ende_neu = date('Y-m-d H:i:s', $newTime);
                                    $update_sync .= ', `beendet` = "' . $ende_neu . '"';
                                }
                                break;
                            
                            case 'SENDING':
                                $update_status .= ', `status` = "6"';
                                break;
                        }
                        
                        $versendet = $MailingWebservice->getSentRecipientCount($m_id);
                        if ($MailingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingWebservice, 'Error -> getSentRecipientCount');
                            }

                            die('BroadmailError: MailingWebservice');
                        }
                        $versendet = round($versendet, -1);
                        
                        $from = $MailingWebservice->getFromName($m_id);
                        if ($MailingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingWebservice, 'Error -> getFromName');
                            }

                            die('BroadmailError: MailingWebservice');
                        }
                        $from = htmlspecialchars($from, ENT_QUOTES);
                        
                        $subject = $MailingWebservice->getSubject($m_id);
                        if ($MailingWebservice->getError()) {
                            if ($firePhp instanceof FirePHP) {
                                $firePhp->log($MailingWebservice, 'Error -> getSubject');
                            }
                            die('BroadmailError: MailingWebservice');
                        }
                        $subject = htmlspecialchars($subject, ENT_QUOTES);
                        
                        $update_sync .= ', `versendet` = "' . $versendet . '", `absendername` = "' . $from . '", `betreff` = "' . $subject . '"';
                        
                        $unsubscribeCount = $MailingReportingWebservice->getUnsubscribeCount($m_id);
						if ($MailingReportingWebservice->getError()) {
							if ($firePhp instanceof FirePHP) {
								$firePhp->log($MailingReportingWebservice, 'Error -> getUnsubscribeCount');
							}
							
							die('BroadmailError: MailingReportingWebservice');
						}
						$abmelder_sql = ', `abmelder` = ' . intval($unsubscribeCount);
                        
                        $factory->logout();
                        break;
                    
                    case 'K':
                        require_once('../library/kajomiManager.php');
                        
                        $kajomiManager = new kajomiManager();
                        $mailingDataArr = $kajomiManager->getMailingData(
                            $mandantID,
                            $m_id
                        );
                        // debug
                        if ($firePhp instanceof FirePHP) {
                            $firePhp->log($mailingDataArr, 'mailingDataArr');
                            $firePhp->log($mandantID, 'mandantID');
                            $firePhp->log($m_id, 'm_id');
                        }
                        
                        $datum = $mailingDataArr['versanddatum'] . ' ' . $mailingDataArr['versandzeit'];
                        $versendet = $mailingDataArr['versendet'];
                        $openings = $mailingDataArr['openings_u'];
                        $openings_all = $mailingDataArr['openings'];
                        $klicks = $mailingDataArr['klicks_u'];
                        $klicks_all = $mailingDataArr['klicks'];
                        $softbounces = $mailingDataArr['softbounces'];
                        $hardbounces = $mailingDataArr['hardbounces'];
                        
                        if ($klicks_all > 0) {
                            $update_sync .= ', `datum` = "' . $datum . '", `gestartet` = "' . $datum . '", `versendet` = "' . $versendet . '"';
                            
                            $update_status = ', `status` = 5';
                        }
                        break;
                }
                
                if ($zeile_hv['status'] > 7) {
                    $update_status = '';
                }
                
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log($zeile_hv, 'zeile_hv');
                    $firePhp->log($versandsystem, 'versandsystem');
                    $firePhp->log($update_sync, 'update_sync');
                    $firePhp->log($update_status, 'update_status');
                    $firePhp->log($abmelder_sql, 'abmelder_sql');
                }
                

                if ($versandsystem == 'BM' || $versandsystem == 'K') {
                    $update_sync_ = mysql_query(
                        'UPDATE `' . $kampagne_db . '` SET' . 
                                ' `openings` = "' . $openings . '", `klicks` = "' . $klicks . '", `openings_all` = "' . $openings_all . '"' . 
                                ', `klicks_all` = "' . $klicks_all . '", `sbounces` = "' . $softbounces . '", `hbounces` = "' . $hardbounces . '"' . 
                                $update_sync . $update_status . $abmelder_sql . 
                            ' WHERE `mail_id` = "' . $m_id . '"'
                        ,
                        $verbindung
                    );
                    if (!$update_sync_) {
                        // debug
                        if ($firePhp instanceof FirePHP) {
                            $firePhp->log(mysql_error($verbindung), 'sql error: update_sync_');
                        }
                    }
                }
            }
            
            if ($kid == '') {
                $resKampagneUpdate = mysql_query(
                    'UPDATE `' . $kampagne_db . '` SET `gestartet` = NOW() WHERE `k_id` = "1"',
                    $verbindung
                );
                if (!$resKampagneUpdate) {
                    // debug
                    if ($firePhp instanceof FirePHP) {
                        $firePhp->log(mysql_error($verbindung), 'sql error: resKampagneUpdate');
                    }
                }
            }
        }
        mysql_free_result($mail_id_daten);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->groupEnd();
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: mail_id_daten');
        }
    }
}

echo '<script language="javascript" type="text/javascript">
        document.getElementById(\'yui-gen28-button\').style.display = \'block\';
        document.getElementById(\'text\').innerHTML = \'<span style="color:#333333;font-size:12px;font-weight:bold;"><img src="img/Tango/22/actions/dialog-apply.png" style="margin-bottom:-4px;" /> Aktualisierung abgeschlossen</span>\';
    </script>'
;
?>