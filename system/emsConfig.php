<?php
\date_default_timezone_set('Europe/Berlin');
\setlocale(LC_TIME, 'de_DE', 'de_DE.utf-8');

\define('DIR_configs', __DIR__ . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR);
\define('DIR_configsInit', DIR_configs . 'Init' . \DIRECTORY_SEPARATOR);
\define('DIR_deliverySystems', DIR_configs . 'DeliverySystems' . \DIRECTORY_SEPARATOR);
\define('DIR_userPackages', DIR_configs . 'UserPackages' . \DIRECTORY_SEPARATOR);

\define('DIR_Actions', __DIR__ . \DIRECTORY_SEPARATOR . 'Actions' . \DIRECTORY_SEPARATOR);

\define('DIR_library', __DIR__ . \DIRECTORY_SEPARATOR . 'library' . \DIRECTORY_SEPARATOR);
\define('DIR_Entity', DIR_library . 'Entity' . \DIRECTORY_SEPARATOR);
\define('DIR_Factory', DIR_library . 'Factory' . \DIRECTORY_SEPARATOR);
\define('DIR_Manager', DIR_library . 'Manager' . \DIRECTORY_SEPARATOR);
\define('DIR_Utils', DIR_library . 'Utils' . \DIRECTORY_SEPARATOR);
\define('DIR_Webservice', DIR_library . 'Webservice' . \DIRECTORY_SEPARATOR);
\define('DIR_deliverySystemWebservice', DIR_Webservice . 'DeliverySystemWebservice' . \DIRECTORY_SEPARATOR);

\define('DIR_packages', DIR_library . 'packages' . \DIRECTORY_SEPARATOR);

\define('DIR_Pear', DIR_library . 'contrib' . \DIRECTORY_SEPARATOR . 'pear' . \DIRECTORY_SEPARATOR);


/**
 * @deprecated -> use DIR_Actions
 */
\define('DIR_Action', __DIR__ . \DIRECTORY_SEPARATOR . 'kampagne' . \DIRECTORY_SEPARATOR . 'Actions' . \DIRECTORY_SEPARATOR);

/**
 * @deprecated use Configs -> emsConfig
 */