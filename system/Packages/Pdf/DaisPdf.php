<?php
namespace MAAS\Pdf;

// Include the main TCPDF library.
require_once(DIR_Vendor . 'tcpdf/tcpdf.php');


/**
 * Description of DaisPdf
 *
 * @author Cristian.Reus
 */
class DaisPdf extends \TCPDF {
	protected $kPathImages;
	
	protected $imageFilename = 'briefpapier.jpg';
	
	
	
	/**
	 * Header
	 * 
	 * @return void
	 */
	public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		
		// disable auto-page-break
		$this->SetAutoPageBreak(
			false,
			0
		);
		
		// set bacground image
		$this->Image(
			$this->kPathImages . $this->imageFilename,
			0,
			0,
			210,
			297,
			'',
			'',
			'',
			false,
			149,
			'',
			false,
			false,
			0
		);
		
		// restore auto-page-break status
		$this->SetAutoPageBreak(
			$auto_page_break,
			$bMargin
		);
		
		// set the starting point for the page content
		$this->setPageMark();
	}
	
	
	
	
	
	/* *******************************************************************************************
	 *
	 *              setter - Functions
	 *
	 * ***************************************************************************************** */
	public function setKPathImages($kPathImages) {
		$this->kPathImages = $kPathImages;
	}
	
	public function setImageFilename($imageFilename) {
		$this->imageFilename = $imageFilename;
	}

}
