<?php
require_once(\DIR_Manager . 'ClientManager.php');

$clientManager = new \ClientManager();
$clientManager->setPdoDebug($pdoDebug);
$clientManager->setStatementsDebug($pdoStatmentDebug);
$clientManager->init();