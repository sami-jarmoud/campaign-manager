<?php
$pdoDebug = $pdoStatmentDebug = false;

require_once(\DIR_library . 'debugLogManager.php');

$debugLogManager = new \debugLogManager();
#$debugLogManager->setUserId($_SESSION['benutzer_id']);
$debugLogManager->init();

if ($debugLogManager->getFirePhp() instanceof \FirePHP) {
	$pdoDebug = true;
}