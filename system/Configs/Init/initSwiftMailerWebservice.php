<?php
require_once(\DIR_Webservice . 'MailerWebservice/SwiftMailerWebservice.php');

$swiftMailerWebservice = new \SwiftMailerWebservice();
$swiftMailerWebservice->setSmtpHost($smtp_host);
$swiftMailerWebservice->setSmtpUsername($smtp_user);
$swiftMailerWebservice->setSmtpPassword($smtp_pw);
$swiftMailerWebservice->setTransportTyp('smtp');
$swiftMailerWebservice->setServerName($serverName);
$swiftMailerWebservice->setFromEmail($absender_email);
$swiftMailerWebservice->setFromName($absender_name);
$swiftMailerWebservice->setEncryption($smtpEncryption);
$swiftMailerWebservice->setSmtpPort($smtpPort);
$swiftMailerWebservice->init();