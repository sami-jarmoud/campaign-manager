<?php
require_once(\DIR_Manager . 'CampaignManager.php');

$campaignManager = new \CampaignManager();
$campaignManager->setHost($dbHostEms);
$campaignManager->setDbName($db_name);
$campaignManager->setDbUser($db_user);
$campaignManager->setDbPassword($db_pw);
$campaignManager->setCampaignTable($kampagne_db);
$campaignManager->setCustomerTable($kunde);
$campaignManager->setContactPersonTable($kunde_ap);
$campaignManager->setLogTable($log);
$campaignManager->setPdoDebug($pdoDebug);
$campaignManager->setStatementsDebug($pdoStatmentDebug);
$campaignManager->init();