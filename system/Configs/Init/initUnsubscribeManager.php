<?php
require_once(\DIR_Manager . 'UnsubscribeManager.php');

$unsubscribeManager = new \UnsubscribeManager();
$unsubscribeManager->setPdoDebug($pdoDebug);
$unsubscribeManager->setStatementsDebug($pdoStatmentDebug);
$unsubscribeManager->init();