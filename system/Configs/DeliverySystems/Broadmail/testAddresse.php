<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


	/**
	 * getUserDataItemById
	 */
	$userEntity = $clientManager->getUserDataItemById($_SESSION['benutzer_id']);
	if (!($userEntity instanceof \UserEntity)) {
		throw new \DomainException('no UserEntity', 1426155610);
	}
	
try {
	/**
	* getWebserviceObjectByType
	 * 
	 * RecipientWebservice
	*/
	$recipientWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientWebservice');
	
	/**
	 * getAttributes
	 * 
	 * debug
	 */
	$isExiste = $recipientWebservice->contains(
          $deliverySystemWebservice->getSession(),
           $recipientListId,
		   $userEntity->getEmail()
    );
	
		if(!$isExiste){
	$result =	$recipientWebservice->add2(
				$deliverySystemWebservice->getSession(),
				$recipientListId,
				'0',
				$userEntity->getEmail(),
				$userEntity->getEmail(),
				array(
					'Email',
					'Anrede',
					'Vorname',
					'Nachname'
				  ),
				array(
					$userEntity->getEmail(),
					$userEntity->getAnrede(),
					$userEntity->getVorname(),
					$userEntity->getNachname()
				 )		
			);
	  $isExiste = $recipientWebservice->contains(
          $deliverySystemWebservice->getSession(),
           $recipientListId,
		   $userEntity->getEmail()
    );
		}
	
		if($isExiste){
				$recipientDataArray = $recipientWebservice->getAttributes(
					$deliverySystemWebservice->getSession(),
					$recipientListId,
					$userEntity->getEmail(),
					$attributesDataArray
				);
	   }
    $debugLogManager->logData('recipientDataArray', $recipientDataArray);
	
    if (\count($recipientDataArray) > 0) {

			$resultDataArray[] = array(
                'email' => $recipientDataArray[0],
                'vn' => \htmlspecialchars($recipientDataArray[1]),
                'nn' => \htmlspecialchars($recipientDataArray[2])
            );

	}
} catch (\Exception $e) {
	require_once('processException.php');
}