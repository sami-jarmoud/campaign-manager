<?php
/**
 * getBroadmailMailingDataForCampaignDataArray
 * 
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return string
 */
function getBroadmailMailingDataForCampaignDataArray(\BroadmailWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$status = 0;
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * MailingWebservice
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	/**
	 * getStatus
	 * 
	 * debug
	 */
	$mailingStatus = $mailingWebservice->getStatus(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mailingStatus', $mailingStatus);
	if ($mailingStatus !== 'NEW') {
		switch ($mailingStatus) {
			case 'DONE':
				if ($campaignEntity->getStatus() != 5 
					&& $campaignEntity->getStatus() < 20 
				) {
					$status = 5;
				}
				
				$mailingFinishedDate = new \DateTime(
					$mailingWebservice->getSendingFinishedDate(
						$deliverySystemWebservice->getSession(),
						$campaignEntity->getMail_id()
					)
				);
				$mailingFinishedDate->setTimezone(new \DateTimeZone('Europe/Berlin'));
				$debugLogManager->logData('mailingFinishedDate', $mailingFinishedDate);
				
				$campaignUpdateDataArray['beendet'] = array(
					'value' => $mailingFinishedDate->format('Y-m-d H:i:s'),
					'dataType' => \PDO::PARAM_STR
				);
				break;

			case 'SENDING':
				$status = 6;
				break;

			case 'CANCELED':
				$status = 7;
				break;
		}
		
		$mailingStartetDate = new \DateTime(
			$mailingWebservice->getSendingStartedDate(
				$deliverySystemWebservice->getSession(),
				$campaignEntity->getMail_id()
			)
		);
		$mailingStartetDate->setTimezone(new \DateTimeZone('Europe/Berlin'));
		$debugLogManager->logData('mailingStartetDate', $mailingStartetDate);
		
		$campaignUpdateDataArray['gestartet'] = array(
			'value' => $mailingStartetDate->format('Y-m-d H:i:s'),
			'dataType' => \PDO::PARAM_STR
		);
		$campaignUpdateDataArray['datum'] = array(
			'value' => $mailingStartetDate->format('Y-m-d H:i:s'),
			'dataType' => \PDO::PARAM_STR
		);
	
		if ($status > 0) {
			$campaignUpdateDataArray['status'] = array(
				'value' => $status,
				'dataType' => \PDO::PARAM_INT
			);
		}
	}
	
	/**
	 * getSentRecipientCount
	 */
	$campaignUpdateDataArray['versendet'] = array(
		'value' => $mailingWebservice->getSentRecipientCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id()
		),
		'dataType' => \PDO::PARAM_INT
	);
	
	/**
	 * getFromName
	 */
	$campaignUpdateDataArray['absendername'] = array(
		'value' => \htmlspecialchars(
			$mailingWebservice->getFromName(
				$deliverySystemWebservice->getSession(),
				$campaignEntity->getMail_id()
			),
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);
	
	/**
	 * getSubject
	 */
	$campaignUpdateDataArray['betreff'] = array(
		'value' => \htmlspecialchars(
			$mailingWebservice->getSubject(
				$deliverySystemWebservice->getSession(),
				$campaignEntity->getMail_id()
			),
			\ENT_QUOTES
		),
		'dataType' => \PDO::PARAM_STR
	);
	
	/**
	 * getMimeType
	 */
	$mailingMimeType = $mailingWebservice->getMimeType(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mailingMimeType', $mailingMimeType);
	switch ($mailingMimeType) {
		case 'multipart/alternative':
			$mimeType = 'm';
			break;

		case 'text/html':
			$mimeType = 'h';
			break;

		case 'text/plain':
			$mimeType = 't';
			break;
	}
	$campaignUpdateDataArray['mailtyp'] = array(
		'value' => $mimeType,
		'dataType' => \PDO::PARAM_STR
	);
	
	$debugLogManager->endGroup();
	
	return $mailingStatus;
}

/**
 * getBroadmailMailingReportingDataForCampaignDataArray
 * 
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getBroadmailMailingReportingDataForCampaignDataArray(\BroadmailWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * MailingReportingWebservice
	 */
	$mailingReportingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingReportingWebservice');
	
	/**
	* getOpenCount
	* 
	* openings, openings_all
	*/
	$campaignUpdateDataArray['openings'] = array(
		'value' => $mailingReportingWebservice->getOpenCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id(),
			true
		),
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => $mailingReportingWebservice->getOpenCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id(),
			false
		),
		'dataType' => \PDO::PARAM_INT
	);

	/**
	* getClickCount
	* 
	* klicks, klicks_all
	*/
	$campaignUpdateDataArray['klicks'] = array(
		'value' => $mailingReportingWebservice->getClickCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id(),
			true
		),
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => $mailingReportingWebservice->getClickCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id(),
			false
		),
		'dataType' => \PDO::PARAM_INT
	);

	// getUnsubscribeCount
	$campaignUpdateDataArray['abmelder'] = array(
		'value' => $mailingReportingWebservice->getUnsubscribeCount(
			$deliverySystemWebservice->getSession(),
			$campaignEntity->getMail_id()
		),
		'dataType' => \PDO::PARAM_INT
	);
	
	$debugLogManager->endGroup();
}

/**
 * getBroadmailResponseDataForCampaignDataArray
 * 
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getBroadmailResponseDataForCampaignDataArray(\BroadmailWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * ResponseWebservice
	 */
	$responseWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ResponseWebservice');
	
	/**
	* getMailingResponseCount
	* 
	* sbounces, hbounces
	*/
	$campaignUpdateDataArray['sbounces'] = array(
		'value' => $responseWebservice->getMailingResponseCount(
			$deliverySystemWebservice->getSession(),
			'softbounce',
			$campaignEntity->getMail_id()
		),
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => $responseWebservice->getMailingResponseCount(
			$deliverySystemWebservice->getSession(),
			'hardbounce',
			$campaignEntity->getMail_id()
		),
		'dataType' => \PDO::PARAM_INT
	);
	
	$debugLogManager->endGroup();
}

/**
 * checkIfRecipientIsBlacklisted
 * 
 * @param string $recipient
 * @param \SoapClient $blacklistWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return boolean
 */
function checkIfRecipientIsBlacklisted($recipient, \SoapClient $blacklistWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * isBlacklisted
	 * (es wird sowohl die abc@domain.tdl aus auch die *@domain.tdl überprüft)
	 * 
	 * debug
	 */
	$result = $blacklistWebservice->isBlacklisted(
		$deliverySystemWebservice->getSession(),
		$recipient
	);
	$debugLogManager->logData($recipient, $result);
	
	$debugLogManager->endGroup();
	
	return $result;
}

/**
 * checkIfRecipientContainsOnTheBlacklist
 * 
 * @param string $recipient
 * @param \SoapClient $blacklistWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return boolean
 */
function checkIfRecipientContainsOnTheBlacklist($recipient, \SoapClient $blacklistWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * contains
	 * (es der übergebene value überprüft)
	 * 
	 * debug
	 */
	$result = $blacklistWebservice->contains(
		$deliverySystemWebservice->getSession(),
		$recipient
	);
	$debugLogManager->logData($recipient, $result);
	
	$debugLogManager->endGroup();
	
	return $result;
}