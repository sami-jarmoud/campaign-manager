<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


/**
 * cleanRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function cleanRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		if (\strlen($emailAddress) === 0) {
			unset($recipientsDataArray[$key]);
		}
	}
}

/**
 * correctRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function correctRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		$recipientsDataArray[$key] = \correctEmailAddress($emailAddress);
	}
}

/**
 * correctEmailAddress
 * 
 * @param string $emailAddress
 * @return string
 */
function correctEmailAddress($emailAddress) {
	$emailAddressDataArray = \explode('@', $emailAddress);
	
	return \strtolower($emailAddressDataArray[0]) . '@' . $emailAddressDataArray[1];
}

/**
 * processEmailBlacklist
 * 
 * @param string $emailAddress
 * @param \SoapClient $blacklistWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @param array $blacklistRecipients
 * @return void
 */
function processEmailBlacklist($emailAddress, \SoapClient $blacklistWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager, array &$blacklistRecipients) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * checkIfRecipientIsBlacklisted
	 */
	$isBlacklisted = \checkIfRecipientIsBlacklisted(
		$emailAddress,
		$blacklistWebservice,
		$deliverySystemWebservice,
		$debugLogManager
	);
	if ((boolean) $isBlacklisted) {
		$blacklistRecipients[] = $emailAddress;
		
		/**
		 * remove
		 * 
		 * debug
		 */
		$blacklistWebservice->remove(
			$deliverySystemWebservice->getSession(),
			$emailAddress
		);
		$debugLogManager->logData('remove', $emailAddress);
		
		/**
		* checkAndProcessDomainEmailBlacklist
		*/
		\checkAndProcessDomainEmailBlacklist(
			$emailAddress,
			$blacklistWebservice,
			$deliverySystemWebservice,
			$debugLogManager,
			$blacklistRecipients
		);
	}
	
	$debugLogManager->endGroup();
}

/**
 * checkAndProcessDomainEmailBlacklist
 * 
 * @param string $emailAddress
 * @param \SoapClient $blacklistWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @param array $blacklistRecipients
 * @return void
 */
function checkAndProcessDomainEmailBlacklist($emailAddress, \SoapClient $blacklistWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager, array &$blacklistRecipients) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$emailAddressDataArray = \explode('@', $emailAddress);
	$domainEmailAddress = '*@' . $emailAddressDataArray[1];
	unset($emailAddressDataArray);
	
	/**
	 * checkIfRecipientContainsOnTheBlacklist
	 */
	$isDomainBlacklisted = \checkIfRecipientContainsOnTheBlacklist(
		$domainEmailAddress,
		$blacklistWebservice,
		$deliverySystemWebservice,
		$debugLogManager
	);
	if ((boolean) $isDomainBlacklisted) {
		$blacklistRecipients[] = $domainEmailAddress;

		/**
		 * remove
		 * 
		 * debug
		 */
		$blacklistWebservice->remove(
			$deliverySystemWebservice->getSession(),
			$domainEmailAddress
		);
		$debugLogManager->logData('remove', $domainEmailAddress);
	}
	
	$debugLogManager->endGroup();
}

/**
 * processSendMultipartTestmail
 * 
 * @param integer $mailingId
 * @param integer $recipientListId
 * @param array $recipientsDataArray
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @throws \InvalidArgumentException
 * @return void
 */
function processSendMultipartTestmail($mailingId, $recipientListId, array $recipientsDataArray, \SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * copy
	 * 
	 * debug
	 */
	$newMailingId = $mailingWebservice->copy(
		$deliverySystemWebservice->getSession(),
		$mailingId
	);
	if (!\is_numeric($newMailingId)) {
		throw new \InvalidArgumentException((string) $mailingWebservice->getError());
	}
	$debugLogManager->logData('copy', $newMailingId);
	
	/**
	 * setMimeType
	 */
	$mailingWebservice->setMimeType(
		$deliverySystemWebservice->getSession(),
		$newMailingId,
		'text/plain'
	);
	
	/**
	 * sendTestmailToRecipentsDataArray
	 * 
	 * debug
	 */
	$result = \sendTestmailToRecipentsDataArray(
		$newMailingId,
		$recipientListId,
		$recipientsDataArray,
		$mailingWebservice,
		$deliverySystemWebservice
	);
	$debugLogManager->logData('sendTestmailToRecipentsDataArray', $result);
	
	/**
	 * remove
	 * 
	 * debug
	 */
	$mailingWebservice->remove(
		$deliverySystemWebservice->getSession(),
		$newMailingId
	);
	$debugLogManager->logData('remove', $newMailingId);
	
	$debugLogManager->endGroup();
}

/**
 * sendTestmailToRecipentsDataArray
 * 
 * @param integer $mailingId
 * @param integer $recipientListId
 * @param array $recipientsDataArray
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @return integer
 */
function sendTestmailToRecipentsDataArray($mailingId, $recipientListId, array $recipientsDataArray, \SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice) {
	return $mailingWebservice->sendTestmails(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$recipientListId,
		$recipientsDataArray
	);
}

/**
 * processBlacklistRecipients
 * 
 * @param \SoapClient $blacklistWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param array $blacklistRecipients
 * @return void
 */
function processBlacklistRecipients(\SoapClient $blacklistWebservice, \BroadmailWebservice $deliverySystemWebservice, array $blacklistRecipients) {
	if (\count($blacklistRecipients) > 0) {
		foreach ($blacklistRecipients as $email) {
			$blacklistWebservice->add(
				$deliverySystemWebservice->getSession(),
				$email
			);
		}
	}
}


$blacklistRecipients = array();
try {
	require_once('BroadmailFunctions.php');
	
	/**
	 * cleanRecipientsDataArray
	 * 
	 * debug
	 */
	\cleanRecipientsDataArray($recipientsDataArray);
	$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
	
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * MailingWebservice
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * BlacklistWebservice
	 */
	$blacklistWebservice = $deliverySystemWebservice->getWebserviceObjectByType('BlacklistWebservice');
	
	foreach ($recipientsDataArray as $emailAddress) {
		// processEmailBlacklist
		\processEmailBlacklist(
			$emailAddress,
			$blacklistWebservice,
			$deliverySystemWebservice,
			$debugLogManager,
			$blacklistRecipients
		);
	}
	
	/**
	 * getMimeType
	 * 
	 * debug
	 */
	$mimeType = $mailingWebservice->getMimeType(
		$deliverySystemWebservice->getSession(),
        $campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mimeType', $mimeType);
	
	
	/**
	 * correctRecipientsDataArray
	 */
	\correctRecipientsDataArray($recipientsDataArray);
	
	if ($mimeType == 'multipart/alternative') {
		// processSendMultipartTestmail
		\processSendMultipartTestmail(
			$campaignEntity->getMail_id(),
			$recipientListId,
			$recipientsDataArray,
			$mailingWebservice,
			$deliverySystemWebservice,
			$debugLogManager
		);
	}

	/**
	 * sendTestmailToRecipentsDataArray
	 * 
	 * debug
	 */
	$result = \sendTestmailToRecipentsDataArray(
		$campaignEntity->getMail_id(),
		$recipientListId,
		$recipientsDataArray,
		$mailingWebservice,
		$deliverySystemWebservice
	);
	$debugLogManager->logData('sendTestmailToRecipentsDataArray', $result);

	/**
	 * processBlacklistRecipients
	 * 
	 * debug
	 */
	\processBlacklistRecipients(
		$blacklistWebservice,
		$deliverySystemWebservice,
		$blacklistRecipients
	);
	$debugLogManager->logData('processBlacklistRecipients', $blacklistRecipients);
} catch (\Exception $e) {
	require_once('processException.php');
	
	\MailUtils::$EMAIL_BETREFF = 'Admin - Broadmail Error: ' . \basename(__FILE__);
	\MailUtils::sendMail(
		'client: ' . $_SESSION['mandant'] . ' (' . (int) $_SESSION['mID'] . ')' . \chr(11)
		. 'userFullname: ' . $_SESSION['u_vorname'] . ' ' . $_SESSION['u_nachname'] . ' (' . $_SESSION['benutzer_id'] . ')' . \chr(11) . \chr(11) 
		. 'blacklistRecipients:' . \chr(11)
		. \implode(\chr(11), $blacklistRecipients)
	);
	
	/**
	 * processBlacklistRecipients
	 * 
	 * debug
	 */
	\processBlacklistRecipients(
		$blacklistWebservice,
		$deliverySystemWebservice,
		$blacklistRecipients
	);
	$debugLogManager->logData('processBlacklistRecipients', $blacklistRecipients);
	
	echo \nl2br('Folgender Fehler ist aufgetreten:' . \chr(10) . $errorMessage);
}