<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


try {
	require_once('BroadmailFunctions.php');
	
	/**
	 * checkIfRecipientIsBlacklisted
	 */
	$deliverySystemResult = \checkIfRecipientIsBlacklisted(
		$recipient,
		$deliverySystemWebservice->getWebserviceObjectByType('BlacklistWebservice'),
		$deliverySystemWebservice,
		$debugLogManager
	);
} catch (\Exception $e) {
	require_once('processException.php');
}