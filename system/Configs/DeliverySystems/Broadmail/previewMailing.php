<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	 * getContent
	 * 
	 * debug
	 */
	$result = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice')->getContent(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id(),
		($mailType == 'html' ? 'text/html' : 'text/plain')
	);
	$debugLogManager->logData('content', $result);
	
	$result = \str_replace("src=", " src=", $result);
	
	echo ($mailType == 'html' ? $result : \nl2br($result));
} catch (\Exception $e) {
	require_once('processException.php');
}