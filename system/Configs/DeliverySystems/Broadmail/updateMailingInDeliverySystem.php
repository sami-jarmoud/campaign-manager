<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * updateMailingDataInDeliverySystem
 * 
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function updateMailingDataInDeliverySystem(\BroadmailWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getWebserviceObjectByType
	 * 
	 * MailingWebservice
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	/**
	 * setRecipientListIds
	 * 
	 * debug
	 */
	$mailingWebservice->setRecipientListIds(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id(),
		array(
			$_SESSION['deliverySystemDistributorEntityDataArray'][$campaignEntity->getDsd_id()]->getDistributor_id()
		)
	);
	$debugLogManager->logData('setRecipientListIds', $_SESSION['deliverySystemDistributorEntityDataArray'][$campaignEntity->getDsd_id()]->getDistributor_id());
	
	/**
	 * setMaxRecipients (randomOrder = true)
	 * 
	 * debug
	 */
	$mailingWebservice->setMaxRecipients(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id(),
		$campaignEntity->getVorgabe_m(),
		true
	);
	$debugLogManager->logData('setMaxRecipients', $campaignEntity->getVorgabe_m());
	
	$debugLogManager->endGroup();
}


try {
	/**
	 * updateMailingDataInDeliverySystem
	 */
	\updateMailingDataInDeliverySystem(
		$deliverySystemWebservice,
		$campaignEntity,
		$debugLogManager
	);
	
	
	/**
	 * addNewLogEntry
	 * 
	 * debug
	 */
	$idAddLogEntry = \addNewLogEntry(
		$campaignManager,
		$campaignEntity,
		12
	);
	$debugLogManager->logData('idAddLogEntry', $idAddLogEntry);
} catch (\Exception $e) {
	require_once('processException.php');
}