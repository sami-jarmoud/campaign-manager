<?php
/* @var $deliverySystemWebservice \BroadmailWebservice */
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */
/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


/**
 * startMailingToDeliveryTime
 * 
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param integer $mailingId
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function startMailingToDeliveryTime(\SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, $mailingId, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	$mailingWebservice->start(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$campaignEntity->getDatum()->format('c')
	);
	$debugLogManager->logData('startDatum', $campaignEntity->getDatum()->format('c'));
	
	$debugLogManager->endGroup();
}

/**
 * updateMailingDataInDeliverySystem
 * 
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param integer $mailingId
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function updateMailingDataInDeliverySystem(\SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, $mailingId, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * zielgruppen bereiniegen
	 * 
	 * setRecipientFilterIds
	 */
	$recipientFilterIds = \processClearMailingRecipientFilterWebservices(
		$mailingWebservice->getRecipientFilterIds(
			$deliverySystemWebservice->getSession(),
			$mailingId
		),
		$deliverySystemWebservice,
		$debugLogManager
	);
	$mailingWebservice->setRecipientFilterIds(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$recipientFilterIds
	);
	$debugLogManager->logData('recipientFilterIds', $recipientFilterIds);
	
	/**
	 * setName
	 * 
	 * debug
	 */
	$mailingName = $campaignEntity->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name();
	$mailingWebservice->setName(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$mailingName
	);
	$debugLogManager->logData('setName', $mailingName);
	
	/**
	 * setRecipientListIds
	 * 
	 * debug
	 */
	$mailingWebservice->setRecipientListIds(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		array(
			$_SESSION['deliverySystemDistributorEntityDataArray'][$campaignEntity->getDsd_id()]->getDistributor_id()
		)
	);
	$debugLogManager->logData('setRecipientListIds', $_SESSION['deliverySystemDistributorEntityDataArray'][$campaignEntity->getDsd_id()]->getDistributor_id());
	
	/**
	 * setMaxRecipients (randomOrder = true)
	 * 
	 * debug
	 */
	$mailingWebservice->setMaxRecipients(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$campaignEntity->getVorgabe_m(),
		true
	);
	$debugLogManager->logData('setMaxRecipients', $campaignEntity->getVorgabe_m());
	
	/**
	 * setSubject
	 * 
	 * debug
	 */
	$mailingWebservice->setSubject(
		$deliverySystemWebservice->getSession(),
		$mailingId,
		$campaignEntity->getBetreff()
	);
	$debugLogManager->logData('setSubject', $campaignEntity->getBetreff());
	
	$debugLogManager->endGroup();
}

/**
 * getAndUpdateCampaignUpdateDataArrayFromDeliverySystem
 * 
 * @param \SoapClient $mailingWebservice
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param integer $mailingId
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getAndUpdateCampaignUpdateDataArrayFromDeliverySystem(\SoapClient $mailingWebservice, \BroadmailWebservice $deliverySystemWebservice, $mailingId, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getFromName
	 * 
	 * debug
	 */
	$fromName = $mailingWebservice->getFromName(
		$deliverySystemWebservice->getSession(),
		$mailingId
	);
	$debugLogManager->logData('fromName', $fromName);
	$campaignUpdateDataArray['absendername'] = array(
		'value' => $fromName,
		'dataType' => \PDO::PARAM_STR
	);
	
	/**
	 * getSubject
	 * 
	 * debug
	 */
	$subject = $mailingWebservice->getSubject(
		$deliverySystemWebservice->getSession(),
		$mailingId
	);
	$debugLogManager->logData('subject', $subject);
	$campaignUpdateDataArray['betreff'] = array(
		'value' => $subject,
		'dataType' => \PDO::PARAM_STR
	);
	
	$debugLogManager->endGroup();
}

/**
 * processClearMailingRecipientFilterWebservices
 * 
 * @param array $recipientFilterIds
 * @param \BroadmailWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return array
 */
function processClearMailingRecipientFilterWebservices(array $recipientFilterIds, \BroadmailWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	$debugLogManager->beginGroup(__FUNCTION__);
	$debugLogManager->logData('recipientFilterIds', $recipientFilterIds);
	
	$resultDataArray = array();
	if (\count($recipientFilterIds) > 0) {
		/**
		 * getWebserviceObjectByType
		 * 
		 * RecipientFilterWebservice
		 */
		$recipientFilterWebservice = $deliverySystemWebservice->getWebserviceObjectByType('RecipientFilterWebservice');
		
		foreach ($recipientFilterIds as $recipientFilterId) {
			if (\strpos($recipientFilterWebservice->getName($deliverySystemWebservice->getSession(), $recipientFilterId), 'Exclude Mailing') === false) {
				$resultDataArray[] = $recipientFilterId;
			}
		}
	}
	
	$debugLogManager->endGroup();
	
	return $resultDataArray;
}


try {
	/**
	 * getWebserviceObjectByType
	 * 
	 * MailingWebservice
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
	
	/**
	 * copy
	 * 
	 * debug
	 */
	$newMailingId = $mailingWebservice->copy(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('newMailingId', $newMailingId);
	if (\is_numeric($newMailingId)) {
		/**
		 * updateMailingDataInDeliverySystem
		 */
		\updateMailingDataInDeliverySystem(
			$mailingWebservice,
			$deliverySystemWebservice,
			$newMailingId,
			$campaignEntity,
			$debugLogManager
		);
		
		
		/**
		 * campaignDataArray for updateCampaignDataRowByKid
		 */
		$campaignUpdateDataArray = array(
			'mail_id' => array(
				'value' => $newMailingId,
				'dataType' => \PDO::PARAM_STR
			),
			'status' => array(
				'value' => 1,
				'dataType' => \PDO::PARAM_INT
			),
		);

		/**
		 * getAndUpdateCampaignUpdateDataArrayFromDeliverySystem
		 */
		\getAndUpdateCampaignUpdateDataArrayFromDeliverySystem(
			$mailingWebservice,
			$deliverySystemWebservice,
			$newMailingId,
			$debugLogManager,
			$campaignUpdateDataArray
		);
		
		if ((boolean) $campaignEntity->getUse_campaign_start_date() === true) {
			// programiert
			$campaignUpdateDataArray['status']['value'] = 13;
			
			/**
			 * startMailingToDeliveryTime
			 * 
			 * mailing zum programiertes zeitpunkt starten
			 */
			\startMailingToDeliveryTime(
				$mailingWebservice,
				$deliverySystemWebservice,
				$newMailingId,
				$campaignEntity,
				$debugLogManager
			);
		}

		/**
		 * getMimeType
		 * 
		 * debug
		 */
		$mimeType = $mailingWebservice->getMimeType(
			$deliverySystemWebservice->getSession(),
			$newMailingId
		);
		$debugLogManager->logData('mimeType', $mimeType);
		switch ($mimeType) {
			case 'multipart/alternative':
				$campaignUpdateDataArray['mailtyp'] = array(
					'value' => 'm',
					'dataType' => \PDO::PARAM_STR
				);
				break;

			case 'text/html':
				$campaignUpdateDataArray['mailtyp'] = array(
					'value' => 'h',
					'dataType' => \PDO::PARAM_STR
				);
				break;

			case 'text/plain':
				$campaignUpdateDataArray['mailtyp'] = array(
					'value' => 't',
					'dataType' => \PDO::PARAM_STR
				);
				break;
		}


		/**
		 * updateCampaignAndAddLogItem
		 * 
		 * debug
		 */
		$campaignUpdate = $campaignManager->updateCampaignAndAddLogItem(
			$campaignEntity->getK_id(),
			$campaignEntity->getNv_id(),
			(int) $_SESSION['benutzer_id'],
			17,
			(int) $campaignUpdateDataArray['status']['value'],
			$campaignUpdateDataArray
		);
		$debugLogManager->logData('campaignUpdate', $campaignUpdate);
	} else {
		throw new \InvalidArgumentException((string) $mailingWebservice->getError());
	}
} catch (\Exception $e) {
	require_once('processException.php');
}