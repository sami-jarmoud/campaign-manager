<?php
/* @var $deliverySystemWebservice BroadmailWebservice */
/* @var $campaignEntity CampaignEntity */
/* @var $debugLogManager debugLogManager */


try {
	/**
	* getWebserviceObjectByType
	 * 
	 * MailingWebservice
	*/
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');

	/**
	* getScheduleDate
	* 
	* debug
	*/
	$scheduleDate = $mailingWebservice->getScheduleDate(
		$deliverySystemWebservice->getSession(),
		$campaignEntity->getMail_id()
	);
	$debugLogManager->logData('scheduleDateBM', $scheduleDate);

	if (\strlen($scheduleDate) > 0) {
		$scheduleDateTime = new \DateTime($scheduleDate);
		$scheduleDateTime->setTimezone(new \DateTimeZone('Europe/Berlin'));
	} else {
		$scheduleDateTime = null;
	}
} catch (\Exception $e) {
	require_once('processException.php');
}