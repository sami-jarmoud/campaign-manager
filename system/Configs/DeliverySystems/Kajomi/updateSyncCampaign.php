<?php
/* @var $deliverySystemWebservice \KajomiWebservice */
/* @var $campaignManager \CampaignManager */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


try {
	require_once('KajomiFunctions.php');
	
	$campaignUpdateDataArray = array(
		'mail_id' => array(
			'value' => $campaignEntity->getMail_id(),
			'dataType' => \PDO::PARAM_STR
		),
	);
	
	/**
	 * getKajomiMailingDataForCampaignDataArray
	 */
	\getKajomiMailingDataForCampaignDataArray(
		$deliverySystemWebservice,
		$campaignEntity,
		$debugLogManager,
		$campaignUpdateDataArray
	);
	
	// debug
	$debugLogManager->logData('updataDataArray', $campaignUpdateDataArray);
	
	
	/**
	* updateCampaignAndAddLogItem
	* 
	* debug
	*/
	$resLogCampaignUpdate = $campaignManager->updateCampaignAndAddLogItem(
		$campaignEntity->getK_id(),
		$campaignEntity->getNv_id(),
		(int) $_SESSION['benutzer_id'],
		$actionId,
		0,
		$campaignUpdateDataArray
	);
	$debugLogManager->logData('resLogCampaignUpdate', $resLogCampaignUpdate);
	unset($campaignUpdateDataArray);
} catch (\Exception $e) {
	require_once('processException.php');
}