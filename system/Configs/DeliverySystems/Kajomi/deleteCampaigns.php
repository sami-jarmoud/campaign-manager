<?php
/* @var $deliverySystemWebservice \KajomiWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


try {
	/**
	 * getWebserviceObjectByType
	 */
	$result = $deliverySystemWebservice->deleteMailing($campaignEntity->getMail_id());
	$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = $result;
	
	$resultData['mailingStatus'][$campaignEntity->getK_id()] = $campaignEntity->getStatus();
} catch (\Exception $e) {
	require_once('processException.php');
}