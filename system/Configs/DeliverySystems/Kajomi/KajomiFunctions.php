<?php
/**
 * getKajomiMailingDataForCampaignDataArray
 * 
 * @param \KajomiWebservice $deliverySystemWebservice
 * @param \CampaignEntity $campaignEntity
 * @param \debugLogManager $debugLogManager
 * @param array $campaignUpdateDataArray
 * @return void
 */
function getKajomiMailingDataForCampaignDataArray(\KajomiWebservice $deliverySystemWebservice, \CampaignEntity $campaignEntity, \debugLogManager $debugLogManager, array &$campaignUpdateDataArray) {
	$debugLogManager->beginGroup(__FUNCTION__);
	
	/**
	 * getMailingData
	 * 
	 * debug
	 */
	$mailingData = $deliverySystemWebservice->getMailingData($campaignEntity->getMail_id());
	/* @var $mailingData \SimpleXMLElement */
	$debugLogManager->logData('mailingData', $mailingData);
	
	
	$campaignUpdateDataArray['versendet'] = array(
		'value' => (int) $mailingData->gesamt->menge->brutto,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings'] = array(
		'value' => (int) $mailingData->gesamt->oeffner->unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['openings_all'] = array(
		'value' => (int) $mailingData->gesamt->oeffner->gesamt,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks'] = array(
		'value' => (int) $mailingData->gesamt->klicker->unique,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['klicks_all'] = array(
		'value' => (int) $mailingData->gesamt->klicker->gesamt,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['sbounces'] = array(
		'value' => (int) $mailingData->gesamt->menge->softbounces,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['hbounces'] = array(
		'value' => (int) $mailingData->gesamt->menge->hardbounces,
		'dataType' => \PDO::PARAM_INT
	);
	$campaignUpdateDataArray['gestartet'] = $campaignUpdateDataArray['datum'] = array(
		'value' => $mailingData->versandDatum,
		'dataType' => \PDO::PARAM_STR
	);
	
	if ($campaignEntity->getStatus() !== 5 
		&& $campaignEntity->getStatus() < 20 
		&& $campaignUpdateDataArray['klicks_all']['value'] > 0
	) {
		$campaignUpdateDataArray['status'] = array(
			'value' => 5,
			'dataType' => \PDO::PARAM_INT
		);
	}
	
	$debugLogManager->endGroup();
}