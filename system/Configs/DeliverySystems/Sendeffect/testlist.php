<?php
/* @var $deliverySystemWebservice \SendeffectWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
/* @var $deliverySystemDistributorWidthClientDeliverySystemEntity \DeliverySystemDistributorWidthClientDeliverySystemEntity */


/**
 * getTestRecipientList
 * 
 * @param \DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity
 * @param \SendeffectWebservice $deliverySystemWebservice
 * @param \debugLogManager $debugLogManager
 * @return void
 */
function getTestRecipientList(\DeliverySystemDistributorWidthClientDeliverySystemEntity $deliverySystemDistributorWidthClientDeliverySystemEntity, \SendeffectWebservice $deliverySystemWebservice, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	

	/**
	 * getContactsByFilterId
	 * 
	 * debug
	 */
	$listDataArray = $deliverySystemWebservice->GetListSubscribers(
                $recipientListId
                );
        /*if($response->isSuccess()){
            $listDataObject = $response->getResult();
        }*/
        
	$debugLogManager->logData('listDataArray', $listDataArray);
	
	
	$testListDataArray = array();
	foreach ($listDataArray as $recipient) {
            
	        $testListDataArray[$recipient->Email] = $recipient->Email;		
	}
	
	// debug
	$debugLogManager->logData('testListDataArray', $testListDataArray);

	\RegistryUtils::set(
		'campaign',
		array(
			$deliverySystemDistributorWidthClientDeliverySystemEntity->getClientDeliveryEntity()->getDeliverySystem()->getId() => array(
				$deliverySystemDistributorWidthClientDeliverySystemEntity->getM_asp_id() => array(
					'testListDataArray' => 'testListe'
				)
			)
		)
	);
	unset($testListDataArray);
	
	// debug
	$debugLogManager->endGroup();
}


try {
	if (!isset($testListDataArray)) {
		/**
		 * getTestRecipientList
		 */
		\getTestRecipientList(
			$deliverySystemDistributorWidthClientDeliverySystemEntity,
			$deliverySystemWebservice,
			$debugLogManager
		);
		
		// getTestListDataArray
		$testListDataArray = \getTestListDataArray($deliverySystemDistributorWidthClientDeliverySystemEntity);
	}
	
	foreach ($testListDataArray as $recipientListId => $recipientLisName) {
		$selected = null;
		if ((string) $recipientListId === (string) $deliverySystemDistributorWidthClientDeliverySystemEntity->getTestlist_distributor_id()) {
			$selected = 'selected';
		}
		
		$optionsItems .= \HtmlFormUtils::createOptionItem(
			array(
				'value' => '40',
				'selected' => $selected
			),
			'testListe'
		);
	}
} catch (\Exception $e) {
	require_once('processException.php');
}