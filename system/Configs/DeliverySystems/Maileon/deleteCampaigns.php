<?php
/* @var $deliverySystemWebservice \MaileonWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */

try {
    
	/**
	 * getWebserviceObjectByType
	 */
	$mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
       
                          $debug = isset($_POST['debug']) && $_POST['debug']==="true";
                         $mailingWebservice->setDebug($debug);
         $mailingWebservice->deleteMailing(
                $campaignEntity->getMail_id()
                );

                
			$resultData[$campaignEntity->getK_id()]['deleteFromDeliverySystem'] = true;
			$result = true;
	
	$resultData['mailingStatus'][$campaignEntity->getK_id()] = $campaignEntity->getStatus();
} catch (\Exception $e) {
	require_once('processException.php');
}