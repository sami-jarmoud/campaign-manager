<?php

/* @var $deliverySystemWebservice \MaileonWebservice */
/* @var $debugLogManager \debugLogManager */


try {

    $contactWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ContactsService');
        $xqContact = new com_maileon_api_contacts_Contact();
        $xqContact->anonymous = FALSE;
        $xqContact->email = $Email;
        $xqContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
        $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$FIRSTNAME] = $Vorname;
        $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$LASTNAME] = $Nachname;
        $xqContact->standard_fields[com_maileon_api_contacts_StandardContactField::$SALUTATION] = $Anrede;
        $xqContact->custom_fields["Test"] = TRUE;

         $contactWebservice->createContact($xqContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "src", "subscriptionPage", false, false);
        
        
         $ContactFiltersWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ContactFiltersService');
         $ContactFiltersWebservice->refreshContactFilterContacts(
                 $contactFilterId, 
                 time()*1000);
         
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
