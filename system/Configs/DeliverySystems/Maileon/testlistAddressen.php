<?php
/* @var $deliverySystemWebservice \MaileonWebservice */
/* @var $clientManager \ClientManager */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	* getWebserviceObjectByType
	 * 
	 * RecipientWebservice
	*/
	$recipientWebservice = $deliverySystemWebservice->getWebserviceObjectByType('ContactsService');
	
	/**
	 * getContactsByFilterId
	 * 
	 * debug
	 */
        //TODO distributor ID
	$response = $recipientWebservice->getContactsByFilterId(
                $recipientListId
                );
         if($response->isSuccess()){
             $listDataObject = $response->getResult();
        }
    $debugLogManager->logData('$listDataObject', $listDataObject);
    
    foreach ($listDataObject as $recipientsListe) {
        
            $item = (array)$recipientsListe;
        if (\count($item) > 0) {    
	$resultDataArray[] = array(
                'id' => \HtmlFormUtils::createCheckbox(
                    array(
                        'onclick' => 'addRecipientToList(this.value)',
                        'name' => 'testempfaenger[]',
                        'value' => $item['email']
                    )
                ),
                'email' => (array)$item['email']
            );
           }     	
	}
       
} catch (\Exception $e) {
	require_once('processException.php');
}