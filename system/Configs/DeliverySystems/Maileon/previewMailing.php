<?php
/* @var $deliverySystemWebservice \MaileonWebservice */
/* @var $debugLogManager \debugLogManager */

/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */


try {
	/**
	 * getContent
	 * 
	 * debug
	 */
    $mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
       
                          $debug = isset($_POST['debug']) && $_POST['debug']==="true";
                         $mailingWebservice->setDebug($debug);
      if($campaignEntity->getStatus() === 5){
            $fileURL =  $mailingWebservice->getArchiveUrl(
                $campaignEntity->getMail_id()
                );
            $URL = $fileURL->getBodyData();
            $result = \file_get_contents($URL);
            
        }else{                   
     $results =  $mailingWebservice->getHTMLContent(
                $campaignEntity->getMail_id()
                );
           $result = $results->getBodyData();
        }
     

	$debugLogManager->logData('content', $result);
        
	$result = \str_replace("src=", " src=", $result);
	
	echo ($mailType == 'html' ? $result : \nl2br($result));
} catch (\Exception $e) {
	require_once('processException.php');
}