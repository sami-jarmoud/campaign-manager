<?php
/* @var $deliverySystemWebservice \MaileonWebservice */
/* @var $campaignEntity \CampaignEntity */
/* @var $debugLogManager \debugLogManager */


/**
 * cleanRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function cleanRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		if (\strlen($emailAddress) === 0) {
			unset($recipientsDataArray[$key]);
		}
	}
}

/**
 * correctRecipientsDataArray
 * 
 * @param array $recipientsDataArray
 * @return void
 */
function correctRecipientsDataArray(array &$recipientsDataArray) {
	foreach ($recipientsDataArray as $key => $emailAddress) {
		$recipientsDataArray[$key] = \correctEmailAddress($emailAddress);
	}
}

/**
 * correctEmailAddress
 * 
 * @param string $emailAddress
 * @return string
 */
function correctEmailAddress($emailAddress) {
	$emailAddressDataArray = \explode('@', $emailAddress);
	
	return \strtolower($emailAddressDataArray[0]) . '@' . $emailAddressDataArray[1];
}




$blacklistRecipients = array();
try {
		
	/**
	 * cleanRecipientsDataArray
	 * 
	 * debug
	 */
	\cleanRecipientsDataArray($recipientsDataArray);
	$debugLogManager->logData('recipientsDataArray', $recipientsDataArray);
	
	/**
	 * correctRecipientsDataArray
	 */
	\correctRecipientsDataArray($recipientsDataArray);
       /**
	* getWebserviceObjectByType
	 * 
	 * MailingWebservice
	*/
        $mailingWebservice = $deliverySystemWebservice->getWebserviceObjectByType('MailingWebservice');
        
        foreach ($recipientsDataArray as $recipientsData){
                      $result = $mailingWebservice->sendTestmailToSingle(
                                $campaignEntity->getMail_id(),
                                $recipientsData
                                );
        }
	//TODO
	/**
	 * getWebserviceObjectByType
	 * 
	 * BlacklistWebservice
	 */
	/*/*$blacklistWebservice = $deliverySystemWebservice->getWebserviceObjectByType('BlacklistWebservice');
	
	foreach ($recipientsDataArray as $emailAddress) {
		// processEmailBlacklist
		\processEmailBlacklist(
			$emailAddress,
			$blacklistWebservice,
			$deliverySystemWebservice,
			$debugLogManager,
			$blacklistRecipients
		);
	}
	*/
	/**
	 * getMimeType
	 * 
	 * debug
	 */
	/*$mimeType = $mailingWebservice->getMimeType(
		$deliverySystemWebservice->getSession(),
        $campaignEntity->getMail_id()
	);
	$debugLogManager->logData('mimeType', $mimeType);
	*/
	

	/*
	if ($mimeType == 'multipart/alternative') {
		// processSendMultipartTestmail
		\processSendMultipartTestmail(
			$campaignEntity->getMail_id(),
			$recipientListId,
			$recipientsDataArray,
			$mailingWebservice,
			$deliverySystemWebservice,
			$debugLogManager
		);
	}
*/
	/**
	 * sendTestmailToRecipentsDataArray
	 * 
	 * debug
	 */
	/*$result = \sendTestmailToRecipentsDataArray(
		$campaignEntity->getMail_id(),
		$recipientListId,
		$recipientsDataArray,
		$mailingWebservice,
		$deliverySystemWebservice
	);*/
	$debugLogManager->logData('sendTestmailToRecipentsDataArray', $result);

	/**
	 * processBlacklistRecipients
	 * 
	 * debug
	 */
	/*\processBlacklistRecipients(
		$blacklistWebservice,
		$deliverySystemWebservice,
		$blacklistRecipients
	);
	$debugLogManager->logData('processBlacklistRecipients', $blacklistRecipients);*/
} catch (\Exception $e) {
	require_once('processException.php');

}