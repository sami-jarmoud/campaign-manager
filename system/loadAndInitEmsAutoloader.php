<?php
require_once(__DIR__ . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR . 'emsConfig.php');
require_once(\DIR_library . 'EmsAutoloader.php');


\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();


\RegistryUtils::set('emsWorkPath', __DIR__ . \DIRECTORY_SEPARATOR);

\define('version', '1.7.1');