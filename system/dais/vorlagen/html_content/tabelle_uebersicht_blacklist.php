<?php
$i = 1;
foreach ($daten_array as $key => $item) {
    ?>
    <tr id="bl_<?php echo $i; ?>" >
        <td><?php echo $i; ?></td>
        <td><?php echo $item['email']; ?></td>
        <td><?php echo date_converter($item['datum']); ?></td>
        <td><?php echo $item['grund']; ?></td>
        <td><?php echo (\strlen($item['firma']) > 0 ? $item['firma'] : $item['firmaalt']); ?></td>
        <td>
            <?php
                echo getUserDataById(
                    $userDataArray,
                    $item['bearbeiter']
                );
            ?>
        </td>
    </tr>
    <?php
    $i++;
}
?>