<?php
$tmpMandantValue = explode('_', $mandant_db);
$tmpMandant = $tmpMandantValue[0] ? $tmpMandantValue[0] : $mandant_db;

$i = 1;
foreach ($daten_array as $key => $item) {
    ?>
    <tr id="<?php echo $div_id . '_' . $mandant_db . '_' . $i; ?>">
        <td><?php echo $i; ?></td>
        <td><label for="<?php echo $div_id . '_' . $item['username']; ?>" ><?php echo $item['email']; ?></label></td>
        <td><?php echo \convertToEncoding($item['vorname'], 'silver_encoding'); ?></td>
        <td><?php echo \convertToEncoding($item['nachname'], 'silver_encoding'); ?></td>
        <td><?php echo $item['herkunft']; ?></td>
        <td><?php echo $tmpMandant; ?> <img src="imgs/info.gif" width="13" height="13" alt="" align="right" title="<?php echo $mandant_db; ?>" /></td>
        <td align="center"><input type="radio" name="user_id" value="<?php echo $item['user_id']; ?>" id="<?php echo $div_id . '_' . $item['username']; ?>" onclick="document.<?php echo $div_id; ?>_form.submit();" /></td>
    </tr>
    <?php
    $i++;
}
?>