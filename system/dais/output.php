<?php
$ergebnisArray = array();
$countArray = array();

if (isset($adminGruppe[$mandant]) 
	&& (
		\is_array($adminGruppe[$mandant]) 
		&& \count($adminGruppe[$mandant]) > 0
	)
) {
    foreach ($adminGruppe[$mandant] as $tmpItem) {
        $ergebnisArray[$tmpItem] = \ergebnis_db(
			$tmpItem,
			$search
		);
		$countArray[$tmpItem] = \is_array($ergebnisArray[$tmpItem]) ? \count($ergebnisArray[$tmpItem]) : 0;

        $daisTmpArray[] = \ergebnis_db(
            'bl_db',
            $search,
            'Datenauskunft',
            $tmpItem
        );
    }

    foreach ($daisTmpArray as $tmpItem) {
        if (\is_array($tmpItem)) {
            foreach ($tmpItem as $item) {
                $ergebnisDatenauskunft[] = $item;
            }
        }
    }
} else {
    $ergebnisArray[$mandant] = \ergebnis_db(
		$mandant,
		$search
	);
	$countArray[$mandant] = \is_array($ergebnisArray[$mandant]) ? \count($ergebnisArray[$mandant]) : 0;
            
	// datenauskunft
	$ergebnisDatenauskunft = \ergebnis_db(
		'bl_db',
		$search,
		'Datenauskunft',
		$mandant
	);
}

// Blacklist
$ergebnisBlacklist = \ergebnis_db(
    'bl_db',
    $search,
    'Blacklist'
);

$userDataArray = \getAllUserDataArray($ems_db);

/*
\DebugAndExceptionUtils::showDebugData($adminZugang, 'adminZugang');
\DebugAndExceptionUtils::showDebugData($adminGruppe, 'adminGruppe');
\DebugAndExceptionUtils::showDebugData($mandant, 'mandant');
\DebugAndExceptionUtils::showDebugData($ergebnisArray, 'ergebniss_array');
\DebugAndExceptionUtils::showDebugData($countArray, 'countArray');
\DebugAndExceptionUtils::showDebugData($ergebnisDatenauskunft, 'ergebnisDatenauskunft');
\DebugAndExceptionUtils::showDebugData($ergebnisBlacklist, 'ergebnisBlacklist');
\DebugAndExceptionUtils::showDebugData($userDataArray, 'userDataArray');
 * 
 */

$countGesamt = \array_sum($countArray);
$countDatenauskunft = $ergebnisDatenauskunft ? \count($ergebnisDatenauskunft) : 0;
$countBlacklist = $ergebnisBlacklist ? \count($ergebnisBlacklist) : 0;


/**
 * navigation reiter
 */
?>
<ul class="ui-tabs-nav">
    <li><a href="#alle">Alle Treffer (<?php echo $countGesamt; ?>)</a></li>

<?php
if (isset($adminGruppe[$mandant]) 
	&& (
		\is_array($adminGruppe[$mandant]) 
		&& \count($adminGruppe[$mandant]) > 0
	)
) {
	foreach ($adminGruppe[$mandant] as $tmpItem) {
		?>
		<li><a href="#<?php echo $tmpItem; ?>"><?php echo \ucfirst(\str_replace('_', ' ', $tmpItem)); ?> (<?php echo $countArray[$tmpItem]; ?>)</a></li>
		<?php
	}
} else {
	?>
	<li><a href="#<?php echo $mandant; ?>"><?php echo \ucfirst($mandant) . ' (' . $countArray[$mandant] . ')'; ?></a></li>
	<?php
}

$tabId = '';
if ($countDatenauskunft > 0) {
    $tabId = 'id="dais_auskunft"';
}
?>
    <li <?php echo $tabId; ?>><a href="#datenauskunft">Datenauskunft (<?php echo $countDatenauskunft; ?>)</a></li>
    <li><a href="#blacklist">Blacklist (<?php echo $countBlacklist; ?>)</a></li>
</ul>
<?php
/**
 * divs inhalt anzeigen
 */
if (isset($adminGruppe[$mandant]) 
	&& (
		\is_array($adminGruppe[$mandant]) 
		&& \count($adminGruppe[$mandant]) > 0
	)
) {
	// alle anzeigen
	\showTableInfos(
		'header',
		'alle',
		$countGesamt
	);
	if ($countGesamt > 0) {
		foreach ($adminGruppe[$mandant] as $subClient) {
			\showList(
				$ergebnisArray[$subClient],
				'alle',
				$subClient
			);
		}
	} else {
		\showTableInfos(
			'noContent',
			$mandant,
			''
		);
	}
	\showTableInfos(
		'footer',
		'alle',
		''
	);
	
	foreach ($adminGruppe[$mandant] as $subClient) {
		\showTableInfos(
			'header',
			$subClient,
			$countArray[$subClient]
		);
		
		if ($countArray[$subClient] > 0) {
			\showList(
				$ergebnisArray[$subClient],
				$subClient,
				$subClient
			);
		} else {
			\showTableInfos(
				'noContent',
				$subClient,
				''
			);
		}
		
		\showTableInfos(
			'footer',
			$subClient,
			''
		);
	}
} else {
	\showTableInfos(
		'header',
		'alle',
		$countGesamt
	);
	if ($countGesamt > 0) {
		\showList(
			$ergebnisArray[$mandant],
			'alle',
			$mandant
		);
	} else {
		\showTableInfos(
			'noContent',
			$mandant,
			''
		);
	}

	\showTableInfos(
		'footer',
		'alle',
		''
	);

	\showTableInfos(
		'header',
		$mandant,
		$countArray[$mandant]
	);

	if ($countArray[$mandant] > 0) {
		\showList(
			$ergebnisArray[$mandant],
			$mandant,
			$mandant
		);
	} else {
		\showTableInfos(
			'noContent',
			$mandant,
			''
		);
	}

	\showTableInfos(
		'footer',
		$mandant,
		''
	);
}

// datenauskunft
\showTableInfos(
	'header',
	'datenauskunft',
	$countDatenauskunft
);
if (\count($ergebnisDatenauskunft) > 0) {
	\showList(
		$ergebnisDatenauskunft,
		'datenauskunft',
		'datenauskunft'
	);
} else {
	\showTableInfos(
		'noContent',
		'datenauskunft',
		''
	);
}
\showTableInfos(
	'footer',
	'datenauskunft',
	''
);


// blacklist
\showTableInfos(
	'header',
	'blacklist',
	$countBlacklist
);
if (\count($ergebnisBlacklist) > 0) {
	\showList(
		$ergebnisBlacklist,
		'blacklist',
		'blacklist',
		$userDataArray
	);
} else {
	\showTableInfos(
		'noContent',
		'blacklist',
		''
	);
}
\showTableInfos(
	'footer',
	'blacklist',
	''
);