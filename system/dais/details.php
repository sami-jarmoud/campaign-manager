<?php
$doi_date = $doi_ip = $checked = $readOnly = $suplierReadOnly = $submitFormSettings = $anmeldung_uhrzeit = '';
$mandantId = $datenauskunftId = 0;

// lieferanten (partner tabelle)
$detailsLieferantenArr = array(
    'firma' => '',
    'strasse' => '',
    'plz' => '',
    'ort' => '',
    'geschaeftsfuehrer' => '',
    'telefon' => '',
    'fax' => '',
    'website' => ''
);


// debug
\DebugAndExceptionUtils::showDebugData($_POST, 'post variable');

// username aus dem $_POST in 3 variable teilen/splitten ($mandant, $username, $typ)
$string = preg_split('/:/', $_POST['user_id']);
$db_abkz = isset($string[0]) ? $string[0] : '';
$mandantIntern = $mandant;

#$mandantIntern = isset($string[0]) ? $string[0] : '';
#$db_abkz = $mandantIntern;

$username = isset($string[1]) ? $string[1] : '';
// gilt nur beim datenauskunft (dais bzw. ais)
$typ = isset($string[2]) ? $string[2] : '';

// debug
$debugInfoArray = array(
    'mandantIntern' => $mandantIntern,
    'db_abkz' => $db_abkz,
    'username' => $username,
    'typ' => $typ
);
\DebugAndExceptionUtils::showDebugData($debugInfoArray, 'debugInfoArray');

$readOnly = '';
$suplierReadOnly = 'readonly="readonly"';
$jsOnClickFunctionName = 'formCheckIo();';

// allgemeine einstellungen
if ($string[0] != 'manuelle_eingabe') {
    $manuelle_eingabe = false;
} else {
    // manuelle eingabe - einstellungen
    $manuelle_eingabe = true;
    $mandantIntern = $mandant;

    $detailsUserArr = array(
        'anrede' => '',
        'vorname' => '',
        'nachname' => '',
        'strasse' => '',
        'plz' => '',
        'ort' => '',
        'land' => '',
        'partner' => '',
        'ip-adresse' => '',
        'herkunft' => '',
        'geburtsdatum' => '',
		'telefon' => '',
    );

    $header = '<h2 id="details">Benutzerdaten Manuell erfassen:</h2>';
    $email = '<input type="text" name="email" value="' . $string[1] . '" id="email" />';
    $username = '';
	
	$suplierReadOnly = '';
}


// formular mit dem user details wird aufgerufen
if (!$manuelle_eingabe) {
    $header = '<h2 id="details">Benutzerdaten Details:</h2>';

    // datenauskunft tabelle abfragen ob es schon einen eintrag gibt (hat priorit�t)
    if ($typ == 'Datenauskunft') {
        // mandanten infos aufrufen
        foreach (mandanten_details($mandantIntern) as $key => $value) {
            $detailsMandantenArr[strtolower($key)] = convertToEncoding(
                $value,
                $encoding
            );
        }
        $mandantId = $detailsMandantenArr['id'] ? $detailsMandantenArr['id'] : 0;

        // gespeicherte daten aufrufen (nur beim datenauskunft)
        foreach (users_details('bl_db', $username, 'Datenauskunft', $mandantId, $typ, $db_abkz) as $key => $value) {
            $detailsDatenauskunftArr[strtolower($key)] = convertToEncoding(
                $value,
                $encoding
            );
        }
        $datenauskunftId = $detailsDatenauskunftArr['id'] ? $detailsDatenauskunftArr['id'] : 0;

        $detailsUserArrTmp = unserialize($detailsDatenauskunftArr['zusammenfassung']);
        foreach ($detailsUserArrTmp as $key => $value) {
            $detailsUserArr[strtolower($key)] = convertToEncoding(
                $value,
                $encoding
            );
        }
        unset($detailsUserArrTmp);

        if (isset($detailsUserArr['lieferanten'])) {
            if (count($detailsLieferantenArr) > 0) {
                foreach ($detailsUserArr['lieferanten'] as $key => $value) {
                    $detailsLieferantenArr[strtolower($key)] = convertToEncoding(
                        $value,
                        $encoding
                    );
                }
            }
        } else {
            // lieferanten informationen anhand der partner variable (gilt nur bei ältere schreibens
            foreach (lieferanten_details(strtolower($detailsMandantenArr['abkz']), $detailsUserArr['partner']) as $key => $value) {
                $detailsLieferantenArr[strtolower($key)] = convertToEncoding(
                    $value,
                    $encoding
                );
            }
        }

        $anmeldung_uhrzeit = $detailsUserArr['anmeldung'];
        if (isset($detailsUserArr['doi_datum']) && strlen($detailsUserArr['doi_datum']) > 0) {
            // doi daten vorhanden
            $doi_date = $detailsUserArr['doi_datum'];
            $doi_ip = $detailsUserArr['doi_ip'];
        }

        // dais_anfrage_datum
        if (!$detailsUserArr['dais_anfrage_datum']) {
            $detailsUserArr['dais_anfrage_datum'] = date('d.m.Y');
        }

        // geburtsdatum
        if (!$detailsUserArr['geburtsdatum']) {
            $detailsUserArr['geburtsdatum'] = '';
        }
		
		// telefon
        if (!$detailsUserArr['telefon']) {
            $detailsUserArr['telefon'] = '';
        }

        // TODO: mandantIntern
        $tmpString = explode('_', $mandantIntern);
        $mandantIntern = isset($tmpString['0']) ? $tmpString['0'] : $mandantIntern;
        $username = $detailsDatenauskunftArr['username'];
    } else {
		$detailsDatenauskunftArr = null;
		
        // mandanten details
        foreach (mandanten_details($mandantIntern) as $key => $value) {
            $detailsMandantenArr[strtolower($key)] = convertToEncoding(
                $value,
                $encoding
            );
        }
		
        $mandantId = $detailsMandantenArr['id'] ? $detailsMandantenArr['id'] : 0;

        if (!empty($typ)) {
            // gilt beim typ ais bzw. datenauskunft Extern
            $tmpUser = users_details(
                'bl_db',
                $username,
                'Datenauskunft',
                $mandantId,
                $typ,
                $db_abkz
            );
            $username = $tmpUser['Username'];
        }

        switch ($_SESSION['mID']) {
			case 4:
				foreach (users_details($db_abkz, $username) as $key => $value) {
                    $detailsUserArr[strtolower($key)] = convertToEncoding(
                        $value,
                        $encoding
                    );
                }
				
				// lieferanten informationen
                foreach (lieferanten_details($db_abkz, $detailsUserArr['partner']) as $key => $value) {
                    $detailsLieferantenArr[strtolower($key)] = convertToEncoding(
                        $value,
                        $encoding
                    );
                }
				
				//  herkunft neu erstellen, gilt nur f�r planet49,
				if ($detailsUserArr['partner'] == '102' || $detailsUserArr['partner'] == 'planet49') {
					$herkunft = herkunftUrl($detailsUserArr['herkunft'], ($db_abkz == 'intone' ? $intone_db : $intone_opt_db));
					
					if ($herkunft) {
						$detailsUserArr['herkunft'] = $herkunft;
					}
				}
				break;
			
            default:
                foreach (users_details($db_abkz, $username) as $key => $value) {
                    $detailsUserArr[strtolower($key)] = convertToEncoding(
                        $value,
                        $encoding
                    );
                }

                // lieferanten informationen
                foreach (lieferanten_details($db_abkz, $detailsUserArr['partner']) as $key => $value) {
                    $detailsLieferantenArr[strtolower($key)] = convertToEncoding(
                        $value,
                        $encoding
                    );
                }
                break;
        }


        $anmeldung = strtotime($detailsUserArr['anmeldung']);

        // wenn anmelde datum vorhanden ist verwende es weiter ansonsten lasse es leer
        if (trim($detailsUserArr['anmeldung']) != '' && $detailsUserArr['anmeldung'] != '0000-00-00 00:00:00') {
            // datum und uhrzeit in format dd-mm-yy H:i:s umwandeln
            $anmeldung_uhrzeit = date('d.m.Y - H:i:s', $anmeldung);
        }

        if (isset($detailsUserArr['doi_datum']) && $detailsUserArr['doi_datum']) {
            // doi daten vorhanden
            $doi_date = date_converter($detailsUserArr['doi_datum'], 'd.m.Y H:i:s');
            $doi_ip = $detailsUserArr['doi_ip'];
        }

        // dais_anfrage_datum festlegen
        $detailsUserArr['dais_anfrage_datum'] = date('d.m.Y');
    }

    // dais einträge suchen
    $daisInfoArr = daisInfos(
        $mandantId,
        $detailsUserArr['email'],
        'Datenauskunft'
    );
    
    // externe dais einträge suchen
    $daisExternInfoArr = daisInfos(
        $mandantId,
        $detailsUserArr['email'],
        'Datenauskunft_Extern'
    );
    
    // blacklist eintrage suchen
    $detailsBacklistArr = users_details(
        'bl_db',
        $detailsUserArr['email'],
        'Blacklist'
    );

    $email = '<input name="email" type="hidden" value="' . $detailsUserArr['email'] . '" id="email" /><div style="padding: 6px 0 0 0; font-weight: bold;">' . $detailsUserArr['email'] . '</div>';
}

// checkSuplierDataIfTheyAreFilled
$checkSuplierDataIfTheyAreFilled = \checkSuplierDataIfTheyAreFilled($detailsLieferantenArr);

#########################################
// debug
\DebugAndExceptionUtils::showDebugData($detailsMandantenArr, 'detailsMandantenArr');
\DebugAndExceptionUtils::showDebugData($detailsUserArr, 'detailsUserArr');
\DebugAndExceptionUtils::showDebugData($detailsLieferantenArr, 'detailsLieferantenArr');
\DebugAndExceptionUtils::showDebugData($detailsDatenauskunftArr, 'detailsDatenauskunftArr');
\DebugAndExceptionUtils::showDebugData($daisInfoArr, 'daisInfoArr');
\DebugAndExceptionUtils::showDebugData($daisExternInfoArr, 'daisExternInfoArr');
\DebugAndExceptionUtils::showDebugData($detailsBacklistArr, 'detailsBacklistArr');
\DebugAndExceptionUtils::showDebugData($mandant, 'mandant');
\DebugAndExceptionUtils::showDebugData($mandantIntern, 'mandantIntern');
\DebugAndExceptionUtils::showDebugData($adminZugang, 'adminZugang');
\DebugAndExceptionUtils::showDebugData($silverGroupArray, '$silverGroupArray');

$isBlacklist = 0;
if (!empty($detailsBacklistArr)) {
    $isBlacklist = 1;
}

echo $header;

// felder bei mandand zmail sowie manuelle eingabe auf bearbeiten stellen
if (in_array($mandantIntern, $silverGroupArray) || $manuelle_eingabe != true) {
    $readOnly = 'readonly="readonly"';
}
	
if (!$checkSuplierDataIfTheyAreFilled) {
	$submitFormSettings = 'style="visibility: hidden;" disabled="disabled"';
}

if ($manuelle_eingabe) {
	$checkSuplierDataIfTheyAreFilled = 1;
}
?>

<form name="form_details" id="form_details" action="include/php2pdf.php?KeepThis=true&amp;TB_iframe=true&amp;width=400&amp;height=200&amp;modal=true" method="post" onsubmit="return tbSendform(this, '')" >
    <div style="float:left; width: 63%;">
        <fieldset class="edit">
            <legend>Allgemeine Informationen:</legend>
            <label for="email">Email:</label> <?php echo $email; ?><br class="clear-float" />
            <label for="anrede">Anrede:</label><input type="text" name="anrede" id="anrede" value="<?php echo $detailsUserArr['anrede']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="vorname">Vorname:</label><input type="text" name="vorname" id="vorname" value="<?php echo $detailsUserArr['vorname']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="nachname">Nachname:</label><input type="text" name="nachname" id="nachname" value="<?php echo $detailsUserArr['nachname']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="strasse">Strasse:</label><input type="text" name="strasse" id="strasse" value="<?php echo $detailsUserArr['strasse']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="plz">PLZ - Ort:</label><input type="text" name="plz" id="plz" class="plz" value="<?php echo $detailsUserArr['plz']; ?>" <?php echo $readOnly; ?> /> - <input type="text" name="ort" id="ort" class="ort" value="<?php echo $detailsUserArr['ort']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="land">Land:</label><input type="text" name="land" id="land" value="<?php echo $detailsUserArr['land']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" /><br />
			<label for="telefon">Telefon:</label><input type="text" name="telefon" id="telefon" value="<?php echo $detailsUserArr['telefon']; ?>" <?php echo $readOnly; ?> /><br class="clear-float" /><br />
        </fieldset>

        <fieldset class="edit">
            <legend>Herkunfts Informationen:</legend>
            <label for="partner">Partner:</label><input name="partner" type="hidden" id="partner" value="<?php echo $detailsUserArr['partner']; ?>" /><div style="padding: 6px 0 0 0;"><?php echo $detailsUserArr['partner']; ?></div><br class="clear-float" />
            <label for="herkunft">Herkunft:</label><input type="text" name="herkunft" id="herkunft" value="<?php echo $detailsUserArr['herkunft']; ?>" <?php echo $readOnly; ?> onchange="return <?php echo $jsOnClickFunctionName; ?>" /><br class="clear-float" />
            <label for="anmeldung">Eintragsdatum:</label><input type="text" name="anmeldung" id="anmeldung_readonly" value="<?php echo $anmeldung_uhrzeit; ?>" onchange="return <?php echo $jsOnClickFunctionName; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            <label for="dais_anfrage_datum">Anfragedatum:</label><input type="text" name="dais_anfrage_datum" id="dais_anfrage_datum" value="<?php echo $detailsUserArr['dais_anfrage_datum']; ?>"  /><br class="clear-float" />
            <label for="anmeldung_ip">IP-Adresse:</label><input type="text" name="anmeldung_ip" id="anmeldung_ip" value="<?php echo $detailsUserArr['ip-adresse']; ?>" onchange="return <?php echo $jsOnClickFunctionName; ?>" <?php echo $readOnly; ?> /><br class="clear-float" />
            
            <?php
            if (strlen($doi_date) > 0) {
                ?>
                <label for="doi_datum">Double Opt-In Datum:</label><input type="text" name="doi_datum" value="<?php echo $doi_date; ?>" readonly="readonly" /><br class="clear-float" /><br />
                <label for="doi_ip">Double Opt-In IP:</label><input type="text" name="doi_ip" value="<?php echo $doi_ip; ?>" readonly="readonly" /><br class="clear-float" /><br />
                <?php
            } if ($manuelle_eingabe) {
				?>
                <label for="doi_datum">Double Opt-In Datum:</label><input type="text" name="doi_datum" value="" /><br class="clear-float" /><br />
                <label for="doi_ip">Double Opt-In IP:</label><input type="text" name="doi_ip" value="" /><br class="clear-float" /><br />
                <?php
			} else {
                ?>
                <input name="doi_datum" type="hidden" value="<?php echo $doi_date; ?>" />
                <input name="doi_ip" type="hidden" value="<?php echo $doi_ip; ?>" />
                <?php
            }
            ?><br />
        </fieldset>

        <fieldset class="edit">
            <legend>Lieferanten Informationen:</legend>
			
			<?php 
				echo \getSuplierCheckResultByClient(
					$checkSuplierDataIfTheyAreFilled,
					$mandantIntern
				);
			?>
            <label for="lieferanten_firma">Firma:</label><input name="lieferanten[firma]" type="text" id="lieferanten_firma" value="<?php echo $detailsLieferantenArr['firma']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_strasse">Anschrift:</label><input name="lieferanten[strasse]" type="text" id="lieferanten_strasse" value="<?php echo $detailsLieferantenArr['strasse']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_plz">PLZ - Ort:</label><input name="lieferanten[plz]" type="text" id="lieferanten_plz" class="plz" value="<?php echo $detailsLieferantenArr['plz']; ?>" <?php echo $suplierReadOnly; ?> /> - <input name="lieferanten[ort]" type="text" id="lieferanten_ort" class="ort" value="<?php echo $detailsLieferantenArr['ort']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_geschaeftsfuehrer">Gesch&auml;ftsf&uuml;hrer:</label><input name="lieferanten[geschaeftsfuehrer]" type="text" id="lieferanten_geschaeftsfuehrer" value="<?php echo $detailsLieferantenArr['geschaeftsfuehrer']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_telefon">Telefon:</label><input name="lieferanten[telefon]" type="text" id="lieferanten_telefon" value="<?php echo $detailsLieferantenArr['telefon']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_fax">Fax:</label><input name="lieferanten[fax]" type="text" id="lieferanten_fax" value="<?php echo $detailsLieferantenArr['fax']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
            <label for="lieferanten_website">Web:</label><input name="lieferanten[website]" type="text" id="lieferanten_website" value="<?php echo $detailsLieferantenArr['website']; ?>" <?php echo $suplierReadOnly; ?> /><br class="clear-float" />
        </fieldset>

        <div id="aktionen">
            <fieldset class="edit" id="edit-neu">
                <legend>Aktionen:</legend>
                <div style="margin: 0 0 0 110px;">
                    <input name="pdf" type="radio" id="pdf-vorschau" value="pdf-vorschau" onclick="<?php echo $jsOnClickFunctionName; ?>" checked="checked" /><label for="pdf-vorschau">Datenauskunft PDF-Vorschau</label><br class="clear-float" />
                    <input type="radio" name="pdf" id="send-pdf-kunde" value="send-pdf-kunde" onclick="<?php echo $jsOnClickFunctionName; ?>" /><label for="send-pdf-kunde">Datenauskunft an Kunden schicken</label><br class="clear-float" />
                    <input type="radio" name="pdf" id="send-pdf-email" value="send-pdf-email" onclick="<?php echo $jsOnClickFunctionName; ?>"/><label for="send-pdf-email">Datenauskunft an Dritten verschicken</label><br class="clear-float" />
                    <input type="radio" name="pdf" id="delete-contact" value="delete-contact" onclick="<?php echo $jsOnClickFunctionName; ?>"/><label for="delete-contact">Datensatz löschen</label><br class="clear-float" />
                    <div id="externe_email" style="visibility: hidden; display: none;">
                        <div id="externe_email_infos">
                            <div class="externe_email_header">Schnelleingabe:</div>
                            <div class="externe_email_header">Andere Email:</div>
                            <input type="text" name="externe_email_m" value="" onclick="<?php echo $jsOnClickFunctionName; ?>" onchange="<?php echo $jsOnClickFunctionName; ?>" /><br class="clear-float" />
                        </div>
                        
                        <?php
                        // nur wenn doi vorhanden und nur bei bestimmten mandanten (intoneGroup), TODO: später über die: $_SESSION['intoneGroup']
                        if (strlen($doi_date) > 0 
							&& !in_array($mandantIntern, $silverGroupArray)
							&& (
								$mandantIntern != 'intone' 
								&& $mandantIntern != 'complead'
							)
						) {
                            ?>
                            <input type="checkbox" name="doi_infos" id="doi_infos" value="1" /><label for="doi_infos" id="label_doi_infos">erweiterte Informationen (Eintragsdatum & Doi)</label><br class="clear-float" />
                            <?php
                        } else {
                            echo '<input type="hidden" name="doi_infos" value="0" />';
                        }
                        ?>
                    </div>
                    <?php
                    // wenn blacklist eintrag schon vorhanden ist blende das checkbox aus, ansonsten zeige es an
                    if (empty($detailsBacklistArr)) {
                        ?>
                        <div id="blacklist_option">
                            <input type="checkbox" name="black_list" id="black_list" checked="checked" value="ja" onclick="auswahl();" /><label for="black_list">Blacklist Eintrag (optional)</label><br class="clear-float" />
                        </div>
                        <div id="blacklist">
                            <div class="externe_email_header">Grund:</div>
                            <input type="text" name="grund" id="grund" value="Datenauskunft" />
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </fieldset>
        </div>

        <div id="error_message">  </div>

        <fieldset class="edit">
            <label>&nbsp;</label>
            <input name="username" type="hidden" value="<?php echo $username; ?>" />
            <input name="mitarbeiter" type="hidden" value="<?php echo $_POST['mitarbeiter']; ?>" />
            <input name="mandant" type="hidden" value="<?php echo $mandant; ?>" />
            <input name="mandant_intern" type="hidden" value="<?php echo $mandantIntern; ?>" />
            <input name="manuelle_eingabe" type="hidden" value="<?php echo $manuelle_eingabe; ?>" />
            <input name="senden" type="hidden" value="1" />
            <input name="db_abkz" type="hidden" value="<?php echo $db_abkz; ?>" />
            <input name="isBlacklist" type="hidden" value="<?php echo $isBlacklist; ?>" />
            <input name="mandantId" type="hidden" value="<?php echo intval($_SESSION['mID']); ?>" />
			<input name="checkSuplierDataIfTheyAreFilled" type="hidden" value="<?php echo \intval($checkSuplierDataIfTheyAreFilled); ?>" />
			<input name="geburtsdatum" type="hidden" value="" />
			<input type="submit" name="weiter" <?php echo $submitFormSettings; ?> id="weiter" value="weiter" />
            <input type="button" name="update" id="update" style="visibility: hidden;" value="aktualisieren" onclick="return <?php echo $jsOnClickFunctionName; ?>" />
        </fieldset>
    </div>

    <div style="float:right; width:34%;" id="infobox">
        <a href="?action=search&amp;mitarbeiter=<?php echo $mitarbeiter; ?>&amp;mandant=<?php echo $mandant; ?>&amp;search_option=normale_suche&amp;search=<?php echo $detailsUserArr['email']; ?>" style="font-weight: bold; font-size: 12px; margin: 0 0 15px 0; color: #3A83B6; display: block;">zurück zur Übersicht</a>
        <div style="margin: 0 0 30px 0;">
            <p style="margin: 0 0 10px 0;"><b>Herkunftsdatenbank: <?php echo $mandantIntern; ?></b></p>
            <?php
            // auswahlmenü außer beim manueller eintrag nicht anzeigen
            if ($manuelle_eingabe) {
                ?>
                <fieldset style="float:left;" >
                    <legend>Auskunftgeber:</legend>
                    <select name="mandant_intern" onchange="mandanten_wechsel(this);"  onkeydown="mandanten_wechsel(this);"  onkeyup="mandanten_wechsel(this);" >
                        <?php echo select_mandant($mandantIntern); ?>
                    </select>
                </fieldset>
                <?php
            }
            ?>
            <img src="mandanten_templates/<?php echo $mandantIntern; ?>/logo.gif" name="mandanten" id="mandanten" style="float:left; margin: 6px 0 0 20px; max-width: 200px;" alt="mandanten logo" /><br class="clear-float" />
        </div>

        <?php
        if (!empty($daisInfoArr) || !empty($daisExternInfoArr) || !empty($detailsBacklistArr)) {
            ?>
            <div>
                <?php
                // hinweis DOI (Double Opt In)
                if (!empty($daisInfoArr)) {
                    include('vorlagen/php/dais_info.php');
                }

                if (!empty($daisExternInfoArr)) {
                    include('vorlagen/php/externe_dais_info.php');
                }

                // hinweis BlackList eintrag
                if (!empty($detailsBacklistArr)) {
                    include('vorlagen/php/blacklist_info.php');
                }
                ?>
                <br class="clear-float" />
            </div>
            <?php
        }
        ?>
    </div><br class="clear-float" />
</form>

<div id="dialogConfirm" style="display: none;">
    <p style="margin: 10px 0; font-weight: bold;">
        Der Kunde befindet sich auf der Blacklist.<br />
        M&ouml;chten Sie wirklich die Mail an ihn senden?<br /><br />
    </p>
</div>

<?php
// speicher freigeben
unset($detailsDatenauskunftArr);
unset($detailsBacklistArr);
unset($detailsUserArr);
?>