<?php
/**
 * debug_info
 * 
 * @param   string  $name
 * @param   array   $array_daten
 * @param   boolean $debug_status
 * 
 * @return boolean 
 */
function debug_info($name, $array_daten, $debug_status) {
    if ($debug_status == true) {
        echo $name . '<br />';
        echo '<pre>';
        print_r($array_daten);
        echo '</pre>';

        return true;
    }
}


/**
 * sendMail
 * 
 * @param   string  $nachricht
 * 
 * @return  void
 */
function sendMail($nachricht) {
    $header = 'MIME-Version: 1.0' . chr(13) . chr(10);
    $header .= 'Content-type: text/html; charset=iso-8859-1' . chr(13) . chr(10);
    $header .= 'From: Intone API Mailer <info@maas.treaction.de>' . chr(13) . chr(10);
    $header .= 'X-Mailer: PHP/' . phpversion();

    mail('sami.jarmoud@treaction.de', 'DAIS Stoerung', $nachricht, $header);
}


/**
 * date_converter
 * 
 * datum ins deutsches datum konvertieren
 *
 * @param   string  $datum
 * @param   string  $format
 *
 * @return  string
 */
function date_converter($datum, $format = 'd.m.Y') {
    $datum_neu = '';

    if ($datum != '0000-00-00 00:00:00') {
        $datum_db = strtotime($datum);
        $datum_neu = date($format, $datum_db);
    }

    return $datum_neu;
}


/**
 * convertToEncoding
 * 
 * @param   string  $stringValue
 * @param   string  $encoding
 *
 * @return  string
 */
function convertToEncoding($stringValue, $encoding) {
    switch ($encoding) {
        case 'UTF-8':
            // latin1 zeichensätze werden im utf-8 zeichensätze konvertiert
            if (mb_detect_encoding($stringValue, 'UTF-8,ISO-8859-1') == 'ISO-8859-1') {
                $stringValue = utf8_encode($stringValue);
            }
            break;

        case 'ISO-8859-1':
            // utf-8 zeichensätze werden in latin1 zeichensätze konvertiert
            if (mb_detect_encoding($stringValue, 'UTF-8,ISO-8859-1') == 'UTF-8') {
                $stringValue = utf8_decode($stringValue);
            }
            break;

        case 'silver_encoding':
            $stringValue = utf8_encode($stringValue);
            break;
    }

    return $stringValue;
}


/**
 * date_converter_en
 * 
 * @param   string  $datum
 *
 * @return  string
 */
function date_converter_en($datum) {
    $datum_temp = explode('.', $datum);
    $jahr_array = explode('-', $datum_temp[2]);

    $datum_neu = trim($jahr_array[0]) . '-' . $datum_temp[1] . '-' . $datum_temp[0] . ' ' . trim($jahr_array[1]);

    return $datum_neu;
}


/**
 * ergebnis_db
 * 
 * search abfragen und für die überschicht liste vorbereiten
 *
 * @param   string  $bezeichnung
 * @param   string  $searchValue
 * @param   string  $table
 * @param   string  $mandant
 *
 * @return  array
 */
function ergebnis_db($bezeichnung, $searchValue, $table = '', $mandant = '') {
    $feld = '`Email`';
    $dais = false;
    $row = null;
    $tmpRow = null;

    switch ($bezeichnung) {
        // blacklist db
        case 'bl_db':
            global $bl_db;

            $db = $bl_db;
            switch ($table) {
                case 'Datenauskunft':
                    $felder = '`Username`, `Email`, `Versanddatum_Normal`, `Permission_Email`, `Mitarbeiter`, `Typ`, `db_abkz`';
                    $table = '`Datenauskunft`';
                    $feld = '`Email`';
                    $order_by = '`Versanddatum_Normal` DESC, `db_abkz` ASC, `Typ` DESC';

                    $dais = true;
                    break;

                case 'Blacklist':
					$felder = '`b`.`Email`, `b`.`Datum`, `b`.`Grund`, `b`.`Notizen`, `m`.`mandant` as `Firma`, `b`.`Firma` as `FirmaAlt`, `b`.`Bearbeiter`';
                    $feld = '`b`.`Email`';
                    $table = '`Blacklist` as `b`' 
                        . ' LEFT JOIN `mandant_ems` as `m` ON `b`.`mandant` = `m`.`id`'
                    ;
                    $order_by = '`b`.`Datum` DESC';
                    break;
            }
            break;
        
        case 'demoClient':
            global $demoClient_db;

            $db = $demoClient_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
        
        case 'skylinePerformance':
            global $skylinePerformance_db;

            $db = $skylinePerformance_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
		
        case 'complead':
            global $complead_db;

            $db = $complead_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
			break;
			
		case 'intone':
            global $intone_db;

            $db = $intone_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
		
		// opdb Users, History, emsTabelle
		case 'optdb_users':
			global $intone_opt_db;

            $db = $intone_opt_db;
            $felder = '`ID` AS `Username`, `email`, `vorname`, `nachname`, `herkunft`';
            $table = '`Users`';
            $order_by = 'Anmeldung DESC';
			break;
		
		case 'optdb_history':
            global $intone_opt_db;

            $db = $intone_opt_db;
            $felder = '`ID` AS `Username`, `email`, `vorname`, `nachname`, `herkunft`';
            $table = '`History`';
            $order_by = 'Anmeldung DESC';
            break;
		
		case 'stellarPerformance':
            global $stellarPerformance_db;

            $db = $stellarPerformance_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
        
           case '4waveMarketing':
            global $waveMarketing_db;

            $db = $waveMarketing_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
        
         case 'leadWorld':
            global $leadworld_db;

            $db = $leadworld_db;
            $felder = '`history`.`ID` AS `Username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `Herkunft`'
            ;
            $table = '`history`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`'
            ;
            $feld = '`history`.`Email`';
            $order_by = '`history`.`EINTRAGSDATUM` DESC';
            break;
        
        
    }

    // xss attaken vermeiden
    $where = $feld . ' = "' . mysql_real_escape_string($searchValue) . '"';

    // beim datenauskunft nur das dais von der jeweiligen mandant anzeigen
    if ($dais == true) {
        $where .= ' AND `db_abkz` LIKE "' . $mandant . '"';
    }

    $sql = 'SELECT ' . $felder . 
        ' FROM ' . $table . 
        ' WHERE ' . $where . 
        ' ORDER BY ' . $order_by
    ;

    // sql durchf�hren
    if (($res = mysql_query($sql, $db))) {
        while (($rows = mysql_fetch_assoc($res))) {
            if ($dais == true) {
                $rows['User_Id'] = $rows['db_abkz'] . ':' . $rows['Email'] . ':' . $rows['Typ'];
            } else {
                if ($table != 'Blacklist') {
                    $rows['User_Id'] = $bezeichnung . ':' . $rows['Username'];
                }
            }

            $tmpRow[] = $rows;
        }
        mysql_free_result($res);

        if (is_array($tmpRow)) {
            foreach ($tmpRow as $key => $itemArr) {
                foreach ($itemArr as $key2 => $itemValue) {
                    $row[$key][strtolower($key2)] = $itemValue;
                }
            }
        }

        // debug
        debug_info($sql, $row, false);
    } else {
        // email an admin
        #sendMail(mysql_error($db));
        print_r(mysql_error($db));
    }

    return $row;
}


/**
 * herkunftUrl
 * 
 * @param   string  $herkunft
 * @param   resource  $clientDbResource
 *
 * @return  string
 */
function herkunftUrl($herkunft, $clientDbResource) {
    $result = null;

    $sql = 'SELECT `url`' . 
        ' FROM `herkunft`' . 
        ' WHERE `hid` = "' . mysql_real_escape_string($herkunft) . '"'
    ;

    if (($res = mysql_query($sql, $clientDbResource))) {
        $row = mysql_fetch_assoc($res);
        mysql_free_result($res);

        $result = $row['url'];

        // debug
        debug_info($sql, $row, false);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($clientDbResource));
    }

    return $result;
}


/**
 * showTableInfos
 * 
 * table_anzeigen($count, $datenArray, 'intone', 'div_id');
 *
 * @param   string  $typ
 * @param   string  $div_id
 * @param   intval  $count
 * 
 * @return  void
 */
function showTableInfos($typ, $div_id, $count) {
    global $mitarbeiter;
    global $mandant;

    switch ($typ) {
        case 'footer':
            include('vorlagen/html_content/tabelle_footer.php');
            break;

        case 'noContent';
            include('vorlagen/html_content/tabelle_nocontent.php');
            break;

        case 'header':
            $switchOption = $div_id;
            if ($div_id == 'alle' && $mandant != 'intone') {
                $switchOption = $mandant;
            }
            
            include('vorlagen/html_content/tabelle_header.php');
            break;
    }
}


/**
 * showList
 * 
 * @param   string  $daten_array
 * @param   string  $div_id
 * @param   string  $mandant_db
 * @param   array   $userDataArray
 * 
 * @return  void
 */
function showList($daten_array, $div_id, $mandant_db, array $userDataArray = array()) {
    if ($daten_array) {
        switch ($div_id) {
            case 'datenauskunft':
                include('vorlagen/html_content/tabelle_uebersicht_datenauskunft.php');
                break;

            case 'blacklist':
                include('vorlagen/html_content/tabelle_uebersicht_blacklist.php');
                break;

            default:
                $switchOption = $mandant_db;
                if ($div_id == 'alle' && $mandant_db != $_SESSION['mandant']) {
                    $switchOption = 'none';
                }
                
                include('vorlagen/html_content/tabelle_uebersicht.php');
                break;
        }
    }
}


/**
 * users_details
 * 
 * @param   string  $bezeichnung
 * @param   string  $searchValue
 * @param   string  $table
 * @param   intval  $mandantId
 * @param   string  $typ
 * @param   string  $db_abkz
 *
 * @return array
 */
function users_details($bezeichnung, $searchValue, $table = '', $mandantId = 0, $typ = '', $db_abkz = '') {
    $row = null;

    switch ($bezeichnung) {
        // blacklist db
        case 'bl_db':
            global $bl_db;

            $db = $bl_db;
            $felder = '*';

            switch ($table) {
                case 'Datenauskunft':
                    $table = '`Datenauskunft`';
                    $where = '`Email` = "' . mysql_real_escape_string($searchValue, $db) . '"' . 
                        ' AND `Mandant` = "' . $mandantId . '"' . 
                        ' AND `Typ` = "' . $typ . '"' . 
                        ' AND `db_abkz` = "' . $db_abkz . '"'
                    ;
                    break;

                case 'Blacklist':
                    $felder = '`b`.`Email`, `b`.`Datum`, `b`.`Grund`, `b`.`Notizen`, `m`.`mandant` as `Firma`, `b`.`Firma` as `FirmaAlt`';
                    $feld = '`b`.`Email`';
                    $table = '`Blacklist` as `b`' 
                        . ' LEFT JOIN `mandant_ems` as `m` ON `b`.`mandant` = `m`.`id`'
                    ;
                    $where = '`b`.`Email` = "' . mysql_real_escape_string($searchValue, $db) . '"';
                    break;
            }
            break;

		case 'demoClient':
            global $demoClient_db;

            $db = $demoClient_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
		
		case 'skylinePerformance':
            global $skylinePerformance_db;

            $db = $skylinePerformance_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
		
		case 'complead':
            global $complead_db;

            $db = $complead_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
		
		case 'intone':
            global $intone_db;

            $db = $intone_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
		
		// opdb Users, History, emsTabelle
		case 'optdb_users':
			global $intone_opt_db;

            $db = $intone_opt_db;
            $felder = '`Users`.`ID` AS `Username`, `Users`.`Email`, `Users`.`Anrede`, `Users`.`Vorname`, `Users`.`Nachname`, `Users`.`Strasse`' . 
                ', `Users`.`PLZ`, `Users`.`Ort`, `Users`.`Land`, `Users`.`Herkunft`, `Users`.`Anmeldung`, `Users`.`Herkunft_IP` AS `ip-adresse`, `Users`.`Telefon` AS `telefon`' . 
                ', `Partner`.`Pkz` AS `partner`'
            ;

            $table = '`Users` ' . 
                'LEFT JOIN `Partner` ON `Users`.`PID` = `Partner`.`PID`'
            ;

            $where = '`ID` = ' . intval($searchValue);
			break;
		
		case 'optdb_history':
            global $intone_opt_db;

            $db = $intone_opt_db;
            $felder = '`ID` AS `Username`, `Email`, `Anrede`, `Vorname`, `Nachname`, `Strasse`, `PLZ`, `Ort`, `Land`, `Herkunft`, `Anmeldung`' . 
                ', `Herkunft_IP` AS `ip-adresse`, `Partner`, `Telefon`'
            ;
            $table = '`History`';
            $where = '`ID` = ' . intval($searchValue);
            break;
		
		case 'stellarPerformance':
            global $stellarPerformance_db;

            $db = $stellarPerformance_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
        
        case '4waveMarketing':
            global $waveMarketing_db;

            $db = $waveMarketing_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
        
         case 'leadWorld':
            global $leadworld_db;

            $db = $leadworld_db;
            $felder = '`history`.`ID` AS `username`, `history`.`EMAIL`, `history`.`VORNAME`, `history`.`NACHNAME`, `history`.`STRASSE`, `history`.`PLZ`, `history`.`ORT`' . 
                ', `history`.`LAND`, `history`.`EINTRAGSDATUM` AS `Anmeldung`, `history`.`IP` AS `ip-adresse`, `history`.`DOI_DATE` AS `doi_datum`, `history`.`DOI_IP` AS `doi_ip`, `history`.`TELEFON` AS `telefon`' . 
                ', `anrede`.`ANREDE` AS `anrede`' . 
                ', `herkunft`.`HERKUNFT_NAME` AS `herkunft`' . 
                ', `partner`.`PKZ` AS `partner`'
            ;

            $table = '`history`' . 
                ' LEFT JOIN `anrede` ON `history`.`ANREDE` = `anrede`.`ID`' . 
                ' LEFT JOIN `herkunft` ON `history`.`HERKUNFT` = `herkunft`.`ID`' . 
                ' LEFT JOIN `import_datei` ON `history`.`IMPORT_NR` = `import_datei`.`IMPORT_NR`' . 
                ' LEFT JOIN `partner` ON `import_datei`.`PARTNER` = `partner`.`PID`'
            ;

            $where = '`history`.`ID` = ' . intval($searchValue);
            break;
    }

    $sql = 'SELECT ' . $felder . 
        ' FROM ' . $table . 
        ' WHERE ' . $where
    ;
    if (($res = mysql_query($sql, $db))) {
        $row = mysql_fetch_assoc($res);
        mysql_free_result($res);

        // debug
        debug_info($sql, $row, false);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($db));
    }

    return $row;
}


/**
 * daisInfos
 * 
 * @param   intval  $daisMandantId
 * @param   string  $searchValue
 * @param   string  $typ
 *
 * @return  array
 */
function daisInfos($daisMandantId, $searchValue, $typ) {
    global $bl_db;
    $row = null;
    $tmpRow = null;

    // richtigen sql befehl für die suche auswählen
    $sql = 'SELECT `Email`, `Versanddatum_Normal`, `Permission_Email`, `Typ`' . 
        ' FROM `Datenauskunft`' . 
        ' WHERE `Email` = "' . mysql_real_escape_string($searchValue, $bl_db) . '"' . 
            ' AND `Typ` = "' . $typ . '"' . 
            ' AND `Mandant` = "' . $daisMandantId . '"' . 
        ' ORDER BY `Typ` DESC'
    ;
    if (($res = mysql_query($sql, $bl_db))) {
        while (($rows = mysql_fetch_assoc($res))) {
            $tmpRow[] = $rows;
        }
        mysql_free_result($res);

        if (is_array($tmpRow)) {
            foreach ($tmpRow as $key => $itemArr) {
                foreach ($itemArr as $key2 => $itemValue) {
                    $row[$key][strtolower($key2)] = $itemValue;
                }
            }
        }

        // debug
        debug_info($sql, $row, false);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
    }

    return $row;
}


/**
 * select_mandant
 * 
 * mandanten wechsel nur beim manueller eintrag
 *
 * @param   string  $mandantIntern
 *
 * @return  string
 */
function select_mandant($mandantIntern) {
    global $adminGruppe;
    global $adminZugang;

    $content = '';
    if ($adminZugang) {
        foreach ($adminGruppe[$mandantIntern] as $tmpMandant) {
                $selected = '';

                if ($mandantIntern == $tmpMandant) {
                    $selected = 'selected="selected"';
                }

                $content .= '<option value="' . $tmpMandant . '" ' . $selected . ' >' . $tmpMandant . '</option>';
        }
    } else {
        $content = '<option value="' . $mandantIntern . '" selected="selected" >' . $mandantIntern . '</option>';
    }

    return $content;
}


/**
 * blacklist_eintrag
 * 
 * @param   string  $email
 * @param   string  $grund
 * @param   integer $mandantId
 * @param   string  $mitarbeiter
 * 
 * @return  void
 */
function blacklist_eintrag($email, $grund, $mandantId, $mitarbeiter) {
    global $bl_db;

    $sql = 'SELECT *' . 
        ' FROM `Blacklist`' . 
        ' WHERE `Email` = "' . mysql_real_escape_string($email, $bl_db) . '"'
    ;
    if (($result_bl = mysql_query($sql, $bl_db))) {
        $count = mysql_num_rows($result_bl);
        mysql_free_result($result_bl);

        if ($count == 0) {
            $sql_insert = 'INSERT INTO `Blacklist`' . 
                ' (`Email`, `Datum`, `Grund`, `mandant`, `Bearbeiter`) ' . 
                ' VALUES(' . 
                    '"' . mysql_real_escape_string($email, $bl_db) . '"' . 
                    ', NOW()' . 
                    ', "' . mysql_real_escape_string($grund, $bl_db) . '"' . 
                    ', "' . $mandantId . '"' . 
                    ', "' . $mitarbeiter . '"' . 
                ')'
            ;
            if (!($res = mysql_query($sql_insert, $bl_db))) {
                // email an admin
                sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
                
                die('Mysql Error: Der Administrator wurde informiert. Bitte versuchen Sie es spaeter nocheinmal.');
            }
        }
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
    }
}


/**
 * dais eintrag
 *
 * @param   intval  $daisMandantenId
 * @param   string  $mitarbeiter
 * @param   string  $email
 * @param   string  $username
 * @param   string  $permissionEmail
 * @param   string  $typ
 * @param   string  $zusammenfassung
 * @param   string  $dbAbkz
 * 
 * @return  void
 */
function dais_eintrag($daisMandantenId, $mitarbeiter, $email, $username, $permissionEmail, $typ, $zusammenfassung, $dbAbkz) {
    global $bl_db;

    // nach passende einträge suchen
    $sql = 'SELECT *' . 
        ' FROM `Datenauskunft`' . 
        ' WHERE `Mandant` = "' . $daisMandantenId . '"' . 
            ' AND `Typ` = "' . $typ . '"' . 
            ' AND `Email` = "' . mysql_real_escape_string($email, $bl_db) . '"' . 
            ' AND `db_abkz` = "' . mysql_real_escape_string($dbAbkz, $bl_db) . '"'
    ;
    
    if ($typ == 'Datenauskunft_Extern') {
        $sql .= ' AND `Permission_Email` = "' . mysql_real_escape_string($permissionEmail, $bl_db) . '"';
    }

    if (($res_dais = mysql_query($sql, $bl_db))) {
        $count = mysql_num_rows($res_dais);
        mysql_free_result($res_dais);

        // debug
        debug_info($sql, $count, false);

        // wenn nichts gefunden wurde erstelle einen neuen eintrag
        if ($count == 0) {
            $sql_insert = 'INSERT INTO `Datenauskunft`' . 
                ' (`Versanddatum_Normal`, `Mitarbeiter`, `Email`, `Username`, `Permission_Email`, `Mandant`, `Zusammenfassung`, `Typ`, `db_abkz`)' . 
                ' VALUES(' . 
                    'NOW()' . 
                    ', "' . $mitarbeiter . '"' . 
                    ', "' . mysql_real_escape_string($email, $bl_db) . '"' . 
                    ', "' . $username . '"' . 
                    ', "' . $permissionEmail . '"' . 
                    ', "' . $daisMandantenId . '"' . 
                    ', "' . addslashes($zusammenfassung) . '"' . 
                    ', "' . $typ . '"' . 
                    ', "' . $dbAbkz . '"' . 
                ')'
            ;
            if (!($res = mysql_query($sql_insert, $bl_db))) {
                // email an admin
                sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
                
                die('Mysql Error: Der Administrator wurde informiert. Bitte versuchen Sie es später nocheinmal.');
            }
        }
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
    }
}

/**
 * insertEmailForCronjob
 *
 * @param   intval  $daisMandantenId
 * @param   string  $mitarbeiter
 * @param   string  $email
 * @param   string  $username
 * @param   string  $zusammenfassung
 * @param   string  $dbAbkz
 * 
 * @return  Boolean
 */
function insertEmailForCronjob($daisMandantenId, $mitarbeiter, $email, $username,$permissionEmail, $zusammenfassung, $dbAbkz){
    global $bl_db;
    
    $sql = 'SELECT * FROM `DeleteContact` WHERE `Email` = \''.$email.'\' and `Mandant` = \''.$daisMandantenId.'\'';
    $checkEmail_sql = mysql_query($sql, $bl_db);
    $checkEmailCnt = mysql_num_rows($checkEmail_sql);
   
    if(intval($checkEmailCnt) === 0){
    $sql_insert = 'INSERT INTO `DeleteContact`' . 
                ' (`Mitarbeiter`, `Email`, `Username`, `db_abkz`, `Mandant`, `Zusammenfassung`, `Create_Datum`, `Permission_Email`)' . 
                ' VALUES(' . 
                    '"' . $mitarbeiter . '"' . 
                    ', "' . mysql_real_escape_string($email, $bl_db) . '"' . 
                    ', "' . $username . '"' . 
                    ', "' . $dbAbkz . '"' . 
                    ', "' . $daisMandantenId . '"' . 
                    ', "' . addslashes($zusammenfassung) . '"' . 
                    ', NOW()' .
                   ', "' . $permissionEmail . '"' .
                ')'
            ;
    
     if (!($res = mysql_query($sql_insert, $bl_db))) {
                // email an admin
                sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
                
                die('Mysql Error: Der Administrator wurde informiert. Bitte versuchen Sie es später nocheinmal.');
            }else{
                return TRUE;
            }
}
}
/**
 * mandanten_details
 * 
 * @param   string  $stringValue
 *
 * @return  array
 */
function mandanten_details($stringValue) {
    global $bl_db;
    $row = null;

    $sql = 'SELECT *' . 
        ' FROM `Mandanten`' . 
        ' WHERE `Abkz` LIKE "' . mysql_real_escape_string($stringValue) . '"'
    ;
    if (($res = mysql_query($sql, $bl_db))) {
        $row = mysql_fetch_assoc($res);
        mysql_free_result($res);

        // debug
        debug_info($sql, $row, false);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($bl_db));
    }

    return $row;
}


/**
 * lieferanten_details
 * 
 * @param   string  $mandant
 * @param   string  $stringValue
 *
 * @return  array
 */
function lieferanten_details($mandant, $stringValue) {
    $row = null;
    $fields = '`firma`, `strasse`, `plz`, `ort`, `geschaeftsfuehrer`, `telefon`, `fax`, `website`';

    // db auswahl (mandanten bzw. lieferanten db ausw�hlen)
    switch ($mandant) {
		case 'demoClient':
            global $demoClient_db;

            $db = $demoClient_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
			
		case 'skylinePerformance':
            global $skylinePerformance_db;

            $db = $skylinePerformance_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
			
		case 'complead':
            global $complead_db;

            $db = $complead_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
			
		case 'intone':
            global $intone_db;

            $db = $intone_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
			
		case 'optdb_history':
        case 'optdb_users':
            global $intone_opt_db;

            $db = $intone_opt_db;
            $table = '`Partner`';

            if (is_numeric($stringValue)) {
                $where = '`pid` LIKE "' . $stringValue . '"';
            } else {
                $where = '`pkz` LIKE "' . $stringValue . '"';
            }
            break;
			
		case 'stellarPerformance':
            global $stellarPerformance_db;

            $db = $stellarPerformance_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
            
            case '4waveMarketing':
            global $waveMarketing_db;

            $db = $waveMarketing_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
            
               case 'leadWorld':
            global $leadworld_db;

            $db = $leadworld_db;

            $fields = '`Firma`, `Strasse`, `PLZ`, `Ort`, `Geschaeftsfuehrer`, `Telefon`, `Fax`, `Website`';
            $table = '`partner`';

            if (is_numeric($stringValue)) {
                $where = '`PID` LIKE "' . $stringValue . '"';
            } else {
                $where = '`Pkz` LIKE "' . $stringValue . '"';
            }
            break;
    }

    $sql = 'SELECT ' . $fields . 
        ' FROM ' . $table . 
        ' WHERE ' . $where
    ;
    if (($res = mysql_query($sql, $db))) {
        $row = mysql_fetch_assoc($res);
        mysql_free_result($res);

        // debug
        debug_info($sql, $row, false);
    } else {
		$message = __FUNCTION__ . ':' . mysql_error($db) . \chr(11)
			. 'client: ' . $mandant . \chr(11)
			. 'stringValue: ' . $stringValue . \chr(11)
			. 'sql: ' . $sql . \chr(11)
			. 'db: ' . $db
		;
		
        // email an admin
        sendMail($message);
    }

    return $row;
}


/**
 * mail_to_username
 * 
 * email für den kunde vorbereiten
 *
 * @param   string  $toEmail
 * @param   string  $toName
 * @param   string  $emailBetreff
 * @param   string  $mandant
 * @param   array   $detailsMandantenArr
 * @param   string  $htmlBody
 * @param   string  $fileAttachment
 * @param   string  $fileName
 *
 * @return  string
 */
function mail_to_username($toEmail, $toName, $emailBetreff, $mandant, $detailsMandantenArr, $htmlBody, $fileAttachment = null, $fileName = null) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/system/phpmailer/class.phpmailer.php');
	
	$smtpSecure = '';
	
    // email und pdf encoding
    $encoding = 'ISO-8859-1';
	
    $output = '';

    $host = 'smtp.1und1.de';
	$bbcName = 'Service Datenauskunft';
	if($detailsMandantenArr['email'] === 'office@4wave-marketing.com'){
            $detailsMandantenArr['email'] = 'office@4wave-marketing.de';
           
        }
	$fromEmail = $detailsMandantenArr['email'];

    switch ($mandant) {
		case 'demoClient':
            $fromName = 'Datenschutz demoClient';
			
			$host = 'smtp.office365.com:587';
			$hostName = 'www.treaction.de';
			$fromEmail = $hostUsername = 'sami.jarmoud@treaction.onmicrosoft.com';
			$hostPassword = ';4Cva|fCSA3ljf/:';
			$smtpSecure = 'tls';
            break;
		
		case 'skylinePerformance':
            $fromName = 'Datenschutz Skyline Performance GmbH';

            $host = 'smtp.office365.com:587';
            $hostName = 'www.skyline-performance.de';
            $fromEmail = $hostUsername = 'kontakt@skylineperformance.onmicrosoft.com';
            $hostPassword = '4DcPJGgh5fYdin+.';
			
			$smtpSecure = 'tls';
            break;
		
		case 'complead':
            $fromName = 'Datenschutz Complead GmbH';

            $host = 'smtp.office365.com:587';
            $hostName = 'www.complead.de';
            $fromEmail = $hostUsername = 'kontakt@complead.de';
            $hostPassword = 'GxK=zIToc,cD\q';
			
			$smtpSecure = 'tls';
            break;
		
		case 'intone':
            $fromName = 'Datenschutz Interactive One GmbH';

			$host = 'smtp.1und1.de:587';
            $hostName = 'www.interactive-one.de';
            $hostUsername = 'treaction@interactive-one.de';
            $hostPassword = '+!84CF41Zirz';
			$smtpSecure = 'tls';
            break;
		
		case 'stellarPerformance':
            $fromName = 'Datenschutz Stellar Performance GmbH';

			$host = 'smtp.office365.com:587';
			$hostName = 'stellar-performance.de';
			$fromEmail = $hostUsername = 'csc@stellar-performance.de';
			$hostPassword = 'jK4.1WX2b!+p';
			$smtpSecure = 'tls';
            break;
        
              case '4waveMarketing':
            $fromName = 'Datenschutz Test 4Wave Marketing GmbH';

			$host = 'smtp.office365.com:587';
			$hostName = '4wave-marketing.de';
			$fromEmail = 'office@4wave-marketing.de';
                        $hostUsername = 'office@4wave-marketing.de';
			$hostPassword = 'djks!TTSss';
			$smtpSecure = 'tls';
            break;
        
        case 'leadWorld':
            $fromName = 'Datenschutz Lead World ltd';

			$host = 'smtp.office365.com:587';
			$hostName = 'stellar-performance.de';
			$fromEmail = $hostUsername = 'csc@stellar-performance.de';
			$hostPassword = 'jK4.1WX2b!+p';
			$smtpSecure = 'tls';
            break;
    }

    // email vorbereiten
    $mail = new PHPMailer();
    $mail->From = $fromEmail;
    $mail->FromName = convertToEncoding(
        $fromName,
        $encoding
    );
    $mail->Subject = convertToEncoding(
        $emailBetreff,
        $encoding
    );
	
    $mail->IsSMTP(true);
	$mail->SMTPSecure = $smtpSecure;
    $mail->Host = $host;
    $mail->Hostname = $hostName;
    $mail->SMTPAuth = true;
    $mail->Username = $hostUsername;
    $mail->Password = $hostPassword;
    $mail->Mailer = 'smtp';
	$mail->Body = $htmlBody;
    $mail->IsHTML(true);
    $mail->AddAddress(
        $toEmail,
        convertToEncoding(
            $toName,
            $encoding
		)
    );
    $mail->AddBCC(
        $detailsMandantenArr['email'],
        $bbcName
    );
 
    // bild url
    $mail->AddEmbeddedImage(
		'../mandanten_templates/' . $mandant . '/email_logo.jpg', 'firmenlogo_' . $detailsMandantenArr['abkz'],
		$detailsMandantenArr['abkz'],
		'base64',
		'image/jpeg'
	);

	// attachment
    if ($fileAttachment) {
        $mail->AddStringAttachment(
            $fileAttachment,
            $fileName,
            'base64',
            'application/octet-stream'
		);
	}
	
      
        
    // sende email
    if (!$mail->Send()) {
        $output = '<b>' . $mail->ErrorInfo . '</b><br /><br />';
    }
	
    // Clear all addresses and attachments for next loop
    $mail->ClearAddresses();
    if ($fileAttachment) {
        $mail->ClearAttachments();
	}
	
    return $output;
}


/**
 * info_seite
 * 
 * @param   string  $header
 * @param   string  $content
 * 
 * @return  void
 */
function info_seite($header, $content) {
    include('infobox.php');
}


/**
 * sendLieferantenInfos
 * 
 * @param   array   $detailsLieferantenArr
 * @param   array   $detailsMandantenArr
 * @param   string  $mandant
 *
 * @return  string
 */
function sendLieferantenInfos($detailsLieferantenArr, $detailsMandantenArr, $mandant) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/system/phpmailer/class.phpmailer.php');

    // email und pdf encoding
    $encoding = 'ISO-8859-1';

    $output = '';

    // HTML body laden
    $bodyHtml = file_get_contents('../vorlagen/mandanten/default/email_lieferanten_infos.html');

    // lieferantenDaten personalisieren
    $bodyHtml = preg_replace('/{lieferanten_(.*?)}/e', 'isset($detailsLieferantenArr[\'$1\']) ? $detailsLieferantenArr[\'$1\'] : ""', $bodyHtml);

    // footer bereich laden und personalisieren
    $emailSignaturHtml = file_get_contents('../vorlagen/mandanten/default/email_footer_template.html');
    $emailSignaturHtml = preg_replace('/{(.*?)}/e', 'isset($detailsMandantenArr[\'$1\']) ? $detailsMandantenArr[\'$1\'] : ""', $emailSignaturHtml);

    // email content zusammen führen
    $bodyHtml = str_replace('{emailSignaturHtml}', $emailSignaturHtml, $bodyHtml);


    // email vorbereiten
    $mail = new PHPMailer();
    $mail->From = 'dais@interactive-one.de';
    $mail->FromName = 'DAIS - Interactive One GmbH';
    $mail->Subject = 'Neue Lieferanten Infos';
    $mail->IsSMTP(true);
    $mail->Host = 'smtp.1und1.de';
    $mail->Hostname = 'www.interactive-one.de';
    $mail->SMTPAuth = true;
    $mail->Username = 'postmaster@interactive-one.de';
    $mail->Password = 'ati2109coyu2002+';
    $mail->Mailer = 'smtp';
    $mail->Body = convertToEncoding(
        $bodyHtml,
        $encoding
    );
    $mail->IsHTML(true);
    $mail->AddAddress('Berin.Arnaut@qmin.com', 'Berin Arnaut');
    $mail->AddEmbeddedImage(
        '../mandanten_templates/' . $mandant . '/email_logo.jpg',
        'firmenlogo_' . $mandant,
        $mandant,
        'base64',
        'image/jpeg'
    );

    // sende email
    if (!$mail->Send()) {
        $output = '<b>' . $mail->ErrorInfo . '</b><br /><br />';
    }

    return $output;
}


/**
 * getSuplierCheckResultByClient
 * 
 * @param boolean $suplierDataArray
 * @param string $client
 * @return string
 */
function getSuplierCheckResultByClient($checkSuplierDataIfTheyAreFilledResult, $client) {
	$result = '';
	
	if ((boolean) \intval($checkSuplierDataIfTheyAreFilledResult) !== true) {
		$result = \utf8_encode('<div class="error" style="margin-bottom: 1em;"><img src="../img/Tango/16/status/dialog-warning.png" /> Achtung: Die Lieferanteninformationen sind nicht vollst&auml;ndig.<br />Bitte erg&auml;nzen Sie diese!</div>');
	}
	
	return $result;
}

/**
 * checkSuplierDataIfTheyAreFilled
 * 
 * @param array $suplierDataArray
 * @return boolean
 */
function checkSuplierDataIfTheyAreFilled(array $suplierDataArray) {
	$result = true;
	
	foreach ($suplierDataArray as $value) {
		if (\strlen($value) === 0) {
			$result = false;
			break;
		}
	}
	
	return $result;
}


/**
 * getAllUserDataArray
 * 
 * @param   resource    $ems_db
 * @return  array
 */
function getAllUserDataArray($ems_db) {
    $userDataArray = array();
    
    $sql = 'SELECT benutzer_id, CONCAT(`vorname`, " ", `nachname`) as `name`' 
        . ' FROM `benutzer`'
    ;
    if (($res = mysql_query($sql, $ems_db))) {
        while (($row = mysql_fetch_array($res)) !== false) {
            $userDataArray[$row['benutzer_id']] = $row;
        }
        mysql_free_result($res);

        // debug
        debug_info($sql, $userDataArray, false);
    } else {
        // email an admin
        sendMail(__FUNCTION__ . ':' . mysql_error($ems_db));
    }

    return $userDataArray;
}


function getUserDataById($userDataArray, $userId) {
    $result = '';
    
    if (array_key_exists($userId, $userDataArray)) {
        $result = $userDataArray[$userId]['name'];
    }
    
    return $result;
}
?>