<?php
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mitarbeiter = $_SESSION['benutzer_id'];
$mandant = $_SESSION['mandant'];
$mandantAbkz = $mandant;

require_once('include/adminGroupSettings.php');


// datenbank verbindung
require_once('include/connect_inc.php');
require_once('include/functionen.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


// debug
#$debugLogManager->logData('SESSION', $_SESSION);
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
$debugLogManager->logData('mandantAbkz', $mandantAbkz);
$debugLogManager->logData('adminGruppe', $adminGruppe);
$debugLogManager->logData('connectionErrorDataArray', $connectionErrorDataArray);


$action = isset($_GET['action']) ? $_GET['action'] : '';
$debugLogManager->logData('action', $action);

$search = isset($_GET['search']) ? trim($_GET['search']) : '';
$debugLogManager->logData('search', $search);


$adminZugang = array_key_exists($mandant, $adminGruppe) ? true : false;
$debugLogManager->logData('adminZugang', $adminZugang);

// encoding f�r die zeichens�tze festlegen
$encoding = 'UTF-8';


$silverGroupArray = array();
$debugLogManager->logData('silverGroupArray', $silverGroupArray);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>DAIS - Daten-Auskunfts-Informations-System</title>

        <!-- cachen deaktivieren -->
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="cache-control" content="no-store" />
        <meta http-equiv="PRAGMA" content="no-cache" />
        <meta name="robots" content="none" />

        <!-- jquery plugins -->
        <script type="text/javascript" src="js/jquery/jquery.js"></script>
        <script type="text/javascript" src="js/jquery/tablesorter.js"></script>
        <script type="text/javascript" src="js/jquery/tablepager.js"></script>
        <script type="text/javascript" src="js/jquery/thickbox.js"></script>

        <!-- jquery ui -->
        <script type="text/javascript" src="js/jquery/ui/ui.core.js"></script>
        <script type="text/javascript" src="js/jquery/ui/ui.tabs.js"></script>
        <script type="text/javascript" src="js/jquery/ui/ui.datepicker.js"></script>
        <script type="text/javascript" src="js/jquery/ui/ui.draggable.js"></script>
        <script type="text/javascript" src="js/jquery/ui/ui.resizable.js"></script>
        <script type="text/javascript" src="js/jquery/ui/ui.dialog.js"></script>
        <script type="text/javascript" src="js/jquery/ui/language/ui.datepicker-de.js"></script>
        <script type="text/javascript" src="js/jquery/jquery_dimensions.js"></script>
        <script type="text/javascript" src="js/jquery/jquery_tooltip.js"></script>

        <script type="text/javascript" src="js/dais_jquery.js"></script>
        <script type="text/javascript" src="js/auswahl.js"></script>

        <link href="css/screen.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery/ui/ui.all.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery/table.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery/thickbox.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery/custom_jquery.css" rel="stylesheet" type="text/css" />

        <?php
        if ($action == 'details') {
            echo '<link href="css/jquery/ui/datepicker.css" rel="stylesheet" type="text/css" />';
        } else {
            echo '<link href="css/jquery/ui/tabs.css" rel="stylesheet" type="text/css" />';
        }
        ?>
    </head>
    <body>
        <div id="start">
            <h1>DAIS - Daten-Auskunfts-Informations-System</h1>
            <div id="content">
                <!-- such formular -->
                <div id="suche">
                    <form name="schnellsuche" method="get" action="index.php">
                        <fieldset>
                            <label for="search">E-Mail:</label><input type="text" id="search" name="search" value="<?php echo $search; ?>"  /> <input type="submit" name="submit"  value="go" />
                            <div>
                                <input type="radio" name="search_option" id="normale_suche" value="normale_suche" checked="checked" /> <label for="normale_suche">Normale suche</label>
                            </div>
                        </fieldset>
                        <input type="hidden" name="action" value="search" />
                        <input type="hidden" name="mitarbeiter" value="<?php echo intval($mitarbeiter); ?>" />
                        <input type="hidden" name="mandant" value="<?php echo $mandant; ?>" />
                    </form>
                </div>
                <!-- such formular - ende -->
                <div id="trennlinie">&nbsp;</div>
                <div id="inhalt">
                    <?php
                    switch ($action) {
                        case 'search':
                            if (!empty($search)) {
                                // wenn suchbegriff nicht leer ist zeige die ergebnisse
                                include 'output.php';
                            } else {
                                include 'home.php';
                            }
                            break;

                        case 'details':
                            include 'details.php';
                            break;

                        default:
                            include 'home.php';
                            break;
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
// datenbankverbindung trennen
require_once('include/close_connection.php');