function show_div(id) {
	document.getElementById(id).style.visibility = 'visible';
	document.getElementById(id).style.display = 'block';
}

function hidden_div(id) {
	document.getElementById(id).style.visibility = 'hidden';
	document.getElementById(id).style.display = 'none';
}

function checkDefaultForm(formObj) {
	var errorMessage = '';
	var reg = new RegExp(
		'^([a-zA-Z0-9\-\.\_\!\#\$\%\&\*\+\-\/\=\?\^\`\{\|\}\~]+)' + //Name
		'(\@)' + //@-Zeichen
		'([a-z0-9\-\.]+){2}' + //Domain
		'(\.)' + //Punkt
		'([a-z]{2,4})$'	//TLD
	);
	
	// send email einblenden bzw. ausblenden
	if (formObj.elements['send-pdf-email'].checked) {
		show_div('externe_email');

		if (formObj.externe_email_m.value === '') {
			errorMessage += '<li>Hinweis: Empfänger Email Adresse ist leer</li>';
		} else if (reg.test(formObj.externe_email_m.value) === false) {
			errorMessage += "<li>Empfänger Email Adresse ist falsch</li>";
		}
	} else {
		hidden_div('externe_email');

		// doi infos deaktivieren
		formObj.elements['doi_infos'].checked = false;
	}
	
          if (formObj.elements['delete-contact'].checked) {
                alert("Datensatz dauerhaft löschen? \n Es wird eine Kopie der Datenauskunft an das Zentrale Postfach gesendet");
        }else{
            
        }
	return errorMessage;
}

function showOrProcessFormCheckError(errorMessage) {
	if (errorMessage !== '') {
		show_div('error_message'); // error einblenden
		show_div('update'); // update button einblenden

		disabledSendForm();

		var errormsg = '<p><b>Bitte korrigieren Sie folgende Felder aus:</b></p>';
		errormsg += '<ul>' + errorMessage + '</ul>';

		var nachricht = document.getElementById('error_message');
		nachricht.style.display = 'block';
		nachricht.innerHTML = errormsg;
	} else {
		hidden_div('error_message'); // error ausblenden
		hidden_div('update'); // update button ausblenden

		show_div('weiter'); // submit einblenden
		document.getElementById('weiter').disabled = false;
	}
}


function formCheckIo() {
	var form = document.forms['form_details'];
	var error = '';
	
	if (parseInt(form.checkSuplierDataIfTheyAreFilled.value) === 0) {
		hidden_div('update'); // update button ausblenden
		
		disabledSendForm();
	} else {
		/**
		* checkDefaultForm
		* 
		* send email einblenden bzw. ausblenden
		*/
		error += checkDefaultForm(form);

		// showOrProcessFormCheckError
		showOrProcessFormCheckError(error);
	}
}

function disabledSendForm() {
	hidden_div('weiter'); // submit ausblenden
	
	document.getElementById('weiter').disabled = true;
}


function auswahl() {
	// blacklist einblenden bzw. ausblenden
	if (document.getElementById('black_list').checked === true) {
		show_div('blacklist');
	} else {
		hidden_div('blacklist');
	}
}

function mandanten_wechsel(select) {
	var client = select.options[select.options.selectedIndex].value;

	document.getElementById('mandanten').src = 'mandanten_templates/' + client + '/' + client + '.gif';
	document.form_details.elements['pdf-vorschau'].checked = true;

	formCheckIo();
}


// formular an thickerbox schicken
function tbSendform(f, c) {
	f.action.match(/(\bKeepThis=(true|false)&TB_iframe=true.+$)/);
	var url = RegExp.$1;

	f.target = '';
	if (f.elements['pdf-vorschau'].checked === true) {
		f.target = '_blank';
	} else if (parseInt(f.isBlacklist.value) === 1 
		&& f.elements['send-pdf-kunde'].checked === true
	) {
		$('#dialogConfirm').dialog({
			title: 'BlacklistConfirm',
			modal: true,
			close: function(event, ui) {
				$(this).dialog('destroy');
			},
			buttons: {
				'Ja': function() {
					$(this).dialog('close');

					tb_show(c, 'about:blank?' + url);
					f.target = $('#TB_iframeContent').attr('name');
					f.submit();

					return true;
				},
				'Nein': function() {
					$(this).dialog('close');
				}
			}
		});

		return false;
	} else {
		tb_show(c, 'about:blank?' + url);
		f.target = $('#TB_iframeContent').attr('name');
	}
}
