$(document).ready(function() {
    // tabs einstellungen
    $('#inhalt').tabs();
	
    // table sorter einstellungen
    $('.tablesorter').tablesorter({
        widthFixed: true,
        widgets: ['zebra'],
		
        // sortierung für folgende spalten (erste = 0; zweite = 1 usw.) ausschalten 
        headers: {
            0: {
                sorter: false
            },
            1: {
                sorter: false
            },
            2: {
                sorter: false
            },
            3: {
                sorter: false
            },
            4: {
                sorter: false
            },
            5: {
                sorter: false
            },
            6: {
                sorter: false
            }
        }
    })
	
    // table pager einstellungen
    $('#alle_sortable').tablePager({
        offset: 0,
        limit: 10,
        placeTop: false
    });
	
    // calender einstellungen
    date_obj = new Date();
    date_obj_hours = date_obj.getHours();
    date_obj_mins = date_obj.getMinutes();
    date_obj_sec = date_obj.getSeconds();
	
    if(date_obj_hours < 10) {
        date_obj_hours = '0' + date_obj_hours;
    }
    if(date_obj_mins < 10) {
        date_obj_mins = '0' + date_obj_mins;
    }
    if(date_obj_sec < 10) {
        date_obj_sec = '0' + date_obj_sec;
    }
    date_obj_time = "'" + date_obj_hours + ':' + date_obj_mins + ':' + date_obj_sec + "'";
	

    /*
    jQuery('#anmeldung').datepicker({
        showOn: 'both',
        buttonImage: 'imgs/cal.gif',
        buttonImageOnly: true,
        buttonText: 'Calender aufrufen',
        dateFormat: 'dd.mm.yy ',
        showButtonPanel: true
    });
    */
   
    $('#dais_anfrage_datum').datepicker({
        showOn: 'both',
        buttonImage: 'imgs/cal.gif',
        buttonImageOnly: true,
        buttonText: 'Calender aufrufen',
        dateFormat: 'dd.mm.yy',
        showButtonPanel: true
    });
	
    // tooltips
    $('.tablesorter img').tooltip({
        delay: 0,
        showURL: false,
        fade: 300,
        top: 0,
        left: 0,
        extraClass: 'tooltip_normal'
    });
});