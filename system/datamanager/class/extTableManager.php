<?php
class extTableManager {
    public function __construct($dbHost, $dbname, $dbuser, $dbpw, $dbHostEms, $db_name, $db_user, $db_pw) {
        $this->dbh = new PDO('mysql:host=' . $dbHostEms . ';dbname=' . $db_name, $db_user, $db_pw);
        $this->dbh_ext = new PDO('mysql:host=' . $dbHost . ';dbname=' . $dbname, $dbuser, $dbpw);
    }

    public function setAttribute($fileName, $attribute, $data) {
        $qry = 'UPDATE `' . $fileName . '`' . 
            ' SET `attribute` = :attribute' . 
            ' WHERE (' . $data . ')' . 
                ' AND `attribute` = ""'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':attribute', $attribute, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getEmailExtTable($fileName, $limit, $feldname, $extTbl, $attr) {
        $z = 0;

        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($attr == 'b') {
            $domainArr = $this->getBLdomain(
                $feldname,
                $extTbl
            );
        }

        $sql = '';
        foreach ($result as $row) {
            $sql .= ' `' . $feldname . '` = "' . $row['email'] . '" OR ';

            if ($attr == 'b') {
                $domain_name = explode('@', $row['email']);
                $domain_endung = trim($domain_name[1]);
                $domain = "*@" . $domain_endung;

                if (in_array($domain, $domainArr)) {
                    $sql2 .= ' `email` = "' . $row['email'] . '" OR ';
                    $z++;
                }
            }
        }
        $sql = substr($sql, 0, -4);

        $qry2 = 'SELECT `' . $feldname . '` as `Email`' . 
            ' FROM `' . $extTbl . '`' . 
            ' WHERE (' . $sql . ')' . 
            ' GROUP BY `Email`'
        ;
        $stmt2 = $this->dbh_ext->prepare($qry2);
        $stmt2->execute();
        $result2 = $stmt2->fetchAll();

        $sql2 = '';
        foreach ($result2 as $row2) {
            $sql2 .= ' `email` = "' . $row2['Email'] . '" OR ';
            $z++;
        }
        $sql2 = substr($sql2, 0, -4);
        
        $this->setAttribute(
            $fileName,
            $attr,
            $sql2
        );

        return $z;
    }

    public function getBLdomain($feldname, $extTbl) {
        $qry = 'SELECT `' . $feldname . '` as `RegEx`' . 
            ' FROM `' . $extTbl . '`' . 
            ' WHERE `' . $feldname . '` LIKE "*@%"'
        ;
        $stmt = $this->dbh_ext->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['RegEx'];
        }

        return $dataArr;
    }
    
    
    /*
     ***************************************************************
     * other functions (werden zurzeit nicht verwendet)
     ***************************************************************
     */
    public function getExtCnt($extTbl) {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `' . $extTbl . '`'
        ;
        $stmt = $this->dbh_ext->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }
}
?>