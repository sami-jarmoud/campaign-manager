<?php
class dataUtil {
    public static function getFeldzuordnung($spalte, $fieldUnbekannt, $alleFelderArr) {
        $select = '<select name="unbekannt' . $fieldUnbekannt . '"><option value="ignorieren">-ignorieren-</option>';
        foreach ($alleFelderArr as $feld) {
            $selColor = 'style="color:red"';
            $select .= '<option value="' . $feld . '" ';
            
            if ($spalte == $feld) {
                $select .= 'selected';
            }
            $select .= '>' . $feld . '</option>';
        }

        $select .= '</select>';

        return $select;
    }

    public static function getAnrede($str) {
        $str = strtolower(trim($str));
        if ($str == 'h' || $str == 'm' || $str == 'mr' || $str == 'mr.' || $str == '1' || $str == 'male') {
            $str = 'Herr';
        } elseif ($str == 'f' || $str == 'w' || $str == 'mrs' || $str == 'mrs.' || $str == '2' || $str == 'female') {
            $str = 'Frau';
        }

        return $str;
    }

    public static function getAnredeBez($str) {
        $str = strtolower(trim($str));
        if ($str == 'h' || $str == 'm' || $str == 'mr' || $str == 'mr.' || $str == '1' || $str == 'male') {
            $str = 'Herr';
        } elseif ($str == 'f' || $str == 'w' || $str == 'mrs' || $str == 'mrs.' || $str == '0' || $str == 'female') {
            $str = 'Frau';
        }

        return $str;
    }

    public static function getAnredeBezLand($anrede, $land) {
        if ((array_key_exists($land, CampaignAndOthersUtils::$genderCountryDataArray)) === true) {
            switch ($anrede) {
                case 'Herr':
                    $result = CampaignAndOthersUtils::$genderCountryDataArray[$land]['m'];
                    break;
                
                case 'Frau':
                    $result = CampaignAndOthersUtils::$genderCountryDataArray[$land]['w'];
                    break;
            }
        } else {
            $result = $anrede;
        }
        
        return $result;
    }

    public static function getAnredeID($str) {
        $str = strtolower(trim($str));

        if ($str == 'h' || $str == 'm' || $str == 'mr' || $str == 'mr.' || $str == '1' || $str == 'male' || $str == 'herr') {
            $str = 1;
        } elseif ($str == 'f' || $str == 'w' || $str == 'mrs' || $str == 'mrs.' || $str == '2' || $str == 'female' || $str == 'frau') {
            $str = 0;
        }

        return $str;
    }

    public static function getLand($str) {
        // TODO: dynamischer programieren evtl. eigene db tabelle
        $str = strtolower(trim($str));
        if (strpos($str, 'd') !== false) {
            $str = 'DE';
        }

        if (strpos($str, 'ö') !== false || $str == 'Österreich') {
            $str = 'AT';
        }
        
        if (strpos($str, 'ch') !== false || $str == 'schweiz') {
            $str = 'CH';
        }
        
        if (strpos($str, 'nl') !== false || $str == 'holland' || $str == 'niederlande') {
            $str = 'NL';
        }
        
        if (strpos($str, 'fr') !== false || $str == 'frankreich') {
            $str = 'FR';
        }
        
        if (strpos($str, 'be') !== false || $str == 'belgien') {
            $str = 'BE';
        }
        
        if (strpos($str, 'es') !== false || $str == 'spanien') {
            $str = 'ES';
        }
        
        if (strpos($str, 'dk') !== false || $str == 'dänemark') {
            $str = 'DK';
        }
        
        if (strpos($str, 'se') !== false || $str == 'schweden') {
            $str = 'SE';
        }
        
        if (strpos($str, 'uk') !== false || $str == 'england') {
            $str = 'UK';
        }
        
        if (strpos($str, 'no') !== false || $str == 'norwegen') {
            $str = 'NO';
        }
        
        if (strpos($str, 'fi') !== false || $str == 'finnland') {
            $str = 'FI';
        }
        
        if (strpos($str, 'it') !== false || $str == 'italien') {
            $str = 'IT';
        }
        
        return $str;
    }

    public static function zahl_format($zahl) {
        $zahl = number_format($zahl, 0, 0, '.');
        
        return $zahl;
    }

    public static function getQuote($brutto, $netto) {
        $q = 0;
        if ($brutto > 0) {
            $q = round((($netto / $brutto) * 100), 1);
        }
        
        return $q;
    }

    public static function konv_date($date) {
        if (false === strpos($date, '-')) {
            return implode('-', array_reverse(explode('.', $date)));
        }

        return implode('.', array_reverse(explode('-', $date)));
    }

    public static function konv_date2Eng($date, $conv = '') {
        $date = str_replace("'", '', $date);
        $time = '';

        if (strpos($date, ':') !== false) {
            $date2 = explode(' ', $date);
            $date = $date2[0];
            $time = $date2[1];
        }

        $date = implode('-', array_reverse(explode('.', $date)));

        if ($conv != 'date') {
            if ($time != '') {
                $date = $date . ' ' . $time;
            }
        }

        return $date;
    }

    public static function cleanName($str) {
        $str = strtolower($str);
        $str = preg_replace('/[^a-zÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿß .-]/', '', $str);
        $str = str_replace('-', ' - ', $str);
        $str = ucfirst(ucwords(trim($str)));
        $str = str_replace(' - ', '-', $str);
        
        return $str;
    }

    public static function cleanStrasse($str, $export_street_hsnr) {
        $strasseArr = array();
        
        $hsnr = null;
        $split = preg_split('/(?=[0-9])/', $str, 2);
        if (count($split) > 1) {
            $hsnr = ' ' . $split[1];
        }
        $strasse = $split[0];
        $strasse = dataUtil::cleanName($strasse);
        
        if ($export_street_hsnr) {
            $strasseArr['strasse'] = $strasse;
            $strasseArr['hsnr'] = $hsnr;
        } else {
            $strasseArr['strasse'] = $strasse . $hsnr;
        }

        return $strasseArr;
    }

    public static function cleanLand($land, $email, $plz) {
        $l = '';
        
        if ($land == '') {
            // land ist leer, dann wird versucht das land anhand von regeln zu ermitteln
            $email = strtolower($email);
            
            // TODO: http://www.teialehrbuch.de/Kostenlose-Kurse/PHP/9324-Aufspalten-einer-Zeichenkette-mit-explode.html
            $email_endung = substr($email, strlen($email) - 2);
            
            $plz = preg_replace('/[^0-9]/', '', $plz);

            // Wenn DS valider DE-DS dann DE
            if (strlen($plz) == 5 && $email_endung == 'de') {
                $l = 'DE';
            } elseif (strlen($plz) <= 4 && $email_endung == 'at') {
                // Wenn DS valider AT-DS dann AT
                $l = 'AT';
            } elseif (strlen($plz) <= 4 && $email_endung == 'ch') {
                // Wenn DS valider CH-DS dann CH
                $l = 'CH';
            }
            
            if (strlen($l) == 0) {
                // check if tld found in countriesDataArray (CampaignAndOthersUtils::$countriesDataArray)
                if ((array_key_exists(strtoupper($email_endung), CampaignAndOthersUtils::$countriesDataArray)) === true) {
                    $l = strtoupper($email_endung);
                }
                
                // check if tld exit in countriesDataArray (CampaignAndOthersUtils::$countriesDataArray)
                if ((array_key_exists(strtoupper($email_endung), CampaignAndOthersUtils::$countriesDataArray)) !== true) {
                    $l = 'DE';
                }
            }

            if (strlen($l) == 0) {
                $l = 'DE';
            }
        } else {
            $l = $land;
        }
        
        return $l;
    }

    public static function cleanPLZ($plz, $land) {
        if ($land == 'DE') {
            $plz = preg_replace('/[^0-9]/', '', $plz);
            if (strlen($plz) == 4) {
                $plz = '0' . $plz;
            }
            
            // TODO: ???
            #if (strlen($plz) <= 2) {
            #    $plz = '';
            #}
        }

        return $plz;
    }

    public static function cleanIP($ip) {
        $ip = preg_replace('/[^0-9.]/', '', $ip);
        
        return $ip;
    }
}
?>