<?php
class tmpTableManager {
    protected $silverGroupDataArray = array('silvermedia', 'mm3g', 'arena', 'maxilife', 'cpmdialog', 'silverPerformance', 'dailyTravel', 'proLeagionStandalone', 'arenaAddress', 'preissturz');
    
    
    
    
    
    public function __construct($dbHostEms, $db_name, $db_user, $db_pw) {
        $this->dbh = new PDO(
			'mysql:host=' . $dbHostEms . ';dbname=' . $db_name,
			$db_user,
			$db_pw,
			array(
				//\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			)
		);
        $this->dbh_bl = new PDO(
			'mysql:host=localhost;dbname=blacklist',
			'blackuser',
			'ati21coyu09t',
			array(
				//\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			)
		);
    }

    public function showTables($db_name) {
        $qry = 'SHOW TABLES FROM `' . $db_name . '`';
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row[0];
        }

        return $dataArr;
    }

    public function showColumns($table_name) {
        $qry = 'SHOW COLUMNS FROM `' . $table_name . '`';
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row[0];
        }

        return $dataArr;
    }

    public function dropTmpTable($fileName) {
        $qry = 'DROP TABLE IF EXISTS `' . $fileName . '`';
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function truncateTmpTable($fileName) {
        $qry = 'TRUNCATE TABLE `' . $fileName . '`';
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function createTmpTable($spalten, $fileName) {
        $qry = 'CREATE TABLE `' . $fileName . '`' . 
            ' (' . $spalten . ')' . 
            ' DEFAULT CHARSET = utf8'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function addAttributeField($fileName) {
        $qry = 'ALTER TABLE `' . $fileName . '`' . 
            ' ADD `attribute` CHAR(1) NOT NULL,' . 
            ' ADD INDEX (`attribute`),' . 
            ' ADD `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,' . 
            ' ADD PRIMARY KEY(`id`)'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function insertTmpTable($values, $fileName, $charset = '') {
        if ($charset == 'utf-8') {
            $values = \utf8_decode($values);
        }

        $qry = 'INSERT INTO `' . $fileName . '`' . 
            ' VALUES (' . $values . ')'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function insertAllTmpTable($values, $fileName, $charset = '') {
        if ($charset == 'utf-8') {
            $values = \utf8_decode($values);
        }

        $qry = 'INSERT INTO `' . $fileName . '`' . 
            ' VALUES ' . $values
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function getTmpTableData($anzahl_elemente, $fileName) {
        $qry = 'SELECT * ' . 
            ' FROM `' . $fileName . '`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $arr = array();

            for ($j = 0; $j < $anzahl_elemente; $j++) {
                //$arr[] = utf8_decode($row['unbekannt' . $j]);
				$arr[] = $row['unbekannt' . $j];
            }

            $dataArr[] = $arr;
        }

        return $dataArr;
    }

    public function getEmailTmpTableData($fileName) {
        $qry = 'SELECT *' . 
            ' FROM `' . $fileName . '`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['email'];
        }

        return $dataArr;
    }

    public function changeTmpTableFieldName($oldName, $newName, $fileName) {
        $qry = 'ALTER TABLE `' . $fileName . '`' . 
            ' CHANGE `' . $oldName . '` `' . $newName . '`' . 
            ' VARCHAR(255) NOT NULL'
        ;

        $qry2 = 'ALTER TABLE `' . $fileName . '`' . 
            ' ADD INDEX (`' . $newName . '`)'
        ;

        $stmt = $this->dbh->prepare($qry);
        $stmt2 = $this->dbh->prepare($qry2);

        if ($newName != 'ignorieren') {
            $stmt->execute();
            $stmt2->execute();
        }
    }

    public function getColumnsTmpTable($fileName) {
        $qry = 'SHOW COLUMNS FROM `' . $fileName . '`';
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['Field'];
        }

        return $dataArr;
    }

    public function getTmpTableDataNew($getColumnsTmpTableArr, $fileName, $charset) {
        $qry = 'SELECT *' . 
            ' FROM `' . $fileName . '`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $arr = array();
            foreach ($getColumnsTmpTableArr as $field) {
				if ($charset == 'utf-8') {
					$arr[$field] = \utf8_encode($row[$field]);
				} else {
					$arr[$field] = $row[$field];
				}
				
				
            }
            
            $dataArr[] = $arr;
        }

        return $dataArr;
    }

    public function setAttribute($fileName, $attribute, $data) {
        $qry = 'UPDATE `' . $fileName . '`' . 
            ' SET `attribute` = :attribute' . 
            ' WHERE (' . $data . ')' . 
                ' AND `attribute` = ""'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':attribute', $attribute, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getBLdomain() {
        $qry = 'SELECT `Email`' . 
            ' FROM `Blacklist`' . 
            ' WHERE `Email` LIKE "%*@%"'
        ;
        $stmt = $this->dbh_bl->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['Email'];
        }

        return $dataArr;
    }

    public function getFakeEmail() {
        $qry = 'SELECT `regex`' . 
            ' FROM `Fake_email`'
        ;
        $stmt = $this->dbh_bl->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['regex'];
        }

        return $dataArr;
    }

    public function cntAttribute($fileName, $attribute) {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `' . $fileName . '`' . 
            ' WHERE `attribute` = :attribute'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':attribute', $attribute, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }

    public function cntNetto($fileName) {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `' . $fileName . '`' . 
            ' WHERE `attribute` = ""'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }

    public function getBLTmpTable($fileName, $limit, $domainArr) {
        $sql = '';
        $sql2 = '';

        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $sql .= ' `Email` = "' . $row['email'] . '" OR ';

            $domain_name = explode('@', $row['email']);
            $domain_endung = trim($domain_name[1]);
            $domain = '*@' . $domain_endung;

            if (in_array($domain, $domainArr)) {
                $sql2 .= ' `email` = "' . $row['email'] . '" OR ';
            }
        }
        $sql = substr($sql, 0, -4);

        $qry2 = 'SELECT `Email`' . 
            ' FROM `Blacklist`' . 
            ' WHERE (' . $sql . ')'
        ;
        $stmt2 = $this->dbh_bl->prepare($qry2);
        $stmt2->execute();
        $result2 = $stmt2->fetchAll();

        foreach ($result2 as $row) {
            $sql2 .= ' `email` = "' . $row['Email'] . '" OR ';
        }
        $sql2 = substr($sql2, 0, -4);

        $this->setAttribute(
            $fileName,
            'b',
            $sql2
        );
    }

    public function getBouncesTmpTable($fileName, $limit) {
        $sql = '';
        $sql2 = '';

        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC ' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $sql .= ' `email` = "' . $row['email'] . '" OR ';
        }
        $sql = substr($sql, 0, -4);

        $qry2 = 'SELECT `email`' . 
            ' FROM `Hardbounce`' . 
            ' WHERE (' . $sql . ')'
        ;
        $stmt2 = $this->dbh_bl->prepare($qry2);
        $stmt2->execute();
        $result2 = $stmt2->fetchAll();

        foreach ($result2 as $row) {
            $sql2 .= ' `email` = "' . $row['email'] . '" OR ';
        }
        $sql2 = substr($sql2, 0, -4);

        $this->setAttribute(
            $fileName,
            'h',
            $sql2
        );
    }

    public function getMetaTmpTable($fileName, $limit, $felderMetaDB, $metaDB) {
        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $sql = '';
        foreach ($result as $row) {
            $sql .= ' `' . $felderMetaDB['EMAIL'] . '` = "' . $row['email'] . '" OR ';
        }
        $sql = substr($sql, 0, -4);

        $qry2 = 'SELECT `' . $felderMetaDB['EMAIL'] . '` as `Email`' . 
            ' FROM `' . $metaDB . '`' . 
            ' WHERE (' . $sql . ')' . 
            ' GROUP BY `Email`'
        ;
        $stmt2 = $this->dbh->prepare($qry2);
        $stmt2->execute();
        $result2 = $stmt2->fetchAll();

        $sql2 = '';
        foreach ($result2 as $row2) {
            $sql2 .= ' `email` = "' . $row2['Email'] . '" OR ';
        }
        $sql2 = substr($sql2, 0, -4);
        
        $this->setAttribute(
            $fileName,
            'm',
            $sql2
        );
    }

    public function getFakeEmailTmpTable($fileName, $limit, $regexArr) {
        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $sql = '';
        foreach ($result as $row) {
            $a = 0;
            foreach ($regexArr as $suchmuster) {
                if (preg_match("[" . $suchmuster . "]", $row['email'])) {
                    $a++;
                    
                    if ($a == 1) {
                        $sql .= ' `email` = "' . $row['email'] . '" OR ';
                    }
                    break;
                }
            }
        }
        $sql = substr($sql, 0, -4);

        $this->setAttribute(
            $fileName,
            'f',
            $sql
        );
    }

    public function getDublPostTmpTable($fileName, $limit, $felderMetaDB, $metaDB) {
        $qry = 'SELECT *' . 
            ' FROM `' . $fileName . '`' . 
            ' ORDER BY `id` ASC' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $sql = '';
            $sql2 = '';

            if ($row['vorname'] != '' && $row['nachname'] != '' && $row['plz'] != '' && $row['ort'] != '') {
                $sql .= ' `' . $felderMetaDB['VORNAME'] . '` = "' . $row['vorname'] . '" AND ';
                $sql .= ' `' . $felderMetaDB['NACHNAME'] . '` = "' . $row['nachname'] . '" AND ';
                $sql .= ' `' . $felderMetaDB['PLZ'] . '` = "' . $row['plz'] . '" AND ';
                $sql .= ' `' . $felderMetaDB['ORT'] . '` = "' . $row['ort'] . '" ';

                $qry2 = 'SELECT *' . 
                    ' FROM `' . $metaDB . '`' . 
                    ' WHERE ' . $sql
                ;
                $stmt2 = $this->dbh->prepare($qry2);
                $stmt2->execute();
                $result2 = $stmt2->fetchAll();

                foreach ($result2 as $row2) {
                    $sql2 = ' `email` = "' . $row['email'] . '" ';
                }

                $this->setAttribute(
                    $fileName,
                    'd',
                    $sql2
                );
            }
        }
    }

    public function getDubletteInDateiTmpTable($fileName) {
        $qry = 'SELECT `email`' . 
            ' FROM `' . $fileName . '`' . 
            ' GROUP BY `email`' . 
            ' HAVING COUNT(*) > 1'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $sql = '';
        foreach ($result as $row) {
            $sql .= ' `email` = "' . $row['email'] . '" OR ';
        }
        $sql = substr($sql, 0, -4);

        $this->setAttribute(
            $fileName,
            'i',
            $sql
        );
    }

    public function getHerkunft($fileName) {
        $qry = 'SELECT `herkunft`' . 
            ' FROM `' . $fileName . '`' . 
            ' GROUP BY `herkunft`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        foreach ($result as $row) {
            $dataArr[] = $row['herkunft'];
        }

        return $dataArr;
    }

    public function setHerkunft($fileName, $hArr, $herkunftTable, $felderHerkunftDB) {
        $qry = 'SELECT `' . $felderHerkunftDB['ID'] . '` as `hID`' . 
            ' FROM `' . $herkunftTable . '`' . 
            ' WHERE `' . $felderHerkunftDB['HERKUNFT_NAME'] . '` = :herkunft'
        ;

        $qry2 = 'UPDATE `' . $fileName . '`' . 
            ' SET `herkunft` = :hID' . 
            ' WHERE `herkunft`= :herkunft'
        ;

        $qry3 = 'INSERT INTO `' . $herkunftTable . '`' . 
            ' (`' . $felderHerkunftDB['HERKUNFT_NAME'] . '`)' . 
            ' VALUES (:herkunft)'
        ;

        $stmt = $this->dbh->prepare($qry);

        foreach ($hArr as $herkunft) {
            $hID = '';
            $stmt->bindParam(':herkunft', $herkunft, PDO::PARAM_STR);
            $stmt->execute();
            $result = $stmt->fetchAll();
            
            $resultCnt = count($result);
            if ($resultCnt > 0) {
                foreach ($result as $row) {
                    $hID = $row['hID'];
                }
            } else {
                $stmt3 = $this->dbh->prepare($qry3);
                $stmt3->bindParam(':herkunft', $herkunft, PDO::PARAM_STR);
                $stmt3->execute();
                
                $stmt = $this->dbh->prepare($qry);
                $stmt->bindParam(':herkunft', $herkunft, PDO::PARAM_STR);
                $stmt->execute();
                $result = $stmt->fetchAll();
                
                foreach ($result as $row) {
                    $hID = $row['hID'];
                }
            }

            $stmt2 = $this->dbh->prepare($qry2);
            $stmt2->bindParam(':herkunft', $herkunft, PDO::PARAM_STR);
            $stmt2->bindParam(':hID', $hID, PDO::PARAM_INT);
            $stmt2->execute();
        }
    }

    public function setImportdatei($dataArr, $import_datei_db) {
        $qry = 'INSERT INTO `' . $import_datei_db . '`' . 
            ' (`IMPORT_NR`, `IMPORT_NR2`, `PARTNER`, `IMPORT_DATEI`, `IMPORT_DATUM`, `GELIEFERT_DATUM`)' . 
            ' VALUES ( :impnr, :impnr, :pid, :datei, NOW(), :lieferdatum )'
        ;

        $qry2 = 'SELECT `IMPORT_NR` ' . 
            ' FROM `' . $import_datei_db . '`' . 
            ' ORDER BY `IMPORT_NR` DESC ' . 
            ' LIMIT 1'
        ;
        $stmt2 = $this->dbh->prepare($qry2);
        $stmt2->execute();
        $result = $stmt2->fetch();
        
        $impNr = $result['IMPORT_NR'];
        $impNr++;
        
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':impnr', $impNr, PDO::PARAM_INT);
        $stmt->bindParam(':datei', $dataArr['dateiname'], PDO::PARAM_STR);
        $stmt->bindParam(':pid', $dataArr['pid'], PDO::PARAM_INT);
        $stmt->bindParam(':lieferdatum', $dataArr['lieferdatum'], PDO::PARAM_STR);
        $stmt->execute();

        return $impNr;
    }

    public function updateImportDateiTable($import_datei_db, $importDateiStatsArr, $importNr) {
        $qry = 'UPDATE `' . $import_datei_db . '`' . 
            ' SET `BRUTTO` = :brutto,' . 
                ' `NETTO` = :netto,' . 
                ' `NETTO_EXT` = :netto,' . 
                ' `BLACKLIST` = :blacklist,' . 
                ' `DUBLETTEN` = :dubletten,' . 
                ' `FAKE` = :fakeemail,' . 
                ' `BOUNCES` = :bounces,' . 
                ' `METADB` = :meta,' . 
                ' `DUBLETTE_INDATEI` = :dblindatei' . 
            ' WHERE `IMPORT_NR` = :importNr'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':importNr', $importNr, PDO::PARAM_INT);
        $stmt->bindParam(':brutto', $importDateiStatsArr['brutto'], PDO::PARAM_INT);
        $stmt->bindParam(':netto', $importDateiStatsArr['netto'], PDO::PARAM_INT);
        $stmt->bindParam(':blacklist', $importDateiStatsArr['blacklist'], PDO::PARAM_INT);
        $stmt->bindParam(':dubletten', $importDateiStatsArr['dubletten'], PDO::PARAM_INT);
        $stmt->bindParam(':fakeemail', $importDateiStatsArr['fakeemail'], PDO::PARAM_INT);
        $stmt->bindParam(':bounces', $importDateiStatsArr['bounces'], PDO::PARAM_INT);
        $stmt->bindParam(':meta', $importDateiStatsArr['meta'], PDO::PARAM_INT);
        $stmt->bindParam(':dblindatei', $importDateiStatsArr['dublettenindatei'], PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getDataTmpTable($fileName, $fieldNamenArr, $limit) {
        $qry = 'SELECT *' . 
            ' FROM `' . $fileName . '`' . 
            ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $dataArr = array();
        $fieldValuesArr = array();
        foreach ($result as $row) {
            foreach ($fieldNamenArr as $field) {
                $fieldValuesArr["'" . $field . "'"] = $row[$field];
            }

            $dataArr[] = $fieldValuesArr;
        }

        return $dataArr;
    }

    public function setMetaData($metaDataArr, $fieldNamenArr, $felderMetaDB, $metaDB, $importNr, $charset = '') {
        $f = '';
        $fields = '';
        foreach ($fieldNamenArr as $field) {
            $f = strtoupper($field);
            
            if ($field != 'hsnr') {
                $fields .= '`' . $felderMetaDB[$f] . '`,';
            }
        }
        $fields .= '`' . $felderMetaDB['IMPORT_NR'] . '`';

        if (in_array('hsnr', $fieldNamenArr)) {
            $hsnr = 1;
        } else {
            $hsnr = 0;
        }

        $valuesAll = '';
        $metaDataCnt = count($metaDataArr);
        for ($i = 0; $i < $metaDataCnt; $i++) {
            $values = '';
            $valuesAll .= '(';
            
            foreach ($fieldNamenArr as $field) {
                if ($field == 'geburtsdatum' || $field == 'eintragsdatum' || $field == 'doi_date') {
                    $metaDataArr[$i]["'" . $field . "'"] = dataUtil::konv_date2Eng($metaDataArr[$i]["'" . $field . "'"]);
                }

                if ($field == 'anrede') {
                    $metaDataArr[$i]["'" . $field . "'"] = dataUtil::getAnredeID($metaDataArr[$i]["'" . $field . "'"]);
                }

                if ($field == 'strasse' && $hsnr == 1) {
                    $strasse = $metaDataArr[$i]["'" . $field . "'"] . ' ' . $metaDataArr[$i]["'hsnr'"];
                    $strasse = str_replace("'", "", $strasse);
                    $values .= "'" . $strasse . "',";
                } elseif ($field != 'hsnr') {
                    $values .= "'" . $metaDataArr[$i]["'" . $field . "'"] . "',";
                }
            }

            $valuesAll .= $values . "'" . $importNr . "'),";
        }
        $valuesAll = substr($valuesAll, 0, -1);

        $qry = 'INSERT INTO `' . $metaDB . '`' . 
            ' ( ' . $fields . ' )' . 
            ' VALUES ' . $valuesAll
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }

    public function getImportDateiData($import_datei_db, $impNr, $partner_db, $felderPartnerDB) {
        $qry = 'SELECT *' 
            . ' FROM `' . $import_datei_db . '` as `d`' 
                . ' INNER JOIN `' . $partner_db . '` as `p` ON `d`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`' 
            . ' WHERE `d`.`IMPORT_NR` = :impNr'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':impNr', $impNr, PDO::PARAM_INT);
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $dataArr = array(
            'Datei' => $row['IMPORT_DATEI'],
            'Firma' => $row[$felderPartnerDB['Firma']],
            'Lieferdatum' => $row['GELIEFERT_DATUM'],
            'Brutto' => $row['BRUTTO'],
            'Netto' => $row['NETTO'],
            'Blacklist' => $row['BLACKLIST'],
            'Bounces' => $row['BOUNCES'],
            'Dublette_indatei' => $row['DUBLETTE_INDATEI'],
            'Fakeemail' => $row['FAKE'],
            'Dublette_post' => $row['DUBLETTEN'],
            'meta' => $row['METADB'],
            'pid' => $row[$felderPartnerDB['PID']],
            'broadmailImport' => (isset($row['BROADMAIL_IMPORT']) ? $row['BROADMAIL_IMPORT'] : 0)
        );

        return $dataArr;
    }

    public function getExportData($metaDB, $felderMetaDB, $herkunft_db, $felderHerkunftDB, $abgleichArr, $impNr, $pid, $limit) {
        $sql = '';
        if (count($abgleichArr) > 0) {
            foreach ($abgleichArr as $a) {
                if ($a != 'm') {
                    $sql .= ' AND `m`.`' . $felderMetaDB['ATTRIBUTE'] . '` != "' . $a . '" ';
                }
            }
        }

        if (!in_array('m', $abgleichArr)) {
            $sql .= ' AND `m`.`' . $felderMetaDB['ATTRIBUTE'] . '` != "m" ';
        }

        $qry = 'SELECT *, `h`.`' . $felderHerkunftDB['HERKUNFT_NAME'] . '` as `' . $felderMetaDB['HERKUNFT'] . '`' 
            . ' FROM `' . $metaDB . '` as `m`' 
                . ' LEFT JOIN `' . $herkunft_db . '` as `h` ON `m`.`' . $felderMetaDB['HERKUNFT'] . '` = `h`.`' . $felderHerkunftDB['ID'] . '`' 
            . ' WHERE `m`.`' . $felderMetaDB['IMPORT_NR'] . '` = :impNr' 
                . $sql 
            . ' LIMIT ' . $limit
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':impNr', $impNr, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $dataArr = array();
        foreach ($result as $row) {
            $arr = array();
            foreach ($felderMetaDB as $field) {
                if ($field == $felderMetaDB['PARTNER']) {
                    $arr[$field] = $pid;
                } else {
                    #if (in_array($_SESSION['mandantIMP'], $this->getSilverGroupDataArray())) {
                    #    $arr[$field] = utf8_encode($row[$field]);
                    #} else {
                    if($field == 'HerkunftID'){
                        $arr[$field] = $row['ID'];
                    }elseif ($field == 'ID'){
                       
                           $arr[$field] = $row[0];
                        }else{
                        $arr[$field] = utf8_encode($row[$field]);
                    }
                }
            }

            $dataArr[] = $arr;
        }

        return $dataArr;
    }

    public function writeData($filehandle, $trennzeichen, $dataArr) {
        $end = -1;
        
        if ($trennzeichen == 't') {
            $feldnamen = implode("\t", $dataArr);
            $end = -2;
        } else {
            $feldnamen = implode($trennzeichen, $dataArr);
        }

        $feldnamen = $feldnamen . "\r\n";
        fwrite($filehandle, $feldnamen);
    }
    
    
    /**
     * updateImportDateiBroadmailStatById
     * 
     * @param   string  $importDateiTable
     * @param   array   $dataArray
     * @param   integer $importNr
     * 
     * @return  void
     */
    public function updateImportDateiBroadmailStatById($importDateiTable, array $dataArray, $importNr) {
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $qry = 'UPDATE `' . $importDateiTable . '`' 
            . ' SET `BRUTTO` = :brutto' 
                . ', `NETTO` = :netto' 
                . ', `NETTO_EXT` = :netto' 
                . ', `BLACKLIST` = :blacklist' 
                . ', `FAKE` = :fake' 
                . ', `METADB` = :meta' 
                . ', `DUBLETTE_INDATEI` = :intradoubletten' 
                . ', `TICKET_ID` = :ticketId' 
                . ', `BROADMAIL_IMPORT` = 1' 
            . ' WHERE `IMPORT_NR` = :importNr'
        ;
        
        $stmt = $this->dbh->prepare($qry);
        $stmt->bindParam(':importNr', $importNr, PDO::PARAM_INT);
        $stmt->bindParam(':brutto', $dataArray['brutto'], PDO::PARAM_INT);
        $stmt->bindParam(':netto', $dataArray['netto'], PDO::PARAM_INT);
        $stmt->bindParam(':blacklist', $dataArray['blacklist'], PDO::PARAM_INT);
        $stmt->bindParam(':fake', $dataArray['fake'], PDO::PARAM_INT);
        $stmt->bindParam(':meta', $dataArray['meta'], PDO::PARAM_INT);
        $stmt->bindParam(':intradoubletten', $dataArray['intradoubletten'], PDO::PARAM_INT);
        $stmt->bindParam(':ticketId', $dataArray['ticketId'], PDO::PARAM_STR);
        $stmt->execute();
    }
    
    
    
    
    
    /********************************************************************************************
     *
     *              setter and getter - functions
     *
     *******************************************************************************************/
    public function getSilverGroupDataArray() {
        return $this->silverGroupDataArray;
    }
    public function setSilverGroupDataArray($silverGroupDataArray) {
        $this->silverGroupDataArray = $silverGroupDataArray;
    }

        
    
    /*
     ***************************************************************
     * other functions (werden zurzeit nicht verwendet)
     ***************************************************************
     */
    public function getBLCnt() {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `Blacklist`'
        ;
        $stmt = $this->dbh_bl->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }
    
    public function getFakeCnt() {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `Fake_email`'
        ;
        $stmt = $this->dbh_bl->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }
    
    public function getBouncesCnt() {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `Hardbounce`'
        ;
        $stmt = $this->dbh_bl->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }
    
    public function getMetaCnt($metaDB) {
        $qry = 'SELECT COUNT(*) as `anzahl`' . 
            ' FROM `' . $metaDB . '`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['anzahl'];
    }
    
    public function createTmpTableExp($tblName, $metaDB) {
        $qry = 'CREATE TABLE `' . $tblName . '`' . 
            ' LIKE `' . $metaDB . '`'
        ;
        $stmt = $this->dbh->prepare($qry);
        $stmt->execute();
    }
}
?>