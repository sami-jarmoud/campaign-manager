<?php
class enrichmentManager {
    public function __construct($anreicherungDBArr, $dbHostEms, $db_name, $db_user, $db_pw) {
        $this->dbh = new PDO(
			'mysql:host=' . $dbHostEms . ';dbname=' . $db_name,
			$db_user,
			$db_pw,
			array(
				//\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			)
		);
        $this->dbh_anreicherung = new PDO(
			'mysql:host=' . $anreicherungDBArr['dbhost'] . ';dbname=' . $anreicherungDBArr['dbname'],
			$anreicherungDBArr['dbuser'],
			$anreicherungDBArr['dbpw'],
			array(
				//\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			)
		);
        $this->dbh_bkz_vorname = new PDO(
			'mysql:host=localhost;dbname=peppmt',
			'dbo49323410',
			'5C4i7E9v',
			array(
				//\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
			)
		);
    }

    public function enrichDataAll($dataEArr, $metaDB, $felderMetaDB, $impNr, $anreicherungDBArr) {
        global $fieldCntArr;
        
        $dataArr = array();
        $fieldCntArr = $_SESSION['anreicherungCnt'];

        $sql = '';
        $dataCnt = count($dataEArr);
        for ($i = 0; $i < $dataCnt; $i++) {
            if ($dataEArr[$i][$felderMetaDB['ATTRIBUTE']] == 'm') {
                $sql .= ' `' . $felderMetaDB['EMAIL'] . '` = "' . $dataEArr[$i][$felderMetaDB['EMAIL']] . '" OR ';
            }
        }
        $sql = substr($sql, 0, -3);

        $qry = 'SELECT *' . 
            ' FROM `' . $anreicherungDBArr['table'] . '`' . 
            ' WHERE `' . $felderMetaDB['IMPORT_NR'] . '` != :impNr' . 
                ' AND (' . $sql . ')'
        ;

        $qry_vorname = 'SELECT `Anrede`' . 
            ' FROM `Vorname`' . 
            ' WHERE `Vorname` = :vorname'
        ;

        $qry_bkz = 'SELECT `bkz`' . 
            ' FROM `orte`' . 
            ' WHERE `plz` = :plz' . 
            ' GROUP BY `plz`'
        ;

        $stmt = $this->dbh_anreicherung->prepare($qry);
        $stmt->bindParam(':impNr', $impNr, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $fieldEnrichArr = array('ANREDE', 'VORNAME', 'NACHNAME', 'STRASSE', 'PLZ', 'ORT', 'LAND', 'GEBURTSDATUM', 'TELEFON');

        foreach ($result as $row) {
            for ($i = 0; $i < $dataCnt; $i++) {
                foreach ($fieldEnrichArr as $field) {
                    if ($dataEArr[$i][$felderMetaDB['EMAIL']] == $row[$felderMetaDB['EMAIL']]) {
                        $data = trim($dataEArr[$i][$felderMetaDB[$field]]);
                        $dataEnrich = trim($row[$field]);
                        
                        if ($data == '' && $dataEnrich != '') {
                            $dataEArr[$i][$felderMetaDB[$field]] = $row[$felderMetaDB[$field]];
                            $fieldCntArr[$field]++;
                        }
                    }
                }
            }
        }

        for ($i = 0; $i < $dataCnt; $i++) {
            if ($dataEArr[$i][$felderMetaDB['ANREDE']] == '' && $dataEArr[$i][$felderMetaDB['VORNAME']] != '') {
                $stmt2 = $this->dbh_bkz_vorname->prepare($qry_vorname);
                $stmt2->bindParam(':vorname', $dataEArr[$i][$felderMetaDB['VORNAME']], PDO::PARAM_STR);
                $stmt2->execute();
                $result2 = $stmt2->fetchAll();
                
                foreach ($result2 as $row2) {
                    $dataEArr[$i][$felderMetaDB['ANREDE']] = $row2['Anrede'];
                    $fieldCntArr['ANREDE']++;
                }
            }

            if ($dataEArr[$i][$felderMetaDB['PLZ']] != '' && $dataEArr[$i][$felderMetaDB['LAND']] == 'DE') {
                $stmt3 = $this->dbh_bkz_vorname->prepare($qry_bkz);
                $stmt3->bindParam(':plz', $dataEArr[$i][$felderMetaDB['PLZ']], PDO::PARAM_INT);
                $stmt3->execute();
                $result3 = $stmt3->fetchAll();
                
                foreach ($result3 as $row3) {
                    $dataEArr[$i][$felderMetaDB['BKZ']] = $row3['bkz'];
                    $fieldCntArr['BUNDESLAND']++;
                }
            }
        }

        $_SESSION['anreicherungCnt'] = $fieldCntArr;
        
        return $dataEArr;
    }
}
?>