<?php
for ($i = $z; $i < $anzahlDS; $i++) {
    if ($i < $elements) {
        $line = trim($fcontents[$i]);
        $arr = explode($trennzeichen, $line);
        $arr = str_replace("\"", '', $arr);
        $arr = str_replace("'", '', $arr);
        $arr = str_replace('"', '', $arr);
        
        $values = implode('","', $arr);
        $values = '"' . $values . '"';
        
        $arrCnt = count($arr);

        $zeileFehler = $i + 1;

        if ($fieldNamenCnt != $arrCnt) {
            $zeileFehlerArr[] = '<b>Zeile ' . $zeileFehler . ':</b> ' . $values . '<br />';
        } else {
            $tmpTableManager->insertTmpTable(
                $values,
                $fileName
            );
        }
    }
}

$fZeile = '';
foreach ($zeileFehlerArr as $zeileF) {
    $zeileF = str_replace('&', '&amp;', $zeileF);
    $zeileF = str_replace('"', "'", $zeileF);
    
    $fZeile .= '
        <tr>
            <td>' . $zeileF . '</td>
        </tr>
    ';
}

$zeileFehlerCnt = count($zeileFehlerArr);
$z += $anzahlDS;
$anzahlDS += $anzahlDS;

if ($d > 0) {
    $pWidth = ceil(($d / $durchlaeufeCnt) * 550);
}

print '<script>$("#fehlerCnt").text("' . $zeileFehlerCnt . '");</script>';
print '<script>$("#fehlerVorschau").html("' . $fZeile . '");</script>';
print '<script>$("#progressBar").css("width","' . $pWidth . 'px");</script>';
?>