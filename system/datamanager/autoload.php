<?php
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

ini_set('memory_limit', '1024M');
ini_set('post_max_size', '320M');
ini_set('upload_max_filesize', '320M');
ini_set('max_execution_time', '120');

if (!isset($_SESSION['encoding'])) {
    header('Content-Type: text/html; charset=ISO-8859-1');
} else {
    header('Content-Type: text/html; charset=' . $_SESSION['encoding']);
}

if (isset($_SESSION['mandantIMP'])) {
    $mandant = $_SESSION['mandantIMP'];
}

if (strpos($_SERVER['SCRIPT_NAME'], '/datamanager/import.php') !== false) {
    $mandant = $_SESSION['mandant'];
    $_SESSION['mandantIMP'] = $mandant;
}

$impOption = '';

include('../limport/db_connect.inc.php');
require('../library/dataManagerWebservice.php');
require('class/tmpTableManager.php');
require('class/extTableManager.php');
require('class/dataUtil.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


// debug
$debugLogManager->logData('mandant', $mandant);

if ((int) $_SESSION['broadmailImport'] === 1 
    && (
        isset($_SESSION['importtyp']) && (int) $_SESSION['importtyp'] === 3
    )
) {
    /**
     * broadmailImport
     * 
     * abgleiche ausschalten
     */
    $abgleichTableArr = array();
}
$abgleichTableCnt = count($abgleichTableArr);

$tmpTableManager = new tmpTableManager(
    $dbHostEms,
    $db_name,
    $db_user,
    $db_pw
);

$anzahlDS = 1000;
$anzahlTmpZeilen = 25;
$progressBarWidth = 750;
$uploadFolder = 'uploads/';
?>