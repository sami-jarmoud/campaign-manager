<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

$dManager = new dataManagerWebservice();

$err = array();
$errMsg = '';
$tables = $tmpTableManager->showTables($db_name);

if (!in_array($anrede_db, $tables)) {
    $err[] = 'Tabelle <b>' . $anrede_db . '</b> fehlt!';
}

if (!in_array($herkunft_db, $tables)) {
    $err[] = 'Tabelle <b>' . $herkunft_db . '</b> fehlt!';
}

if (!in_array($import_datei_db, $tables)) {
    $err[] = 'Tabelle <b>' . $import_datei_db . '</b> fehlt!';
}

if (!in_array($partner_db, $tables)) {
    $err[] = 'Tabelle <b>' . $partner_db . '</b> fehlt!';
} else {
    $columns = $tmpTableManager->showColumns($partner_db);
    foreach ($felderPartnerDB as $c) {
        if (!in_array($c, $columns)) {
            $err[] = 'Feld <b>' . $c . '</b> fehlt in Tabelle ' . $partner_db . '!';
        }
    }
}

if (!in_array($metaDB, $tables)) {
    $err[] = 'Tabelle <b>' . $metaDB . '</b> fehlt!';
} else {
    $columns = $tmpTableManager->showColumns($metaDB);
    
    foreach ($felderMetaDB as $c) {
        if (!in_array($c, $columns)) {
           # $err[] = 'Feld <b>' . $c . '</b> fehlt in Tabelle ' . $metaDB . '!';
        }
    }
}

if ($metaDB_test) {
    if (!in_array($import_datei_db . '_test', $tables)) {
        $err[] = 'Tabelle <b>' . $import_datei_db . '_test</b> fehlt!';
    }

    if (!in_array($metaDB . '_test', $tables)) {
        $err[] = 'Tabelle <b>' . $metaDB . '_test</b> fehlt!';
    } else {
        $columns = $tmpTableManager->showColumns($metaDB . '_test');
        foreach ($felderMetaDB as $c) {
            if (!in_array($c, $columns)) {
                $err[] = 'Feld <b>' . $c . '</b> fehlt in Tabelle ' . $metaDB . '_test !';
            }
        }
    }
}

if (count($err) > 0) {
    $errMsg = implode('<br />', $err);
}

if ($abgleichTableCnt > 0) {
    $impOption .= '<tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2">Weitere Abgleichstabellen:</td></tr>';

    for ($i = 0; $i < $abgleichTableCnt; $i++) {
        $impOption .= '<tr><td><input type="checkbox" name="abgleich[]" value="' . $abgleichTableArr[$i]['id'] . '" checked /></td><td><b>' . $abgleichTableArr[$i]['info'] . '</b></td></tr>';
    }
}

$partnerOpt = '';
$lieferantArr = $dManager->getKundenDataByType(
    $partner_db,
    $felderPartnerDB['GenArt'],
    $felderPartnerDB['Firma'],
    $felderPartnerDB['PID'],
    $felderPartnerDB['Status']
);

// debug
$debugLogManager->logData('lieferantArr', $lieferantArr);
$debugLogManager->logData('felderPartnerDB', $felderPartnerDB);
#$debugLogManager->logData('SESSION', $_SESSION);

$lieferantCnt = count($lieferantArr);
if ($lieferantCnt > 0) {
    for ($i = 0; $i < $lieferantCnt; $i++) {
        $partnerOpt .= "<option value=" . $lieferantArr[$i]['id'] . ">" . $lieferantArr[$i]['firma'] . " (" . $lieferantArr[$i]['id'] . ")</option>";
    }
} else {
    $partnerOpt = "<option>Keine Lieferanten Vorhanden</option>";
}

$testimportSelection = '';
if (isset($metaDB_test)) {
    $Checked = $testChecked = '';
    
    if (isset($_SESSION['testimport']) && (int) $_SESSION['testimport'] === 1) {
        $testChecked = 'checked';
    } else {
        $Checked = 'checked';
    }

    $testimportSelection = '<input type="radio" name="importtype" value="1" ' . $Checked . '/> Import<br />';
    if ($metaDB_test) {
        $testimportSelection .= '<input type="radio" name="importtype" value="2" ' . $testChecked . '/> Testimport';
    }
    
    if ((int) $_SESSION['broadmailImport'] === 1) {
        $testimportSelection .= '<br /><input type="radio" id="broadmailImport" name="importtype" value="3" /> Broadmail-Import';
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>MaaS - IMPORT</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <link href="../javascript/jquery/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="../javascript/jquery/css/flick/jquery-ui-1.8.9.custom.css" rel="Stylesheet" />
        <link type="text/css" href="../javascript/jquery/css/style-checkbox.css" rel="Stylesheet" />	
        
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="../javascript/jquery/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/iphone-style-checkboxes.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(':radio').change(function() {
                    if (parseInt($(this).val()) === 3) {
                        $(':checkbox').each(function() {
                            $(this).attr('checked', '');
                        });
                        
                        $('#abgleich_dTR').hide('slow');
                    } else {
                        $('#abgleich_dTR').show('slow');
                        
                        $(':checkbox').each(function() {
                            $(this).attr('checked', 'checked');
                        });
                    }
                });
                
                $(':checkbox').iphoneStyle({
                    checkedLabel: 'JA',
                    uncheckedLabel: 'NEIN'
                });
                
                $('#file_upload').uploadify({
                    'uploader': '../javascript/jquery/uploadify/uploadify.swf',
                    'script': 'step2.php',
                    'scriptData': {
                        'mandant': '<?php echo \base64_encode($mandant); ?>'
                    },
                    'cancelImg': '../javascript/jquery/uploadify/cancel.png',
                    'folder': 'uploads/',
                    'fileExt': '*.csv;*.txt',
                    'fileDesc': 'CSV oder TXT',
                    'auto': false,
                    'buttonText': 'Datei suchen',
                    'sizeLimit': 310240000,
                    'multi': false,
                    'onCancel': function() {
                        $('#uplbtn').html('<span style="color:red">Dateiupload abgebrochen</span>');
                        location.href = 'cancel.php';
                    },
                    'onError': function(event, ID, fileObj, errorObj) {
						console.log(event);
						console.log(ID);
						console.log(fileObj);
						console.log(errorObj);
                        alert(errorObj.type + ' Error: ' + errorObj.info);
                    },
                    'onComplete': function(event, queueID, fileObj, response, data) {
                        p = $('#partner option:selected').val();
                        d = $('#datepicker').val();
                        e = $('#encoding option:selected').val();
                        aArr = $('.abgleich input:checkbox:checked').serialize();
                        aArr = decodeURIComponent(aArr);
                        importtyp = $('input:radio[name=importtype]:checked').val();
                        location.href = 'step2.php?importtyp='+importtyp+'&encoding='+e+'&p='+p+'&d='+d+"&"+aArr+"&f="+fileObj.name;
                    }
                });
	
                $('#datepicker').datepicker({
                    maxDate: '0'
                });
                jQuery(function($){
                    $.datepicker.regional['de'] = {
                        clearText: 'l&ouml;schen',
                        clearStatus: 'aktuelles Datum l&ouml;schen',
                        closeText: 'schliessen',
                        closeStatus: 'ohne &auml;nderungen schliessen',
                        prevText: '&#x3c;zur&uuml;ck',
                        prevStatus: 'letzten Monat zeigen',
                        nextText: 'Vor&#x3e;',
                        nextStatus: 'n&auml;chsten Monat zeigen',
                        currentText: 'heute',
                        currentStatus: '',
                        monthNames: ['Januar','Februar','M&auml;rz','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'],
                        monthNamesShort: ['Jan','Feb','M&auml;r','Apr','Mai','Jun','Jul','Aug','Sep','Okt','Nov','Dez'],
                        monthStatus: 'anderen Monat anzeigen',
                        yearStatus: 'anderes Jahr anzeigen',
                        weekHeader: 'Wo',
                        weekStatus: 'Woche des Monats',
                        dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                        dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                        dayStatus: 'Setze DD als ersten Wochentag',
                        dateStatus: 'W&auml;hle D, M d',
                        dateFormat: 'dd.mm.yy',
                        firstDay: 1, 
                        initStatus: 'W&auml;hle ein Datum',
                        isRTL: false
                    };
                    $.datepicker.setDefaults($.datepicker.regional['de']);
                });
            });
        </script>
    </head>
    <body>
        <div id="upload_text"></div>
        <div id="upload_form">
            <?php
            if ($errMsg) {
                echo "
                    <h3 style='color:red;background-color:#FFF;padding:15px;height:100%;line-height:22px;'>ABBRUCH!<hr />" . $errMsg . "</h3>
                    <span style='color:#FFFFFF'>Bitte wenden Sie sich an Ihren Administrator oder an den EMS Support.</span><br /><br />
                    <input type='button' onclick='window.close();' value='OK' />
                ";
                die;
            }
            ?>
            <form action="action.php" name="optionen" enctype="multipart/form-data" method="post">
                <table width="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#666666" style="font-size:11px">
                    <tr>
                        <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#8e8901; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT [<?php print $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Dateiauswahl</span></td>
                    </tr>
                    <tr style="background-color:#666666;border-top:1px solid #666666;">
                        <td colspan="2" height="15" style="color:#FFFFFF"><span class="bold">Dateiauswahl</span> > <span class="deactive">Feldzuordnung</span> > <span class="deactive">Kontrolltabelle</span> > <span class="deactive">Datenanalyse</span> > <span class="deactive">Import</span></td>
                    </tr>
                    <tr style="background-color:#F2F2F2;">
                        <td width="100" style="color:#8e8901" align="right" valign="top"></td>
                        <td><?php print $testimportSelection; ?></td>
                    </tr>
                    <tr style="background-color:#F2F2F2;">
                        <td width="100" style="color:#8e8901" align="right" valign="top">Dateiauswahl<br />(max. 300MB)</td>
                        <td><input id="file_upload" name="file_upload" type="file" /></td>
                    </tr>
                    <tr class="hideTR" id="lTR" style="background-color:#F2F2F2">
                        <td style="color:#8e8901" align="right">Lieferant</td>
                        <td>
                            <select name="partner" id="partner" style="width:225px">
                                <option value="">- Lieferant ausw&auml;hlen (PID) -</option>
                                <?php print $partnerOpt; ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="hideTR" id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#8e8901" align="right">Geliefert am</td>
                        <td><input type="text" id="datepicker" readonly/></td>
                    </tr>
                    <tr class="hideTR" id="dTR" style="background-color:#F2F2F2">
                        <td style="color:#8e8901" align="right">Zeichensatz</td>
                        <td><select name="encoding" id="encoding" style="width:225px">
                                <option value="ISO-8859-1">ISO-8859-1</option>
                                <option value="utf-8">UTF-8</option>
                            </select></td>
                    </tr>
                    <tr class="hideTR" id="abgleich_dTR" style="background-color:#F2F2F2">
                        <td style="color:#8e8901" align="right" valign="top">Abgleiche &amp; Checks</td>
                        <td class="abgleich">
                            <table>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="-bl-" checked/></td>
                                    <td><b class="label">Globale Blacklist</b></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="-de-" checked/></td>
                                    <td><b class="label">Dubletten (Email &hArr; MetaDB)</b></td>
                                </tr>
                                <tr>
                                    <td> <input type="checkbox" name="abgleich[]" value="-dp-" checked/></td>
                                    <td><b class="label">Dubletten (postalisch &hArr; MetaDB)</b></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="-dd-" checked/></td>
                                    <td> <b class="label">Dubletten innerhalb der Lieferung (Email)</b></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="-bo-" checked/></td>
                                    <td><b class="label">Bounces</b></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="abgleich[]" value="-fe-" checked/></td>
                                    <td><b class="label">Fake-Check (Email)</b></td>
                                </tr>
                                <?php print $impOption; ?>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color:#F2F2F2">
                        <td colspan="2" height="50"></td>
                    </tr>
                    <tr style="background-color:#666666">
                        <td height="50"></td>
                        <td valign="top">
                            <span id="uplbtn"><input name="button" type="button" onclick="return formcheck();" value="Datei hochladen" /> <input type="button" onclick="window.close();" value="abbrechen" style="margin-left:15px" /></span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <script type="text/javascript">
            var p;
            var d;
            function formcheck() {
                var error = '';
                var p = $('#partner option:selected').val();
                var d = $('#datepicker').val();
                var f = $('.fileName').text();
	
                if (f == '') {
                    error += " - Bitte eine Datei auswaehlen\n";
                }
                if (p == '') {
                    error += " - Bitte Lieferant auswaehlen\n";
                }
                if (d == '') {
                    error += " - Bitte Lieferdatum angeben";
                }

                if (error != '') { 
                    alert(error); 
                    
                    return false; 
                } else {
                    $('.hideTR').fadeOut();
                    $('#uplbtn').html('<span style="color:#FFFFFF">Datei wird hochgeladen, bitte warten...</span>');
                    $('#file_upload').uploadifyUpload();
                    
                    return true;
                }
            }
        </script>
    </body>
</html>