<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */


/**
 * showDebug
 * 
 * @param   string  $title
 * @param   array   $debugData
 * @param   boolean $showDebug
 * 
 * @return  string
 */
function showDebug($title, array $debugData, $showDebug = false) {
    if ((boolean) $showDebug === true) {
        echo nl2br($title . chr(13));
        echo '<pre>';
        print_r($debugData);
        echo '</pre>';
    }
}

// debug
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);

$showDebugData = false;
$resultData = array(
    'success' => false
);

if ((int) $_SESSION['broadmailImport'] === 1 
    && (int) $_POST['importtyp'] === 3 
    && (int) $_POST['submitted'] === 1
) {
    showDebug(
        'POST',
        $_POST,
        false
    );
    
    $mappingDataArray = array(
        'Ticket' => 'ticketId',
        'Datensätze bearbeitet' => 'brutto',
        'Datensätze hinzugefügt' => 'netto',
        #'Datensätze aktualisiert:' => '',
        'Datensätze übersprungen (fehlerhaft in Datei)' => 'fake',
        #'Datensätze übersprungen (nicht in Liste):' => '',
        'Datensätze übersprungen (Sperrliste)' => 'blacklist',
        #'Datensätze übersprungen (Abbesteller):' => '',
        #'Datensätze übersprungen (Zielgruppen):' => '',
        'Doppelte Datensätze in der Datei' => 'intradoubletten',
        'Bereits in der Datenbank vorhandene Datensätze' => 'meta',
        'Fehlerhafte Datensätze' => 'fake'
    );
    showDebug(
        'mappingDataArray',
        $mappingDataArray,
        $showDebugData
    );
    
    if (strlen(trim($_POST['broadmailImportStat'])) > 0) {
        $postDataArray = str_getcsv($_POST['broadmailImportStat'], chr(10));
        showDebug(
            'postDataArray',
            $postDataArray,
            false
        );
        
        $dataArray = array();
        foreach ($postDataArray as $item) {
            $tmpDataArray = preg_split('/[\:]/', $item);
            
            if (count($tmpDataArray) > 1) {
                if ($tmpDataArray[0] == 'Ticket') {
                    $dataArray[utf8_decode($tmpDataArray[0])] = trim($tmpDataArray[1]);
                } else {
                    $dataArray[utf8_decode($tmpDataArray[0])] = (int) trim($tmpDataArray[1]);
                }
            }
            unset($tmpDataArray);
        }
        showDebug(
            'dataArray',
            $dataArray,
            $showDebugData
        );
        
        if (count($dataArray) > 0) {
            $importStatDataArray = array();
            foreach ($dataArray as $key => $item) {
                if ((boolean) (array_key_exists($key, $mappingDataArray)) === true) {
                    if (isset($importStatDataArray[$mappingDataArray[$key]])) {
                        $importStatDataArray[$mappingDataArray[$key]] += $item;
                    } else {
                        $importStatDataArray[$mappingDataArray[$key]] = $item;
                    }
                }
            }
            showDebug(
                'importStatDataArray',
                $importStatDataArray,
                $showDebugData
            );
            
            // importDateiNr aktualisieren (Tabelle: import_datei)
            $tmpTableManager->updateImportDateiBroadmailStatById(
                $import_datei_db,
                $importStatDataArray,
                (int) $_POST['importNr']
            );
            
            $resultData['success'] = true;
            $resultData['message'] = utf8_encode('Kampagne erfolgreich aktualisiert!');
            $resultData['dataArray'] = $importStatDataArray;
        } else {
            $resultData['message'] = utf8_encode('Datenformat ist falsch!');
        }
    } else {
        $resultData['message'] = utf8_encode('Keine broadmailImportStatistik daten übergeben!');
    }
} else {
    $resultData['message'] = utf8_encode('Keine valide daten übergeben!');
}

echo json_encode($resultData);
?>