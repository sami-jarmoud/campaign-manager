<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

$dManager = new dataManagerWebservice();

$anzahlDS = 5000;
$fieldNamenArr = $_SESSION['fieldNamenArr'];
$fileName = $_SESSION['fileName'];
$anzahl_elemente = (int) $_SESSION['anzahl_elemente'];
$abgleichArr = $_SESSION['impAbgleich'];
$anzahlDatensaetze = (int) $_SESSION['impAnzahl'];
$trennzeichen = $_SESSION['trennzeichen'];
$berichtArr = $_SESSION['bericht'];
$importNr = isset($_SESSION['importNr']) ? $_SESSION['importNr'] : 0;

$importDateiStatsArr = array();

$importtyp = (int) $_SESSION['importtyp'];
if ($importtyp === 2) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

$Abgleich = '';

$pid = $_SESSION['impPid'];
$partnerArr = $dManager->getPartnerData(
    $partner_db,
    $felderPartnerDB['Firma'],
    $felderPartnerDB['PID'],
    $pid
);

// debug
if (!isset($_GET['d'])) {
    $debugLogManager->logData('partnerArr', $partnerArr);
    $debugLogManager->logData('import_datei_db', $import_datei_db);
    $debugLogManager->logData('berichtArr', $berichtArr);
    $debugLogManager->logData('abgleichArr', $abgleichArr);
    $debugLogManager->logData('anzahl_elemente', $anzahl_elemente);
    $debugLogManager->logData('anzahlDatensaetze', $anzahlDatensaetze);
    #$debugLogManager->logData('SESSION', $_SESSION);
}

if (in_array('-de-', $abgleichArr)) {
    $metaCnt = ($berichtArr['metaCnt'] - (isset($berichtArr['m']) ? $berichtArr['m'] : 0));
    $metaQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $metaCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Bereits erhaltene Adressen (MetaDB)</td>
            <td style="width:150px" id="metaCnt">' . dataUtil::zahl_format($metaCnt) . '</td>
            <td style="width:350px"><span id="metaQ">' . $metaQ . '</span>%</td>
        </tr>
    ';
} else {
    $metaCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Bereits erhaltene Adressen (MetaDB)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-bl-', $abgleichArr)) {
    $blCnt = ($berichtArr['blCnt'] - (isset($berichtArr['b']) ? $berichtArr['b'] : 0));
    $blQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $blCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Globale Blacklist</td>
            <td style="width:150px" id="blCnt">' . dataUtil::zahl_format($blCnt) . '</td>
            <td style="width:350px"><span id="blQ">' . $blQ . '</span>%</td>
        </tr>
    ';
} else {
    $blCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Globale Blacklist</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-dp-', $abgleichArr)) {
    $dpCnt = ($berichtArr['dpCnt'] - (isset($berichtArr['d']) ? $berichtArr['d'] : 0));
    $dpQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $dpCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Dubletten (postalisch)</td>
            <td style="width:150px" id="dpCnt">' . dataUtil::zahl_format($dpCnt) . '</td>
            <td style="width:350px"><span id="dpQ">' . $dpQ . '</span>%</td>
        </tr>
    ';
} else {
    $dpCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Dubletten (postalisch)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-bo-', $abgleichArr)) {
    $boCnt = ($berichtArr['boCnt'] - (isset($berichtArr['h']) ? $berichtArr['h'] : 0));
    $boQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $boCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Bounces</td>
            <td style="width:150px" id="boCnt">' . dataUtil::zahl_format($boCnt) . '</td>
            <td style="width:350px"><span id="boQ">' . $boQ . '</span>%</td>
        </tr>
    ';
} else {
    $boCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Bounces</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-fe-', $abgleichArr)) {
    $feCnt = ($berichtArr['feCnt'] - (isset($berichtArr['f']) ? $berichtArr['f'] : 0));
    $feQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $feCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Fake-Check (Email)</td>
            <td style="width:150px" id="feCnt">' . dataUtil::zahl_format($feCnt) . '</td>
            <td style="width:350px"><span id="feQ">' . $feQ . '</span>%</td>
        </tr>
    ';
} else {
    $feCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Fake-Check (Email)</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if (in_array('-dd-', $abgleichArr)) {
    $ddCnt = ($berichtArr['ddCnt'] - (isset($berichtArr['i']) ? $berichtArr['i'] : 0));
    $ddQ = dataUtil::getQuote(
        $anzahlDatensaetze,
        $ddCnt
    );
    
    $Abgleich .= '
        <tr>
            <td>Dubletten innerhalb der Lieferung</td>
            <td style="width:150px" id="ddCnt">' . dataUtil::zahl_format($ddCnt) . '</td>
            <td style="width:350px"><span id="ddQ">' . $ddQ . '</span>%</td>
        </tr>
    ';
} else {
    $ddCnt = 0;
    $Abgleich .= '
        <tr>
            <td>Dubletten innerhalb der Lieferung</td>
            <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
            <td style="width:350px"></td>
        </tr>
    ';
}

if ($abgleichTableCnt > 0) {
    for ($a = 0; $a < $abgleichTableCnt; $a++) {
        $xCnt = $berichtArr[$abgleichTableArr[$a]['id']];
        $xQ = dataUtil::getQuote(
            $anzahlDatensaetze,
            $xCnt
        );

        if (in_array($abgleichTableArr[$a]['id'], $abgleichArr)) {
            $Abgleich .= '
                <tr>
                    <td>' . $abgleichTableArr[$a]['info'] . '</td>
                    <td style="width:150px" id="' . $abgleichTableArr[$a]['id'] . 'Cnt">' . dataUtil::zahl_format($xCnt) . '</td>
                    <td style="width:350px"><span id="' . $abgleichTableArr[$a]['id'] . 'Q">' . $xQ . '</span>%</td>
                </tr>
            ';
        } else {
            $Abgleich .= '
                <tr>
                    <td>' . $abgleichTableArr[$a]['info'] . '</td>
                    <td style="width:150px;color:#CCCCCC;">deaktiviert</td>
                    <td style="width:350px"></td>
                </tr>
            ';
        }
    }
}

$skipDS = 0;
$skipDSQ = 0;
$skipDS = dataUtil::zahl_format($berichtArr['zeileFehlerCnt']);
$skipDSQ = dataUtil::getQuote(
    $anzahlDatensaetze,
    $skipDS
);

$nettoCnt = 0;
$nettoQ = 0;
$nettoCnt = dataUtil::zahl_format($berichtArr['nettoCnt']);
$nettoQ = dataUtil::getQuote(
    $anzahlDatensaetze,
    $berichtArr['nettoCnt']
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['encoding']; ?>" />
        <title>MaaS - IMPORT</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
            <tr>
                <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT  [<?php echo $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Import</span></td>
            </tr>
            <tr style="background-color:#666666;border-top:1px solid #666666;">
                <td colspan="2" height="15" style="color:#FFFFFF"><span class="deactive">Dateiauswahl</span> > <span class="deactive">Feldzuordnung</span> > <span class="deactive">Kontrolltabelle</span> > <span class="deactive">Datenanalyse</span> > <span class="bold">Import</span></td>
            </tr>
            <tr style="background-color:#F2F2F2;">
                <td colspan="2" height="15">
                    <div style="padding:5px;border:1px solid #CCCCCC;width:<?php echo $progressBarWidth; ?>px;float:left">
                        <div style="background-color:#CCCCCC;width:<?php echo $progressBarWidth; ?>px;height:2px;">
                            <div id="progressBar" style="background-color:#0066FF;width:5px;height:2px;float:left"></div>		
                        </div>
                        <div style="margin:auto;width:400px;text-align:center;margin-top:5px">
                            Fortschritt: <span id="processedDS">0</span> (<?php echo dataUtil::zahl_format($anzahlDatensaetze); ?>) | <span id="progressBarProzent">0</span>% | Laufzeit: <span id="laufzeit">0</span> min
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Datenabgleich Bericht</legend>
                        <div style="width:750px;overflow:auto;background-color:#FFFFFF;">
                            <table id="importVorschau" width="100%">
                                <tr>
                                    <td>Dateiname</td>
                                    <td colspan="2"><?php echo $_SESSION['fileNameOrig']; ?></td>
                                </tr>
                                <tr>
                                    <td>Lieferant</td>
                                    <td colspan="2"><?php echo ($_SESSION['encoding'] == 'utf-8' ? $partnerArr['Firma'] : \utf8_decode($partnerArr['Firma'])); ?></td>
                                </tr>
                                <tr>
                                    <td>Datens&auml;tze</td>
                                    <td colspan="2"><?php echo dataUtil::zahl_format($anzahlDatensaetze); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="border:0px;background-color:#F2F2F2;"></td>
                                </tr>
                                <tr>
                                    <td class="tdbg">Abgleich</td>
                                    <td class="tdbg">Gefundene Datens&auml;tze</td>
                                    <td class="tdbg">Quote</td>
                                </tr>
                                <?php echo $Abgleich; ?>
                                <tr>
                                    <td>&Uuml;bersprungene Datens&auml;tze</td>
                                    <td class="fehlerCnt"><?php echo $skipDS; ?></td>
                                    <td><span id="skipQ"><?php echo $skipDSQ; ?></span>%</td>
                                </tr>
                                <tr>
                                    <td class="tdbg">Netto</td>
                                    <td class="tdbg" id="nettoCnt"><?php echo $nettoCnt; ?></td>
                                    <td class="tdbg"><span id="nettoQ"><?php echo $nettoQ; ?></span>%</td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2; display: none;" id="broadmailFieldContent">
                <td>
                    <div id="resultInfo" style="display: none;"></div>
                    <form id="updateBroadmailImport" action="updateBroadmailImport.php" method="post" target="_blank">
                        <fieldset style="border:solid 1px #666666;padding:.5em;">
                            <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Broadmail Statistik Daten</legend>
                            <textarea name="broadmailImportStat" style="width: 95%; border:1px solid #999999; height: 300px;" class="textfeld"></textarea>
                        </fieldset>
                        <div style="text-align: right; margin: 5px 0 20px 0; width: 95%;">
                            <input name="importtyp" type="hidden" value="<?php echo $_SESSION['importtyp']; ?>"/>
                            <input name="importNr" id="importNr" type="hidden" value=""/>
                            <input name="submitted" type="hidden" value="1"/>
                            
                            <input id="submitBroadmail" type="button" value="daten aktualisieren"/>
                        </div>
                    </form>
                </td>
            </tr>
            <tr style="background-color:#666666">
                <td>
                    <span id="cancelBtn"><input type="button" value="abbrechen" /></span> <input type="button" onclick="location.href='import.php'" id="weiterBtn" value="Weitere Datei importieren" style="display:none;margin-left:60px" /> <input type="button" id="closeBtn" onclick="window.close();" value="schliessen" style="display:none;" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            <?php
                require_once('include/cancel.js');
            ?>
        </script>
        
        <?php
        $d = 1;
        $z = 0;
        $ende = $anzahlDS;
        $pWidth = 5;
        $durchlaeufeCnt = ceil($anzahlDatensaetze / $anzahlDS);

        if (!empty($_GET['d'])) {
            $d = intval($_GET['d']);
        }
        
        if (!empty($_GET['z'])) {
            $z = intval($_GET['z']);
        }
        
        if (!empty($_GET['ende'])) {
            $ende = intval($_GET['ende']);
        }

        if ((int) $d === 1) {
            $impdataArr = array();
            $unsetIdx = 0;
            foreach ($fieldNamenArr as $field) {
                if ($field == 'ignoriert') {
                    unset($fieldNamenArr[$unsetIdx]);
                }
                
                $unsetIdx++;
            }

            $fieldNamenArr[] = 'attribute';
            $_SESSION['fieldNamenArr'] = $fieldNamenArr;

            $herkunftArr = $tmpTableManager->getHerkunft($fileName);
            $tmpTableManager->setHerkunft(
                $fileName,
                $herkunftArr,
                $herkunft_db,
                $felderHerkunftDB
            );
            
            $impdataArr['pid'] = $pid;
            $impdataArr['dateiname'] = $_SESSION['fileNameOrig'];
            $impdataArr['lieferdatum'] = dataUtil::konv_date($_SESSION['impDate']);
            
            $importNr = $tmpTableManager->setImportdatei(
                $impdataArr,
                $import_datei_db
            );
            $_SESSION['importNr'] = $importNr;
        }

        if ($d <= $durchlaeufeCnt) {
            $startScript = time();

            $limit = $z . ',' . $anzahlDS;

            $metaDataArr = $tmpTableManager->getDataTmpTable(
                $fileName,
                $fieldNamenArr,
                $limit
            );
            $tmpTableManager->setMetaData(
                $metaDataArr,
                $fieldNamenArr,
                $felderMetaDB,
                $metaDB,
                $importNr,
                $_SESSION['encoding']
            );

            $endScript = time();
            $sec = ($endScript - $startScript) * ($durchlaeufeCnt - $d);
            if ($sec < 0) {
                $sec = 0;
            }
            $laufzeit = sprintf('%02d:%02d', floor($sec / 60), $sec % 60);

            $z += $anzahlDS;

            if ($d > 1) {
                $pWidth = ceil(($d / $durchlaeufeCnt) * $progressBarWidth);
                $pProzent = ceil(($d / $durchlaeufeCnt) * 100);
            }
            $d++;

            $processedCnt = dataUtil::zahl_format($z);
            if ($d > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $processedCnt = dataUtil::zahl_format($anzahlDatensaetze);
                $pWidth = $progressBarWidth;
                $pProzent = 100;
            }

            echo '
                <script type="text/javascript">
                        $("#processedDS").text("' . $processedCnt . '");
                        $("#progressBar").css("width","' . $pWidth . 'px");
                        $("#progressBarProzent").text("' . $pProzent . '");
                        $("#laufzeit").text("' . $laufzeit . '");
                </script>
            ';

            if ($d > $durchlaeufeCnt || (int) $durchlaeufeCnt === 1) {
                $importDateiStatsArr['dublettenindatei'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'i'
                );
                
                $importDateiStatsArr['fakeemail'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'f'
                );
                
                $importDateiStatsArr['bounces'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'h'
                );
                
                $importDateiStatsArr['dubletten'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'd'
                );
                
                $importDateiStatsArr['blacklist'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'b'
                );
                
                $importDateiStatsArr['meta'] = $tmpTableManager->cntAttribute(
                    $fileName,
                    'm'
                );
                
                $importDateiStatsArr['brutto'] = ($_SESSION['impAnzahl'] - $skipDS);
                $importDateiStatsArr['netto'] = $tmpTableManager->cntNetto($fileName);
                
                $tmpTableManager->dropTmpTable($fileName);
                $tmpTableManager->updateImportDateiTable(
                    $import_datei_db,
                    $importDateiStatsArr,
                    $importNr
                );

                echo '
                    <script type="text/javascript">
                        $("#progressBar").css("background-color","#A3D119");
                        $("#cancelBtn").html("<span style=\'color:#A3D119;font-weight:bold;\'><img src=\'../img/Tango/22/actions/dialog-apply.png\' style=\'margin-bottom:-6px\' /> Datenimport abgeschlossen!</span>");
                        alert("Datenimport abgeschlossen!");
                        $("#weiterBtn").fadeIn(\'slow\');
                        $("#closeBtn").fadeIn(\'slow\');
                    </script>
                ';
                
                if ((int) $_SESSION['importtyp'] === 3 
                    && (int) $_SESSION['broadmailImport'] === 1
                ) {
                    // broadmailImport
                    echo '
                        <script type="text/javascript">
                            $("#importNr").val(' . (int) $_SESSION['importNr'] . ');
                    ';
                    
                    require_once('include/processBroadmail.js');
                    
                    echo '</script>';
                }
                
                // session daten l�schen von import
                unset(
                    $_SESSION['fileName'], $_SESSION['fileNameOrig'], $_SESSION['importtyp'], $_SESSION['encoding'], $_SESSION['impPid'], $_SESSION['impDate']
                    , $_SESSION['impAbgleich'] , $_SESSION['impAnzahl'], $_SESSION['trennzeichen'], $_SESSION['anzahl_elemente'], $_SESSION['spaltenNamenArr'], $_SESSION['fieldNamenArr']
                    , $_SESSION['zeileFehlerArr'], $_SESSION['blDomain'], $_SESSION['fakeEmail'], $_SESSION['startZeit'], $_SESSION['bericht'], $_SESSION['importNr']
                );
            }

            if ($d <= $durchlaeufeCnt) {
                echo '<script>location.href = "step5.php?d=' . $d . '&z=' . $z . '";</script>';
            }
        }
        ?>
    </body>
</html>