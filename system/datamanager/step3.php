<?php
require_once('autoload.php');

/* @var $debugLogManager debugLogManager */

$spaltenNamenArr = $_SESSION['spaltenNamenArr'];
$fileName = $_SESSION['fileName'];
$anzahl_elemente = $_SESSION['anzahl_elemente'];
$td = '';
$tddata = '';
$tr_vorschau = '';
$feld_email = false;
$feld_ip = false;
$feld_herkunft = false;
$feld_eintragsdatum = false;
$hinweis = '';

$i = 0;
foreach ($spaltenNamenArr as $spalteName) {
    $oldName = 'unbekannt' . $i;
    $feldzuordnung = $_POST['unbekannt' . $i];
    
    $tmpTableManager->changeTmpTableFieldName(
        $oldName,
        $feldzuordnung,
        $fileName
    );
    $i++;
}

$getColumnsTmpTableArr = $tmpTableManager->getColumnsTmpTable($fileName);
$getColumnsTmpTableArrCnt = count($getColumnsTmpTableArr);
// debug
$debugLogManager->logData('getColumnsTmpTableArr', $getColumnsTmpTableArr);

$tmpDataArr = $tmpTableManager->getTmpTableDataNew(
    $getColumnsTmpTableArr,
    $fileName,
	$_SESSION['encoding']
);
$tmpDataCnt = count($tmpDataArr);
// debug
$debugLogManager->logData('tmpDataArr', $tmpDataArr);

$fieldNamenArr = array();
foreach ($getColumnsTmpTableArr as $field) {
    $classVorschau = 'tdbg';
    if (strpos($field, 'unbekannt') !== false) {
        $classVorschau = 'tdbgskip';
        $field = 'ignoriert';
    }

    $fieldNamenArr[] = $field;
    
    switch ($field) {
        case 'email':
            $feld_email = true;
            break;
        
        case 'ip':
        case 'doi_ip':
            $feld_ip = true;
            break;
        
        case 'herkunft':
            $feld_herkunft = true;
            break;
        
        case 'eintragsdatum':
        case 'doi_date':
            $feld_eintragsdatum = true;
            break;
    }

    $td .= '<td class="' . $classVorschau . '">' . $field . '</td>';
}

$_SESSION['fieldNamenArr'] = $fieldNamenArr;
// debug
$debugLogManager->logData('fieldNamenArr', $fieldNamenArr);
#$debugLogManager->logData('SESSION', $_SESSION);

$tr_vorschau = '<tr>' . $td . '</tr>';
for ($i = 0; $i < $tmpDataCnt; $i++) {
    if ($i > 0) {
        $tddata .= '<tr>';
        
        foreach ($getColumnsTmpTableArr as $field) {
            $feld_name = $tmpDataArr[$i][$field];
            $classVorschau = '';
            if (strpos($field, 'unbekannt') !== false) {
                $classVorschau = "tdbgskipTD";
            }

            switch ($field) {
                case 'anrede':
                    $feld_name = dataUtil::getAnrede($feld_name);
                    break;
                
                case 'land':
                    $feld_name = dataUtil::getLand($feld_name);
                    break;
                
                case 'geburtsdatum':
                case 'eintragsdatum':
                case 'doi_date':
                    $feld_name = dataUtil::konv_date2Eng($feld_name);
                    break;
            }

            $tddata .= '<td class="' . $classVorschau . '">' . $feld_name . '</td>';
        }
        
        $tddata .= '</tr>';
    }
}
$tr_vorschau .= $tddata;

if ((boolean) $feld_email !== true) {
    $hinweis = "<img src='../img/Tango/16/status/dialog-warning.png' style='margin-bottom:-3px' /> <span style='color:#F8901B'> Feld <span class='bold_warning'>EMAIL</span> fehlt!<span><br />";
}

if ((boolean) $feld_ip !== true) {
    $hinweis .= "<img src='../img/Tango/16/status/dialog-warning.png' style='margin-bottom:-3px' /> <span style='color:#F8901B'> Feld <span class='bold_warning'>IP</span> fehlt!<span><br />";
}

if ((boolean) $feld_herkunft !== true) {
    $hinweis .= "<img src='../img/Tango/16/status/dialog-warning.png' style='margin-bottom:-3px' /> <span style='color:#F8901B'> Feld <span class='bold_warning'>HERKUNFT</span> fehlt!<span><br />";
}

if ((boolean) $feld_eintragsdatum !== true) {
    $hinweis .= "<img src='../img/Tango/16/status/dialog-warning.png' style='margin-bottom:-3px' /> <span style='color:#F8901B'> Feld <span class='bold_warning'>EINTRAGSDATUM</span> fehlt!<span><br />";
}

if (strlen($hinweis) === 0) {
    $hinweis = "<img src='../img/Tango/16/actions/dialog-apply.png' style='margin-bottom:-3px' /> <span style='color:green'> Alle Pflichtfelder vorhanden (Email,IP,Herkunft,Eintragsdatum)<span><br />";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['encoding']; ?>" />
        <title>MaaS - IMPORT - Kontrolltabelle</title>
        <link rel="stylesheet" type="text/css" href="../css/dataManager.css" />
        <script type="text/javascript" src="../javascript/jquery/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="../javascript/jquery/js/jquery-ui-1.8.9.custom.min.js"></script>
    </head>
    <body>
        <table width="100%" height="100%" cellpadding="8" cellspacing="0" border="0" bgcolor="#F2F2F2" style="font-size:11px">
            <tr>
                <td height="70" colspan="2" style="background-color:#FFFFFF; font-weight:bold; font-size:16px; color:#0066FF; border-bottom:1px solid #666666;letter-spacing:0.1em" align="left">DATEI IMPORT [<?php echo $mandant; ?>]<br /><span style="color:#999999;font-size:12px;font-weight:normal;letter-spacing:normal">Kontrolltabelle</span></td>
            </tr>
            <tr style="background-color:#666666;border-top:1px solid #666666;">
                <td colspan="2" height="15" style="color:#FFFFFF"><span class="deactive">Dateiauswahl</span> > <span class="deactive">Feldzuordnung</span> > <span class="bold">Kontrolltabelle</span> > <span class="deactive">Datenanalyse</span> > <span class="deactive">Import</span></td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Kontrolltabelle</legend>
                        <div style="width:750px;height:200px;overflow:auto;background-color:#FFFFFF;">
                            <table id="importVorschau" width="100%"><?php echo $tr_vorschau; ?></table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#F2F2F2">
                <td>
                    <fieldset style="border:solid 1px #666666;padding:.5em;">
                        <legend style="color:#0066FF;font-weight:bold;font-size:12px;">Hinweise</legend>
                        <div style="width:750px;height:50px;overflow:auto;background-color:#FFFFFF;">
                            <table width="100%"><?php echo $hinweis; ?></table>
                        </div>
                    </fieldset>
                </td>
            </tr>
            <tr style="background-color:#666666">
                <td id="progress">
                    <input type="button" onclick="window.back();" value="zur&uuml;ck" style="margin-right:25px"/>
                    <input type="button" id="step4Btn" onclick="location.href='step4.php'" value="Datenanalyse starten"/>
                    <span id="cancelBtn"><input type="button" value="abbrechen" /></span>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            $('#step4Btn').click(function() {
                $('#progress').html('<span style="font-weight:bold;color:#FFFFFF;">Datenanalyse wird gestartet...</span>');
            });
            
            <?php
                require_once('include/cancel.js');
            ?>
        </script>
    </body>
</html>