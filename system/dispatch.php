<?php
\header('Content-Type: text/html; charset=utf-8');

$configsPath = $_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'Configs' . \DIRECTORY_SEPARATOR;
try {
	require_once($configsPath . 'config.php');
	
	$clientId = (int) $_SESSION['mID'];
	if ($clientId <= 0) {
		throw new \InvalidArgumentException('invalid clientId.', 1447155002);
	}
	
	$loggedUserId = (int) $_SESSION['benutzer_id'];
	if ($loggedUserId <= 0) {
		throw new \InvalidArgumentException('no valid user logged.', 1447243424);
	}
	
	if (\count($_REQUEST) === 0) {
		throw new \InvalidArgumentException('no data send!', 1446815172);
	}
	
	/**
	 * init
	 * 
	 * systemConnectionHandle
	 * systemRepository
	 * userRepository
	 */
	require_once(\DIR_Inits . 'Connections' . \DIRECTORY_SEPARATOR . 'systemConnection.php');
	/* @var $systemConnectionHandle \Packages\Core\Manager\Database\AbstractConnection */
	
	require_once(\DIR_Inits . 'Repository' . \DIRECTORY_SEPARATOR . 'systemRepository.php');
	/* @var $systemRepository \Packages\Core\Domain\Repository\SystemRepository */
	
	require_once(\DIR_Inits . 'Repository' . \DIRECTORY_SEPARATOR . 'userRepository.php');
	/* @var $userRepository \Packages\Core\Domain\Repository\UserRepository */
	
	$clientEntity = $systemRepository->findById($clientId);
	if (!($clientEntity instanceof \Packages\Core\Domain\Model\ClientEntity)) {
		throw new \DomainException('invalid clientEntity', 1447243979);
	}
	
	$loggedUserEntity = $userRepository->findById($loggedUserId);
	if (!($loggedUserEntity instanceof \Packages\Core\Domain\Model\UserEntity)) {
		throw new \DomainException('invalid userEntity', 1447244045);
	}
	
	\Packages\Core\Dispatch::checkAndInitModule(
		$clientEntity,
		$loggedUserEntity,
		$systemRepository,
		$userRepository,
		$_REQUEST
	);
} catch (\Exception $e) {
	require_once($configsPath . 'exceptionsHandling.php');
	$_SESSION['exception']= 'TRUE';
	// TODO: prod/test und dev unterscheidung
	\Packages\Core\Utility\DebugUtility::debug(
		$exceptionDataArray,
		__FILE__ . ' -> ' . __LINE__
	);
	
	die();
}