<?php
header('Content-Type: text/xml; charset=ISO-8859-1');

function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}

?>
	<table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<div class="corner">
					 <table cellpadding="0" cellspacing="0" border="0">
						<tr class="tab_bg">
							<th style="border-right:1px solid #DBB7FF;"><div><div style="padding-top:7px">Monat</div></div></th>
						</tr>
					</table>
				</div>
			</td>
			<td>
				<div class="headerRow">
					<table cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
						<tr class="tab_bg">
							<th><div><div>Anrufe<br />Eingegangen</div></div></th>
                            <th><div>Anrufe<br />ausserhalb GZ</div></th>
                            <th><div>Anrufe<br />in GZ</div></th>
							<th><div>Anrufe<br />angenommen</div></th>
							<th><div>mittl.<br />Wartezeit</div></th>
							<th><div>mittl.<br />Sprechzeit</div></th>
							<th><div>Anrufabnahme-<br />Quote</div></th>
                            <th><div></div></th>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<div class="headerColumn">
					<table cellpadding="0" cellspacing="0" border="0">
                    	<tr bgcolor="#FFFFFF"><th><div><div style="padding-top:7px">Mai</div></div></th></tr>
						<tr bgcolor="#E4E4E4"><th><div><div style="padding-top:7px">April</div></div></th></tr>
						<tr bgcolor="#FFFFFF"><th><div><div style="padding-top:7px">M�rz</div></div></th></tr>
						<tr bgcolor="#E4E4E4"><th><div><div style="padding-top:7px">Februar</div></div></th></tr>
						<tr bgcolor="#FFFFFF"><th><div><div style="padding-top:7px">Januar</div></div></th></tr>
						<tr bgcolor="#E4E4E4"><th style='border-right:0px;'><div id="fill_height"></div></th></tr>
					</table>
				</div>
				<div style="height:19px;background-color:#F2F2F2;width:100%;<!--height:18px;-->"></div>
			</td>
			<td valign="top">
				<div class="body">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr bgcolor="#FFFFFF">
							<td style="text-align:right"><div><div style="padding-top:7px">66</div></div></td>
							<td style="text-align:right"><div>0</div></td>
							<td style="text-align:right"><div>66</div></td>
							<td style="text-align:right"><div>48</div></td>
							<td style="text-align:right"><div>00:00:28</div></td>
							<td style="text-align:right"><div>00:00:52</div></td>
							<td style="text-align:right"><div>73%</div></td>
							<td><div></div></td>
						</tr>
						<tr bgcolor="#E4E4E4">
							<td style="text-align:right"><div><div style="padding-top:7px">74</div></div></td>
							<td style="text-align:right"><div>19</div></td>
							<td style="text-align:right"><div>55</div></td>
							<td style="text-align:right"><div>45</div></td>
							<td style="text-align:right"><div>00:00:31</div></td>
							<td style="text-align:right"><div>00:00:40</div></td>
							<td style="text-align:right"><div>82%</div></td>
							<td><div></div></td>
						</tr>
						<tr bgcolor="#FFFFFF">
							<td style="text-align:right"><div><div style="padding-top:7px">55</div></div></td>
							<td style="text-align:right"><div>0</div></td>
							<td style="text-align:right"><div>55</div></td>
							<td style="text-align:right"><div>44</div></td>
							<td style="text-align:right"><div>00:00:34</div></td>
							<td style="text-align:right"><div>00:01:07</div></td>
							<td style="text-align:right"><div>80%</div></td>
							<td><div></div></td>
						</tr>
						<tr bgcolor="#E4E4E4">
							<td style="text-align:right"><div><div style="padding-top:7px">47</div></div></td>
							<td style="text-align:right"><div>2</div></td>
							<td style="text-align:right"><div>45</div></td>
							<td style="text-align:right"><div>38</div></td>
							<td style="text-align:right"><div>00:00:34</div></td>
							<td style="text-align:right"><div>00:01:06</div></td>
							<td style="text-align:right"><div>84%</div></td>
							<td><div></div></td>
						</tr>
						<tr bgcolor="#FFFFFF">
							<td style="text-align:right"><div><div style="padding-top:7px">52</div></div></td>
							<td style="text-align:right"><div>0</div></td>
							<td style="text-align:right"><div>52</div></td>
							<td style="text-align:right"><div>40</div></td>
							<td style="text-align:right"><div>00:00:40</div></td>
							<td style="text-align:right"><div>00:01:28</div></td>
							<td style="text-align:right"><div>77%</div></td>
							<td><div></div></td>
						</tr>
						<tr bgcolor="#E4E4E4">
							<td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div></div></td><td style='border-right:0px;'><div id="fill_td"></div></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
<input type="hidden" name="scrollT_width" id="scrollT_width" value="" /> 
<script type="text/javascript">
var tables = new Array();
var headerRowDivs = new Array();
var headerColumnDivs = new Array();
var bodyDivs = new Array();
var widths = new Array();
var heights = new Array();
var borderHorizontals = new Array();
var borderVerticals = new Array();
var tableWidths = new Array();
var tableHeights = new Array();
var arrayCount = 0;
var paddingTop = 0;
var paddingBottom = 0;
var paddingLeft = 5;
var paddingRight = 5;

wi = document.documentElement.clientWidth ;
	wi = wi-223;
	table_wi = document.getElementById("scrollTable").offsetWidth;
    document.getElementById("scrollT_width").value = table_wi;
	wi_total = wi-table_wi-10;
	
	if (table_wi<wi) {
		document.getElementById("fill_td").style.width = wi_total+"px";
	} 
	
	hi = document.documentElement.clientHeight ;
	hi = hi-238;
	table_hi = document.getElementById("scrollTable").offsetHeight;
	hi_total = hi-table_hi+135;
	
	if (table_hi<hi) {
		document.getElementById("fill_height").style.height = hi_total+"px";
		document.getElementById("fill_td").style.height = hi_total+"px";
	} 


function ScrollTableAbsoluteSize(table, width, height)
{
	ScrollTable(table, null, null, width, height);
}

function ScrollTableRelativeSize(table, borderHorizontal, borderVertical)
{
	ScrollTable(table, borderHorizontal, borderVertical, null, null);
}

function ScrollTable(table, borderHorizontal, borderVertical, width, height)
{
	var childElement = 0;
	if (table.childNodes[0].tagName == null)
	{
		childElement = 1;
	}
	
	var cornerDiv = table.childNodes[childElement].childNodes[0].childNodes[childElement].childNodes[childElement];
	var headerRowDiv = table.childNodes[childElement].childNodes[0].childNodes[(childElement + 1) * 2 - 1].childNodes[childElement];
	var headerColumnDiv = table.childNodes[childElement].childNodes[childElement + 1].childNodes[childElement].childNodes[childElement];
	var bodyDiv = table.childNodes[childElement].childNodes[childElement + 1].childNodes[(childElement + 1) * 2 - 1].childNodes[childElement];
	
	tables[arrayCount] = table;
	headerRowDivs[arrayCount] = headerRowDiv;
	headerColumnDivs[arrayCount] = headerColumnDiv;
	bodyDivs[arrayCount] = bodyDiv;
	borderHorizontals[arrayCount] = borderHorizontal;
	borderVerticals[arrayCount] = borderVertical;
	tableWidths[arrayCount] = width;
	tableHeights[arrayCount] = height;
	ResizeCells(table, cornerDiv, headerRowDiv, headerColumnDiv, bodyDiv);	
	
	widths[arrayCount] = bodyDiv.offsetWidth;
	heights[arrayCount] = bodyDiv.offsetHeight;
	arrayCount++;
	ResizeScrollArea();
	
	bodyDiv.onscroll = SyncScroll;
	
	
	if (borderHorizontal != null)
	{
		window.onresize = ResizeScrollArea;
	}
	
}

function ResizeScrollArea()
{
	var isIE = true;
	var scrollbarWidth = 17;
	if (!document.all)
	{
		isIE = false;
		scrollbarWidth = 19;
	}
	
	for (i = 0; i < arrayCount; i++)
	{
		bodyDivs[i].style.overflow = "scroll";
		bodyDivs[i].style.overflowX = "scroll";
		bodyDivs[i].style.overflowY = "scroll";
		var diffWidth = 0;
		var diffHeight = 0;
		var scrollX = true;
		var scrollY = true;
		
		var columnWidth = headerColumnDivs[i].offsetWidth;
		if (borderHorizontals[i] != null)
		{
			var width = document.documentElement.clientWidth - borderHorizontals[i] - columnWidth;
		}
		else
		{
			var width = tableWidths[i];
		}
		
		if (width > widths[i])
		{
			width = widths[i];
			bodyDivs[i].style.overflowX = "hidden";
			scrollX = false;
		}
		
		var columnHeight = headerRowDivs[i].offsetHeight;
		if (borderVerticals[i] != null)
		{
			var height = document.documentElement.clientHeight - borderVerticals[i] - columnHeight;
		}
		else
		{
			var height = tableHeights[i];
		}
		
		if (height > heights[i])
		{
			height = heights[i];
			bodyDivs[i].style.overflowY = "hidden";
			scrollY = false;
		}

		headerRowDivs[i].style.width = width + "px";
		headerRowDivs[i].style.overflow = "hidden";
		headerColumnDivs[i].style.height = height + "px";
		headerColumnDivs[i].style.overflow = "hidden";
		bodyDivs[i].style.width = width + scrollbarWidth + "px";
		bodyDivs[i].style.height = height + scrollbarWidth + "px";

		if (!scrollX && isIE)
		{
			bodyDivs[i].style.overflowX = "hidden";
			bodyDivs[i].style.height = bodyDivs[i].offsetHeight - scrollbarWidth + "px";
		}
		if (!scrollY && isIE)
		{
			bodyDivs[i].style.overflowY = "hidden";
			bodyDivs[i].style.width = bodyDivs[i].offsetWidth - scrollbarWidth + "px";
		}
		if (!scrollX && !scrollY && !isIE)
		{
			bodyDivs[i].style.overflow = "hidden";
		}
	}
}

function ResizeCells(table, cornerDiv, headerRowDiv, headerColumnDiv, bodyDiv)
{
	var childElement = 0;
	if (table.childNodes[0].tagName == null)
	{
		childElement = 1;
	}
	
	SetWidth(
		cornerDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes[childElement],
		headerColumnDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes[0]);
		
	SetHeight(
		cornerDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes[childElement],
		headerRowDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes[childElement]);
	
	var headerRowColumns = headerRowDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes;
	var bodyColumns = bodyDiv.childNodes[childElement].childNodes[childElement].childNodes[0].childNodes;
	for (i = 0; i < headerRowColumns.length; i++)
	{
		if (headerRowColumns[i].tagName == "TD" || headerRowColumns[i].tagName == "TH")
		{
			SetWidth(
				headerRowColumns[i], 
				bodyColumns[i], 
				i == headerRowColumns.length - 1);
		}
	}
	
	var headerColumnRows = headerColumnDiv.childNodes[childElement].childNodes[childElement].childNodes;
	var bodyRows = bodyDiv.childNodes[childElement].childNodes[childElement].childNodes;
	for (i = 0; i < headerColumnRows.length; i++)
	{
		if (headerColumnRows[i].tagName == "TR")
		{
			SetHeight(
				headerColumnRows[i].childNodes[0],
				bodyRows[i].childNodes[childElement],
				i == headerColumnRows.length - 1);
		}
	}
}

function SetWidth(element1, element2, isLastColumn)
{
	var diff = paddingLeft + paddingRight;
	
	if (element1.offsetWidth < element2.offsetWidth)
	{
		element1.childNodes[0].style.width = element2.offsetWidth - diff + "px";
		element2.childNodes[0].style.width = element2.offsetWidth - diff + "px";
	}
	else
	{
		element2.childNodes[0].style.width = element1.offsetWidth - diff + "px";
		element1.childNodes[0].style.width = element1.offsetWidth - diff + "px";
	}
}

function SetHeight(element1, element2, isLastRow)
{
	var diff = paddingTop + paddingBottom;
	
	if (element1.offsetHeight < element2.offsetHeight)
	{
		element1.childNodes[0].style.height = element2.offsetHeight - diff + "px";
		element2.childNodes[0].style.height = element2.offsetHeight - diff + "px";
	}
	else
	{
		element2.childNodes[0].style.height = element1.offsetHeight - diff + "px";
		element1.childNodes[0].style.height = element1.offsetHeight - diff + "px";
	}
}

function SyncScroll()
{
	for (i = 0; i < arrayCount; i++)
	{
		headerRowDivs[i].scrollLeft = bodyDivs[i].scrollLeft;
		headerColumnDivs[i].scrollTop = bodyDivs[i].scrollTop;
	}
}

ScrollTableRelativeSize(
		document.getElementById("scrollTable"), 
		224, 
		138);

</script>