<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * @deprecated use ajax width actionMethod=client
 */



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * 
 * $debugLogManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

if ((boolean) \BaseUtils::isProdSystem()) {
	$debugLogManager->logData('prodSystem', true);
} elseif ((boolean) \BaseUtils::isTestSystem()) {
	$debugLogManager->logData('testSystem', true);
} else {
	$debugLogManager->logData('devSystem', true);
}

$actionMethod = isset($_POST['actionMethod']) ? \DataFilterUtils::filterData($_POST['actionMethod']) : '';
$debugLogManager->logData('actionMethod', $actionMethod);

$createSelectItem = isset($_POST['createSelectItem']) ? (boolean) \intval($_POST['createSelectItem']) : true;
$debugLogManager->logData('createSelectItem', $createSelectItem);

$disableNotSelectedItem = isset($_POST['disableNotSelectedItem']) ? (boolean) \intval($_POST['disableNotSelectedItem']) : false;
$debugLogManager->logData('disableNotSelectedItem', $disableNotSelectedItem);

$jsonData = null;
$addDefaultItem = false;

$actionPath = __DIR__ . \DIRECTORY_SEPARATOR . 'Actions/Client/';

try {
	if (\strlen($actionMethod) === 0) {
		throw new \BadMethodCallException('No action given!', 1424768295);
	} else {
		if ((\file_exists($actionPath . $actionMethod . '.php')) === true) {
			/**
			 * init
			 * 
			 * $clientManager
			 */
			require_once(DIR_configsInit . 'initClientManager.php');
			/* @var $clientManager \ClientManager */
			
			
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once($actionPath . $actionMethod . '.php');
			
			// debug
			$debugLogManager->endGroup();
		} else {
			throw new \BadMethodCallException('Unknown action: ' . $actionMethod, 1424768303);
		}
	}
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');
	
	$jsonData['success'] = false;

	$result = array(
		'content' => $exceptionMessage,
		'jsonData' => $jsonData
	);
	
	/**
	 * sendDebugData
	 */
	\DebugAndExceptionUtils::sendDebugData(
		$e,
		__FILE__ . ' -> ' . $actionMethod
	);
}

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());

echo \json_encode($result);