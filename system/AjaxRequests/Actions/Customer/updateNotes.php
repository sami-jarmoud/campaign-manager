<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
$debugLogManager->logData('customerId', $customerId);

if ($customerId > 0) {
	$dataArray = array(
		'notes' => array(
			'value' => \FormatUtils::cleanUpNotes(\DataFilterUtils::filterData(
				$_POST['customer']['notes'],
				\DataFilterUtils::$validateFilder['special_chars']
			)),
			'dataType' => \PDO::PARAM_STR
		),
	);
	
	$campaignManager->updateCustomerDataById(
		$customerId,
		$dataArray
	);
	$resultData = array(
		'success' => TRUE,
		'message' => ''
	);
} else {
	$jsonData['message'] = 'invalid customerId: ' . $customerId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766193);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);