<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$campaignId = isset($_POST['campaign']['kid']) ? \intval($_POST['campaign']['kid']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

$campaignStatus = isset($_POST['campaign']['status']) ? \intval($_POST['campaign']['status']) : 0;
$debugLogManager->logData('campaignStatus', $campaignStatus);

if ($campaignId > 0) {
	if ($campaignStatus === 0) {
		throw new \InvalidArgumentException('invalid campaignStatus', 1424766272);
	}
	
	/**
	 * updateCampaignDataRowByKid
	 * 
	 * debug
	 */
	$resultData = $campaignManager->updateCampaignAndAddLogItem(
		$campaignId,
		$campaignId,
		$_SESSION['benutzer_id'],
		20,
		$campaignStatus,
		array(
			'status' => array(
				'value' => (int) $campaignStatus,
				'dataType' => \PDO::PARAM_INT
			)
		)
	);
	$debugLogManager->logData('resultData', $resultData);
} else {
	$jsonData['message'] = 'invalid campaignId: ' . $campaignId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766235);
}

unset($_SESSION['campaign']['reporting'][$campaignId]);

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);