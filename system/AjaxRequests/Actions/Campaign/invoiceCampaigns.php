<?php
/* @var $campaignManager \CampaignManager */
/* @var $swiftMailerWebservice \SwiftMailerWebservice */
/* @var $debugLogManager \debugLogManager */

$requireFile = DIR_ModuleClientSettings . 'Campaigns' . \DIRECTORY_SEPARATOR . 'Invoice' . \DIRECTORY_SEPARATOR . 'Resources' 
	. \DIRECTORY_SEPARATOR . \RegistryUtils::get('clientEntity')->getAbkz() . \DIRECTORY_SEPARATOR . 'InvoiceClient.php'
;
if ((\file_exists($requireFile)) === true) {
	require_once($requireFile);
	
	$invoice = new \InvoiceClient();
} else {
	require_once(DIR_ModuleClientSettings . 'Campaigns' . \DIRECTORY_SEPARATOR . 'Invoice' . \DIRECTORY_SEPARATOR . 'Resources' . \DIRECTORY_SEPARATOR . 'DefaultClient.php');
	
	$invoice = new \DefaultClient();
}

$invoice->setCampaignManager($campaignManager);
$invoice->setSwiftMailerWebservice($swiftMailerWebservice);
$invoice->setDebugLogManager($debugLogManager);
$jsonData = $invoice->main(
	$_POST['invoice'],
	'docx'
);

$result = array(
	'content' => $jsonData,
	'jsonData' => $jsonData
);