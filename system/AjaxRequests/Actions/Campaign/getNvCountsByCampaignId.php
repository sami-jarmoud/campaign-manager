<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


$campaignId = isset($_POST['campaignId']) ? \intval($_POST['campaignId']) : 0;
$debugLogManager->logData('campaignId', $campaignId);

if ($campaignId > 0) {
	/**
	 * getNvCampaignsCountByKid
	 * 
	 * debug
	 */
	$resultData = $campaignManager->getNvCampaignsCountByKid($campaignId);
	$debugLogManager->logData('resultData', $resultData);
} else {
	$jsonData['message'] = 'invalid campaignId: ' . $campaignId;
	
	throw new \InvalidArgumentException($jsonData['message'], 1424766235);
}

$result = array(
	'content' => $resultData,
	'jsonData' => $resultData
);