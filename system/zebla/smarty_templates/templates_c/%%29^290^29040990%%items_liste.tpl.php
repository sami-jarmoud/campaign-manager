<?php /* Smarty version 2.6.26, created on 2010-03-10 15:09:43
         compiled from core/items_liste.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'core/items_liste.tpl', 22, false),)), $this); ?>
<table cellpadding="0" cellspacing="0" class="table">
	<thead>
    	<tr>
            <th width="220">Email</th>
            <th width="110">Datum</th>
            <th width="60">Mandant</th>
            <th width="200">Grund</th>
            <th width="50">Bearbeiter</th>
        </tr>
    </thead>
    
    <tbody>
    	<?php $_from = $this->_tpl_vars['blacklistItemsArr']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['blacklistItemsArr'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['blacklistItemsArr']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
        $this->_foreach['blacklistItemsArr']['iteration']++;
?>
    		<?php if ($this->_foreach['blacklistItemsArr']['iteration'] % 2 == 1): ?>
				<?php $this->assign('style', 'odd'); ?>
			<?php else: ?>
				<?php $this->assign('style', 'even'); ?>
			<?php endif; ?>
			
			<tr class="<?php echo $this->_tpl_vars['style']; ?>
">
                <td class="table_trenner"><?php echo $this->_tpl_vars['item']->getEmail(); ?>
</td>
                <td class="table_trenner"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->getDatum())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y %H:%M:%S") : smarty_modifier_date_format($_tmp, "%d.%m.%Y %H:%M:%S")); ?>
</td>
                <td class="table_trenner"><?php echo $this->_tpl_vars['item']->getFirma(); ?>
</td>
                <td class="table_trenner" align="right">
                	<span><?php echo $this->_tpl_vars['item']->getGrund(); ?>
</span>
					<?php if ($this->_tpl_vars['item']->getNotizen() != ''): ?>
						<img src="imgs/info.gif" alt="Notizen vorhanden" title="<?php echo $this->_tpl_vars['item']->getNotizen(); ?>
" />
					<?php endif; ?>
				</td>
                <td><?php echo $this->_tpl_vars['item']->getBearbeiter(); ?>
</td>
            </tr>
        <?php endforeach; endif; unset($_from); ?>
    </tbody>
</table>