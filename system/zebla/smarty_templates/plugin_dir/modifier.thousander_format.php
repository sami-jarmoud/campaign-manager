<?php

/**
 * @param $number float
 * @param $decimals integer
 * @return string
 */
function smarty_modifier_thousander_format($number, $decimals = 0)
{
    return MathUtil::numberFormat($number, $decimals);
}
?>
