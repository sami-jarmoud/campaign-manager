<table cellpadding="0" cellspacing="0" class="table">
	<thead>
    	<tr>
            <th width="220">Email</th>
            <th width="110">Datum</th>
            <th width="60">Mandant</th>
            <th width="200">Grund</th>
            <th width="50">Bearbeiter</th>
        </tr>
    </thead>
    
    <tbody>
    	{foreach name=blacklistItemsArr from=$blacklistItemsArr item=item key=key}
    		{if $smarty.foreach.blacklistItemsArr.iteration % 2 == 1}
				{assign var="style" value="odd"}
			{else}
				{assign var="style" value="even"}
			{/if}
			
			<tr class="{$style}">
                <td class="table_trenner">{$item->getEmail()}</td>
                <td class="table_trenner">{$item->getDatum()|date_format:"%d.%m.%Y %H:%M:%S"}</td>
                <td class="table_trenner">{$item->getFirma()}</td>
                <td class="table_trenner" align="right">
                	<span>{$item->getGrund()}</span>
					{if $item->getNotizen() != ''}
						<img src="imgs/info.gif" alt="Notizen vorhanden" title="{$item->getNotizen()}" />
					{/if}
				</td>
                <td>{$item->getBearbeiter()}</td>
            </tr>
        {/foreach}
    </tbody>
</table>