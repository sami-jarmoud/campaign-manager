<ul class="ui-tabs-nav">
	<li><a href="#blacklist_start">Blacklist Eintrag hinzufügen:</a></li>
</ul>

<div id="blacklist_start">
	<div id="blacklist_item">
		<form id="blacklist" name="blacklist" method="post" action="" onsubmit="return formCheckBlacklist('', 50);">
			<fieldset class="edit">
				<label for="email" style="text-align:right;"><span>Email:</span> <img src="imgs/info.gif" alt="email info" title="Um eine gesamte Domain auf die Blacklist zu setzen, geben Sie diese bitte folgendermaße ein: - *@domain.de" /></label>
				<input type="text" name="blacklist[email]" id="email" style="float:left;" />
				<textarea name="blacklist[multi_email]" id="multi_email" cols="100" rows="20" style="display: none; float: left;" onkeyup="formCheckBlacklist(this, '50')" onkeydown="formCheckBlacklist(this, '50')" onchange="formCheckBlacklist(this, '50')" onfocus="formCheckBlacklist(this, '50')" ></textarea>
				<a href="#" onclick="auswahlBlacklist('email', 'multi_email'); return false;" style="font-size: 11px; color: #3A83B6; font-weight: bold; margin: 0 0 0 5px;" > + Mehrere Emails</a><br class="clear-float" />

				<label for="mandant">Mandant:</label>
				{if $isAdmin}
					<select name="blacklist[mandant]" id="mandant" onchange="formCheckBlacklist(this);" onkeydown="formCheckBlacklist(this);"  onkeyup="formCheckBlacklist(this);" >
						<option value="---" >Bitte auswählen</option>
						{foreach name=mandantenArrListe from=$mandantenArrListe item=mandantenItem}
							<option value="{$mandantenItem|lower}" {if $mandant == $mandantenItem|lower}selected="selected" {/if} >{$mandantenItem|lower}</option>
						{/foreach}
					</select>
				{else}
					<span>{$mandant}</span> <input type="hidden" name="blacklist[mandant]"  value="{$mandant}" />
				{/if}
				<br class="clear-float" />
                
				<label for="grund">Grund:</label>
				<select name="blacklist[grund]" id="grund" onchange="formCheckBlacklist(this);" onkeydown="formCheckBlacklist(this);"  onkeyup="formCheckBlacklist(this);" >
					<option value="---">Bitte auswählen</option>
					{foreach name=grundArrListe from=$grundArrListe item=grundItem}
						<option value="{$grundItem->getGrund()}" >{$grundItem->getGrund()}</option>
					{/foreach}
					<option value="Telefonische Beschwerde">Telefonische Beschwerde</option>
					<option value="manuell">-&gt; Sonstiges</option>
    			</select>
				<div style="display:none; visibility: hidden; margin: 10px 0 0 75px;" id="grund_optional">
					<b>Sonstiges:</b><br />
					<input type="text" name="blacklist[grund_optional]" value="" onchange="formCheckBlacklist(this);" />
				</div><br class="clear-float" />
    		        
				<label for="notizen">Notizen:</label><textarea name="blacklist[notizen]" id="notizen" cols="40" rows="3"></textarea>
			</fieldset>

			<fieldset class="edit">
				<label>&nbsp;</label>
				<input type="hidden" name="mitarbeiter"  value="{$mitarbeiter}" />
				<input type="hidden" name="action"  value="submit_blacklist" />

				<input type="submit" name="submit_blacklist" id="submit_blacklist" style="display:none;" value="JETZT EINTRAGEN!" disabled="disabled" />
			</fieldset>
		</form>
		<div id="error_blacklist">  </div>
	</div>
	
	{* download box *}
	{include file='core/download_box.tpl'}
</div>