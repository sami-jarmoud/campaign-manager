{include file='core/header.tpl'}

<div id="start">
	<h1>ZEBLA - Zentrales Blacklist und Abmelde-Tool</h1>
	<div id="content">
		<div id="content_left">
			<h2>Herzlich Willkommen {$mandant}!</h2>
			<!-- such formular -->
			{include file='core/search_form.tpl'}
			<!-- such formular - ende -->
		</div>
		
		<!-- mandanten logo -->
		<img src="imgs/{$mandant}.gif" id="mandanten_logo" border="0" alt="{$mandant}" />
		
		<div id="trennlinie">&nbsp;</div>
		
		<div id="inhalt">
			{* zeige den action eintrag *}
			{$action_content}
		</div>
		
		<div id="letzten_eintraegen">
			<h3>Die letzten 10 Einträge:</h3>
			{include file='core/items_liste.tpl'}
		</div>
	</div>
</div>

{include file='core/footer.tpl'}