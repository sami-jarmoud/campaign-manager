<?php
class Database {
    private static $dbUser = 'blackuser';
    private static $dbPassword = 'ati21coyu09t';
    private static $dbName = 'blacklist';
    private static $dbHost = 'localhost';
    private static $dbPort = '3306';
    private static $pdoOption = array(
    PDO::ATTR_ERRMODE => PDO :: ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
    PDO::ATTR_CASE => PDO::CASE_LOWER,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_PERSISTENT => false
    );
    private static $dbHandle = null;


    private function __construct(){}
    private function __clone() {}



    /*
     * pdo connected
     */
    public function getPdo() {
        if(self::$dbHandle === null) {
            $dsn = sprintf('mysql:dbname=%s;host=%s;port=%s', self::$dbName, self::$dbHost, self::$dbPort);

            try {
                self::$dbHandle = new PDO($dsn, self::$dbUser, self::$dbPassword, self::$pdoOption);
            } catch (PDOException $e) {
                print "Connection failed: ".$e->getMessage()."\n";

                die();
            }
        }

        return self::$dbHandle;
    }



    public function __deconstruct() {
        self::$dbHandle = null;
    }
}
?>