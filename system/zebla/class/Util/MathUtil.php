<?php
final class MathUtil {
    private function __construct() {}
    private function __clone() {}

    const GERMANY_DECIMAL_SEPERATOR = ',';
    const GERMANY_THOUSANDS_SEPERATOR = '.';

    /**
     * @param $number number
     * @param $decimals integer
     * @return string
     */
    public static function numberFormat($number, $decimals){
        return number_format($number, $decimals, self::GERMANY_DECIMAL_SEPERATOR, self::GERMANY_THOUSANDS_SEPERATOR);
    }


    /**
     * @param $string1
     * @param $string2
     * @return string
     */
    public static function getProzent($string1, $string2) {
        $value = 0;

        if (intval($string2) > 0) {
            $value =  strval(round(intval($string1) / (intval($string2) / 100), 2));
        }

        return $value . ' %';
    }
}
?>