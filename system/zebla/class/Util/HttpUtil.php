<?php
final class HttpUtil {
    const MIME_TYPE_ZIP = "application/zip";
    const MIME_TYPE_CSV = "text/csv";
    const MIME_TYPE_TXT = "text/plain";
    const MIME_TYPE_OCTET_STREAM = "application/octet-stream";

    private function __clone() {}
    private function __construct() {}

    public static function setDownloadHeader($downloadFileName = null, $contentType = null, $filePath = null){
        if($contentType == null){
            header('Content-Type: ' . self::MIME_TYPE_OCTET_STREAM);
        } else {
            header('Content-Type: ' . $contentType);
        }

        if($filePath != null){
            header('Content-Length: ' . filesize($filePath));
        }

        if($downloadFileName == null){
            throw new SigadException('no filename given');
        }

        header('Content-Disposition: attachment; filename="' . $downloadFileName . '"');
        header('Pragma: must-revalidate');
        header('Expires: 0');
    }
}
?>