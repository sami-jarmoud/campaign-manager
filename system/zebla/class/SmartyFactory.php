<?php
require_once('externe_libs/smarty/Smarty.class.php');

class SmartyFactory extends Smarty {
    public $smarty;
    private $template_name;

    public function __construct() {
        $this->smarty = new Smarty();

        // config dirs
        $this->smarty->template_dir = 'smarty_templates/templates/';
        $this->smarty->compile_dir = 'smarty_templates/templates_c/';
        $this->smarty->config_dir = 'smarty_templates/configs/';
        $this->smarty->cache_dir = 'smarty_templates/cache/';

        // eigenes plugin dir
        $this->smarty->plugins_dir[] = 'smarty_templates/plugin_dir/';

        // caching steuerung
        $this->smarty->caching = false;
        $this->smarty->debugging = false;
        $this->smarty->compile_check = true;
    }


    /*
     * Template Name festlegen
     */
    public function setTemplateName($template_name) {
        $this->template_name = $template_name;
    }
    public function getTemplateName() {
        return $this->template_name;
    }


    public function assign($varName, $value) {
        $this->smarty->assign($varName, $value);
    }


    /*
     * smarty template in variable speichern
     */
    public function display() {
        return $this->smarty->fetch($this->template_name . '.tpl');
    }
}
?>