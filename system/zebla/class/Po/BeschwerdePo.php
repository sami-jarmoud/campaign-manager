<?php
class BeschwerdePo {
    private $id;
    private $anzahl;
    private $grund;


    function initFromPdo($row) {
        $this->id = $row['id'];
        $this->anzahl = $row['anzahl'];
        $this->grund = $row['grund'];
    }



    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getAnzahl() {
        return $this->anzahl;
    }
    public function setAnzahl($anzahl) {
        $this->anzahl = $anzahl;
    }

    public function getGrund() {
        return $this->grund;
    }
    public function setGrund($grund) {
        $this->grund = $grund;
    }
}
?>