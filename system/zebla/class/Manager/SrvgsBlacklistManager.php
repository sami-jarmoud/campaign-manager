<?php
class SrvgsBlacklistManager {
    /**
     * @param $emailItem
     * @param $grund
     * @param $mandant
     * @param $mitarbeiter
     * @param $notizen
     */
    public static function insertBlacklistItems($emailItem, $grund, $mandant, $mitarbeiter, $notizen) {
        $sql  = "INSERT INTO `Blacklist`";
        $sql .= " (";
        $sql .= "   `email`";
        $sql .= "   ,`datum`";
        $sql .= "   ,`grund`";
        $sql .= "   ,`firma`";
        $sql .= "   ,`bearbeiter`";
        $sql .= "   ,`notizen`";
        $sql .= " )";
        $sql .= " VALUES (";
        $sql .= "   :email";
        $sql .= "   ,NOW()";
        $sql .= "   ,:grund";
        $sql .= "   ,:mandant";
        $sql .= "   ,:mitarbeiter";
        $sql .= "   ,:notizen";
        $sql .= " )";
        $sql .= ";";

        try {
            $sth = SrvgsDatabase::getPdo()->prepare($sql);

            if(CheckEmailUtil::CheckEmail($emailItem)) {
                $sth->bindParam(':email', $emailItem, PDO::PARAM_STR);
                $sth->bindParam(':grund', $grund, PDO::PARAM_STR);
                $sth->bindParam(':mandant', $mandant, PDO::PARAM_STR);
                $sth->bindParam(':mitarbeiter', $mitarbeiter, PDO::PARAM_STR);
                $sth->bindParam(':notizen', $notizen, PDO::PARAM_STR);
                $sth->execute();
            }
        } catch (PDOException $e) {
            // duplicate entry code ignorieren
            if($e->errorInfo[1] != '1062') {
                $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
                $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

                echo $error_txt;
                die;
            }
        }
    }
}
?>