<?php
class BlacklistManager {
    /**
     * @return count
     */
    public static function countBlacklistItem() {
        $sql  = "SELECT COUNT(1) AS `anzahl`";
        $sql .= " FROM `Blacklist`";
        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);
            $sth->execute();

            return $sth->fetchColumn();
        } catch (PDOException $e) {
            $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
            $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

            echo $error_txt;
            die;
        }
    }


    /**
     * @param $orderBy
     * @param $limit
     * @return array $returnArr
     */
    public static function selectBlacklistItems($orderBy = '`id` DESC', $limit = null) {
        $returnArr = array();

        $sql  = "SELECT ";
        $sql .= "   `id`";
        $sql .= "   ,`email`";
        $sql .= "   ,`datum`";
        $sql .= "   ,`grund`";
        $sql .= "   ,`firma`";
        $sql .= "   ,`bearbeiter`";
        $sql .= "   ,`notizen`";
        $sql .= " FROM `Blacklist`";
        $sql .= " ORDER BY " . $orderBy;

        if(!is_null($limit)) {
            $sql .= " LIMIT 0," . $limit;
        }

        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);
            $sth->execute();

            foreach($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
                $tmpObj = new BlacklistPo();
                $tmpObj->initFromPdo($row);
                $returnArr[$tmpObj->getId()] = $tmpObj;
            }

            return $returnArr;
        } catch (PDOException $e) {
            $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
            $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

            echo $error_txt;
            die;
        }
    }



    /**
     * @param $email
     * @param $searchOption
     * @return array $returnArr
     */
    public static function searchBlacklistEmail($email, $searchOption) {
        $returnArr = array();

        // email extrahieren (z.b.: xxx@xyz.de => *@xyz.de)
        $emailDomain = preg_replace('/.*@/', '*@', $email);

        if($searchOption == 'einfache_suche') {
            $where  = "`email` = :email OR `email` = :emailDomain";
        } else {
            $where  = "`email` LIKE :email";
        }

        $sql  = "SELECT ";
        $sql .= "   `id`";
        $sql .= "   ,`email`";
        $sql .= "   ,`datum`";
        $sql .= "   ,`grund`";
        $sql .= "   ,`firma`";
        $sql .= "   ,`bearbeiter`";
        $sql .= "   ,`notizen`";
        $sql .= " FROM `Blacklist`";
        $sql .= " WHERE " . $where;
        $sql .= " ORDER BY `id` DESC";
        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);

            if($searchOption == 'einfache_suche') {
                $sth->bindParam(':emailDomain', $emailDomain, PDO::PARAM_STR);
                $sth->bindParam(':email', $email, PDO::PARAM_STR);
            } else {
                // erweiterte suche (like %variable%)
                $email = '%' . $email .'%';
                $sth->bindParam(':email', $email, PDO::PARAM_STR);
            }
            $sth->execute();

            foreach($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
                $tmpObj = new BlacklistPo();
                $tmpObj->initFromPdo($row);
                $returnArr[$tmpObj->getId()] = $tmpObj;
            }

            return $returnArr;
        } catch (PDOException $e) {
            $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
            $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

            echo $error_txt;
            die;
        }
    }


    /**
     * vorauswahl der m�glichen mandanten (gilt nur f�r intone, magolino und pepperos)
     *
     * @return array $returnArr
     */
    public static function selectMandantenListe() {
        $returnArr = array();

        $sql  = "SHOW COLUMNS";
        $sql .= " FROM `Blacklist`";
        $sql .= " LIKE 'Firma'";
        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);
            $sth->execute();

            $tmpRow = $sth->fetch(PDO::FETCH_ASSOC);
            $tmpArr = preg_replace(array("/'(.*)'/U", "/^enum\((.*)\)/i"), '$1', $tmpRow['type']);
            $returnArr = explode(',', $tmpArr);

            return $returnArr;
        } catch (PDOException $e) {
            $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
            $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

            echo $error_txt;
            die;
        }
    }


    /**
     * @return array $returnArr
     */
    public static function selectTop10Beschwerden() {
        $returnArr = array();

        $sql  = "SELECT ";
        $sql .= "   `id`";
        $sql .= "   ,COUNT(1) AS `anzahl`";
        $sql .= "   ,`grund`";
        $sql .= " FROM `Blacklist`";
        $sql .= " WHERE `grund` != 'BC-Import'";
        $sql .= "   AND `grund` != 'Magpartner-Import'";
        $sql .= "   AND `grund` != 'pren'";
        $sql .= "   AND `grund` != 'Partnerimport'";
        $sql .= "   AND `grund` != 'import'";
        $sql .= "   AND `grund` != 'Altliste'";
        $sql .= "   AND `grund` != 'Blacklist'";
        $sql .= "   AND `grund` != ''";
        $sql .= " GROUP BY `grund`";
        $sql .= " ORDER BY `anzahl` DESC";
        $sql .= " LIMIT 0,10";
        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);
            $sth->execute();

            foreach($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
                $tmpObj = new BeschwerdePo();
                $tmpObj->initFromPdo($row);
                $returnArr[$tmpObj->getId()] = $tmpObj;
            }

            return $returnArr;
        } catch (PDOException $e) {
            $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
            $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

            echo $error_txt;
            die;
        }
    }



    /**
     * @param $emailItem
     * @param $grund
     * @param $mandant
     * @param $mitarbeiter
     * @param $notizen
     * @return array $returnArr
     */
    public static function insertBlacklistItems($emailItem, $grund, $mandant, $mitarbeiter, $notizen) {
        $resultArr = array();

        $sql  = "INSERT INTO `Blacklist`";
        $sql .= " (";
        $sql .= "   `email`";
        $sql .= "   ,`datum`";
        $sql .= "   ,`grund`";
        $sql .= "   ,`firma`";
        $sql .= "   ,`bearbeiter`";
        $sql .= "   ,`notizen`";
        $sql .= " )";
        $sql .= " VALUES (";
        $sql .= "   :email";
        $sql .= "   ,NOW()";
        $sql .= "   ,:grund";
        $sql .= "   ,:mandant";
        $sql .= "   ,:mitarbeiter";
        $sql .= "   ,:notizen";
        $sql .= " )";
        $sql .= ";";

        try {
            $sth = Database::getPdo()->prepare($sql);

            if(CheckEmailUtil::CheckEmail($emailItem)) {
                // blacklist eintrag
                $sth->bindParam(':email', $emailItem, PDO::PARAM_STR);
                $sth->bindParam(':grund', $grund, PDO::PARAM_STR);
                $sth->bindParam(':mandant', $mandant, PDO::PARAM_STR);
                $sth->bindParam(':mitarbeiter', $mitarbeiter, PDO::PARAM_STR);
                $sth->bindParam(':notizen', $notizen, PDO::PARAM_STR);
                $sth->execute();

                $resultArr['ok'] = $emailItem;
            } else {
                // falsche email in array speichern
                $resultArr['falsch'] = $emailItem;
            }
        } catch (PDOException $e) {
            if($e->errorInfo[1] == '1062') {
                // duplicate entry
                $resultArr['blacklist'] = $emailItem;
            } else {
                // andere fehlermeldung
                $error_txt  = '<b>SQL - Befehl:</b> ' . $sql . "\n";
                $error_txt .= '<b>Syntax Error:</b> ' . $e->getMessage();

                echo $error_txt;
                die;
            }
        }

        return $resultArr;
    }
}
?>