$(function() {
    // tabs einstellungen
    $("#inhalt").tabs()
    
    // tooltip einstellungen
    $('#blacklist img').tooltip({
        track: true,
        showBody: " - ",
        delay: 0,
        showURL: false,
        fade: 300,
        left: 0,
        top: 0,
        extraClass: "tooltip_email"
    });
    
    $('#letzten_eintraegen img').tooltip({
        delay: 0,
        showURL: false,
        fade: 300,
        top: 0,
        left: 0,
        extraClass: "tooltip_normal"
    });
});