<?php
session_start();
function table_check() {
	$mandant = $_SESSION['mandant'];
	include('db_connect.inc.php');
	#mysql_query("ALTER TABLE $import_datei_db ADD RE smallint(6) NOT NULL",$verbindung);
	#mysql_query("ALTER TABLE $metaDB ADD INDEX(VORNAME)",$verbindung);
	#mysql_query("ALTER TABLE $metaDB ADD INDEX(NACHNAME)",$verbindung);
	#mysql_query("ALTER TABLE $metaDB ADD INDEX(PLZ)",$verbindung);
	#mysql_query("ALTER TABLE $metaDB ADD INDEX(ORT)",$verbindung);
	#mysql_query("ALTER TABLE $metaDB ADD ATTRIBUTE char(1)",$verbindung);
	#mysql_query("ALTER TABLE $import_datei_db ADD METADB INT( 11 ) NOT NULL",$verbindung);
	#mysql_query("ALTER TABLE $import_datei_db ADD FAKE INT( 11 ) NOT NULL",$verbindung);
	#mysql_query("ALTER TABLE $import_datei_db ADD DUBLETTE_INDATEI INT( 11 ) NOT NULL",$verbindung);
	
	$history_arr = array(
						"ID" 				=> "INT( 11 ) NOT NULL AUTO_INCREMENT" ,
						"EMAIL" 			=> "VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL" ,
						"ANREDE"			=> "CHAR( 1 ) CHARACTER SET utf8 NOT NULL" ,
						"VORNAME"			=> "VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL" ,
						"NACHNAME"			=> "VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL" ,
						"STRASSE"			=> "VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL" ,
						"PLZ"				=> "VARCHAR( 9 ) CHARACTER SET utf8 NOT NULL" ,
						"ORT"				=> "VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL" ,
						"LAND"				=> "VARCHAR( 5 ) CHARACTER SET utf8 NOT NULL" ,
						"BKZ"				=> "CHAR( 2 ) CHARACTER SET utf8 NOT NULL" ,
						"GEBURTSDATUM"		=> "VARCHAR( 10 ) CHARACTER SET utf8 NOT NULL" ,
						"TELEFON"			=> "VARCHAR( 150 ) CHARACTER SET utf8 NOT NULL" ,
						"EINTRAGSDATUM"		=> "DATETIME NOT NULL" ,
						"IP"				=> "VARCHAR( 25 ) CHARACTER SET utf8 NOT NULL" ,
						"HERKUNFT"			=> "VARCHAR( 250 ) CHARACTER SET utf8 NOT NULL" ,
						"PARTNER"			=> "SMALLINT( 5 ) NOT NULL" ,
						"DOI_DATE"			=> "VARCHAR( 25 ) CHARACTER SET utf8 NOT NULL" ,
						"DOI_IP"			=> "VARCHAR( 35 ) CHARACTER SET utf8 NOT NULL" ,
						"IMPORTDATUM"		=> "DATETIME NOT NULL" ,
						"IMPORT_NR"			=> "INT( 11 ) NOT NULL",
						"ATTRIBUTE"			=> "CHAR(1) NOT NULL"
						 );
						 
	$partner_arr = array(
						"PID" 				=> "SMALLINT( 3 ) NOT NULL" ,
						"Pkz" 				=> "VARCHAR( 75 ) CHARACTER SET utf8 NOT NULL" ,
						"Gen"				=> "VARCHAR( 3 ) CHARACTER SET utf8 NOT NULL" ,
						"GenArt"			=> "VARCHAR( 8 ) CHARACTER SET utf8 NOT NULL" ,
						"Quote"				=> "TINYINT( 2 ) NOT NULL" ,
						"Firma"				=> "VARCHAR( 60 ) CHARACTER SET utf8 NOT NULL" ,
						"Website"			=> "VARCHAR( 100 ) CHARACTER SET utf8 NOT NULL" ,
						"Strasse"			=> "VARCHAR( 100 ) CHARACTER SET utf8 NOT NULL" ,
						"PLZ"				=> "VARCHAR( 5 ) CHARACTER SET utf8 NOT NULL" ,
						"Ort"				=> "VARCHAR( 50 ) CHARACTER SET utf8 NOT NULL" ,
						"Geschaeftsfuehrer"	=> "VARCHAR( 200 ) CHARACTER SET utf8 NOT NULL" ,
						"Registergericht"	=> "VARCHAR( 200 ) CHARACTER SET utf8 NOT NULL" ,
						"Rep_anrede"		=> "CHAR( 4 ) CHARACTER SET utf8 NOT NULL" ,
						"Rep_ap"			=> "VARCHAR( 150 ) CHARACTER SET utf8  NOT NULL" ,
						"Rep_email"			=> "VARCHAR( 200 ) CHARACTER SET utf8 NOT NULL" ,
						"Lieferintervall"	=> "VARCHAR( 35 ) CHARACTER SET utf8 NOT NULL" ,
						"Bedingung"			=> "TEXT CHARACTER SET utf8 NOT NULL",
						"Telefon"			=> "VARCHAR( 200 ) CHARACTER SET utf8 NOT NULL",
						"Fax"				=> "VARCHAR( 200 ) CHARACTER SET utf8 NOT NULL",
						"PRIMARY KEY "		=> "(  PID )",
						"Abschlag" 			=> "TINYINT( 4 ) NOT NULL"
						 );

	
	$history_table_check = mysql_query("SHOW TABLES LIKE '".$metaDB."'", $verbindung); 
			if (mysql_num_rows($history_table_check)== 1) {
			
				$felder = mysql_query("SHOW COLUMNS FROM $metaDB", $verbindung);
				while ($zh = mysql_fetch_array($felder)) {
					$h[] = $zh['Field'];
				}
				
				foreach ($history_arr as $history_arr_values => $history_arr_value) {
					if (!in_array($history_arr_values, $h)) {
							mysql_query("ALTER TABLE $metaDB ADD $history_arr_values $history_arr_value", $verbindung);
					} 
				}
							
			} else {
			
				foreach ($history_arr as $history_arr_values => $history_arr_value) {
					$metaDB_felder .= $history_arr_values." ".$history_arr_value.",";
				}
				
				$metaDB_felder = substr($metaDB_felder, 0, -1);
				create_table($metaDB,$metaDB_felder);
			}
			
	$anrede_table_check = mysql_query("SHOW TABLES LIKE '".$anrede_db."'", $verbindung); 
			if (mysql_num_rows($anrede_table_check)== 1) { 
							
			} else {
				mysql_query("CREATE TABLE $anrede_db
						(`ID` char(1) CHARACTER SET utf8 NOT NULL,
						 `ANREDE` char(4) CHARACTER SET utf8 NOT NULL,
						  PRIMARY KEY (`ID`)
						) DEFAULT CHARSET = utf8", $verbindung);
				mysql_query("INSERT INTO $anrede_db (`ID`, `ANREDE`) VALUES
							('0', 'Frau'),
							('1', 'Herr')", $verbindung);
			}
			
			$herkunft_check = mysql_query("SHOW TABLES LIKE '".$herkunft_db."'", $verbindung); 
			if (mysql_num_rows($herkunft_check)== 1) { 
							
			} else {
				mysql_query("CREATE TABLE $herkunft_db
						(`ID` int(11) NOT NULL AUTO_INCREMENT,
						  `HERKUNFT_NAME` varchar(255) NOT NULL,
						  PRIMARY KEY (`ID`),
						  UNIQUE KEY `HERKUNFT_NAME` (`HERKUNFT_NAME`),
						  INDEX (`ID`)
						) DEFAULT CHARSET = utf8", $verbindung);
			}
			
			$import_datei_check = mysql_query("SHOW TABLES LIKE '".$import_datei_db."'", $verbindung); 
			if (mysql_num_rows($import_datei_check)== 1) { 
							
			} else {
				mysql_query("CREATE TABLE $import_datei_db
						(`IMPORT_NR` int(11) NOT NULL,
						  `IMPORT_NR2` int(11) NOT NULL,
						  `IMPORT_DATEI` varchar(255) NOT NULL,
						  `PARTNER` smallint(6) NOT NULL,
						  `IMPORT_DATUM` datetime NOT NULL,
						  `GELIEFERT_DATUM` date NOT NULL,
						  `BRUTTO` int(11) NOT NULL,
						  `NETTO` int(11) NOT NULL,
						  `DUBLETTEN` int(11) NOT NULL,
						  `LANGADR` int(11) NOT NULL,
						  `VOLL_NAME` int(11) NOT NULL,
						  `LAND_DE` int(11) NOT NULL,
						  `LAND_AT` int(11) NOT NULL,
						  `LAND_CH` int(11) NOT NULL,
						  `BLACKLIST` int(11) NOT NULL,
						  `BOUNCES` int(11) NOT NULL,
						  `BROADMAIL` int(11) NOT NULL,
						  `BROADMAIL_DATUM` date NOT NULL,
						  `INOPTDB` int(11) NOT NULL,
						  `INOPTDB_DATUM` date NOT NULL,
						  `EMAIL_MEHRFACH` int(11) NOT NULL,
						  `NETTO_EXT` int(11) NOT NULL,
						  `NOTIZ` text CHARACTER SET utf8 NOT NULL,
						  `REPORTING_DATUM` datetime NOT NULL,
						  `REPORTING_ABSENDER` varchar(155) NOT NULL,
						  `REPORTING_TYP` char(1) NOT NULL,
						  `ANR_AR` int(11) NOT NULL,
						  `ANR_VN` int(11) NOT NULL,
						  `ANR_NN` int(11) NOT NULL,
						  `ANR_STR` int(11) NOT NULL,
						  `ANR_PLZ` int(11) NOT NULL,
						  `ANR_ORT` int(11) NOT NULL,
						  `ANR_LAND` int(11) NOT NULL,
						  `ANR_DE` int(11) NOT NULL,
						  `ANR_AT` int(11) NOT NULL,
						  `ANR_CH` int(11) NOT NULL,
						  `ANR_BKZ` int(11) NOT NULL,
						  `ANR_TEL` int(11) NOT NULL,
						  `ANR_GEB` int(11) NOT NULL,
						  `RE` smallint(6) NOT NULL,
						  `FAKE` int(11) NOT NULL,
						  `METADB` int(11) NOT NULL,
						  `DUBLETTE_INDATEI` int(11) NOT NULL,
						  `STATUS` TINYINT(4) NOT NULL,
						  `CHECK` varchar(255) NOT NULL,
						  `REPORTING_INFO` varchar(255) NOT NULL,
						  `HINWEIS` TEXT NOT NULL,
						  PRIMARY KEY (`IMPORT_NR`),
						  INDEX (`IMPORT_NR`)
						) DEFAULT CHARSET = utf8", $verbindung);
			}
			
			
			$partner_check = mysql_query("SHOW TABLES LIKE '".$partner_db."'", $verbindung); 
			if (mysql_num_rows($partner_check)==1) {
				$felder = mysql_query("SHOW COLUMNS FROM $partner_db", $verbindung);
				while ($zp = mysql_fetch_array($felder)) {
					$p[] = $zp['Field'];
					
					if ($zp['Field']=="ID") {
						mysql_query("ALTER TABLE $partner_db CHANGE ID PID smallint( 3 ) NOT NULL AUTO_INCREMENT", $verbindung);
					}
					
					if ($zp['Field']=="NAME") {
						mysql_query("ALTER TABLE $partner_db CHANGE NAME Pkz VARCHAR( 15 ) CHARACTER SET utf8 NOT NULL", $verbindung);
					}
					
					if ($zp['Field']=="REP_ANREDE") {
						mysql_query("ALTER TABLE $partner_db CHANGE REP_ANREDE Rep_anrede CHAR( 4 ) CHARACTER SET utf8 NOT NULL", $verbindung);
					}
					
					if ($zp['Field']=="REP_AP") {
						mysql_query("ALTER TABLE $partner_db CHANGE REP_AP Rep_ap VARCHAR( 150 ) CHARACTER SET utf8  NOT NULL", $verbindung);
					}
					
					if ($zp['Field']=="REP_EMAIL") {
						mysql_query("ALTER TABLE $partner_db CHANGE REP_EMAIL Rep_email VARCHAR( 200 ) CHARACTER SET utf8  NOT NULL", $verbindung);
					}
					
					if ($zp['Field']=="INTERVALL") {
						mysql_query("ALTER TABLE $partner_db CHANGE INTERVALL Lieferintervall VARCHAR( 35 ) CHARACTER SET utf8  NOT NULL", $verbindung);
					}
					
					if ($zp['Field']=="BEDINGUNG") {
						mysql_query("ALTER TABLE $partner_db CHANGE BEDINGUNG Bedingung VARCHAR( 200 ) CHARACTER SET utf8  NOT NULL", $verbindung);
					}
				}
				
				foreach ($partner_arr as $partner_arr_values => $partner_arr_value) {
					if (!in_array($partner_arr_values, $p)) {
							mysql_query("ALTER TABLE $partner_db ADD $partner_arr_values $partner_arr_value", $verbindung);
					} 
				}
							
			} else {
				foreach ($partner_arr as $partner_arr_values => $partner_arr_value) {
					$partner_felder .= $partner_arr_values." ".$partner_arr_value.",";
				}
				
				$partner_felder = substr($partner_felder, 0, -1);
				create_table($partner_db,$partner_felder);
			}
			
			
			/*
			$temp_check = mysql_query("SHOW TABLES LIKE '".$temp_db."'", $verbindung); 
			if (mysql_num_rows($temp_check)== 1) { 
							
			} else {
				mysql_query("CREATE TABLE $temp_db DEFAULT CHARSET = utf8", $verbindung);
			}
			
			
			$temp_history_check = mysql_query("SHOW TABLES LIKE '".$metaDB_temp."'", $verbindung); 
			if (mysql_num_rows($temp_history_check)== 1) { 
				mysql_query("ALTER TABLE $metaDB_temp CHANGE COLUMN HERKUNFT HERKUNFT VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL", $verbindung);
			} else {
				mysql_query("CREATE TABLE $metaDB_temp LIKE $metaDB", $verbindung);
				mysql_query("ALTER TABLE $metaDB_temp CHANGE COLUMN HERKUNFT HERKUNFT VARCHAR( 255 ) CHARACTER SET utf8 NOT NULL", $verbindung);
			}
			
			$bl_temp_check = mysql_query("SHOW TABLES LIKE '".$bl_temp."'", $verbindung); 
			if (mysql_num_rows($bl_temp_check)== 1) { 
							
			} else {
				mysql_query("CREATE TABLE $bl_temp (
							`email` varchar(255) NOT NULL,
  							PRIMARY KEY (`email`)) DEFAULT CHARSET = utf8", $verbindung);
			}
			*/
}

function create_table($table, $feldnamen) {
	$mandant = $_SESSION['mandant'];
	include('db_connect.inc.php');
	mysql_query("CREATE TABLE $table
				 (".$feldnamen.")
				 DEFAULT CHARSET = utf8", $verbindung);
}

table_check();
?>