<div style="padding:10px;">
    <span style="font-size:16px;font-weight:bold">Analyse</span><br /><br />
    Folgende Analysen stehen Ihnen zur Verf&uuml;gung: <br /><br /><br />
    <b>Schnell&uuml;bersicht</b>: Zeigt alle relevanten Analysen in einer Kurzansicht an<br /><br />
    <b>Reporting nach Bereinigungsart</b>: Zeigt die Anzahl der unbrauchbaren oder fehlerhaften Datens&auml;tze an<br /><br />
    <b>Quote Brutto-Netto</b>: Zeigt die Quote der verwertbaren Datens&auml;tze an<br /><br />
    <b>Vollst&auml;ndigkeit der Spalten</b>: Zeigt die Datenstruktur der Lieferung und die Anzahl der Datens&auml;tze des jeweiligen Feldes<br /><br />
    <b>Verteilung nach Land</b>: Zeigt die L&auml;nderverteilung der Lieferung an<br /><br />
    <b>Verteilung nach Geschlecht</b>: Zeigt die Geschlechterverteilung der Lieferung an<br /><br />
    <b>Verteilung nach Alter</b>: Zeigt die Altersstruktur der Empf&auml;nger in der Lieferung an<br /><br />
    <b>Verteilung nach Emaildomain</b>: Zeigt die Domainverteilung der Lieferung an<br /><br />
    <b>Verteilung nach Herkunft</b>: Zeigt die Herkunftsverteilung der Lieferung an<br /><br />
    <b>Verteilung nach Tag der Anmeldung</b>: Zeigt die Anmeldedaten der Lieferung an<br /><br />
    <b>Verlauf der letzten Abgleichquoten</b>: Zeigt die Quoten der Reportings der letzten x Lieferungen<br /><br />
    <b>&Uuml;berschneidungen</b> : Zeigt die &Uuml;berschneidungsquoten der Lieferung zu anderen Lieferanten an
</div>