<?php
$import_nr = $_GET['import_nr'];
$anzahl_brutto = $_GET['anzahl_brutto'];
$prog = $_GET['prog'];

$heute = time();
$jahr = date("y", $heute);
$kw = date("W", $heute);
$option_nl = "";
$option_kw = "";
$option_jahr = "";
$option_format ="";

for ($n=1; $n<=10; $n++) {
	$option_nl .= "<option ";
		if ($n==2) {$option_nl .= "selected";}
	$option_nl .= ">".$n."</option>";
}

for ($i=1; $i<=53; $i++) {
	$option_kw .= "<option ";
		if ($kw-1==$i) {$option_kw .= "selected";}
	$option_kw .= ">".$i."</option>";
}

for ($j=$jahr; $j>=$jahr-1; $j--) {
	$option_jahr .= "<option>".$j."</option>";
}

for ($m=1; $m<=3; $m++) {
	switch ($m) {
		case 1: $format="(TEXT)";break;
		case 2: $format="(Multipart)";break;
		case 3: $format="(HTML)";break;
	}
	$option_format .= "<option value='".$m."' ";
		if ($m==2) {$option_format .= "selected";}
	$option_format .= ">".$m." ".$format."</option>";
}
?>
<form action="action.php" method="post">
	<table>
		<tr><td colspan="2"></td></tr>
		<tr><td>Newsletter (Verteiler):</td><td><select name="newsletter"><?php print $option_nl; ?></select></td></tr>
		<tr><td>KW:</td><td><select name="kw_nr"><?php print $option_kw; ?></select> / <select name="kw_jahr"><?php print $option_jahr; ?></select></td></tr>
		<tr><td>Format</td><td><select name="bc_format"><?php print $option_format; ?></select></td></tr>
		<tr><td></td><td><input type="submit" name="submit" value="weiter" /></td></tr>
		<input type="hidden" name="import_nr" value="<?php print $import_nr;?>" />
		<input type="hidden" name="anzahl_brutto" value="<?php print $anzahl_brutto;?>" />
		<input type="hidden" name="prog" value="<?php print $prog;?>" />
		<input type="hidden" name="mandant" value="<?php print $mandant;?>" />
		<input type="hidden" name="sql" value="1" />
		<input type="hidden" name="step" value="export" />
	</table>
</form>