<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>ProgressBar 1.2</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	</head>
<body>
<h1>ProgressBar 1.2</h1>
<?php

 require_once 'ProgressBar.class.php';
 $bar = new ProgressBar();
 $bar->setMessage('loading ...');
 $bar->setAutohide(true);
 $bar->setSleepOnFinish(1);
 $bar->setForegroundColor('#FFA200');

 $elements = 1800; //total number of elements to process
 $bar->initialize($elements); //print the empty bar

 for($i=0;$i<$elements;$i++){

 	sleep(1); // simulate a time consuming process

 	$bar->increase(); //calls the bar with every processed element

 	if($i==1){
 		$bar->setMessage('End of simulation: '.(($i+1)*33).'%');
 	}
 }
?>
<h2>Fertig !</h2>

<?php
/*
 $bar1 = new ProgressBar('End of simulation: 0%');
 $bar1->setForegroundColor('#FFA200');

 $elements1 = 50; //total number of elements to process
 $bar1->initialize($elements1); //print the empty bar

 for($i=0;$i<$elements1;$i++){

 	#sleep(0); // simulate a time consuming process

 	$bar1->increase(); //calls the bar with every processed element
 	$bar1->setMessage('End of simulation: '.(($i+1)*33).'%');
 }
 $bar1->setMessage('End of simulation: 100%');
*/
?>
</body>
</html>
