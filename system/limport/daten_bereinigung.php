<?php
foreach ($i_anzahl as $feld_name => $feld_wert) {
		$fehler = false;
		$fehler_v = false;
		$fehler_n = false;
		$datensaetze_ = '';
		#if ($prog=="backclick") {$HERKUNFT = $exp_zeile['HERKUNFT_NAME'];}
		
			$EMAIL = strtolower($feld_wert['Email']);
			$HERKUNFT = $feld_wert['Herkunft'];
			$HERKUNFTSDATUM = $feld_wert['Eintragsdatum'];
			if ($HERKUNFTSDATUM=='0000-00-00 00:00:00') {$HERKUNFTSDATUM='';}
			$IP = preg_replace('/[^0-9.]/', '', $feld_wert['IP']);
			$BKZ = $feld_wert['BKZ'];
			$exp_doi_ip = preg_replace('/[^0-9.]/', '', $feld_wert['DOI_IP']);
			$exp_doi_date = $feld_wert['DOI_DATE'];
			
			$EMAIL = trim( $feld_wert['Email'], '.' );
			
			$EMAIL = str_replace(' ','',$feld_wert['Email']); 
			$email_endung = substr($EMAIL,strlen($EMAIL)-2);
			
			if ($_SESSION['blacklist']=="bl_ja") {
				$domain_name = explode('@', $EMAIL);
				if (isset($domain_name[1])) {
					$domain_endung = trim($domain_name[1]);
				} else {
					$domain_endung = "";
				}
				
				if (isset($domain_name[0])) {
					$domain_anfang = trim($domain_name[0], '.');
					$EMAIL = $domain_anfang.'@'.$domain_endung;
				}
	
				$domain = "*@".$domain_endung;
				
				
				$bl_email_domain = $_SESSION['bl_email'];
				if (in_array($domain, $bl_email_domain)) {
					$fehler = true;
					$bl++;
				} 
			}
						
			if (check_email_address($EMAIL)==false)
			{
				$email_fehler++;
				$fehler=true;
			}
			
			$ANREDE = $feld_wert['Anrede'];
				if ($ANREDE=='1') 
				{
					$ANREDE="Herr";
				}
				
				if ($ANREDE=='0') 
				{
					$ANREDE="Frau";
				}
		
		
		if (strlen($feld_wert['Vorname'])>1) 
		{
			$VORNAME = strtolower($feld_wert['Vorname']);
			if (strpos($VORNAME, 'sdf')) {
				$vorname_fehler++;
				$fehler_v=true;
				$VORNAME='';
			} else {
				$VORNAME = preg_replace('/[^a-z���� .-]/', '', $VORNAME);
				$VORNAME = str_replace('-',' - ',$VORNAME);
				$VORNAME = ucfirst(ucwords(trim($VORNAME)));
				$VORNAME = str_replace(' - ','-',$VORNAME);
			}
		} else {
			$VORNAME='';
		}
		
		if (strlen($feld_wert['Nachname'])>1) 
		{
			$NACHNAME = strtolower($feld_wert['Nachname']);
			if (strpos($NACHNAME, 'sdf')) {
				$nachname_fehler++;
				$fehler_n=true;
				$NACHNAME='';
			} else {
				$NACHNAME = preg_replace('/[^a-z���� .-]/', '', $NACHNAME);
				$NACHNAME = str_replace('-',' - ',$NACHNAME);
				$NACHNAME = ucfirst(ucwords(trim($NACHNAME)));
				$NACHNAME = str_replace(' - ','-',$NACHNAME);
			}
		} else {
			$NACHNAME='';
		}
		
		if (strlen($feld_wert['Strasse'])>1) 
		{
			if (strpos($feld_wert['STRASSE'], 'sdf')) {
				$STRASSE_fehler++;
				$STRASSE = '';
				$HSNR = '';
				$exp_STRASSE_united = '';
			} else {
				$HSNR="";
				$split  = preg_split('/(?=[0-9])/', $feld_wert['Strasse'], 2);
				$street = array('Str' => $feld_wert['Strasse'], 'Hsnr' => '');
	
				if(count($split) > 1) 
				{
					$street = array('Str' => $split[0], 'Hsnr' => $split[1]);
				}
					$STRASSE = $split[0];
					if (isset($split[1])) {$HSNR = $split[1];}
					$STRASSE = strtolower($STRASSE);
					$STRASSE = str_replace('-',' - ',$STRASSE);
					$STRASSE = ucfirst(ucwords(trim($STRASSE)));
					$STRASSE = str_replace(' - ','-',$STRASSE);
					$exp_STRASSE_united = $STRASSE." ".$HSNR;
			}
			
		} else {
			$STRASSE = '';
			$HSNR = '';
			$exp_STRASSE_united = '';
		}
		
		if ($feld_wert['Land']!='') {$LAND = $feld_wert['Land'];} else {$LAND = '';}
		if ($feld_wert['PLZ']!='') {
			$PLZ = preg_replace('/[^0-9]/', '', $feld_wert['PLZ']);
			if (strlen($PLZ)==4) {$PLZ = "0".$PLZ;}
			if (strlen($PLZ)<=2) {$PLZ = '';}
		} else {
			$PLZ = '';
		}
	
		if ($PLZ!='' && $LAND=="DE" && $PLZ<10000 && $email_endung=="at") 
		{
			$LAND = "AT"; $land_fehler++;
		}
		
		if ($PLZ!='' && $LAND=="DE" && $PLZ<10000 && $email_endung=="ch") 
		{
			$LAND = "CH"; $land_fehler++;
		}
		
		if ($email_endung!="ch" && $email_endung!="de" && $email_endung!="at" && $email_endung!="fr" && $email_endung!="nl" && $LAND=='') {
			$LAND=="DE";
		}
		
		if ($email_endung=="nl" && $LAND=="") 
		{
			$LAND = "NL"; $land_fehler++;
		}
		
		if ($email_endung=="fr" && $LAND=="") 
		{
			$LAND = "FR"; $land_fehler++;
		}
		
		

		if ($feld_wert['Ort']!='') {
			$ORT = strtolower($feld_wert['Ort']);
			$ORT = str_replace('-',' - ',$ORT);
			$ORT = ucfirst(ucwords(trim($ORT)));
			$ORT = str_replace(' - ','-',$ORT);
		} else {
			$ORT='';
		}
		

		if ($feld_wert['Telefon']!='') {$TEL = $feld_wert['Telefon'];}  else {$TEL='';}
		
		if ($feld_wert['Geburtsdatum']!='') {
			$GEBURTSDATUM = $feld_wert['Geburtsdatum'];
			if (strpos($GEBURTSDATUM,".")) {
				$GEBURTSDATUM_TEILE = explode(".",$GEBURTSDATUM);
				if (strlen($GEBURTSDATUM_TEILE[2])==4) {
					$GEBURTSDATUM = $GEBURTSDATUM_TEILE[2]."-".$GEBURTSDATUM_TEILE[1]."-".$GEBURTSDATUM_TEILE[0];
				}
			}
		} else {
			$GEBURTSDATUM='';
		}
		
		if ($fehler_v && $fehler_n) {$fehler=true;}
		
		if (in_array("HSNR",$felder)==false) {
			$STRASSE = $exp_STRASSE_united;
		}
		
		
		if ($_SESSION['datumsformat']=='d') {
			if ($HERKUNFTSDATUM!='') {
				if (strpos($HERKUNFTSDATUM,".")) {
					$HERKUNFTSDATUM_TEILE = explode(".",$HERKUNFTSDATUM);
					if (strlen($HERKUNFTSDATUM_TEILE[2])==4) {
						$HERKUNFTSDATUM_united = $HERKUNFTSDATUM_TEILE[2]."-".$HERKUNFTSDATUM_TEILE[1]."-".$HERKUNFTSDATUM_TEILE[0];
						$HERKUNFTSDATUM = $HERKUNFTSDATUM_united;
					}
				}
				
				$HERKUNFTSDATUM_TEILE2 = explode(" ",$HERKUNFTSDATUM);
				$HERKUNFTSDATUM = $HERKUNFTSDATUM_TEILE2[0];
			}
		} else {
			if (isset($feld_geburtsdatum)) {$GEBURTSDATUM = $GEBURTSDATUM.$feld_geburtsdatum;}
		}
		
		
		$PARTNER = $partner_id;
		
		if (!$fehler) {
			foreach ($felder as $feld) {
				$datensaetze_ .= $$feld;
				if ($prog=='t') {
					$datensaetze_ .= "\t";
				} else {
					$datensaetze_ .= $prog;
				}
				
			}
			
			$datensaetze_ = substr($datensaetze_, 0, -1);
			$datensaetze_ = $datensaetze_."\r\n";
			
			/*
			$datensaetze2 .= $EMAIL."\t".$ANREDE."\t".$VORNAME."\t".$NACHNAME."\t".$exp_STRASSE_united."\t".$PLZ."\t".$ORT."\t".$LAND."\t".$GEBURTSDATUM."\t".$HERKUNFT."\t".$HERKUNFTSDATUM."\t".$IP."\t".$PARTNER."\r\n";
			*/
			$netto_anzahl++;
		}
		$datensaetze .= $datensaetze_;
		
}
?>