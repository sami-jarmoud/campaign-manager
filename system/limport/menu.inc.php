<?php
if (!$step && !$report) {$h_selected="class='a_selected'";}

print "<div id='menu' style='background-color:#2647A0;margin:-1px;padding:5px;padding-left:10px;font-size:11px;color:#FFFFFF;margin-bottom:10px;border-bottom:2px solid #4F9BFF'><span style='color:#FFFFFF'>";

$menu_txt = '| <a href="index.php" '.$h_selected.'>Importe verwalten</a>&nbsp;&nbsp;&nbsp; 
	   | <a href="action.php?step=1" '.$i_selected.'>Datei importieren</a>&nbsp;&nbsp;&nbsp; 
	   | <a href="action.php?step=partner" '.$p_selected.'>Partner</a>&nbsp;&nbsp;&nbsp; 
	   | <a href="index.php?report=sponsoring" '.$s_selected.'>Reporting</a>&nbsp;&nbsp;&nbsp; 
	   | <a href="action.php?step=statistik" '.$st_selected.'>Analysen (beta)</a>&nbsp;&nbsp;&nbsp;
	   | <a href="action.php?step=search" '.$search_selected.'>Suche</a>&nbsp;&nbsp;&nbsp;';
	
if ($mandant=='intone' || $mandant=='pepperos') {
	$menu_txt .= ' | <a href="k_manager/k_manager.php" '.$k_selected.'>Kampagnenverwaltung</a>&nbsp;&nbsp;&nbsp;';
}

switch ($step) {
	case '1' : $menu_txt = "<span style='color:#CCCCCC'><span style='font-weight:bold; color:#FFFFFF;'>1. Dateiauswahl</span> - 2. Dateianalyse - 3. Spaltenzuordnung - 4. Import</span>";break;
	case '2' : $menu_txt = "<span style='color:#CCCCCC'>1. Dateiauswahl - <span style='font-weight:bold; color:#FFFFFF;'>2. Dateianalyse</span> - 3. Spaltenzuordnung - 4. Import</span>";break;
	case '3' : $menu_txt = "<span style='color:#CCCCCC'>1. Dateiauswahl - 2. Dateianalyse - <span style='font-weight:bold; color:#FFFFFF;'>3. Spaltenzuordnung</span> - 4. Import</span>";break;
	case '4' : $menu_txt = "<span style='color:#CCCCCC'>1. Dateiauswahl - 2. Dateianalyse - 3. Spaltenzuordnung - <span style='font-weight:bold; color:#FFFFFF;'>4. Import</span>";break;
	case '5' : $menu_txt = "<div id='imp_bericht'><span style='font-weight:bold; color:#FFFFFF;'>Datei wird jetzt importiert, bitte warten...</span></div>";break;
}

print $menu_txt;
print "</span></div>";
?>