<?php
session_start();

// debug
function firePhPDebug() {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

function zahl_format($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}

function getMonat($monthNumber) {
    $monthDataArray = array(
        1 => 'Jan',
        2 => 'Feb',
        3 => 'M&auml;rz',
        4 => 'Apr',
        5 => 'Mai',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Aug',
        9 => 'Sep',
        10 => 'Okt',
        11 => 'Nov',
        12 => 'Dez'
    );
    
    return $monthDataArray[$monthNumber];
}

function getCnt($type) {
    global $mandant;
    global $jahr;
    
    include('../db_connect.inc.php');
    
    $firePhp = firePhPDebug();

    $result = 0;
    $addWhere = null;
    if ($type != 'k') {
        $addWhere = ' AND `abrechnungsart` = "' . $type . '"';
    }
    
    $res = mysql_query(
        'SELECT count(1) as `count`' . 
            ' FROM `' . $kampagne_db . '`' . 
            ' WHERE YEAR(`datum`) = "' . $jahr . '` AND `k_id` = `nv_id` AND `k_id` > 1' . $addWhere
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_array($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($dataArray['count'], 'getCnt');
        }
        
        $result = $dataArray['count'];
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getCnt()');
        }
    }
    
    return $result;
}

function getQuote($brutto, $netto) {
    $rnd = round((($netto / $brutto) * 100), 1);
    
    return $rnd;
}

function kVolumen() {
    global $mandant;
    global $jahr;
    
    include('db_connect.inc.php');
    
    $firePhp = firePhPDebug();
    
    $cat = '<categories>';
    $data2 = '<dataset seriesName=\'Brutto\' color=\'267CE6\'>';
    $data3 = '<dataset seriesName=\'Netto\' color=\'9900FF\'>';

    $res = mysql_query(
        'SELECT MONTH(`GELIEFERT_DATUM`) as `Monat`' . 
            ' FROM `' . $import_datei_db . '`' . 
            ' WHERE YEAR(`GELIEFERT_DATUM`) = "' . $jahr . '"' . 
            ' GROUP BY `Monat`' . 
            ' ORDER BY `Monat` ASC'
        ,
        $verbindung
    );
    if ($res) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('kVolumen res');
        }
        
        while (($kz = mysql_fetch_array($res)) !== false) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log($kz, '$kz');
            }
            
            $cat .= '<category label=\'' . getMonat($kz['Monat']) . '\' />';
            
            $res2 = mysql_query(
                'SELECT SUM(`BRUTTO`) as `B`, SUM(`NETTO`) as `N`' . 
                    ' FROM `' . $import_datei_db . '`' . 
                    ' WHERE YEAR(`GELIEFERT_DATUM`) = "' . $jahr . '" AND MONTH(`GELIEFERT_DATUM`) = "' . $kz['Monat'] . '"'
                ,
                $verbindung
            );
            if ($res2) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->group('kVolumen res2');
                }
                
                while (($nvz = mysql_fetch_array($res2)) !== false) {
                    // debug
                    if ($firePhp instanceof FirePHP) {
                        $firePhp->log($nvz, '$nvz');
                    }
                    
                    $data2 .= '<set value=\'' . $nvz['B'] . '\' color=\'267CE6\' />';
                    $data3 .= '<set value=\'' . $nvz['N'] . '\' color=\'9900FF\' />';
                }
                mysql_free_result($res2);
                
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->groupEnd();
                }
            } else {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: kVolumen() - $res2');
                }
            }
            
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->groupEnd();
            }
        }
        mysql_free_result($res);
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: kVolumen() - $res');
        }
    }

    $data = '<chart canvasBorderColor=\'999999\' xAxisName=\'Monat\' yAxisName=\'Volumen\' showValues=\'0\' showBorder=\'0\' borderColor=\'CCCCCC\' borderThickness=\'1\' bgColor=\'FFFFFF\'>' . 
        $cat . '</categories>' . 
        $data2 . '</dataset>' . 
        $data3 . '</dataset>' . 
        '</chart>'
    ;
    
    return createChart::renderChart(
        'idkOVv',
        $data,
        'MSLine',
        '780',
        '200'
    );
}

function kAB() {
    global $mandant;
    global $jahr;
    
    include('db_connect.inc.php');
    
    $firePhp = firePhPDebug();

    $data = '<chart canvasBorderColor=\'999999\' showValues=\'0\' showBorder=\'0\' borderColor=\'CCCCCC\' borderThickness=\'1\' bgColor=\'FFFFFF\'>';
    
    $res = mysql_query(
        'SELECT `abrechnungsart` as `a`, COUNT(1) as `count`' . 
            ' FROM `' . $kampagne_db . '`' . 
            ' WHERE YEAR(`datum`) = "' . $jahr . '" AND `k_id` = `nv_id` AND `k_id` > 1' . 
            ' GROUP BY `a`'
        ,
        $verbindung
    );
    if ($res) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('kAB');
        }
        
        while (($kz = mysql_fetch_array($res)) !== false) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log($kz, '$kz');
            }
            
            $data .= '<set value=\'' . $kz['count'] . '\' label=\'' . $kz['a'] . '\' />';
        }
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->groupEnd();
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: kAB()');
        }
    }

    $data .= '</chart>';
    
    return createChart::renderChart(
        'idkAB',
        $data,
        'Pie3D',
        '375',
        '178'
    );
}

function getASP_name($asp) {
    $result = null;
    
    switch ($asp) {
        case 'BM':
            $result = 'Broadmail';
            break;
        
        case 'BC':
            $result = 'Backclick';
            break;
        
        case 'E':
            $result = 'Elaine';
            break;
        
        case 'K':
            $result = 'Kajomi';
            break;
    }
    
    return $result;
}

function getBruttoAndNettoCountFromImportDatei($importDateiDb, $deliveryYear, $pid, $verbindung) {
    $firePhp = firePhPDebug();

    $dataArray = null;
    $addWhere = null;
    $msg = null;
    if (!empty($pid)) {
        $addWhere = ' AND `PARTNER` = "' . $pid . '"';
        $msg = ' Partner';
    }
    
    $res = mysql_query(
        'SELECT SUM(`BRUTTO`) as `b`, SUM(`NETTO`) as `n`' . 
            ' FROM `' . $importDateiDb . '`' . 
            ' WHERE YEAR(`GELIEFERT_DATUM`) = "' . $deliveryYear . '"' . $addWhere
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_array($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($dataArray, 'getBruttoAndNettoCountFromImportDatei' . $msg);
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getBruttoAndNettoCountFromImportDatei()' . $msg);
        }
    }
    
    return $dataArray;
}

function kVS() {
    global $mandant;
    global $jahr;
    global $gesamt_volumen;
    global $gesamt_netto;
    global $tr_volumen;
    global $tr_performance;
    global $data2_chart;

    include('db_connect.inc.php');
    
    $firePhp = firePhPDebug();

    $countDataArray = getBruttoAndNettoCountFromImportDatei(
        $import_datei_db,
        $jahr,
        null,
        $verbindung
    );
    if (is_array($countDataArray)) {
        $gesamt_volumen = $countDataArray['b'];
        $gesamt_netto = $countDataArray['n'];
    }

    $data = '<chart canvasBorderColor=\'999999\' showValues=\'0\' showBorder=\'0\' borderColor=\'CCCCCC\' borderThickness=\'1\' bgColor=\'FFFFFF\'>';
    
    $data2 = '<chart canvasBorderColor=\'999999\' labelDisplay=\'Stagger\' showValues=\'0\' showBorder=\'0\' borderColor=\'CCCCCC\' borderThickness=\'1\' bgColor=\'FFFFFF\'>';
    $data2_cat = '<categories>';
    $data2_dataset = '<dataset seriesName=\'Brutto\' color=\'267CE6\'>';
    $data2_dataset2 = '<dataset seriesName=\'Netto\' color=\'9900FF\'>';
    
    $res = mysql_query(
        'SELECT `p`.`' . $felderPartnerDB['PID'] . '` as `pid`, `p`.`' . $felderPartnerDB['Pkz'] . '` as `pkz`, SUM(`i`.`BRUTTO`) as `b`' . 
            ' FROM `' . $import_datei_db . '` as `i`,' . 
                '`' . $partner_db . '` as `p`' . 
            ' WHERE YEAR(`i`.`GELIEFERT_DATUM`) = "' . $jahr . '" AND `i`.`IMPORT_NR` = `i`.`IMPORT_NR2`' . 
                ' AND `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`' . 
            ' GROUP BY `p`.`' . $felderPartnerDB['PID'] . '`' . 
            ' ORDER BY `b` DESC'
        ,
        $verbindung
    );
    if ($res) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->group('kVS data');
        }
        
        while (($kz = mysql_fetch_array($res)) !== false) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->group('row');
                $firePhp->log($kz, '$kz');
            }
            
            $pkz = $kz['pkz'];
            $pid = $kz['pid'];
            $nr = $kz['nr'];
            
            $kzDataArray = getBruttoAndNettoCountFromImportDatei(
                $import_datei_db,
                $jahr,
                $pid,
                $verbindung
            );
            if (is_array($kzDataArray)) {
                $gesamt_o = $kzDataArray['b'];
                $gesamt_k = $kzDataArray['n'];
            }
            
            if ($gesamt_o > 0) {
                $data .= '<set value=\'' . $gesamt_o . '\' label=\'' . $pkz . '\' />';
                $data2_dataset .= '<set value=\'' . $gesamt_o . '\' color=\'267CE6\' />';
                $data2_dataset2 .= '<set value=\'' . $gesamt_k . '\' color=\'9900FF\' />';
                $data2_cat .= '<category label=\'' . $pkz . '\' />';
                
                $tr_volumen .= '
                    <tr class="ovTR1">
                        <td class="ovTR1_label">' . $pkz . '</td>
                        <td class="ovTR1_label2">' . zahl_format($gesamt_o) . '</td>
                        <td class="ovTR1_label3">' . getQuote($gesamt_volumen, $gesamt_o) . '%</td>
                    </tr>
                ';
                
                $tr_performance .= '
                    <tr class="ovTR1">
                        <td class="ovTR1_label">' . $pkz . '</td>
                        <td class="ovTR1_label2">' . getQuote($gesamt_o, $gesamt_k) . '%</td>
                    </tr>
                ';
            }
            
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->groupEnd();
            }
        }
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->groupEnd();
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: kVS() - res');
        }
    }
    $data .= '</chart>';
    
    $data2 .= $data2_cat . '</categories>' . 
        $data2_dataset . '</dataset>' . 
        $data2_dataset2 . '</dataset>' . 
        '</chart>'
    ;
    $data2_chart = createChart::renderChart(
        'idkVSp',
        $data2,
        'MSCombi3D',
        '378',
        '300'
    );

    return createChart::renderChart(
        'idkVS',
        $data,
        'Pie3D',
        '378',
        '300'
    );
}

function getASPquote() {
    global $mandant;
    global $jahr;
    global $gesamt_versendet;
    global $gesamt_openings;
    global $gesamt_klicks;
    
    include('../db_connect.inc.php');
    
    $firePhp = firePhPDebug();

    $res = mysql_query(
        'SELECT SUM(`versendet`) as `gsmt`, SUM(`openings`) as `o`, SUM(`klicks`) as `k`' . 
            ' FROM `' . $kampagne_db . '`' . 
            ' WHERE YEAR(`datum`) = "' . $jahr . '"'
        ,
        $verbindung
    );
    if ($res) {
        $kz2 = mysql_fetch_array($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($kz2, '$kz2');
        }
        
        $gesamt_versendet = $kz2['gsmt'];
        $gesamt_openings = $kz2['o'];
        $gesamt_klicks = $kz2['k'];
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getASPquote()');
        }
    }
}

ob_start();

$mandant = $_SESSION['mandant'];
require_once('../library/chart.php');
include('db_connect.inc.php');

$firePhp = firePhPDebug();

if ($_GET['jahr']) {
    $jahr = $_GET['jahr'];
} else {
    $jahr = date('Y');
}

getASPquote();

$yLink = '';
$resYear = mysql_query(
    'SELECT YEAR(`GELIEFERT_DATUM`) as `jahr`' . 
        ' FROM `' . $import_datei_db . '`' . 
        ' WHERE YEAR(`GELIEFERT_DATUM`) > "0"' . 
        ' GROUP BY `jahr`' . 
        ' ORDER BY `jahr` DESC'
    ,
    $verbindung
);
if ($resYear) {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('resYear');
    }
    
    while (($row = mysql_fetch_array($resYear)) !== false) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($row, '$row');
        }
        
        $yLink .= '<a href="#" onclick="setRequest(\'limport/overview.php?jahr=' . $row['jahr'] . '\',\'dm_ov\');return false;"> | ' . $row['jahr'] . '</a>';
    }
    mysql_free_result($resYear);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: resYear');
    }
}

$tr_volumen = '';
$tr_performance = '';
?>
<div class="ovHeader">
    &Uuml;bersicht<br />
    <span style="font-size:18px;color:#8e8901">Datenlieferungen - <?php echo $jahr . $yLink; ?>
</div>
<div style="width:850px;margin:auto;">
    <div id="kOV">
        <div class="kOV_table">
            <table class="ovTable" width="800">
                <tr>
                    <td class="ovBlock">Lieferungen</td>
                </tr>
                <tr>
                    <td ><?php echo kVolumen(); ?></td>
                </tr>
            </table>
        </div>
        <div class="kOV_table" style="float:left;margin-right:20px;">
            <table class="ovTable" width="390">
                <tr>
                    <td class="ovBlock">Lieferanten / Volumen</td>
                </tr>
                <tr>
                    <td><?php echo kVS(); ?></td>
                </tr>     	
            </table>
        </div>
        <div class="kOV_table" style="float:left;margin-right:20px;">
            <table class="ovTable" width="150">
                <tr>
                    <td class="ovBlock">Lieferanten / Quote</td>
                </tr>
                <tr>
                    <td><?php echo $data2_chart; ?></td>
                </tr>     	
            </table>
        </div>
        <div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
            <table class="ovTable" width="390">
                <tr>
                    <td class="ovTR" width="100">Lieferant</td>
                    <td class="ovTR" align="right">Geliefert</td>
                    <td class="ovTR" align="right">Quote</td>
                </tr>
                <?php echo $tr_volumen; ?>
                <tr class="ovTR1">
                    <td class="ovTR">Gesamt</td>
                    <td class="ovTR" align="right"><?php echo zahl_format($gesamt_volumen); ?></td>
                    <td class="ovTR"></td>
                </tr>
            </table>
        </div>
        <div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
            <table class="ovTable" width="390">
                <tr>
                    <td class="ovTR" width="100">Lieferant</td>
                    <td class="ovTR" align="right">Netto Quote</td>
                </tr>
                <?php echo $tr_performance; ?>
                <tr class="ovTR1">
                    <td class="ovTR">Gesamt</td>
                    <td class="ovTR" align="right">&#216; <?php echo getQuote($gesamt_volumen, $gesamt_netto); ?>%</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="ovFooter"></div>
<?php
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($jahr, '$jahr');
    $firePhp->log($gesamt_versendet, '$gesamt_versendet');
    $firePhp->log($gesamt_openings, '$gesamt_openings');
    $firePhp->log($gesamt_klicks, '$gesamt_klicks');
    $firePhp->log($gesamt_volumen, '$gesamt_volumen');
    $firePhp->log($gesamt_netto, '$gesamt_netto');
    $firePhp->log($tr_volumen, '$tr_volumen');
    $firePhp->log($tr_performance, '$tr_performance');
    $firePhp->log($data2_chart, '$data2_chart');
}

ob_end_flush();
?>