<?php
function zahl_formatieren($zahl) {
    $zahl = number_format($zahl, 0, 0, '.');
    
    return $zahl;
}



require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');

$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */


$u_rechte = $_SESSION['rechte'];
$u_zugang = $_SESSION['zugang'];
$mandant_all = $_SESSION['mandant_all'];
$zugang_mandant = isset($_GET['m']) ? $_GET['m'] : '';

if (isset($_GET['testimport'])) {
    $_SESSION['testimport'] = (int) $_GET['testimport'];
}
$testimport = $_SESSION['testimport'];

// debug
$debugLogManager->logData('mandant', $mandant);
$debugLogManager->logData('POST', $_POST);
$debugLogManager->logData('GET', $_GET);
#$debugLogManager->logData('SESSION', $_SESSION);

if ((int) $testimport === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

// drop stats_ and others Table
mysql_query(
    'DROP TABLE IF EXISTS `stats_' . $_SESSION['benutzer_name'] . '`',
    $verbindung
);
mysql_query(
    'DROP TABLE IF EXISTS `' . $temp_db . '`',
    $verbindung
);
mysql_query(
    'DROP TABLE IF EXISTS `' . $bl_temp . '`',
    $verbindung
);
mysql_query(
    'DROP TABLE IF EXISTS `' . $metaDB_temp . '`',
    $verbindung
);
mysql_query(
    'DROP TABLE IF EXISTS `tmp_bl_' . $_SESSION['benutzer_name'] . '`',
    $verbindung
);


/**
 * TODO - begin
 * 
 * ???
 */
include('db_pep.inc.php');
$user_anmelden_sql = mysql_query(
    'UPDATE `benutzer`' 
        . ' SET `timestamp` = ""' 
        . ' WHERE `name` = ' . mysql_real_escape_string($_SESSION['username'])
    ,
    $verbindung_pep
);

$user_logedin = mysql_query(
    'SELECT `timestamp`' 
        . ' FROM `benutzer`' 
        . ' WHERE `timestamp` != ""'
    ,
    $verbindung_pep
);
if ($user_logedin) {
    $zeile_user = mysql_fetch_assoc($user_logedin);
    mysql_free_result($user_logedin);
    
    // debug
    $debugLogManager->logData('zeile_user', $zeile_user);
    
    $user_session_time = $zeile_user['timestamp'];
} else {
    // debug
    $debugLogManager->logData('sql error: user_logedin', mysql_error($verbindung_pep));
    
    $user_session_time = 0;
}

$jetzt = time();
$user_zeit = $jetzt - 36000;
if ($user_zeit > $user_session_time) {
    $user_abmelden_sql = mysql_query(
        'UPDATE `benutzer`' 
            . ' SET `timestamp` = ""' 
            . ' WHERE `timestamp` != ""'
        ,
        $verbindung_pep
    );
}
/**
 * TODO: end
 */

/*
if ((int) $_SESSION['mID'] === 1) {
    require_once('db_optdb_connect.inc.php');
    
    mysql_query(
        'DROP TABLE IF EXISTS `stats_' . $_SESSION['benutzer_name'] . '`',
        $verbindung_optdb
    );
    mysql_query(
        'DROP TABLE IF EXISTS `temp_limport_' . $_SESSION['benutzer_name'] . '`',
        $verbindung_optdb
    );
}
 * 
 */

require_once('db_blacklist_connect.inc.php');
mysql_query(
    'DROP TABLE IF EXISTS `temp_' . $_SESSION['benutzer_name'] . '`',
    $verbindung_bl
);
?>
<script type="text/javascript">
    function check(imp_nr) {
        Check = confirm('Wollen Sie die Datens�tze wirklich l�schen?');
        
        if (Check === true) {
            //window.location.href = 'index.php?import_nr=' + imp_nr + '&del=1';
        }
    }
  
    function quote(feld,wert,prozent) {
        var neue_quote;
        
        neue_quote = (wert*(prozent/100));
        document.update.feld.value = neue_quote;
    }

    function str_replace(search, replace, subject) {
        return subject.split(search).join(replace);
    }
</script>
<?php
include('tabelle.inc2.php');
?>

<input type="hidden" name="page" id="page" value="limport/index.php" />
<input type="hidden" name="page_sub" id="page_sub" value="a" />