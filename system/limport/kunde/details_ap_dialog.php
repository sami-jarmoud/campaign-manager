<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$id = $_GET['id'];
$kunde_id = $_GET['kunde_id'];
include('../../limport/db_connect.inc.php');
include('../../library/util.php');
include('../../library/dataManagerWebservice.php');

$dmMweb = new dataManagerWebservice();
$APDataArr = $dmMweb->getAP($partner_ap,$id);

if ($APDataArr['anrede'] == "Frau") {
	$anr_frau = "checked";
} else {
	$anr_herr= "checked";
} 

?>
<input type="hidden" name="dm_ap_id" id="dm_ap_id" value="<?php print $id; ?>" />
<input type="hidden" name="dm_kunde_id2" id="dm_kunde_id2" value="<?php print $kunde_id; ?>" />
<input type="hidden" name="dm_action" id="dm_action" value="kunde_ap" />
<table>
	<tr>
    	<td class='details_info'>Anrede</td>
        <td class='details2_info'><input type="radio" name="anrede" value="Herr" "<?php print $anr_herr; ?>" /> Herr &nbsp;&nbsp;&nbsp;<input type="radio" name="anrede" value="Frau" "<?php print $anr_frau; ?>" /> Frau</td>
    </tr>
    <tr>
    	<td class='details_info2'>Titel</td>
        <td class='details2_info2'><input type="text" name="titel" value="<?php print $APDataArr['titel']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Vorname*</td>
        <td class='details2_info'><input type="text" name="vorname" id="vorname" value="<?php print $APDataArr['vorname']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Nachname*</td>
        <td class='details2_info2'><input type="text" name="nachname" id="nachname" value="<?php print $APDataArr['nachname']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Email*</td>
        <td class='details2_info'><input type="text" name="email" id="email" value="<?php print $APDataArr['email']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Telefon</td>
        <td class='details2_info2'><input type="text" name="telefon" value="<?php print $APDataArr['telefon']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Fax</td>
        <td class='details2_info'><input type="text" name="fax" value="<?php print $APDataArr['fax']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Mobil</td>
        <td class='details2_info2'><input type="text" name="mobil" value="<?php print $APDataArr['mobil']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Position</td>
        <td class='details2_info'><input type="text" name="position" value="<?php print $APDataArr['position']; ?>" /></td>
    </tr>

</table>