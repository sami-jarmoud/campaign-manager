<?php
session_start();

// debug
function firePhp () {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];

include('../../limport/db_connect.inc.php');

$firePhp = firePhp();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_POST, 'POST');
}

$action = $_POST['dm_action'];
if ($action == 'kunde') {
    $del_kunde_id = $_POST['dm_del_kunde_id'];

    $id = $_POST['dm_kunde_id'];
    $firma = utf8_decode($_POST['firma']);
    $firma_short = utf8_decode($_POST['firma_short']);
    $strasse = utf8_decode($_POST['strasse']);
    $plz = $_POST['plz'];
    $ort = utf8_decode($_POST['ort']);
    $telefon = $_POST['telefon'];
    $fax = $_POST['fax'];
    $website = $_POST['website'];
    $email = $_POST['email'];
    $genart = $_POST['genart'];
    $gf = utf8_decode($_POST['gf']);
    $registergericht = utf8_decode($_POST['registergericht']);

    if ($del_kunde_id != '') {
        $check_sql = mysql_query(
            'SELECT *' . 
                ' FROM `' . $import_datei_db . '`' . 
                ' WHERE `PARTNER` = "' . $del_kunde_id . '"'
            ,
            $verbindung
        );
        if (mysql_num_rows($check_sql) > 0) {
            $sql = mysql_query(
                'UPDATE `' . $partner_db . '`' . 
                    ' SET `' . $felderPartnerDB['Status'] . '` = 0' . 
                    ' WHERE `' . $felderPartnerDB['PID'] . '` = ' . $del_kunde_id
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: update Kunde');
                }
            }
        } else {
            $sql = mysql_query(
                'DELETE FROM `' . $partner_db . '`' . 
                    ' WHERE `' . $felderPartnerDB['PID'] . '` = ' . $del_kunde_id
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: delete Kunde');
                }
            }
        }
    } else {
        if ($id) {
            $sql = mysql_query(
                'UPDATE `' . $partner_db . '`' . 
                    ' SET `' . $felderPartnerDB['Firma'] . '` = "' . $firma . '"' . 
                        ', `' . $felderPartnerDB['Pkz'] . '` = "' . $firma_short . '"' . 
                        ', `' . $felderPartnerDB['Strasse'] . '` = "' . $strasse . '"' . 
                        ', `' . $felderPartnerDB['PLZ'] . '` = "' . $plz . '"' . 
                        ', `' . $felderPartnerDB['Ort'] . '` = "' . $ort . '"' . 
                        ', `' . $felderPartnerDB['Telefon'] . '` = "' . $telefon . '"' . 
                        ', `' . $felderPartnerDB['Fax'] . '` = "' . $fax . '"' . 
                        ', `' . $felderPartnerDB['Website'] . '` = "' . $website . '"' . 
                        ', `' . $felderPartnerDB['Rep_email'] . '` = "' . $email . '"' . 
                        ', `' . $felderPartnerDB['GenArt'] . '` = "' . $genart . '"' . 
                        ', `' . $felderPartnerDB['Geschaeftsfuehrer'] . '` = "' . $gf . '"' . 
                        ', `' . $felderPartnerDB['Registergericht'] . '` = "' . $registergericht . '"' . 
                    ' WHERE `' . $felderPartnerDB['PID'] . '` = ' . $id
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: update Kunde Id');
                }
            }
        } else {
            print $felderPartnerDB['Firma'];
            
            $sql = mysql_query(
                'INSERT INTO `' . $partner_db . '` (' . 
                        '`' . $felderPartnerDB['Firma'] . '`, `' . $felderPartnerDB['Pkz'] . '`, `' . $felderPartnerDB['Strasse'] . '`, `' . $felderPartnerDB['PLZ'] . '`' . 
                        ', `' . $felderPartnerDB['Ort'] . '`, `' . $felderPartnerDB['Telefon'] . '`, `' . $felderPartnerDB['Fax'] . '`, `' . $felderPartnerDB['Website'] . '`' . 
                        ', `' . $felderPartnerDB['Rep_email'] . '`, `' . $felderPartnerDB['GenArt'] . '`, `' . $felderPartnerDB['Geschaeftsfuehrer'] . '`' . 
                        ', `' . $felderPartnerDB['Registergericht'] . '`, `' . $felderPartnerDB['Status'] . '`' . 
                    ') VALUES (' . 
                        '"' . $firma . '", "' . $firma_short . '", "' . $strasse . '", "' . $plz . '"' . 
                        ', "' . $ort . '", "' . $telefon . '", "' . $fax . '", "' . $website . '"' . 
                        ', "' . $email . '", "' . $genart . '", "' . $gf . '"' . 
                        ', "' . $registergericht . '", 1' . 
                    ')'
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: insert Kunde');
                }
            }
        }
    }
}


if ($action == 'kunde_ap') {
    $del_ap_id = $_POST['dm_del_ap_id'];

    $ap_id = $_POST['dm_ap_id'];
    $kunde_id = $_POST['dm_kunde_id2'];
    $anrede = $_POST['anrede'];
    $titel = utf8_decode($_POST['titel']);
    $vorname = utf8_decode($_POST['vorname']);
    $nachname = utf8_decode($_POST['nachname']);
    $email = utf8_decode($_POST['email']);
    $telefon = $_POST['telefon'];
    $fax = $_POST['fax'];
    $mobil = $_POST['mobil'];
    $position = utf8_decode($_POST['position']);

    if ($del_ap_id != '') {
        $sql = mysql_query(
            'DELETE FROM `' . $partner_ap . '`' . 
                ' WHERE `ap_id` = ' . $del_ap_id
            ,
            $verbindung
        );
        if (!$sql) {
            // debug
            if ($firePhp instanceof FirePHP) {
                $firePhp->log(mysql_error($verbindung), 'sql error: delete KundeAp');
            }
        }
    } else {
        if ($ap_id) {
            $sql = mysql_query(
                'UPDATE `' . $partner_ap . '`' . 
                    ' SET `anrede` = "' . $anrede . '"' . 
                        ', `titel` = "' . $titel . '"' . 
                        ', `vorname` = "' . $vorname . '"' . 
                        ', `nachname` = "' . $nachname . '"' . 
                        ', `email` = "' . $email . '"' . 
                        ', `telefon` = "' . $telefon . '"' . 
                        ', `fax` = "' . $fax . '"' . 
                        ', `mobil` = "' . $mobil . '"' . 
                        ', `position` = "' . $position . '"' . 
                    ' WHERE `ap_id` = ' . $ap_id
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: update KundeAp');
                }
            }
        } else {
            $sql = mysql_query(
                'INSERT INTO `' . $partner_ap . '` (' . 
                        '`kunde_id`, `anrede`, `titel`, `vorname`, `nachname`, `email`, `telefon`, `fax`, `mobil`, `position`, `status`' . 
                    ') VALUES (' . 
                        '"' . $kunde_id . '", "' . $anrede . '", "' . $titel . '", "' . $vorname . '", "' . $nachname . '", "' . $email . '"' . 
                        ', "' . $telefon . '", "' . $fax . '", "' . $mobil . '", "' . $position . '", 1' . 
                    ')'
                ,
                $verbindung
            );
            if (!$sql) {
                // debug
                if ($firePhp instanceof FirePHP) {
                    $firePhp->log(mysql_error($verbindung), 'sql error: insert KundeAp');
                }
            }
        }
    }
}
?>