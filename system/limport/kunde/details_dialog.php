<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$id = $_GET['id'];
include('../../limport/db_connect.inc.php');
include('../../library/util.php');
include('../../library/dataManagerWebservice.php');

$dmMweb = new dataManagerWebservice();
$kundeDataArr = $dmMweb->getKundeData(	$partner_db,
										$felderPartnerDB['PID'],
										$felderPartnerDB['Pkz'],
										$felderPartnerDB['GenArt'],
										$felderPartnerDB['Firma'],
										$felderPartnerDB['Website'],
										$felderPartnerDB['Strasse'],
										$felderPartnerDB['PLZ'],
										$felderPartnerDB['Ort'],
										$felderPartnerDB['Geschaeftsfuehrer'],
										$felderPartnerDB['Registergericht'],
										$felderPartnerDB['Telefon'],
										$felderPartnerDB['Fax'],
										$felderPartnerDB['Bedingung'],
										$felderPartnerDB['Rep_email'],
										$felderPartnerDB['Status'],
										$id
									 );



if ($kundeDataArr['status'] == 1) {
	$status = "<span style='color:green'>Aktiv</span>";
} else {
	$status = "<span style='color:red'>Inaktiv</span>";
}

if (strtolower($kundeDataArr['genart']) == 'realtime') {
	$realtime_checked = "checked";
} else {
	$import_checked = "checked";
}
?>
<input type="hidden" name="dm_kunde_id" id="dm_kunde_id" value="<?php print $id; ?>" />
<input type="hidden" name="dm_action" id="dm_action" value="kunde" />
<table>
	<tr>
    	<td class='details_info2'>Lieferart</td>
        <td class='details2_info2'><input type="radio" name="genart" value="Import" <?php print $import_checked; ?>/> Import &nbsp;&nbsp;<input type="radio" name="genart" value="Realtime" <?php print $realtime_checked; ?>/> Realtime</td>
    </tr>
	<tr>
    	<td class='details_info'>Firma*</td>
        <td class='details2_info'><input type="text" name="firma" id="firma" value="<?php print $kundeDataArr['firma']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>K&uuml;rzel</td>
        <td class='details2_info2'><input type="text" name="firma_short" value="<?php print $kundeDataArr['firma_short']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Strasse</td>
        <td class='details2_info'><input type="text" name="strasse" value="<?php print $kundeDataArr['strasse']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Plz</td>
        <td class='details2_info2'><input type="text" name="plz" value="<?php print $kundeDataArr['plz']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Ort</td>
        <td class='details2_info'><input type="text" name="ort" value="<?php print $kundeDataArr['ort']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Telefon</td>
        <td class='details2_info2'><input type="text" name="telefon" value="<?php print $kundeDataArr['telefon']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Fax</td>
        <td class='details2_info'><input type="text" name="fax" value="<?php print $kundeDataArr['fax']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Webseite</td>
        <td class='details2_info2'>http://<input type="text" name="website" style="width:158px" value="<?php print $kundeDataArr['website']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Email</td>
        <td class='details2_info'><input type="text" name="email" value="<?php print $kundeDataArr['email']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info2'>Gesch&auml;ftsf&uuml;hrer</td>
        <td class='details2_info2'><input type="text" name="gf" value="<?php print $kundeDataArr['geschaeftsfuehrer']; ?>" /></td>
    </tr>
    <tr>
    	<td class='details_info'>Registergericht</td>
        <td class='details2_info'><input type="text" name="registergericht" value="<?php print $kundeDataArr['registergericht']; ?>" /></td>
    </tr>
</table>