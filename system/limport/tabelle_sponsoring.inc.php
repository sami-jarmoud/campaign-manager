<?php
include('check_login.inc.php');
$user_id = session_id();
include('db_connect.inc.php');

if (isset($_REQUEST['import_jahr'])) {$import_jahr = $_REQUEST['import_jahr'];} else {$import_jahr = date("Y");}
if (isset($_REQUEST['import_monat'])) {$import_monat = $_REQUEST['import_monat'];} else {$import_monat = "";}
if (isset($_REQUEST['import_partner'])) {$import_partner = $_REQUEST['import_partner'];} else {$import_partner = "";}
if (isset($_REQUEST['report_filter'])) {$report_filter = $_REQUEST['report_filter'];} else {$report_filter = "offen";}

if (isset($_POST['email'])) {$emailAbfrage = trim($_POST['email']);}
if (file_exists("test_export.txt")) {unlink('test_export.txt');}

if ($_REQUEST['GenArt']) {
	if ($_REQUEST['GenArt']=='Import') {
		$genart="Import";
		importe_raussuchen();
	}
	if ($_REQUEST['GenArt']=='Realtime') {
		$genart="Realtime";
		realtime_raussuchen();
	}
} else {
	$genart="Import";
	importe_raussuchen();
}

function form() {
	$form = "";
}

function importe_raussuchen() 
{
	global $db_name;
	global $mandant;
	global $import_monat;
	global $import_jahr;
	global $import_partner;
	global $anrede_db;
	global $herkunft_db;
	global $import_datei_db;
	global $partner_db;
	global $metaDB;
	global $temp_db;
	global $metaDB_temp;
	global $netto_ext;
	global $report_filter;
	global $u_rechte;
	global $importe;
	include('db_connect.inc.php');

	switch ($report_filter) {
		case "versendet" : $sql_ = " AND $import_datei_db.REPORTING_DATUM != '0000-00-00 00:00:00'";break;
		case "offen" : $sql_ = " AND $import_datei_db.REPORTING_DATUM = '0000-00-00 00:00:00'";break;
	}
	
	if ($import_monat>=1) {
		$sql_monat = " AND MONTH($import_datei_db.IMPORT_DATUM) = '$import_monat'";
	}
	
	if ($import_partner!='') {
		$sql_partner = " AND $partner_db.PID = '$import_partner'";
	}
	
	$i = 1;
	$imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db, $partner_db
							  WHERE $import_datei_db.PARTNER=$partner_db.PID
							  AND YEAR($import_datei_db.IMPORT_DATUM) = '$import_jahr'
							  AND $import_datei_db.IMPORT_NR=$import_datei_db.IMPORT_NR2
							  ".$sql_.$sql_monat.$sql_partner."
							  ORDER BY $import_datei_db.GELIEFERT_DATUM desc
							",$verbindung);
							
	$imp_vorhanden = mysql_num_rows($imp_daten);
	
	if ($imp_vorhanden) {
					
							
	$m =1;
	while ($imp_zeile = mysql_fetch_array($imp_daten))
	{
		$imp_nr = $imp_zeile['IMPORT_NR'];
		$imp_anzahl_sql = mysql_query("SELECT * FROM $import_datei_db WHERE IMPORT_NR2='$imp_nr'",$verbindung);
		$imp_datei_anzahl = mysql_num_rows($imp_anzahl_sql);
		
		

		if ($i==2) {$bg = "#EDF5FF";} else {$bg = "#FFFFFF";$i=1;}; 
		$imp_partner = $imp_zeile['Pkz'];
		$imp_partner_id = $imp_zeile['PARTNER'];
		$imp_datum = $imp_zeile['GELIEFERT_DATUM'];
		$imp_datum_teile = explode(" ", $imp_datum);
		$imp_datum_teile_zeit = explode(":", $imp_datum_teile[1]);
		$imp_datum_teile2 = explode("-", $imp_datum_teile[0]);
		$imp_datum_de = $imp_datum_teile2[2].".".$imp_datum_teile2[1].".".$imp_datum_teile2[0];
		$imp_mehrfach = $imp_zeile['EMAIL_MEHRFACH'];
		$geliefert_datum = $imp_zeile['GELIEFERT_DATUM'];
		$geliefert_teile = explode("-", $geliefert_datum);
		$geliefert_datum_de = $geliefert_teile[2].".".$geliefert_teile[1].".".$geliefert_teile[0];
		
		$imp_datei = substr($imp_zeile['IMPORT_DATEI'], 0, 30);
		if (strlen($imp_datei)>=30) {$imp_datei .= "...";}
		$imp_anzahl = $imp_zeile['BRUTTO']-$imp_mehrfach;
		$imp_netto = $imp_zeile['NETTO'];
		$imp_netto_ext = $imp_zeile['NETTO_EXT'];
		$imp_notiz = $imp_zeile['NOTIZ'];
		$imp_rep_datum = $imp_zeile['REPORTING_DATUM'];
		$absender = $imp_zeile['REPORTING_ABSENDER'];
		$reporting_typ = $imp_zeile['REPORTING_TYP'];
		
		$re = $imp_zeile['RE'];
		if ($re==0) {
			$re_text = "<a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=import&re=erhalten\" style='color#2647A0'>Rechnung erhalten</a>";
		} 
		
		if ($re==1) {
			$re_text = '<div style="font-size:9px; color:#009900; float:left;">(Rechnung erhalten)</div>';
		} 
		
		$imp_langadr = $imp_zeile['LANGADR'];
		$imp_bounces = $imp_zeile['BOUNCES'];
		$imp_dbl = $imp_zeile['DUBLETTEN'];
		$imp_opt_datum = $imp_zeile['INOPTDB_DATUM'];
		$opt_datum_teile = explode("-", $imp_opt_datum);
		$opt_datum_de = $opt_datum_teile[2].".".$opt_datum_teile[1].".".$opt_datum_teile[0];
		
		if ($imp_dbl>0) {$q_dbl = round(($imp_dbl/$imp_anzahl)*100,1);} else {$q_dbl = 0;}
		if ($imp_langadr>0) {$q_langadr = round(($imp_langadr/$imp_anzahl)*100,1);} else {$q_langadr = 0;}
		$q_netto_zw = ($imp_dbl/$imp_anzahl)*100;
		$q_netto = round((100-$q_netto_zw),1);
		
		
		$brutto_neu = $imp_anzahl;
		if ($imp_datei_anzahl>1) {
			$imp_daten2 = mysql_query("SELECT SUM(BRUTTO) AS BRUTTO, SUM(NETTO) AS NETTO, SUM(EMAIL_MEHRFACH) AS EMAIL_MEHRFACH, ".$sql." SUM(NETTO_EXT) AS NETTO_EXT
								  FROM $import_datei_db
								  WHERE IMPORT_NR2='$imp_nr'
								",$verbindung);
			$imp_zeile2 = mysql_fetch_array($imp_daten2);
			$imp_mehrfach = $imp_zeile2['EMAIL_MEHRFACH'];
			$imp_anzahl = $imp_zeile2['BRUTTO']-$imp_mehrfach;
			$brutto_neu = $imp_anzahl;
			$imp_netto = $imp_zeile2['NETTO'];
			$imp_netto_ext = $imp_zeile2['NETTO_EXT'];
			
		};
		
		
		
		
		$q_bounces = round(($imp_bounces/$imp_anzahl)*100,1);
		$quote_ = round(($imp_netto_ext/$brutto_neu)*100,1);
		
		if ($typ=='n') {$kom_zahl = $netto;} else {$kom_zahl = $imp_netto_ext;}
		
	
		$tooltip.$m = "<table width=\'auto\' cellspacing=\'0\' cellpadding=\'3\' border=\'1\' bordercolor=\'#CCCCCC\' style=\'border-collapse:collapse\'><tr><td align=\'left\'>versendet am: ".$imp_rep_datum."</td></tr><tr><td align=\'left\'>Absender: ".$absender."</td></tr><tr><td align=\'left\'>Kommunizierte Zahl: ".$kom_zahl."</td></tr></table>";
		
		if ($imp_rep_datum == '0000-00-00 00:00:00') {
			$imp_rep_datum_txt = '<span style="color:#FF0000">offen</span>';
		} else {
			$imp_rep_datum_txt = "<span onmouseover=\"Tip('".$tooltip.$m."')\" onmouseout=\"UnTip()\" style='font-size:10px;color:#666666'><img src='info.jpg' width='18' /></span>";
		}
		
		
		
		if ($report_filter!='offen') {
			if ($imp_netto_ext==$imp_netto) {
				$readonly = "style='text-align:right; border:0px;' readonly";
			} else {
				if ($reporting_typ=='e') {$color = "#FFFF00; color:000000";} else {$color = "#FF0000;color:#FFFFFF";}
				$readonly="style='text-align:right; background-color:".$color."; border-top:1px solid silver; border-left:1px solid silver;'";
			}
		} else {
			if ($imp_netto_ext==$imp_netto) {
			
				$readonly="style='text-align:right; background-color:#FFFF99; border-top:1px solid silver; border-left:1px solid silver;'";
			} else {
				$readonly="style='text-align:right; background-color:#FF0000;color:#FFFFFF; border-top:1px solid silver; border-left:1px solid silver;'";
			}
		}

		
		$importe .= "<tr style='background-color:".$bg."'>
						<td align='center'>".$imp_rep_datum_txt."</td>
					 	<td>".$imp_partner."</td>
						<td>".$imp_datum_de."</td>
						<td><span title='".$imp_zeile['IMPORT_DATEI']."'>".$imp_datei."</span></td>
						<td align='right'>".zahl_formatieren($imp_anzahl)."</td>
						<td align='right'>".zahl_formatieren($imp_netto)."</td>
						<td align='right'>".round(($imp_netto/$imp_anzahl)*100,2)."%</td>
						<td align='right'><input type='text' name='ext".$imp_nr."' size='9' value='".$imp_netto_ext."' style='text-align:right; border:0px;background-color:".$bg."' readonly /></td>
						<td align='right'><input type='text' size='8' onkeyup='document.update.ext".$imp_nr.".value=Math.round(".$imp_anzahl."*(this.value/100))' name='quote_".$imp_nr."' value='".$quote_."' ".$readonly." />%</td>
						<td><textarea name='note_".$imp_nr."' cols='15' rows='3'>".$imp_notiz."</textarea></td>";
						
						if ($report_filter=='offen') {
						$importe .=
						"<td align='center' style='line-height:18px'><a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=import&quote=".$quote_."\" style='color#2647A0'>Reporting erstellen</a><br /><a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=import&erledigt=1&rep_typ=e&netto_ext=".$imp_netto_ext."\" style='color#2647A0'>Als erledigt markieren</a></td>";
						}
						
						if ($report_filter=='versendet' && $u_rechte==5) {
						$importe .=
						"<td align='center' style='line-height:18px'>
						 	<a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=import&erledigt=offen\" style='color#2647A0'>Als offen markieren</a><br />".$re_text."</td>";
						}
						
					$importe .= "</tr>";
		$netto_ext[] = $imp_nr;
		$i++;$m++;
	}
	} else {
		$importe = "<tr><td colspan='12'>- Keine Daten vorhanden -</td></tr>";
	}
	
	
	if ($_POST['submit_import']) {
		foreach ($netto_ext as $impnr) {
				$import_nummer = $impnr;
				$imp_anzahl_sql = mysql_query("SELECT * FROM $import_datei_db WHERE IMPORT_NR2='$import_nummer'",$verbindung);
				$imp_datei_anzahl = mysql_num_rows($imp_anzahl_sql);
				
				$neu_netto_ext = $_POST['ext'.$impnr];
				
				if ($imp_datei_anzahl>1) {
					$neu_netto_ext = $neu_netto_ext/$imp_datei_anzahl;
				}
				$notiz = $_POST["note_".$impnr];
				$update_sql = mysql_query("UPDATE $import_datei_db
										   SET NETTO_EXT='$neu_netto_ext', NOTIZ='$notiz'
										   WHERE IMPORT_NR2 = '$import_nummer'", $verbindung);
		}
		
		print "<script language='javascript' type='text/javascript'>
					window.location.href = \"index.php?import_nr=".$import_nr."&report=sponsoring&report_filter=".$report_filter."\"
				</script>";
		
	}
	
	/*
	if (($_POST['submitmerge'] || isset($_POST['submitmerge_x'])) && sizeof($_POST['merge'])>=2) {
		
		$rep_id_query = mysql_query("SELECT REPORTING_ID FROM $db_name.$import_datei_db ORDER BY REPORTING_ID DESC");
		$rep_id_holen = mysql_fetch_array($rep_id_query);
		$rep_id = $rep_id_holen['REPORTING_ID'];
		$rep_id++;
		
		$merge_dateien = $_POST['merge'];
		foreach ($merge_dateien as $imp_nr) {
			$update_sql = mysql_query("UPDATE $db_name.$import_datei_db
									   SET REPORTING_ID='$rep_id'
									   WHERE IMPORT_NR = '$imp_nr'", $verbindung);
		}
	
	}
	*/
}

function realtime_raussuchen() {
	global $db_name;
	global $verbindung;
	global $mandant;
	global $import_jahr;
	global $import_monat;
	global $import_partner;
	global $anrede_db;
	global $herkunft_db;
	global $import_datei_db;
	global $partner_db;
	global $metaDB;
	global $temp_db;
	global $metaDB_temp;
	global $netto_ext;
	global $report_filter;
	global $u_rechte;
	global $importe;
	include('db_optdb_connect.inc.php');
	switch ($report_filter) {
		case "versendet" : $sql_ = "AND Report.reporting_datum != '0000-00-00'";break;
		case "offen" : $sql_ = "AND Report.reporting_datum = '0000-00-00'";break;
	}
	
	if ($import_monat>=1) {
		$sql_monat = " AND MONTH(Report.zeitraum) = '$import_monat'";
	}

	if ($import_partner!='') {
		$sql_partner = " AND Partner.PID = '$import_partner'";
	}
	
	$rt_daten = mysql_query("SELECT * 
							 FROM Report, Partner
							 WHERE YEAR(Report.zeitraum)='$import_jahr' ".$sql_.$sql_monat.$sql_partner."
							 AND Report.pid=Partner.PID
							 AND BRUTTO >= 15
							 ORDER BY Report.zeitraum DESC",$verbindung_optdb);
	$rt_vorhanden = mysql_num_rows($rt_daten);
	
	if ($rt_vorhanden) {
	$i=1;$m=1;
	while ($rt_zeile = mysql_fetch_array($rt_daten)) {
	
		if ($i==2) {$bg = "#EDF5FF";} else {$bg = "#FFFFFF";$i=1;}; 
		$imp_partner = $rt_zeile['Pkz'];
		$imp_datum = $rt_zeile['zeitraum'];
		$imp_datum_teile = explode("-", $imp_datum);
		$imp_datum_de = $imp_datum_teile[0]."-".$imp_datum_teile[1];
		
		$geliefert_datum = $rt_zeile['zeitraum'];
		$geliefert_teile = explode("-", $geliefert_datum);
		$geliefert_datum_de = $geliefert_teile[2].".".$geliefert_teile[1].".".$geliefert_teile[0];
		$imp_nr = $rt_zeile['id'];
		$imp_anzahl = $rt_zeile['brutto'];
		$imp_netto = $rt_zeile['netto'];
		$imp_notiz = $rt_zeile['notiz'];
		$imp_netto_ext = $rt_zeile['netto_ext'];
		$imp_rep_datum = $rt_zeile['reporting_datum'];
		$absender = $rt_zeile['reporting_absender'];
		$reporting_typ =  $rt_zeile['reporting_typ'];
		$details = $rt_zeile['details'];
		$details = str_replace("|", "<br />", $details );
		$quote_ = round(($imp_netto_ext/$imp_anzahl)*100,1);
		
		$re = $rt_zeile['RE'];
		if ($re==0) {
			$re_text = "<a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=realtime&re=erhalten\" style='color#2647A0'>Rechnung erhalten</a>";
		} 
		
		if ($re==1) {
			$re_text = '<div style="font-size:9px; color:#009900; float:left;">(Rechnung erhalten)</div>';
		} 
		
		$kom_zahl = $imp_netto_ext;
		$tooltip.$m = "<table width=\'auto\' cellspacing=\'0\' cellpadding=\'3\' border=\'1\' bordercolor=\'#CCCCCC\' style=\'border-collapse:collapse\'><tr><td align=\'left\'>versendet am: ".$imp_rep_datum."</td></tr><tr><td align=\'left\'>Absender: ".$absender."</td></tr><tr><td align=\'left\'>Kommunizierte Zahl: ".$kom_zahl."</td></tr></table>";
				
				
				if ($imp_rep_datum == '0000-00-00 00:00:00') {
					$imp_rep_datum_txt = '<span style="color:#FF0000">offen</span>';
				} else {
					$imp_rep_datum_txt = "<span onmouseover=\"Tip('".$tooltip.$m."')\" onmouseout=\"UnTip()\" style='font-size:10px;color:#666666'><img src='info.jpg' width='18' /></span>";
				}
				
				if ($report_filter!='offen') {
							if ($imp_netto_ext==$imp_netto) {
								$readonly = "style='text-align:right; border:0px;' readonly";
							} else {
								if ($reporting_typ=='e') {$color = "#FFFF00; color:000000";} else {$color = "#FF0000;color:#FFFFFF";}
								$readonly="style='text-align:right; background-color:".$color."; border-top:1px solid silver; border-left:1px solid silver;'";
							}
						} else {
							if ($imp_netto_ext==$imp_netto) {
								$readonly="style='text-align:right; background-color:#FFFF99; border-top:1px solid silver; border-left:1px solid silver;'";
							} else {
								$readonly="style='text-align:right; background-color:#FF0000;color:#FFFFFF; border-top:1px solid silver; border-left:1px solid silver;'";
							}
						}	
						
				$importe .= "<tr style='background-color:".$bg."'>
								<td align='center'>".$imp_rep_datum_txt."</td>
							 	<td>".$imp_partner."</td>
								<td align='center'>".$imp_datum_de."</td>
								<td align='left'>".$details."</td>
								<td align='right'>".zahl_formatieren($imp_anzahl)."</td>
								<td align='right'>".zahl_formatieren($imp_netto)."</td>
								<td align='right'>".round(($imp_netto/$imp_anzahl)*100,2)."%</td>
								<td align='right'><input type='text' name='rt_".$imp_nr."' size='9' style='text-align:right; border:0px;background-color:".$bg."' readonly value='".$imp_netto_ext."' /></td>
								<td align='right'><input type='text' size='8' onkeyup='document.update.rt_".$imp_nr.".value=Math.round(".$imp_anzahl."*(this.value/100))' name='quote_".$imp_nr."' value='".$quote_."' ".$readonly." />%</td>
								<td><textarea name='rt_note_".$imp_nr."' cols='15' rows='3'>".$imp_notiz."</textarea></td>";
								
								if ($report_filter=='offen') {
									$importe .= "<td align='center' style='line-height:18px'><a href='action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=rt&quote=".$quote_."'>Reporting erstellen</a><br /><a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=rt&erledigt=1&rep_typ=e&quote=".$quote_."\" style='color#2647A0'>Als erledigt markieren</a></td>";
								}
								
								if ($report_filter=='versendet' && $u_rechte==5) {
									$importe .=	"<td align='center' style='line-height:18px'><a href=\"action.php?import_nr=".$imp_nr."&step=report_erstellen&gen_art=rt&erledigt=offen\" style='color#2647A0'>Als offen markieren</a><br />".$re_text."</td>";
								}
								$importe .= "</tr>";
				$netto_ext[] = $imp_nr;
				$i++;$m++;
		}
	}	
			else {
				$importe = "<tr><td colspan='12'>- Keine Daten vorhanden -</td></tr>";
			}
		
						
			if ($_POST['submit_rt']) {
				foreach ($netto_ext as $impnr) {
						$import_nummer = $impnr;
						$neu_netto_ext = $_POST["rt_".$impnr];
						$notiz = $_POST["rt_note_".$impnr];
						$update_sql = mysql_query("UPDATE Report 
												   SET netto_ext='$neu_netto_ext', notiz='$notiz'
												   WHERE id = '$import_nummer'", $verbindung_optdb);
						}
		
				print "<script language='javascript' type='text/javascript'>
							window.location.href = \"index.php?import_nr=".$import_nr."&report=sponsoring&report_filter=".$report_filter."\"
						</script>";
			}
			
}

function monat_select($imp_monat) {
	$i = 0;
	$monat_array = array("Monat -alle-","Januar","Februar","M�rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
	
	switch ($imp_monat) {
		case "1" : $imp_monat = "Januar";break;
		case "2" : $imp_monat = "Februar";break;
		case "3" : $imp_monat = "M�rz";break;
		case "4" : $imp_monat = "April";break;
		case "5" : $imp_monat = "Mai";break;
		case "6" : $imp_monat = "Juni";break;
		case "7" : $imp_monat = "Juli";break;
		case "8" : $imp_monat = "August";break;
		case "9" : $imp_monat = "September";break;
		case "10" : $imp_monat = "Oktober";break;
		case "11" : $imp_monat = "November";break;
		case "12" : $imp_monat = "Dezember";break;
		case "0" : $imp_monat = "Monat -alle-";
	}
	foreach ($monat_array as $monat) {
		$option .= "<option value='".$i."' ";
				if ($imp_monat==$monat) {$option .= "selected";}
		$option .= ">".$monat."</option>";
		$i++;
	}
	return $option;
}

function jahr_select() {
	global $mandant;
	global $import_jahr;
	global $verbindung;
	global $import_datei_db;
	
	$jahre_sql = mysql_query("SELECT DISTINCT YEAR(`IMPORT_DATUM`) AS JAHR FROM $import_datei_db ORDER BY JAHR desc", $verbindung);
	while ($zeile = mysql_fetch_row($jahre_sql)) {
		$option .= "<option ";
			if ($zeile[0]==$import_jahr) {$option .= "selected";}
		$option .= ">".$zeile[0]."</option>";
	}
	return $option;
}

function partner_select() {
	global $mandant;
	include('db_connect.inc.php');
	global $genart;
	global $import_partner;
	if ($genart!='Realtime') {$genart2 = "OR GenArt=''";}
	
		$partner_sql = mysql_query("SELECT * FROM $partner_db WHERE GenArt='$genart' ".$genart2." ORDER BY Pkz asc", $verbindung);
		$option .= "<option value='' ";
			if (!$_POST['GenArt']) {$option .= "selected";}
		$option .= ">Partner -alle-</option>";
		while ($zeile = mysql_fetch_array($partner_sql)) {
			$pid = $zeile['PID'];
			$gen_art = $zeile['GenArt'];
			$option .= "<option value='".$pid."' ";
				if ($pid==$import_partner) {$option .= "selected";}
			$option .= ">".$zeile['Pkz']." (".$pid.")</option>";
	}
	return $option;
}

function filter_select() {
	global $report_filter;
	$option_filter = "<option value='offen' ";
							if ($report_filter=='offen') {$option_filter .= "selected";}
	$option_filter .= ">offen</option>";
	$option_filter .= "<option value='versendet' ";
							if ($report_filter=='versendet') {$option_filter .= "selected";}
	$option_filter .= ">versendet</option>";
	$option_filter .= "<option value='alle' ";
							if ($report_filter=='alle') {$option_filter .= "selected";}
	$option_filter .= ">Alle</option>";
	return $option_filter;
}

$rep_daten = mysql_query("SELECT SUM(BRUTTO) AS brutto, SUM(NETTO) AS netto, SUM(DUBLETTEN) AS dubletten, SUM(LANGADR) AS langadr, SUM(BOUNCES) AS bounces
						  FROM $import_datei_db
						  WHERE YEAR(IMPORT_DATUM) = '$import_jahr'
							", $verbindung);
$rep_zeile = mysql_fetch_array($rep_daten);
$rep_brutto = $rep_zeile['brutto'];
$rep_netto = $rep_zeile['netto'];
$rep_dubletten = $rep_zeile['dubletten'];
$rep_langadr = $rep_zeile['langadr'];
$rep_bounces = $rep_zeile['bounces'];
$rep_brutto_formatiert = number_format($rep_brutto, 0,0,'.');
$q_meta_db = round(($rep_dubletten/$rep_brutto)*100,1);
$q_bounces = round(($rep_bounces/$rep_brutto)*100,1);

$rep_daten_all = mysql_query("SELECT SUM(BRUTTO) AS brutto, SUM(NETTO) AS netto, SUM(DUBLETTEN) AS dubletten, SUM(LANGADR) AS langadr, SUM(BOUNCES) AS bounces
						  	  FROM $import_datei_db
							 ", $verbindung);
$rep_zeile_all = mysql_fetch_array($rep_daten_all);
$rep_brutto_all = $rep_zeile_all['brutto'];
$rep_netto_all = $rep_zeile_all['netto'];
$rep_dubletten_all = $rep_zeile_all['dubletten'];
$rep_langadr_all = $rep_zeile_all['langadr'];
$rep_bounces_all = $rep_zeile_all['bounces'];
$rep_brutto_all_formatiert = number_format($rep_brutto_all, 0,0,'.');
$q_meta_db_all = round(($rep_dubletten_all/$rep_brutto_all)*100,1);
$q_bounces_all = round(($rep_bounces_all/$rep_brutto_all)*100,1);

	#include('menu.inc.php');
	if (isset($import_del_confirm)) {print $import_del_confirm;}


if ($_GET['email'] == 1) {
	$info = "
	<div style='padding-top:15px;padding-bottom:15px'>
		<div style='border:1px solid #0066FF;color:#0066FF;padding:5px;text-align:center;width:250px;font-weight:bold;'>- Report wurde versendet -</div>
	</div>";
} elseif ($_GET['email'] == "speichern") {
	$info = "
	<div style='padding-top:15px;padding-bottom:15px'>
		<div style='border:1px solid #0066FF;color:#0066FF;padding:5px;text-align:center;width:250px;font-weight:bold;'>- Report wurde gespeichert -</div>
	</div>";
} elseif ($_GET['email'] == "offen") {
	$info = "
	<div style='padding-top:15px;padding-bottom:15px'>
		<div style='border:1px solid #0066FF;color:#0066FF;padding:5px;text-align:center;width:250px;font-weight:bold;'>- Report wurde als offen markiert -</div>
	</div>";
}

/*
for ($i=0; $i<1000; $i++)
{
mysql_query("update $import_datei_db SET REPORTING_ID = '$i' where IMPORT_NR = '$i'",$verbindung);
}
*/
?>
<form name="update" action="<?php echo $server['self'];?>" method="POST">
	<select name="import_monat">
		<?php print monat_select($import_monat);?>
	</select>
	<select name="import_jahr">
		<?php print jahr_select();?>
	</select>
	<select name="report_filter">
		<?php print filter_select();?>
	</select>
	<select name="import_partner">
		<?php print partner_select();?>
	</select>
    <?php
	if ($mandant=='intone') {
		print '
		<select name="GenArt">
			<option value="Import" ';
			 if ($genart=="Import") {print "selected";}
		print '>Import</option>
			<option value="Realtime" ';
			if ($genart=="Realtime") {print "selected";}
		print '>Realtime</option>
		</select>';
    }
    ?>
	<input type="hidden" name="report" value="sponsoring" />
	<input type="submit" name="aktualisieren" value="aktualisieren" />
<br /><br />
<?php
if ($report_filter!='offen') {
print '
<div style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#333333; width:100%;float:left; padding-bottom:5px;">
	<div style="width:15px;height:15px;background-color:red; float:left;">&nbsp;</div><div style="float:left">&nbsp;= versendet&nbsp;&nbsp;</div> <div style="width:15px;height:15px;background-color:#FFFF00; float:left;">&nbsp;</div>&nbsp;= erledigt
</div>';
}
?>
<div style="float:left;font-size:11px;">
	<?php print $info;
			if ($genart=='Import') {	print '
	<table cellspacing="0" cellpadding="3" border="1" bordercolor="#CCCCCC" style="border-collapse:collapse">
		<tr>
			<td colspan="111" align="center" bgcolor="#666666"><font color="#FFFFFF" size="2"><b>Importe</b></font></td>
		</tr>
		<tr class="ueberschrift">
			<td align="center">Status</td>
			<td>Partner</td>
			<td>Lieferdatum</td>
			<td style="font-family:">Import Datei</td>
			<td align="center">Datens�tze<br />(Brutto)</td>
			<td align="center">Datens�tze <br />(Netto)</td>
			<td align="center">Quote</td>
			<td align="center">Netto EXT</td>
			<td align="center">Quote EXT</td>
			<td align="center">Notiz</td>';
			 if ($report_filter=='offen') {print '<td align="right">�nderungen <input type="submit" name="submit_import" value="speichern" />&nbsp;</td>';}
		print '</tr>'.$importe.'</table>';
}
			if ($genart=='Realtime') {
			echo '
				<table cellspacing="0" cellpadding="3" border="1" bordercolor="#CCCCCC" style="border-collapse:collapse">
			<tr>
				<td colspan="11" align="center" bgcolor="#333333"><font color="#FFFFFF" size="2"><b>Realtime</b></font></td>
			</tr>
			<tr class="ueberschrift">
				<td align="center">Status</td>
				<td>Partner</td>
				<td align="center">Lieferdatum</td>
				<td align="center">Details</td>
				<td align="center">Datens�tze<br />(Brutto)</td>
				<td align="center">Datens�tze <br />(Netto)</td>
				<td align="center">Quote</td>
				<td align="center">Netto EXT</td>
				<td align="center">Quote EXT</td>
				<td align="center">Notiz</td>';
				 if ($report_filter=='offen') {print '<td align="right">�nderungen <input type="submit" name="submit_rt" value="speichern" />&nbsp;</td></tr>';}
				print $importe;
			}
		?>
	</table><br />
</div>
</form>