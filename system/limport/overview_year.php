<?php
session_start();
$mandant = $_SESSION['mandant'];
require_once('../library/chart.php');

if ($_GET['jahr']) {
	$jahr = $_GET['jahr'];
} else {
	$jahr = date("Y");
}

function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}

function getMonat($m) {

	switch ($m) {
		case 1: return "Jan";
		case 2: return "Feb";
		case 3: return "M&auml;rz";
		case 4: return "Apr";
		case 5: return "Mai";
		case 6: return "Juni";
		case 7: return "Juli";
		case 8: return "Aug";
		case 9: return "Sep";
		case 10: return "Okt";
		case 11: return "Nov";
		case 12: return "Dez";
	}
	
}


function getCnt($type) {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	if ($type != 'k') {
		$sql = "AND abrechnungsart = '$type' ";
	}
	
	$k_sql = mysql_query("SELECT *
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1 ".$sql,$verbindung);
	$cnt = mysql_num_rows($k_sql);
	if ($cnt == '') {$cnt = 0;}	
	return $cnt;	  
}

function getQuote($brutto,$netto) {
	$rnd = round((($netto/$brutto)*100),1);
	return $rnd;
}


function kVolumen() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$k_sql = mysql_query("SELECT MONTH(GELIEFERT_DATUM) as Monat
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
					      GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' xAxisName='Monat' yAxisName='Volumen' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$cat = "<categories>";
	$data2 = "<dataset seriesName='Brutto' color='267CE6'>";
	$data3 = "<dataset seriesName='Netto' color='9900FF'>";
	
	 
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$monat = getMonat($kz['Monat']);
		
		$cat .= "<category label='".$monat."' />";
		
		$nv_sql = mysql_query("SELECT SUM(BRUTTO) as B, SUM(NETTO) as N
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
					      AND MONTH(GELIEFERT_DATUM) = '".$kz['Monat']."'
						  ",$verbindung);
		$nvz = mysql_fetch_array($nv_sql);
		$data2 .= "<set value='".$nvz['B']."' color='267CE6' />";
		$data3 .= "<set value='".$nvz['N']."' color='9900FF' />";
	}
	
	$data .= $cat."</categories>";
	$data .= $data2."</dataset>";
	$data .= $data3."</dataset>";
	$data .= "</chart>";
	return createChart::renderChart('idkOVv',$data,'MSLine','780','200');
}


function kAB() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$k_sql = mysql_query("SELECT abrechnungsart as a, COUNT(*) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY a",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$a = $kz['a'];
		$data .= "<set value='".$kz['Anzahl']."' label='".$a."' />";
		
	}
	
	$data .= "</chart>";
	return createChart::renderChart('idkAB',$data,'Pie3D','375','178');
}

function getASP_name($asp) {
	switch ($asp) {
		case "BM": return "Broadmail";break;
		case "BC": return "Backclick";break;
		case "E": return "Elaine";break;
		case "K": return "Kajomi";break;
	}
}

function kVS() {
	global $mandant;
	global $jahr;
	global $gesamt_volumen;
	global $gesamt_netto;
	global $tr_volumen;
	global $tr_performance;
	global $data2_chart;
	
	include('../db_connect.inc.php');
	
	$cnt_sql = mysql_query("SELECT SUM(BRUTTO) as gsmt, SUM(NETTO) as netto
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
						  ",$verbindung);
	$gsmtz = mysql_fetch_array($cnt_sql);
	$gesamt_volumen = $gsmtz['gsmt'];
	$gesamt_netto = $gsmtz['netto'];
						  
	$k_sql = mysql_query("SELECT p.PID as pid, p.Pkz as pkz, SUM(i.BRUTTO) as b
						  FROM $import_datei_db as i, $partner_db as p
						  WHERE YEAR(i.GELIEFERT_DATUM) = '$jahr'
						  AND i.IMPORT_NR = i.IMPORT_NR2
						  AND i.PARTNER = p.PID
						  GROUP BY p.PID 
						  ORDER BY b DESC
						  ",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$data2 = "<chart canvasBorderColor='999999' labelDisplay='Stagger' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$data2_cat = "<categories>";
	$data2_dataset = "<dataset seriesName='Brutto' color='267CE6'>";
	$data2_dataset2 = "<dataset seriesName='Netto' color='9900FF'>";
		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$pkz = $kz['pkz'];
		$pid = $kz['pid'];
		$nr = $kz['nr'];
		
		$cnt_asp_sql = mysql_query("SELECT SUM(BRUTTO) as B, SUM(NETTO) as N
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
						  AND PARTNER = '$pid'
						  ",$verbindung);
		$kz2 = mysql_fetch_array($cnt_asp_sql);		
		$gesamt_o = $kz2['B'];		
		$gesamt_k = $kz2['N'];		  
		
		if ($gesamt_o > 0) {
		
		
			$data .= "<set value='".$gesamt_o."' label='".$pkz."' />";
			$data2_dataset .= "<set value='".$gesamt_o."' color='267CE6' />";
			$data2_dataset2 .= "<set value='".$gesamt_k."' color='9900FF' />";
			
			$data2_cat .= "<category label='".$pkz."' />";
			
			$tr_volumen .= '<tr class="ovTR1">
					<td class="ovTR1_label">'.$pkz.'</td>
					<td class="ovTR1_label2">'.zahl_format($gesamt_o).'</td>
					<td class="ovTR1_label3">'.getQuote($gesamt_volumen,$gesamt_o).'%</td>
				</tr>';
				
			$tr_performance .= '<tr class="ovTR1">
					<td class="ovTR1_label">'.$pkz.'</td>
					<td class="ovTR1_label2">'.getQuote($gesamt_o,$gesamt_k).'%</td>
				</tr>';
		}
		
		
	}
	
	$data .= "</chart>";
	
	
	$data2_cat .= "</categories>";
	$data2_dataset .= "</dataset>";
	$data2_dataset2 .= "</dataset>";
	$data2 .= $data2_cat.$data2_dataset.$data2_dataset2."</chart>";
	$data2_chart = createChart::renderChart('idkVSp',$data2,'MSCombi3D','378','300');
	
	return createChart::renderChart('idkVS',$data,'Pie3D','378','300');
}

function getASPquote() {
	global $mandant;
	global $jahr;
	global $gesamt_versendet;
	global $gesamt_openings;
	global $gesamt_klicks;
	include('../db_connect.inc.php');

	$cnt_asp_sql = mysql_query("SELECT SUM(versendet) as gsmt, SUM(openings) as o, SUM(klicks) as k
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  ",$verbindung);
						  
	$kz2 = mysql_fetch_array($cnt_asp_sql);
		$gesamt_versendet = $kz2['gsmt'];
		$gesamt_openings = $kz2['o'];		
		$gesamt_klicks = $kz2['k'];		
}

getASPquote();
?>

<div class="ovHeader">
	&Uuml;bersicht<br />
	<span style="font-size:18px;color:#9900FF">Datenlieferungen - <?php print $jahr; ?></span>
</div>

<div style="width:850px;margin:auto;">
<div id="kOV">
	
	
    <div class="kOV_table">
    	<table class="ovTable" width="800">
        	<tr>
            	<td class="ovBlock">Lieferungen</td>
            </tr>
        	<tr>
            	<td ><?php print kVolumen(); ?></td>
            </tr>
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovBlock">Lieferanten / Volumen</td>
            </tr>
        	<tr>
            	<td><?php print kVS(); ?></td>
            </tr>     	
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="150">
        	<tr>
            	<td class="ovBlock">Lieferanten / Quote</td>
            </tr>
        	<tr>
            	<td><?php print $data2_chart; ?></td>
            </tr>     	
        </table>
    </div>
		
	<div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovTR" width="100">Lieferant</td>
                <td class="ovTR" align="right">Geliefert</td>
                <td class="ovTR" align="right">Quote</td>
            </tr>
            <?php print $tr_volumen; ?>
			<tr class="ovTR1">
            	<td class="ovTR">Gesamt</td>
                <td class="ovTR" align="right"><?php print zahl_format($gesamt_volumen); ?></td>
                <td class="ovTR"></td>
            </tr>
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovTR" width="100">Lieferant</td>
                <td class="ovTR" align="right">Netto Quote</td>
            </tr>
            <?php print $tr_performance; ?>
			<tr class="ovTR1">
            	<td class="ovTR">Gesamt</td>
                <td class="ovTR" align="right">&#216; <?php print getQuote($gesamt_volumen,$gesamt_netto); ?>%</td>
            </tr>
        </table>
    </div>
	
</div>
</div>
<div class="ovFooter"></div>
