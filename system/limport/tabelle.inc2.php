<?php
header('Content-Type: text/xml; charset=ISO-8859-1');

/**
 * getMonth
 * 
 * @param   integer $monthNumber
 * 
 * @return  string
 */
function getMonth($monthNumber) {
    $monthDataArray = array(
        1 => 'Januar',
        2 => 'Februar',
        3 => 'M&auml;rz',
        4 => 'April',
        5 => 'Mai',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'August',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Dezember'
    );
    
    return $monthDataArray[$monthNumber];
}


/**
 * getImporteMainDataArray
 * 
 * @param   string  $importDateiDb
 * @param   integer $importNumber
 * @param   resource    $verbindung
 * @param   debugLogManager $debugLogManager
 * 
 * @return  null|array
 */
function getImporteMainDataArray($importDateiDb, $importNumber, $verbindung, debugLogManager $debugLogManager) {
    $dataArray = null;
    $res = mysql_query(
        'SELECT SUM(`BRUTTO`) AS `brutto`, SUM(`NETTO`) AS `netto`, SUM(`NETTO_EXT`) AS `netto_ext`, SUM(`BOUNCES`) AS `bounces`, SUM(`METADB`) AS `metadb`' 
                . ', SUM(`BLACKLIST`) AS `blacklist`, SUM(`DUBLETTEN`) AS `doubletten`, SUM(`DUBLETTE_INDATEI`) AS `intradoubletten`, SUM(`FAKE`) AS `fake`' 
            . ' FROM `' . $importDateiDb . '`' 
            . ' WHERE `IMPORT_NR2` = ' . intval($importNumber)
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $dataArray;
}


/**
 * getCountFromImportTableByImportNr2
 * 
 * @param   string  $importDateiDb
 * @param   integer $importNumber
 * @param   resource    $verbindung
 * @param   debugLogManager $debugLogManager
 * 
 * @return  integer
 */
function getCountFromImportTableByImportNr2($importDateiDb, $importNumber, $verbindung, debugLogManager $debugLogManager) {
    $result = 0;
    $res = mysql_query(
        'SELECT count(1) as `count`' 
            . ' FROM `' . $importDateiDb . '`' 
            . ' WHERE `IMPORT_NR2` = ' . intval($importNumber)
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray['count']);
        
        $result = $dataArray['count'];
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return intval($result);
}


/**
 * getMonthDataArray
 * 
 * @param   string  $importDateiDb
 * @param   integer $deliveryYear
 * @param   integer $deliveryMonth
 * @param   resource    $verbindung
 * @param   debugLogManager $debugLogManager
 * 
 * @return  null|array
 */
function getMonthDataArray($importDateiDb, $deliveryYear, $deliveryMonth, $verbindung, debugLogManager $debugLogManager) {
    $dataArray = null;
    $res = mysql_query(
        'SELECT SUM(`BRUTTO`) AS `brutto`, SUM(`NETTO`) AS `netto`, SUM(`NETTO_EXT`) AS `netto_ext`, SUM(`BOUNCES`) AS `bounces`, SUM(`METADB`) AS `metadb`' 
                . ', SUM(`BLACKLIST`) AS `blacklist`, SUM(`DUBLETTEN`) AS `doubletten`, SUM(`DUBLETTE_INDATEI`) AS `intradoubletten`, SUM(`FAKE`) AS `fake`' 
            . ' FROM `' . $importDateiDb . '`' 
            . ' WHERE YEAR(`GELIEFERT_DATUM`) = ' . intval($deliveryYear) 
                . ' AND MONTH(`GELIEFERT_DATUM`) = ' . intval($deliveryMonth) 
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $dataArray;
}


/**
 * getImportDataArray
 * 
 * @param   string  $importDateiDb
 * @param   string  $partnerDb
 * @param   string  $felderPartnerDB
 * @param   integer $partnerId
 * @param   integer $deliveryYear
 * @param   integer $deliveryMonth
 * @param   resource    $verbindung
 * @param   debugLogManager $debugLogManager
 * 
 * @return  null|array
 */
function getImportDataArray($importDateiDb, $partnerDb, $felderPartnerDB, $partnerId, $deliveryYear, $deliveryMonth, $verbindung, debugLogManager $debugLogManager) {
    $dataArray = null;
    $res = mysql_query(
        'SELECT *' 
            . ' FROM `' . $importDateiDb . '` as `i`, `' . $partnerDb . '` as `p`' 
            . ' WHERE `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`' 
                . ' AND `p`.`' . $felderPartnerDB['PID'] . '` = ' . intval($partnerId) 
                . ' AND YEAR(`i`.`GELIEFERT_DATUM`) = ' . intval($deliveryYear) 
                . ' AND MONTH(`i`.`GELIEFERT_DATUM`) = ' . intval($deliveryMonth) 
                . ' AND `i`.`IMPORT_NR` = `i`.`IMPORT_NR2`' 
            . ' ORDER BY `i`.`GELIEFERT_DATUM` desc'
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = array();
        while (($row = mysql_fetch_assoc($res)) !== false) {
            $dataArray[] = $row;
        }
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $dataArray;
}


/**
 * getAddDataArrayByImportNumber
 * 
 * @param   string  $importDateiDb
 * @param   integer $importNumber
 * @param   resource    $verbindung
 * @param   debugLogManager $debugLogManager
 * 
 * @return  null|array
 */
function getAddDataArrayByImportNumber($importDateiDb, $importNumber, $verbindung, debugLogManager $debugLogManager) {
    $dataArray = null;
    $res = mysql_query(
        'SELECT *' 
            . ' FROM `' . $importDateiDb . '`' 
            . ' WHERE `IMPORT_NR2` = ' . intval($importNumber)
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = array();
        while (($row = mysql_fetch_assoc($res)) !== false) {
            $dataArray[] = $row;
        }
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $dataArray;
}


/**
 * suche_offene
 * 
 * @global  string  $mandant
 * 
 * @param   integer $partnerId
 * @param   integer $month
 * @param   integer $year
 * @param   debugLogManager $debugLogManager
 * 
 * @return  string
 */
function suche_offene($partnerId, $month, $year, debugLogManager $debugLogManager) {
    global $mandant;
    
    include('db_connect.inc.php');
    
    if ((int) $_SESSION['testimport'] === 1) {
        $metaDB = $metaDB . '_test';
        $import_datei_db = $import_datei_db . '_test';
    }

    $result = 'green';
    $res = mysql_query(
        'SELECT count(1) as `count`' 
            . ' FROM `' . $import_datei_db . '`' 
            . ' WHERE `PARTNER` = ' . (int) $partnerId 
                . ' AND YEAR(`GELIEFERT_DATUM`) = ' . intval($year) 
                . ' AND MONTH(`GELIEFERT_DATUM`) = ' . intval($month) 
                . ' AND `IMPORT_NR` = "IMPORT_NR2"' 
                . ' AND `STATUS` < 3'
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray['count']);

        if (intval($dataArray['count'])) {
            $result = '<span style="color:red">' . $dataArray['count'] . ' offen</span>';
        }
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $result;
}


function importe_anzahl($partnerId, $month, $year, debugLogManager $debugLogManager) {
    global $mandant;
    
    include('db_connect.inc.php');
    
    if ((int) $_SESSION['testimport'] === 1) {
        $metaDB = $metaDB . '_test';
        $import_datei_db = $import_datei_db . '_test';
    }

    $result = '';
    $res = mysql_query(
        'SELECT count(1) as `count`' 
            . ' FROM `' . $import_datei_db .'`' 
            . ' WHERE `PARTNER` = ' . (int) $partnerId 
                . ' AND YEAR(`GELIEFERT_DATUM`) = ' . mysql_real_escape_string($year) 
                . ' AND MONTH(`GELIEFERT_DATUM`) = ' . mysql_real_escape_string($month) 
                . ' AND `IMPORT_NR` = `IMPORT_NR2`'
        ,
        $verbindung
    );
    if ($res) {
        $dataArray = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData(__FUNCTION__, $dataArray['count']);

        if (intval($dataArray['count'])) {
            $result = '<span style="color:#999999;" title="' . $dataArray['count'] . ' Datei(en)">(' . $dataArray['count'] . ')</span>';
        }
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $result;
}


/**
 * details_footer
 * 
 * @global  string  $mandant
 * @param   integer $import_jahr
 * @param   debugLogManager $debugLogManager
 * 
 * @return  array
 */
function details_footer($import_jahr, debugLogManager $debugLogManager) {
    global $mandant;
    include('db_connect.inc.php');
    
    $resultDataArray = array();
    
    if ((int) $_SESSION['testimport'] === 1) {
        $metaDB = $metaDB . '_test';
        $import_datei_db = $import_datei_db . '_test';
    }

    $res = mysql_query(
        'SELECT SUM(`BRUTTO`) as `b`, SUM(`NETTO`) as `n`' 
            . ' FROM `' . $import_datei_db . '`' 
            . ' WHERE YEAR(`GELIEFERT_DATUM`) = ' . intval($import_jahr)
        ,
        $verbindung
    );
    if ($res) {
        $z = mysql_fetch_assoc($res);
        mysql_free_result($res);
        
        // debug
        $debugLogManager->logData('z', $z);
        
        $resultDataArray['brutto'] = (int) $z['b'];
        $resultDataArray['netto'] = (int) $z['n'];
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> res', mysql_error($verbindung));
    }

    $res2 = mysql_query(
        'SELECT count(1) as `count`' 
            . ' FROM `' . $import_datei_db . '`' 
            . ' WHERE YEAR(`GELIEFERT_DATUM`) = ' . intval($import_jahr) 
                . ' AND `IMPORT_NR` = `IMPORT_NR2`'
        ,
        $verbindung
    );
    if ($res2) {
        $dataArray = mysql_fetch_assoc($res2);
        mysql_free_result($res2);
        
        $resultDataArray['gesamt'] = (int) $dataArray['count'];
        
        // debug
        $debugLogManager->logData('countDataArray', $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> res2', mysql_error($verbindung));
    }

    $res3 = mysql_query(
        'SELECT count(1) as `count`' 
            . ' FROM `' . $import_datei_db . '`' 
            . ' WHERE YEAR(`GELIEFERT_DATUM`) = ' . intval($import_jahr) 
            . ' AND `IMPORT_NR` = `IMPORT_NR2`' 
            . ' AND `STATUS` < 3'
        ,
        $verbindung
    );
    if ($res3) {
        $dataArray = mysql_fetch_assoc($res3);
        mysql_free_result($res3);
        
        $resultDataArray['open'] = (int) $dataArray['count'];
        
        // debug
        $debugLogManager->logData('openDataArray', $dataArray);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__ . ' -> res3', mysql_error($verbindung));
    }
    
    return $resultDataArray;
}


/**
 * importe_raussuchen
 * 
 * @global  string  $mandant
 * @param   integer $import_jahr
 * @param   debugLogManager $debugLogManager
 * 
 * @return  string
 */
function importe_raussuchen($import_jahr, debugLogManager $debugLogManager) {
    global $mandant;
    
    include('db_connect.inc.php');
    
    if ((int) $_SESSION['testimport'] === 1) {
        $metaDB = $metaDB . '_test';
        $import_datei_db = $import_datei_db . '_test';
    }
    
    $importe = '';
    $i = 1;
    $imp_ges = mysql_query(
        'SELECT `p`.`' . $felderPartnerDB['Pkz'] . '` as `Pkz`, `i`.`PARTNER`, MONTH(`GELIEFERT_DATUM`) as `monat`' 
                . ', SUM(`BRUTTO`) AS `brutto_total`, SUM(`NETTO`) AS `netto_total`, SUM(`BOUNCES`) AS `bounces_total`, SUM(`METADB`) AS `metadb_total`' 
                . ', SUM(`BLACKLIST`) AS `blacklist_total`, SUM(`DUBLETTEN`) AS `doubletten_total`, SUM(`DUBLETTE_INDATEI`) AS `intradoubletten_total`, SUM(`FAKE`) AS `fake_total`' 
            . ' FROM `' . $import_datei_db . '` as `i`, `' . $partner_db . '` as `p`' 
            . ' WHERE YEAR(`i`.`GELIEFERT_DATUM`) = ' . intval($import_jahr) 
                . ' AND `i`.`PARTNER` = `p`.`' . $felderPartnerDB['PID'] . '`' 
            . ' GROUP BY `i`.`PARTNER`, `monat`' 
            . ' ORDER BY `monat` DESC, `Pkz` ASC'
        ,
        $verbindung
    );
    if ($imp_ges) {
        $timestamp = 0;
        
        while (($igz = mysql_fetch_assoc($imp_ges)) !== false) {
            // debug
            $debugLogManager->beginGroup('imp_ges');
            $debugLogManager->logData('igz', $igz);
            
            $monat_ges = $igz['monat'];
            
            $pid_ges = (int) $igz['PARTNER'];
            $b_ges = (int) $igz['brutto_total'];
            $n_ges = (int) $igz['netto_total'];

            $timestamp2 = $timestamp;
            $timestamp = $monat_ges;

            if ($i === 2) {
                $bg = '#E4E4E4';
            } else {
                $bg = '#FFFFFF';
                $i = 1;
            };
            
            if ($timestamp != $timestamp2) {
                $izmz = getMonthDataArray(
                    $import_datei_db,
                    $import_jahr,
                    $monat_ges,
                    $verbindung,
                    $debugLogManager
                );
                if (is_array($izmz)) {
                    $importe .= '
                        <tr>
                            <td colspan="50"></td>
                        </tr>
                        <tr style="background-color:#41557B;font-size:12px;color:#FFFFFF; border-bottom:2px solid #8e8901; font-weight:bold; background-image:url(img/treaction-gruen.png);background-repeat:repeat-x">
                            <td colspan="4" valign="bottom"> ' . getMonth($monat_ges) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['brutto']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['netto']) . '</td>
                            <td align="right"><i>' . round(((int) $izmz['netto'] / (int) $izmz['brutto']) * 100, 2) . '%</i></td>
                            <td>&nbsp;</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['metadb']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['doubletten']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['intradoubletten']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['blacklist']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['bounces']) . '</td>
                            <td align="right">' . zahl_formatieren((int) $izmz['fake']) . '</td>
                            <td>&nbsp;</td>
                        </tr>
                    ';
                }
            }
            
            $offen = suche_offene(
                $pid_ges,
                $monat_ges,
                $import_jahr,
                $debugLogManager
            );
            if ($offen == 'green') {
                $st_farbe = 'green';
                $offen = '';
            } else {
                $st_farbe = 'red';
            }
            
            $importe .= '
                <tr onclick="select_datei(event,\'' . $pid_ges . '\',\'' . $monat_ges . '\',\'' . $import_jahr . '\');"' 
                        . ' ondblclick="show(\'' . $pid_ges . '\',\'' . $monat_ges . '\');" class="container2" bgcolor="' . $bg . '"' 
                        . ' onMouseOver="this.bgColor=\'#bfdaff\'" onMouseOut="this.bgColor=\'' . $bg . '\'" style="cursor:pointer">
                    <td></td>
                    <td colspan="3" style="color:#800080;font-weight:bold">
                        <img onclick="show(\'' . $pid_ges . '\',\'' . $monat_ges . '\');" id="img' . $pid_ges . $monat_ges . '" src="img/arrow_down.png" width="12" />&nbsp;&nbsp;' 
                        . $igz['Pkz'] . ' ' 
                        . importe_anzahl(
                            $pid_ges,
                            $monat_ges,
                            $import_jahr,
                            $debugLogManager
                        ) . '
                    </td>
                    <td align="right">' . zahl_formatieren($b_ges) . '</td>
                    <td align="right">' . zahl_formatieren($n_ges) . '</td>
                    <td align="right"><i>' . round(($n_ges / $b_ges) * 100, 2) . '%</i></td>
                    <td align="right">&nbsp;</td>
                    <td align="right">' . zahl_formatieren((int) $igz['metadb_total']) . '</td>
                    <td align="right">' . zahl_formatieren((int) $igz['doubletten_total']) . '</td>
                    <td align="right">' . zahl_formatieren((int) $igz['intradoubletten_total']) . '</td>
                    <td align="right">' . zahl_formatieren((int) $igz['blacklist_total']) . '</td>
                    <td align="right">' . zahl_formatieren((int) $igz['bounces_total']) . '</td>
                    <td align="right">' . zahl_formatieren((int) $igz['fake_total']) . '</td>
                    <td>&nbsp;</td>
                </tr>
            ';
            
            $importDataArray = getImportDataArray(
                $import_datei_db,
                $partner_db,
                $felderPartnerDB,
                $pid_ges,
                $import_jahr,
                $monat_ges,
                $verbindung,
                $debugLogManager
            );
            if (is_array($importDataArray) && count($importDataArray) > 0) {
                $m = 1;
                $feld = 1;
                
                foreach ($importDataArray as $imp_zeile) {
                    $imp_nr = (int) $imp_zeile['IMPORT_NR'];
                    
                    $imp_partner = $imp_zeile[$felderPartnerDB['Pkz']];
                    $imp_partner_id = (int) $imp_zeile['PARTNER'];

                    $geliefert_datum = $imp_zeile['GELIEFERT_DATUM'];
                    $geliefert_teile = explode('-', $geliefert_datum);
                    $geliefert_datum_de = $geliefert_teile[2] . '.' . $geliefert_teile[1] . '.' . $geliefert_teile[0];

                    $imp_datei = $imp_zeile['IMPORT_DATEI'];
                    
                    $imp_anzahl = (int) $imp_zeile['BRUTTO'];
                    $imp_netto = (int) $imp_zeile['NETTO'];
                    $imp_netto_ext = (int) $imp_zeile['NETTO_EXT'];
                    $imp_rep_info = $imp_zeile['REPORTING_INFO'];
                    $imp_rep_notiz = $imp_zeile['NOTIZ'];

                    $ap_anrede = $imp_zeile[$felderPartnerDB['Rep_anrede']];
                    $ap = $imp_zeile[$felderPartnerDB['Rep_ap']];
                    $ap_email = $imp_zeile[$felderPartnerDB['Rep_email']];
                    $abschlag = $imp_zeile[$felderPartnerDB['Abschlag']];
                    
                    $q_netto = round(($imp_netto / $imp_anzahl) * 100, 2);
                    $q_netto_ext = round(($imp_netto_ext / $imp_anzahl) * 100, 2);

                    $re_bez = '';
                    $statusfarbe_r = 'red';
                    $statusfarbe_re = 'red';

                    $status = (int) $imp_zeile['STATUS'];
                    switch ($status) {
                        case 0:
                            $status_bez = 'Neu';
                            $statusfarbe = 'red';
                            break;

                        case 5:
                            $status_bez = 'importiert';
                            $statusfarbe = '#5DB00D';
                            break;

                        case 20:
                            $status_bez = 'Report intern<br />verschickt';
                            $statusfarbe_r = '#FF9900';
                            break;

                        case 21:
                            $status_bez = 'Erstreporting<br />abgeschlossen';
                            $statusfarbe_r = '#FF9900';
                            break;

                        case 22:
                            $status_bez = 'Endreporting<br />abgeschlossen';
                            $statusfarbe_r = '#5DB00D';
                            break;

                        case 30:
                            $status_bez = 'Rechnung<br />erhalten';
                            $statusfarbe_re = '#FF9900';
                            break;

                        case 31:
                            $status_bez = 'Rechnung<br />best&auml;tigt';
                            $statusfarbe_re = '#FF9900';
                            $re_bez = 'Best&auml;tigt';
                            break;

                        case 32:
                            $status_bez = 'Rechnung<br />gecheckt';
                            $statusfarbe_re = '#FF9900';
                            $re_bez = '<img src="img/Tango/22/actions/dialog-apply.png" />';
                            break;

                        case 33:
                            $status_bez = 'Rechnung<br />bezahlt';
                            $statusfarbe_re = '#F2F2F2';
                            break;
                    }

                    if ($status < 20) {
                        if ($status == 5) {
                            $statusfarbe_r = 'red';
                        }
                    } elseif ($status >= 20 && $status < 30) {
                        $statusfarbe = '#5DB00D';

                        if ($status == 22) {
                            $statusfarbe_re = 'red';
                        }
                    } elseif ($status >= 30 && $status < 33) {
                        $statusfarbe = '#5DB00D';
                        $statusfarbe_r = '#5DB00D';
                    } elseif ($status == 33) {
                        $statusfarbe = '#5DB00D';
                        $statusfarbe_r = '#5DB00D';
                        $statusfarbe_re = '#5DB00D';
                    }
                    
                    $importe_main = '';
                    
                    // getCountFromImportTableByImportNr2
                    $imp_anz = getCountFromImportTableByImportNr2(
                        $import_datei_db,
                        $imp_nr,
                        $verbindung,
                        $debugLogManager
                    );
                    if ($imp_anz > 1) {
                        $imp_main_zeile = getImporteMainDataArray(
                            $import_datei_db,
                            $imp_nr,
                            $verbindung,
                            $debugLogManager
                        );
                        if (is_array($imp_main_zeile)) {
                            $importe_main = '
                                <tr onclick="doIt_adressen(event,\'\',\'' . $imp_nr . '\',\'' . $imp_partner_id . '\',\'S\',\'' . $imp_partner . '\',\'' . $geliefert_datum_de . '\',\'' . $imp_main_zeile['brutto'] . '\',\'' . $imp_main_zeile['netto'] . '\',\'' . $imp_netto_ext . '\',\'' . $imp_rep_info . '\',\'' . $imp_rep_notiz . '\',\'' . $ap_anrede . '\',\'' . $ap . '\',\'' . $ap_email . '\',\'' . $status . '\',\'' . $abschlag . '\',\'' . $monat_ges . '\',\'' . $import_jahr . '\');"' . 
                                        ' class="container2" bgcolor="' . $bg . '" onMouseOver="this.bgColor=\'#bfdaff\'" onMouseOut="this.bgColor=\'' . $bg . '\'"' . 
                                        ' id="' . $pid_ges . $monat_ges . $feld . '" style="cursor:pointer">
                                    <td style="border-right:1px solid #DBB7FF;padding:0px;" align="center">
                                        <div style="line-height:11px;margin-top:-1px;width:79px;">
                                            <div style="float:left;">
                                                <div style="float:left;border:1px solid #999999;width:25px;height:2px;background-color:' . $statusfarbe . '"></div>
                                                <div style="float:left;border:1px solid #999999;width:25px;height:2px;margin-left:-1px;background-color:' . $statusfarbe_r . '"></div>
                                                <div style="float:left;border:1px solid #999999;width:25px;height:2px;margin-left:-1px;background-color:' . $statusfarbe_re . '"></div>
                                            </div>
                                            <div style="float:left;font-size:9px;width:79px;text-align:center;height:22px;color:#666666">' . $status_bez . '</div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td align="center">' . $geliefert_datum . '</td>
                                    <td>[Gesamt] ' . $imp_datei . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['brutto']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['netto']) . '</td>
                                    <td align="right"><i>' . $q_netto . '%</i></td>
                                    <td align="right"><i>' . round(((int) $imp_main_zeile['netto_ext'] / (int) $imp_main_zeile['brutto']) * 100, 2) . '%</i></td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['metadb']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['doubletten']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['intradoubletten']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['blacklist']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['bounces']) . '</td>
                                    <td align="right">' . zahl_formatieren((int) $imp_main_zeile['fake']) . '</td>
                                    <td>&nbsp;</td>
                                </tr>
                            ';
                        }
                        
                        $feld++;
                        $addDataArrayByImportNumber = getAddDataArrayByImportNumber(
                            $import_datei_db,
                            $imp_nr,
                            $verbindung,
                            $debugLogManager
                        );
                        if (is_array($addDataArrayByImportNumber)) {
                            foreach ($addDataArrayByImportNumber as $imp_add_zeile) {
                                $importe_main .= '
                                    <tr style="color:#333333;cursor:pointer" class="container2"' 
                                            . ' onclick="doIt_adressen(event,\'\',\'' . (int) $imp_add_zeile['IMPORT_NR'] . '\',\'' . $imp_partner_id . '\',\'T\');"' 
                                            . ' bgcolor="' . $bg . '" onMouseOver="this.bgColor=\'#bfdaff\'" onMouseOut="this.bgColor=\'' . $bg . '\'"' 
                                            . ' id="' . $pid_ges . $monat_ges . $feld . '">
                                        <td colspan="3">&nbsp;</td>
                                        <td>' . $imp_add_zeile['IMPORT_DATEI'] . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['BRUTTO']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['NETTO']) . '</td>
                                        <td align="right">&nbsp;</td>
                                        <td align="right">&nbsp;</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['METADB']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['DUBLETTEN']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['DUBLETTE_INDATEI']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['BLACKLIST']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['BOUNCES']) . '</td>
                                        <td align="right">' . zahl_formatieren((int) $imp_add_zeile['FAKE']) . '</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                ';
                            }
                            
                            $feld++;
                        }
                        
                        $importe .= $importe_main;
                    } else {
                        $importe .= '
                            <tr onclick="doIt_adressen(event,\'\',\'' . $imp_nr . '\',\'' . $imp_partner_id . '\',\'H\',\'' . $imp_partner . '\',\'' . $geliefert_datum_de . '\',\'' . $imp_anzahl . '\',\'' . $imp_netto . '\',\'' . $imp_netto_ext . '\',\'' . $imp_rep_info . '\',\'' . $imp_rep_notiz . '\',\'' . $ap_anrede . '\',\'' . $ap . '\',\'' . $ap_email . '\',\'' . $status . '\',\'' . $abschlag . '\',\'' . $monat_ges . '\',\'' . $import_jahr . '\');"' 
                                    . ' class="container2" bgcolor="' . $bg . '" onMouseOver="this.bgColor=\'#bfdaff\'" onMouseOut="this.bgColor=\'' . $bg . '\'"' 
                                    . ' id="' . $pid_ges . $monat_ges . $feld . '" style="cursor:pointer">
                                <td style="border-right:1px solid #DBB7FF;padding:0px;" align="center">
                                    <div style="line-height:11px;margin-top:-1px;width:79px;">
                                        <div style="float:left;">
                                            <div style="float:left;border:1px solid #999999;width:25px;height:2px;background-color:' . $statusfarbe . '"></div>
                                            <div style="float:left;border:1px solid #999999;width:25px;height:2px;margin-left:-1px;background-color:' . $statusfarbe_r . '"></div>
                                            <div style="float:left;border:1px solid #999999;width:25px;height:2px;margin-left:-1px;background-color:' . $statusfarbe_re . '"></div>
                                        </div>
                                        <div style="float:left;font-size:9px;width:79px;text-align:center;height:22px;color:#666666">' . $status_bez . '</div>
                                    </div>
                                </td>
                                <td></td>
                                <td align="center">' . $geliefert_datum . '</td>
                                <td>' . $imp_datei . '</td>
                                <td align="right">' . zahl_formatieren($imp_anzahl) . '</td>
                                <td align="right">' . zahl_formatieren($imp_netto) . '</td>
                                <td align="right"><i>' . $q_netto . '%</i></td>
                                <td align="right">' . zahl_formatieren(round((($imp_anzahl * $q_netto_ext) / 100), 0)) . ' (<i>' . $q_netto_ext . '%</i>)</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['METADB']) . '</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['DUBLETTEN']) . '</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['DUBLETTE_INDATEI']) . '</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['BLACKLIST']) . '</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['BOUNCES']) . '</td>
                                <td align="right">' . zahl_formatieren((int) $imp_zeile['FAKE']) . '</td>
                                <td>&nbsp;</td>
                            </tr>
                        ';
                        
                        $m++;
                        $feld++;
                    }
                }
                
                $i++;
            }
            
            // debug
            $debugLogManager->endGroup();
        }
        mysql_free_result($imp_ges);
    } else {
        // debug
        $debugLogManager->logData('sql error: ' . __FUNCTION__, mysql_error($verbindung));
    }
    
    return $importe;
}




$_SESSION['stats_id'] = '';

if (isset($_GET['import_jahr'])) {
    $import_jahr = $_GET['import_jahr'];
    $_SESSION['import_jahr'] = $import_jahr;
} elseif (isset($_SESSION['import_jahr'])) {
    $import_jahr = $_SESSION['import_jahr'];
} else {
    $import_jahr = date('Y');
}


// details_footer
$footerDataArray = details_footer(
    $import_jahr,
    $debugLogManager
);
// debug
$debugLogManager->logData('footerDataArray', $footerDataArray);
?>
<div id="fakeContainer">
    <table cellspacing="0" cellpadding="0" border="0" id="scrollTable2" bordercolor="#CCCCCC" style="border-collapse:collapse;">
        <tr class="tab_bg">
            <th>Status</th>
            <th>Partner</th>
            <th align="center">Lieferdatum</th>
            <th>Dateiname</th>
            <th style="text-align:right">Brutto</th>
            <th style="text-align:right">Netto</th>
            <th style="text-align:right">Quote</th>
            <th style="text-align:right">EXT</th>
            <th style="text-align:right">Bereits erhalten</th>
            <th style="text-align:right">Dubletten</th>
            <th style="text-align:right">Intradubletten</th>
            <th style="text-align:right">Blacklist</th>
            <th style="text-align:right">Bounces</th>
            <th style="text-align:right">Fake Email</th>
            <th style="width: 100%;"></th>
        </tr>
        <tr>
            <td colspan="50" style="background-color:#FFFFFF;font-size:16px;font-weight:bold;color:#8e8901">
                <?php echo $import_jahr; ?>
            </td>
        </tr>
        <?php 
            echo importe_raussuchen(
                $import_jahr,
                $debugLogManager
            );
        ?>
    </table>
</div>
<div style="background-color:#8e8901;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php
        if ($footerDataArray['gesamt'] > 0) {
            echo zahl_formatieren($footerDataArray['gesamt']) . ' Datei(en)';
        } else {
            echo 'Keine Dateien vorhanden &nbsp;&nbsp;&nbsp;';
        }
        ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src="img/treaction_gruen_seperator.png" />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php echo 'Brutto: ' . zahl_formatieren($footerDataArray['brutto']); ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src="img/treaction_gruen_seperator.png" />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php echo 'Netto: ' . zahl_formatieren($footerDataArray['netto']); ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src="img/treaction_gruen_seperator.png" />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php echo 'Quote: ' . round(($footerDataArray['netto'] / $footerDataArray['brutto']) * 100, 2) . '%'; ?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src="img/treaction_gruen_seperator.png" />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
        <?php echo zahl_formatieren($footerDataArray['open']) . ' offene Reportings'; ?>
    </div>
</div>
<script type="text/javascript">
    wi = document.documentElement.clientWidth;
    hi = document.documentElement.clientHeight;
    
    if (uBrowser == 'Firefox') {
        hi = hi-142;
        wi = wi-205;
    } else {
        hi = hi-143;
        wi = wi-205;
    }
    
    document.getElementById('fakeContainer').style.width = wi + 'px';
    document.getElementById('fakeContainer').style.height = hi + 'px';

    function Resize() {
        setRequest('limport/index.php','a');
    }

    window.onresize = Resize;

    var mySt = new superTable('scrollTable2', {
        cssSkin: 'sSky',
        fixedCols: 0,
        headerRows: 2
    });
</script>