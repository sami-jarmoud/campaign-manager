<?php
$felderMetaDB = array(
    'EMAIL' => 'EMAIL',
    'ID'=> 'ID',
    'HerkunftID' => 'HerkunftID',
    'ANREDE' => 'ANREDE',
    'VORNAME' => 'VORNAME',
    'NACHNAME' => 'NACHNAME',
    'STRASSE' => 'STRASSE',
    'PLZ' => 'PLZ',
    'ORT' => 'ORT',
    'LAND' => 'LAND',
    'BKZ' => 'BKZ',
    'GEBURTSDATUM' => 'GEBURTSDATUM',
    'TELEFON' => 'TELEFON',
    'EINTRAGSDATUM' => 'EINTRAGSDATUM',
    'IP' => 'IP',
    'HERKUNFT' => 'HERKUNFT',
    'PARTNER' => 'PARTNER',
    'DOI_DATE' => 'DOI_DATE',
    'DOI_IP' => 'DOI_IP',
    'IMPORT_NR' => 'IMPORT_NR',
    'ATTRIBUTE' => 'ATTRIBUTE',
    'IMPORTDATUM' => 'IMPORTDATUM'
);

$feldExportName = array(
    'EMAIL' => 'EMAIL',
    'ID' => 'ID',
    'HerkunftID' => 'HerkunftID',
    'ANREDE' => 'ANREDE',
    'VORNAME' => 'VORNAME',
    'NACHNAME' => 'NACHNAME',
    'STRASSE' => 'STRASSE',
    'PLZ' => 'PLZ',
    'ORT' => 'ORT',
    'LAND' => 'LAND',
    'BKZ' => 'BKZ',
    'GEBURTSDATUM' => 'GEBURTSDATUM',
    'TELEFON' => 'TELEFON',
    'EINTRAGSDATUM' => 'HERKUNFTSDATUM',
    'IP' => 'IP',
    'HERKUNFT' => 'HERKUNFT',
    'PARTNER' => 'PARTNER',
    'DOI_DATE' => 'DOI_DATE',
    'DOI_IP' => 'DOI_IP',
    'IMPORT_NR' => 'IMPORT_NR',
    'ATTRIBUTE' => 'ATTRIBUTE',
    'IMPORTDATUM' => 'IMPORTDATUM'
);

$felderPartnerDB = array(
    'PID' => 'PID',
    'Pkz' => 'Pkz',
    'Gen' => 'Gen',
    'GenArt' => 'GenArt',
    'Quote' => 'Quote',
    'Firma' => 'Firma',
    'Website' => 'Website',
    'Strasse' => 'Strasse',
    'PLZ' => 'PLZ',
    'Ort' => 'Ort',
    'Geschaeftsfuehrer' => 'Geschaeftsfuehrer',
    'Registergericht' => 'Registergericht',
    'Rep_anrede' => 'Rep_anrede',
    'Rep_ap' => 'Rep_ap',
    'Rep_email' => 'Rep_email',
    'Lieferintervall' => 'Lieferintervall',
    'Bedingung' => 'Bedingung',
    'Telefon' => 'Telefon',
    'Fax' => 'Fax',
    'Abschlag' => 'Abschlag',
    'Status' => 'Status'
);

$felderHerkunftDB = array(
    'ID' => 'ID',
    'HERKUNFT_NAME' => 'HERKUNFT_NAME'
);

$abgleichTableArr = array();

$felder = array('EMAIL', 'ANREDE', 'VORNAME', 'NACHNAME', 'STRASSE', 'PLZ', 'ORT', 'LAND', 'BKZ', 'GEBURTSDATUM', 'TELEFON', 'HERKUNFTSDATUM', 'IP', 'HERKUNFT', 'PARTNER');
$felder_bc = array('EMAIL', 'ANREDE', 'VORNAME', 'NACHNAME', 'STRASSE', 'PLZ', 'ORT', 'LAND', 'GEBDATUM', 'TELEFON', 'EINTRAGSDATUM', 'IP', 'HERKUNFT', 'PARTNER');
$export_street_hsnr = false;


$dbHostEms = 'localhost';

// dataManager - Configs
$anrede_db = 'anrede';
$herkunft_db = 'herkunft';
$import_datei_db = 'import_datei';
$partner_db = 'partner';
$partner_ap = 'partner_ap';
$metaDB = 'history';
$metaDB_test = false;

$cm_absender_prefix = 'news';


$errorMysql = '<p style="color:red;font-size:10px;">FEHLER: Keine Verbindung -> ' . $mandant . '-Meta-Datenbank!</p>';
$errorDB = '<p style="color:red;font-size:10px;">FEHLER: Keine ' . $mandant . ' Datenbank!</p>';

$smtpPort = 25;
$smtpEncryption = null;
switch ($mandant) {
    case 'demoClient':
        $db_name = 'demo-client_maas_db';
        $db_user = 'dc-maas-user-db';
        $db_pw = 'Q0l9L7k7';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'sami.jarmoud@treaction.onmicrosoft.com';
        $smtp_pw = ';4Cva|fCSA3ljf/:';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@treaction.de';
        $absender_email = 'sami.jarmoud@treaction.onmicrosoft.com';
        $absender_name = 'Maas Treaction';
		$serverName = 'maas.treaction.de';
        
        $anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;

	case 'skylinePerformance':
        $db_name = 'skyline-performance_maas_db';
        $db_user = 'sk-maas-user-db';
        $db_pw = '6A3x2M9h';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'kontakt@skylineperformance.onmicrosoft.com';
        $smtp_pw = '4DcPJGgh5fYdin+.';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@skyline-performance.de';
        $absender_email = 'kontakt@skylineperformance.onmicrosoft.com';
        $absender_name = 'Skyline Performance GmbH';
		$serverName = 'skyline-performance.de';
        
        $anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;
	
	case 'complead':
        $db_name = 'complead_maas_db';
        $db_user = 'complead-user-db';
        $db_pw = '7I5s5D3d';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'kontakt@complead.de';
        $smtp_pw = 'GxK=zIToc,cD\q';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@complead.de';
        $absender_email = 'kontakt@complead.de';
        $absender_name = 'Complead GmbH';
		$serverName = 'complead.de';
        
        $anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;
	
	case 'intone':
        $db_name = 'intone_maas_db';
        $db_user = 'intone-user-db';
        $db_pw = '9O8p0H6d';
		
		$smtp_host = 'smtp.1und1.de';
        $smtp_user = 'treaction@interactive-one.de';
        $smtp_pw = '+!84CF41Zirz';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@interactive-mailing.de';
        $absender_email = 'treaction@interactive-one.de';
        $absender_name = 'Interactive One GmbH';
		$serverName = 'interactive-one.de';
		
		$abgleichTableArr = array(
			array(
				'dbhost' => $dbHostEms,
				'dbname' => 'opt_db',
				'dbuser' => 'dbo39323310',
				'dbpw' => 'ama7beg4',
				'tableName' => 'Users', // Realtime Intone
				'info' => 'OptDB von Intone (Email)',
				'feldname' => 'Email',
				'attribute' => 'm',
				'id' => 'optID'
			),
		);

		$anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;
	
	case 'stellarPerformance':
        $db_name = 'stellar_maas_db';
        $db_user = 'stellar-user-db';
        $db_pw = 'V1s9S3y6';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'csc@stellar-performance.de';
        $smtp_pw = 'jK4.1WX2b!+p';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@stellar-performance.de';
        $absender_email = 'kontakt@stellar-performance.de';
        $absender_name = 'Stellar Performance GmbH';
		$serverName = 'stellar-performance.de';
        
        $anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;
    
    case '4waveMarketing':
        $db_name = '4wave_maas_db';
        $db_user = '4wave-user-db';
        $db_pw = '5U8t9N8j';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'office@4wave-marketing.de';
        $smtp_pw = 'djks!TTSss';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@4wave-marketing.de';
        $absender_email = 'office@4wave-marketing.de';
        $absender_name = '4Wave Marketing GmbH';
		$serverName = '4wave-marketing.de';
        
        $anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
        break;
    case 'leadWorld':
        $db_name = 'leadworld_maas_db';
        $db_user = 'lead-user-db';
        $db_pw = '4H5d4Q4t';
        
        // campaign - Configs
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'csc@stellar-performance.de';
        $smtp_pw = 'jK4.1WX2b!+p';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@stellar-performance.de';
        $absender_email = 'kontakt@stellar-performance.de';
        $absender_name = 'Stellar Online GmbH';
		$serverName = 'stellar-performance.de';
		
		$anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
		
		break;
    
    case 'taglichAngebote':
        
        $db_name = 'taglich_angebote_db';
        $db_user = 'taglichangebote';
        $db_pw = 'rhO5%z95';
        
		$smtp_host = 'smtp.office365.com';
        $smtp_user = 'csc@stellar-performance.de';
        $smtp_pw = 'jK4.1WX2b!+p';
		$smtpEncryption = 'tls';
		$smtpPort = 587;
		
        $cm_absender_suffix = '@stellar-performance.de';
        $absender_email = 'kontakt@stellar-performance.de';
        $absender_name = 'Täglich Angebote UG';
		$serverName = 'stellar-performance.de';
        
        		
		$anreicherungDBArr = array(
            'dbhost' => $dbHostEms,
            'dbname' => $db_name,
            'dbuser' => $db_user,
            'dbpw' => $db_pw,
            'table' => $metaDB
        );
		
        
        break;     
}

$verbindung = mysql_connect($dbHostEms, $db_user, $db_pw) or print $errorMysql;
mysql_select_db($db_name, $verbindung) or print $errorDB;
mysql_query("SET NAMES 'LATIN1'", $verbindung);

$temp_db = 'tmp_' . $_SESSION['benutzer_name'];
$bl_temp = 'tmp_bl_' . $_SESSION['benutzer_name'];
$metaDB_temp = 'meta_temp_' . $_SESSION['benutzer_name'];
?>