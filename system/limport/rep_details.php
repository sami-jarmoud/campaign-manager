<?php
header('Content-Type: text/html; charset=ISO-8859-1');

session_start();

// debug
function firePhp () {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}


$firePhp = firePhp();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
    $firePhp->log($_GET, '$_GET');
}

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];

include('db_connect.inc.php');
include('../library/util.php');

$impNr = $_GET['import_nr'];

$sql_data = mysql_query(
    'SELECT *' . 
        ' FROM `' . $import_datei_db . '` as `i`, `' . $partner_db . '` as `p`' . 
        ' WHERE `i`.`IMPORT_NR` = "' . $impNr . '"' . 
            ' AND `i`.`Partner` = `p`.`' . $felderPartnerDB['PID'] . '`'
    ,
    $verbindung
);
if ($sql_data) {
    $z = mysql_fetch_array($sql_data);
    mysql_free_result($sql_data);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z, 'z');
    }
    
    $abschlag = $z['Abschlag'];
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: sql_data');
    }
}

$sql_data2 = mysql_query(
    'SELECT SUM(`BRUTTO`) as `b`, SUM(`NETTO`) as `n`, SUM(`NETTO_EXT`) as `ne`' . 
        ' FROM `' . $import_datei_db . '` as `i`, `' . $partner_db . '` as `p`' . 
        ' WHERE `i`.`IMPORT_NR2` = "' . $impNr . '" AND `i`.`Partner` = `p`.`' . $felderPartnerDB['PID'] . '`'
    ,
    $verbindung
);
if ($sql_data2) {
    $z2 = mysql_fetch_array($sql_data2);
    mysql_free_result($sql_data2);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z2, 'z2');
    }
    
    $brutto = $z2['b'];
    $netto = $z2['n'];
    $netto_e = $z2['ne'];
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: sql_data2');
    }
}

// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($abschlag, 'abschlag');
    $firePhp->log($brutto, 'brutto');
    $firePhp->log($netto, 'netto');
    $firePhp->log($netto_e, 'netto_e');
}

if ($abschlag > 0) {
    $abschlag = $z['Abschlag'] . '%';
    $abschlag_button = '<input type="button" value="aktivieren" style="width:65px;text-align:center;background-color:#333333;color:#FFFFFF;cursor:pointer" id="b_abschlag_activated"' . 
        ' onclick="abschlag(\'' . $brutto . '\',\'' . $z['Abschlag'] . '\');" />
    ';
} else {
    $abschlag = '-';
    $abschlag_button = '';
}

include('../db_pep.inc.php');
$sql_m = mysql_query(
    'SELECT *' . 
        ' FROM `mandant`' . 
        ' WHERE `id` = "' . $mID . '"'
    ,
    $verbindung_pep
);
if ($sql_m) {
    $zm = mysql_fetch_array($sql_m);
    mysql_free_result($sql_m);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($zm, 'zm');
    }
    
    $vorlage = $zm['dm_rep_mail'];
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung), 'sql error: sql_m');
    }
}
?>
<input type="hidden" id="dateiname" value="<?php echo $z['IMPORT_DATEI']; ?>" />
<input type="hidden" id="brutto" value="<?php echo $brutto; ?>" />
<input type="hidden" id="netto" value="<?php echo $netto; ?>" />
<input type="hidden" id="lieferdatum" value="<?php echo util::datum_de($z['GELIEFERT_DATUM'], 'date'); ?>" />
<input type="hidden" id="netto_ext_old" value="<?php echo util::getQuote($brutto, $netto_e); ?>" />

<div id="dm_rep" class="yui-navset" style="text-align:left;border:0px;width:600">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class="selected" id="tab_rep1_li"><a href="#tab_rep1"><em>Reporting Zahlen</em></a></li>
        <li id="tab_rep2_li"><a href="#tab_rep2"><em>Reporting Email</em></a></li>
    </ul>
    <div class="yui-content"> 
        <div id="tab_rep1">
            <table width="250" class="neu_k" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class='details_hd_info'>Allgemein</td>
                    <td colspan="2" class='details_hd_info'></td>
                </tr>
                <tr>
                    <td align="right" class='details_info2'>Partner</td>
                    <td colspan="2" align="left" class='details2_info2' id="partner_report"><?php echo $z[$felderPartnerDB['Pkz']]; ?></td>
                </tr>
                <tr>
                    <td align="right" class='details_info'>Lieferung</td>
                    <td colspan="2" align="left" class='details2_info'><?php echo $z['IMPORT_DATEI']; ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'>Lieferdatum</td>
                    <td colspan="2" align="left" class='details2_info2'>
                        <?php
                            echo util::datum_de(
                                $z['GELIEFERT_DATUM'],
                                'date'
                            );
                        ?>
                    </td>
                </tr>
                <tr>
                    <td align="right" class='details_info'>Brutto</td>
                    <td colspan="2" align="left" id="brutto_report" class='details2_info'><?php echo util::zahl_format($brutto); ?></td>
                </tr>
                <tr>
                    <td align="right" class='details_info2'>Netto</td>
                    <td align="left" class='details2_info2' id="netto_report"><?php echo util::zahl_format($netto); ?></td>
                    <td class='details2_info2'>
                        <input type="text" value="<?php echo util::getQuote($brutto, $netto); ?>" style="border:0px;background-color:#E4E4E4;width:40px;text-align:right" readonly/>%
                    </td>
                </tr>
                <tr>
                    <td align="right" class="details_info">Netto (Ext)</td>
                    <td align="left" id="netto_report_ext">
                        <input type="text" name="netto_ext" id="netto_ext" value="<?php echo util::zahl_format($netto_e); ?>" style="border:0px;background-color:#F2F2F2;text-align:left" readonly/>
                    </td>
                    <td style="white-space:nowrap;">
                        <input type="text" name="netto_rate_ext" id="netto_rate_ext" value="<?php echo util::getQuote($brutto, $netto_e); ?>" style="background-color:#FFFFC1;width:40px;text-align:right" />%
                    </td>
                </tr>
                <tr>
                    <td align="right" class='details_info2'>Standardabschlag</td>
                    <td align="left" class='details2_info2' id="abschlag" ><?php echo $abschlag; ?></td>
                    <td class='details2_info2'><?php echo $abschlag_button; ?></td>
                </tr>
                <tr>
                    <td align="right" class="info" valign="top">Notiz</td>
                    <td colspan="2" style="font-size:11px;color:#333333"><textarea id="notiz_rep" name="notiz_rep" rows="3" cols="35"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2" id="rep_info" style="font-size:10px;color:#666666"></td>
                </tr>
            </table>
        </div>
        <div id="tab_rep2">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td height="25" class="info">An:</td>
                    <td> 
                        <input type="text" readonly="readonly" style="background-color:#F2F2F2; border:0px; font-weight:bold; width:160px;" id="ap_email" name="ap_email" value="" /> 
                        (<span id="ap_rep" name="ap_rep" style="font-size:11px"></span>)
                    </td>
                </tr>
                <tr>
                    <td height="25" class="info">CC:</td>
                    <td> 
                        <input type="checkbox" name="empfaenger[]" value="sami.jarmoud@treaction.de" /> Sami                     
                    </td>
                </tr>
                <tr>
                    <td height="25" class="info">Betreff:</td>
                    <td><input type="text" id="betreff_rep" name="betreff_rep" size="50" value="<?php echo $mailing_betreff; ?>" /></td>
                </tr>
                <tr>
                    <td height="25" class="info">Von:</td>
                    <td><?php echo $_SESSION['username']; ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea id="mail_text" name="mail_text" rows="15" style="width:360px;"><?php echo $vorlage; ?></textarea>
                        <textarea id="mail_text2" style="display:none;"><?php echo $vorlage; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="30"><button id="rep_email_send">Email senden</button></td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="netto_report_ext2" name="netto_report_ext2" value='' />
        <input type="hidden" id="import_nr" name="import_nr" value='<?php echo $z['IMPORT_NR']; ?>' />
        <input type="hidden" id="status_rep" name="status_rep" value='<?php echo $z['STATUS']; ?>' />
        <input type="hidden" id="rep_email_status" name="rep_email_status" value='' />
    </div>
</div>
<script type="text/javascript">
    var tabView_dm_rep = new YAHOO.widget.TabView('dm_rep');
    document.getElementById('netto_rate_ext').onkeyup = netto_calc;
    
    function netto_calc() {
        var brutto = document.getElementById('brutto').value;
        var netto_ext_rate = document.getElementById('netto_rate_ext').value;
        netto_ext_old = document.getElementById('netto_ext_old').value;
        
        netto_ext_rate = netto_ext_rate.replace(',', '.');
        
        if (netto_ext_rate > 100 || netto_ext_rate < 0) {
            alert('Diese Rate ist ung�ltig!');
            
            netto_ext_old = document.getElementById('netto_ext_old').value;
            netto = document.getElementById('netto').value;
            document.getElementById('netto_ext').value = Trenner(netto);
            document.getElementById('netto_rate_ext').value = netto_ext_old;
            document.getElementById('netto_report_ext2').value = netto;
        } else {
            new_netto = (brutto*netto_ext_rate)/100;
            document.getElementById('netto_ext').value = Trenner(new_netto.toFixed(0));
            document.getElementById('netto_report_ext2').value = new_netto.toFixed(0);
        }
    }
    
    function abschlag(b,abschlag) {
        netto_ext_old = document.getElementById('netto_ext_old').value;
        new_netto = (b*abschlag)/100;
        document.getElementById('netto_ext').value = Trenner(new_netto.toFixed(0));
        document.getElementById('netto_rate_ext').value = abschlag;
        
        buttonObj = document.getElementById('b_abschlag_activated').value;
        document.getElementById('b_abschlag_activated').style.border = '1px solid #666666';
        
        if (buttonObj == 'aktivieren') {
            new_netto = (b*abschlag)/100;
            
            document.getElementById('netto_ext').value = Trenner(new_netto.toFixed(0));
            document.getElementById('netto_rate_ext').value = abschlag;
            document.getElementById('b_abschlag_activated').value = 'deaktivieren';
            document.getElementById('netto_report_ext2').value = new_netto.toFixed(0);
        } else {
            netto = document.getElementById('netto').value;
            
            document.getElementById('netto_ext').value = Trenner(netto);
            document.getElementById('netto_rate_ext').value = netto_ext_old;
            document.getElementById('b_abschlag_activated').value = 'aktivieren';
            document.getElementById('netto_report_ext2').value = netto;
        }
        
        return false;
    }
    
    document.getElementById('tab_rep2_li').onclick = function() {
        dateiname = document.getElementById('dateiname').value;
        mailtext = document.getElementById('mail_text').value;
        brutto = document.getElementById('brutto').value;
        netto = document.getElementById('netto_ext').value;
        lieferdatum = document.getElementById('lieferdatum').value;
        ext_q = document.getElementById('netto_rate_ext').value;
        
        mailtext = str_replace('{Dateiname}', dateiname, mailtext);
        mailtext = str_replace('{Lieferdatum}', lieferdatum, mailtext);
        mailtext = str_replace('{Brutto}', Trenner(brutto), mailtext);
        mailtext = str_replace('{Netto}', netto, mailtext);
        mailtext = str_replace('{Quote}', runde(ext_q), mailtext);
        
        document.getElementById('mail_text').value = mailtext;
    }
    
    document.getElementById('tab_rep1_li').onclick = function() {
        mailtext2 = document.getElementById('mail_text2').value;
        document.getElementById('mail_text').value = mailtext2;
    }
</script>