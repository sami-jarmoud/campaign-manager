<?php
session_start();

$mandant = $_SESSION['mandant'];
include('db_connect.inc.php');

// debugLogManager
require_once('../library/debugLogManager.php');

$debugLogManager = new debugLogManager();
$debugLogManager->setUserId($_SESSION['benutzer_id']);
$debugLogManager->init();

$debugLogManager->logData('GET', $_GET);
#$debugLogManager->logData('SESSION', $_SESSION);


$temp_datei = 'export' . $_SESSION['benutzer_name'] . '.csv';

if ((int) $_SESSION['testimport'] === 1) {
    $metaDB = $metaDB . '_test';
    $import_datei_db = $import_datei_db . '_test';
}

if (file_exists($temp_datei)) {
    unlink($temp_datei);
}

$import_nr = (int) $_GET['impnr'];
$a = isset($_GET['a']) ? strip_tags($_GET['a']) : '';

$filehandle = fopen($temp_datei, 'a');

$export_query = mysql_query(
        'SELECT m.' . $felderMetaDB['EMAIL'] . ', m.' . $felderMetaDB['ANREDE'] . ', m.' . $felderMetaDB['VORNAME'] . ', m.' . $felderMetaDB['NACHNAME'] 
            . ', m.' . $felderMetaDB['STRASSE'] . ', m.' . $felderMetaDB['PLZ'] . ', m.' . $felderMetaDB['ORT'] 
        . ' FROM ' . $metaDB . ' as m' 
            . ', ' . $herkunft_db . ' as h' 
            . ', ' . $import_datei_db . ' as i' 
            . ', ' . $partner_db . ' as p' 
        . ' WHERE m.' . $felderMetaDB['IMPORT_NR'] . ' = ' . $import_nr 
            . ' AND m.' . $felderMetaDB['HERKUNFT'] . ' = h.' . $felderHerkunftDB['ID'] 
            . ' AND i.IMPORT_NR = ' . $import_nr 
            . ' AND i.PARTNER = p.' . $felderPartnerDB['PID'] 
            . ' AND m.' . $felderMetaDB['ATTRIBUTE'] . ' = "' . mysql_real_escape_string($a) . '"'
    ,
    $verbindung
);
if (!$export_query) {
    $debugLogManager->logData('sql error: export_query', mysql_error($verbindung));
    
    die('error');
} else {
    // debug
    $debugLogManager->beginGroup(__FILE__);

    $debugLogManager->logData('row', mysql_num_fields($export_query));
    while (($row = mysql_fetch_assoc($export_query)) !== false) {
        // debug
        $debugLogManager->logData('row', $row);

        if ($row[$felderMetaDB['ANREDE']] == '1') {
            $anrede = 'Herr';
        } elseif ($row[$felderMetaDB['ANREDE']] == '0') {
            $anrede = 'Frau';
        }
        
        fputcsv(
            $filehandle,
            array(
                $row[$felderMetaDB['EMAIL']],
                $anrede,
                $row[$felderMetaDB['VORNAME']],
                $row[$felderMetaDB['NACHNAME']],
                $row[$felderMetaDB['STRASSE']],
                $row[$felderMetaDB['PLZ']],
                $row[$felderMetaDB['ORT']]
            )
        );
    }
    mysql_free_result($export_query);

    // debug
    $debugLogManager->endGroup();

    fclose($filehandle);

    header('Content-type: application/octet-stream');
    header('Content-disposition: attachment; filename=' . $temp_datei);
    readfile($temp_datei);
}
?>