<table class="transp" style="white-space: nowrap;">
    <tr>
        <td class="details_hd_info">Art</td>
        <td class="details_hd_info" style="text-align:left;">Anzahl</td>
        <td class="details_hd_info" style="text-align:left;">Export</td>
    </tr>
    <tr>
        <td class="details_info">Bereits erhalten</td>
        <td class="details_info3"><?php echo zahl_format($imp_metadb) . ' (' . $q_imp_metadb . '%)'; ?></td>
        <td class="details2_info"><?php echo $imp_meta_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Dubletten</td>
        <td class="details2_info3"><?php echo zahl_format($imp_dbl) . ' (' . $q_imp_dbl . '%)'; ?></td>
        <td class="details2_info2"><?php echo $imp_dbl_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info">Dubletten in Datei</td>
        <td class="details_info3"><?php echo zahl_format($imp_dbl_indatei) . ' (' . $q_imp_dbl_indatei . '%)'; ?></td>
        <td class="details2_info"><?php echo $imp_dbl_indatei_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Blacklist</td>
        <td class="details2_info3"><?php echo zahl_format($imp_blacklist) . ' (' . $q_imp_blacklist . '%)'; ?></td>
        <td class="details2_info2"><?php echo $imp_blacklist_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info">Bounces</td>
        <td class="details_info3"><?php echo zahl_format($imp_bounces) . ' (' . $q_imp_bounces . '%)'; ?></td>
        <td class="details2_info"><?php echo $imp_bounces_exp; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Fake Email</td>
        <td class="details2_info3"><?php echo zahl_format($imp_fake) . ' (' . $q_imp_fake . '%)'; ?></td>
        <td class="details2_info2"><?php echo $imp_fake_exp; ?></td>
    </tr>
</table>