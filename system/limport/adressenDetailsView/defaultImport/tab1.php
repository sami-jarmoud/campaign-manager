<table class="transp">
    <tr>
        <td class="details_hd_info">&Uuml;bersicht</td>
        <td class="details_hd_info"></td>
    </tr>
    <tr>
        <td class="details_info2">Importnr.</td>
        <td class="details2_info2"><?php echo $imp_nr; ?></td>
    </tr>
    <tr>
        <td class="details_info">Partner</td>
        <td class="details2_info"><?php echo $imp_partner; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Lieferdatum</td>
        <td class="details2_info2"><?php echo $geliefert_datum; ?></td>
    </tr>
    <tr>
        <td class="details_info">Importdatum</td>
        <td class="details2_info"><?php echo $imp_datum; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Dateiname</td>
        <td class="details2_info2"><?php echo $imp_datei; ?></td>
    </tr>
    <tr>
        <td class="details_info">Liefermenge</td>
        <td class="details2_info"><?php echo zahl_format($imp_anzahl); ?></td>
    </tr>
    <tr>
        <td class="details_info2">Netto</td>
        <td class="details2_info2"><?php echo zahl_format($imp_netto); ?></td>
    </tr>
</table>