<table class="transp">
    <tr>
        <td class="details_hd_info">&Uuml;bersicht: Broadmail-Import</td>
        <td class="details_hd_info"></td>
    </tr>
    <tr>
        <td class="details_info2">Importnr.</td>
        <td class="details2_info2"><?php echo $imp_nr; ?></td>
    </tr>
    <tr>
        <td class="details_info">TicketId</td>
        <td class="details2_info"><a href="https://iosm.pixsoftware.de/browse/<?php echo $rowImportDataArray['TICKET_ID']; ?>" target="_blank"><?php echo $rowImportDataArray['TICKET_ID']; ?></a></td>
    </tr>
    <tr>
        <td class="details_info2">Partner</td>
        <td class="details2_info2"><?php echo $imp_partner; ?></td>
    </tr>
    <tr>
        <td class="details_info">Lieferdatum</td>
        <td class="details2_info"><?php echo $geliefert_datum; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Importdatum</td>
        <td class="details2_info2"><?php echo $imp_datum; ?></td>
    </tr>
    <tr>
        <td class="details_info">Dateiname</td>
        <td class="details2_info"><?php echo $imp_datei; ?></td>
    </tr>
    <tr>
        <td class="details_info2">Liefermenge</td>
        <td class="details2_info2"><?php echo zahl_format($imp_anzahl); ?></td>
    </tr>
</table>