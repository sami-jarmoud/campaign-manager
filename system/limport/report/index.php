<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$mandant = $_SESSION['mandant'];
include('../../library/util.php');
include('../../limport/db_connect.inc.php');
include('../../library/dataManagerWebservice.php');

$dmMweb = new dataManagerWebservice();
$openDataArr = $dmMweb->getLieferungDataAll($import_datei_db,$partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],'');
$cntOpen = count($openDataArr);

if ($mandant=='intone' || $mandant == 'temp_mandant') {
	$openRTDataArr = $dmMweb->getRealtimeLieferungDataAll($partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],'','');
	$cntRTOpen = count($openRTDataArr);
	$doneRTDataArr = $dmMweb->getRealtimeLieferungDataAll($partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],'real_archiv','');
	$cntRTDone = count($doneRTDataArr);
} else {
	$cntRTOpen = 0;
	$cntRTDone = 0;
}

$doneDataArr = $dmMweb->getLieferungDataAll($import_datei_db,$partner_db,$felderPartnerDB['Firma'],$felderPartnerDB['PID'],'archiv');
$cntDone = count($doneDataArr);

$v_tab = $_GET['v'];
if (!$v_tab) {$v_tab = 'import';}
?>


<div id="global_layout" style="overflow:hidden;border:0px;">
    <div id="tab_k_r" class="yui-navset spalten_tab">
        <ul class="yui-nav" style="margin-left:3px;">
            <li class="selected" id="import_tab" onclick="switchTab('import');"><a href="#tab1"><em>Import (<?php print $cntOpen; ?>)</em></a></li>
			
            <?php 
				if ($mandant=='intone' || $mandant == 'temp_mandant') {
					print '<li id="real_tab" onclick="switchTab(\'real\');"><a href="#tab2"><em>Realtime ('.$cntRTOpen.')</em></a></li>';
				} 
			?>
           
            <li id="vorlage_tab" onclick="switchTab('vorlage');"><a href="#dm_rep_vorlage"><em>Vorlage</em></a></li>
            <li id="archiv_tab"  onclick="switchTab('archiv');"><a href="#tab4"><em>Archiv (<?php print $cntDone; ?>)</em></a></li>
			<?php 
				if ($mandant=='intone' || $mandant == 'temp_mandant') {
					print '<li id="real_archiv_tab" onclick="switchTab(\'real_archiv\');"><a href="#tab5"><em>Archiv Realtime ('.$cntRTDone.')</em></a></li>';
				} 
			?>
        </ul>
    </div>
    
    <div id="left_table" style="overflow:hidden;background-color:#F2F2F2;"></div>
    <div id="center_content" style="background-color:#FFFFFF;overflow:auto"></div>
	<div id="dm_rep_vorlage" style="display:none"></div>
</div>

<input type="hidden" id="dm_rep_page" value="import" />
<script type="text/javascript">
//setRequest_default("kampagne/report/rep_table.php","rep_table2");
var layoutb;

var tabView_status = new YAHOO.widget.TabView('tab_k_r');

var myContextMenu = false;
var v_tab = "<?php print $v_tab; ?>"+"_tab";
switchTab("<?php print $v_tab; ?>");



function switchTab(v) {
	document.getElementById('dm_rep_page').value = v;
	v_tab = v+"_tab";
	document.getElementById(v_tab).setAttribute("class", "selected");

	document.getElementById('dm_rep_vorlage').style.display='none';
	document.getElementById('left_table').style.display='';
	document.getElementById('center_content').style.display='';

	switch (v) {
		case 'import':  	left_table(v,<?php print $cntOpen; ?>);break;
		case 'real':  		left_table(v,<?php print $cntRTOpen; ?>);break;
		case 'archiv':  	left_table(v,<?php print $cntDone; ?>);break;
		case 'real_archiv': left_table(v,<?php print $cntRTDone; ?>);break;
		case 'vorlage': setRequest_default('limport/report/rep_vorlage.php','dm_rep_vorlage'); 
						document.getElementById('dm_rep_vorlage').style.display='';
						document.getElementById('left_table').style.display='none';
						document.getElementById('center_content').style.display='none';
						layoutb = new YAHOO.widget.Layout('global_layout', {
							height: Dom.getClientHeight()-119, //Height of the viewport
							width: Dom.get('global_layout').offsetWidth, //Width of the outer element
							minHeight: 150, //So it doesn't get too small
							units: [
								{ position: 'left', width: 0, body: 'left_table', gutter: '-2 -5 -1 -1'},
								{ position: 'center', width: 0, body: 'center_content', gutter: '-1 -1 0 -5'}
							]
						});
						layoutb.render();
						break;
	}

}
		
		//alert(height_layoutb);
        //Handle the resizing of the window
	   

    function left_table(view,cnt) {

		if (view == 'archiv') {
			def_content = "<div style='padding:25px'><span style='font-size:16px;'>Reporting Datenlieferungen [Archiv]</span><br /><br />Bitte w&auml;hlen Sie eine Lieferung aus, um das Reporting anzuzeigen.</div>";
		} 
		
		if (view == 'real_archiv') {
			def_content = "<div style='padding:25px'><span style='font-size:16px;'>Reporting Datenlieferungen [Archiv]</span><br /><br />Bitte w&auml;hlen Sie eine Lieferung aus, um das Reporting anzuzeigen.</div>";
		} 
		
		if (view == 'import') {
			def_content = "<div style='padding:25px'><span style='font-size:16px;'>Reporting Datenlieferungen [Importe]</span><br /><br />Bitte w&auml;hlen Sie eine Lieferung aus, um das Reporting zu bearbeiten.</div>";
		}
		
		if (view == 'real') {
			def_content = "<div style='padding:25px'><span style='font-size:16px;'>Reporting Datenlieferungen [Realtime]</span><br /><br />Bitte w&auml;hlen Sie eine Lieferung aus, um das Reporting zu bearbeiten.</div>";
		}
		
		if (cnt == 0) {
			def_content = "<div style='padding:25px'><span style='font-size:16px;'>Reporting Datenlieferungen</span><br /><br />Keine Reportings vorhanden.</div>";
		}
	
	document.getElementById("center_content").innerHTML = def_content;
	
	var layoutb = new YAHOO.widget.Layout('global_layout', {
            height: Dom.getClientHeight()-119, //Height of the viewport
            width: Dom.get('global_layout').offsetWidth, //Width of the outer element
            minHeight: 150, //So it doesn't get too small
            units: [
                { position: 'left', width: 302, body: 'left_table', scroll:false, gutter: '-2 0 -1 -1'},
                { position: 'center', width: (Dom.get('global_layout').offsetWidth-368), body: 'center_content', gutter: '-1 -1 0 0'}
            ]
        });
		
			
		
        layoutb.render();
		
        var myColumnDefs = [
            {key:"id", resizeable:true},
			{key:"status", label:"Status", resizeable:true, width: 65},
            {key:"datei", label:"Lieferung",  resizeable:true, sortable:false, width: 165}
        ];
		
		
        var myDataSource = new YAHOO.util.DataSource("limport/report/lieferung_json.php?view="+view);
        myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;
		myDataSource.connXhrMode = "queueRequests";
        myDataSource.responseSchema = { 
	        resultsList: "Result", 
            fields: ["id","status","datei"] 
	     }; 
			
		
		 var myDataTable = new YAHOO.widget.ScrollingDataTable("left_table",
                myColumnDefs, myDataSource, {
					height: (Dom.getClientHeight()-188)+"px",
					width: "301px",
                    selectionMode:"single"
                });
				
        myDataTable.hideColumn("id");
        // Subscribe to events for row selection
        myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
        myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
        myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);
		myDataTable.subscribe("rowSelectEvent", function() { 
		
			/*
			var div = document.getElementById('center_content');
			var handleSuccess = function(o){
				if(o.responseText !== undefined){
					div.innerHTML = o.responseText;
				}
			}
			
			var handleFailure = function(o){
				if(o.responseText !== undefined){
					div.innerHTML = "<ul><li>Transaction id: " + o.tId + "</li>";
					div.innerHTML += "<li>HTTP status: " + o.status + "</li>";
					div.innerHTML += "<li>Status code message: " + o.statusText + "</li></ul>";
				}
			}
			
			var callback =
			{
			  success:handleSuccess,
			  failure:handleFailure
			};
			*/
			var myDataTableData = this.getRecordSet().getRecord(this.getSelectedRows()[0])._oData;
			
			if (view == 'real' || view == 'real_archiv') {
				setRequest_default("limport/report/rep_details_rt.php?id="+myDataTableData.id,"center_content");
			} else {
				setRequest_default("limport/report/rep_details.php?id="+myDataTableData.id,"center_content");
			}
			
			//var sUrl = "kampagne/report/rep_details.php?kid="+myDataTableData.id; 
			//var request = YAHOO.util.Connect.asyncRequest('GET', sUrl, callback);
			
			
	    }); 
		
		
		
        return {
            oDS: myDataSource,
            oDT: myDataTable
        };
	
    };


 
//Event.on(window, 'resize', layoutb.resize, layoutb, true);
height2 = Dom.getClientHeight()-166;
YAHOO.util.Dom.setStyle('center_content', 'height', height2 + 'px');			
function Resize_dm_rep() {
	v = document.getElementById('dm_rep_page').value;
	setRequest('limport/report/index.php?v='+v,'a_rep');
}
window.onresize = Resize_dm_rep;
</script>
