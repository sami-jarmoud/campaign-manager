<?php
include('db_connect.inc.php');
$import_nr = $_GET['import_nr'];


$imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db, $partner_db
							  WHERE $import_datei_db.PARTNER=$partner_db.PID
							  AND $import_datei_db.IMPORT_NR = '$import_nr'
							" ,$verbindung);
							
$imp_zeile = mysql_fetch_array($imp_daten);
$imp_partner = $imp_zeile['Pkz'];
$imp_partner_id = $imp_zeile['PARTNER'];
$imp_datum = $imp_zeile['IMPORT_DATUM'];
$imp_nr = $imp_zeile['IMPORT_NR'];
$imp_datei = $imp_zeile['IMPORT_DATEI'];
$imp_anzahl = $imp_zeile['BRUTTO'];
$imp_mehrfach = $imp_zeile['EMAIL_MEHRFACH'];
$brutto_neu = $imp_anzahl-$imp_mehrfach;
$imp_inoptdb = $imp_zeile['INOPTDB'];
$netto = $brutto_neu-$imp_inoptdb;


$anfang = 0;
$anzahl_datensaetze = 5000;
include('db_optdb_connect.inc.php');
$del_doppelte = mysql_query("DELETE `imp` 
							 FROM `ImpTemp` AS `imp`, `Users` AS `usr` 
							 WHERE `imp`.`Email` = `usr`.`Email`"
							 ,$verbindung_optdb);
$anz_imp = mysql_query("SELECT * FROM `ImpTemp`",$verbindung_optdb);
$anzahl_eintraege = mysql_num_rows($anz_imp);

function imp_users($anfang) {
	include('db_optdb_connect.inc.php');
	global $anfang;
	global $anzahl_datensaetze;
	
	$insert_optdb = mysql_query("INSERT INTO `Users`
							     SELECT NULL ,1,5, `ImpTemp`.*
							     FROM `ImpTemp`
								 LIMIT $anfang,$anzahl_datensaetze"
							     ,$verbindung_optdb);
	$anfang += $anzahl_datensaetze;
}						 


require_once('progressbar/ProgressBar.class.php');
$bar = new ProgressBar();
$bar->setMessage("importiere in optDB...");
$bar->setAutohide(true);
$bar->setSleepOnFinish(1);
$bar->setForegroundColor('#0066FF');


$durchlaeufe = ceil($anzahl_eintraege/$anzahl_datensaetze);
$elements = $durchlaeufe; //total number of elements to process
$bar->initialize($elements); //print the empty bar

for($i=1;$i<=$elements;$i++){
 	imp_users($anfang);
 	$bar->increase(); //calls the bar with every processed element
	$bar->setMessage('Import optDB Fortschritt: '.round((($i/$elements)*100)).'%');
}
?>
<table width="400" cellspacing="0" cellpadding="3" border="1" bordercolor="#CCCCCC" style="border-collapse:collapse">
	<tr><td colspan="3"><strong>Import optDB - Report</strong></td></tr>
	<tr><td colspan='3'>&nbsp;</td></tr>
	<tr><td>Partner</td><td align="right"><?php print $imp_partner; ?></td><td>&nbsp;</td></tr>
	<tr><td>Datei</td><td align="right"><?php print $imp_datei; ?></td><td>&nbsp;</td></tr>
	<tr style="background-color:#0066CC; color:#FFFFFF; font-weight:bold;"><td>Brutto</td><td align="right"><?php print zahl_formatieren($imp_anzahl); ?></td><td>&nbsp;</td></tr>
	<tr><td colspan='2'>&nbsp;</td><td align="right">Quote</td></tr>
	<tr class='bg_weiss'><td>Doppelte Datensätze</td><td align="right"><?php print zahl_formatieren($imp_mehrfach); ?></td><td align="right"><?php print round(($imp_mehrfach/$imp_anzahl)*100,1); ?>%</td></tr>
	<tr class='bg_weiss'><td>Brutto Neu</td><td align="right"><?php print zahl_formatieren($brutto_neu); ?></td><td align="right"><?php print round(($brutto_neu/$imp_anzahl)*100,1); ?>%</td></tr>
	<tr class='bg_weiss'><td>Bereits in optDB</td><td align="right"><?php print zahl_formatieren($imp_inoptdb); ?></td><td align="right"><?php print round(($imp_inoptdb/$brutto_neu)*100,1); ?>%</td></tr>
	<tr style="background-color:#0066CC; color:#FFFFFF; font-weight:bold;"><td>Netto</td><td align="right"><?php print zahl_formatieren($netto); ?></td><td align="right"><?php print round(($netto/$imp_anzahl)*100,1); ?>%</td></tr>
</table>
<br />
