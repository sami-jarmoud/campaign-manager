<?php
include('db_connect.inc.php');
$import_nr = $_GET['import_nr'];

$imp_daten = mysql_query("SELECT *
							  FROM $import_datei_db, $partner_db
							  WHERE $import_datei_db.PARTNER=$partner_db.ID
							  AND $import_datei_db.IMPORT_NR = '$import_nr'
							", $verbindung);
							
$imp_zeile = mysql_fetch_array($imp_daten);
$imp_partner = $imp_zeile['NAME'];
$imp_partner_id = $imp_zeile['PARTNER'];
$imp_datum = $imp_zeile['IMPORT_DATUM'];
$imp_nr = $imp_zeile['IMPORT_NR'];
$imp_datei = $imp_zeile['IMPORT_DATEI'];
$imp_anzahl_formatiert = number_format($imp_zeile['BRUTTO'], 0,0,'.');
$imp_anzahl = $imp_zeile['BRUTTO'];
$bounces = $imp_zeile['BOUNCES'];

$adr_sql = mysql_query("SELECT COUNT(*) AS ADR
						FROM $metaDB_temp
						WHERE ORT!='' AND PLZ!='' AND STRASSE!=''
						", $verbindung);
$adr_zeile = mysql_fetch_array($adr_sql);
$adr = $adr_zeile['ADR'];
$adr_formatiert = number_format($adr, 0,0,'.');

$dbl_sql = mysql_query("SELECT COUNT(DISTINCT $metaDB.EMAIL) AS ANZAHL
						FROM $metaDB, $metaDB_temp
						WHERE $metaDB_temp.EMAIL=$metaDB.EMAIL
						", $verbindung);
$dbl_zeile = mysql_fetch_array($dbl_sql);
$dubletten = $dbl_zeile['ANZAHL'];
$dubletten_formatiert = number_format($dubletten, 0,0,'.');
$netto = $imp_anzahl-$dubletten-$bounces;

$update_sql = mysql_query("UPDATE $import_datei_db SET NETTO='$netto', NETTO_EXT='$netto', LANGADR = '$adr', DUBLETTEN = '$dubletten' WHERE IMPORT_NR = '$import_nr'", $verbindung);

$temp_imp_daten = mysql_query("INSERT INTO $metaDB (
											EMAIL,
											ANREDE,
											VORNAME,
											NACHNAME,
											STRASSE,
											PLZ,
											ORT,
											LAND,
											GEBURTSDATUM,
											TELEFON,
											EINTRAGSDATUM,
											IP,
											HERKUNFT,
											DOI_DATE,
											DOI_IP,
											IMPORT_NR
										) SELECT 
											EMAIL,
											ANREDE,
											VORNAME,
											NACHNAME,
											STRASSE,
											PLZ,
											ORT,
											LAND,
											GEBURTSDATUM,
											TELEFON,
											EINTRAGSDATUM,
											IP,
											HERKUNFT,
											DOI_DATE,
											DOI_IP,
											IMPORT_NR
										FROM $metaDB_temp", $verbindung);
?>
<table width="400" cellspacing="0" cellpadding="3" border="1" bordercolor="#CCCCCC" style="border-collapse:collapse">
	<tr><td colspan="3">Import - Report</td></tr>
	<tr><td colspan='3'>&nbsp;</td></tr>
	<tr><td>Partner</td><td align="right"><?php print $imp_partner; ?></td><td>&nbsp;</td></tr>
	<tr><td>Datei</td><td align="right"><?php print $imp_datei; ?></td><td>&nbsp;</td></tr>
	<tr><td>Brutto</td><td align="right"><?php print $imp_anzahl_formatiert; ?></td><td>&nbsp;</td></tr>
	<tr><td>Postadressen (vollständig)</td><td align="right"><?php print $adr_formatiert; ?></td><td align="right"><?php print round(($adr/$imp_anzahl)*100,1); ?>%</td></tr>
	<tr><td colspan='2'>&nbsp;</td><td align="right">Quote</td></tr>
	<tr class='bg_weiss'><td>Bereits in metaDB</td><td align="right"><?php print $dubletten_formatiert; ?></td><td align="right"><?php print round(($dubletten/$imp_anzahl)*100,1); ?>%</td></tr>
	<tr class='bg_weiss'><td>Bounces</td><td align="right"><?php print $bounces; ?></td><td align="right"><?php print round(($bounces/$imp_anzahl)*100,1); ?>%</td></tr>
	<tr style="background-color:#0066CC; color:#FFFFFF; font-weight:bold;"><td>Netto</td><td align="right"><?php print (number_format(($imp_anzahl-$dubletten), 0,0,'.')); ?></td><td align="right"><?php print round((($imp_anzahl-$dubletten-$bounces)/$imp_anzahl)*100,1); ?>%</td></tr>
</table>
<br />