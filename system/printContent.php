<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$data = $_SESSION['data'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <style type="text/css">
            table {
                background-color:#FFFFFF;
                border-collapse:collapse;
                font-size:10px;
                font-family:arial;
            }
            table td{
                padding:3px;
                border:1px solid #CCCCCC;
            }
            table th{
                padding:5px;
                font-weight:bold;
                background-color:#EAEAEA;
                color:#212121;
                border-bottom:1px solid #333333;
                border-left:1px solid #CCCCCC;
                border-right:1px solid #CCCCCC;
            }
            #hiddeContent {
                display: none;
            }
        </style>
    </head>
    <body onLoad="window.print()">
        <?php echo $data; ?>
    </body>
</html>