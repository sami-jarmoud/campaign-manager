<?php
session_start();
$export_ordner = "temp/";
$temp_datei = "test_export_unsub_".$_SESSION['benutzer_id'].".txt";

if (file_exists($export_ordner.$temp_datei)) {
	unlink($export_ordner.$temp_datei);
}

$filehandle = fopen($export_ordner.$temp_datei, "a");
$datensaetze = $_SESSION['datensaetze_unsub'];

fwrite($filehandle, $datensaetze);
fclose($filehandle);

header("Content-type: application/octet-stream"); 
header("Content-disposition: attachment; filename=abmelder.csv");
readfile($export_ordner.$temp_datei); 
?>
