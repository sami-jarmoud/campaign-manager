<?php
session_start();
header('Content-Type: text/html; charset=ISO-8859-1');
$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
include('../../db_connect.inc.php');
include('../../library/util.php');


$kid = $_GET['kid'];
$sql = mysql_query("SELECT * FROM $kampagne_db WHERE k_id = '$kid'",$verbindung);
$z = mysql_fetch_array($sql);

$asp = $z['versandsystem'];
if ($asp == 'BM') {
	$datum = util::bmTime($z['datum']);
} else {
	$datum = $z['datum'];
}

$klickrate_extern = $z['klickrate_extern'];
$openingrate_extern = $z['openingrate_extern'];
$klickrate_ext = $klickrate_extern;
$openingrate_ext = $openingrate_extern;

if ($klickrate_extern == 0) {
	$klickrate_extern = "&plusmn;0";
	$klickrate_ext = 0;
} 

if ($openingrate_extern == 0) {
	$openingrate_extern = "&plusmn;0";
	$openingrate_ext = 0;
} 



$sql_all = mysql_query("SELECT SUM(versendet) as v, SUM(openings_all) as o, SUM(klicks_all) as k FROM $kampagne_db WHERE nv_id = '$kid'",$verbindung);
$za = mysql_fetch_array($sql_all);

$oRate = util::getQuote($z['gebucht'],$za['o']);
$kRate = util::getQuote($z['gebucht'],$za['k']);

$o_new 	= ($za['o']*$openingrate_ext)/100;
$o_new = $za['o']+$o_new;
$oRate_ext = util::getQuote($z['gebucht'],$o_new);
if ($openingrate_ext > 0) {
	$oRateExt_color = "green";
}

if ($openingrate_ext < 0) {
	$oRateExt_color = "red";
}

$k_new 	= ($za['k']*$klickrate_ext)/100;
$k_new = $za['k']+$k_new;
$kRate_ext = util::getQuote($z['gebucht'],$k_new);
if ($klickrate_ext > 0) {
	$kRateExt_color = "green";
}

if ($klickrate_ext < 0) {
	$kRateExt_color = "red";
}


$kRate_rel = util::getQuote($za['o'],$za['k']);
$kRate_rel_ext = util::getQuote($o_new,$k_new);

$kRate_rel_abs_ext = util::getQuote($z['gebucht'],$k_new);
$kRate_rel_abs = util::getQuote($za['v'],$za['k']);
?>
<input type="hidden" name="kid_rep_edit" id="kid_rep_edit" value="<?php print $kid; ?>" />
<div id="cm_rep" class="yui-navset" style="text-align:left;border:0px;">            
        	<table width="250" class="neu_k" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="3" class='details_hd_info' style="text-align:left">Versandinformationen</td>
				</tr>
                <tr>
                    <td align="right" class='details_info2'>Kampagne</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print $z['k_name']; ?></td>
                </tr>
				<tr>
                    <td align="right" class='details_info'>Agentur</td>
                    <td colspan="2" align="left" class='details2_info'><?php print $z['agentur']; ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'>Anprechpartner</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print $z['ap']; ?></td>
                </tr>
                <tr>
                    <td align="right" class='details_info'>Versanddatum</td>
                    <td colspan="2" align="left" class='details2_info'><?php print $datum; ?></td>

                </tr>
                 <tr>
                    <td align="right" class='details_info2'>Gebucht</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print util::zahl_format($z['gebucht']); ?> (<?php print util::zahl_format($za['v']); ?>)</td>
                </tr>
                 <tr>
                    <td align="right" class='details_info'>Selektion</td>
                    <td colspan="2" align="left" class='details2_info'><?php print util::cutString($z['zielgruppe'], 35); ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'>Abrechnungsart</td>
                    <td colspan="2" align="left" class='details2_info2'><?php print $z['abrechnungsart']; ?></td>
                </tr>
                 <tr>
                    <td align="right" class='details_info'>Zielvorgabe</td>
                    <td colspan="2" align="left" class='details2_info'><?php print "&Ouml;ffnungsrate: ".$z['vorgabe_o']."%, Klickrate: ".$z['vorgabe_k']."%"; ?></td>

                </tr>
                <tr>
                    <td align="right" class='details_info2'></td>
                    <td colspan="2" align="left" class='details2_info2'></td>
                </tr>
                <tr>
					<td colspan="3" class='details_hd_info' style="text-align:left">Performancezahlen</td>
				</tr>
                <tr>
                    <td colspan="3" align="center" class='details_info2'>
                    	<table class="performance_table" style="background-color:#FFFFFF;width:423px" align="center">
                            <tr class="performance_tr" style="color:#000000">
                                <td></td>
                                <td>Intern</td>
                                <td class="rep_zahlen_ext" style="border-top:1px solid #999999">Extern</td>
								<td style="text-align:center">Anpassung in %</td>
                            </tr>
							<tr>
                                <td class="fieldset_label">Empf&auml;nger</td>
                                <td class="performance" style="color:#333333"><?php print util::zahl_format($za['v']); ?></td>
								<td class="performance rep_zahlen_ext" style="color:#333333"><?php print util::zahl_format($z['gebucht']); ?></td>
                                
							</tr>
							<tr>
                                <td class="fieldset_label">&Ouml;ffnungen</td>
                                <td class="performance" style="color:#333333"><?php print util::zahl_format($za['o']); ?></td>
								<td class="performance rep_zahlen_ext"><input type="text" id="o_new" style="background:transparent;border:0px;width:40px;text-align:right;color:#9900FF;" value="<?php print util::zahl_format($o_new); ?>" readonly/></td>
								<td style="text-align:center"><input class="inp_button" type="button" onclick="newRate_o('-5');" value="-5" /> <input class="inp_button" type="button" onclick="newRate_o('-1');" value="-" /> <input type="text" id="oRateExt" name="oRateExt" value="<?php print $openingrate_ext; ?>" style="background:transparent;border:0px;width:25px;text-align:center;color:<?php print $oRateExt_color; ?>" readonly/> <input class="inp_button" type="button" onclick="newRate_o('1');return false" value="+" /> <input class="inp_button" type="button" onclick="newRate_o('5');return false" value="+5" /></td>
                                
							</tr>
							<tr>
                                <td class="fieldset_label">&Ouml;ffnungsrate</td>
                                <td class="performance" style="color:#333333"><?php print $oRate; ?>%</td>
                                <td class="performance rep_zahlen_ext" style="color:#9900FF"><input type="text" id="oRate" style="background:transparent;border:0px;width:30px;text-align:right;color:#9900FF;" value="<?php print $oRate_ext; ?>" readonly/>%</td>
								<td></td>
							</tr>
							<tr>
                                <td class="fieldset_label">Klicks</td>
                                <td class="performance" style="color:#333333"><?php print util::zahl_format($za['k']); ?></td>
                                <td class="performance rep_zahlen_ext" style="color:#9900FF"><input type="text" id="kl_new" style="background:transparent;border:0px;width:40px;text-align:right;color:#9900FF;" value="<?php print util::zahl_format($k_new); ?>" readonly/></td>
								<td style="text-align:center"><input type="button" class="inp_button" onclick="newRate_k('-5');" value="-5" /> <input type="button" class="inp_button" onclick="newRate_k('-1');" value="-" /> <input type="text" id="kRateExt" name="kRateExt" value="<?php print $klickrate_ext; ?>" style="background:transparent;border:1px;width:25px;text-align:center;color:<?php print $kRateExt_color; ?>" readonly/> <input type="button" class="inp_button" onclick="newRate_k('1');" value="+" /> <input type="button" class="inp_button" onclick="newRate_k('5');" value="+5" /></td>
							</tr>
							<tr>
                                <td class="fieldset_label">Klickrate</td>
                                <td class="performance" style="color:#333333"><?php print $kRate_rel_abs; ?>%</td>
                                <td class="performance rep_zahlen_ext" style="color:#9900FF"><input type="text" id="kRate_rel_abs_ext" style="background:transparent;border:1px;width:30px;text-align:right;color:#9900FF;" value="<?php print $kRate_rel_abs_ext; ?>" readonly/>%</td>
								<td></td>
							</tr>
							<tr>
                                <td class="fieldset_label">Klickrate rel.</td>
                                <td class="performance" style="color:#333333"><?php print $kRate_rel; ?>%</td>
                                <td class="performance rep_zahlen_ext" style="color:#9900FF;border-bottom:1px solid #999999"><input type="text" id="kRate_rel_ext" style="background:transparent;border:1px;width:30px;text-align:right;color:#9900FF;" value="<?php print $kRate_rel_ext; ?>" readonly/>%</td>
								<td></td>
							</tr>
                        </table>
                    </td>
                </tr>
            </table>
</div>
<script type="text/javascript">

var o = <?php print $za['o']; ?>;
var k = <?php print $za['k']; ?>;
var gebucht = <?php print $z['gebucht']; ?>;


function newRate_o(step) {

	step = parseInt(step);
	oRateExt = document.getElementById('oRateExt').value;
	newOrateExt = parseInt(oRateExt)+step;
	document.getElementById('oRateExt').value = newOrateExt;
	openingrate_ext = document.getElementById('oRateExt').value;
	if (newOrateExt < 0) {
		document.getElementById('oRateExt').style.color = "red";
	} 
	
	if (newOrateExt > 0) {
		document.getElementById('oRateExt').style.color = "green";
	} 

	o_new = (parseInt(o)*parseInt(openingrate_ext))/100;
	o_new = parseInt(o)+o_new;
	oRate_ext = (o_new/parseInt(gebucht))*100;
	
	o_new = o_new.toFixed(0);
	oRate_ext = oRate_ext.toFixed(2);
	document.getElementById('oRate').value = oRate_ext;
	document.getElementById('o_new').value = Trenner(o_new);
	
	getKrateRel();
}

function newRate_k(step) {

	step = parseInt(step);
	
	kRateExt = document.getElementById('kRateExt').value;
	newKrateExt = parseInt(kRateExt)+step;
	document.getElementById('kRateExt').value = newKrateExt;
	klickrate_ext = document.getElementById('kRateExt').value;
	if (newKrateExt < 0) {
		document.getElementById('kRateExt').style.color = "red";
	} 
	
	if (newKrateExt > 0) {
		document.getElementById('kRateExt').style.color = "green";
	}
	
	
	k_new_ = (parseInt(k)*parseInt(klickrate_ext))/100;
	k_new_ = parseInt(k)+k_new_;
	kRate_ext = (k_new_/parseInt(gebucht))*100;
	
	
	k_new_ = k_new_.toFixed(0);
	kRate_ext = kRate_ext.toFixed(2);
	document.getElementById('kl_new').value = Trenner(k_new_);
	getKrateRel();
}

function getKrateRel() {
	o_n = document.getElementById('o_new').value;
	k_n = document.getElementById('kl_new').value;
	o_n = str_replace(".","",o_n);
	k_n = str_replace(".","",k_n);
	
	kRate_rel_ext =  (parseInt(k_n)/parseInt(o_n))*100;
	kRate_rel_ext = kRate_rel_ext.toFixed(2);
	document.getElementById('kRate_rel_ext').value = kRate_rel_ext;
	
	kRate_rel_abs_ext =  (parseInt(k_n)/parseInt(gebucht))*100;
	kRate_rel_abs_ext = kRate_rel_abs_ext.toFixed(2);
	document.getElementById('kRate_rel_abs_ext').value = kRate_rel_abs_ext;
}
</script>