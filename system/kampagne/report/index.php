<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$mandant = $_SESSION['mandant'];
include('../../db_connect.inc.php');
include('../../library/util.php');


$sql_rep = mysql_query("SELECT *, k.status as status
								FROM $kampagne_db as k
								LEFT JOIN $log as l ON k.k_id = l.kid
								WHERE 1 = 1 
                                                                AND l.timestamp > '2017-01-01 00.00.00'
								AND k.k_id > 1 
								AND k.k_id = k.nv_id 
								AND l.status != 22
								AND (
									(
										k.status = 20 
										OR k.status = 21 
										OR k.status = 23 
										OR k.status = 24 
									) OR ( 
										k.status > 30 
										AND (
											l.status = 20 
											OR l.status = 21 
											OR l.status = 23 
											OR l.status = 24 
										)
									) 
								) 
								GROUP BY k.k_id", $verbindung);
$rep_cnt = mysql_num_rows($sql_rep);
?>
<div id="tab_k_r" class="yui-navset spalten_tab">
    <ul class="yui-nav" style="margin-left:3px;">
        <li class='selected' onclick="setRequest_default('kampagne/report/rep_table.php', 'rep_table2');"><a href="#tab1"><em>Reportings (<?php print $rep_cnt; ?>)</em></a></li>
        <li onclick="setRequest_default('kampagne/report/rep_vorlage.php', 'rep_vorlage_content');"><a href="#tab2"><em>Vorlage</em></a></li>
        <li href="#tab3" onclick="setRequest_default('kampagne/report/rep_archiv.php', 'rep_table3');"><a><em>Archiv</em></a></li>
    </ul>

    <div class="yui-content" style="margin:0px;padding:0px;border-right:0px;border-bottom:0px;">

        <div id="tab1" style="padding-top:5px;"><div id="rep_table2"></div></div>
		<div id="tab2"><div id="rep_vorlage_content"></div></div>
		<div id="tab3" style="padding-top:5px;"><div id="rep_table3"></div></div>   
    </div>
</div>

<script type="text/javascript">
	setRequest_default("kampagne/report/rep_table.php", "rep_table2");

	var tabView_status = new YAHOO.widget.TabView('tab_k_r');
</script>