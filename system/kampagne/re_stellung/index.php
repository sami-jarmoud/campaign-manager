<?php
header('Content-Type: text/html; charset=ISO-8859-1');

session_start();

// debug
function firePhPDebug() {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

$mandant = $_SESSION['mandant'];
$mID = $_SESSION['mID'];
$userID = $_SESSION['benutzer_id'];
$kid = $_GET['kid'];

include('../../library/util.php');
include('../../library/log.php');
include('../../library/campaignManagerWebservice.php');
include('../../library/cm_fillVorlage.php');
include('../../library/mandant.class.php');
include('../../db_pep.inc.php');

$firePhp = firePhPDebug();

// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
    $firePhp->log($_GET, '$_GET');
}

$logManager = new logManager();
$mandantManager = new mandant();

$resBenutzer = mysql_query(
    'SELECT * FROM `benutzer` WHERE `benutzer_id` = "' . $userID . '"',
    $verbindung_pep
);
if ($resBenutzer) {
    $z_user = mysql_fetch_array($resBenutzer);
    mysql_free_result($resBenutzer);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z_user, '$z_user');
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resBenutzer');
    }
}

$resVorlage = mysql_query(
    'SELECT * FROM `mandant` WHERE `id` = "' . $mID . '"',
    $verbindung_pep
);
if ($resVorlage) {
    $z_vorlage = mysql_fetch_array($resVorlage);
    mysql_free_result($resVorlage);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($z_vorlage, '$z_vorlage');
    }
    
    $z_vorlage_mail = fillVorlage(
        $z_vorlage['cm_rechnung_mail'],
        $kid
    );
    $z_vorlage_betreff = fillVorlage(
        $z_vorlage['cm_rechnung_betreff'],
        $kid
    );
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resVorlage');
    }
}

$resEmail = mysql_query(
    'SELECT * FROM `benutzer` WHERE `mid` = "' . $mID . '" AND `email` != "" AND `active` = "1" ORDER BY `vorname` ASC',
    $verbindung_pep
);
if ($resEmail) {
    $tr_empf = null;
    $empf_buchhaltung = null;
    
    $cntIntern = mysql_num_rows($resEmail);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('resEmail');
        
        $firePhp->log($cntIntern, '$cntIntern');
    }
    
    while (($z_e = mysql_fetch_array($resEmail)) !== false) {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($z_e, '$z_e');
        }
        
        if ($z_user[''] == $z_e['']) {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }
        
        $tr_empf .= '
            <tr>
                <td><a href="#" title="&lt;' .$z_e['email'] . '&gt; hinzuf&uuml;gen" onclick="addEmpf(\'' . $z_e['email'] . '\');"' . 
                        ' style="white-space:nowrap" id="rep_email_empf">' . 
                    $z_e['vorname'] . ' ' . $z_e['nachname'] . ' &lt;' . $z_e['email'] . '&gt;' . 
                '</a></td>
            </tr>
        ';
        
        if ($z_e['abteilung'] == 'b') {
            $empf_buchhaltung .= $z_e['email'] . ',';
        }
    }
    mysql_free_result($resEmail);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
} else {
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log(mysql_error($verbindung_pep), 'sql error: $resEmail');
    }
}

if ($empf_buchhaltung) {
    $empf_buchhaltung = substr($empf_buchhaltung, 0, -1);
}

include('../../db_connect.inc.php');

$lastZustellreportArr = $logManager->getLastLogByAction(
    $log,
    6,
    $kid
);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($lastZustellreportArr, '$lastZustellreportArr');
}

if (count($lastZustellreportArr) > 0) {
    $lastZustellreportDate = util::datum_de($lastZustellreportArr['timestamp'],'');
    $lastZustellreportUser = $lastZustellreportArr['userid'];
    $userZustellDataArr = $mandantManager->getUserDataById($lastZustellreportUser);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->log($userZustellDataArr, '$userZustellDataArr');
    }
    
    $lastZustellreportData = '
        <img src="img/Oxygen/16/status/dialog-warning.png" style="margin-bottom:-4px" /> <span style="color:red;font-weight:bold;font-size:10px">Rechnungsstellungsmail bereits versendet: ' . 
            $lastZustellreportDate . ' Uhr von ' . $userZustellDataArr['vorname'] . ' ' . $userZustellDataArr['nachname'] . '</span><br /><br />
    ';
} else {
    $lastZustellreportData = '';
}

$tr_empf_main .= '
    <tr style="background-color:#F2F2F2;color:#9900FF;font-weight:bold">
        <td>Intern (' . $cntIntern . ')</td>
    </tr>' . 
    $tr_empf
;

echo $lastZustellreportData;
?>
<input type="hidden" name="zustell_kid" value="<?php echo $kid; ?>" />
<input type="hidden" name="buchhaltungmail_" id="buchhaltungmail_" value="b" />
<input type="hidden" name="zustellreport_email" id="zustellreport_email" value="1" />
<table class="performance_table">
    <tr>
        <td class="performance" style="vertical-align:top">An:</td>
        <td class="performance" style="text-align:left;">Buchhaltung</td>
    </tr>
    <tr id="empf_liste" style="display:none">
        <td></td>
        <td style="text-align:left">
            <div style="height:100px;overflow:auto;width:350px;background-color:#FFFFFF;padding:2px;margin-left:1px;border:1px solid #CCCCCC; margin-top:-1px;">
                <table class="empf_liste" cellpadding="0" cellspacing="0" border="0">
                    <?php echo $tr_empf_main; ?>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height:5px;"></td>
    </tr>
    <tr>
        <td class="performance">Betreff:</td>
        <td class="performance" style="text-align:left">
            <input type="text" name="vorlage_zustellreport_betreff_edit" id="vorlage_zustellreport_betreff_edit" style="width:350px;padding:3px;" value="<?php echo $z_vorlage_betreff; ?>" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="height:5px;"></td>
    </tr>
    <tr>
        <td class="performance">Von:</td>
        <td class="performance" style="text-align:left;color:#333333;"><?php echo $z_user['email']; ?></td>
    </tr>
</table><br />
<textarea id="vorlage_zustellreport_mail" name="vorlage_zustellreport_mail"><?php echo $z_vorlage_mail; ?></textarea>

<script type="text/javascript">
    if(CKEDITOR.instances['vorlage_zustellreport_mail']) {
        CKEDITOR.remove(CKEDITOR.instances['vorlage_zustellreport_mail']);
    }
    
    CKEDITOR.replace('vorlage_zustellreport_mail', {
        height: 250,
        toolbarStartupExpanded: false,
        toolbar : 'Basic'
    });

    
    function addEmpf(e) {
	var s = document.getElementById('zustellreport_email').value;
	if (s != '') {
            s = s + ',';
        }
	
	e = e + ',';
	var vorhanden = s.search(e);

	if (vorhanden != -1) {
            i = i-1;
	} else {
            s = s + e;
            i = i+1;
	}
	
	document.getElementById('zustellreport_email').value = s;
        
	s2 = document.getElementById('zustellreport_email').value;
	s2 = s2.substring(0, s2.length-1);
        
	document.getElementById('zustellreport_email').value = s2;
	if (s2.length > 40) {
            document.getElementById('zustellreport_email').style.height = '30px';
	}
	
	if (s2.length > 80) {
            document.getElementById('zustellreport_email').style.height = '45px';
	} 
	
	if (s2.length > 120) {
            document.getElementById('zustellreport_email').style.height = '60px';
	}
	
	if (s2.length > 160) {
            document.getElementById('zustellreport_email').style.height = '100px';
	}
	
	if (s2.length <= 40) {
            document.getElementById('zustellreport_email').style.height = '15px';
	}
    }
</script>