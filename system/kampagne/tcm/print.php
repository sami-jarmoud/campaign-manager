<?php
header('Content-Type: text/html; charset=ISO-8859-1');

session_start();

$onLoadAction = ' onLoad="window.print()"';
if ((boolean) $_SESSION['tcmData']['useExtendedPrint'] === true) {
    $onLoadAction = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

        <link rel="stylesheet" type="text/css" media="all" href="css/tcm.css" />
        <link rel="stylesheet" type="text/css" media="all" href="../../css/tcm.css" />
        <link rel="stylesheet" type="text/css" media="all" href="css/tcmTableView.css" />
        <link rel="stylesheet" type="text/css" media="print" href="css/printTcm.css" />
        
        <script type="text/javascript" src="../../javascript/jquery/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.delFeld').click(function() {
                    var feldId = $(this).attr('id');
                    switch (feldId) {
                        case 'targetVolume':
                        case 'bookingAmount':
                        case 'shippedAmount':
                        case 'openingRate':
                        case 'clickRate':
                            $('.kzdgk').attr('colspan', ($('.kzdgk').attr('colspan') - 1));
                            break;
                            
                        case 'date':
                        case 'time':
                        case 'campaign':
                        case 'customerCompany':
                        case 'targetGroup':
                            $('.beforeKzdgk').attr('colspan', ($('.beforeKzdgk').attr('colspan') - 1));
                            break;
                            
                        default:
                            $('.afterKzdgk').attr('colspan', ($('.afterKzdgk').attr('colspan') - 1));
                            break;
                    }
                    
                    if ($('.kzdgk').attr('colspan') === 0) {
                        $('.trKzdgk').fadeOut('fast');
                    }
                    
                    $('.' + feldId).fadeOut('fast');
                });
            });
            
            function printContent() {
                $('.headerEditColumns').remove();
                
                var htmlHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' 
                    + '<html xmlns="http://www.w3.org/1999/xhtml">' 
                    + '<head>' 
                        + '<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />' 
                        + '<link rel="stylesheet" type="text/css" media="all" href="css/tcm.css" />' 
                        + '<link rel="stylesheet" type="text/css" media="all" href="../../css/campaign.css" />' 
                        + '<link rel="stylesheet" type="text/css" media="all" href="css/tcmTableView.css" />' 
                        + '<link rel="stylesheet" type="text/css" media="print" href="css/printTcm.css" />' 
                    + '</head>' 
                    + '<body>'
                ;
                var htmlFooter = '</body></html>';
                var DocumentContainer = document.getElementById('printContentData');
                var WindowObject = window.open(
                    '',
                    'tcmPrint',
                    'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes'
                );
                WindowObject.document.writeln(htmlHeader + DocumentContainer.innerHTML + htmlFooter);
                WindowObject.document.close();
                this.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>
    <body <?php echo $onLoadAction; ?>>
        <?php echo $_SESSION['data']; ?>
    </body>
</html>