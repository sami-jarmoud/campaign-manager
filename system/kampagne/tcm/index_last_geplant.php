<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

// debug
function firePhp () {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

function getKampagneDataArray($kampagneDb, $logDb, $lastWeek, $year, $timestamp, $verbindung) {
    $dataArray = array();
    $firePhp = firePhp();
    
    $res = mysql_query(
        'SELECT *' . 
            ' FROM `' . $kampagneDb . '` as `k`, `' . $logDb . '` as `l`' . 
            ' WHERE `k`.`k_id` = `k`.`nv_id` AND `k`.`k_id` > "1" AND WEEK(`k`.`datum`, 1) = "' . $lastWeek . '" AND YEAR(`k`.`datum`) = "' . $year . '" AND `k`.`k_id` = `l`.`kid`' . 
                ' AND `l`.`action` = "1" AND `l`.`timestamp` <= "' . $timestamp . '"' . 
            ' ORDER BY `k`.`datum` ASC'
        ,
        $verbindung
    );
    if ($res) {
        while(($row = mysql_fetch_array($res)) !== false) {
            $dataArray[] = $row;
        }
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($dataArray, 'dataArray');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getKampagneDataArray()');
        }
    }
    
    return $dataArray;
}

require_once('../../library/ems_package.php');
require_once('../../library/util.php');

$mandant = $_SESSION['mandant'];
$userID = $_SESSION['benutzer_id'];

$firePhp = firePhp();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
}

$userManager = new emsPackage();

$userZugangCheckArr = $userManager->getUserUntermandantID($userID);
if (count($userZugangCheckArr) > 0) {
    $userZugangArr = $userManager->getUserUntermandantID($userID);
} else {
    $userZugangArr = array();
}
$userZugangArr[] = $_SESSION['hauptmandantID'];
asort($userZugangArr);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($userZugangCheckArr, 'userZugangCheckArr');
    $firePhp->log($userZugangArr, 'userZugangArr');
}

$weekNow = date('W', time());
$weekLast = date('W', (time() - 604800));
$jahr = date('Y', time());

$firstdayUnix = util::getKW(
    $weekLast,
    $jahr
);
$lastdayUnix = $firstdayUnix + 518400;
$firstdayKW = date('d.m.Y', $firstdayUnix);
$monday = date('Y-m-d 10:00:00', $firstdayUnix);
$lastdayKW = date('d.m.Y', $lastdayUnix);

$tbl = '
    <h1 style="font-family:arial;font-weight:bold;font-size:16px">Versendungen KW ' . $weekLast . ' (' . $firstdayKW . ' - ' . $lastdayKW . ') - Geplant Stand ' . $firstdayKW . ' 10:00 Uhr</h1><br />
    <table class="tcm_table">
';
$th = '
    <tr>
        <th>Datum</th>
        <th>Kampagne</th>
        <th>Gebucht</th>
        <th>Kunde / Agentur</th>
        <th>Zielgruppe</th>
    </tr>
';

$j = 1;
foreach ($userZugangArr as $mandantID) {
    $mandant = $userManager->getUntermandantAbkz($mandantID);
    $mandantName = $userManager->getUntermandantName($mandantID);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('mandant: ' . $mandant);
        
        $firePhp->log($mandant, 'mandant');
        $firePhp->log($mandantName, 'mandantName');
    }

    $gebucht_gsmt = 0;
    
    include('../../db_connect.inc.php');
    $kampagneDataArray = getKampagneDataArray(
        $kampagne_db,
        $log,
        $weekLast,
        $jahr,
        $monday,
        $verbindung
    );
    $cnt = count($kampagneDataArray);
    if ($cnt == 0) {
        $cnt = 'Keine Kampagnen im aktuellen Zeitraum';
    }
    
    $tr .= '
        <tr style="background-color:#' . $bg_color . '">
            <td colspan="5" class="tcm_mandant">
                <img src="http://maas.treaction.de/system/img/logo/' . $mandant . '.gif" height="20" /> (' . $cnt . ')
            </td>
        </tr>
    ';

    if (intval($cnt) > 0) {
        $tr .= $th;
        
        foreach ($kampagneDataArray as $z) {
            $gebucht_gsmt += $z['gebucht'];
            
            $zg = str_replace('Männer, Frauen,', '', utf8_encode($z['zielgruppe']));
            
            $tr .= '
                <tr style="background-color:#' . $bg_color . '">
                    <td class="tcm_date" style="white-space:nowrap">' . util::getWochentag($z['datum'], 'short') . ' ' . util::datum_vs($z['datum']) . '</td>
                    <td>' . util::cutString($z['k_name'], 25) . '</td>
                    <td align="right">' . util::zahl_format($z['gebucht']) . '</td>
                    <td>' . utf8_decode(util::cutString($z['agentur'], 25)) . '</td>
                    <td>' . utf8_decode(util::cutString($zg, 25)) . '</td>
                </tr>
            ';
        }
        
        $tr .= '
            <tr class="tcm_summe">
                <td colspan="2"></td>
                <td align="right">' . util::zahl_format($gebucht_gsmt) . '</td>
                <td colspan="2"></td>
            </tr>
        ';
    }
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}

$tbl .= $tr . '</table>';

$_SESSION['data'] = $tbl;
require_once('menu.php');
?>