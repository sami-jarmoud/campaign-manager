<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();

// debug
function firePhp () {
    $firePhp = null;
    if (intval($_SESSION['benutzer_id']) === 24) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/system/library/FirePHPCore/FirePHP.class.php');
        $firePhp = FirePHP::getInstance(true);
    }
    
    return $firePhp;
}

function getKampagneDataArray($kampagneDb, $lastWeek, $year, $verbindung) {
    $dataArray = array();
    $firePhp = firePhp();
    
    $res = mysql_query(
        'SELECT *' . 
            ' FROM `' . $kampagneDb . '`' . 
            ' WHERE `k_id` = `nv_id` AND `k_id` > "1" AND WEEK(`datum`, 1) = "' . $lastWeek . '" AND YEAR(`datum`) = "' . $year . '"' . 
            ' ORDER BY `datum` ASC'
        ,
        $verbindung
    );
    if ($res) {
        while(($row = mysql_fetch_array($res)) !== false) {
            $dataArray[] = $row;
        }
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($dataArray, 'dataArray');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getKampagneDataArray()');
        }
    }
    
    return $dataArray;
}

function getKampagneSumDataArray($kampagneDb, $kampagneId, $verbindung) {
    $firePhp = firePhp();
    
    $row = null;
    $res = mysql_query(
        'SELECT SUM(`gebucht`) as `g`, SUM(`versendet`) as `v`, SUM(`openings_all`) as `o`, SUM(`klicks_all`) as `k`' . 
            ' FROM `' . $kampagneDb . '`' . 
            ' WHERE `nv_id` = "' . $kampagneId . '"'
        ,
        $verbindung
    );
    if ($res) {
        $row = mysql_fetch_array($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($row, 'row');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getKampagneSumDataArray()');
        }
    }
    
    return $row;
}

function getKampagneNumRowByNvId($kampagneDb, $nvId, $verbindung) {
    $numRow = 0;
    $firePhp = firePhp();
    
    $res = mysql_query(
        'SELECT *' . 
            ' FROM `' . $kampagneDb . '`' . 
            ' WHERE `nv_id` = "' . $nvId . '"'
        ,
        $verbindung
    );
    if ($res) {
        $numRow = mysql_num_rows($res);
        mysql_free_result($res);
        
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log($numRow, 'numRow');
        }
    } else {
        // debug
        if ($firePhp instanceof FirePHP) {
            $firePhp->log(mysql_error($verbindung), 'sql error: getKampagneNumRowByNvId()');
        }
    }
    
    return $numRow;
}

require_once('../../library/ems_package.php');
require_once('../../library/util.php');

$mandant = $_SESSION['mandant'];
$userID = $_SESSION['benutzer_id'];

$firePhp = firePhp();
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($_SESSION, '$_SESSION');
}

$userManager = new emsPackage();

$userZugangCheckArr = $userManager->getUserUntermandantID($userID);
if (count($userZugangCheckArr) > 0) {
    $userZugangArr = $userManager->getUserUntermandantID($userID);
} else {
    $userZugangArr = array();
}
$userZugangArr[] = $_SESSION['hauptmandantID'];
asort($userZugangArr);
// debug
if ($firePhp instanceof FirePHP) {
    $firePhp->log($userZugangCheckArr, 'userZugangCheckArr');
    $firePhp->log($userZugangArr, 'userZugangArr');
}

$weekNow = date('W', time());
$weekLast = date('W', (time() - 604800));
$jahr = date('Y', time());

$firstdayUnix = util::getKW(
    $weekLast,
    $jahr
);
$lastdayUnix = $firstdayUnix + 518400;
$firstdayKW = date('d.m.Y', $firstdayUnix);
$lastdayKW = date('d.m.Y', $lastdayUnix);

$tbl = '
    <h1 style="font-family:arial;font-weight:bold;font-size:16px">Versendungen KW ' . $weekLast . ' (' . $firstdayKW . ' - ' . $lastdayKW . ') - Real</h1><br />
    <table class="tcm_table">
';
$th = '
    <tr>
        <th>Datum</th>
        <th>Kampagne</th>
        <th>Gebucht</th>
        <th>Versendet</th>
        <th>&Ouml;-Rate</th>
        <th>K-Rate</th>
        <th>&Uuml;SQ</th>
        <th>NV</th>
        <th>Kunde / Agentur</th>
        <th>Zielgruppe</th>
    </tr>
';

$j = 1;
foreach ($userZugangArr as $mandantID) {
    if ($j > 1) {
        $pageBreak = ' style="page-break-before: always"';
    } else {
        $pageBreak = '';
    }
    
    $mandant = $userManager->getUntermandantAbkz($mandantID);
    $mandantName = $userManager->getUntermandantName($mandantID);
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->group('mandant: ' . $mandant);
        
        $firePhp->log($mandant, 'mandant');
        $firePhp->log($mandantName, 'mandantName');
    }

    $gebucht_gsmt = 0;
    $versendet_gsmt = 0;
    $nv_gsmt = 0;

    include('../../db_connect.inc.php');
    $kampagneDataArray = getKampagneDataArray(
        $kampagne_db,
        $weekLast,
        $jahr,
        $verbindung
    );
    $cnt = count($kampagneDataArray);
    if ($cnt == 0) {
        $cnt = 'Keine Kampagnen im aktuellen Zeitraum';
    }
    
    $tr .= '
        <tr' . $pageBreak . '>
            <td colspan="10" class="tcm_mandant">
                <img src="http://maas.treaction.de/system/img/logo/' . $mandant . '.gif" height="20" /> (' . $cnt . ')
            </td>
        </tr>
    ';

    if (intval($cnt) > 0) {
        $tr .= $th;
        
        foreach ($kampagneDataArray as $z) {
            $gebucht_gsmt += $z['gebucht'];
            $kid = $z['k_id'];
            
            $z2 = getKampagneSumDataArray(
                $kampagne_db,
                $kid,
                $verbindung
            );
            $versendet_gsmt += $z2['v'];
            $g_gsmt += $z2['g'];
            $o_gsmt += $z2['o'];
            $k_gsmt += $z2['k'];

            $z3 = getKampagneNumRowByNvId(
                $kampagne_db,
                $kid,
                $verbindung
            );
            $nv_anz = $z3 - 1;
            $nv_gsmt += $nv_anz;

            $q1 = $z2['v'] - $z['gebucht'];
            $usq = round((($q1 / $z['gebucht']) * 100), 0);
            if ($usq < 0 || $usq == 0) {
                $usq = 0;
            }

            $zg = str_replace('Männer, Frauen,', '', utf8_encode($z['zielgruppe']));

            $oQ = util::getQuote(
                $z['gebucht'],
                $z2['o']
            );
            $kQ = util::getQuote(
                $z['gebucht'],
                $z2['k']
            );
            
            $tr .= '
                <tr style="background-color:#' . $bg_color . '">
                    <td class="tcm_date" style="white-space:nowrap">' . util::getWochentag($z['datum'], 'short') . ' ' . util::datum_vs($z['datum']) . '</td>
                    <td>' . util::cutString($z['k_name'], 25) . '</td>
                    <td align="right">' . util::zahl_format($z['gebucht']) . '</td>
                    <td align="right">' . util::zahl_format($z2['v']) . '</td>
                    <td align="right">' . $oQ . '%</td>
                    <td align="right">' . $kQ . '%</td>
                    <td align="right">' . $usq . '%</td>
                    <td align="right">' . $nv_anz . '</td>
                    <td>' . utf8_decode(util::cutString($z['agentur'], 25)) . '</td>
                    <td>' . utf8_decode(util::cutString($zg, 25)) . '</td>
                </tr>
            ';
        }
    }

    $q = $versendet_gsmt - $gebucht_gsmt;
    $usq_gsmt = round((($q / $gebucht_gsmt) * 100), 0);
    if ($usq_gsmt < 0) {
        $usq_gsmt = 0;
    }

    $oQ_gesamt = util::getQuote(
        $g_gsmt,
        $o_gsmt
    );
    $kQ_gesamt = util::getQuote(
        $g_gsmt,
        $k_gsmt
    );

    if ($cnt > 0) {
        $tr .= '
            <tr class="tcm_summe">
                <td colspan="2"></td>
                <td align="right">' . util::zahl_format($gebucht_gsmt) . '</td>
                <td align="right">' . util::zahl_format($versendet_gsmt) . '</td>
                <td align="right">' . $oQ_gesamt . '%</td>
                <td align="right">' . $kQ_gesamt . '%</td>
                <td align="right">' . $usq_gsmt . '%</td>
                <td align="right">' . $nv_gsmt . '</td>
                <td colspan="2"></td>
            </tr>
        ';
    }

    $j++;
    
    // debug
    if ($firePhp instanceof FirePHP) {
        $firePhp->groupEnd();
    }
}

$tbl .= $tr . '</table>';

$_SESSION['data'] = $tbl;
require_once('menu.php');
?>