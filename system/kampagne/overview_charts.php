<?php
session_start();
$mandant = $_SESSION['mandant'];
include('../db_connect.inc.php');
require_once('../library/chart.php');
include('../library/campaignManagerWebservice.php');

$campaignManager = new campaignManagerWebservice();
$getYearsArr = $campaignManager ->getCampaignYear($kampagne_db);


if ($_GET['jahr']) {
	$jahr = $_GET['jahr'];
} else {
	$jahr = date("Y");
}

$ycnt = count($getYearsArr);
for($i = 0; $i < $ycnt; $i++) {
	$year = $getYearsArr[$i]['jahr'];
	if($year == $jahr) {
		$selected = "selected";
	} else {
		$selected = "";
	}
	$yearOpt .= "<option value='".$year."' ".$selected .">".$year."</option>";
}


function zahl_format($zahl) {
	$zahl = number_format($zahl, 0,0,'.');
	return $zahl;
}

function getMonat($m) {

	switch ($m) {
		case 1: return "Jan";
		case 2: return "Feb";
		case 3: return "M&auml;rz";
		case 4: return "Apr";
		case 5: return "Mai";
		case 6: return "Juni";
		case 7: return "Juli";
		case 8: return "Aug";
		case 9: return "Sep";
		case 10: return "Okt";
		case 11: return "Nov";
		case 12: return "Dez";
	}
	
}

function kAll() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$k_sql = mysql_query("SELECT MONTH(datum) as Monat, COUNT(*) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' xAxisName='Monat' yAxisName='Kampagnen' showValues='1' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF' canvasPadding='30'>";		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$monat = getMonat($kz['Monat']);
		$data .= "<set value='".$kz['Anzahl']."' label='".$monat."' color='267CE6' />";
		
	}
	
	$data .= "</chart>";
	return createChart::renderChart('idkOV',$data,'Line','780','150');
}

function getCnt($type) {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	if ($type != 'k') {
		$sql = "AND abrechnungsart = '$type' ";
	}
	
	$k_sql = mysql_query("SELECT *
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1 ".$sql,$verbindung);
	$cnt = mysql_num_rows($k_sql);
	if ($cnt == '') {$cnt = 0;}	
	return $cnt;	  
}

function getQuote($brutto,$netto) {
	$rnd = round((($netto/$brutto)*100),1);
	return $rnd;
}


function kVolumen() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$k_sql = mysql_query("SELECT MONTH(datum) as Monat, SUM(gebucht) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' xAxisName='Monat' yAxisName='Volumen' showValues='1' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF' canvasPadding='30'>";
	$cat = "<categories>";
	$data2 = "<dataset seriesName='Gebucht' color='267CE6'>";
	$data3 = "<dataset seriesName='Versendet' color='9900FF'>";
	
	 
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$monat = getMonat($kz['Monat']);
		
		$cat .= "<category label='".$monat."' />";
		$data2 .= "<set value='".$kz['Anzahl']."' color='267CE6' />";
		
		$nv_sql = mysql_query("SELECT SUM(versendet) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
					      AND MONTH(datum) = '".$kz['Monat']."'
						  ",$verbindung);
		$nvz = mysql_fetch_array($nv_sql);
		$data3 .= "<set value='".$nvz['Anzahl']."' color='9900FF' />";
	}
	
	$data .= $cat."</categories>";
	$data .= $data2."</dataset>";
	$data .= $data3."</dataset>";
	$data .= "</chart>";
	return createChart::renderChart('idkOVv',$data,'MSLine','780','250');
}


function vsVolumenMonth() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$asp_qry = mysql_query("SELECT versandsystem AS vs
							FROM $kampagne_db 
							WHERE YEAR( datum ) = '$jahr'
							GROUP BY vs",$verbindung);
	
	$k_monat_qry = mysql_query("SELECT MONTH(datum) as Monat
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);
						  
	
			  
	$data = "<chart inThousandSeparator='.' inDecimalSeparator='.' numberScaleValue='100000' canvasBorderColor='999999' xAxisName='Monat' yAxisName='Volumen' showValues='1' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF' canvasPadding='30'>";
	$cat = "<categories>";
	while ($kmz = mysql_fetch_array($k_monat_qry)) {
		$monat = getMonat($kmz['Monat']);
		$cat .= "<category label='".$monat."' />";
		$monatArr[] = $kmz['Monat'];
	}	
	
		while ($aspz = mysql_fetch_array($asp_qry)) {
		
			$vs = $aspz['vs'];
			$asp = getASP_name($vs);
			
			$data2 .= "<dataset seriesName='".$asp."'>";
			
			foreach ($monatArr as $m) {
			
				$k_sql = mysql_query("SELECT SUM(versendet) as Anzahl
								  FROM $kampagne_db 
								  WHERE YEAR(datum) = '$jahr'
								  AND versandsystem = '$vs'
								  AND MONTH(datum) = '$m'",$verbindung);
				$kz = mysql_fetch_array($k_sql);
				$data2 .= "<set value='".$kz['Anzahl']."'/>";
			
			}
			
			$data2 .= "</dataset>";
		
		}
		
		
	
	
	
	$data .= $cat."</categories>";
	$data .= $data2;
	$data .= "</chart>";
	return createChart::renderChart('idvsOVv',$data,'MSLine','780','250');
}



function kAB() {
	global $mandant;
	global $jahr;
	include('../db_connect.inc.php');
	
	$k_sql = mysql_query("SELECT abrechnungsart as a, COUNT(*) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY a",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$a = $kz['a'];
		$data .= "<set value='".$kz['Anzahl']."' label='".$a."' />";
		
	}
	
	$data .= "</chart>";
	return createChart::renderChart('idkAB',$data,'Pie3D','375','178');
}

function getASP_name($asp) {
	switch ($asp) {
		case "BM": return "Broadmail";break;
		case "BC": return "Backclick";break;
		case "E": return "Elaine";break;
		case "K": return "Kajomi";break;
		case "MS": return "MailSolution";break;
	}
}

function kVS() {
	global $mandant;
	global $jahr;
	global $gesamt_volumen;
	global $tr_volumen;
	global $tr_performance;
	global $data2_chart;
	
	include('../db_connect.inc.php');
	
	$cnt_sql = mysql_query("SELECT SUM(versendet) as gsmt
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  ",$verbindung);
	$gsmtz = mysql_fetch_array($cnt_sql);
	$gesamt_volumen = $gsmtz['gsmt'];
						  
	$k_sql = mysql_query("SELECT versandsystem as v, COUNT(*) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY v
						  ORDER BY Anzahl DESC",$verbindung);
			  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$data2 = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$data2_cat = "<categories>";
	$data2_dataset = "<dataset seriesName='&Ouml;ffnungsrate'>";
	$data2_dataset2 = "<dataset seriesName='Klickrate'>";
		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$v = $kz['v'];
		$asp_name = getASP_name($kz['v']);
		
		$cnt_asp_sql = mysql_query("SELECT SUM(versendet) as gsmt_asp, SUM(openings) as o, SUM(klicks) as k
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND versandsystem = '$v'
						  ",$verbindung);
		$kz2 = mysql_fetch_array($cnt_asp_sql);		
		$gesamt_asp = $kz2['gsmt_asp'];
		$gesamt_o = $kz2['o'];		
		$gesamt_k = $kz2['k'];		  
		
		if ($gesamt_asp > 0) {
		
		
			$data .= "<set value='".$gesamt_asp."' label='".$asp_name."' />";
			$data2_dataset .= "<set value='".getQuote($gesamt_asp,$gesamt_o)."' />";
			$data2_dataset2 .= "<set value='".getQuote($gesamt_asp,$gesamt_k)."' />";
			
			$data2_cat .= "<category label='".$asp_name."' />";
			
			$tr_volumen .= '<tr class="ovTR1">
					<td class="ovTR1_label">'.$asp_name.'</td>
					<td class="ovTR1_label2">'.zahl_format($gesamt_asp).'</td>
					<td class="ovTR1_label3">'.getQuote($gesamt_volumen,$gesamt_asp).'%</td>
				</tr>';
				
			$tr_performance .= '<tr class="ovTR1">
					<td class="ovTR1_label">'.$asp_name.'</td>
					<td class="ovTR1_label2">&#216; '.getQuote($gesamt_asp,$gesamt_o).'%</td>
					<td class="ovTR1_label2">&#216; '.getQuote($gesamt_asp,$gesamt_k).'%</td>
				</tr>';
		}
		
		
	}
	
	$data .= "</chart>";
	
	
	$data2_cat .= "</categories>";
	$data2_dataset .= "</dataset>";
	$data2_dataset2 .= "</dataset>";
	$data2 .= $data2_cat.$data2_dataset.$data2_dataset2."</chart>";
	$data2_chart = createChart::renderChart('idkVSp',$data2,'MSColumn3D','378','178');
	
	return createChart::renderChart('idkVS',$data,'Pie3D','378','178');
}

function getASPquote() {
	global $mandant;
	global $jahr;
	global $gesamt_versendet;
	global $gesamt_openings;
	global $gesamt_klicks;
	include('../db_connect.inc.php');

	$cnt_asp_sql = mysql_query("SELECT SUM(versendet) as gsmt, SUM(openings) as o, SUM(klicks) as k
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  ",$verbindung);
						  
	$kz2 = mysql_fetch_array($cnt_asp_sql);
		$gesamt_versendet = $kz2['gsmt'];
		$gesamt_openings = $kz2['o'];		
		$gesamt_klicks = $kz2['k'];		
}

getASPquote();
?>

<div class="ovHeader">
	&Uuml;bersicht<br />
	<span style="font-size:18px;color:#8e8901">Kampagnen - <select onchange="changeYear(this.value);" style="font-size:18px;color:#8e8901;"><?php print $yearOpt; ?></select></span>
</div>

<div style="width:850px;margin:auto;">
<div id="kOV">
    <div class="kOV_table">
    	<table class="ovTable" width="800">
        	<tr>
            	<td class="ovBlock">Kampagnen pro Monat</td>
            </tr>
        	<tr>
            	<td><?php print kAll(); ?></td>
            </tr>     	
        </table>
    </div>
    
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="150">
        	<tr>
            	<td class="ovBlock">Abrechnungsart</td>
            </tr>
        	<tr>
            	<td><?php print kAB(); ?></td>
            </tr>     	
        </table>
    </div>
	
	<div class="kOV_table">
    	<table class="ovTable" width="393">
        	<tr>
            	<td colspan="4" class="ovBlock">&nbsp;</td>
            </tr>
        	<tr>
            	<td class="ovTR" width="130"><?php print $jahr ?></td>
                <td class="ovTR" align="right">Anzahl</td>
                <td class="ovTR" align="right">Quote</td>
            </tr>
            <tr class="ovTR1">
                <td class="ovTR1_label">TKP</td>
                <td class="ovTR1_label2"><?php print getCnt('TKP'); ?></td>
                <td class="ovTR1_label3"><?php print getQuote(getCnt('k'),getCnt('TKP')); ?>%</td>
            </tr>
            <tr class="ovTR1">
                <td class="ovTR1_label">CPL</td>
                <td class="ovTR1_label2"><?php print getCnt('CPL'); ?></td>
                <td class="ovTR1_label3"><?php print getQuote(getCnt('k'),getCnt('CPL')); ?>%</td>
            </tr>
            <tr class="ovTR1">
                <td class="ovTR1_label">CPO</td>
                <td class="ovTR1_label2"><?php print getCnt('CPO'); ?></td>
                <td class="ovTR1_label3"><?php print getQuote(getCnt('k'),getCnt('CPO')); ?>%</td>
            </tr>
            <tr class="ovTR1">
                <td class="ovTR1_label">CPC</td>
                <td class="ovTR1_label2"><?php print getCnt('CPC'); ?></td>
                <td class="ovTR1_label3"><?php print getQuote(getCnt('k'),getCnt('CPC')); ?>%</td>
            </tr>
            <tr class="ovTR1">
                <td class="ovTR1_label">Hybrid</td>
                <td class="ovTR1_label2"><?php print getCnt('Hybri'); ?></td>
                <td class="ovTR1_label3"><?php print getQuote(getCnt('k'),getCnt('Hybri')); ?>%</td>
            </tr>
			<tr class="ovTR1">
            	<td class="ovTR">Gesamt</td>
                <td class="ovTR" align="right"><?php print getCnt('k'); ?></td>
                <td class="ovTR"></td>
            </tr>
        </table>
    </div>
	
	
    <div class="kOV_table">
    	<table class="ovTable" width="800">
        	<tr>
            	<td class="ovBlock">Versandvolumen Gesamt</td>
            </tr>
        	<tr>
            	<td ><?php print kVolumen(); ?></td>
            </tr>
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovBlock">Versandsysteme / Versandvolumen pro Monat</td>
            </tr>
        	<tr>
            	<td><?php print vsVolumenMonth(); ?></td>
            </tr>     	
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovBlock">Versandsysteme / Aufteilung Versandvolumen</td>
            </tr>
        	<tr>
            	<td><?php print kVS(); ?></td>
            </tr>     	
        </table>
    </div>
	
	
	
	<div class="kOV_table" style="float:left;margin-right:20px;">
    	<table class="ovTable" width="150">
        	<tr>
            	<td class="ovBlock">Versandsysteme / Performance</td>
            </tr>
        	<tr>
            	<td><?php print $data2_chart; ?></td>
            </tr>     	
        </table>
    </div>
		
	<div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovTR" width="100"><?php print $jahr ?></td>
                <td class="ovTR" align="right">Versandvolumen</td>
                <td class="ovTR" align="right">Quote</td>
            </tr>
            <?php print $tr_volumen; ?>
			<tr class="ovTR1">
            	<td class="ovTR">Gesamt</td>
                <td class="ovTR" align="right"><?php print zahl_format($gesamt_volumen); ?></td>
                <td class="ovTR"></td>
            </tr>
        </table>
    </div>
	
	<div class="kOV_table" style="float:left;margin-right:20px;margin-top:-1px">
    	<table class="ovTable" width="390">
        	<tr>
            	<td class="ovTR" width="100"><?php print $jahr ?></td>
                <td class="ovTR" align="right">&Ouml;ffnungsrate</td>
                <td class="ovTR" align="right">Klickrate</td>
            </tr>
            <?php print $tr_performance; ?>
			<tr class="ovTR1">
            	<td class="ovTR">Gesamt</td>
                <td class="ovTR" align="right">&#216; <?php print getQuote($gesamt_versendet,$gesamt_openings); ?>%</td>
                <td class="ovTR" align="right">&#216; <?php print getQuote($gesamt_versendet,$gesamt_klicks); ?>%</td>
            </tr>
        </table>
    </div>
	
</div>
</div>
<div class="ovFooter"></div>
<script>
function changeYear(y) {
	setRequest('kampagne/overview_charts.php?jahr='+y,'cm_ov');
}
</script>
