<?php
require_once('../emsConfig.php');
require_once('../EmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();


if (isset($_SESSION['csvExportData']) 
	&& \count($_SESSION['csvExportData']['data']) > 0
) {
	\header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	\header('Content-Description: File Transfer');
	\header('Content-type: text/csv');
	\header('Content-Disposition: attachment; filename=csvExport.csv');
	\header('Expires: 0');
	\header('Pragma: public');

	$handle = @\fopen('php://output', 'w');

	// Add a header row if it hasn't been added yet
	\fputcsv(
		$handle,
		$_SESSION['csvExportData']['header']
	);
	foreach ($_SESSION['csvExportData']['data'] as $item) {
		\fputcsv(
			$handle,
			$item
		);
	}
	\fclose($handle);

	unset($_SESSION['csvExportData']);
	exit;
} else {
	echo 'Keine Daten f�r den Export gefunden!';
}