<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


for ($day = 1; $day <= $monthCountDay; $day++) {
	// debug
	$debugLogManager->beginGroup('tag - ' . $day);
	
	$bgColorFest = $bgColor = ($day % 2 ? '#E4E4E4' : '#FFFFFF');
	
	$campaignQueryPartsDataArray['WHERE']['day'] = array(
		'sql' => 'DAY(`datum`)',
		'value' => $day,
		'comparison' => 'integerEqual'
	);
	$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaignQueryPartsDataArray);
	if (\count($campaignsDataArray) > 0) {
		foreach ($campaignsDataArray as $campaignEntity) {
			/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
			
			$campaignType = 'HV';
			$allCampaignsDataArray = array();
			
			$timestamp2 = $timestamp;
			$timestamp = $campaignEntity->getDatum()->format('d.m.Y');
			$actWeekDay = \DateUtils::getWeekDay(
				$campaignEntity->getDatum(),
				true
			);

			if ($actWeekDay == 'Sa.' || $actWeekDay == 'So.') {
				$bgColorFest = $bgColor = '#C4DDFF';
			}

			$dateTodayHeader = '';
			if ($timestamp != $timestamp2) {
				$dateTodayHeader = $actWeekDay . ', ' . $timestamp;
			}

			$isToday = \DateUtils::isDateToday($campaignEntity->getDatum());
			if ((boolean) $isToday === true) {
				$bgTodayStyle = 'background-color:#E63C3C; color:#FFFFFF';
				$dateTodayHeader = 'Heute';
				
				if (!$dayAnkerSet) {
					$dateTodayHeader .= '<a href="#" id="HEUTE"></a>';
					$dayAnkerSet = true;
				}
			} else {
				$bgTodayStyle = 'background-color:' . $bgColor;
			}
			
			
			$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
			// debug
			$debugLogManager->logData('campaignEntity', $campaignEntity);
			
			
			/**
			 * getNvCampaignsCountByKid
			 * 
			 * debug
			 */
			$nvCampaignsDataArray = $campaignManager->getNVCampaignsAndCustomerDataItemsByKid($campaignEntity->getK_id());
			#$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);
			$nvCampaignsCount = \count($nvCampaignsDataArray);
			$debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
			if ($nvCampaignsCount > 0) {
				$allCampaignsDataArray = array(
					'versendet' => 0,
					'openings' => 0,
					'openings_all' => 0,
					'klicks' => 0,
					'klicks_all' => 0,
					'leads' => 0,
					'leads_u' => 0,
					'abmelder' => 0,
					'hbounces' => 0,
					'sbounces' => 0,
					'unit_price' => 0,
					'totalPrice' => 0
				);
				
				foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
					/* @var $nvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
					
					// debug
					$debugLogManager->beginGroup('nvCampaignView');
					
					if (\strlen($nvCampaignEntity->getAbrechnungsart()) === 0) {
						$nvCampaignEntity->setAbrechnungsart($campaignEntity->getAbrechnungsart());
					} else {
						$nvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$nvCampaignEntity->getAbrechnungsart()]);
					}
					
					// TODO: evtl. preis, preis2, unit_price
					
					// debug
					$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);
					
					$allCampaignsDataArray['versendet'] += $nvCampaignEntity->getVersendet();
					$allCampaignsDataArray['openings'] += $nvCampaignEntity->getOpenings();
					$allCampaignsDataArray['openings_all'] += $nvCampaignEntity->getOpenings_all();
					$allCampaignsDataArray['klicks'] += $nvCampaignEntity->getKlicks();
					$allCampaignsDataArray['klicks_all'] += $nvCampaignEntity->getKlicks_all();
					$allCampaignsDataArray['leads'] += $nvCampaignEntity->getLeads();
					$allCampaignsDataArray['leads_u'] += $nvCampaignEntity->getLeads_u();
					$allCampaignsDataArray['abmelder'] += $nvCampaignEntity->getAbmelder();
					$allCampaignsDataArray['hbounces'] += $nvCampaignEntity->getHbounces();
					$allCampaignsDataArray['sbounces'] += $nvCampaignEntity->getSbounces();
					
					$allCampaignsDataArray['unit_price'] += $nvCampaignEntity->getUnit_price();
					$allCampaignsDataArray['totalPrice'] += \FormatUtils::totalPriceCalculationByBillingType($nvCampaignEntity);
					
					// debug
					$debugLogManager->endGroup();
				}
			}
			unset($nvCampaignsDataArray);
			
			if (\count($allCampaignsDataArray) > 0) {
				$campaignEntity->setVersendet($allCampaignsDataArray['versendet']);
				$campaignEntity->setOpenings($allCampaignsDataArray['openings']);
				$campaignEntity->setOpenings_all($allCampaignsDataArray['openings_all']);
				$campaignEntity->setKlicks($allCampaignsDataArray['klicks']);
				$campaignEntity->setKlicks_all($allCampaignsDataArray['klicks_all']);
				$campaignEntity->setLeads($allCampaignsDataArray['leads']);
				$campaignEntity->setLeads_u($allCampaignsDataArray['leads_u']);
				$campaignEntity->setAbmelder($allCampaignsDataArray['abmelder']);
				$campaignEntity->setSbounces($allCampaignsDataArray['sbounces']);
				$campaignEntity->setHbounces($allCampaignsDataArray['hbounces']);
				
				// debug
				$debugLogManager->logData('allCampaignsDataArray', $allCampaignsDataArray);
			}
			unset($allCampaignsDataArray);

			require('viewData/viewData_list.php');
			$tableData .= $contentList;
			
			$recipientDataArray['gebucht'] += $campaignEntity->getGebucht();
			
			$csvItemCount++;
		}
	} else {
		require('viewData/viewData_emptyDay.php');
		$tableData .= $tableEmptyDayContent;
	}
	
	// debug
	$debugLogManager->endGroup();
}