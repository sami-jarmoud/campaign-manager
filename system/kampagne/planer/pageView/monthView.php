<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */


for ($day = 1; $day <= $monthCountDay; $day++) {
	// debug
	$debugLogManager->beginGroup('tag - ' . $day);
	
	$bgColorFest = $bgColor = ($day % 2 ? '#E4E4E4' : '#FFFFFF');
	
	$campaignQueryPartsDataArray['WHERE']['day'] = array(
		'sql' => 'DAY(`datum`)',
		'value' => $day,
		'comparison' => 'integerEqual'
	);
	$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaignQueryPartsDataArray);
	if (\count($campaignsDataArray) > 0) {
		foreach ($campaignsDataArray as $campaignEntity) {
			/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */
			
			$timestamp2 = $timestamp;
			$timestamp = $campaignEntity->getDatum()->format('d.m.Y');
			$actWeekDay = \DateUtils::getWeekDay(
				$campaignEntity->getDatum(),
				true
			);

			if ($actWeekDay == 'Sa.' || $actWeekDay == 'So.') {
				$bgColorFest = $bgColor = '#C4DDFF';
			}

			$dateTodayHeader = '';
			if ($timestamp != $timestamp2) {
				$dateTodayHeader = $actWeekDay . ', ' . $timestamp;
			}

			$isToday = \DateUtils::isDateToday($campaignEntity->getDatum());
			if ((boolean) $isToday === true) {
				$bgTodayStyle = 'background-color:#E63C3C; color:#FFFFFF';
				$dateTodayHeader = 'Heute';
				
				if (!$dayAnkerSet) {
					$dateTodayHeader .= '<a href="#" id="HEUTE"></a>';
					$dayAnkerSet = true;
				}
			} else {
				$bgTodayStyle = 'background-color:' . $bgColor;
			}

			$campaignType = 'HV';
			if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
				$campaignType = 'NV';

				/**
				 * getCampaignAndCustomerDataItemById
				 * 
				 * debug
				 */
				$hvCampaignEntity = $campaignManager->getCampaignAndCustomerDataItemById($campaignEntity->getNv_id());
				if (!($hvCampaignEntity instanceof \CampaignWidthCustomerAndContactPersonEntity)) {
					throw new \DomainException('no CampaignWidthCustomerAndContactPersonEntity');
				}
				$debugLogManager->logData('hvCampaignEntity', $hvCampaignEntity);

				$campaignEntity->setCustomerEntity($hvCampaignEntity->getCustomerEntity());
				$campaignEntity->setContactPersonEntity($hvCampaignEntity->getContactPersonEntity());

				if (\strlen($campaignEntity->getAbrechnungsart()) === 0) {
					$campaignEntity->setAbrechnungsart($hvCampaignEntity->getAbrechnungsart());
				}

				unset($hvCampaignEntity);
			}

			$campaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);

			// debug
			$debugLogManager->logData('campaignType', $campaignType);
			$debugLogManager->logData('campaignEntity', $campaignEntity);

			require('viewData/viewData_list.php');
			$tableData .= $contentList;
			
			if ($campaignEntity->getK_id() === $campaignEntity->getNv_id()) {
				$recipientDataArray['gebucht'] += $campaignEntity->getGebucht();
			}
			
			$csvItemCount++;
		}
	} else {
		require('viewData/viewData_emptyDay.php');
		$tableData .= $tableEmptyDayContent;
	}
	
	// debug
	$debugLogManager->endGroup();
}