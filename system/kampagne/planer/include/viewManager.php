<?php
/* @var $debugLogManager \debugLogManager */
/* @var $clientManager \ClientManager */


switch ($abrTyp) {
    case 'cpx':
        $abrTypSql = ' AND `abrechnungsart` LIKE "CP%" ';
        $abr_selected_cpx = 'selected="selected"';
        break;
    
    case 'cpx_o':
        $abrTypSql = ' AND `abrechnungsart` LIKE "CP%" AND (`leads_u` = 0 AND `preis_gesamt_verify` = 0)';
        $abr_selected_cpx_o = 'selected="selected"';
        break;
    
    case 'tkp':
        $abrTypSql = ' AND `abrechnungsart` = "TKP"';
        $abr_selected_tkp = 'selected="selected"';
        break;
    
    case 'hyb':
        $abrTypSql = ' AND `abrechnungsart` = "Hybri"';
        $abr_selected_hyb = 'selected="selected"';
        break;
    
    case 'hyb_o':
        $abrTypSql = ' AND `abrechnungsart` = "Hybri" AND (`leads_u` = 0 OR (`preis_gesamt` != "" AND `preis_gesamt_verify` = 0))';
        $abr_selected_hyb_o = 'selected="selected"';
        break;
    
    default:
		$abrTypSql = '';
        $abr_selected_def = 'selected="selected"';
        break;
}


$viewParameter = 'view=' . $view;
$abrTypParameter = 'abrTyp=' . $abrTyp;
$deliverySystemParameter = 'deliverySystem=' . $deliverySystem;
$deliverySystemDistributorParameter = 'deliverySystemDistributor=' . $deliverySystemDistributor;

/**
 * $deliverySystemOptionItems
 */
$deliverySystem_selected = 'selected="selected"';
$deliverySystemFound = false;
$deliverySystemOptionItems = '';
foreach ($_SESSION['deliverySystemsDataArray'] as $deliverySystemEntiy) {
	/* @var $deliverySystemEntiy \DeliverySystemEntity */
	
	$selected = '';
	if ($deliverySystemEntiy->getAsp_abkz() == $deliverySystem) {
		$selected = 'selected';
		$deliverySystem_selected = '';
		$deliverySystemFound = true;

		$abrTypSql .= ' AND `versandsystem` = "' . mysql_real_escape_string($deliverySystem) . '" ';
	}

	$deliverySystemOptionItems .= \HtmlFormUtils::createOptionItem(
		array(
			'value' => 'kampagne/planer/index.php?' 
				. $viewParameter 
				. '&' . $abrTypParameter 
				. '&deliverySystem=' . $deliverySystemEntiy->getAsp_abkz()
			,
			'selected' => $selected
		),
		$deliverySystemEntiy->getAsp()
	);
}


/**
 * deliverySystemDistributorOptionItems
 */
$deliverySystemDistributor_selected = 'selected="selected"';
$deliverySystemDistributorOptionItems = '';
if ($deliverySystemFound === true) {
	try {
		// TODO: anpassen
		$deliverySystemDistributorDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientIdAndDeliverySystemAsp(
			$_SESSION['mID'],
			$deliverySystem,
			$_SESSION['underClientIds']
		);
		$debugLogManager->logData('deliverySystemDistributorDataArray', $deliverySystemDistributorDataArray);
		
		if (\count($deliverySystemDistributorDataArray) > 0) {
			foreach($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
				/* @var $deliverySystemDistributorEntity \DeliverySystemDistributorEntity */
				
				$selected = '';
				if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributor) {
					$selected = 'selected';
					$deliverySystemDistributor_selected = '';

					$abrTypSql .= ' AND `dsd_id` = ' . $deliverySystemDistributorEntity->getId();
				}
				
				if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributorEntity->getParent_id()) {
					$optgroupDataArray = \HtmlFormUtils::createOptgroupDataArray(
						\CampaignAndOthersUtils::getParentClientDeliverySystemDistributorTitle($deliverySystemDistributorEntity->getTitle())
					);
					
					// openSelectOptgroup
					$deliverySystemDistributorOptionItems .= \HtmlFormUtils::openSelectOptgroup(
						$key,
						$optgroupDataArray
					);
				}

				$deliverySystemDistributorOptionItems .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => 'kampagne/planer/index.php?' 
							. $viewParameter
							. '&' . $abrTypParameter 
							. '&' . $deliverySystemParameter 
							. '&deliverySystemDistributor=' . $deliverySystemDistributorEntity->getId() . '" '
						,
						'selected' => $selected
					),
					$deliverySystemDistributorEntity->getTitle()
				);
				
				if (\count($deliverySystemDistributorDataArray) === $key) {
					// closeSelectOptgroup
					$deliverySystemDistributorOptionItems .= \HtmlFormUtils::closeSelectOptgroup($optgroupDataArray);
					unset($optgroupDataArray);
				}
			}
		}
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
		
		echo $exceptionMessage;
	}
}


/**
 * $pAbrTypOption
 */
$pAbrTypDataArray = array(
	'week' => 'KW (nur HV)',
	'month' => 'Monat (HV und NV)',
	'monthSum' => 'Monat (nur HV, NV sum.)'
);
$pAbrTypOption = '';
foreach ($pAbrTypDataArray as $key => $item) {
	$selected = '';
	if ($view === $key) {
		$selected = 'selected';
	}
	
	$pAbrTypOption .= \HtmlFormUtils::createOptionItem(
		array(
			'value' => 'kampagne/planer/index.php?view=' . $key 
				. '&' . $abrTypParameter 
				. '&' . $deliverySystemParameter 
				. '&' . $deliverySystemDistributorParameter
			,
			'selected' => $selected
		),
		$item
	);
}