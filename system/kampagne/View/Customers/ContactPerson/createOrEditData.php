<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * getAllSalesManagerItemsDepartments
 * 
 * @param \ClientManager $clientManager
 * @param integer $mandantId
 * @param \ContactPersonEntity $contactPersonEntity
 * @return string
 */
function getAllSalesManagerItemsDepartments(\ClientManager $clientManager, $mandantId, \ContactPersonEntity $contactPersonEntity) {
	$result = '';

	try {
		/**
		 * getDepartmentsUsersDataItemsByMandantId
		 */
		$usersDataArray = $clientManager->getDepartmentsUsersDataItemsByMandantId(
			$mandantId,
			'v,g',
			true
		);
		if (\count($usersDataArray) > 0) {
			foreach ($usersDataArray as $userEntity) {
				/* @var $userEntity UserEntity */

				$selected = '';
				if ($userEntity->getBenutzer_id() === $contactPersonEntity->getVertriebler_id()) {
					$selected = 'selected';
				}

				$result .= \HtmlFormUtils::createOptionItem(
					array(
						'value' => $userEntity->getBenutzer_id(),
						'selected' => $selected
					),
					$userEntity->getVorname() . ' ' . $userEntity->getNachname() . '<span style="color:#666666">(' . $userEntity->getMandant() . ')</span>'
				);
			}
		}
		unset($usersDataArray);
	} catch (\Exception $e) {
		require(DIR_configs . 'exceptions.php');
	}

	return $result;
}



$mandant = $_SESSION['mandant'];
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');



/**
 * init $debugLogManager
 * init $campaignManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


try {
	$customerId = isset($_GET['customer']['id']) ? \intval($_GET['customer']['id']) : 0;
	if ($customerId === 0) {
		throw new \InvalidArgumentException('invalid customerId');
	}
	$debugLogManager->logData('customerId', $customerId);
	
	$contactPersonId = isset($_GET['contactPerson']['id']) ? \intval($_GET['contactPerson']['id']) : 0;
	$debugLogManager->logData('contactPersonId', $contactPersonId);
	
	/**
	 * getCustomerDataItemById
	 * 
	 * debug
	 */
	$customerEntity = $campaignManager->getCustomerDataItemById($customerId);
	if (!($customerEntity instanceof \CustomerEntity)) {
		throw new \DomainException('invalid CustomerEntity');
	}
	$debugLogManager->logData('customerEntity', $customerEntity);
	
	if ($contactPersonId > 0) {
		/**
		 * getContactPersonDataItemById
		 */
		$contactPersonEntity = $campaignManager->getContactPersonDataItemById($contactPersonId);
		if (!($contactPersonEntity instanceof \ContactPersonEntity)) {
			throw new \DomainException('invalid ContactPersonEntity');
		}
		
		$actionType = 'editContactPersonData';
	} else {
		$contactPersonEntity = new \ContactPersonEntity();
		$contactPersonEntity->setVertriebler_id($_SESSION['benutzer_id']);
		
		$actionType = 'addContactPersonData';
	}
	
	$genderDataArray = array(
		'Herr' => ($contactPersonEntity->getAnrede() != 'Frau' ? 'checked' : ''),
		'Frau' => ($contactPersonEntity->getAnrede() == 'Frau' ? 'checked' : '')
	);
	
	// debug
	$debugLogManager->logData('contactPersonEntity', $contactPersonEntity);
	$debugLogManager->logData('actionType', $actionType);
	$debugLogManager->logData('genderDataArray', $genderDataArray);
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
?>
<input type="hidden" name="customer[kunde_id]" id="kunde_id2" value="<?php echo $customerId; ?>" />
<input type="hidden" name="contactPerson[ap_id]" id="ap_id" value="<?php echo $contactPersonId; ?>" />
<input type="hidden" name="customerOrContactPerson[action]" id="action" value="<?php echo $actionType; ?>" />
<table>
	<tr>
    	<td class="details_info">Vertriebler*</td>
        <td class="details2_info">
			<select name="contactPerson[vertriebler_id]" id="contactPersonVertriebler" style="width:89%">
				<option value="">- bitte ausw&auml;hlen -</option>
				<?php 
				echo \getAllSalesManagerItemsDepartments(
					$clientManager,
					\intval($_SESSION['mID']),
					$contactPersonEntity
				);
				?>
			</select>
		</td>
    </tr>
    <tr>
    	<td class="details_info2">Anrede</td>
        <td class="details2_info2" style="white-space: normal;">
			<?php
			echo \HtmlFormUtils::createRadioboxItemWidthLabel(
				array(
					'name' => 'contactPerson[anrede]',
					'id' => 'contactPersonGender_1',
					'value' => 'Herr',
					'class' => 'floatLeft',
					'checked' => $genderDataArray['Herr'],
				),
				'Herr',
				array(
					'for' => 'contactPersonGender_1',
					'style' => 'color: #000000; padding: 0 15px 2px 10px; clear: none; font-weight: normal;',
					'class' => 'widthAuto'
				)
			);
			echo \HtmlFormUtils::createRadioboxItemWidthLabel(
				array(
					'name' => 'contactPerson[anrede]',
					'id' => 'contactPersonGender_2',
					'value' => 'Frau',
					'class' => 'floatLeft',
					'checked' => $genderDataArray['Frau'],
				),
				'Frau',
				array(
					'for' => 'contactPersonGender_2',
					'style' => 'color: #000000; padding: 0 0 2px 10px; clear: none; font-weight: normal;',
					'class' => 'widthAuto'
				)
			);
			?>
			<br style="clear: both;" />
		</td>
    </tr>
    <tr>
    	<td class="details_info">Titel</td>
        <td class="details2_info"><input type="text" name="contactPerson[titel]" id="contactPersonTitle" value="<?php echo $contactPersonEntity->getTitel(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Vorname*</td>
        <td class="details2_info2"><input type="text" name="contactPerson[vorname]" id="contactPersonFirstname" value="<?php echo $contactPersonEntity->getVorname(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Nachname*</td>
        <td class="details2_info"><input type="text" name="contactPerson[nachname]" id="contactPersonLastname" value="<?php echo $contactPersonEntity->getNachname(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Email*</td>
        <td class="details2_info2"><input type="text" name="contactPerson[email]" id="contactPersonEmail" value="<?php echo $contactPersonEntity->getEmail(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Telefon</td>
        <td class="details2_info"><input type="text" name="contactPerson[telefon]" id="contactPersonPhone" value="<?php echo $contactPersonEntity->getTelefon(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Fax</td>
        <td class="details2_info2"><input type="text" name="contactPerson[fax]" id="contactPersonFax" value="<?php echo $contactPersonEntity->getFax(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Mobil</td>
        <td class="details2_info"><input type="text" name="contactPerson[mobil]" id="contactPersonMobil" value="<?php echo $contactPersonEntity->getMobil(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Position</td>
        <td class="details2_info2"><input type="text" name="contactPerson[position]" id="contactPersonPosition" value="<?php echo $contactPersonEntity->getPosition(); ?>" /></td>
    </tr>
</table>