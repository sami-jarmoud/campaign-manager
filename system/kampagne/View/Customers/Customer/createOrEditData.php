<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



$mandant = $_SESSION['mandant'];
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');



/**
 * init $debugLogManager
 * init $campaignManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


$customerId = isset($_GET['customer']['id']) ? \intval($_GET['customer']['id']) : 0;
$debugLogManager->logData('customerId', $customerId);

try {
	if ($customerId > 0) {
		/**
		* getCustomerDataItemById
		*/
	   $customerEntity = $campaignManager->getCustomerDataItemById($customerId);
	   if (!($customerEntity instanceof \CustomerEntity)) {
		   throw new \DomainException('invalid CustomerEntity');
	   }
	   
	   $actionType = 'editCustomerData';
	} else {
		$customerEntity = new \CustomerEntity();
		$customerEntity->setPayment_deadline(10);
		$customerEntity->setStaticCountry(new \StaticCountryEntity());
		
		$actionType = 'addCustomerData';
	}
	
	// debug
	$debugLogManager->logData('customerEntity', $customerEntity);
	$debugLogManager->logData('actionType', $actionType);
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
?>

<input type="hidden" name="customer[kunde_id]" id="kunde_id" value="<?php echo $customerId; ?>" />
<input type="hidden" name="customerOrContactPerson[action]" id="action" value="<?php echo $actionType; ?>" />

<table>
	<tr>
    	<td class="details_info">Firma*</td>
        <td class="details2_info"><input type="text" name="customer[firma]" id="customerCompany" value="<?php echo $customerEntity->getFirma(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">K&uuml;rzel</td>
        <td class='details2_info2'><input type="text" name="customer[firma_short]" id="customerShortCompany" value="<?php echo $customerEntity->getFirma_short(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Strasse*</td>
        <td class="details2_info"><input type="text" name="customer[strasse]" id="customerStreet" value="<?php echo $customerEntity->getStrasse(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Plz*</td>
        <td class='details2_info2'><input type="text" name="customer[plz]" id="customerZip" value="<?php echo $customerEntity->getPlz(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Ort*</td>
        <td class="details2_info"><input type="text" name="customer[ort]" id="customerCity" value="<?php echo $customerEntity->getOrt(); ?>" /></td>
    </tr>
	<tr>
    	<td class="details_info2" style="white-space: normal;">Land*</td>
        <td class="details2_info2" style="white-space: normal;">
			<input type="text" name="country" id="customerCountrySearch" value="<?php echo $customerEntity->getStaticCountry()->getCn_short_de(); ?>" style="position: relative;" />
			<div id="countriesListContainer"></div>
			
			<input type="hidden" name="customer[country_id]" id="customerCountryId" value="<?php echo $customerEntity->getCountry_id(); ?>" />
		</td>
    </tr>
    <tr>
    	<td class="details_info">Telefon</td>
        <td class="details2_info"><span id="customerCountryPhoneCode"></span><input type="text" name="customer[telefon]" id="customerPhone" value="<?php echo $customerEntity->getTelefon(); ?>" /></td>
    </tr>
    <tr>
		<td class="details_info2">Fax</td>
        <td class="details2_info2"><span id="customerCountryPhoneFaxCode"></span><input type="text" name="customer[fax]" id="customerFax" value="<?php echo $customerEntity->getFax(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Webseite</td>
        <td class="details2_info">http://<input type="text" name="customer[website]" id="customerWebsite" style="width:158px" value="<?php echo $customerEntity->getWebsite(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Email</td>
        <td class="details2_info2"><input type="text" name="customer[email]" id="customerEmail" value="<?php echo $customerEntity->getEmail(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info">Gesch&auml;ftsf&uuml;hrer</td>
        <td class="details2_info"><input type="text" name="customer[geschaeftsfuehrer]" id="customerCeo" value="<?php echo $customerEntity->getGeschaeftsfuehrer(); ?>" /></td>
    </tr>
    <tr>
    	<td class="details_info2">Registergericht</td>
        <td class="details2_info2"><input type="text" name="customer[registergericht]" id="customerRegistergericht" value="<?php echo $customerEntity->getRegistergericht(); ?>" /></td>
    </tr>
	<tr>
    	<td class="details_info">USt-IdNr. (Vat.Nr.)</td>
        <td class="details2_info"><input type="text" name="customer[vat_number]" id="customerVatNumber" value="<?php echo $customerEntity->getVat_number(); ?>" /></td>
    </tr>
	<tr>
    	<td class="details_info2">Zahlungsfrist</td>
        <td class="details2_info2"><input type="text" name="customer[payment_deadline]" id="customerPaymentDeadline" style="width:60px" value="<?php echo $customerEntity->getPayment_deadline(); ?>" /> Tage</td>
    </tr>
	<tr>
    	<td class="details_info">Opt-In</td>
        <td class="details2_info">
			<select name="customer[data_selection]" id="customerDataSelection">
				<option value="">- bitte ausw&auml;hlen -</option>
				<?php
					echo \CampaignAndOthersUtils::getCustomerDataSelectionItems($customerEntity->getData_selection());
				?>
			</select>
		</td>
    </tr>
</table>
<script type="text/javascript">
	YAHOO.example.ItemSelectCountryHandler = function() {
		var customerCountryId = YAHOO.util.Dom.get('customerCountryId');
		var customerCountryPhoneCode = YAHOO.util.Dom.get('customerCountryPhoneCode');
		var customerCountryPhoneFaxCode = YAHOO.util.Dom.get('customerCountryPhoneFaxCode');
		var customerPaymentDeadline = YAHOO.util.Dom.get('customerPaymentDeadline');
		
		var countriesDS = new YAHOO.util.LocalDataSource(<?php echo \json_encode($_SESSION['campaign']['staticCountryDataArray']); ?>);
		countriesDS.responseSchema = {fields: ['title', 'phoneCode', 'uid']};
		
		var aAC = new YAHOO.widget.AutoComplete(
			'customerCountrySearch',
			'countriesListContainer',
			countriesDS,
			{
				useShadow: true,
				resultTypeList: false,
				minQueryLength: 2,
				animVert: 5
			}
		);
		
		aAC.itemSelectEvent.subscribe(function(sType, aArgs) {
			var oData = aArgs[2]; // object literal of selected item's result data
			var uidData = parseInt(oData.uid);
			
			// update hidden form field with the selected item's ID
			customerCountryId.value = uidData;
			customerCountryPhoneCode.innerHTML = customerCountryPhoneFaxCode.innerHTML = '(+' + parseInt(oData.phoneCode) + ') ';
			
			var customerPaymentDeadlineValue = 10;
			if (uidData !== 54) {
				customerPaymentDeadlineValue = 14;
			}
			customerPaymentDeadline.value = customerPaymentDeadlineValue;
		});
		aAC.unmatchedItemSelectEvent.subscribe(function(sType, aArgs) {
			var newValue = aArgs[0]._elTextbox.value;
			if (newValue.length === 0) {
				customerCountryId.value = customerCountryPhoneCode.innerHTML = customerCountryPhoneFaxCode.innerHTML = '';
			}
		});
		
		return {
			countriesDS: countriesDS,
			aAC: aAC
		};
	}();
</script>