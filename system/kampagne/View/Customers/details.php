<?php
require_once('../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * getSalesPersonDataById
 * 
 * @param integer $salesId
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return string
 * 
 * @throws \DomainException
 */
function getSalesPersonDataById($salesId, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	$result = '';
	if ($salesId > 0) {
		/**
		 * getUserDataItemById
		 * 
		 * debug
		 */
		$salesPersonDataEntity = $clientManager->getUserDataItemById($salesId);
		if (!($salesPersonDataEntity instanceof \UserEntity)) {
			throw new \DomainException(\utf8_encode('Der Vertriebler wurde gelöscht. Bitte wenden Sie sich an dem support!'));
		}
		$debugLogManager->logData('salesPersonDataEntity', $salesPersonDataEntity);
		
		$result = $salesPersonDataEntity->getVorname() . ' ' . $salesPersonDataEntity->getNachname();
	}
	
	return $result;
}

/**
 * getContactPersonCampaignCount
 * 
 * @param \ContactPersonEntity $contactPersonEntity
 * @param \CampaignManager $campaignManager
 * @param \debugLogManager $debugLogManager
 * @return integer
 */
function getContactPersonCampaignCount(\ContactPersonEntity $contactPersonEntity, \CampaignManager $campaignManager, \debugLogManager $debugLogManager) {
	$result = $campaignManager->getCampaignsCountByQueryParts(
		array(
			'WHERE' => array(
				'campaigns' => array(
					'sql' => '`k_id`',
					'value' => '`nv_id`',
					'comparison' => 'fieldEqual'
				),
				'customerId' => array(
					'sql' => '`agentur_id`',
					'value' => $contactPersonEntity->getKunde_id(),
					'comparison' => 'integerEqual'
				),
				'contactPersonId' => array(
					'sql' => '`ap_id`',
					'value' => $contactPersonEntity->getAp_id(),
					'comparison' => 'integerEqual'
				),
			)
		)
	);
	$debugLogManager->logData(__FUNCTION__, $result);
	
	return $result;
}



$mandant = $_SESSION['mandant'];
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');



/**
 * init $debugLogManager
 * init $campaignManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


try {
	$customerId = isset($_GET['customer']['id']) ? \intval($_GET['customer']['id']) : 0;
	if ($customerId === 0) {
		throw new \InvalidArgumentException('invalid customerId');
	}
	$debugLogManager->logData('customerId', $customerId);
	
	/**
	 * getCustomerDataItemById
	 * 
	 * debug
	 */
	$customerEntity = $campaignManager->getCustomerDataItemById($customerId);
	if (!($customerEntity instanceof \CustomerEntity)) {
		throw new \DomainException('invalid CustomerEntity');
	}
	$debugLogManager->logData('customerEntity', $customerEntity);
	
	/**
	 * costomerCampaignsCount
	 * 
	 * debug
	 */
	$customerCampaignCount = $campaignManager->getCampaignsCountByQueryParts(
		array(
			'WHERE' => array(
				'campaigns' => array(
					'sql' => '`k_id`',
					'value' => '`nv_id`',
					'comparison' => 'fieldEqual'
				),
				'customerId' => array(
					'sql' => '`agentur_id`',
					'value' => $customerEntity->getKunde_id(),
					'comparison' => 'integerEqual'
				),
			)
		)
	);
	$debugLogManager->logData('customerCampaignCount', $customerCampaignCount);
	
	
	/**
	 * getContactPersonDataItemsByQueryParts
	 * 
	 * debug
	 */
	$contactPersonDataArray = $campaignManager->getContactPersonDataItemsByQueryParts(
		array(
			'WHERE' => array(
				'customerId' => array(
					'sql' => '`kunde_id`',
					'value' => $customerId,
					'comparison' => 'integerEqual'
				)
			),
			'ORDER_BY' => '`email` ASC'
		),
		\PDO::FETCH_CLASS
	);
	$debugLogManager->logData('contactPersonDataArray', $contactPersonDataArray);
	
	$customerStatusContent = "<span style='color:red'>Inaktiv</span>";
	if ($customerEntity->getStatus() === 1) {
		$customerStatusContent = "<span style='color:green'>Aktiv</span>";
	}
	
	$contactPersonsContent = '';
	if (\count($contactPersonDataArray) > 0) {
		require_once('View/contactPersons_list.php');
	}
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
?>
<input type="hidden" name="kunde_id_def" id="kunde_id_def" value="<?php echo $customerId; ?>" />
<input type="hidden" name="ap_id_def" id="ap_id_def" value="" />
<div id="details_header_div" style="height:150px; background:url(img/rep_bg.png) repeat-x 100% 100%; border-bottom:1px solid #CCCCCC; width:100%;">
	<table cellpadding="0" cellspacing="0" border="0" style="margin-top:10px;">
		<tr>
			<td style="padding-left:35px;" valign="top"><img src="img/Tango/48/mimetypes/office-contact.png" /></td>
			<td valign="top" style="padding-left:5px;">
				<span class="label_big"><?php echo $customerEntity->getFirma(); ?></span><br />
				<span class="label_sub1">Tel.: <?php echo $customerEntity->getTelefon() . ', Fax: ' . $customerEntity->getFax(); ?></span><br />
				<span class="label_sub2"><a href="http://<?php echo $customerEntity->getWebsite(); ?>" target="_blank" style="color:#333333">http://<?php echo $customerEntity->getWebsite(); ?></a><br />
			</td>
		</tr>
		<tr>
			<td colspan="2" height="15"></td>
		</tr>
		<tr>
			<td></td>
			<td style="padding-left:5px;color:#666666;">Status: <?php echo $customerStatusContent; ?></td>
		</tr>
		<tr>
			<td colspan="2" height="20"></td>
		</tr>
		<tr>
			<td></td>
			<td style="padding-left:5px;color:#666666;white-space:nowrap;">
				<input type="button" id="button_kunde_edit" onclick="editCustomerData(<?php echo $customerId; ?>);" style="cursor:pointer;" value="Bearbeiten" />
				<input type="button" class="negative" onclick="deleteCustomerEntity();" value="L&ouml;schen" />
				
				<input type="button" class="positive" onclick="showCustomerCampaigns(<?php echo $customerId; ?>);" value="<?php echo $customerCampaignCount; ?> Kampagnen anzeigen" />
			</td>
		</tr>
	</table>
</div>
<div id="report_c">
	<div style="padding:25px;background-color:#FFFFFF">
		<fieldset class="fieldset_default" style="margin-top:-15px">
			<legend class="legend_default">Allgemeine Informationen</legend>
			<table class="float left" style="width: 30%;">
				<tr>
					<td class="fieldset_label">Firma:</td>
					<td class="fieldset_data"><?php echo $customerEntity->getFirma(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">K&uuml;rzel:</td>
					<td class="fieldset_data"><?php echo $customerEntity->getFirma_short(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">Adresse:</td>
					<td class="fieldset_data">
						<?php 
						echo $customerEntity->getStrasse() . \nl2br(\chr(13)) 
							. $customerEntity->getPlz() . ' ' . $customerEntity->getOrt() . \nl2br(\chr(13)) 
							. $customerEntity->getStaticCountry()->getCn_short_de()
						;
						?>
					</td>
				</tr>
				<tr>
					<td class="fieldset_label">Telefon:</td>
					<td class="fieldset_data"><?php echo '(+' . $customerEntity->getStaticCountry()->getCn_phone() . ') ' . $customerEntity->getTelefon(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">Fax:</td>
					<td class="fieldset_data"><?php echo '(+' . $customerEntity->getStaticCountry()->getCn_phone() . ') ' . $customerEntity->getFax(); ?></td>
				</tr>
			</table>
			
			<table class="float left">
				<tr>
					<td class="fieldset_label">Webseite:</td>
					<td class="fieldset_data"><a href="http://<?php echo $customerEntity->getWebsite(); ?>" target="_blank">http://<?php echo $customerEntity->getWebsite(); ?></a></td>
				</tr>
				<tr>
					<td class="fieldset_label">Email:</td>
					<td class="fieldset_data"><a href="mailto:<?php echo $customerEntity->getEmail(); ?>"><?php echo $customerEntity->getEmail(); ?></a></td>
				</tr>
				<tr>
					<td class="fieldset_label">Gesch&auml;ftsf&uuml;hrer:</td>
					<td class="fieldset_data"><?php echo $customerEntity->getGeschaeftsfuehrer(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">Registergericht:</td>
					<td class="fieldset_data"><?php echo $customerEntity->getRegistergericht(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">USt-IdNr. (Vat.Nr.):</td>
					<td class="fieldset_data"><?php echo $customerEntity->getVat_number(); ?></td>
				</tr>
				<tr>
					<td class="fieldset_label">Opt-In:</td>
					<td class="fieldset_data"><?php echo \CampaignAndOthersUtils::$customerDataSelections[$customerEntity->getData_selection()]; ?></td>
				</tr>
			</table>
		</fieldset>
		
            <fieldset class="fieldset_default">
                <legend class="legend_default">Monatsrechnung:</legend>
                <table class="float left">
                   <tr>
                    <td class="fieldset_label">Eine Rechnung pro Monat:</td>
                    <td class="fieldset_data"><input type="checkbox" id="monatsrechnung" name="customer[monatsrechnung]" <?php if($customerEntity->getMonatsrechnung() == 1){echo 'checked';}?> /></td>
                    <input type="button" id="button_monatsrechnung" class="floatRight spacerTopWrap spacerRightWrap" onclick="editCustomerInvoice(<?php echo $customerId; ?>);" style="cursor:pointer;" value="speichern" />
                </tr>
                </table>

            </fieldset>
		<fieldset class="fieldset_default">
			<legend class="legend_default">Notizen:</legend>

			<div class="clearBoth">
				<textarea name="customer[notes]" id="customer_notes" cols="15" rows="5" style="width:98%;" class="textfeld"><?php echo $customerEntity->getNotes(); ?></textarea>
				
				<input type="button" id="button_customer_editNotes" class="floatRight spacerTopWrap spacerRightWrap" onclick="editCustomerNotes(<?php echo $customerId; ?>);" style="cursor:pointer;" value="Notizen hinzufügen/aktualisieren" />
			</div>
		</fieldset>
		
		<fieldset class="fieldset_default">
			<legend class="legend_default">Kontakte</legend>
			<div style="margin-left: 25px; margin-bottom: 15px;">
				<input type="image" src="img/Oxygen/32/actions/contact-new.png" onclick="editOrCreateContactPersonData(<?php echo $customerId; ?>, '');" title="Neuen Kontakt anlegen" /> 
				<a onclick="editOrCreateContactPersonData(<?php echo $customerId; ?>, ''); return false;" href="#" style="padding-left:5px;">Neuer Kontakt</a>
			</div>
			<table width="95%">
				<?php echo $contactPersonsContent; ?>
			</table>
		</fieldset>
	</div>
</div>
<script type="text/javascript">
    function editCustomerData(customerId) {
        setRequest_default(
			'kampagne/View/Customers/Customer/createOrEditData.php?customer[id]=' + parseInt(customerId),
			'kunde_content'
		);
        YAHOO.example.container.container_kunde.show();
    }

    function editOrCreateContactPersonData(customerId, contactPersonId) {
        setRequest_default(
			'kampagne/View/Customers/ContactPerson/createOrEditData.php?customer[id]=' + parseInt(customerId) + '&contactPerson[id]=' + contactPersonId,
			'kunde_ap_content'
		);
        YAHOO.example.container.container_kunde_ap.show();
    }

    function deleteContactPersonEntity(contactPersonId) {
        YAHOO.util.Dom.get('ap_id_def').value = parseInt(contactPersonId);
        YAHOO.example.container.container_del_kunde_ap.show();
    }

    function deleteCustomerEntity() {
        YAHOO.example.container.container_del_kunde.show();
    }
	
	function showCustomerCampaigns(customerId) {
		/**
		 * select customer (kAgentur)
		 * and getContactPersons
		 */
		selectIndexByValue('kAgentur', customerId);
		getContactPersons(customerId, 'kAnsprechpartner');

		// setRequest
		setRequest('kampagne/k_main.php?new=1', 'k');
	}
	
	function showContactPersonCampaigns(customerId, contactPersonId) {
		/**
		 * select customer (kAgentur)
		 * and getContactPersons
		 */
		selectIndexByValue('kAgentur', customerId);
		getContactPersons(customerId, 'kAnsprechpartner');
		
		setTimeout(function() {
			// select customer (kAgentur)
			selectIndexByValue('kAnsprechpartner', contactPersonId);
			
			// setRequest
			setRequest('kampagne/k_main.php?new=1', 'k');
		}, 1000);
	}
	
	function selectIndexByValue(selectElementId, selectId) {
		var elementOptions = YAHOO.util.Dom.get(selectElementId).options;
		
		for(var i=0; i < elementOptions.length; i++) {
			if (parseInt(elementOptions[i].value) === parseInt(selectId)) {
				YAHOO.util.Dom.get(selectElementId).selectedIndex = i;
			}
		}
	}
	
	function editCustomerInvoice(customerId) {
		console.log(customerId);
		
		var postData = [];
		var customerNotesElement = YAHOO.util.Dom.get('monatsrechnung');
		postData.push('actionMethod=customer');
		postData.push('actionType=updateInvoiceMonth');
		postData.push('customer[kunde_id]=' + parseInt(customerId));
		postData.push(customerNotesElement.name + '=' + customerNotesElement.checked);
		
		processAjaxRequest(
			'AjaxRequests/ajax.php',
			postData.join('&'),
			'',
			function (result) {
				console.log(result);
			}
		);
	}

function editCustomerNotes(customerId) {
		console.log(customerId);
		
		var postData = [];
		var customerNotesElement = YAHOO.util.Dom.get('customer_notes');
		postData.push('actionMethod=customer');
		postData.push('actionType=updateNotes');
		postData.push('customer[kunde_id]=' + parseInt(customerId));
		postData.push(customerNotesElement.name + '=' + encodeURIComponent(customerNotesElement.value));
		
		processAjaxRequest(
			'AjaxRequests/ajax.php',
			postData.join('&'),
			'',
			function (result) {
				console.log(result);
			}
		);
	}
</script>