<?php
require_once('../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * sendUserInfoEmail
 * 
 * @param \SwiftMailerWebservice $swiftMailerWebservice
 * @param \debugLogManager $debugLogManager
 * @param string $emailMessage
 * @param string $emailSubject
 * @param array $failedRecipients
 * @return integer
 */
function sendUserInfoEmail(\SwiftMailerWebservice $swiftMailerWebservice, \debugLogManager $debugLogManager, $emailMessage, $emailSubject, array &$failedRecipients) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);
	$debugLogManager->logData('swiftMailerWebservice', $swiftMailerWebservice);
	
	$swiftMessage = $swiftMailerWebservice->createMessage();
	/* @var $swiftMessage Swift_Message */
	
	$swiftMessage->setBody($emailMessage)
		->setSubject($emailSubject)
		->setTo($_SESSION['u_email'])
    ;
	
	/**
	 * sendMessage
	 * 
	 * debug
	 */
	$mailerSendResults = $swiftMailerWebservice->sendMessage($swiftMessage);
    $debugLogManager->logData('mailerSendResults', $mailerSendResults);
	
	/**
	 * getFailedRecipients
	 * 
	 * debug
	 */
	$failedRecipients = $swiftMailerWebservice->getFailedRecipients();
    $debugLogManager->logData('failedRecipients', $failedRecipients);
    $swiftMailerWebservice->setFailedRecipients(array());
	
	// debug
	$debugLogManager->endGroup();
	
	return $mailerSendResults;
}

/**
 * createCustomerDataArray
 * 
 * @param array $dataArray
 * @return array
 */
function createCustomerDataArray(array $dataArray) {
	return array(
		'firma' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['firma'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'firma_short' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['firma_short'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'strasse' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['strasse'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'plz' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['plz'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'ort' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['ort'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'telefon' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['telefon'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'fax' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['fax'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'website' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['website'],
				\DataFilterUtils::$validateFilder['url']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'email' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['email'],
				\DataFilterUtils::$validateFilder['email']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'geschaeftsfuehrer' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['geschaeftsfuehrer'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'registergericht' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['registergericht'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'country_id' => array(
			'value' => \intval($dataArray['country_id']),
			'dataType' => \PDO::PARAM_INT
		),
		'vat_number' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['vat_number'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'payment_deadline' => array(
			'value' => \intval($dataArray['payment_deadline']),
			'dataType' => \PDO::PARAM_INT
		),
		'data_selection' => array(
			'value' => \intval($dataArray['data_selection']),
			'dataType' => \PDO::PARAM_INT
		),
	);
}

/**
 * createContactPersonDataArray
 * 
 * @param array $dataArray
 * @return array
 */
function createContactPersonDataArray(array $dataArray) {
	return array(
		'vertriebler_id' => array(
			'value' => intval($dataArray['vertriebler_id']),
			'dataType' => \PDO::PARAM_INT
		),
		'anrede' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['anrede'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'titel' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['titel'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'vorname' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['vorname'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'nachname' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['nachname'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'email' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['email'],
				\DataFilterUtils::$validateFilder['email']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'telefon' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['telefon'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'fax' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['fax'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'mobil' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['mobil'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
		'position' => array(
			'value' => \DataFilterUtils::filterData(
				$dataArray['position'],
				\DataFilterUtils::$validateFilder['special_chars']
			),
			'dataType' => \PDO::PARAM_STR
		),
	);
}



$mandant = $_SESSION['mandant'];
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');



/**
 * init $debugLogManager
 * init $campaignManager
 * init $clientManager
 * init $swiftMailerWebservice
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require(DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */

require_once(DIR_configsInit . 'initSwiftMailerWebservice.php');
/* @var $swiftMailerWebservice \SwiftMailerWebservice */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


$actionMethod = isset($_POST['customerOrContactPerson']['action']) 
	? \DataFilterUtils::filterData(
		$_POST['customerOrContactPerson']['action'],
		\DataFilterUtils::$validateFilder['full_special_chars']
	) 
	: null
;
$debugLogManager->logData('actionMethod', $actionMethod);

try {
	switch ($actionMethod) {
		case 'addCustomerData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/Customer/addData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		case 'editCustomerData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/Customer/updateData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		case 'deleteCustomerData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/Customer/deleteData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		case 'addContactPersonData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/ContactPerson/addData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		case 'editContactPersonData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/ContactPerson/updateData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		case 'deleteContactPersonData':
			// debug
			$debugLogManager->beginGroup($actionMethod);
			
			require_once(DIR_Action . 'Customers/ContactPerson/deleteData.php');
			
			// debug
			$debugLogManager->endGroup();
			break;
		
		default:
			throw new \BadFunctionCallException('Unknown action: ' . $actionMethod);
	}
} catch (\Exception $e) {
	\DebugAndExceptionUtils::sendDebugData($e);
	
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}