<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * getClientTableContent
 * 
 * @param array $tableTHeaderDataArray
 * @param array $tableRowDataArray
 * @param string $clientAbkz
 * @param string $clientPageBreakStyle
 * @return string
 */
function getClientTableContent(array $tableTHeaderDataArray, array $tableRowDataArray, $clientAbkz, $clientPageBreakStyle) {
	/**
	 * createTable - Tag
	 */
	$tableDataArray = \HtmlTableUtils::createTable(
		array(
			'class' => 'tcm_table newTableView ' . $clientAbkz,
			'style' => $clientPageBreakStyle
		)
	);

	$result = 
		$tableDataArray['begin']
			. $tableTHeaderDataArray['begin']
				. $tableRowDataArray ['begin']
					. \HtmlTableUtils::createTableHeaderCellWidthContent(
						array(
							'class' => 'noBorder transparent',
							'colspan' => $_SESSION['tcmData']['totalColspan'],
							'style' => 'text-align: left;'
						),
						'<img src="http://maas.treaction.de/system/img/logo/' . $clientAbkz . '.gif" alt="" style="width: 100px; height: auto; margin-right: 20px;" />'
							. '<img class="delFeld hidden" id="' . $clientAbkz . '"  src="http://maas.treaction.de/system/img/Tango/16/actions/dialog-close.png" title="Mandant entfernen" alt="" />'
					)
			. $tableRowDataArray['end']
	;

	return $result;
}

/**
 * createEditableTableHeadForMandatory
 * 
 * @param array $tableColumnsDataArray
 * @return string
 */
function createEditableTableHeadForMandatory(array $tableColumnsDataArray) {
	/**
	 * createTable - Tag
	 */
	$tableDataArray = \HtmlTableUtils::createTable(
		array(
			'class' => 'tcm_table newTableView headerEditColumns'
		)
	);

	$result = $tableDataArray['begin'];
	foreach ($tableColumnsDataArray as $key => $itemDataArray) {
		$result .= \HtmlTableUtils::createTableCellWidthContent(
			array(
				'class' => 'noBorder ' . $key,
				'align' => 'center'
			),
			'<img class="delFeld" id="' . $key . '"  src="http://maas.treaction.de/system//img/Tango/16/actions/dialog-close.png" title="Spalte: ' . $itemDataArray['rowTitle'] . ' entfernen" alt="" />'
		);
	}
	$result .= $tableDataArray['end'];

	return $result;
}

/**
 * processDeliverySystemWebservice
 * 
 * @param \ClientEntity $clientEntity
 * @param mixed $underClientIds
 * @param \CampaignEntity $campaignEntity
 * @param \ClientManager $clientManager
 * @param \debugLogManager $debugLogManager
 * @return null|\DateTime
 * 
 * @throws \DomainException
 */
function processDeliverySystemWebservice($underClientIds, \ClientEntity $clientEntity, \CampaignEntity $campaignEntity, \ClientManager $clientManager, \debugLogManager $debugLogManager) {
	// debug
	$debugLogManager->beginGroup(__FUNCTION__);

	/**
     * getClientDeliveryDataByIdAndDeliverySystemDistributorId
     * 
     * debug
     */
    $clientDeliveryEntity = $clientManager->getClientDeliveryDataByIdAndDeliverySystemDistributorId(
		$clientEntity->getId(),
		$campaignEntity->getDsd_id(),
		$underClientIds
	);
	if (!($clientDeliveryEntity instanceof \ClientDeliveryEntity)) {
		/**
		* getClientDeliveryDataByClientIdAndDeliverySystemAsp
		* 
		* gilt nur f�r alte kampagnen, wenn die db aktualisiert wurde kann dieser code entfernt werden
		*/
		$clientDeliveryEntity = $clientManager->getClientDeliveryDataByClientIdAndDeliverySystemAsp(
			$clientEntity->getId(),
			$campaignEntity->getVersandsystem()
		);
		if (!($clientDeliveryEntity instanceof \ClientDeliveryEntity)) {
			throw new \DomainException('invalid ClientDeliveryEntity');
		}
	}
    $debugLogManager->logData('clientDeliveryEntity', $clientDeliveryEntity);
	
	
	/**
     * DeliverySystemFactory::getAndInitInstance
	 * 
	 * debug
     */
    $deliverySystemWebservice = \DeliverySystemFactory::getAndInitInstance(
		$campaignEntity->getVersandsystem(),
		$clientDeliveryEntity
	);
    #$debugLogManager->logData('deliverySystemWebservice', $deliverySystemWebservice);


	/**
	 * connect to deliveryWebservice
	 */
	$deliverySystemWebservice->login();
	
	if ((\file_exists(DIR_deliverySystems . \CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz($campaignEntity->getVersandsystem())->getAsp() . \DIRECTORY_SEPARATOR . 'scheduleDate.php')) === true) {
		require(DIR_deliverySystems . \CampaignAndOthersUtils::getDeliverySystemEntityByAspAbkz($campaignEntity->getVersandsystem())->getAsp() . \DIRECTORY_SEPARATOR . 'scheduleDate.php');
	} else {
		$scheduleDateTime = null;
	}
	// debug
	$debugLogManager->logData('scheduleDateTime', $scheduleDateTime);

	/**
	 * disconnect to deliveryWebservice
	 */
	$deliverySystemWebservice->logout();
	unset($deliverySystemWebservice);
	
	// debug
	$debugLogManager->endGroup();

	return $scheduleDateTime;
}



// Factory
require_once(DIR_Factory . 'DeliverySystemFactory.php');



/**
 * init $debugLogManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);


$dateNowObj = new \DateTime('now');

if (isset($_GET['dateBegin']) 
	&& \strlen($_GET['dateBegin']) > 0
) {
	$dateBeginObj = new \DateTime($_GET['dateBegin']);

	if (isset($_GET['dateEnd']) 
		&& \strlen($_GET['dateEnd']
	) > 0) {
		$dateEndObj = new \DateTime($_GET['dateEnd']);
	} else {
		$dateEndObj = $dateBeginObj;
	}
} else {
	$dateBeginObj = new \DateTime('now');

	$dateEndObj = new \DateTime('now');
	$dateEndObj->add(new \DateInterval('P3D'));
}
// debug
$debugLogManager->logData('dateNowObj', $dateNowObj);
$debugLogManager->logData('dateBegin', $dateBeginObj);
$debugLogManager->logData('dateEnd', $dateEndObj);


/**
 * getClientsAccessDataArrayByLoggedUser
 * 
 * debug
 */
$userClientsDataArray = \CampaignAndOthersUtils::getClientsAccessDataArrayByLoggedUser();
$debugLogManager->logData('userClientsDataArray', $userClientsDataArray);


$tableColumnsDataArray = array(
	'date' => array(
		'rowTitle' => 'Datum'
	),
	'time' => array(
		'rowTitle' => 'Uhrzeit'
	),
	'campaign' => array(
		'rowTitle' => 'Kampagne'
	),
	'customerCompany' => array(
		'rowTitle' => 'Kunde / Agentur'
	),
	'deliverySystem' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['asp']['sublabel']
	),
	'deliverySystemDistributor' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['dsd_id']['sublabel']
	),
	'targetGroup' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['zielgruppe']['sublabel']
	),
	'targetVolume' => array(
		'rowTitle' => 'Zielmenge'
	),
	'notes' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['notiz']['sublabel']
	),
	'campaignEditor' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['bearbeiter']['sublabel']
	),
	'blacklist' => array(
		'rowTitle' => 'BL'
	),
	'useCampaignStartDate' => array(
		'rowTitle' => \CampaignAndOthersUtils::$allTabFieldsDataArray['use_campaign_start_date']['sublabel']
	),
	'status' => array(
		'rowTitle' => 'Status'
	),
);
$debugLogManager->logData('tableColumnsDataArray', $tableColumnsDataArray);

$_SESSION['tcmData']['totalColspan'] = \count($tableColumnsDataArray);


/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(array());

/**
 * createTHeadTag - Tag
 */
$tableTHeaderDataArray = \HtmlTableUtils::createTHeadTag();

/**
 * createTableRow - Tag
 */
$tableRowDataArray = \HtmlTableUtils::createTableRow(array());


$campaign_queryPartsDataArray = array(
	'SELECT' => '*',
	'WHERE' => array(
		'dateFrom' => array(
			'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
			'value' => $dateBeginObj->format('Y-m-d'),
			'comparison' => '>='
		),
		'dateTo' => array(
			'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
			'value' => $dateEndObj->format('Y-m-d'),
			'comparison' => '<='
		),
		'special' => array(
			'sql' => '((`k_id` != `nv_id` AND `vorgabe_m` > 0) OR LEFT(`k_name`, 3) = "NV:")',
			'value' => '',
			'comparison' => ''
		),
	),
	'ORDER_BY' => 'datum ASC'
);
$debugLogManager->logData('campaign_queryPartsDataArray', $campaign_queryPartsDataArray);


try {
	$i = 1;

	$tableData = $clientPageBreakStyle = '';
	
	// debug
	$debugLogManager->beginGroup('TcmNvList');
	foreach ($userClientsDataArray as $clientId => $clientName) {
		$debugLogManager->beginGroup($clientName);

		if ($i > 1) {
			$clientPageBreakStyle = ' page-break-before: always;';
		}


		/**
		 * getClientDataById
		 * 
		 * debug
		 */
		$clientEntity = $clientManager->getClientDataById($clientId);
		if (!($clientEntity instanceof \ClientEntity)) {
			throw new DomainException('invalid ClientEntity');
		}
		$debugLogManager->logData('clientEntity', $clientEntity);
		
		
		$mandant = $clientEntity->getAbkz();
		require($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');
		
		
		/**
		 * init $campaignManager
		 */
		require(DIR_configsInit . 'initCampaignManager.php');
		/* @var $campaignManager CampaignManager */
		
		
		/**
		 * getClientTableContent
		 */
		$tableData .= \getClientTableContent(
			$tableTHeaderDataArray,
			$tableRowDataArray,
			$mandant,
			$clientPageBreakStyle
		);
		
		/**
		 * getUnderClientIdsByClientId
		 * 
		 * debug
		 */
		$underClientIds = $clientManager->getUnderClientIdsByClientId($clientId);
		$debugLogManager->logData('underClientIds', $underClientIds);
		
		
		/**
		 * getDeliverySystemDistributorsDataItemsByClientId
		 * 
		 * debug
		 */
		$clientDeliverySystemDistributorEntityDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientId(
			$clientId,
			$underClientIds
		);
		$debugLogManager->logData('clientDeliverySystemDistributorEntityDataArray', $clientDeliverySystemDistributorEntityDataArray);
		
		/**
		 * getCampaignsAndCustomerDataItemsByQueryParts
		 * 
		 * debug
		 */
		$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaign_queryPartsDataArray);
		#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
		
		$campaignCount = \count($campaignsDataArray);
		$debugLogManager->logData('campaignsCount', $campaignCount);
		if ($campaignCount > 0) {
			// viewData_header
			require('View/viewData_header.php');

			// viewData_list
			require('View/viewData_list.php');

			$tableData .= $tableHeaderContent
				. $tableTHeaderDataArray['end']
				. $tableContent
			;
		} else {
			// viewData_noData
			require('View/viewData_noData.php');
			
			$tableData .= $tableTHeaderDataArray['begin']
				. $tableContent
			;
			unset($tableContent);
		}
		$tableData .= $tableDataArray['end'];

		$i++;

		$debugLogManager->endGroup();
	}
	// debug
	$debugLogManager->endGroup();
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	$tableData = $exceptionMessage;
}

// getCalView
$calView = \file_get_contents('../calView.html');

$tbl = $calView
	. '<h1 class="tcmViewHeader">TCM-NV - Tagesliste</h1>'
	. \createEditableTableHeadForMandatory($tableColumnsDataArray)
	. $tableData
;


$_SESSION['data'] = '
    <div id="printInfoContent">
        <p>
            Die Spalten, die nicht ausgedruckt werden sollen, k&ouml;nnen mit einem Klick auf <img src="http://maas.treaction.de/system/img/Tango/16/actions/dialog-close.png" alt="" /> entfernt werden.<br /><br />
            <button onclick="printContent();">Drucken</button> <button onclick="location.reload();">Zur&uuml;cksetzen</button>
        </p>
    </div>
    <div id="printContentData">' . \utf8_decode($tbl) . '</div>
';

require_once('../menu.php');

$_SESSION['tcmData']['useExtendedPrint'] = true;
?>
<script type="text/javascript">
	function changeView() {
		setRequest('kampagne/View/Specialisten/TcmNvList/index.php' 
				+ '?dateBegin=' + YAHOO.util.Dom.get('in_tcmList').value 
				+ '&dateEnd=' + YAHOO.util.Dom.get('out_tcmList').value 
				+ '&p=7'
			,
			'k_tcm'
		);
	}
</script>