<?php
/**
 * createTableHeaderRow - Tag
 */
$tableSubheadDataArray = \HtmlTableUtils::createTableRow(
	array(
		'class' => 'headerStyleColumn headerColumn'
	)
);

$tableHeaderContent = $tableSubheadDataArray['begin'];

foreach ($tableColumnsDataArray as $key => $itemDataArray) {
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'title' => (isset($itemDataArray['title']) ? ' ' . $itemDataArray['title'] : ''),
			'class' => $key
		),
		$itemDataArray['rowTitle']
	);
}
$tableHeaderContent .= $tableSubheadDataArray['end'];

unset($tableHeadDataArray);