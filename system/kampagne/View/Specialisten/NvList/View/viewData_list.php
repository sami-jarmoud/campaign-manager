<?php
/* @var $campaignManager \CampaignManager */
/* @var $debugLogManager \debugLogManager */

$tableContent = '';
foreach ($campaignsDataArray as $key => $campaignEntity) {
	/* @var $campaignEntity \CampaignWidthCustomerAndContactPersonEntity */

	if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
		$hvCampaign_queryPartsDataArray = array(
			'SELECT' => array(
				'versendet' => 'SUM(`versendet`)',
				'openings_all' => 'SUM(`openings_all`)',
				'klicks_all' => 'SUM(`klicks_all`)',
			),
			'WHERE' => array(
				'nvId' => array(
					'sql' => '`nv_id`',
					'value' => $campaignEntity->getNv_id(),
					'comparison' => '='
				)
			),
		);
		$debugLogManager->logData('hvCampaign_queryPartsDataArray', $hvCampaign_queryPartsDataArray);

		$hvCampaignDataArray = $campaignManager->getCampaignDataItemByQueryParts(
			$hvCampaign_queryPartsDataArray,
			\PDO::FETCH_ASSOC
		);
		if (!\is_array($hvCampaignDataArray)) {
			throw new \InvalidArgumentException('no hvCampaignDataArray');
		}
		
		$campaignEntity->setVersendet($hvCampaignDataArray['versendet']);
		$campaignEntity->setOpenings_all($hvCampaignDataArray['openings_all']);
		$campaignEntity->setKlicks_all($hvCampaignDataArray['klicks_all']);
		$debugLogManager->logData('hvCampaignDataArray -> ' . $campaignEntity->getNv_id(), $hvCampaignDataArray);
	}

	// debug
	$debugLogManager->logData('campaignItem -> ' . $key, $campaignEntity);


	$tableContent .=
		$tableRowDataArray['begin']
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'tcm_date date'
				),
				\DateUtils::getWeekDay(
					$campaignEntity->getDatum(),
					true
				) . ', ' . $campaignEntity->getDatum()->format('d.m.Y')
			) 
			. HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'tcm_date time'
				),
				$campaignEntity->getDatum()->format('H:i')
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'campaign'
				),
				$campaignEntity->getK_name()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'customerCompany'
				),
				$campaignEntity->getCustomerEntity()->getFirma()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'targetGroup'
				),
				$campaignEntity->getZielgruppe()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'targetVolume',
					'align' => 'right'
				),
				($campaignEntity->getVorgabe_m() > 0 
					? \FormatUtils::numberFormat($campaignEntity->getVorgabe_m()) 
					: ''
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'bookingAmount',
					'align' => 'right'
				),
				\FormatUtils::numberFormat($campaignEntity->getGebucht())
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'shippedAmount',
					'align' => 'right'
				),
				\FormatUtils::numberFormat($campaignEntity->getVersendet())
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'openingRate',
					'align' => 'right'
				),
				($campaignEntity->getOpenings_all() > 0 
					? \FormatUtils::rateCalculation(
						$campaignEntity->getOpenings_all(),
						$campaignEntity->getGebucht()
					) . ' %' 
					: ' - '
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'clickRate',
					'align' => 'right'
				),
				($campaignEntity->getKlicks_all() > 0 
					? \FormatUtils::rateCalculation(
						$campaignEntity->getKlicks_all(),
						$campaignEntity->getGebucht()
					) . ' %' 
					: ' - '
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'notes'
				),
				(\strlen($campaignEntity->getNotiz()) > 0 
					? \nl2br($campaignEntity->getNotiz()) 
					: ''
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'campaignEditor',
					'nowrap' => 'nowrap'
				),
				$campaignEntity->getBearbeiter()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'useCampaignStartDate',
					'nowrap' => 'nowrap'
				),
				((boolean) $campaignEntity->getUse_campaign_start_date() === true 
					? '<img src="img/Tango/22/actions/appointment-new.png" title="' . $campaignEntity->getDatum()->format('d.m.Y H:i') . '" /> ' . $campaignEntity->getDatum()->format('d.m.Y H:i') 
					: ''
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'blacklist'
				),
				($campaignEntity->getBlacklist() == 'X' 
					? '<span style="background-color:red;color:#FFFFFF;padding:1px 5px 1px 5px;font-weight:bold;font-size:12px">X</span>' 
					: ''
				)
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'status',
					'nowrap' => 'nowrap'
				),
				$_SESSION['campaign']['statusDataArray'][$campaignEntity->getStatus()]['label']
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'deliverySystem',
					'nowrap' => 'nowrap'
				),
				$campaignEntity->getVersandsystem()
			) 
			. \HtmlTableUtils::createTableCellWidthContent(
				array(
					'class' => 'deliverySystemDistributor',
					'nowrap' => 'nowrap'
				),
				($campaignEntity->getDsd_id() > 0 
					? $clientDeliverySystemDistributorEntityDataArray[$campaignEntity->getDsd_id()]->getTitle() 
					: ''
				)
			)
		. $tableRowDataArray['end']
	;


	$footerDataArray['targetVolume'] += $campaignEntity->getVorgabe_m();
	$footerDataArray['bookingAmount'] += $campaignEntity->getGebucht();
	$footerDataArray['shippedAmount'] += $campaignEntity->getVersendet();
	$footerDataArray['openingRate'] += $campaignEntity->getOpenings_all();
	$footerDataArray['clickRate'] += $campaignEntity->getKlicks_all();
}