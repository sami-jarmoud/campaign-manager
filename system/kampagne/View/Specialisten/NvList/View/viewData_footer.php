<?php
// createTableFooterRow - Tag
$tableFooterDataArray = \HtmlTableUtils::createTableFooterRow(
	array(
		'class' => 'footerRow trKzdgk'
	)
);

$tableFooterContent = 
	$tableFooterDataArray['begin']
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $_SESSION['tcmData']['beforeColspanGroup'],
				'class' => 'beforeKzdgk'
			), ''
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'class' => 'targetVolume'
			),
			\FormatUtils::numberFormat($footerDataArray['targetVolume'])
		) . \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'class' => 'bookingAmount'
			),
			\FormatUtils::numberFormat($footerDataArray['bookingAmount'])
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'class' => 'shippedAmount'
			),
			\FormatUtils::numberFormat($footerDataArray['shippedAmount'])
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'class' => 'openingRate'
			),
			\FormatUtils::rateCalculation(
				$footerDataArray['openingRate'],
				$footerDataArray['bookingAmount']
			) . ' %'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'class' => 'clickRate'
			),
			\FormatUtils::rateCalculation(
				$footerDataArray['clickRate'],
				$footerDataArray['bookingAmount']
			) . ' %'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'colspan' => $_SESSION['tcmData']['afterColspanGroup'],
				'class' => 'afterKzdgk'
			),
			''
		)
	. $tableFooterDataArray['end']
;
unset($tableFooterDataArray);