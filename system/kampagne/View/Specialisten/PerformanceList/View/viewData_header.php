<?php
/**
 * createTableHeaderRow - Tag
 */
$tableHeadDataArray = \HtmlTableUtils::createTableHeaderRow(
	array(
		'class' => 'headerStyleColumn'
	)
);

$tableHeaderContent = $tableHeadDataArray['begin'];

if ((boolean) $campaignView === true) {
	$tableHeaderContent .= 
		\HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'width' => 160
			),
			$listName
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Gebucht'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Versendet'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'&Uuml;Quote (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'&Ouml;ffner (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Klicker (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Hardbounces (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Softbounces (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Abmelder (%)'
		)
	;
} else {
	$tableHeaderContent .= 
		\HtmlTableUtils::createTableHeaderCellWidthContent(
			array(
				'width' => 160
			),
			$listName
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Versendet'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'&Ouml;ffner (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Klicker (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Hardbounces (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Softbounces (%)'
		) 
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			$cellHeaderSettingsDataArray,
			'Abmelder (%)'
		)
	;
}
$tableHeaderContent .= $tableHeadDataArray['end'];

unset($tableHeadDataArray);