<?php
// createTableFooterRow - Tag
$tableFooterDataArray = \HtmlTableUtils::createTableFooterRow(
	array(
		'class' => 'footerRow'
	)
);

$tableFooterContent = 
	$tableFooterDataArray['begin']
		. \HtmlTableUtils::createTableHeaderCellWidthContent(
			array(),
			'Gesamt:'
		)
;
if ((boolean) $campaignView === true) {
	$tableFooterContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalBooked'])
	);
}

$tableFooterContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
	$cellSettingsDataArray,
	\FormatUtils::numberFormat($footerDataArray['totalSend'])
);

if ((boolean) $campaignView === true) {
	$tableFooterContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::rateCalculation(
			$footerDataArray['totalSend'],
			$footerDataArray['totalBooked']
		) . ' %'
	);
}

$tableFooterContent .= 
	\HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalOpener'])
			. ' (' . \FormatUtils::rateCalculation(
				$footerDataArray['totalOpener'],
				$footerDataArray['totalSend']
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalClicks'])
			. ' (' . \FormatUtils::rateCalculation(
				$footerDataArray['totalClicks'],
				$footerDataArray['totalSend']
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalHBounces'])
			. ' (' . \FormatUtils::rateCalculation(
				$footerDataArray['totalHBounces'],
				$footerDataArray['totalSend']
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalSBounces'])
			. ' (' . \FormatUtils::rateCalculation(
				$footerDataArray['totalSBounces'],
				$footerDataArray['totalSend']
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableHeaderCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($footerDataArray['totalUnsubscriber'])
			. ' (' . \FormatUtils::rateCalculation(
				$footerDataArray['totalUnsubscriber'],
				$footerDataArray['totalSend']
			) . ' %)'
);

$tableFooterContent .= $tableFooterDataArray['end'];