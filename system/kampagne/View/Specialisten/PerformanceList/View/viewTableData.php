<?php
/* @var $campaignEntity \CampaignEntity */

if ((boolean) $campaignView === true) {
	$tableContent .= \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getGebucht())
	);
}

$tableContent .= \HtmlTableUtils::createTableCellWidthContent(
	$cellSettingsDataArray,
	\FormatUtils::numberFormat($campaignEntity->getVersendet())
);

if ((boolean) $campaignView === true) {
	$tableContent .= \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::rateCalculation(
			$campaignEntity->getVersendet(),
			$campaignEntity->getGebucht()
		) . ' %'
	);
}

$tableContent .= 
	\HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getOpenings_all())
			. ' (' . \FormatUtils::rateCalculation(
				$campaignEntity->getOpenings_all(),
				$campaignEntity->getVersendet()
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getKlicks_all())
			. ' (' . \FormatUtils::rateCalculation(
				$campaignEntity->getKlicks_all(),
				$campaignEntity->getVersendet()
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getHbounces())
			. ' (' . \FormatUtils::rateCalculation(
				$campaignEntity->getHbounces(),
				$campaignEntity->getVersendet()
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getSbounces())
			. ' (' . \FormatUtils::rateCalculation(
				$campaignEntity->getSbounces(),
				$campaignEntity->getVersendet()
			) . ' %)'
	) 
	. \HtmlTableUtils::createTableCellWidthContent(
		$cellSettingsDataArray,
		\FormatUtils::numberFormat($campaignEntity->getAbmelder())
			. ' (' . \FormatUtils::rateCalculation(
				$campaignEntity->getAbmelder(),
				$campaignEntity->getVersendet()
			) . ' %)'
		)
;