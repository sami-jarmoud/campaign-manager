<?php
require_once('../../loadEmsAutoloader.php');

\EmsAutoloader::init();
\EmsAutoloader::startAndCheckClientSession();
\EmsAutoloader::initErrorReporting();

\header('Content-Type: text/html; charset=utf-8');



/**
 * createTableHeaderContent
 * 
 * @param array $tableRowDataArray
 * @return string
 */
function createTableHeaderContent(array $tableRowDataArray) {
	return 
		$tableRowDataArray['begin'] 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Datum'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Kampagne'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Kunde / Agentur'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Asp.'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Verteiler'
			)
			 . \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'align' => 'right'
				),
				'Gebucht'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 80,
					'align' => 'right'
				),
				'Real'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 240
				),
				'Notiz'
			)
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 240
				),
				'Zielgruppe'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 15
				),
				'BL'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(
					'width' => 30
				),
				'Status'
			)
		. $tableRowDataArray['end']
	;
}

/**
 * getCampaignQueryPartsByDateTime
 * 
 * @param \DateTime $dateTime
 * @return array
 */
function getCampaignQueryPartsByDateTime(\DateTime $dateTime) {
	return array(
		'SELECT' => '*',
		'WHERE' => array(
			'kId' => array(
				'sql' => '`k_id`',
				'value' => '`nv_id`',
				'comparison' => 'fieldEqual'
			),
			'date' => array(
				'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
				'value' => $dateTime->format('Y-m-d'),
				'comparison' => '='
			),
			'special' => array(
				'sql' => 'LEFT(`k_name`, 3) != "NV:"',
				'value' => '',
				'comparison' => ''
			),
		),
		'ORDER_BY' => '`datum` ASC'
	);
}



/**
 * init $debugLogManager
 * init $clientManager
 */
require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager debugLogManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager ClientManager */


// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);
#$debugLogManager->logData('SESSION', $_SESSION);



/**
 * getClientsAccessDataArrayByLoggedUser
 * 
 * debug
 */
$userClientsDataArray = CampaignAndOthersUtils::getClientsAccessDataArrayByLoggedUser();
$debugLogManager->logData('userClientsDataArray', $userClientsDataArray);


$dayDiff = 0;
if (isset($_GET['dateBegin']) && \strlen($_GET['dateBegin']) > 0
) {
	$dateBeginObj = new \DateTime($_GET['dateBegin']);

	if (isset($_GET['dateEnd']) 
		&& \strlen($_GET['dateEnd']) > 0
	) {
		$dateEndObj = new \DateTime($_GET['dateEnd']);
	} else {
		$dateEndObj = $dateBeginObj;
	}

	// debug
	$debugLogManager->logData('dateEndObj', $dateEndObj);
	
	$interval = $dateBeginObj->diff($dateEndObj);
	$dayDiff = (int) $interval->format('%a');
	unset($interval, $dateEndObj);
} else {
	$dateBeginObj = new \DateTime('now');
}
// debug
$debugLogManager->logData('dateBeginObj', $dateBeginObj);

$daysDataArray = array(
	$dateBeginObj
);
if ($dayDiff > 0) {
	for ($i = 1; $i < ($dayDiff + 1); $i++) {
		$dateCloneObj = clone $dateBeginObj;
		$daysDataArray[] = $dateCloneObj->add(new \DateInterval('P' . $i . 'D'));
	}
} else {
	if ((int) $dateBeginObj->format('N') === 5) {
		for ($i = 1; $i < 4; $i++) {
			$dateCloneObj = clone $dateBeginObj;
			$daysDataArray[] = $dateCloneObj->add(new \DateInterval('P' . $i . 'D'));
		}
	} else {
		for ($i = 1; $i < 2; $i++) {
			$dateCloneObj = clone $dateBeginObj;
			$daysDataArray[] = $dateCloneObj->add(new \DateInterval('P' . $i . 'D'));
		}
	}
}
// debug
$debugLogManager->logData('dayDiff', $dayDiff);
$debugLogManager->logData('dayOfWeek', $dateBeginObj->format('N'));
$debugLogManager->logData('daysDataArray', $daysDataArray);


// getCalView
$calView = \file_get_contents('../calView.html');


/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(
	array(
		'class' => 'tcm_table',
		'width' => 950
	)
);

/**
 * createTableRow - Tag
 */
$tableRowDataArray = \HtmlTableUtils::createTableRow(array());


$tbl = $calView .
	$tableDataArray['begin']
;
$tr = null;

try {
	// debug
	$debugLogManager->beginGroup('dayView');
	
	$x = 1;
	foreach ($daysDataArray as $dateTimeObj) {
		// debug
		$debugLogManager->beginGroup('date: ' . $dateTimeObj->format('d.m.Y'));

		$pageBreak = '';
		if ($x > 1) {
			$pageBreak = 'page-break-before: always;';
		}

		/**
		* createTableRow - Tag
		*/
		$tableRowWidthStyleDataArray = \HtmlTableUtils::createTableRow(
			array(
				'style' => $pageBreak
			)
		);

		$tr .= 
			$tableRowWidthStyleDataArray['begin'] 
				. \HtmlTableUtils::createTableCellWidthContent(
					array(
						'colspan' => 11,
						'style' => 'border:0px;color:red;font-weight:bold;font-size:12px;'
					),
					\DateUtils::getWeekDay($dateTimeObj) . ', ' . $dateTimeObj->format('d.m.Y') 
						. '<div style="border-top:2px solid red;text-align:bottom;width:100%;"></div>'
				)
			. $tableRowWidthStyleDataArray['end']
		;

		foreach ($userClientsDataArray as $clientId => $clientName) {
			// debug
			$debugLogManager->beginGroup('mandant: ' . $clientName);

			$clientEntity = $clientManager->getClientDataById($clientId);
			if (!($clientEntity instanceof \ClientEntity)) {
				throw new \DomainException('invalid ClientEntity');
			}
			
			$mandant = $clientEntity->getAbkz();
			require($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . $emsWorkPath . \DIRECTORY_SEPARATOR . 'db_connect.inc.php');
			
			/**
			 * init $campaignManager
			 */
			require(DIR_configsInit . 'initCampaignManager.php');
			/* @var $campaignManager CampaignManager */
			
			/**
			 * getCampaignQueryPartsByDateTime
			 * 
			 * debug
			 */
			$queryPartsDataArray = \getCampaignQueryPartsByDateTime($dateTimeObj);
			$debugLogManager->logData('queryPartsDataArray', $queryPartsDataArray);
			
			/**
			 * getCampaignsAndCustomerDataItemsByQueryParts
			 * 
			 * debug
			 */
			$campaignDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($queryPartsDataArray);
			#$debugLogManager->logData('campaignDataArray', $campaignDataArray);
			if (\count($campaignDataArray) > 0) {
				/**
				 * getDeliverySystemDistributorsDataItemsByClientId
				 */
				$clientDeliverySystemDistributorEntityDataArray = $clientManager->getDeliverySystemDistributorsDataItemsByClientId(
					$clientId,
					$clientManager->getUnderClientIdsByClientId($clientId)
				);
				$debugLogManager->logData('clientDeliverySystemDistributorEntityDataArray', $clientDeliverySystemDistributorEntityDataArray);
				
				$tr .= 
					$tableRowDataArray['begin'] 
						. \HtmlTableUtils::createTableCellWidthContent(
							array(
								'colspan' => 11,
								'style' => 'height:50px;vertical-align:bottom;border-left:0px;border-right:0px;',
								'class' => 'tcm_mandant'
							),
							'<img src="http://maas.treaction.de/system/img/logo/' . $mandant . '.gif" height="20" />'
						)
					. $tableRowDataArray['end'] 
					. \createTableHeaderContent($tableRowDataArray)
				;
				
				// viewData_week
				require('View/viewData_list.php');
				$tr .= $tableContent;
			}

			// debug
			$debugLogManager->endGroup();
		}
		$x++;

		// debug
		$debugLogManager->endGroup();
	}
	
	// debug
	$debugLogManager->endGroup();
} catch (\Exception $e) {
	require(DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}
$tbl .= $tr 
	. $tableDataArray['end']
;

$_SESSION['data'] = utf8_decode($tbl);
require_once('../menu.php');
?>
<script type="text/javascript">
	function changeView() {
		setRequest('kampagne/View/Specialisten/DayList/index.php' 
				+ '?dateBegin=' + YAHOO.util.Dom.get('in_tcmList').value 
				+ '&dateEnd=' + YAHOO.util.Dom.get('out_tcmList').value 
				+ '&p=1'
			,
			'k_tcm'
		);
	}
</script>