<?php
header('Content-Type: text/html; charset=ISO-8859-1');
session_start();
$mandant = $_SESSION['mandant'];
include('../db_connect.inc.php');
include('../library/util.php');

if ($_POST['asp']) {
	createMailing($_POST['asp'],$_POST['mailingID'],$_POST['kid']);
}


$kid = $_GET['kid'];

$sql = mysql_query("SELECT * FROM $kampagne_db WHERE k_id = '$kid'",$verbindung);
$z = mysql_fetch_array($sql);

$agentur = $z['agentur'];
$k_name = $z['k_name'];
$notiz = $z['notiz'];
$betreff = $z['betreff'];
$absendername = $z['absendername'];
$versandsystem = $z['versandsystem'];
$mimeType = $z['mailtyp'];
$mailingID = $z['mail_id'];
$datum = $z['datum'];

$k_name = $agentur.", ".$k_name." [".util::datum_vs($datum)."]";

#$testliste = getTestliste($versandsystem);

if ($versandsystem == 'BM') {
	$verteiler = getLists();
} 

if ($mailingID != '') {
	$mailingDataArr = getMailingData($versandsystem,$mailingID,$kid);
	$html = htmlentities($mailingDataArr['Html']);
	$text = $mailingDataArr['Text'];
	$cm_absender_prefix = $mailingDataArr['Absenderemail'];
	$verteiler = getLists($mailingDataArr['Verteiler']);
}


function getLists($listIDs) {
	global $mandant;
	$verteiler = NULL;
	
	include('../bm_soap/bm_'.$mandant.'.php');
	require_once('../bm_soap/broadmail_rpc.php');
	
	$factory = new BroadmailRpcFactory($bmMID,$bmUser,$bmPass);
	if ($factory->getError()) {
				die ('Error during login<code>'.$factory->getError().'</code>');
	}
	
	$RecipientListWebservice = $factory->newRecipientListWebservice();
	$dataArr = $RecipientListWebservice->getDataSet();
	
	$i = 1;
	foreach ($dataArr as $data) {
		$checked = "";
		
		if (in_array($data[0],$listIDs)) {
			$checked = "checked";
		}
		
		if ($i > 1 && $data[3] > 0) {
			$listName = str_replace('[','',$data[1]);
			$listName = str_replace(']','',$listName);
			$tab_td .= "{id: '<input type=\'checkbox\' name=\'verteiler[]\' id=\'verteiler[]\' value=\'".$data[0]."\'  ".$checked."/>', name:'".$listName."', cnt:'".util::zahl_format($data[3])."', description: '".$data[2]."'},";
		}
		
		$i++;
	}
	
	$factory->logout();
	$tab_td = substr($tab_td,0,-1);
			
	$data = '<script type="text/javascript">YAHOO.example.Datalist = {lists: ['.$tab_td.']}</script>';
	print $data;
	
}


function getTestliste($asp) {
	global $mandant;
	
	
	if ($asp == 'BM') {
		
		include('../bm_soap/bm_'.$mandant.'.php');
		require_once('../bm_soap/broadmail_rpc.php');
		
		$factory = new BroadmailRpcFactory($bmMID,$bmUser,$bmPass);
		if ($factory->getError()) {
					die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
		}
		
		$RecipientWebservice = $factory->newRecipientWebservice();
		
		$getRecipientAll = $RecipientWebservice->getAllAdvanced($testlistID,$attributeNames,0,"Email",true,0,1000);
		
		foreach ($getRecipientAll as $recipient) {
		
			$tab_td .= "{id: '<input onclick=\'test(\"".$recipient[0]."\");\' type=\'checkbox\' name=\'testempfaenger[]\' value=\'".$recipient[0]."\' />', email:'".$recipient[0]."', vn:'".$recipient[1]."', nn:'".$recipient[2]."', str:'".$recipient[3]."', plz:'".$recipient[4]."', ort:'".$recipient[5]."'},";
			
		}
		
		$tab_td = substr($tab_td,0,-1);
			
		$data = '<script type="text/javascript">YAHOO.example.Data = {testempfaenger: ['.$tab_td.']}</script>';
		print $data;
		$factory->logout();
	}
        
}

function createMailing($asp,$mailingID,$kid) {

	global $mandant;
	include('../db_connect.inc.php');
	
	$agentur 			= utf8_decode($_POST['agentur']);
	$k_name 			= utf8_decode($_POST['k_name']);
	$notiz 				= utf8_decode($_POST['notiz']);
	$betreff 			= utf8_decode($_POST['betreff']);
	$absendername 		= utf8_decode($_POST['absendername']);
	$absender_prefix 	= utf8_decode($_POST['absender_prefix']);
	$html 				= utf8_decode($_POST['html']);
	$text 				= utf8_decode($_POST['text']);
	$verteilerArr		= $_POST['verteiler'];
	
	if ($html == '' && $text != '') {
		$mimeType = 'text/plain';
	} 
	
	if ($html != '' && $text == '') {
		$mimeType = 'text/html';
	}
	
	if ($html != '' && $text != '') {
		$mimeType = 'multipart/alternative';
	}
	
	if ($asp == 'BM') {
	
		include('../bm_soap/bm_'.$mandant.'.php');
		require_once('../bm_soap/broadmail_rpc.php');
		
		$factory = new BroadmailRpcFactory($bmMID,$bmUser,$bmPass);
		if ($factory->getError()) {
					die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
		}
		
		$MailingWebservice = $factory->newMailingWebservice();
		
		if (!$mailingID) {
			$create_m_id = $MailingWebservice->create("regular", $k_name, $mimeType, $verteilerArr, $absender_prefix, $absendername, NULL);
		} else {
			$create_m_id = $mailingID;
			$setFrom = $MailingWebservice->setFrom($create_m_id,$absender_prefix,$absendername);
			$setMime = $MailingWebservice->setMimeType($create_m_id,$mimeType);
			$setName = $MailingWebservice->setName($create_m_id,$k_name);
			$setRecipientListIds = $MailingWebservice->setRecipientListIds($create_m_id,$verteilerArr);
		}
		
		$subject = $MailingWebservice->setSubject($create_m_id,$betreff);
		$setDescription = $MailingWebservice->setDescription($create_m_id,$notiz);
		
		if ($html == '' && $text != '') {
			$setContent = $MailingWebservice->setContent($create_m_id,"text/plain",$text);
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/plain",true);
		} 
		
		if ($html != '' && $text == '') {
			$setContent = $MailingWebservice->setContent($create_m_id,"text/html",$html);
			$setOpenTracking = $MailingWebservice->setOpenTrackingEnabled($create_m_id,true);
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/html",true);
		}
		
		if ($html != '' && $text != '') {
			$setContent = $MailingWebservice->setContent($create_m_id,"text/html",$html);
			$setContent2 = $MailingWebservice->setContent($create_m_id,"text/plain",$text);
			$setOpenTracking = $MailingWebservice->setOpenTrackingEnabled($create_m_id,true);
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/html",true);
			$setClickTracking2 = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/plain",true);
		}
		
			
			switch ($mimeType) {
				case 'multipart/alternative': $mime_upd = ",mailtyp='m'";break;
				case 'text/html': $mime_upd = ",mailtyp='h'";break;
				case 'text/plain': $mime_upd = ",mailtyp='t'";break;
			}
			
		$factory->logout();
			
			
			$update_sync = mysql_query("UPDATE $kampagne_db SET 
									mail_id = '$create_m_id',
									absendername = '$absendername',
									betreff = '$betreff',
									status = 1
									".$mime_upd."
								WHERE k_id='$kid'",$verbindung);
		
	}

}


function getMailingData($asp,$mailingID,$kid) {

	global $mandant;
	global $agentur;
	global $k_name;
	global $notiz;
	global $betreff;
	global $absendername;
	global $mimeType;
	
	
	include('../db_connect.inc.php');
	
	if ($asp == 'BM') {
	
		include('../bm_soap/bm_'.$mandant.'.php');
		require_once('../bm_soap/broadmail_rpc.php');

		$factory = new BroadmailRpcFactory($bmMID,$bmUser,$bmPass);
		if ($factory->getError()) {
			die ('<h2>Error during login</h2><code>'.$factory->getError().'</code>');
		}
		
		$MailingWebservice = $factory->newMailingWebservice();
		
		$getRecipientListIds = $MailingWebservice->getRecipientListIds($mailingID);
				
		$getFromEmailPrefix = $MailingWebservice->getFromEmailPrefix($mailingID);		
		
		if ($mimeType == 't') {
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($mailingID,"text/plain",false);
			$getContentTxt = $MailingWebservice->getContent($mailingID,"text/plain");
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/plain",true);
		} 
		
		if ($mimeType == 'h') {
			$setClickTracking2 = $MailingWebservice->setClickTrackingEnabled($mailingID,"text/html",false);
			$getContentHtml = $MailingWebservice->getContent($mailingID,"text/html");
			$setClickTracking2 = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/html",true);
		}
		
		if ($mimeType == 'm') {
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($mailingID,"text/plain",false);
			$setClickTracking2 = $MailingWebservice->setClickTrackingEnabled($mailingID,"text/html",false);
			$getContentTxt = $MailingWebservice->getContent($mailingID,"text/plain");
			$getContentHtml = $MailingWebservice->getContent($mailingID,"text/html");
			$setClickTracking = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/html",true);
			$setClickTracking2 = $MailingWebservice->setClickTrackingEnabled($create_m_id,"text/plain",true);
		}
		
	}
			
	$DataArr = array(
								"Absenderemail" => $getFromEmailPrefix,
								"Html"			=> $getContentHtml,
								"Text"			=> $getContentTxt,
								"Verteiler"		=> $getRecipientListIds
							);
	$factory->logout();					
	return $DataArr;
	
}

?>
<input type="hidden" name="kid" value="<?php print $kid; ?>" />
<input type="hidden" id="mimeType" value="<?php print $mimeType; ?>" />
<input type="hidden" id="versandsystem" value="<?php print $versandsystem; ?>" />
<input type="hidden" name="mailingID" id="mailingID" value="<?php print $mailingID; ?>" />
<div id="tab_vs" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#tab_vs1"><em>Mailingdaten</em></a></li>
        <li id="tab_html" style="display:none"><a href="#tab_vs2"><em>HTML</em></a></li>
        <li id="tab_text" style="display:none"><a href="#tab_vs3"><em>TEXT</em></a></li>
        <li id="tab_verteiler" style="display:none"><a href="#tab_vs4"><em>Verteiler</em></a></li>
        <!--<li><a href="#tab_vs5"><em>Testmail</em></a></li>-->
    </ul>

    <div class="yui-content" style="margin:0px;padding:0px;">
        <div id="tab_vs1">
			<table class="neu_k">
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td class="info" align="right">Versandsystem</td>
					<td align="left"><input type="radio" name="asp" value="BM" checked/> Broadmail</td>
					<td width="100%"></td>
				</tr>
				<tr style="background-color:#E4E4E4;">
					<td class="info" align="right">Name</td>
					<td align="left"><input class="inp_vs" type="text" name="k_name" value="<?php print $k_name; ?>" /></td>
					<td width="100%"></td>
				</tr>
				<tr>
					<td class="info" align="right" valign="top">Beschreibung</td>
					<td align="left"><textarea class="inp_vs" name="notiz" rows="3"><?php print $notiz; ?></textarea></td>
					<td width="100%"></td>
				</tr>
				<tr style="background-color:#E4E4E4;">
					<td class="info" align="right">Betreff</td>
					<td align="left"><input class="inp_vs" type="text" name="betreff" value="<?php print $betreff; ?>" /></td>
					<td width="100%"></td>
				</tr>
				<tr>
					<td class="info" align="right">Absendername</td>
					<td align="left"><input class="inp_vs" type="text" name="absendername" value="<?php print $absendername; ?>" /></td>
					<td width="100%"></td>
				</tr>
				<tr style="background-color:#E4E4E4;">
					<td class="info" align="right">Absender Email</td>
					<td align="left"><input  type="text" style="text-align:right" name="absender_prefix" value="<?php print $cm_absender_prefix; ?>" /><?php print $cm_absender_suffix; ?></td>
					<td width="100%"></td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
			</table>
        </div>
		
        <div id="tab_vs2" style="display:none">
			<table class="neu_k">
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="2" style="color:#333333;padding-bottom:10px">Bitte f&uuml;gen Sie hier den HTML-Quellcode ein. </td>
				</tr>
				<tr>
					<td class="info" align="right" valign="top">HTML</td>
					<td align="left"><textarea name="html" style="width:500px;height:210px;"><?php print $html; ?></textarea></td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
			</table>
		</div>
        
        <div id="tab_vs3" style="display:none">
			<table class="neu_k">
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td colspan="2" style="color:#333333;padding-bottom:10px">Bitte f&uuml;gen Sie hier die Textversion ein.</td>
				</tr>
				<tr>
					<td class="info" align="right" valign="top">TEXT</td>
					<td align="left"><textarea name="text" style="width:500px;height:210px;"><?php print $text; ?></textarea></td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
			</table>
		</div>
		
        <div id="tab_vs4" style="display:none"> 
			<table class="neu_k">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td style="padding:5px;"><div id="list_table" style="white-space:nowrap;"></div></td>
				</tr>
				<tr>
					<td></td>
				</tr>
			</table>
        </div>
        
        <div id="tab_vs5" style="display:none"> 
			<table class="neu_k">
           		<tr>
					<td><span id="t_html"><button onclick="check('html');return false;">HTML Testmail senden</button> &nbsp;&nbsp;</span><span id="t_text"><button onclick="check('text');return false;">TEXT Testmail senden</button> &nbsp;&nbsp;</span> <span id="t_ausgabe"></span></td>
				</tr>
				<tr>
					<td style="padding:5px;">
                        	
                        	<div id="testmail_table" style="white-space:nowrap;"></div>
                    </td>
				</tr>
			</table>
        </div>
        
        
    </div>
</div>

                    
                           
<script type="text/javascript">
var tabView_vs = new YAHOO.widget.TabView('tab_vs');

var mimeType = document.getElementById('mimeType').value;
var vs = document.getElementById('versandsystem').value;
var mailingID = document.getElementById('mailingID').value;

if (mimeType == 't') {
	document.getElementById('tab_text').style.display = '';
	document.getElementById('tab_vs3').style.display = '';
	document.getElementById('t_html').style.display = 'none';
}

if (mimeType == 'h') {
	document.getElementById('tab_html').style.display = '';
	document.getElementById('tab_vs2').style.display = '';
	document.getElementById('t_text').style.display = 'none';
}

if (mimeType == 'm') {
	document.getElementById('tab_html').style.display = '';
	document.getElementById('tab_text').style.display = '';
	document.getElementById('tab_vs2').style.display = '';
	document.getElementById('tab_vs3').style.display = '';
}

if (vs == 'BM') {document.getElementById('tab_verteiler').style.display = '';document.getElementById('tab_vs4').style.display = '';}


var t = '';
var s = '';
var s2 = '';
var i = 0;

function check(mime) {
	if (i==0) {
		alert("Bitte einen Empf�nger ausw�hlen.");
		return false;
	} else {
		confirm_check = confirm(t);
		if (confirm_check == true) {
			setRequest_default("kampagne/sendTestmail.php?mime="+mime+"&mailingID="+mailingID+"&e="+s2+"&vs="+vs,"t_ausgabe");
		}
	}
}

function test(e) {

e = " - "+e +"\n";

var vorhanden = s.search(e);

	if (vorhanden != -1){
		s = s.replace(e, "");
		i = i-1;
	} else {
		s = s + e;
		i = i+1;
	}
	
	s2 = s.replace(" - ", "");
	
	t = "Die Testmail wird an folgende Empf�nger ("+i+") versendet:\n\n"+s;
}



YAHOO.util.Event.onDOMReady(function() {


    YAHOO.example.ClientPagination = function() {
	
        var myColumnDefs = [
            {key:"id", label:""},
            {key:"email", label:"Email", sortable:true},
            {key:"vn", label:"Vorname", sortable:true},
            {key:"nn", label:"Nachname", sortable:true},
            {key:"str", label:"Strasse", sortable:true},
			{key:"plz", label:"Plz", sortable:true},
			{key:"ort", label:"Ort", sortable:true}
        ];
 
 
		var myDataSource = new YAHOO.util.DataSource(YAHOO.example.Data.testempfaenger);
        myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
        myDataSource.responseSchema = {
            resultsList: "records",
            fields: ["id","email","vn","nn","str","plz","ort"]
        };
						
        var myDataTable = new YAHOO.widget.ScrollingDataTable("testmail_table", myColumnDefs,
                myDataSource, {height:"20em", width:"565px"});
                
        return {
            oDS: myDataSource,
            oDT: myDataTable
        };
    }();
	
	
	var myColumnDefs2 = [
            {key:"id", label:""},
            {key:"name", label:"Verteiler", sortable:true},
			{key:"cnt", label:"Empf�nger"},
			{key:"description", label:"Beschreibung"}
        ];
 
 
		var myDataSource2 = new YAHOO.util.DataSource(YAHOO.example.Datalist.lists);
        myDataSource2.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
        myDataSource2.responseSchema = {
            resultsList: "records",
            fields: ["id","name","cnt","description"]
        };
						
        var myDataTable2 = new YAHOO.widget.ScrollingDataTable("list_table", myColumnDefs2,
                myDataSource2, {height:"20em"});
                
        return {
            oDS2: myDataSource2,
            oDT2: myDataTable2
        };
	
	
	
});


</script>