<?php
\header('Content-Type: text/html; charset=utf-8');

/**
 * createUserAndEditTableContent
 * 
 * @param array $userTabFieldsDataArray
 * @param string $tableHeaderContent
 * @param string $tableEditHeaderContent
 * @return void
 */
function createUserAndEditTableContent(array $userTabFieldsDataArray, &$tableHeaderContent, &$tableEditHeaderContent) {
	// createTableRow - Tag
	$tableEditHeaderRowDataArray = \HtmlTableUtils::createTableRow(
		array(
			'id' => 'data_tab_felder_edit',
			'class' => 'hidden'
		)
	);
	
	// createTableRow - Tag
	$tableHeaderRowDataArray = \HtmlTableUtils::createTableRow();
	
	$tableEditHeaderContent = 
		$tableEditHeaderRowDataArray['begin'] 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				''
			)
	;
	
	$tableHeaderContent = 
		$tableHeaderRowDataArray['begin'] 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Status'
			) 
			. \HtmlTableUtils::createTableHeaderCellWidthContent(
				array(),
				'Kampagne'
			)
	;
	
	foreach ($userTabFieldsDataArray as $userField) {
		foreach (\CampaignAndCustomerUtils::$allTabFieldsDataArray as $tabField => $tabArray) {
			if ($userField == $tabField) {
				switch ($tabArray['sublabel']) {
					case 'Re-St':
						$title = $tabArray['label'];
						break;
					
					case 'Z-Rep':
						$title = $tabArray['label'];
						break;
					
					case 'Infomail':
						$title = $tabArray['label'];
						break;
					
					default:
						$title = '';
						break;
				}
				
				$tableEditHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => $tabField,
						'align' => 'center'
					),
					'<img class="delFeld" id="' . $tabField . '"  src="../img/Tango/16/actions/dialog-close.png" title="Spalte entfernen" />'
				);
				
				$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
					array(
						'class' => $tabField,
						'title' => $title
					),
					$tabArray['sublabel']
				);
			}
		}
	}
	
	$tableEditHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'width' => '100%'
		),
		''
	) . $tableEditHeaderRowDataArray['end'];
	
	$tableHeaderContent .= \HtmlTableUtils::createTableHeaderCellWidthContent(
		array(
			'width' => '100%'
		),
		''
	) . $tableHeaderRowDataArray['end'];
}

/**
 * getSyncContentByDeliverySystem
 * 
 * @param \CampaignEntity $campaignEntity
 * @return string
 */
function getSyncContentByDeliverySystem(\CampaignEntity $campaignEntity) {
	$result = '';

	if (\strlen($campaignEntity->getMail_id()) === 0 
		&& (
			$campaignEntity->getVersandsystem() == 'BM' 
			|| $campaignEntity->getVersandsystem() == 'E' 
			|| $campaignEntity->getVersandsystem() == 'K'
                        || $campaignEntity->getVersandsystem() == 'SE'
                        || $campaignEntity->getVersandsystem() == 'M'
                        || $campaignEntity->getVersandsystem() == '4W18'
                        || $campaignEntity->getVersandsystem() == '4WMS'
                        || $campaignEntity->getVersandsystem() == 'S18'
                        || $campaignEntity->getVersandsystem() == 'SB'
                        || $campaignEntity->getVersandsystem() == 'SS'
                        || $campaignEntity->getVersandsystem() == '4WB'
                        || $campaignEntity->getVersandsystem() == '4WBL'
                        || $campaignEntity->getVersandsystem() == 'SBL'
                        || $campaignEntity->getVersandsystem() == 'SSE'
                        || $campaignEntity->getVersandsystem() == '4WSE'
                        || $campaignEntity->getVersandsystem() == 'SRES'
                        || $campaignEntity->getVersandsystem() == '4WE'                
		)
	) {
		$result = ' <img src="img/icons/swap.png" alt="bitte synchronisieren" title="bitte synchronisieren" />';
	}

	return $result;
}

/**
 * getTodayTab
 * 
 * @param array $dataArray
 * @param \DateTime $today
 * @return boolean
 */
function getTodayTab(array $dataArray, \DateTime $today) {
	$todayTab = false;

	foreach ($dataArray as $itemToday) {
		$tmpObj = new \DateTime($itemToday['datum']);

		$passedHours = \floor(($today->format('U') - $tmpObj->format('U')) / 3600);
		if ($passedHours > 0) {
			$todayTab = true;

			break;
		}
	}

	return $todayTab;
}



/**
 * init
 * 
 * loadAndInitEmsAutoloader
 * db_connect
 * 
 * $debugLogManager
 * $campaignManager
 * $clientManager
 */
require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'helperFunction.php');
require_once(getEmsWorkRootPath() . 'loadAndInitEmsAutoloader.php');
require_once(DIR_configs . 'exceptionsHandlingAlert.php');
$_SESSION['exception'] = 'FALSE';
$mandant = \RegistryUtils::get('clientEntity')->getAbkz();
require(\RegistryUtils::get('emsWorkPath') . 'db_connect.inc.php');

require_once(DIR_configsInit . 'initDebugLogManager.php');
/* @var $debugLogManager \debugLogManager */

require_once(\DIR_configsInit . 'initCampaignManager.php');
/* @var $campaignManager \CampaignManager */

require_once(DIR_configsInit . 'initClientManager.php');
/* @var $clientManager \ClientManager */



// debug
$debugLogManager->logData('GET', $_GET);
$debugLogManager->logData('POST', $_POST);

\CampaignFieldsUtils::$textLenght = 40;
\CampaignFieldsUtils::$bounceHThreshold = $bounceHThreshold;
\CampaignFieldsUtils::$bounceSThreshold = $bounceSThreshold;

/**
 * session - Settings
 * 
 * debug
 * TODO: remove (use CampaignFieldsUtils::$), @deprecated
 */
$_SESSION['campaignView'] = array(
	'textLenght' => 40,
	'bounceHThreshold' => $bounceHThreshold,
	'bounceSThreshold' => $bounceSThreshold
);

$itemsLimit = 50;

// year
$year = isset($_GET['jahr']) ? \intval($_GET['jahr']) : \date('Y');
$debugLogManager->logData('year', $year);

$beginPageItem = isset($_GET['seite_start']) ? \intval($_GET['seite_start']) : 0;
$debugLogManager->logData('beginPageItem', $beginPageItem);

$actualPageItem = isset($_GET['seite_now']) ? \intval($_GET['seite_now']) : 1;
$debugLogManager->logData('actualPageItem', $actualPageItem);

$campaignAnchor = isset($_GET['campaignAnchor']) ? '#' . \DataFilterUtils::filterData($_GET['campaignAnchor']) : '#heute';
$debugLogManager->logData('campaignAnchor', $campaignAnchor);


$today = new \DateTime('now');
$debugLogManager->logData('today', $today);

$yesterday = new \DateTime('now - 1 day');
$debugLogManager->logData('yesterday', $yesterday);


// selection - begin
$campaignQueryPartsDataArray = array(
	'SELECT' => '*',
	'ORDER_BY' => '`datum` DESC, `k_id` DESC'
);

$kAndNvId = array(
	'sql' => '`k_id`',
	'value' => '`nv_id`',
	'comparison' => 'fieldEqual'
);
$debugLogManager->logData('kAndNvId', $kAndNvId);

$whereCampaignQueryPartsDataArray = array();

$showNvCampaign = false;
if (isset($_POST['kExtendedView']) 
	&& (boolean) \intval($_POST['kExtendedView']) === true
) {
	$hvCampaignView = (boolean) \DataFilterUtils::filterData(
		$_POST['kCampaignSettings']['hvCampaignView'],
		\DataFilterUtils::$validateFilterDataArray['number_int']
	);
	$debugLogManager->logData('hvCampaignView', $hvCampaignView);
	
	if ($hvCampaignView === true) {
		$whereCampaignQueryPartsDataArray['kAndNvId'] = $kAndNvId;
	} else {
		$showNvCampaign = (boolean) \DataFilterUtils::filterData(
			$_POST['kCampaignSettings']['showNvCampaign'],
			\DataFilterUtils::$validateFilterDataArray['number_int']
		);
	}
} else {
	$whereCampaignQueryPartsDataArray['kAndNvId'] = $kAndNvId;
}
$debugLogManager->logData('showNvCampaign', $showNvCampaign);

$searchFilter = false;


// search - begin
if (\strlen($_POST['suchwort']) > 0) {
	$searchValue = \DataFilterUtils::filterData(
		$_POST['suchwort'],
		\DataFilterUtils::$validateFilterDataArray['special_chars']
	);

	$suchbegriff = '('
		. '`k_name` LIKE "%' . $searchValue . '%"'
		. ' OR `crm_order_id` = "' . $searchValue . '"' // TODO: remove
		. ' OR `agentur` LIKE "%' . $searchValue . '%"' // TODO: sp�ter nur noch �ber agentur id suchen
	;
	$suchbegriff .= ')';

	$whereCampaignQueryPartsDataArray['suchwort'] = array(
		'sql' => $suchbegriff,
		'value' => '',
		'comparison' => ''
	);

	$searchFilter = true;
}

if (\strlen($_POST['beginn']) > 0) {
	$_SESSION['in'] = $_POST['beginn'];
	$beginDate = new \DateTime($_SESSION['in']);

	$whereCampaignQueryPartsDataArray['dateBegin'] = array(
		'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
		'value' => $beginDate->format('Y-m-d'),
		'comparison' => '>='
	);

	$searchFilter = true;
}

if (\strlen($_POST['ende']) > 0) {
	$_SESSION['out'] = $_POST['ende'];
	$endDate = new \DateTime($_SESSION['out']);

	$whereCampaignQueryPartsDataArray['dateEnd'] = array(
		'sql' => 'DATE_FORMAT(`datum`, \'%Y-%m-%d\')',
		'value' => $endDate->format('Y-m-d'),
		'comparison' => '<='
	);

	$searchFilter = true;
}

if (isset($_GET['skid'])) {
	$whereCampaignQueryPartsDataArray['skid'] = array(
		'sql' => '`k_id`',
		'value' => \intval($_GET['skid']),
		'comparison' => 'integerEqual'
	);

	$searchFilter = true;
}

$useExtendedSearch = false;


// TODO: remove
$kOldSelektion = '';
if (isset($_POST['kOldSelektion']) 
	&& \strlen($_POST['kOldSelektion']) > 0
) {
	$useExtendedSearch = true;

	$kOldSelektion = ' AND `zielgruppe` LIKE BINARY "%' 
		. \DataFilterUtils::filterData(
			$_POST['kOldSelektion'],
			\DataFilterUtils::$validateFilterDataArray['full_special_chars']
		) . '%"'
	;
}
$debugLogManager->logData('kOldSelektion', $kOldSelektion);

$kAgentur = '';
if (isset($_POST['kAgentur']) 
	&& \intval($_POST['kAgentur']) > 0
) {
	$useExtendedSearch = true;
	$kAgentur = ' AND `agentur_id` = ' . \intval($_POST['kAgentur']);
}
$debugLogManager->logData('kAgentur', $kAgentur);

$kAnsprechpartner = '';
if (isset($_POST['kAnsprechpartner']) 
	&& \intval($_POST['kAnsprechpartner']) > 0
) {
	$useExtendedSearch = true;

	$kAnsprechpartner = ' AND `ap_id` = ' . \intval($_POST['kAnsprechpartner']);
}
$debugLogManager->logData('kAnsprechpartner', $kAnsprechpartner);

$kEditors = '';
if (isset($_POST['kEditors']) 
	&& \strlen($_POST['kEditors']) > 0
) {
	$useExtendedSearch = true;

	// TODO: sp�ter evtl. auf vertriebler_id wechsel
	$kEditors = ' AND `bearbeiter` = "' 
		. \DataFilterUtils::filterData(
			$_POST['kEditors'],
			\DataFilterUtils::$validateFilterDataArray['string']
		) . '"'
	;
}
$debugLogManager->logData('kEditors', $kEditors);

$kStatus = '';
if (isset($_POST['kStatus']) 
	&& \strlen($_POST['kStatus']) > 0
) {
	$useExtendedSearch = true;

	$kStatus = ' AND `status` = ' . \intval($_POST['kStatus']);
}
$debugLogManager->logData('kStatus', $kStatus);

$kBillingType = '';
if (isset($_POST['kBillingType']) 
	&& \strlen($_POST['kBillingType']) > 0
) {
	$useExtendedSearch = true;

	$kBillingType = ' AND `abrechnungsart` LIKE BINARY "%' 
		. \DataFilterUtils::filterData(
			$_POST['kBillingType'],
			\DataFilterUtils::$validateFilterDataArray['string']
		) . '%"'
	;
}
$debugLogManager->logData('kBillingType', $kBillingType);

$kDeliverySystem = '';
if (isset($_POST['kDeliverySystem']) 
	&& \strlen($_POST['kDeliverySystem']) > 0
) {
	$useExtendedSearch = true;

	$kDeliverySystem = ' AND `versandsystem` = "' 
		. \DataFilterUtils::filterData(
			$_POST['kDeliverySystem'],
			\DataFilterUtils::$validateFilterDataArray['full_special_chars']
		) . '"'
	;
	
	if (\intval($_POST['kDsdId']) > 0) {
		$kDeliverySystem .= ' AND `dsd_id` = ' . \intval($_POST['kDsdId']);
	}
}
$debugLogManager->logData('kDeliverySystem', $kDeliverySystem);


if ((boolean) $useExtendedSearch === true) {
	$whereCampaignQueryPartsDataArray['useExtendedSearch'] = array(
		'sql' => '(1 = 1 '
				. $kOldSelektion
				. $kAgentur
				. $kAnsprechpartner
				. $kEditors
				. $kStatus 
				. $kBillingType 
				. $kDeliverySystem
			. ')'
		,
		'value' => '',
		'comparison' => ''
	);

	$searchFilter = true;
}
$debugLogManager->logData('searchFilter', $searchFilter);
// search - end


$suchfilter = '';
$sucheaus = false;
if ((boolean) $searchFilter === true) {
	$suchfilter = 'Suchfilter: aktiv'
		. '&nbsp;&nbsp;<a href="#" onclick="loadNew();" style="color:red;font-weight:bold">[Suchfilter l&ouml;schen] <img src="img/icons/remove.png" alt="Suchfilter l&ouml;schen" title="Suchfilter l&ouml;schen" align="top" style="margin: -2px 0 0 6px;" /></a>'
	;
} else {
	$sucheaus = true;
}
$debugLogManager->logData('sucheaus', $sucheaus);
$debugLogManager->logData('suchfilter', $suchfilter);


// new
$getNew = isset($_GET['new']) ? \intval($_GET['new']) : 0;
$todayTab = false;
if ((int) $getNew > 0) {
	$beginPageItem = 0;

	if ($getNew === 1) {
		/**
		 * getCampaignsDataItemsByQueryParts
		 * 
		 * debug
		 */
		$kTodayDataArray = $campaignManager->getCampaignsDataItemsByQueryParts(
			array(
				'SELECT' => '`datum`',
				'WHERE' => array(
					$kAndNvId
				),
				'ORDER_BY' => '`datum` DESC',
				'LIMIT' => $itemsLimit
			),
			\PDO::FETCH_ASSOC
		);
		if (\count($kTodayDataArray) > 0) {
			/**
			 * getTodayTab
			 */
			$todayTab = \getTodayTab(
				$kTodayDataArray,
				$today
			);
		}
		$debugLogManager->logData('kTodayDataArray', $kTodayDataArray);
		unset($kTodayDataArray);

		$sucheaus = true;
	} elseif ($getNew === 2) {
		$whereCampaignQueryPartsDataArray = array(
			'kAndNvId' => $kAndNvId,
		);
	}
}


$campaignQueryPartsDataArray['WHERE'] = $whereCampaignQueryPartsDataArray;


if ($beginPageItem > 0) {
	$campaignQueryPartsDataArray['LIMIT'] = $beginPageItem . ',' . $itemsLimit;
} else {
	$campaignQueryPartsDataArray['LIMIT'] = $itemsLimit;

	$_SESSION['anfang_seite'] = 0;
	$_SESSION['seite_now'] = 1;
	$actualPageItem = 1;
}
// debug
$debugLogManager->logData('campaignQueryPartsDataArray', $campaignQueryPartsDataArray);

$prevCampaignItem = $beginPageItem - $itemsLimit;
$nextCampaignItem = $beginPageItem + $itemsLimit;



/**
 * createTable - Tag
 */
$tableDataArray = \HtmlTableUtils::createTable(array());

/**
 * createTHeadTag - Tag
 */
$tableHeaderDataArray = \HtmlTableUtils::createTHeadTag();

/**
 * createTableRow - Tag
 */
$tableRowDataArray = \HtmlTableUtils::createTableRow(array());


$tableData = $tableHeaderContent = $tableEditHeaderContent = '';
try {
	/**
	 * getCampaignsCountByQueryParts
	 * 
	 * debug
	 */
	$countCampaigns = $campaignManager->getCampaignsCountByQueryParts(
		array(
			'WHERE' => $campaignQueryPartsDataArray['WHERE']
		)
	);
	$debugLogManager->logData('countCampaigns', $countCampaigns);

	$pageTotalCount = \ceil($countCampaigns / $itemsLimit);

	if (!$todayTab 
		&& $getNew === 1 
		&& $countCampaigns > $itemsLimit
	) {
		echo '
            <script type="text/javascript">
                setRequest(\'kampagne/k_main.php?seite_start=' . $nextCampaignItem . '&jahr=' . $year . '&seite_now=2\', \'k\');
            </script>
        ';
		exit;
	}

	if ($countCampaigns > 0) {
		/**
		 * getTabFieldsDataArrayByIdUser
		 * 
		 * debug
		 */
		$userTabFieldsDataArray = $clientManager->getTabFieldsDataArrayByIdUser(\RegistryUtils::get('userEntity')->getBenutzer_id());
		$debugLogManager->logData('userTabFieldsDataArray', $userTabFieldsDataArray);


		/**
		 * createUserAndEditTableContent
		 */
		\createUserAndEditTableContent(
			$userTabFieldsDataArray,
			$tableHeaderContent,
			$tableEditHeaderContent
		);
		
		$getCustomerDataItem = \array_search('ap', $userTabFieldsDataArray) ? true : false;
		$debugLogManager->logData('getCustomerDataItem', $getCustomerDataItem);


		if ((boolean) $searchFilter === true) {
			$tableData .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						array(
							'colspan' => \count($userTabFieldsDataArray) + 3
						),
						$suchfilter
					)
				. $tableRowDataArray['end']
			;
		}


		/**
		 * getCampaignDataItemById
		 * 
		 * debug
		 */
		$syncLastTimeCampaignEntity = $campaignManager->getCampaignDataItemById(1);
		if (!($syncLastTimeCampaignEntity instanceof \CampaignEntity)) {
			throw new \DomainException('no CampaignEntity', 1424769667);
		}
		$debugLogManager->logData('syncLastTime', $syncLastTimeCampaignEntity);


		/**
		 * getCampaignsAndCustomerDataItemsByQueryParts
		 * 
		 * debug
		 */
		$campaignsDataArray = $campaignManager->getCampaignsAndCustomerDataItemsByQueryParts($campaignQueryPartsDataArray);
		#$debugLogManager->logData('campaignsDataArray', $campaignsDataArray);
		if (\count($campaignsDataArray) > 0) {
			$timestamp = '';

			// debug
			$debugLogManager->beginGroup('campaignView');

			$c = 0;
			foreach ($campaignsDataArray as $hvCampaignEntity) {
				/* @var $hvCampaignEntity \CampaignWidthCustomerAndContactPersonEntity */
				
				$bgColor = ($c % 2 ? '#E4E4E4' : '#FFFFFF');

				// debug
				$debugLogManager->beginGroup($hvCampaignEntity->getK_id());
				
				$hvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
				
				$tmpHvCampaigEntity = new \CampaignEntity();
				if (!($tmpHvCampaigEntity instanceof \CampaignEntity)) {
					throw new \DomainException('no CampaignEntity', 1427371283);
				}

				$tableDataNvList = '';
				if ((boolean) $showNvCampaign === true 
					&& isset($whereCampaignQueryPartsDataArray['useExtendedSearch'])
				) {
					/**
					 * getAllCampaignsDataItemsByKid
					 */
					$nvCampaignsDataArray = $campaignManager->getAllCampaignsDataItemsByKid(
						$hvCampaignEntity->getK_id(),
						array(
							'useExtendedSearch' => $whereCampaignQueryPartsDataArray['useExtendedSearch']
						),
						$getCustomerDataItem
					);
				} else {
					/**
					 * getAllCampaignsDataItemsByKid
					 */
					$nvCampaignsDataArray = $campaignManager->getAllCampaignsDataItemsByKid(
						$hvCampaignEntity->getK_id(),
						array(),
						$getCustomerDataItem
					);
				}
				#$debugLogManager->logData('nvCampaignsDataArray', $nvCampaignsDataArray);
				
				$nvCampaignsCount = \count($nvCampaignsDataArray) - 1;
				$debugLogManager->logData('nvCampaignsCount', $nvCampaignsCount);
				if ($nvCampaignsCount > 0) {
					$allCampaignsDataArray = array(
						'versendet' => 0,
						'openings' => 0,
						'openings_all' => 0,
						'klicks' => 0,
						'klicks_all' => 0,
						'leads' => 0,
						'leads_u' => 0,
						'abmelder' => 0,
						'hbounces' => 0,
						'sbounces' => 0,
						'unit_price' => 0,
						'totalPrice' => 0,
						'kosten' => 0
					);

					foreach ($nvCampaignsDataArray as $nvCampaignEntity) {
						/* @var $nvCampaignEntity \CampaignEntity */

						// debug
						$debugLogManager->beginGroup('nvCampaignView');

						if (\strlen($nvCampaignEntity->getAbrechnungsart()) > 0) {
							$nvCampaignEntity->setAbrechnungsart(\CampaignAndCustomerUtils::$campaignBillingsTypeDataArray[$nvCampaignEntity->getAbrechnungsart()]);
						}

						// debug
						$debugLogManager->logData('nvCampaignEntity', $nvCampaignEntity);

						// begin - campaignView
						require(DIR_Module_Campaigns . 'Templates/Campaign/viewData_nvList.php');
						#require(DIR_Utils . 'CampaignFieldsUtils.php');
						$tableDataNvList .= $nvContentList;

						$allCampaignsDataArray['versendet'] += $nvCampaignEntity->getVersendet();
						$allCampaignsDataArray['openings'] += $nvCampaignEntity->getOpenings();
						$allCampaignsDataArray['openings_all'] += $nvCampaignEntity->getOpenings_all();
						$allCampaignsDataArray['klicks'] += $nvCampaignEntity->getKlicks();
						$allCampaignsDataArray['klicks_all'] += $nvCampaignEntity->getKlicks_all();
						$allCampaignsDataArray['leads'] += $nvCampaignEntity->getLeads();
						$allCampaignsDataArray['leads_u'] += $nvCampaignEntity->getLeads_u();
						$allCampaignsDataArray['abmelder'] += $nvCampaignEntity->getAbmelder();
						$allCampaignsDataArray['hbounces'] += $nvCampaignEntity->getHbounces();
						$allCampaignsDataArray['sbounces'] += $nvCampaignEntity->getSbounces();
						$allCampaignsDataArray['unit_price'] += $nvCampaignEntity->getUnit_price();
						$allCampaignsDataArray['totalPrice'] += \FormatUtils::totalPriceCalculationByBillingType($nvCampaignEntity);
					    $allCampaignsDataArray['kosten'] += $nvKosten;
						// debug
						$debugLogManager->endGroup();
					}
					
					$tmpHvCampaigEntity->setVersendet($allCampaignsDataArray['versendet']);
					$tmpHvCampaigEntity->setOpenings($allCampaignsDataArray['openings']);
					$tmpHvCampaigEntity->setOpenings_all($allCampaignsDataArray['openings_all']);
					$tmpHvCampaigEntity->setKlicks($allCampaignsDataArray['klicks']);
					$tmpHvCampaigEntity->setKlicks_all($allCampaignsDataArray['klicks_all']);
					$tmpHvCampaigEntity->setLeads($allCampaignsDataArray['leads']);
					$tmpHvCampaigEntity->setLeads_u($allCampaignsDataArray['leads_u']);
					$tmpHvCampaigEntity->setAbmelder($allCampaignsDataArray['abmelder']);
					$tmpHvCampaigEntity->setSbounces($allCampaignsDataArray['sbounces']);
					$tmpHvCampaigEntity->setHbounces($allCampaignsDataArray['hbounces']);
					$tmpHvCampaigEntity->setUnit_price($allCampaignsDataArray['unit_price']);
					$tmpHvCampaigEntity->setPreis($allCampaignsDataArray['totalPrice']);
					$tmpHvCampaigEntity->setKosten($allCampaignsDataArray['kosten']);
					
					unset($allCampaignsDataArray);
				}
				unset($nvCampaignsDataArray);

				// debug
				$debugLogManager->logData('hvCampaignEntity', $hvCampaignEntity);
				$debugLogManager->logData('tmpHvCampaigEntity', $tmpHvCampaigEntity);
				
				
				$timestamp2 = $timestamp;
				$timestamp = \strtotime($hvCampaignEntity->getDatum()->format('Y-m-d'));
				$debugLogManager->logData('timestamp2', $timestamp2);
				$debugLogManager->logData('timestamp', $timestamp);

				$dat_heute = $todayAnchor = '';
				$isToday = \DateUtils::isDateToday($hvCampaignEntity->getDatum());
				if ($isToday === true) {
					$dateStyle = 'border-bottom: 2px solid #E63C3C; font-weight: bold;border-right: 0; border-left: 0; vertical-align: bottom;';
					$bgImageUrl = 'img/bgkred.png';
					$dat_heute = '[HEUTE]&nbsp;&nbsp;&nbsp;';
					$todayAnchor = '<a href="#" id="heute"></a>';
				} else {
// 					$dateStyle = 'color: #40547A; border-bottom: 2px solid #40547A; font-weight: normal; border-right: 0; border-left: 0; vertical-align: bottom;';
// 					$bgImageUrl = 'img/bgk.png';
					$dateStyle = 'color: #8e8901; border-bottom: 2px solid #8e8901; font-weight: normal; border-right: 0; border-left: 0; vertical-align: bottom;';
					$bgImageUrl = 'img/treaction-gruen.png';
				}
				$debugLogManager->logData('isToday', $isToday);

				if ($timestamp !== $timestamp2) {
					if ($isToday !== true) {
						if ($yesterday->format('dmY') === $hvCampaignEntity->getDatum()->format('dmY')) {
							$todayAnchor = '<a href="#" id="heute"></a>';
						}
					}

					$tableData .= $tableRowDataArray['begin']
						. \HtmlTableUtils::createTableCellWidthContent(
							array(
								'colspan' => 2,
								'style' => 'height: 35px; vertical-align: bottom; background-color: #F2F2F2; font-size: 11px; padding: 0px; ' . $dateStyle
							),
							$todayAnchor
								. '<div style="padding-top: 5px; height: 15px; background-image:url(' . $bgImageUrl . '); color: #fff; cursor: default;">'
									. '<span style="padding-left: 5px; padding-top: 5px;">'
										. $dat_heute . \DateUtils::getWeekDay(
											$hvCampaignEntity->getDatum(),
											true
										) . ', ' . $hvCampaignEntity->getDatum()->format('d.m.Y')
									. '</span>'
								. '</div>'
						)
					;

					// header
					foreach ($userTabFieldsDataArray as $userField) {
						foreach (\CampaignAndCustomerUtils::$allTabFieldsDataArray as $tabField => $tabArray) {
							if ($userField == $tabField) {
								$tableData .= \HtmlTableUtils::createTableCellWidthContent(
									array(
										'style' => $dateStyle . ' border-right: 1px dotted #fff;'
									),
									''
								);
							}
						}
					}

					$tableData .= \HtmlTableUtils::createTableCellWidthContent(
						array(
							'style' => $dateStyle . ' border-right: 1px dotted #fff;'
						),
						''
					) . $tableRowDataArray['end'];
				}

				// begin - campaignView
				require(DIR_Module_Campaigns . 'Templates/Campaign/viewData_hvList.php');
				$tableData .= $hvContentList;


				// begin - nvCampaignView
				if ($nvCampaignsCount > 0) {
					// show nvCampaignView
					$tableData .= $tableDataNvList;
				}

				$c++;

				// debug
				$debugLogManager->endGroup();
			}

			// debug
			$debugLogManager->endGroup();
		} else {
			// viewData_noData
			require_once(DIR_Module_Campaigns . 'Templates/Campaign/viewData_noData.php');
			$tableData .= $tableNoContent;
		}

		unset($userTabFieldsDataArray, $campaignsDataArray);
	} else {
		if (\strlen($suchfilter) > 0) {
			$tableData .=
				$tableRowDataArray['begin']
					. \HtmlTableUtils::createTableCellWidthContent(
						array(),
						$suchfilter
					)
				. $tableRowDataArray['end']
			;
		}

		// viewData_noMailingsFound
		require_once(DIR_Module_Campaigns . 'Templates/Campaign/viewData_noMailingsFound.php');
		$tableData .= $tableNoContent;
	}
} catch (\Exception $e) {
	require(\DIR_configs . 'exceptions.php');

	die($exceptionMessage);
}


$_SESSION['data'] = '
    <p>Die Spalten, die nicht ausgedruckt werden sollen, k&ouml;nnen mit einem Klick auf <img src="../img/Tango/16/actions/dialog-close.png" /> entfernt werden.</p>
    <p><button onclick="PrintContent();">Drucken</button> <button onclick="location.reload();">Zur&uuml;cksetzen</button></p>
    <div id="tblData">
        <table cellspacing="0" cellpadding="0" style="border-collapse: collapse;">' 
			. $tableEditHeaderContent
			. $tableHeaderContent
			. \utf8_decode($tableData) . '
        </table>
    </div>
';

// debug
#$debugLogManager->logData('SESSION', $_SESSION);
#$debugLogManager->logData('emsSuite', \RegistryUtils::getAll());
?>
<div id="fakeContainer">
    <table class="scrollTable" id="scrollTable" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5" width="100%">
		<?php
		echo $tableHeaderContent
			. $tableData
		;
		?>
    </table>
</div>
<div style="background-color:#8e8901;color:#FFFFFF;font-size:11px;height:23px;width:100%;">
    <div style="padding-top:5px;float:left;">
		<?php
		if ($pageTotalCount > 1 
			&& $actualPageItem > 1
		) {
			echo '
                <a href="#" onclick="setRequest(buildCampaignRequestUrl(' . $prevCampaignItem . ', ' . $year . ', ' . ($actualPageItem - 1) . '), \'k\');">
                    <img src="img/pfeil_li_white.png" />
                </a>
            ';
		}

		if ($countCampaigns > 0) {
			echo '&nbsp;&nbsp;Seite ' . $actualPageItem . ' von ' . $pageTotalCount . '&nbsp;&nbsp;';
		}

		if ($pageTotalCount != $actualPageItem 
			&& $pageTotalCount > 1
		) {
			echo '
                <a href="#" onclick="setRequest(buildCampaignRequestUrl(' . $nextCampaignItem . ', ' . $year . ', ' . ($actualPageItem + 1) . '), \'k\');">
                    <img src="img/pfeil_re_white.png" />
                </a>
            ';
		}
		?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
if ($countCampaigns > 0) {
	echo \FormatUtils::numberFormat($countCampaigns)
		. ' Mailings <a href="#" id="show_info" style="color:#9900FF">&nbsp;</a>'
	;
} else {
	echo '0 Mailings&nbsp;&nbsp;&nbsp;';
}
?>
    </div>
    <div style="float:left;margin-top:1px;">
        <img src='img/treaction_gruen_seperator.png' />
    </div>
    <div style="margin-left:5px;padding-top:5px;float:left;">
<?php
if ($countCampaigns > 0) {
	echo ' Stand: ' . $syncLastTimeCampaignEntity->getGestartet()->format('H:i') . ' Uhr';
}

unset($_SESSION['campaignView']);
?>
    </div>
</div>
<script type="text/javascript">
	function buildCampaignRequestUrl(beginPage, year, actPrevNextPage) {
		return 'kampagne/k_main.php?' 
			+ 'seite_start=' + parseInt(beginPage) 
			+ '&jahr=' + parseInt(year) 
			+ '&seite_now=' + parseInt(actPrevNextPage)
		;
	}
	
	function Resize() {
        setRequest(buildCampaignRequestUrl('<?php echo $beginPageItem; ?>', '<?php echo $year; ?>', '<?php echo $actualPageItem; ?>'), 'k');
    }
	
	
	// set parameter for setRequest()
	YAHOO.util.Dom.get('page').value = buildCampaignRequestUrl('<?php echo $beginPageItem; ?>', '<?php echo $year; ?>', '<?php echo $actualPageItem; ?>');
	YAHOO.util.Dom.get('page_sub').value = 'k';
	
    var wi = document.documentElement.clientWidth;
    var hi = document.documentElement.clientHeight;

    if (uBrowser === 'Firefox') {
        hi = hi-142;
        wi = wi-205;
    } else {
        hi = hi-144;
        wi = wi-205;
    }

    YAHOO.util.Dom.get('fakeContainer').style.width = wi + 'px';
    YAHOO.util.Dom.get('fakeContainer').style.height = hi + 'px';

    window.onresize = Resize;

    var mySt = new superTable('scrollTable', {
        cssSkin: 'sDefault',
        fixedCols: 0,
        headerRows: 1
    });

    location.href = '<?php echo $campaignAnchor; ?>';
    location.href = '#topBody';
</script>