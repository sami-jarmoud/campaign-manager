<?php
/* @var $debugLogManager \debugLogManager */
/* @var $campaignManager \CampaignManager */
/* @var $clientManager \ClientManager */
/* @var $swiftMailerWebservice \SwiftMailerWebservice */



$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
if ($customerId === 0) {
	throw new \InvalidArgumentException('no customerId!');
}


$campaignsCounts = $campaignManager->getCampaignsCountByQueryParts(
	array(
		'SELECT' => 'k_id, nv_id, k_name',
		'WHERE' => array(
			'agenturId' => array(
				'sql' => '`agentur_id`',
				'value' => $customerId,
				'comparison' => 'integerEqual'
			)
		)
	)
);
$debugLogManager->logData('campaignsCounts', $campaignsCounts);

/**
 * getContactPersonsCountByQueryParts
 * 
 * debug
 */
$contactPersonCounts = $campaignManager->getContactPersonsCountByQueryParts(
	array(
		'WHERE' => array(
			'kundeId' => array(
				'sql' => '`kunde_id`',
				'value' => $customerId,
				'comparison' => 'integerEqual'
			)
		)
	)
);
$debugLogManager->logData('contactPersonCounts', $contactPersonCounts);

if ((int) $campaignsCounts === 0 
	&& (int) $contactPersonCounts === 0
) {
	/**
	 * deleteCustomerDataById
	 * 
	 * debug
	 */
	$deleteCustomerResult = $campaignManager->deleteCustomerDataById($customerId);
	$debugLogManager->logData('deleteCustomerResult', $deleteCustomerResult);
	
	if ((boolean) $deleteCustomerResult === true) {
		/**
		 * addEmsActionLogItem
		 * 
		 * create EmsActionLogEntity
		 * 
		 * debug
		 */
		$emsActionLogEntity = new \EmsActionLogEntity();
		$emsActionLogEntity->setClient_id($_SESSION['mID']);
		$emsActionLogEntity->setUser_id($_SESSION['benutzer_id']);
		$emsActionLogEntity->setAction_id(14);
		$emsActionLogEntity->setRecord_id($customerId);
		$emsActionLogEntity->setRecord_table($campaignManager->getCustomerTable());
		$debugLogManager->logData('emsActionLogEntity', $emsActionLogEntity);
		
		// addEmsActionLogItem
		$emsActionLogResult = $clientManager->addEmsActionLogItem($emsActionLogEntity);
		$debugLogManager->logData('emsActionLogResult', $emsActionLogResult);
		unset($emsActionLogEntity);
	}
} else {
	$errorMessage = '';
	if ($contactPersonCounts > 0) {
		$errorMessage .= 'Kunde kann nicht gel�scht werden weil es noch Ansprechpartner (' . $contactPersonCounts . ') hat!' . \chr(13);
	}
	
	if ($campaignsCounts > 0) {
		$errorMessage .= 'Kunde kann nicht gel�scht werden weil es noch Kampagnen (' . $campaignsCounts . ') besitzt!' . \chr(13);
	}
	
	/**
	 * sendUserInfoEmail
	 * 
	 * debug
	 */
	$failedRecipients = array();
	$mailerSendResults = \sendUserInfoEmail(
		$swiftMailerWebservice,
		$debugLogManager,
		\utf8_encode($errorMessage),
		\utf8_encode('Ems Kunden l�schung!'),
		$failedRecipients
	);
	$debugLogManager->logData('mailerSendResults', $mailerSendResults);
	$debugLogManager->logData('failedRecipients', $failedRecipients);
}