<?php
/* @var $debugLogManager debugLogManager */
/* @var $campaignManager CampaignManager */



if (\count($_POST['customer']) === 0) {
	throw new \InvalidArgumentException('no customerData found!');
} else {
	$customerId = isset($_POST['customer']['kunde_id']) ? \intval($_POST['customer']['kunde_id']) : 0;
	if ($customerId === 0) {
		throw new \InvalidArgumentException('no customerId!');
	}
}


/**
 * getCustomerDataItemById (�berpr�fe ob kunde existiert)
 * 
 * debug
 */
$customerEntity = $campaignManager->getCustomerDataItemById($customerId);
if (!($customerEntity instanceof \CustomerEntity)) {
	throw new \DomainException('invalid CustomerEntity!');
}
$debugLogManager->logData('customerEntity', $customerEntity);


/**
 * createCustomerDataArray
 * 
 * debug
 */
$customerDataArray = \createCustomerDataArray($_POST['customer']);
$debugLogManager->logData('customerDataArray', $customerDataArray);

/**
 * updateCustomerDataById
 * 
 * debug
 */
$updateCustomerResult = $campaignManager->updateCustomerDataById(
	$customerId,
	$customerDataArray
);
$debugLogManager->logData('updateCustomerResult', $updateCustomerResult);

$emsActionLogEntity = new \EmsActionLogEntity();
$emsActionLogEntity->setClient_id($_SESSION['mID']);
$emsActionLogEntity->setUser_id($_SESSION['benutzer_id']);
$emsActionLogEntity->setAction_id(23);
$emsActionLogEntity->setRecord_id($customerId);
$emsActionLogEntity->setRecord_table($campaignManager->getCustomerTable());
$emsActionLogEntity->setLog_data(
	array(
		'customerEntity' => $customerEntity,
	)
);
$debugLogManager->logData('emsActionLogEntity', $emsActionLogEntity);

// addEmsActionLogItem
$emsActionLogResult = $clientManager->addEmsActionLogItem($emsActionLogEntity);
$debugLogManager->logData('emsActionLogResult', $emsActionLogResult);
unset($emsActionLogEntity);