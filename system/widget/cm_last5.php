<?php
$tr = '';
$sql = mysql_query("SELECT 
					IF(LENGTH(`k_name`) > 35, CONCAT(LEFT(`k_name`, 32), '...'), `k_name`) AS kname, k_id, versandsystem, datum, agentur, gebucht, k_name,vorgabe_o,vorgabe_k,mailtyp
					FROM $kampagne_db WHERE k_id=nv_id AND status = 5 OR (status >= 20 AND status < 32) AND k_id>1 ORDER BY datum DESC LIMIT 0,5",$verbindung);

$j = 1;
if (mysql_num_rows($sql) > 0) {

	
	
	while ($kz = mysql_fetch_array($sql)) {
		
		
		if ($j==1 || $j==3) {
			$bg_color = "FFFFFF";
			$j=1;
		} elseif ($j==2) {
			$bg_color = "E4E4E4";
		} 

		$kid = $kz['k_id'];
		$k_name = $kz['kname'];
		$k_name_full = $kz['k_name'];
		$asp = $kz['versandsystem'];
		if ($asp == 'BM') {
			$datum = util::bmTime($kz['datum']);
		} else {
			$datum = $kz['datum'];
		}
		
		$datum = util::datum_de($datum,'');
		$agentur = $kz['agentur'];
		$gebucht = $kz['gebucht'];
		$vorgabe_o = $kz['vorgabe_o'];
		$vorgabe_k = $kz['vorgabe_k'];
		$mailtyp = $kz['mailtyp'];
		
		
		$sql2 = mysql_query("SELECT SUM(versendet) as v, SUM(openings_all) as o, SUM(klicks_all) as k, SUM(sbounces) as sb, SUM(hbounces) as hb FROM $db_name.$kampagne_db WHERE nv_id='$kid'",$verbindung);
		$ksz = mysql_fetch_array($sql2);
		
		$versendet = $ksz['v'];
		$o = $ksz['o'];
		$k = $ksz['k'];
		
		$o_quote = util::getQuote($gebucht,$o);
		$k_quote = util::getQuote($gebucht,$k);
		
		if ($mailtyp == 't' && ( $k_quote >= $vorgabe_k)) {
			$o_clr = "<span style='color:green'>".util::getQuote($gebucht,$o)."</span>";
		} else {
			$o_clr = util::getQuoteColor(util::getQuote($gebucht,$o),$vorgabe_o);
		}
		
		$data = "<chart useRoundEdges='1' showLabels='0' showYAxisValues='0' canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='0' bgColor='".$bg_color."' chartLeftMargin='0' chartRightMargin='0' chartTopMargin='0' chartBottomMargin='0' paletteColors='267CE6,9900FF'>";		
		$data .= "<set value='".$o_quote."' label='�' />";
		$data .= "<set value='".$k_quote."' label='K' />";
		$data .= "</chart>";
		$chart = createChart::renderChart('cm_last5'.$kid,$data,'Column2D','35','35');
	
		$tr .= "<tr style='background-color:#".$bg_color."'>";
		$tr .= "<td style='text-align:center'>".$chart."</td>";
		$tr .= "<td style='white-space:nowrap;'><a href='#' onClick='setRequest(\"kampagne/k_main.php?skid=".$kid."\",\"k\");' title='".$k_name_full."'>".util::cutString($k_name,25)."</a><br/><span style='color:#9900FF'>".util::cutString($agentur,25)."</span><br /><span style='color:#666666;font-size:9px;'>".$datum."</span></td>";
		$tr .= "<td align='right' class='medium'>".util::zahl_format($gebucht)."</td>";
		$tr .= "<td align='right' class='medium'>".util::zahl_format($versendet)."</td>";
		$tr .= "<td align='right' class='medium'><b>".$o_clr."</b></td>";
		$tr .= "<td align='right' class='medium'><b>".util::getQuoteColor(util::getQuote($gebucht,$k),$vorgabe_k)."</b></td>";
		$tr .= "</td>";
		$tr .= "</tr>";
		
		
		$j++;
	}
	
	
	
} else {
	$tr = "<tr><td colspan='5'><span style='color:#333333'>Keine Daten vorhanden</span></td></tr>";
}
?>
<table cellpadding="0" cellspacing="0" class="table_widget">
	<tr class="trHeader">
		<td></td>
    	<td width="150">Kampagne</td>
        <td align='right'>Gebucht</td>
        <td align='right'>Versendet</td>
        <td align='right'>&Ouml;-Rate</td>
        <td align='right'>K-Rate</td>
    </tr>
    <?php print $tr; ?>
</table>