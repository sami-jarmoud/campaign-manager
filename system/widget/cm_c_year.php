<?php 
if (isset($_GET['jahr'])) {
	$jahr = $_GET['jahr'];
} else {
	$jahr = date("Y");
}

$k_sql = mysql_query("SELECT MONTH(datum) as Monat, COUNT(*) as Anzahl
						  FROM $kampagne_db 
						  WHERE YEAR(datum) = '$jahr'
						  AND k_id = nv_id
						  AND k_id > 1
					      GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);

if (mysql_num_rows($k_sql) > 0) {		  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";		  
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$monat = util::getMonatName($kz['Monat']);
		$data .= "<set value='".$kz['Anzahl']."' label='".$monat."' color='267CE6' />";
		
	}
	
	$data .= "</chart>";
	$chart_c_year = createChart::renderChart('idkOV',$data,'Line','380','150');
} else {
	$chart_c_year = "<span style='color:#333333'>Keine Daten vorhanden</span>";
}

?>
<table cellpadding="0" cellspacing="0" class="table_widget">
	<tr><td><?php print $chart_c_year;?></td></tr>
</table>
