<?php
$sql = mysql_query("SELECT 
						IF(LENGTH(`IMPORT_DATEI`) > 35, CONCAT(LEFT(`IMPORT_DATEI`, 32), '...'), `IMPORT_DATEI`) AS IMPORTDATEI,
						i.IMPORT_DATEI,
						i.IMPORT_NR,
						i.IMPORT_DATUM,
						p.Pkz,
						i.GELIEFERT_DATUM
					FROM $import_datei_db as i, $partner_db as p
					WHERE IMPORT_NR=IMPORT_NR2
					AND i.PARTNER = p.PID
					ORDER BY i.GELIEFERT_DATUM DESC
					LIMIT 0,5",$verbindung);

$j = 1;
$tr_dm_last5 = '';
if (mysql_num_rows($sql) > 0) {
	while ($kz = mysql_fetch_array($sql)) {
		
		
		if ($j==1 || $j==3) {
			$bg_color = "#FFFFFF";
			$j=1;
		} elseif ($j==2) {
			$bg_color = "#E4E4E4";
		} 

		$id = $kz['IMPORT_NR'];
		$datei_name = $kz['IMPORTDATEI'];
		$datei_name_full = $kz['IMPORT_DATEI'];

		$datum = $kz['GELIEFERT_DATUM'];
		$datum = util::datum_de($datum,'date');
		$partner = $kz['Pkz'];		
		
		$sql2 = mysql_query("SELECT SUM(BRUTTO) as b, SUM(NETTO) as n FROM $import_datei_db WHERE IMPORT_NR2='$id'",$verbindung);
		$ksz = mysql_fetch_array($sql2);
		
		$b = $ksz['b'];
		$n = $ksz['n'];
		$f = $b-$n;
		
		$n_quote = util::getQuote($b,$n);
		
		$data = "<chart showLabels='0' showYAxisValues='0' canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='0' bgColor='".$bg_color."' chartLeftMargin='0' chartRightMargin='0' chartTopMargin='0' chartBottomMargin='0' paletteColors='267CE6,9900FF'>";		
		$data .= "<set value='".$f."' label='F' />";
		$data .= "<set value='".$n."' label='N' />";
		$data .= "</chart>";
		$chart = createChart::renderChart('dm_last'.$kid,$data,'Pie2D','35','35');
		
		$tr_dm_last5 .= "<tr style='background-color:".$bg_color."'>";
		$tr_dm_last5 .= "<td align='center'>".$chart."</td>";
		$tr_dm_last5 .= "<td style='white-space:nowrap;'><b title='".$datei_name_full."'>".$datei_name."</b><br/><span style='color:#9900FF'>".$partner."</span><br /><span style='color:#666666;font-size:9px;'>".$datum."</span></td>";
		$tr_dm_last5 .= "<td align='right' class='medium'>".util::zahl_format($b)."</td>";
		$tr_dm_last5 .= "<td align='right' class='medium'>".util::zahl_format($n)."</td>";
		$tr_dm_last5 .= "<td align='right' class='medium'><b>".util::getDataQuoteColor(util::getQuote($b,$n));"</b></td>";
		$tr_dm_last5 .= "</td>";
		$tr_dm_last5 .= "</tr>";
		
		$j++;
	}
} else {
	$tr_dm_last5 = "<tr><td colspan='5'><span style='color:#333333'>Keine Daten vorhanden</span></td></tr>";
}
?>
<table cellpadding="0" cellspacing="0" class="table_widget">
	<tr class="trHeader">
		<td></td>
    	<td width="150">Lieferung</td>
        <td align='right'>Brutto</td>
        <td align='right'>Netto</td>
        <td align='right'>Quote</td>
    </tr>
    <?php print $tr_dm_last5; ?>
</table>