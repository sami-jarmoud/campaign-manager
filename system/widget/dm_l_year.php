<?php
if (isset($_GET['jahr'])) {
	$jahr = $_GET['jahr'];
} else {
	$jahr = date("Y");
}

$k_sql = mysql_query("SELECT MONTH(GELIEFERT_DATUM) as Monat
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
					      GROUP BY Monat
						  ORDER BY Monat ASC",$verbindung);

if (mysql_num_rows($k_sql) > 0) {		
	  
	$data = "<chart canvasBorderColor='999999' showValues='0' showBorder='0' borderColor='CCCCCC' borderThickness='1' bgColor='FFFFFF'>";
	$cat = "<categories>";
	$data2 = "<dataset seriesName='Brutto' color='267CE6'>";
	$data3 = "<dataset seriesName='Netto' color='9900FF'>";
	
	 
	while ($kz = mysql_fetch_array($k_sql)) {
	
		$monat = util::getMonatName($kz['Monat']);
		
		$cat .= "<category label='".$monat."' />";
		
		$nv_sql = mysql_query("SELECT SUM(BRUTTO) as B, SUM(NETTO) as N
						  FROM $import_datei_db 
						  WHERE YEAR(GELIEFERT_DATUM) = '$jahr'
					      AND MONTH(GELIEFERT_DATUM) = '".$kz['Monat']."'
						  ",$verbindung);
		$nvz = mysql_fetch_array($nv_sql);
		$data2 .= "<set value='".$nvz['B']."' color='267CE6' />";
		$data3 .= "<set value='".$nvz['N']."' color='9900FF' />";
	}
	
	$data .= $cat."</categories>";
	$data .= $data2."</dataset>";
	$data .= $data3."</dataset>";
	$data .= "</chart>";
	$chart_l_year = createChart::renderChart('idkOVv',$data,'MSLine','380','150');

} else {
	$chart_l_year = "<span style='color:#333333'>Keine Daten vorhanden</span>";
}	
?>
<table cellpadding="0" cellspacing="0" class="table_widget">
	<tr><td><?php print $chart_l_year;?></td></tr>
</table>