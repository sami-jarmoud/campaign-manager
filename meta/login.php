<?php
session_start();
include("db_pep.inc.php");
if (isset($_GET['auto'])) {
    $getAuto = $_GET['auto'];
} else {
    $getAuto = '';
}

function getmID($m_abkz) {
    include('db_pep.inc.php');
    
    $qry = mysql_query(
        'SELECT `id`, `crm_hash`, `intone_group`, `broadmail_import`, `client_base_path`, `has_multiple_distributors` FROM `mandant` WHERE `abkz` = "' . $m_abkz . '"',
        $verbindung_pep
    );
    $row = mysql_fetch_array($qry);
    
    $_SESSION['mID'] = $row['id'];
    $_SESSION['crmMid'] = $row['crm_hash'];
    $_SESSION['intoneGroup'] = (int) $row['intone_group'];
    $_SESSION['broadmailImport'] = (int) $row['broadmail_import'];
	$_SESSION['client_base_path'] = $row['client_base_path'];
	$_SESSION['hasMultipleDistributors'] = (int) $row['has_multiple_distributors'];
	
	if ((int) $row['has_multiple_distributors'] === 1) {
		getParentClients(
			$row['id'],
			$verbindung_pep
		);
	} else {
		$_SESSION['parentClients'] = $_SESSION['underClientIds'] = array();
	}
}

function getParentClients($clientId, $verbindung_pep) {
	require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'system/library/Entity/ClientEntity.php');
	
	$parentClients = $underClientIds = array();
    
    $qry = \mysql_query(
        'SELECT * FROM `mandant` WHERE `parent_id` = ' . \intval($clientId),
        $verbindung_pep
    );
	if ((\mysql_num_rows($qry)) > 0) {
		$underClientIds[] = $clientId;
		while ($row = mysql_fetch_object($qry, 'ClientEntity')) {
			$parentClients[] = $row;
			$underClientIds[] = $row->id;
		}
	}
	
	$_SESSION['parentClients'] = $parentClients;
	$_SESSION['underClientIds'] = $underClientIds;
}

function getUserID($username){
    include("db_pep.inc.php");
    $resultID = mysql_query("SELECT benutzer_id FROM benutzer WHERE benutzer_name='$username' AND active=1", $verbindung_pep);
    $row = mysql_fetch_row($resultID);
    $userID = $row[0];    
    return $userID;
}

function saltPassword($password, $salt)
{
     return hash('sha256', $password . $salt);
} 

if ($getAuto == 1) {
    $fehler = "<tr><td><span style='color:#999999;font-size:11px;font-weight:normal'>Sie wurden automatisch ausgeloggt.</span></td></tr>";
}

if (isset($_GET['logout']) && $_GET['logout'] == '1') {
    $mandant = $_SESSION['mandant'];
    $u_name = $_SESSION['username'];
    $benutzer_name = $_SESSION['benutzer_name'];
    #$user_anmelden_sql = mysql_query("UPDATE benutzer SET timestamp = '' WHERE name = '$u_name'", $verbindung_pep);
    mysql_close($verbindung_pep);
	
    //include("db_connect.inc.php");
	include($_SERVER['DOCUMENT_ROOT'] . '/system/db_connect.inc.php');
    mysql_query("DROP TABLE `stats_" . $benutzer_name . "`", $verbindung);
    mysql_close($verbindung);
    session_unset();
    session_destroy();
}

if (isset($_POST['submit'])) {
    include('db_pep.inc.php');
    $username = $_POST['username'];
    $pw = $_POST['pw'];
    $md5pass = \md5($pw);
    $userID = getUserID($username);
    $shapw = saltPassword($md5pass, $userID);
    
    $user_finden = mysql_query("SELECT * FROM benutzer WHERE benutzer_name='$username' AND pw='$pw' AND active=1", $verbindung_pep);
    $user_gefunden = mysql_num_rows($user_finden);
    if($user_gefunden == 1){
        $qry_update = mysql_query(
        "UPDATE benutzer
            SET pw = '$shapw'
            WHERE benutzer_id = '$userID'",
        $verbindung_pep
        );
      } 
      
    $sha_user_finden = mysql_query("SELECT * FROM benutzer WHERE benutzer_name='$username' AND pw='$shapw' AND active=1", $verbindung_pep);
    $sha_user_gefunden = mysql_num_rows($sha_user_finden);    
    
    if( ($sha_user_gefunden == 1) ){
             
        $zeile = mysql_fetch_array($sha_user_finden);

        $u_name = $zeile['name'];
        $u_rechte = $zeile['rechte'];
        $u_email = $zeile['email'];
        $u_anrede = $zeile['anrede'];
        $u_vorname = utf8_decode($zeile['vorname']);
        $u_nachname = utf8_decode($zeile['nachname']);
        $benutzer_id = $zeile['benutzer_id'];
        $u_zugang = $zeile['zugang'];
        $mandant = $zeile['mandant'];
        $hauptmandantID = $zeile['mid'];
        $superview = $zeile['superview'];
        $menu = $zeile['menu'];
        $abteilung = $zeile['abteilung'];
        $benutzer_name = utf8_decode($zeile['benutzer_name']);
        $benutzer_name = preg_replace('/[^[:alnum:]]/', '', $benutzer_name);
        
        // getmID
        getmID($mandant);


        $u_zugang_mandant = $zeile['zugang_mandant'];
        $u_zugang_mandant = explode(",", $u_zugang_mandant);

        $mandant_query = mysql_query("SELECT * FROM mandant ORDER BY mandant ASC", $verbindung_pep);
        while ($zeile = mysql_fetch_array($mandant_query)) {
            $mandant_all[$zeile['mandant']] = $zeile['abkz'];
            $mandant_all_id_Arr[$zeile['mandant']] = $zeile['id'];
        }

        $_SESSION['mandant_all'] = $mandant_all;
        $_SESSION['mandant_all_id_Arr'] = $mandant_all_id_Arr;
        $_SESSION['username'] = $u_name;
        $_SESSION['mandant'] = $mandant;
        $_SESSION['hauptmandantID'] = $hauptmandantID;
        $_SESSION['rechte'] = $u_rechte;
        $_SESSION['u_email'] = $u_email;
        $_SESSION['u_anrede'] = $u_anrede;
        $_SESSION['u_vorname'] = $u_vorname;
        $_SESSION['u_nachname'] = $u_nachname;
        $_SESSION['benutzer_id'] = $benutzer_id;
        $_SESSION['zugang'] = $u_zugang;
        $_SESSION['zugang_mandant'] = $u_zugang_mandant;
        $_SESSION['benutzer_name'] = $benutzer_name;
        $_SESSION['superview'] = $superview;
        $_SESSION['menu'] = $menu;
        $_SESSION['abteilung'] = $abteilung;
        $_SESSION['sid'] = session_id();
        header('Location: /system/index.php');
        exit();
    } else {
        $fehler = "<tr><td><span style='color:red'>Login fehlgeschlagen!</span></td></tr>";
    }
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" style="margin:0px; padding:0px;">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="layout.css"/>
        <link rel="SHORTCUT ICON" href="favicon.ico" type="image/x-icon;charset=binary">
		<link rel="icon" href="favicon.ico" type="image/x-icon;charset=binary">
    </head>

    <body style="margin:0px; padding:0px; background-color:#F2F2F2;">
        <div style="width:100%; margin-top:0px; height:50px; background-color:#FFFFFF; padding:20px;">
           <img src="../system/img/treaction_logo.jpg" height="40" style="padding-left:5px" />
        </div>
        <div style="width:100%; margin-top:15px;">
            <div style="text-align:center;">
                <div style="width:300px;margin-left:auto;margin-right:auto;height:350px;border:1px solid #A5ACB2;background-color:#FFFFFF;padding:30px;text-align:left;">
                    <span style="font-size:22px;font-weight:bold;color:#8e8901">Login</span><br /><br />
                    <form action="login.php" method="post">
                        <table style="font-size:14px;color:#666666;font-weight:bold;">
<?php if (isset($fehler)) {
    print $fehler;
} ?>
                            <tr><td>Benutzername</td></tr>
                            <tr><td><input type="text" name="username" id="username" style="width:290px;padding:4px;background-color:#F2F2F2;border:1px solid #A5ACB2;border-right:0px;border-bottom:0px;" /></td></tr>
                            <tr><td height="20"></td></tr>
                            <tr><td>Passwort</td></tr>
                            <tr><td><input type="password" name="pw" id="pw" style="width:290px;padding:4px;background-color:#F2F2F2;border:1px solid #A5ACB2;border-right:0px;border-bottom:0px;" /></td></tr>
                            <tr><td height="20"></td></tr>
                            <tr><td align="right"><input type="submit" name="submit" id="submit" value="anmelden" style="background-color:#8e8901;border:1px solid #D1B3F2;color:#FFFFFF;padding:3px;cursor:pointer;" /></td></tr>
                        </table>
                    </form>
                    <div id="browser" style="margin-top:50px">
                        <noscript>
                            <p style="color:red;font-weight:bold">Bitte JavaScript in ihrem Browser aktivieren.</p>
                        </noscript>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ //test for Firefox/x.x or Firefox x.x (ignoring remaining digits);
                var ffversion=new Number(RegExp.$1) // capture x.x portion and store as a number
                if (ffversion<3.5)
                    document.getElementById("browser").innerHTML = "<span style='color:red'>Sie verwenden eine �ltere Version von Firefox.</span><br />Bitte aktualisieren Sie ihren Browser um alle Funktionen des EMS nutzen zu k�nnen.";
  
            } 

            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
                var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
                if (ieversion<8)
                    document.getElementById("browser").innerHTML = "<span style='color:red'>Sie verwenden eine �ltere Version vom Internet Explorer.</span><br />Bitte aktualisieren Sie ihren Browser um alle Funktionen des EMS nutzen zu k�nnen.";
            } 

        </script>
    </body>
</html>
