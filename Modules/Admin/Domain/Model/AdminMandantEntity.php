<?php
namespace Modules\Admin\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * @author MohamedSamiJarmoud
 *
 */
class AdminMandantEntity extends AbstractEntity{
	
	/**
	 * id
	 *
	 * @var integer
	 */
	protected $id = NULL;
	
	/**
	 * premium_kampagne
	 * 
	 * @var float
	 */
	protected $premium_kampagne;
	
	/**
	 * kampagne
	 * 
	 * @var float
	 */
	protected $kampagne;
	
	/**
	 * nachversand
     * 
	 * @var float
	 */
	protected $nachversand;
	
	/**
	 * werbemittel
	 * 
	 * @var float
	 */
	protected $werbemittel;
	
	/**
	 * versand_tkp
	 *
	 * @var float
	 */
	protected $versand_tkp;
	
	/**
	 * lead_preis
	 * 
	 * @var float
	 */
	protected $lead_preis;
	
	/**
	 * datum
	 * 
	 * @var string
	 */
	protected $datum;
	
	/**
	 * benutzer
	 * 
	 * @var string
	 */
	protected $benutzer;

	
	public function __construct($primaryFieldName = 'id') {
		parent::__construct($primaryFieldName);
	}
	

	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}
	
	public function getPremiumKampagne() {
		return $this->premium_kampagne;
	}
	public function setPremiumKampagne($premium_kampagne){
		$this->premium_kampagne = $premium_kampagne;
	}
	public function getKampagne() {
		return $this->kampagne;
	}
	public function setNachversand($nachversand){
		$this->nachversand = $nachversand;
	}
		public function getNachversand() {
		return $this->nachversand;
	}
	public function setKampagne($kampagne){
		$this->kampagne = $kampagne;
	}
	public function getWerbemittel() {
		return $this->werbemittel;
	}
	public function setWerbemittel($werbemittel){
	    $this->werbemittel = $werbemittel;
	}
	public function getVersandTkp() {
		return $this->versand_tkp;
	}
	public function setVersandTkp($versand_tkp){
	    $this->versand_tkp = $versand_tkp;
	}
	public function getLeadPreis() {
		return $this->lead_preis;
	}
	public function setLeadPreis($lead_preis){
		$this->lead_preis = $lead_preis;
	}
	public function getDatum() {
		return $this->datum;
	}
	public function setDatum($datum) {
	  $this->datum = $datum;
	}
	public function getBenutzer() {
		return $this->benutzer;
	}
	public function setBenutzer($benutzer){
	    $this->benutzer = $benutzer;
	}
}