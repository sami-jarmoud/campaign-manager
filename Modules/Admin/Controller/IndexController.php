<?php

namespace Modules\Admin\Controller;

use Packages\Core\Mvc\Controller\AbstractController;
use Packages\Core\Utility\SystemHelperUtility;
use Packages\Core\Persistence\Generic\Query;

/**
 * Description of IndexController
 * @author MohamedSamiJarmoud
 *
 */
class IndexController extends AbstractController {

    /**
     * dateNow
     * 
     * @var \DateTime
     */
    protected $dateNow;

    /**
     * systemRepository
     * 
     * @var \Modules\Admin\Domain\Repository\AdminRepository
     */
    protected $systemRepository = NULL;

    /**
     * repository
     *
     * @var \Modules\Admin\Domain\Repository\AdminRepository
     */
    protected $repository = NULL;

    /**
     * postInitAction
     *
     * @return void
     */
    protected function postInitAction() {
        $this->dateNow = new \DateTime();
    }

    /**
     * initDefaultViewDataArray
     *
     * @param array $actionRequestDataArray
     * @return void
     */
    protected function initDefaultViewDataArray(array $actionRequestDataArray) {
        $this->defaultViewDataArray['actionRequestDataArray'] = $actionRequestDataArray;
        $this->defaultViewDataArray['clientEntity'] = $this->clientEntity;
        $this->defaultViewDataArray['settingsDataArray'] = $this->settings;
        $this->defaultViewDataArray['dateNow'] = $this->dateNow;
    }

    public function viewAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $mandantParameters = $this->systemRepository->getAdminMandant();

        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'mandantParameters' => $mandantParameters,
                )
        );

        $this->logger->endGroup();
    }

    public function createAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $benutzer = $this->loggedUserEntity->getBenutzer_name();

        $adminMandantEntity = new \Modules\Admin\Domain\Model\AdminMandantEntity();
        $adminMandantEntity->setBenutzer($benutzer);
        $adminMandantEntity->setDatum($this->dateNow->format('Y-m-d H:i:s'));
        $adminMandantEntity->setPremiumKampagne($actionRequestDataArray['premium_kampagne']);
        $adminMandantEntity->setKampagne($actionRequestDataArray['kampagne']);
        $adminMandantEntity->setNachversand($actionRequestDataArray['nachversand']);
        $adminMandantEntity->setWerbemittel($actionRequestDataArray['preis_werbemittel']);
        $adminMandantEntity->setVersandTkp($actionRequestDataArray['versand_tkp']);
        $adminMandantEntity->setLeadPreis($actionRequestDataArray['lead']);

        $result = $this->systemRepository->add(
                $adminMandantEntity, $this->settings['repositorySettings']['mandant']['mandantTable']
        );
        ?>
        <script type="text/javascript">
            setRequest('dispatch.php?_module=Admin&_controller=index&_action=view', 'admin_parameter');
        </script>
        <?php

        $this->logger->endGroup();
    }

    public function viewClickProfilAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $clientClickProfiles = $this->systemRepository->findClientClickProfiles($this->clientEntity->getId());
        $this->logger->log($clientClickProfiles, '$clientClickProfiles');
        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'clientClickProfiles' => $clientClickProfiles,
                )
        );

        $this->logger->endGroup();
    }

    public function viewDialogClickProfilAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $clientClickProfile = '';
        $clickProfilId = isset($actionRequestDataArray['clickProfilID']) ? \intval($actionRequestDataArray['clickProfilID']) : 0;
        if ($clickProfilId > 0
        ) {
            $clientClickProfile = $this->systemRepository->getClientClickProfile(
                    $this->clientEntity->getId(), $clickProfilId
            );
        }

        $this->logger->log($clientClickProfile, '$clientClickProfile');

        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'clientClickProfile' => $clientClickProfile,
                    'clickProfilId' => $clickProfilId,
                )
        );

        $this->logger->endGroup();
    }

    public function createClickProfilAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $active = $this->getStatus($actionRequestDataArray['status']);

        $clickProfileEntity = new \Packages\Core\Domain\Model\ClickProfileEntity();
        $clickProfileEntity->setTitle($actionRequestDataArray['name']);
        $clickProfileEntity->setShortcut($actionRequestDataArray['shortcut']);

        $clickProfilId = isset($actionRequestDataArray['clickProfilID']) ? \intval($actionRequestDataArray['clickProfilID']) : 0;
        if ($clickProfilId == 0
        ) {
            $clickProfileId = $this->systemRepository->add(
                    $clickProfileEntity, $this->settings['repositorySettings']['clickProfiles']['clickProfilesTable']
            );

            $clientClickProfileEntity = new \Packages\Core\Domain\Model\ClientClickProfileEntity();
            $clientClickProfileEntity->setClient_id($this->clientEntity->getId());
            $clientClickProfileEntity->setClick_profile_id($clickProfileId);
            $clientClickProfileEntity->setActive($active);

            $clientClickProfileId = $this->systemRepository->add(
                    $clientClickProfileEntity, $this->settings['repositorySettings']['clientClickProfiles']['clientClickProfilesTable']);
        } else {

            $newClickProfileEntity = $this->systemRepository->findClickProfileById(
                    $clickProfilId
            );
            $newClickProfileEntity->setTitle($actionRequestDataArray['name']);
            $newClickProfileEntity->setShortcut($actionRequestDataArray['shortcut']);
            $this->logger->log($newClickProfileEntity, '$newClickProfileEntity');

            $clientClickProfileEntity = new \Packages\Core\Domain\Model\ClientClickProfileEntity();
            $clientClickProfileEntity->setClient_id($this->clientEntity->getId());
            $clientClickProfileEntity->setClick_profile_id($clickProfilId);
            $clientClickProfileEntity->setActive($active);

            $resultUpdate = $this->updateClickProfil(
                    $newClickProfileEntity, $clientClickProfileEntity
            );
            $this->logger->log($resultUpdate, '$resultUpdate');
        }
        $this->logger->endGroup();
    }

    public function viewDeliverySystemDistributorAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $deliverySystemDistributor = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientId(
                $this->clientEntity->getId()
        );
        $this->logger->log($deliverySystemDistributor, '$deliverySystemDistributor');

        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'deliverySystemDistributor' => $deliverySystemDistributor,
                )
        );

        $this->logger->endGroup();
    }

    public function viewDialogDeliverySystemDistributorAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $deliverySystemDistributor = '';
        $disabled = '';
        $selectItem = '';
        $deliverySystemDistributorId = isset($actionRequestDataArray['dsdID']) ? \intval($actionRequestDataArray['dsdID']) : 0;
        if ($deliverySystemDistributorId > 0
        ) {
            $deliverySystemDistributor = $this->systemRepository->findDeliverySystemDistributorById(
                    $deliverySystemDistributorId
            );
            $selectItem = $deliverySystemDistributor->getClientDeliveryEntity()->getDeliverySystemEntity()->getId();
            $disabled = 'disabled';
        }

        $this->logger->log($deliverySystemDistributor, '$deliverySystemDistributor');
        #\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributor, '$deliverySystemDistributor');

        $clientDeliveries = $this->clientEntity->getClientDeliveries();
        $this->logger->log($clientDeliveries, '$clientDeliveries');

        $deliverySystemOptionItems = SystemHelperUtility::getDeliverySystemsItemsByIdOptionList(
                        $clientDeliveries, $selectItem
        );
        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'deliverySystemDistributor' => $deliverySystemDistributor,
                    'deliverySystemDistributorId' => $deliverySystemDistributorId,
                    'deliverySystemOptionItems' => $deliverySystemOptionItems,
                    'disabled' => '',
                )
        );

        $this->logger->endGroup();
    }

    public function createDeliverySystemDistributorAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $active = $this->getStatus($actionRequestDataArray['status']);
        $deliverySystemId = \intval($actionRequestDataArray['deliverySystem']);
        $deliverySystem = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
                $this->clientEntity->getId(), $deliverySystemId, $_SESSION['underClientIds'] // TODO: anders lösen
        );
        $m_asp_id = $deliverySystem[0]->getM_asp_id();
        $parent_id = $deliverySystem[0]->getParent_id();
        $test_list_id = isset($actionRequestDataArray['testlist_id']) ? \intval($actionRequestDataArray['testlist_id']) : '';

        $deliverySystemDistributorEntity = new \Packages\Core\Domain\Model\DeliverySystemDistributorEntity();
        $deliverySystemDistributorEntity->setM_asp_id($m_asp_id);
        $deliverySystemDistributorEntity->setTitle($actionRequestDataArray['name']);
        $deliverySystemDistributorEntity->setActive($active);
        $deliverySystemDistributorEntity->setDistributor_id(\intval($actionRequestDataArray['externe_id']));
        $deliverySystemDistributorEntity->setTestlist_distributor_id($test_list_id);
        $deliverySystemDistributorEntity->setParent_id($parent_id);

        $dsdId = isset($actionRequestDataArray['dsdID']) ? \intval($actionRequestDataArray['dsdID']) : 0;

        if ($dsdId == 0) {
            $deliverySystemDistributorId = $this->systemRepository->add(
                    $deliverySystemDistributorEntity, $this->settings['repositorySettings']['deliverySystemDistributor']['deliverySystemDistributorTable']
            );
            $this->logger->log($deliverySystemDistributorId, '$deliverySystemDistributorId');
        } else {
            $deliverySystemDistributorEntity->setId(\intval($dsdId));
            $this->logger->log($deliverySystemDistributorEntity, '$deliverySystemDistributorEntity');
            $resultUpdate = $this->updateDsd(
                    $deliverySystemDistributorEntity
            );
            $this->logger->log($resultUpdate, '$resultUpdate');
        }

        $this->logger->endGroup();
    }

    public function viewRecipientsAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $recipients = $this->repository->findAllRecipients();
        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'recipients' => $recipients,
                )
        );

        $this->logger->endGroup();
    }

    public function createRecipientAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $active = $this->getStatus($actionRequestDataArray['status']);
        $recipientId = isset($actionRequestDataArray['recipientID']) ? \intval($actionRequestDataArray['recipientID']) : 0;

        $recipientEntitiy = new \Modules\Admin\Domain\Model\AdminRecipientsEntitiy();

        $recipientEntitiy->setEmail($actionRequestDataArray['email']);
        $recipientEntitiy->setSex($actionRequestDataArray['anrede']);
        $recipientEntitiy->setFirst_name($actionRequestDataArray['vorname']);
        $recipientEntitiy->setName($actionRequestDataArray['nachname']);
        $recipientEntitiy->setBirthday($actionRequestDataArray['geburtsdatum']);
        $recipientEntitiy->setStreet($actionRequestDataArray['strasse']);
        $recipientEntitiy->setHouse_nr($actionRequestDataArray['hausnummer']);
        $recipientEntitiy->setPostal_code($actionRequestDataArray['plz']);
        $recipientEntitiy->setCity($actionRequestDataArray['ort']);
        $recipientEntitiy->setCountry($actionRequestDataArray['land']);
        $recipientEntitiy->setActive($active);

        if ($recipientId == 0) {
            $result = $this->repository->add(
                    $recipientEntitiy, $this->settings['repositorySettings']['recipients']['recipientsTable']);
        } else {
            $recipientEntitiy->setR_id($recipientId);
            $this->logger->log($recipientEntitiy, '$recipientEntitiy');

            $result = $this->updateRecipient(
                    $recipientEntitiy
            );
        }

        $this->logger->log($result, '$result');

        $this->logger->endGroup();
    }

    public function viewDialogRecipientAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $recipient = '';
        $recipientId = isset($actionRequestDataArray['recipientID']) ? \intval($actionRequestDataArray['recipientID']) : 0;

        if ($recipientId > 0
        ) {
            $recipient = $this->repository->findRecipientById($recipientId);
        }

        $this->logger->log($recipient, '$recipient');

        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'recipient' => $recipient,
                    'recipientId' => $recipientId,
                )
        );

        $this->logger->endGroup();
    }

    public function viewDialogDeliverySystemDomainAction(array $actionRequestDataArray) {

        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

        $deliverySystemDomain = '';
        $selectItem = '';
        $disabled = '';
        $deliverySystemDomainId = isset($actionRequestDataArray['dsdnID']) ? \intval($actionRequestDataArray['dsdnID']) : 0;    
        $deliverySystemDistributorItem = '';
        if ($deliverySystemDomainId > 0
        ) {
            
            $deliverySystemDomain = $this->systemRepository->findDeliverySystemDomainById(
                    $deliverySystemDomainId
            );
            $selectItem = (int)$deliverySystemDomain->getAsp_id();
            $disabled = 'disabled';
               /* @var $deliverySystemDistributorDataArray \Packages\Core\Persistence\ArrayIterator */
        $deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
			$this->clientEntity->getId(),
			$selectItem,
			$_SESSION['underClientIds'] // TODO: anders lösen
		);

        $deliverySystemDistributorItem = SystemHelperUtility::getDeliverySystemDistributorsItems(
                $deliverySystemDistributorDataArray, 
                (int) $deliverySystemDomain->getDsd_id()
                );
        }
        $this->logger->log($deliverySystemDomain, '$deliverySystemDomain');
        #\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributor, '$deliverySystemDistributor');

        $clientDeliveries = $this->clientEntity->getClientDeliveries();
        $this->logger->log($clientDeliveries, '$clientDeliveries');

        $deliverySystemDomainOptionItems = SystemHelperUtility::getDeliverySystemsItemsByIdOptionList(
                        $clientDeliveries, $selectItem
        );
     
                
        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'deliverySystemDomain' => $deliverySystemDomain,
                    'deliverySystemDomainId' => $deliverySystemDomainId,
                    'deliverySystemDomainOptionItems' => $deliverySystemDomainOptionItems,
                    'disabled' => '',
                    'deliverySystemDistributorItem' => $deliverySystemDistributorItem,
                )
        );

        $this->logger->endGroup();
    }

    public function viewDeliverySystemDomainAction(array $actionRequestDataArray) {
        $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
        $domaineDeliverySystems = $this->systemRepository->findDomainDeliverySystemsDataArrayByClientId(
                $this->clientEntity->getId(),
                ''
        );
       
        $this->logger->log($domaineDeliverySystems, '$domaineDeliverySystems');
        $this->initDefaultViewDataArray($actionRequestDataArray);
        $this->initView(
                array(
                    'domaineDeliverySystems' => $domaineDeliverySystems,
                )
        );

        $this->logger->endGroup();
    }

    public function createDeliverySystemDomainAction(array $actionRequestDataArray) {
            $this->logger->beginGroup(__FUNCTION__);
        $this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
 
        $dsd_id = \intval($actionRequestDataArray['campaign']['deliverySystemDistributor']);

        $deliverySystemEntity = $this->systemRepository->findDeliverySystemDistributorById($dsd_id);
         
        $active = $this->getStatus($actionRequestDataArray['status']);
        $deliverySystemId = $deliverySystemEntity->getClientDeliveryEntity()->getAsp_id();
        $deliverySystemDomainEntity = new \Packages\Core\Domain\Model\DomainDeliveryEntity();
        $deliverySystemDomainEntity->setDomain_name($actionRequestDataArray['domain']);
        $deliverySystemDomainEntity->setM_id($this->clientEntity->getId());
        $deliverySystemDomainEntity->setAsp_id($deliverySystemId);
        $deliverySystemDomainEntity->setDsd_id($dsd_id);
        $deliverySystemDomainEntity->setVerteiler($deliverySystemEntity->getTitle());
        $deliverySystemDomainEntity->setActive($active);

        $dsdnId = isset($actionRequestDataArray['dsdnID']) ? \intval($actionRequestDataArray['dsdnID']) : 0;

       if ($dsdnId == 0) {
            $deliverySystemDomainId = $this->systemRepository->add(
                    $deliverySystemDomainEntity, 'domain_asp'
            );
            $this->logger->log($deliverySystemDomainId, '$deliverySystemDomainId');
        } else {
           
            $deliverySystemDomainEntity->setDasp_id(\intval($dsdnId));
            $this->logger->log($deliverySystemDomainEntity, '$deliverySystemDomainEntity');

            $resultUpdate = $this->updateDsdn(
                    $deliverySystemDomainEntity
            );
            $this->logger->log($resultUpdate, '$resultUpdate');
        }

        $this->logger->endGroup();
    }

    protected function getStatus($status) {
        switch ($status) {
            case 'enable':
                $active = '1';
                break;

            case 'disable':
                $active = '0';
                break;

            default :
                $active = '1';
        }
        return $active;
    }

    protected function updateDsd(\Packages\Core\Domain\Model\DeliverySystemDistributorEntity $deliverySystemDistributorEntity) {
        $query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['deliverySystemDistributor']['deliverySystemDistributorTable'])
                . 'SET '
                . Query::masketField('title') . ' = '
                . '\'' . $deliverySystemDistributorEntity->getTitle() . '\','
                . Query::masketField('distributor_id') . ' = '
                . '\'' . $deliverySystemDistributorEntity->getDistributor_id() . '\','
                . Query::masketField('testlist_distributor_id') . ' = '
                . '\'' . $deliverySystemDistributorEntity->getTestlist_distributor_id() . '\','
                . Query::masketField('active') . ' = '
                . '\'' . $deliverySystemDistributorEntity->getActive() . '\''
                . ' WHERE'
                . Query::masketField('id') . ' = '
                . '\'' . $deliverySystemDistributorEntity->getId() . '\''
                . ';'
        ;
        $result = $this->systemRepository->executeQuery(
                $query
        );
        $this->logger->log($result, __FUNCTION__);
        return $result;
    }

    protected function updateDsdn(\Packages\Core\Domain\Model\DomainDeliveryEntity $deliverySystemDomainEntity) {
            $query = 'UPDATE ' . Query::masketField('domain_asp')
                . 'SET '
                . Query::masketField('asp_id') . ' = '
                . '\'' . $deliverySystemDomainEntity->getAsp_id() . '\','
                . Query::masketField('dsd_id') . ' = '
                . '\'' . $deliverySystemDomainEntity->getDsd_id() . '\',' 
                . Query::masketField('verteiler') . ' = '
                . '\'' . $deliverySystemDomainEntity->getVerteiler() . '\','
                . Query::masketField('domain_name') . ' = '
                . '\'' . $deliverySystemDomainEntity->getDomain_name() . '\','               
                . Query::masketField('active') . ' = '
                . '\'' . $deliverySystemDomainEntity->getActive() . '\''
                . ' WHERE'
                . Query::masketField('dasp_id') . ' = '
                . '\'' . $deliverySystemDomainEntity->getDasp_id() . '\''
                . ';'
        ;
      
        $result = $this->systemRepository->executeQuery(
                $query
        );

        $this->logger->log($result, __FUNCTION__);
 
        return $query;
    }
    
    protected function updateClickProfil(\Packages\Core\Domain\Model\ClickProfileEntity $newClickProfileEntity, \Packages\Core\Domain\Model\ClientClickProfileEntity $clientClickProfileEntity) {
        $queryClickProfile = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['clickProfiles']['clickProfilesTable'])
                . 'SET '
                . Query::masketField('title') . ' = '
                . '\'' . $newClickProfileEntity->getTitle() . '\','
                . Query::masketField('shortcut') . ' = '
                . '\'' . $newClickProfileEntity->getShortcut() . '\''
                . ' WHERE'
                . Query::masketField('id') . ' = '
                . '\'' . $newClickProfileEntity->getId() . '\''
                . ';'
        ;
        $result['clickProfile'] = $this->systemRepository->executeQuery(
                $queryClickProfile
        );
        $queryClientClickProfile = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['clientClickProfiles']['clientClickProfilesTable'])
                . 'SET '
                . Query::masketField('active') . ' = '
                . '\'' . $clientClickProfileEntity->getActive() . '\''
                . ' WHERE'
                . Query::masketField('client_id') . ' = '
                . '\'' . $clientClickProfileEntity->getClient_id() . '\' and '
                . Query::masketField('click_profile_id') . ' = '
                . '\'' . $clientClickProfileEntity->getClick_profile_id() . '\''
                . ';'
        ;
        $result['clientClickProfile'] = $this->systemRepository->executeQuery(
                $queryClientClickProfile
        );
        $this->logger->log($result, __FUNCTION__);
        return $result;
    }

    protected function updateRecipient(\Modules\Admin\Domain\Model\AdminRecipientsEntitiy $recipientEntitiy) {
        $query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['recipients']['recipientsTable'])
                . 'SET '
                . Query::masketField('name') . ' = '
                . '\'' . $recipientEntitiy->getName() . '\','
                . Query::masketField('first_name') . ' = '
                . '\'' . $recipientEntitiy->getFirst_name() . '\','
                . Query::masketField('sex') . ' = '
                . '\'' . $recipientEntitiy->getSex() . '\','
                . Query::masketField('birthday') . ' = '
                . '\'' . $recipientEntitiy->getBirthday() . '\','
                . Query::masketField('postal_code') . ' = '
                . '\'' . $recipientEntitiy->getPostal_code() . '\','
                . Query::masketField('country') . ' = '
                . '\'' . $recipientEntitiy->getCountry() . '\','
                . Query::masketField('street') . ' = '
                . '\'' . $recipientEntitiy->getStreet() . '\','
                . Query::masketField('house_nr') . ' = '
                . '\'' . $recipientEntitiy->getHouse_nr() . '\','
                . Query::masketField('city') . ' = '
                . '\'' . $recipientEntitiy->getCity() . '\','
                . Query::masketField('email') . ' = '
                . '\'' . $recipientEntitiy->getEmail() . '\','
                . Query::masketField('active') . ' = '
                . '\'' . $recipientEntitiy->getActive() . '\''
                . ' WHERE'
                . Query::masketField('r_id') . ' = '
                . '\'' . $recipientEntitiy->getR_id() . '\''
                . ';'
        ;
        $result = $this->repository->executeQuery(
                $query
        );
        $this->logger->log($result, __FUNCTION__);
        return $result;
    }

}
