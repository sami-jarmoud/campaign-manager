<?php
#\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributor, '$deliverySystemDistributor');
$disable = $name = $description = $externe_id = $testlist_id = '';
$enable = 'checked';

if($deliverySystemDistributorId > 0
	){

		$name =  $deliverySystemDistributor->getTitle();
		$description = $deliverySystemDistributor->getTitle();
		$externe_id = $deliverySystemDistributor->getDistributor_id();
		$testlist_id = $deliverySystemDistributor->getTestlist_distributor_id();
		$status = $deliverySystemDistributor->getActive();

	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>

<input type="hidden" name="dsdID" id ="dsdID" value="<?php echo $deliverySystemDistributorId; ?>" />
<div id="tab_dsd" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
 <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#dsdTable"><em>Allgemein</em></a></li>
    </ul>	
		<div class="yui-content" style="margin:0px;padding:0px;">
			 <div id="dsdTable">
				<table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
			    <tr>
                    <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Name<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="name" name="name" style="width:250px"  value="<?php echo $name;?>" /></td>
				 </tr>
				 <tr>
                    <td align="right" class="info">Beschreibung<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="description" name="description" style="width:250px"  value="<?php echo $name;?>" /></td>
				 </tr>	
				 <tr class="bg2">
                    <td align="right" class="info">Versandsystem<span style="color:red;">*</span></td>
                   		<td align="left">
                                    <select name="deliverySystem" id="deliverySystem" <?php echo $disabled?>>
								<option value="">- Bitte ausw&auml;hlen -</option>
								<?php echo $deliverySystemOptionItems; ?>
							</select>
						</td>
				 </tr>				 
				 <tr>
                    <td align="right" class="info">Status<span style="color:red;">*</span></td>
                    <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable;?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable;?>/> Deaktivieren</td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Externe ID<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="externe_id" name="externe_id" style="width:100px"  value="<?php echo $externe_id;?>" /></td>
				 </tr>
				 <tr>
                    <td align="right" class="info">Testlist ID</td>
                    <td align="left"><input type="text" id="testlist_id" name="testlist_id" style="width:100px"  value="<?php echo $testlist_id;?>" /></td>
				 </tr>				 
				</table>
			 </div>
		</div>
	</div>
	</div>
<script type="text/javascript">
		 var tabView_dsd = new YAHOO.widget.TabView('tab_dsd');
</script>