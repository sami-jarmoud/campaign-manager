<?php
 $disable = $name = $shortcut = '';
 $enable = 'checked';
if($clickProfilId > 0
	){
	foreach ($clientClickProfile as $clickProfile){
	$name =  $clickProfile->getClickProfileEntity()->getTitle();
	$shortcut= $clickProfile->getClickProfileEntity()->getShortcut();
	$status = $clickProfile->getActive();
	}
	if($status == '1'){
		$enable = 'checked';
	}else{
		$disable = 'checked';
	}
}
?>
<input type="hidden" name="clickProfilID" id ="clickProfilID" value="<?php echo $clickProfilId ?>" />

<div id="tab_click" class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
 <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#clickProfilTable"><em>Allgemein</em></a></li>
    </ul>	
			<div class="yui-content" style="margin:0px;padding:0px;">
			 <div id="clickProfilTable">
				<table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
			    <tr>
                    <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Name<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="name" name="name" style="width:250px"  value="<?php echo $name;?>" /></td>
				 </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Abkürzung<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="abkuerzung" name="shortcut" style="width:250px"  value="<?php echo $shortcut; ?>" /></td>
				 </tr>	
				 <tr>
                    <td align="right" class="info">Status</td>
                    <td align="left"><input type="radio" name="status" value="enable" <?php echo $enable;?>/> aktivieren  &nbsp;&nbsp;<input type="radio" name="status" value="disable" <?php echo $disable;?>/> Deaktivieren</td>
				 </tr>				 
				
				</table>
			 </div>
			</div>
	</div>
	</div>
<script type="text/javascript">
		 var tabView_click = new YAHOO.widget.TabView('tab_click');
</script>