<?php
header('Content-Type: text/html; charset=UTF-8');

use Modules\Statistic\Utility\StatisticUtility;

?>
   <table class="scrollTable2" id="scrollTable2" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
        <tr>
            <th>Benutzer</th>
            <th>Änderung am</th>
            <th>Premium-Kampagne</th>
            <th>Kampagne</th>
            <th>Nachversand</th>
            <th>Werbemittel</th>
			<th>Versand-TKP</th>
			<th>Lead</th>
            <th style="width:100%"></th>
        </tr>
        <?php foreach($mandantParameters as $mandantParameter) { ?>
	    <tr>
			<td style='font-weight:bold;'><?php echo $mandantParameter->getBenutzer();?></td>
			<td><?php echo StatisticUtility::deutschdatum($mandantParameter->getDatum());?></td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getPremiumKampagne());?> €</td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getKampagne());?> €</td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getNachversand());?> €</td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getWerbemittel());?> €</td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getVersandTkp());?> €</td>
			<td><?php echo StatisticUtility::sumFormatWithDecimal3($mandantParameter->getLeadPreis());?> €</td>
        </tr>
		<?php }?>
    </table>

<div id="container_admin_parameter" class="yui-pe-content">
	<div class="panel_label">Administration</div>
    <div class="hd" id="nv_hd">
		<span class="hd_label">Mandantverwaltung</span>
		<span class="hd_sublabel"></span>
	</div>
<div class="bd" style="text-align:left">	
 <div  class="yui-navset" style="padding-top:7px;text-align:left;border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#parameterTable"><em>Allgemein</em></a></li>
    </ul>	
			<div class="yui-content" style="margin:0px;padding:0px;">
			 <div id="parameterTable">
				<table class="neu_k" cellpadding="0" cellspacing="0" border="0" style="font-size:11px;">
			    <tr>
                    <td colspan="2" align="right"><span style="color:red;">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Premium-Kampagne<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="premium_kampagne" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="<?php #print $nachname; ?>" /></td>
                </tr>
				 <tr>
                    <td align="right" class="info">Kampagne<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="kampagne" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="" /></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Nachversand<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="nachversand" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="<?php #print $nachname; ?>" /></td>
                </tr>
				 <tr>
                    <td align="right" class="info">Werbemittel<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="preis_werbemittel" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="<?php #print $nachname; ?>" /></td>
                </tr>
				 <tr class="bg2">
                    <td align="right" class="info">Versand-TKP<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="versand_tkp" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="<?php #print $nachname; ?>" /></td>
                </tr>
				 <tr>
                    <td align="right" class="info">Lead<span style="color:red;">*</span></td>
                    <td align="left"><input type="text" id="lead" style="width:175px" onchange="changeValueToFloat(this.value, this.id)" onBlur="Validate(this.id)" value="<?php #print $nachname; ?>" /></td>
                </tr>				
				
				</table>
			 </div>
			</div>
	</div>
</div>	
</div>

<?php 
$viewPath = \str_replace('Templates', '', __DIR__);
require_once($viewPath . 'Dialog' . \DIRECTORY_SEPARATOR . 'dialogAdminView.php');

?>