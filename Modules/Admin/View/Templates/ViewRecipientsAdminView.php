<?php
use Modules\Admin\Utility\AdminUtility;
$j = $data = '';

foreach($recipients as $recipient) {
        if ($j == 3 || !$j) {
            $j = 1;
        }
        
        if ($j == 1) {
            $bg_color = '#FFFFFF';
        } elseif ($j == 2) {
            $bg_color = '#E4E4E4';
        }

		$status = $recipient->getActive()== '1' ? 'aktiv' : 'inaktiv';
		
		if($status == 'inaktiv'){
			$color = '#F40404';
		}else{
			$color = '';
		}
	    $data .= "<tr onclick=\"doIt_recipient(event,'" . $recipient->getR_id() . "');\" bgcolor='" . $bg_color . "' onMouseOver=\"this.bgColor='#A6CBFF'\" onMouseOut=\"this.bgColor='" . $bg_color . "' \" style='cursor:pointer' >";
        $data .= "<td style='font-weight:bold;'>" . $recipient->getR_id() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getEmail() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getSex() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getFirst_name() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getName() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . AdminUtility::deutschdatum($recipient->getBirthday()) . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getStreet() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getHouse_nr() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getPostal_code() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getCity() . "</td>";
		$data .= "<td style='font-weight:bold;'>" . $recipient->getCountry() . "</td>";
        $data .= "<td style='font-weight:bold; color: ". $color ."'>" . $status . "</td>";
        $data .= "</tr>";
        $j++;	
}	

?>
<input type="hidden" name="recipientID" id ="recipientID" value="" />
<div id="fakeContainer">
 <table class="scrollTable2" id="scrollTable2" cellpadding="0" cellspacing="0" border="0" bgcolor="#F5F5F5">
        <tr>
            <th>ID</th>
			<th>E-Mail</th>
            <th>Anrede</th>
            <th>Vorname</th>
			<th>Nachname</th>
			<th>Geburtsdatum</th>
            <th>Strasse</th>
			<th>Hausnummer</th>
            <th>PLZ</th>
			<th>Ort</th>
            <th>Land</th>
            <th>Status</th>			
			
			
        </tr>
        <?php print $data; ?>
    </table>
</div>
<script type="text/javascript">
    wi = document.documentElement.clientWidth;
    hi = document.documentElement.clientHeight;
    
    if (uBrowser == 'Firefox') {
        hi = hi-142;
        wi = wi-205;
    } else {
        hi = hi-143;
        wi = wi-205;
    }
    
    document.getElementById("fakeContainer").style.width = wi+"px";
    document.getElementById("fakeContainer").style.height = hi+"px";



    var mySt = new superTable("scrollTable2", {
        cssSkin : "sSky",
        fixedCols : 0,
        headerRows : 1
    });
</script>



