<?php
namespace Modules\Campaign\Controller;

use Modules\Campaign\Controller\AbstractController;
use Packages\Core\Factory\DeliverySystem;
use Packages\Core\Utility\GeneralUtility;
use Packages\Core\Utility\FileUtility;
use Packages\Core\Utility\FilterUtility;
use Packages\Core\Utility\MailUtility;
use Packages\Core\Utility\Format\OthersUtility;
use Packages\Core\Utility\Html\FormUtility;
use Packages\Core\Utility\SystemHelperUtility;
use Packages\Core\Persistence\Generic\Query;
use Packages\Core\vendor\TrelloControler;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;
use Modules\Campaign\Utility\MailingEntityUtility;


/**
 * Description of IndexController
 *
 * @author Cristian.Reus
 */
class IndexController extends AbstractController {
	
	/**
	 * dateNow
	 * 
	 * @var \DateTime
	 */
	protected $dateNow;
	
	/**
	 * userDataArray
	 * 
	 * @var \Packages\Core\Persistence\ArrayIterator
	 */
	protected $userDataArray;
		
	/**
	 * postInitAction
	 * 
	 * @return void
	 */
	protected function postInitAction() {
		$this->targetDir = \DIR_Uploads . $this->clientEntity->getAbkz() . \DIRECTORY_SEPARATOR;
		$this->dateNow = new \DateTime();
	}
	
	/**
	 * postInitializeAction
	 * 
	 * @return void
	 */
	public function postInitializeAction() {
		$this->userDataArray = $this->userRepository->findAllByClientId(
			$this->clientEntity->getId(),
			TRUE
		);
	}
	
	/**
	 * initDefaultViewDataArray
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 */
	protected function initDefaultViewDataArray(array $actionRequestDataArray) {
		$this->defaultViewDataArray['actionRequestDataArray'] = $actionRequestDataArray;
		$this->defaultViewDataArray['targetDir'] = $this->targetDir;
		$this->defaultViewDataArray['dateNow'] = $this->dateNow;
		$this->defaultViewDataArray['userDataArray'] = $this->userDataArray;
		$this->defaultViewDataArray['clientEntity'] = $this->clientEntity;
		$this->defaultViewDataArray['loggedUserEntity'] = $this->loggedUserEntity;
		$this->defaultViewDataArray['settingsDataArray'] = $this->settings;
	}
	
	
	/**
	 * newAction
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public function newAction(array $actionRequestDataArray) {
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$customersDataArray = $this->repository->findAllActiveCustomers();
		if (\count($customersDataArray) === 0) {
			throw new \InvalidArgumentException('no customersDataArray', 1447067555);
		}
		$this->logger->log($customersDataArray, '$customersDataArray');
		
		$campaignEntity = GeneralUtility::makeInstance($this->settings['repositorySettings']['campaign']['fetchClass']);
		$this->logger->log($campaignEntity, '$campaignEntity');
		
		$this->clientEntity->getClientDeliveries()->rewind();
		$firstDeliverySystemItemEntity = $this->clientEntity->getClientDeliveries()->current();
		/* @var $firstDeliverySystemItemEntity \Packages\Core\Domain\Model\ClientDeliveryEntity */
		$this->logger->log($firstDeliverySystemItemEntity, '$firstDeliverySystemItemEntity');
		
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		
		$clientDeliveries = $this->clientEntity->getClientDeliveries();
		$this->logger->log($clientDeliveries, '$clientDeliveries');
		
		$salesFullname = $this->getSalesNameFromCampaignData($campaignEntity);
		$this->logger->log($salesFullname, '$salesFullname');	

                $domaineDeliverySystemsDataArray = $this->systemRepository->findDomainDeliverySystemsDataArrayByClientId(
                         $this->clientEntity->getId(),
                        $firstDeliverySystemItemEntity->getAsp_id()
                        );
                $this->logger->log($domaineDeliverySystemsDataArray, '$domaineDeliverySystemsDataArray');

		$this->initDefaultViewDataArray($actionRequestDataArray);
		$this->initView(
			array(
				'customersDataArray' => $customersDataArray,
				'campaignEntity' => $campaignEntity,
				'hvCampaignDate' => NULL,
				'firstDeliverySystemItemEntity' => $firstDeliverySystemItemEntity,
				'clientClickProfiles' => $clientClickProfiles,
				'clientDeliveries' => $clientDeliveries,
				'salesFullname' => $salesFullname,
                                'domaineDeliverySystemsDataArray' => $domaineDeliverySystemsDataArray,
			)
		);
		
		$this->logger->endGroup();
	}
	
	/**
	 * editAction
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \LengthException
	 * @throws \DomainException
	 * @throws \InvalidArgumentException
	 */
	public function editAction(array $actionRequestDataArray) {
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1447419556);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1447420047);
		}
		
		if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
			/**
			 * getCampaignDataItemById
			 * 
			 * getDataFromHVCampaign
			 */
			$hvCampaignEntity = $this->repository->findOneByField('k_id', $campaignEntity->getNv_id());
			$hvCampaignEntity->_resetCleanProperties();
			$this->logger->log($hvCampaignEntity, '$hvCampaignEntity');
			
			$hvCampaignDate = $hvCampaignEntity->getDatum();
		} else {
			$hvCampaignDate = clone $campaignEntity->getDatum();
		}
		
		if (\strlen($campaignEntity->getAbrechnungsart()) === 0) {
			/**
			 * getDataFromHVCampaign (only for old campaigns)
			 */
			$campaignEntity->setAbrechnungsart(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray[$hvCampaignEntity->getAbrechnungsart()]);
			$campaignEntity->setPreis($hvCampaignEntity->getPreis());
			$campaignEntity->setPreis2($hvCampaignEntity->getPreis2());
			$campaignEntity->setPreis_gesamt($hvCampaignEntity->getPreis_gesamt());
			$campaignEntity->setUnit_price($hvCampaignEntity->getUnit_price());
		} else {
			$campaignEntity->setAbrechnungsart(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
		}
		$this->logger->log($campaignEntity, '$campaignEntity');
		
		$campaignIsLock = $this->processCampaignLooked($campaignEntity);
		
		$customersDataArray = $this->repository->findAllActiveCustomers();
		if (\count($customersDataArray) === 0) {
			throw new \InvalidArgumentException('no customersDataArray', 1447067555);
		}
		$this->logger->log($customersDataArray, '$customersDataArray');
		
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		
		$clientDeliveries = $this->clientEntity->getClientDeliveries();
		$this->logger->log($clientDeliveries, '$clientDeliveries');
		
		
		$campaignDeliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
		$this->logger->log($campaignDeliverySystemDistributorEntity, '$campaignDeliverySystemDistributorEntity');

		$deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
			$this->clientEntity->getId(),
			$campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId(),
			$_SESSION['underClientIds'] // TODO: anders lösen
		);

		$salesFullname = $this->getSalesNameFromCampaignData($campaignEntity);
		$this->logger->log($salesFullname, '$salesFullname');
                $domaineDeliverySystemsDataArray = $this->systemRepository->findDomainDeliverySystemsDataArrayByClientId(
                         $this->clientEntity->getId(),
                         $campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId()
                        );
                $this->logger->log($domaineDeliverySystemsDataArray, '$domaineDeliverySystemsDataArray');
                
		$this->initDefaultViewDataArray($actionRequestDataArray);
		$this->initView(
			array(
				'campaignEntity' => $campaignEntity,
				'hvCampaignDate' => $hvCampaignDate,
				'campaignIsLock' => $campaignIsLock,
				'customersDataArray' => $customersDataArray,
				'clientClickProfiles' => $clientClickProfiles,
				'clientDeliveries' => $clientDeliveries,
				'campaignDeliverySystemDistributorEntity' => $campaignDeliverySystemDistributorEntity,
				'deliverySystemDistributorDataArray' => $deliverySystemDistributorDataArray,
				'salesFullname' => $salesFullname,
                                'domaineDeliverySystemsDataArray' => $domaineDeliverySystemsDataArray,
			)
		);
		
		$this->logger->endGroup();
	}
	
	/**
	 * copyAction
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \LengthException
	 * @throws \DomainException
	 * @throws \InvalidArgumentException
	 */
	public function copyAction(array $actionRequestDataArray) {
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1447419556);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1447420047);
		}
		
		$campaignEntity->setAbrechnungsart(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
		
		// resetCampaignAdvertisings
		$campaignEntity->setCampaignAdvertisings(new \Packages\Core\Persistence\ObjectStorage());
		
		// resetCampaignSettings
		$this->resetCampaignSettings($campaignEntity);
		$this->logger->log($campaignEntity, '$campaignEntity');
		
		$hvCampaignDate = clone $campaignEntity->getDatum();
		
		$customersDataArray = $this->repository->findAllActiveCustomers();
		if (\count($customersDataArray) === 0) {
			throw new \InvalidArgumentException('no customersDataArray', 1447067555);
		}
		$this->logger->log($customersDataArray, '$customersDataArray');
		
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		
		$clientDeliveries = $this->clientEntity->getClientDeliveries();
		$this->logger->log($clientDeliveries, '$clientDeliveries');
		
		$campaignDeliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
		$this->logger->log($campaignDeliverySystemDistributorEntity, '$campaignDeliverySystemDistributorEntity');
		
		$deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
			$this->clientEntity->getId(),
			$campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId(),
			$_SESSION['underClientIds'] // TODO: anders lösen
		);
		
		$salesFullname = $this->getSalesNameFromCampaignData($campaignEntity);
		$this->logger->log($salesFullname, '$salesFullname');
		
                $domaineDeliverySystemsDataArray = $this->systemRepository->findDomainDeliverySystemsDataArrayByClientId(
                         $this->clientEntity->getId(),
                        $campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId()
                        );
                $this->logger->log($domaineDeliverySystemsDataArray, '$domaineDeliverySystemsDataArray');
                
		$this->initDefaultViewDataArray($actionRequestDataArray);
		$this->initView(
			array(
				'campaignEntity' => $campaignEntity,
				'hvCampaignDate' => $hvCampaignDate,
				'campaignIsLock' => '',
				'customersDataArray' => $customersDataArray,
				'clientClickProfiles' => $clientClickProfiles,
				'clientDeliveries' => $clientDeliveries,
				'campaignDeliverySystemDistributorEntity' => $campaignDeliverySystemDistributorEntity,
				'deliverySystemDistributorDataArray' => $deliverySystemDistributorDataArray,
				'salesFullname' => $salesFullname,
                                'domaineDeliverySystemsDataArray' => $domaineDeliverySystemsDataArray,
			)
		);
		
		$this->logger->endGroup();
	}
	
	/**
	 * addNvAction
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \LengthException
	 * @throws \DomainException
	 * @throws \InvalidArgumentException
	 */
	public function addNvAction(array $actionRequestDataArray) {
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1449492163);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1447420047);
		}
		
		$hvCampaignDate = clone $campaignEntity->getDatum();
		
		
		$campaignEntity->setAbrechnungsart(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
		
		// changeCampaignDatum
		if ($campaignEntity->getDatum()->format('U') < $this->dateNow->format('U')) {
			$campaignEntity->setDatum('NOW');
		}
		// min. 1 stunde nach hv campagne
		$campaignEntity->getDatum()->modify('+1 hour');
		$campaignEntity->getDatum()->setTime($campaignEntity->getDatum()->format('H'), 0, 0);
		
		$hvCampaignId = isset($actionRequestDataArray['hvCampaignId']) ? \intval($actionRequestDataArray['hvCampaignId']) : 0;
		if ($hvCampaignId > 0) {
			$campaignEntity->setNv_id($hvCampaignId);
			$campaignEntity->setUse_campaign_start_date(FALSE);
		} else {
			$campaignEntity->setNv_id($campaignEntity->getK_id());
		}
		unset($hvCampaignId);
		
		// resetCampaignAdvertisings
		$campaignEntity->setCampaignAdvertisings(new \Packages\Core\Persistence\ObjectStorage());
		
		// resetCampaignSettings
		$this->resetCampaignSettings($campaignEntity);
		
		// resetOthersCampaignSettings
		$campaignEntity->setVorgabe_m(0);
		$campaignEntity->setBroking_price(0);
		$campaignEntity->setBroking_volume(0);
		$this->logger->log($campaignEntity, '$campaignEntity');
		
		$customersDataArray = $this->repository->findAllActiveCustomers();
		if (\count($customersDataArray) === 0) {
			throw new \InvalidArgumentException('no customersDataArray', 1447067555);
		}
		$this->logger->log($customersDataArray, '$customersDataArray');
		
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		
		$clientDeliveries = $this->clientEntity->getClientDeliveries();
		$this->logger->log($clientDeliveries, '$clientDeliveries');
		
		
		$campaignDeliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
		$this->logger->log($campaignDeliverySystemDistributorEntity, '$campaignDeliverySystemDistributorEntity');
		
		$deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
			$this->clientEntity->getId(),
			$campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId(),
			$_SESSION['underClientIds'] // TODO: anders lösen
		);
		
		$salesFullname = $this->getSalesNameFromCampaignData($campaignEntity);
		$this->logger->log($salesFullname, '$salesFullname');
		
                $domaineDeliverySystemsDataArray = $this->systemRepository->findDomainDeliverySystemsDataArrayByClientId(
                         $this->clientEntity->getId(),
                        $campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId()
                        );
                $this->logger->log($domaineDeliverySystemsDataArray, '$domaineDeliverySystemsDataArray');
                
		$this->initDefaultViewDataArray($actionRequestDataArray);
		$this->initView(
			array(
				'campaignEntity' => $campaignEntity,
				'hvCampaignDate' => $hvCampaignDate,
				'campaignIsLock' => '',
				'customersDataArray' => $customersDataArray,
				'clientClickProfiles' => $clientClickProfiles,
				'clientDeliveries' => $clientDeliveries,
				'campaignDeliverySystemDistributorEntity' => $campaignDeliverySystemDistributorEntity,
				'deliverySystemDistributorDataArray' => $deliverySystemDistributorDataArray,
				'salesFullname' => $salesFullname,
                                'domaineDeliverySystemsDataArray' => $domaineDeliverySystemsDataArray,
			)
		);
		
		$this->logger->endGroup();
	}
	
	/**
	 * createAction
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \InvalidArgumentException
	 */
	public function createAction(array $actionRequestDataArray) {
		if (!isset($_POST['campaign'])) {
			throw new \InvalidArgumentException('invalid data!', 1447232162);
		}

		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$requestDataArray');
		
		$newCampaignEntity = GeneralUtility::makeInstance($this->settings['repositorySettings']['campaign']['fetchClass']);
		/* @var $newCampaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
		$this->updateCampaignEntityFromPostDataArray(
			$actionRequestDataArray['campaign'],
			$newCampaignEntity
		);
		
		if ($actionRequestDataArray['mailingTyp'] == 'NV') {
			if (\strpos($newCampaignEntity->getK_name(), '[NV') === FALSE) {
				$newCampaignEntity->setK_name($newCampaignEntity->getK_name() . ' [NV ' . $newCampaignEntity->getDatum()->format('d.m') . ']');
			}
		}
		
		/**
		 * processCampaignContactPersonData
		 */
		$this->processCampaignContactPersonData(
			$actionRequestDataArray['campaign'],
			$actionRequestDataArray['contactPerson'],
			$actionRequestDataArray['customer'],
			$newCampaignEntity
		);
		
		/**
		 * add data into repository
		 */
		$newCampaignId = $this->repository->add(
			$newCampaignEntity,
			$this->settings['repositorySettings']['campaign']['table']
		);
		$this->logger->log($newCampaignId, '$newCampaignId');
		
		// auf update befehl vorbereiten
		$newCampaignEntity->setK_id($newCampaignId);
		$newCampaignEntity->_memorizeCleanState();
		
		$newCampaignEntity->setHash(\md5($this->clientEntity->getMandant() . $newCampaignId));
		if ($newCampaignEntity->getNv_id() === 0) {
			$newCampaignEntity->setNv_id($newCampaignId);
		}
		$this->logger->log($newCampaignEntity, '$newCampaignEntity');
		
		/**
		 * update data into repository
		 */
		$campaignUpdateResult = $this->repository->update($newCampaignEntity);
		$this->logger->log($campaignUpdateResult, '$campaignUpdateResult');
		
		if ($actionRequestDataArray['oldAction'] != 'addNv') {
			$newCampaignEntity->_resetModifiedProperties();
			$newCampaignEntity->_memorizeCleanState();
		}
		
		// logData
		$newActionLogId = $this->processLogCampaignActions(
			1,
			$newCampaignEntity,
			0,
			FALSE
		);
		$this->logger->log($newActionLogId, '$newActionLogId');
		
		$this->addSuccessMessages(
			array(
				__FUNCTION__ => 'Kampagne wurde hinzugefügt.'
			)
		);
		
		// processUploadData
		$this->processUploadData(
			$actionRequestDataArray,
			$newCampaignEntity
		);
		
		/**
		 * processRequestsActions
		 */
		$this->processRequestsActions(
			$actionRequestDataArray,
			$newCampaignEntity,
			TRUE
		);
		
		$this->logger->endGroup();
		
		$this->viewContentResultDataArray();
	}
	
	/**
	 * updateAction
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 * @throws \InvalidArgumentException
	 * @throws \LengthException
	 * @throws \DomainException
	 */
	public function updateAction(array $actionRequestDataArray) {
		if (!isset($_POST['campaign'])) {
			throw new \InvalidArgumentException('invalid data!', 1447232162);
		}
		
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1447419556);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1447420047);
		}
		$this->logger->log($campaignEntity, '$campaignEntity -> before');
		
		/**
		 * updateCampaignEntityFromPostDataArray
		 */
		$this->updateCampaignEntityFromPostDataArray(
			$actionRequestDataArray['campaign'],
			$campaignEntity
		);
		$this->logger->log($campaignEntity, '$campaignEntity -> after');
		
		/**
		 * processCampaignContactPersonData
		 */
		$this->processCampaignContactPersonData(
			$actionRequestDataArray['campaign'],
			$actionRequestDataArray['contactPerson'],
			$actionRequestDataArray['customer'],
			$campaignEntity
		);
		
		$useCampaignStartDate = NULL;
		if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
                    if(isset($actionRequestDataArray['campaign']['use_campaign_start_date'])){
			$useCampaignStartDate = FilterUtility::filterData(
				$actionRequestDataArray['campaign']['use_campaign_start_date'],
				FilterUtility::$validateFilterDataArray['number_int']
			);
			$this->logger->log($useCampaignStartDate, '$useCampaignStartDate');
			
			$campaignEntity->setUse_campaign_start_date($useCampaignStartDate);
                    } 
		}
		
		// processUploadData
		$this->processUploadData(
			$actionRequestDataArray,
			$campaignEntity
		);
		
		// processRequestsActions
		$this->processRequestsActions(
			$actionRequestDataArray,
			$campaignEntity,
			TRUE
		);
		
		// processCampaignUnlocked
		$this->processCampaignUnlocked($campaignEntity);
		
		if (!\is_null($useCampaignStartDate) 
			&& (boolean) $useCampaignStartDate !== TRUE
		) {
			// resetUseCampaignStartDate
			$this->resetUseCampaignStartDate($campaignEntity);
		}
		
		$this->logger->endGroup();
		
		$this->viewContentResultDataArray();
	}
	/**
         * 
         * @param array $actionRequestDataArray
         * @throws \LengthException
         * @throws \DomainException
         */
        public function SendTestMailAction(array $actionRequestDataArray){
           
 		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
                
  		$mailingId = isset($actionRequestDataArray['mailingId']) ? $actionRequestDataArray['mailingId'] : 0;
		if (\strlen($mailingId) <= 0) {
			throw new \LengthException('invalid mailing id', 1447419554);
		}
                
               $campaignId = isset($actionRequestDataArray['campaign']['k_id']) ? \intval($actionRequestDataArray['campaign']['k_id']) : 0;
		if ($campaignId <= 0) {
			throw new \LengthException('invalid campaign id', 1447419556);
		}
		
		$campaignEntity = $this->repository->findById($campaignId);
		if (!($campaignEntity instanceof \Modules\Campaign\Domain\Model\CampaignEntity)) {
			throw new \DomainException('invalid campaignEntity', 1447420047);
		}
                
                $deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
                $asps = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
                            switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':  
                            case 'Stellar Reserve':
                            case '4Wave Extra':    
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
                $deliverySystem = DeliverySystem::getAndInitCampaignInstance(
				$asp,
				$deliverySystemDistributorEntity->getClientDeliveryEntity()
			);
			/* @var $deliverySystem \Packages\Api\DeliverySystem\Campaign\AbstractCampaign */
                       
                $recipientListId = $deliverySystemDistributorEntity->getTestlist_distributor_id();
                
                $mandant_id = (int)$deliverySystemDistributorEntity->getClientDeliveryEntity()->getM_id();
                
                switch($mandant_id){
                    case 7:
                        $sendermail = 'news@4wave-mail.de';
                        $replytomail = 'antwort@4wave-mail.de';
                        break;
                    case 6:
                        $sendermail = 'news@stellar-mail.de';
                        $replytomail = 'antwort@news.stellar-mail.de';
                        break;
                    
                    default:
                       $sendermail = 'news@stellar-mail.de';
                        $replytomail = 'antwort@news.stellar-mail.de';
                        
                }
                $recipient = array(
							'email' => $this->loggedUserEntity->getEmail(),
			                                'anrede' => $this->loggedUserEntity->getAnrede(),
							'vorname' => $this->loggedUserEntity->getVorname(),
							'nachname' =>$this->loggedUserEntity->getNachname(),
                                                        'absendername' => $campaignEntity->getAbsendername(),
                                                        'sendermail' => $sendermail,
                                                        'replytomail' => $replytomail
					      ); 
			
				if(\strlen($recipient['email']) > 0
				 ){
					$resultSendTestMail = $deliverySystem->sendTestMail(
						        $mailingId, 
							$recipientListId, 
							$recipient
							);
				 }
                                 
                $this->logger->endGroup();
        }
	
        /**
         * zustandAction
         */
        public function zustandAction(){
            $this->logger->beginGroup(__FUNCTION__);
            
            //Maileon DeliverySystem
           # $deliverySystemDistributorEntityMaileon = $this->systemRepository->findDeliverySystemDistributorById(45);
            $ClientDeliveriesEntityMaileon = $this->systemRepository->findById((int) $_SESSION['mID']);
         
             foreach ($ClientDeliveriesEntityMaileon->getClientDeliveries() as $ClientDeliveryEntity){
                 if($ClientDeliveryEntity->getApi_user() === 'stellar'
                         ||
                         $ClientDeliveryEntity->getApi_user() === '4wave'
                         ){
                 $deliverySystemMaileon = DeliverySystem::getAndInitCampaignInstance(
				$ClientDeliveryEntity->getDeliverySystemEntity()->getAsp(),
				$ClientDeliveryEntity
			);
                  
			
                  $results[$ClientDeliveryEntity->getDeliverySystemEntity()->getAsp()] = $deliverySystemMaileon->getVerteilerZustand(
                                          $this->clientEntity->getId()
                                        );
             }
             }

           $this->initView(
			array(
				'results' => $results, 
	
			)
		);
           
            $this->logger->endGroup();
        }
	/**
	 * updateCampaignEntityFromPostDataArray
	 * 
	 * @param array $campaignDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function updateCampaignEntityFromPostDataArray(array $campaignDataArray, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		// zielgruppe
		$zielgruppeDataArray = $tmpZgDataArray = array();
		if(empty($campaignDataArray['zgKommentar'])){
                    $campaignDataArray['zgKommentar'] = 'kein Kommentar';
                }
                if(empty($campaignDataArray['advertisings_comment'])){
                    $campaignDataArray['advertisings_comment'] = 'kein Kommentar';
                }
		foreach ($campaignDataArray as $key => $item) {
			$campaignEntity->_setProperty(
				$key,
				FilterUtility::filterData(
					$item,
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			);
		}
		
		$mandant = $this->clientEntity->getMandant();
				$new_mandant = str_replace('GmbH', '', $mandant);
		//Betreff muss nicht leer sein
		if(empty($campaignDataArray['betreff'])){
			$campaignDataArray['betreff'] = $new_mandant. ' präsentiert';
		}
		//Absendername muss nicht leer sein
		if(empty($campaignDataArray['absendername'])){
			$campaignDataArray['absendername'] = $new_mandant;
		}
		
		$campaignEntity->setBetreff(
			OthersUtility::cleanupToAndString(
				FilterUtility::filterData(
					$campaignDataArray['betreff'],
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			)
		);
		$campaignEntity->setAbsendername(
			OthersUtility::cleanupToAndString(
				FilterUtility::filterData(
					$campaignDataArray['absendername'],
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			)
		);
		$campaignEntity->setAbrechnungsart(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray[$campaignEntity->getAbrechnungsart()]);
		
		$campaignEntity->setNotiz(FilterUtility::utf8DecodeAndFilterData(
			$campaignDataArray['note'],
			FilterUtility::$validateFilterDataArray['full_special_chars']
		));
		$campaignEntity->setBlacklist(isset($campaignDataArray['bl']) ? $campaignDataArray['bl'] : '');
		$campaignEntity->setDatum($campaignDataArray['date'] . ' ' . $campaignDataArray['versand_hour'] . ':' . $campaignDataArray['versand_min']);
		
		$campaignEntity->setGebucht(FilterUtility::filterData(
			$campaignDataArray['gebucht'],
			FilterUtility::$validateFilterDataArray['number_int']
		));
		$campaignEntity->setVorgabe_m(FilterUtility::filterData(
			$campaignDataArray['vorgabe_m'],
			FilterUtility::$validateFilterDataArray['number_int']
		));
		$campaignEntity->setVorgabe_o(FilterUtility::convertDataToFloatvalue($campaignDataArray['vorgabe_o']));
		$campaignEntity->setVorgabe_k(FilterUtility::convertDataToFloatvalue($campaignDataArray['vorgabe_k']));
		
		if (isset($campaignDataArray['broking_volume'])) {
			$campaignEntity->setBroking_volume(FilterUtility::filterData(
				$campaignDataArray['broking_volume'],
				FilterUtility::$validateFilterDataArray['number_int']
			));
		}
		
		$campaignEntity->setPreis(FilterUtility::convertDataToFloatvalue($campaignDataArray['preis']));
		$campaignEntity->setPreis2(FilterUtility::convertDataToFloatvalue($campaignDataArray['preis2']));
		$campaignEntity->setUnit_price(FilterUtility::convertDataToFloatvalue($campaignDataArray['unit_price']));
		$campaignEntity->setPreis_gesamt(FilterUtility::convertDataToFloatvalue($campaignDataArray['preis_gesamt']));
		$campaignEntity->setPreis_gesamt_verify(FilterUtility::filterData(
			$campaignDataArray['preis_gesamt_verify'],
			FilterUtility::$validateFilterDataArray['number_int']
		));
		$campaignEntity->setBroking_price(FilterUtility::convertDataToFloatvalue($campaignDataArray['broking_price']));
                $campaignEntity->setHybrid_preis(FilterUtility::convertDataToFloatvalue($campaignDataArray['hybrid_preis']));
                $campaignEntity->setHybrid_preis2(FilterUtility::convertDataToFloatvalue($campaignDataArray['hybrid_preis2']));
		switch ($campaignEntity->getAbrechnungsart()) {
			case 'Hybrid':
				$campaignEntity->setPreis_gesamt(0);
				break;

			case 'Festpreis':
				$campaignEntity->setPreis(0);
				$campaignEntity->setPreis2(0);
				$campaignEntity->setUnit_price(0);
				break;

			default:
				$campaignEntity->setPreis2(0);
				$campaignEntity->setPreis_gesamt(0);
				break;
		}
		
		$campaignEntity->setDsd_id(FilterUtility::filterData(
			$campaignDataArray['deliverySystemDistributor'],
			FilterUtility::$validateFilterDataArray['number_int']
		));
		
		/**
		 * zielgruppe
		 */
		$this->processCampaignTargetGroup(
			$campaignDataArray,
			$campaignEntity
		);

	}
	
	/**
	 * resetCampaignSettings
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function resetCampaignSettings(\Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		$campaignEntity->setVersendet(0);
		$campaignEntity->setOpenings(0);
		$campaignEntity->setOpenings_all(0);
		$campaignEntity->setKlicks(0);
		$campaignEntity->setKlicks_all(0);
		$campaignEntity->setLeads(0);
		$campaignEntity->setLeads_u(0);
		$campaignEntity->setStatus(0);
		$campaignEntity->setAdvertising_materials(1); // Optimierung
		$campaignEntity->setPremium_campaign(0);
                $campaignEntity->setAbmelder(0);
	}
	
	/**
	 * processCampaignTargetGroup
	 * 
	 * @param string $campaignDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function processCampaignTargetGroup(array $campaignDataArray , \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
			
		$zielgruppeDataArray = array();
			// genderSelection
		if (isset($campaignDataArray['genderSelection'])) {
			$genderSelection = FilterUtility::filterData(
				$campaignDataArray['genderSelection'],
				FilterUtility::$validateFilterDataArray['number_int']
			);
		
			if ((int) $genderSelection === 0) {
				$genderSelectionValue = 'Damen und Herren';
			} else {
				$genderSelectionValue = CampaignAndCustomerUtility::$genderDataArray[$genderSelection];
			}
				$zielgruppeDataArray[] = $genderSelectionValue;
		}
		
		if ($campaignEntity->getSelection_click_profiles_id() > 0) {
			foreach ($this->clientEntity->getClientClickProfiles() as $clientClickProfilesEntity) {
				/* @var $clientClickProfilesEntity \Packages\Core\Domain\Model\ClientClickProfileEntity */
				if ($clientClickProfilesEntity->getUid() === $campaignEntity->getSelection_click_profiles_id()) {
						$zielgruppeDataArray[] = $clientClickProfilesEntity->getClickProfileEntity()->getShortcut();
					break;
				}
			}
		}
		
	//alter
		$alterDataArray = 'Alter ';
			if (isset($campaignDataArray['alter_von'])
			&& \strlen(\trim($campaignDataArray['alter_von'])) > 0
		) {				
				  $campaignEntity->setAlterVon($campaignDataArray['alter_von']);
				  $alterDataArray .= 'von '.$campaignEntity->getAlterVon(); 

		}else{
			$campaignEntity->setAlterVon(' ');
		} 
		
			if (isset($campaignDataArray['alter_bis']) 
			&& \strlen(\trim($campaignDataArray['alter_bis'])) > 0		
		) {		
			$alter_bis = FilterUtility::filterData(
				$campaignDataArray['alter_bis'],
				FilterUtility::$validateFilterDataArray['special_chars']
			);			     
			      $campaignEntity->setAlterBis($alter_bis);
                  $alterDataArray .= ' bis '.$campaignEntity->getAlterBis();					
		}else{
			$campaignEntity->setAlterBis(' ');
		}
		
		  if(\strlen($alterDataArray) > 7){
		     $zielgruppeDataArray[] = $alterDataArray;
		  }
		 unset($alterDataArray); 
		 
		//PLZ
		$plzDataArray = 'PLZ ';
			if (isset($campaignDataArray['plz_von'])
			&& \strlen(\trim($campaignDataArray['plz_von'])) > 0		
		) {
			$plz_von = FilterUtility::filterData(
				$campaignDataArray['plz_von'],
				FilterUtility::$validateFilterDataArray['special_chars']
			);		
			    $campaignEntity->setPlzVon($plz_von);
				$plzDataArray .= 'von '.$campaignEntity->getPlzVon();
		  }else{
			    $campaignEntity->setPlzVon(' ');
		  }
			
			if (isset($campaignDataArray['plz_bis'])
			&& \strlen(\trim($campaignDataArray['plz_bis'])) > 0		
		) {
			$plz_bis = FilterUtility::filterData(
				$campaignDataArray['plz_bis'],
				FilterUtility::$validateFilterDataArray['special_chars']
			);
			
			   $campaignEntity->setPlzBis($plz_bis);	   
			   $plzDataArray .= ' bis '.$campaignEntity->getPlzBis();		   
		}else{
			   $campaignEntity->setPlzBis(' ');
		}
		  if(\strlen($plzDataArray) > 5){
		    $zielgruppeDataArray[] = $plzDataArray;
		 }
		 unset($plzDataArray);
					 
		//Reagierer
		    if (isset($campaignDataArray['reagierer']) 
			&& \strlen(\trim($campaignDataArray['reagierer'])) > 0
		) {
			$reagierer = FilterUtility::filterData(
				$campaignDataArray['reagierer'],
				FilterUtility::$validateFilterDataArray['special_chars']
			);
			
			    $campaignEntity->setReagierer($reagierer);
			    $zielgruppeDataArray[] = 'Reagierer seit '.$campaignEntity->getReagierer(). ' Tagen';
		}else{
			  $campaignEntity->setReagierer(' ');
		}
		//Kommentar
		 if (isset($campaignDataArray['zgKommentar']) 
			&& \strlen($campaignDataArray['zgKommentar']) > 0
		) {
			$campaignEntity->setZgKommentar(FilterUtility::utf8DecodeAndFilterData(
				$campaignDataArray['zgKommentar'],
				FilterUtility::$validateFilterDataArray['full_special_chars']
			));
			$zielgruppeDataArray[] = $campaignEntity->getZgKommentar();
		}
		
		$campaignEntity->setZielgruppe(\implode(', ', $zielgruppeDataArray));
		unset($zielgruppeDataArray);
	
		$campaignEntity->setAdvertisings_comment(FilterUtility::utf8DecodeAndFilterData(
			$campaignDataArray['advertisings_comment'],
			FilterUtility::$validateFilterDataArray['full_special_chars']
		));
	}
	
	/**
	 * processContactPersonData
	 * 
	 * @param integer $contactPersonId
	 * @param array $contactPersonDataArray
	 * @param mixed $campaignAgentur
	 * @param array $customerDataArray
	 * @return \Modules\Customer\Domain\Model\ContactPersonEntity
	 */
	protected function processContactPersonData($contactPersonId, array $contactPersonDataArray, $campaignAgentur, array $customerDataArray) {
		if ((int) $contactPersonId > 0) {
			$contactPersonEntity = $this->repository->findContactPersonDataById((int) $contactPersonId);
			if ($contactPersonEntity instanceof \Modules\Customer\Domain\Model\ContactPersonEntity) {
				$contactPersonEntity->_resetCleanProperties();
				
				$customerEntity = $this->repository->findCustomerDataById($contactPersonEntity->getKunde_id());
				$customerEntity->_resetCleanProperties();
				
				$contactPersonEntity->setCustomerEntity($customerEntity);
			}
		} else {
			// initialize empty Entity
			$contactPersonEntity = GeneralUtility::makeInstance($this->settings['repositorySettings']['contactPerson']['contactPersonFetchClass']);
			/* @var $contactPersonEntity \Modules\Customer\Domain\Model\ContactPersonEntity */
			$contactPersonEntity->setStatus(1);
			
			$this->updateContactPersonDataArray(
				$contactPersonDataArray,
				$contactPersonEntity
			);
			
			$customerEntity = $this->processCustomerData(
				$campaignAgentur,
				$customerDataArray
			);
			
			$contactPersonEntity->setKunde_id($customerEntity->getKunde_id());
			
			$newContactPersonId = $this->repository->add(
				$contactPersonEntity,
				$this->settings['repositorySettings']['contactPerson']['contactPersonTable']
			);
			
			$contactPersonEntity->setAp_id($newContactPersonId);
			
			$contactPersonEntity->setCustomerEntity($customerEntity);
		}
		
		return $contactPersonEntity;
	}
	
	/**
	 * processCustomerData
	 * 
	 * @param mixed $campaignAgentur
	 * @param array $customerPostDataArray
	 * @return \Modules\Customer\Domain\Model\CustomerEntity
	 */
	protected function processCustomerData($campaignAgentur, array $customerPostDataArray) {
		$company = FilterUtility::filterData(
			$campaignAgentur,
			FilterUtility::$validateFilterDataArray['string']
		);
		
		if (FilterUtility::isInteger($company)) {
			$customerEntity = $this->repository->findCustomerDataById((int) $company);
		} else {
			$customerEntity = $this->repository->findCustomerDataByCompany($company);
			if (!($customerEntity instanceof \Modules\Customer\Domain\Model\CustomerEntity)) {
				// initialize empty Entity
				$customerEntity = GeneralUtility::makeInstance($this->settings['repositorySettings']['customer']['customerFetchClass']);
				/* @var $customerEntity \Modules\Customer\Domain\Model\CustomerEntity */
				$customerEntity->setFirma($company);
				$customerEntity->setStatus(1);

				$this->updateCustomerEntityFromPostDataArray(
					$customerPostDataArray,
					$customerEntity
				);

				$newCustomerId = $this->repository->add(
					$customerEntity,
					$this->settings['repositorySettings']['customer']['customerTable']
				);

				$customerEntity->setKunde_id($newCustomerId);
			}
		}
		$customerEntity->_resetCleanProperties();
		
		return $customerEntity;
	}
	
	/**
	 * processUploadData
	 * 
	 * @param array $actionRequestDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function processUploadData(array $actionRequestDataArray, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		if (isset($actionRequestDataArray['upload']['campaignAdvertisings']) 
			&& \count($actionRequestDataArray['upload']['campaignAdvertisings']) > 0
		) {
			$destinationDir = $this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR;
			FileUtility::createDirectory($destinationDir);
			
			$crdate = new \DateTime();
			foreach ($actionRequestDataArray['upload']['campaignAdvertisings'] as $key => $fileItems) {
				$mimeType = \ucfirst(\strtolower($key));
				
				if (\is_array($fileItems) 
					&& \count($fileItems) > 0
				) {
					foreach ($fileItems as $file) {
						$this->createCampaignAdvertisingAndAddToCampaignEntity(
							$destinationDir,
							$mimeType,
							$file,
							$crdate,
							$campaignEntity,
                                                        TRUE
						);
					}
				} else {
					$this->createCampaignAdvertisingAndAddToCampaignEntity(
						$destinationDir,
						$mimeType,
						$fileItems,
						$crdate,
						$campaignEntity,
                                                TRUE
					);
				}
			}
			unset($crdate);
		}
		
		$this->logger->endGroup();
	}
	
	/**
	 * processRequestsActions
	 * 
	 * @param array $actionRequestDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param boolean $newCampaign
	 * @return void
	 */
	protected function processRequestsActions(array $actionRequestDataArray, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $newCampaign = FALSE) {
		$this->logger->beginGroup(__FUNCTION__);
		
		// TODO: formular manipulationen abfangen (form injection)
		if (isset($actionRequestDataArray['actions']) 
			&& \count($actionRequestDataArray['actions']) > 0
		) {
			if (isset($actionRequestDataArray['actions']['addIntoDeliverySystem']) 
				&& (int) $actionRequestDataArray['actions']['addIntoDeliverySystem'] === 1
			) {
                            
				$this->addCampaignIntoDeliverySystem(
					$campaignEntity,
					$newCampaign
				);
			}
			
			if (isset($actionRequestDataArray['actions']['autoNv']) 
				&& (int) $actionRequestDataArray['actions']['autoNv'] === 1
			) {
				$this->addAutoNvCampaign(
					FilterUtility::filterData(
						$actionRequestDataArray['campaignMailId'],
						FilterUtility::$validateFilterDataArray['string']
					),
					$campaignEntity
				);  
			}
			
			if (isset($actionRequestDataArray['actions']['updateCampaignIntoDeliverySystem']) 
				&& (int) $actionRequestDataArray['actions']['updateCampaignIntoDeliverySystem'] === 1
			) {
				$updateCampaignAdvertisingsContent = FALSE;
				if (isset($actionRequestDataArray['actions']['updateCampaignAdvertisingsContent']) 
					&& (int) $actionRequestDataArray['actions']['updateCampaignAdvertisingsContent'] === 1
				) {
					$updateCampaignAdvertisingsContent = TRUE;
				}
				
				$this->updateCampaignIntoDeliverySystem(
					$campaignEntity,
					$updateCampaignAdvertisingsContent
				);
			}
			
			if (isset($actionRequestDataArray['actions']['sendNotification']['emails']) 
				&& \count($actionRequestDataArray['actions']['sendNotification']['emails']) > 0
			) {
                            
				/*$this->sendNotificationEmails(
					$actionRequestDataArray['actions']['sendNotification'],
					$campaignEntity
				);*/
			}
			
			if (isset($actionRequestDataArray['actions']['sendNotificationEmailToTcm']) 
				&& (int) $actionRequestDataArray['actions']['sendNotificationEmailToTcm'] === 1
			) {
				if ($campaignEntity->getStatus() === 0 
					&& $campaignEntity->getCampaignAdvertisings()->count() > 0
				) {
					$campaignEntity->setStatus(1);
				}
				
				$this->sendNotificationEmailToTcm($campaignEntity);
			}
		}
		
		if ($campaignEntity->_isDirty()) {
			$campaignUpdateResult = $this->repository->update($campaignEntity);
			$this->logger->log($campaignUpdateResult, '$campaignUpdateResult');
			
			// TODO: überarbeiten (logId)
			if ((boolean) $campaignUpdateResult === TRUE) {
				$logId = $this->processLogCampaignActions(
					2,
					$campaignEntity
				);
				$this->logger->log($logId, '$logId');

				$this->addSuccessMessages(
					array(
						__FUNCTION__ => 'Kampagne wurde aktualisiert.'
					)
				);
			}
		}
		
		$this->logger->endGroup();
	}
	
	/**
	 * updateContactPersonDataArray
	 * 
	 * @param array $contactPersonDataArray
	 * @param \Modules\Customer\Domain\Model\ContactPersonEntity $contactPersonEntity
	 * @return void
	 */
	protected function updateContactPersonDataArray(array $contactPersonDataArray, \Modules\Customer\Domain\Model\ContactPersonEntity &$contactPersonEntity) {
		foreach ($contactPersonDataArray as $key => $item) {
			$contactPersonEntity->_setProperty(
				$key,
				FilterUtility::filterData(
					$item,
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			);
		}
		
		$contactPersonGender = FilterUtility::filterData(
			$contactPersonDataArray['anrede'],
			FilterUtility::$validateFilterDataArray['string']
		);
		$contactPersonEntity->setAnrede(($contactPersonGender == 'Herr' ? 'Herr' : 'Frau')); // anders lösen
		
		$contactPersonEntity->setEmail(FilterUtility::filterData(
			$contactPersonDataArray['email'],
			FilterUtility::$validateFilterDataArray['email']
		));
	}
	
	/**
	 * updateCustomerEntityFromPostDataArray
	 * 
	 * @param array $customerDataArray
	 * @param \Modules\Customer\Domain\Model\CustomerEntity $customerEntity
	 * @return void
	 */
	protected function updateCustomerEntityFromPostDataArray(array $customerDataArray, \Modules\Customer\Domain\Model\CustomerEntity &$customerEntity) {
		foreach ($customerDataArray as $key => $item) {
			$customerEntity->_setProperty(
				$key,
				FilterUtility::filterData(
					$item,
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			);
		}
		
		$customerEntity->setWebsite(FilterUtility::filterData(
			$customerDataArray['website'],
			FilterUtility::$validateFilterDataArray['url']
		));
		$customerEntity->setEmail(FilterUtility::filterData(
			$customerDataArray['email'],
			FilterUtility::$validateFilterDataArray['email']
		));
	}
	
	/**
	 * getSalesNameFromCampaignData
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return string
	 */
	protected function getSalesNameFromCampaignData(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$result = '<span style="color:red">- nicht zugewiesen -</span>'; // anders lösen
		
		if ($campaignEntity->getContactPersonEntity() instanceof \Modules\Customer\Domain\Model\ContactPersonEntity) {
			if ($campaignEntity->getContactPersonEntity()->getVertriebler_id() > 0) {
				$salesEntity = $this->userRepository->findById($campaignEntity->getContactPersonEntity()->getVertriebler_id());
			
				if ($salesEntity instanceof \Packages\Core\Domain\Model\UserEntity) {
					/* @var $salesEntity \Packages\Core\Domain\Model\UserEntity */

					$result = $salesEntity->getVorname() . ' ' . $salesEntity->getNachname() 
						. FormUtility::createHiddenField(
							'campaign[vertriebler_id]',
							$salesEntity->getBenutzer_id()
						)
					;
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * processCampaignContactPersonData
	 * 
	 * @param array $campaignDataArray
	 * @param array $contactPersonDataArray
	 * @param array $customerDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function processCampaignContactPersonData(array $campaignDataArray, array $contactPersonDataArray, array $customerDataArray, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		/**
		 * processContactPersonData
		 */
		$contactPersonEntity = $this->processContactPersonData(
			$campaignDataArray['ap'],
			$contactPersonDataArray,
			$campaignDataArray['agentur'],
			$customerDataArray
		);
		$this->logger->log($contactPersonEntity, '$contactPersonEntity');
		
		$campaignEntity->setAp_id($contactPersonEntity->getAp_id());
		$campaignEntity->setContactPersonEntity($contactPersonEntity);
		
		// vertriebler
		$campaignVertriebler = isset($campaignDataArray['vertriebler_id']) ? \intval($campaignDataArray['vertriebler_id']) : 0;
		$contactPersonVertriebler = $contactPersonEntity->getVertriebler_id() > 0 
			? $contactPersonEntity->getVertriebler_id() 
			: \intval($this->loggedUserEntity->getBenutzer_id())
		;
		$campaignEntity->setVertriebler_id($campaignVertriebler ? $campaignVertriebler : $contactPersonVertriebler);
		
		/**
		 * @deprecated
		 */
		$campaignEntity->setAp($contactPersonEntity->getVorname() . ' ' . $contactPersonEntity->getNachname());
		$campaignEntity->setAgentur_id($contactPersonEntity->getCustomerEntity()->getKunde_id());
		$campaignEntity->setAgentur($contactPersonEntity->getCustomerEntity()->getFirma());
		
		$this->logger->endGroup();
	}
	
	
	/**
	 * addCampaignIntoDeliverySystem
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param boolean $newCampaign
	 * @return void
	 */
	protected function addCampaignIntoDeliverySystem(\Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $newCampaign) {
		$this->logger->beginGroup(__FUNCTION__);
		
		if (\strlen($campaignEntity->getMail_id()) === 0) {
			$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
                        $domainObject = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        ); 
                        $domain = $domainObject->getDomain_name();
                  
			$mailingEntity = MailingEntityUtility::createNewMailingEntityForDeliverySystem(
				$campaignEntity,
				$deliverySystemDistributorEntity,
				$this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR,
                                $domain
			);
			$this->logger->log($mailingEntity, '$mailingEntity');

                        $asps = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
                             switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black': 
                            case 'Stellar Reserve':
                            case '4Wave Extra':    
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
			$deliverySystem = DeliverySystem::getAndInitCampaignInstance(
				$asp,
				$deliverySystemDistributorEntity->getClientDeliveryEntity()
			);
			/* @var $deliverySystem \Packages\Api\DeliverySystem\Campaign\AbstractCampaign */
			$deliverySystem->setCharset($campaignEntity->getCharset());

			$newMailingId = $deliverySystem->create(
				$mailingEntity,
				$campaignEntity
			);
			$campaignEntity->setMail_id($newMailingId);
			
			if((int)$this->clientEntity->getId() !== 1){
			   $recipientListId = $deliverySystemDistributorEntity->getTestlist_distributor_id();
			   $recipient = array(
							'email' => $this->loggedUserEntity->getEmail(),
			                                'anrede' => $this->loggedUserEntity->getAnrede(),
							'vorname' => $this->loggedUserEntity->getVorname(),
							'nachname' =>$this->loggedUserEntity->getNachname()
					      ); 
			
				if(\strlen($recipient['email']) > 0
					&& \strlen($recipientListId) > 0
				 ){
					$resultSendTestMail = $deliverySystem->sendTestMail(
						        $newMailingId, 
							$recipientListId, 
							$recipient
							);
				 }
			}
			unset($deliverySystem);

			// TODO: überarbeiten (logId)
			$logId = $this->processLogCampaignActions(
				17,
				$campaignEntity,
				$campaignEntity->getStatus(),
				(($newCampaign) ? FALSE : TRUE)
			);
			$this->logger->log($logId, '$logId');
			
			$this->addSuccessMessages(
				array(
					__FUNCTION__ => 'Kampagne wurde im Versandsystem angelegt.'
				)
			);
		}
		
		$this->logger->endGroup();
	}
	
	/**
	 * addAutoNvCampaign
	 * 
	 * @param string $campaignMailId
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function addAutoNvCampaign($campaignMailId, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		if (\strlen($campaignMailId) > 0
			&& (
				$campaignEntity->getStatus() < 20 
				&& $campaignEntity->getStatus() !== 5
			)
		) {
			$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
			$destinationDir = $this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR;
                        $HvDestinationDir = $this->targetDir . $campaignEntity->getNv_id(). \DIRECTORY_SEPARATOR;  
                       
                      $domainObject = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        ); 
                      $domain = $domainObject->getDomain_name();   
                   
                        if(($campaignEntity->getVersandsystem() === 'M'
                            || $campaignEntity->getVersandsystem() == '4W18'
                            || $campaignEntity->getVersandsystem() == '4WMS'
                            || $campaignEntity->getVersandsystem() == 'S18'
                            || $campaignEntity->getVersandsystem() == 'SB'
                            || $campaignEntity->getVersandsystem() == 'SS'
                            || $campaignEntity->getVersandsystem() == '4WB'   
                            || $campaignEntity->getVersandsystem() == 'SRES'   
                            || $campaignEntity->getVersandsystem() == '4WE'
                                )&& $campaignEntity->getUse_campaign_start_date() === TRUE
                                ){  
                        $quantity = $campaignEntity->getVorgabe_m();
                        if($quantity > '200000'){
                            $quantity = '200000';
                        }
                        $recepientFilter = $this->systemRepository->findRecipientFilterId(
                                                            $campaignEntity->getDsd_id(),
                                                            $quantity
                                         );   
                        }
                          
                       $asps = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
                           switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black':  
                            case 'Stellar Reserve':
                            case '4Wave Extra':    
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
			$deliverySystem = DeliverySystem::getAndInitCampaignInstance(
				$asp,
				$deliverySystemDistributorEntity->getClientDeliveryEntity()
			);
			/* @var $deliverySystem \Packages\Api\DeliverySystem\Campaign\AbstractCampaign */
			$deliverySystem->setCharset($campaignEntity->getCharset());
			  
			$newMailingId = $deliverySystem->autoNvMailings($campaignMailId, $campaignEntity);
			$this->logger->log($newMailingId, '$newMailingId');
                        
                         $mandant_id = (int)$deliverySystemDistributorEntity->getClientDeliveryEntity()->getM_id();
                
                        switch($mandant_id){
                            case 7:
                                $sendermail = 'news@4wave-mail.de';
                                $replytomail = 'antwort@4wave-mail.de';
                                break;
                            case 6:
                                $sendermail = 'news@stellar-mail.de';
                                $replytomail = 'antwort@news.stellar-mail.de';
                                break;
                             case 17:
                                $sendermail = 'mail@stp-newsl.de';
                                $replytomail = 'antwort@ihr-stp-newsl.de';
                                break;
                            case 18:
                                $sendermail = 'service@four-wave.de';
                                $replytomail = 'antwort@four-wave.de';
                                break;

                            default:
                               $sendermail = 'news@stellar-mail.de';
                                $replytomail = 'antwort@news.stellar-mail.de';

                        }
	                // fuer Send Effect, weil keine kopieren möglich ist
                        if ((int) $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId() === 7 
                                || (int) $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId() === 17
                                || (int) $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId() === 18
                                )
		            {                               
                                   $crdate = new \DateTime();        
                                   $campaignAdvertisings = $this->repository->getCampaignAdvertisingsByCampaignId($campaignEntity->getNv_id());                             
                                   FileUtility::createDirectory($destinationDir);
                                   foreach ($campaignAdvertisings as $campaignAdvertising){
                                        $this->createCampaignAdvertisingAndAddToCampaignEntity(
							$destinationDir,
							$campaignAdvertising->getMime_type(),
							$campaignAdvertising->getFile(),
							$crdate,
							$campaignEntity,
                                                        FALSE
						);
                                   }
                                  
                                   
                                    $mailingEntity = MailingEntityUtility::createNewMailingEntityForDeliverySystem(
                                             $campaignEntity,
                                             $deliverySystemDistributorEntity,
                                             $HvDestinationDir,
                                            ''
                                        );
                                    $apiUser = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getApi_user();
                                    switch ($apiUser) {
                                        case '4wave':
                                        $mailingEntity->setSendermail('news@4wave-mail.de');
                                        $mailingEntity->setReplytomail('antwort@4wave-mail.de');
                                            break;
                                         case '4wave2':
                                        $mailingEntity->setSendermail('service@four-wave.de');
                                        $mailingEntity->setReplytomail('antwort@four-wave.de');
                                            break;
                                         case 'stellar':
                                        $mailingEntity->setSendermail('news@4wave-mail.de');
                                        $mailingEntity->setReplytomail('antwort@4wave-mail.de');
                                            break;
                                         case 'stellar2':
                                        $mailingEntity->setSendermail('mail@stp-newsl.de');
                                        $mailingEntity->setReplytomail('antwort@news.stellar-mail.de');
                                            break;
                                        default:
                                        $mailingEntity->setSendermail('mail@stp-newsl.de');
                                        $mailingEntity->setReplytomail('antwort@news.stellar-mail.de');
                                            break;
                                    }
  
                                     $MailingId = $deliverySystem->create(
                                        $mailingEntity, 
                                        $campaignEntity
                                        );
                                     $newMailingId = strval($MailingId);
	          } 
			// updateCampaignEntities
			$campaignEntity->setMail_id($newMailingId);
			
                        // resetCampaignAdvertisings
		        $campaignEntity->setCampaignAdvertisings(new \Packages\Core\Persistence\ObjectStorage());
                        
			// updateDeliverySystemData
			$mailingEntity = MailingEntityUtility::updateMailingEntityForDeliverySystem(
				$campaignEntity,
				$deliverySystemDistributorEntity,
				$destinationDir,
				FALSE,
                                $domain
			);
                        if( isset($recepientFilter)                                
                              &&  \count($recepientFilter >0)){
                            foreach($recepientFilter as $recepientFilterObject){
                              $mailingEntity->setRecipientFilterIds($recepientFilterObject->getRecipient_filter_id());
                                }
                             }
			$mailingEntity->setTitle($campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . ', ' . $campaignEntity->getK_name());
			$this->logger->log($mailingEntity, '$mailingEntity'); 
			// workaround für kajomi, weil es kein update möglichkeit gibt
                             $domainObject = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        ); 
                        $domain = $domainObject->getDomain_name();
			$newMailingEntity = MailingEntityUtility::createNewMailingEntityForDeliverySystem(
				$campaignEntity,
				$deliverySystemDistributorEntity,
				$destinationDir,
                                $domain
			);
			$this->logger->log($newMailingEntity, '$newMailingEntity');
			
			// memorizeCampaignCleanState
			$campaignEntity->setMail_id(NULL); // resetMailId
			$campaignEntity->_memorizeCleanState();
			
			// updateCampaignEntities
			$campaignEntity->setMail_id($newMailingId);
			$campaignEntity->setStatus(1);

			if ($campaignEntity->getUse_campaign_start_date() === TRUE) {
				$mailingEntity->setUseScheduleDate(TRUE);
				$newMailingEntity->setUseScheduleDate(TRUE);
				$campaignEntity->setStatus(13); // programiert
                                $deliverySystemSendResult = $deliverySystem->send(
                                        $mailingEntity, 
                                        $campaignEntity,
                                        $sendermail,
                                        $replytomail
                                        );
			}
			  if ((int) $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getId() !== 7 )
		            {
			// update campaign 
			$deliverySystemUpdateResult = $deliverySystem->update(
				$mailingEntity,
				$newMailingEntity,
				$campaignEntity
			);
			$this->logger->log($deliverySystemUpdateResult, '$deliverySystemUpdateResult');
			unset($deliverySystem);
                            }
			// campaign update
			$campaignUpdateResult = $this->repository->update($campaignEntity);
			$this->logger->log($campaignUpdateResult, '$campaignUpdateResult');
			$campaignEntity->_memorizeCleanState();
			
			// TODO: überarbeiten (logId)
			$logId = $this->processLogCampaignActions(
				17,
				$campaignEntity,
				$campaignEntity->getStatus()
			);
			$this->logger->log($logId, '$logId');
			
			$this->addSuccessMessages(
				array(
					__FUNCTION__ => 'NV Kampagne wurde im Versandsystem angelegt.'
				)
			);
		}
		
		$this->logger->endGroup();
	}
	
	/**
	 * updateCampaignIntoDeliverySystem
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param boolean $updateCampaignAdvertisingsContent
	 * @return void
	 */
	protected function updateCampaignIntoDeliverySystem(\Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $updateCampaignAdvertisingsContent) {
		$this->logger->beginGroup(__FUNCTION__);
		
		if (\strlen($campaignEntity->getMail_id()) > 0
			&& (
				$campaignEntity->getStatus() < 20 
				&& $campaignEntity->getStatus() !== 5
			)
		) {
                    $destinationDir = $this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR;
                    $deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById($campaignEntity->getDsd_id());
                    
                    $domainObject = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        ); 
                   $domain = $domainObject->getDomain_name();     
  
			$mailingEntity = MailingEntityUtility::updateMailingEntityForDeliverySystem(
				$campaignEntity,
				$deliverySystemDistributorEntity,
				$destinationDir,
				$updateCampaignAdvertisingsContent,
                                $domain
			);
			$this->logger->log($mailingEntity, '$mailingEntity');
                  
			$mailingEntityState = $mailingEntity->_isDirty();
			$this->logger->log($mailingEntityState, '$mailingEntityState');

                        $campaignClickState = $campaignEntity->_isDirty('campaign_click_profiles_id');
			$this->logger->log($campaignClickState, '$campaignClickState');
                        
			$campaignDsdIdState = $campaignEntity->_isDirty('dsd_id');
			$this->logger->log($campaignDsdIdState, '$campaignDsdIdState');
			
			$updateMailingEntityDate = FALSE;
			
			// nur für nv
			if ($campaignEntity->getK_id() !== $campaignEntity->getNv_id()) {
				/**
				 * bei änderungen von useCampaignStartDate bzw. versanddatum
				 */
				$dbDatum = $campaignEntity->_getCleanProperty('datum');
				if (!($dbDatum instanceof \DateTime)) {
					$dbDatum = new \DateTime($dbDatum);
				}
				
				if ($campaignEntity->getUse_campaign_start_date() !== (boolean) $campaignEntity->_getCleanProperty('use_campaign_start_date') 
					|| $campaignEntity->getDatum()->format('U') <> $dbDatum->format('U')
				) {
					$updateMailingEntityDate = TRUE;
					if ($campaignEntity->getUse_campaign_start_date() === TRUE) {
                                                $mailingEntity->setScheduleDate($campaignEntity->getDatum());
						$mailingEntity->setUseScheduleDate(TRUE);
						$campaignEntity->setStatus(13); // programiert
					}
				}
			}

			// TODO: refactoring
			if ((boolean) $mailingEntityState === TRUE 
				|| (boolean) $campaignDsdIdState === TRUE
				|| (boolean) $updateMailingEntityDate === TRUE
                                || (boolean) $campaignClickState === TRUE
			) {
                       
				// workaround für kajomi, weil es kein update möglichkeit gibt
				$newMailingEntity = MailingEntityUtility::createNewMailingEntityForDeliverySystem(
					$campaignEntity,
					$deliverySystemDistributorEntity,
					$destinationDir,
                                        $domain
				);
				$newMailingEntity->setUseScheduleDate($mailingEntity->getUseScheduleDate());
				$this->logger->log($newMailingEntity, '$newMailingEntity');
		     $asps = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
                           switch ($asps) {
                            case '4Wave 2018':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Marketing Sandbox':
                            case 'Stellar 2018':
                            case 'Stellar Basis':
                            case 'Stellarperformance Sandbox':
                            case '4Wave Basis':
                            case '4Wave Black':
                            case 'Stellar Black': 
                            case 'Stellar Reserve':
                            case '4Wave Extra':    
                            case 'Maileon':
                              $asp = 'Maileon';
                                break;
                            case 'Sendeffect':
                            case 'Stellar SE':
                            case '4Wave SE':    
                              $asp = 'Sendeffect';
                                break;                          
                            default:
                             $asp = 'Maileon';
                                break;
                        }
				$deliverySystem = DeliverySystem::getAndInitCampaignInstance(
					$asp,
					$deliverySystemDistributorEntity->getClientDeliveryEntity()
				);
				/* @var $deliverySystem \Packages\Api\DeliverySystem\Campaign\AbstractCampaign */
				$deliverySystem->setCharset($campaignEntity->getCharset());
				
				$deliverySystemUpdateResult = $deliverySystem->update(
					$mailingEntity,
					$newMailingEntity,
					$campaignEntity
				);
				$this->logger->log($deliverySystemUpdateResult, '$deliverySystemUpdateResult');
				
				// autoNv löschen 
				if ($updateMailingEntityDate 
					&& $campaignEntity->getUse_campaign_start_date() !== TRUE
                                        && $campaignEntity->getStatus() === 13
				) {
					
					$deliverySystem->resetShippingDate($campaignEntity->getMail_id());
                                         $campaignEntity->setStatus(1);

				}
				unset($deliverySystem);
                        }
                       
				// TODO: überarbeiten (logId)
				$logId = $this->processLogCampaignActions(
					12,
					$campaignEntity,
					$campaignEntity->getStatus()
				);
				$this->logger->log($logId, '$logId');
				
				$this->addSuccessMessages(
					array(
						__FUNCTION__ => 'Kampagne wurde im Versandsystem aktualisiert.'
					)
				);
			
		}
		
		$this->logger->endGroup();
	}
	
	/**
	 * sendNotificationEmails
	 * 
	 * @param array $sendNotificationDataArray
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function sendNotificationEmails(array $sendNotificationDataArray, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		$notificationNotes = '';
		if (\strlen($sendNotificationDataArray['notes']) > 0) {
			$notificationNotes = FilterUtility::filterData(
				$sendNotificationDataArray['notes'],
				FilterUtility::$validateFilterDataArray['special_chars']
			);
			
			$notificationNotes = OthersUtility::cleanUpNotes($notificationNotes) . \chr(13) . \chr(13);
		}
		$domain = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        );
		$subject = 'Neues Mailing [' . $campaignEntity->getK_name() . ']';
		$messageBody = 'Ein neues Mailing wurde im EKM angelegt:';
		if ($campaignEntity->getK_id() > 0) {
			$subject = 'Mailing [' . $campaignEntity->getK_name() . ']';
			$messageBody = 'Mailing wurde im EKM aktualisiert:';
		}
		$messageBody .= $this->createDefaultInfoMailContent($campaignEntity,$domain->getDomain_name()) 
			. $notificationNotes
		;
		
		$failedRecipients = $this->sendMail(
			\array_unique($sendNotificationDataArray['emails']),
			$subject,
			$messageBody
		);
		
		$logId = $this->processLogCampaignActions(
			8,
			$campaignEntity,
			0,
			FALSE
		);
		$this->logger->log($logId, '$logId');
		
		$this->logger->endGroup();
		
		if (\count($failedRecipients) > 0) {
			$this->addErrorMessages(
				array(
					__FUNCTION__ => 'Die Email konnte an folgende Addressen nicht gesendet werden:' . \implode(\chr(13), $failedRecipients)
				)
			);
		} else {
			$this->addSuccessMessages(
				array(
					__FUNCTION__ => 'Die Email wurde versandt'
				)
			);
		}
	}
	
	/**
	 * sendNotificationEmailToTcm
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function sendNotificationEmailToTcm(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		$notificationNotes = '';
		$additionalCampaignAdvertisingsNote = '';
		$campaignTargetDir = $this->targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR;
		$attachmentDataArray = array();
		$domain = $this->systemRepository->findDeliverySystemDomainById(
                         $campaignEntity->getDasp_id()
                        );
		if (\strlen($campaignEntity->getAdvertisings_comment()) > 0) {
			$notificationNotes = OthersUtility::cleanUpNotes(
				FilterUtility::filterData(
					$campaignEntity->getAdvertisings_comment(),
					FilterUtility::$validateFilterDataArray['special_chars']
				)
			) . \chr(13) . \chr(13);
		}
		
		$subject = 'Neues Werbemittel für [' . $campaignEntity->getK_name() . ']';
		$messageBody = 'Ein neues Werbemittel wurde im EKM hinzugefügt:';
		$messageBody .= $this->createDefaultInfoMailContent($campaignEntity,$domain->getDomain_name()) 
			. $notificationNotes
			. '========================================' . \chr(13) 
		;
		
		if ($campaignEntity->getCampaignAdvertisings()->count() > 0) {
			foreach ($campaignEntity->getCampaignAdvertisings() as $campaignAdvertisingsEntity) {
				/* @var $campaignAdvertisingsEntity \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
				$attachmentDataArray[] = $campaignTargetDir . $campaignAdvertisingsEntity->getFile();

				if ($campaignAdvertisingsEntity->getCrdate()->format('Ymd') === $this->dateNow->format('Ymd')) {
					$userEntity = SystemHelperUtility::getUserEntityFromDataArray(
						$this->userDataArray,
						$campaignAdvertisingsEntity->getUser_id()
					);
					if ($userEntity instanceof \Packages\Core\Domain\Model\UserEntity) {
						$userName = $userEntity->getVorname() . ' ' . $userEntity->getNachname();
					} else {
						$userName = 'Nicht mehr vorhanden!';
					}

					$additionalCampaignAdvertisingsNotes = array(
						'Datum/Uhrzeit: ' . $campaignAdvertisingsEntity->getCrdate()->format('d.m.Y H:i:s'),
						'Benutzer: ' . $userName,
						'Dateiname: ' . $campaignAdvertisingsEntity->getFile()
					);

					$additionalCampaignAdvertisingsNote .= \implode(\chr(9), $additionalCampaignAdvertisingsNotes). \chr(13);
				}
			}
		}
		
		if (\strlen($additionalCampaignAdvertisingsNote) > 0) {
			$messageBody .= 'Zuletz hochgeladen:' . \chr(13) 
				. $additionalCampaignAdvertisingsNote
			;
		} else {
			$messageBody .= 'Aktualisierung der Kampagne wurde ausgelöst ohne Datei-Upload – Bitte Kampagne im EKM prüfen';
		}
		
		$failedRecipients = $this->sendMail(
			MailUtility::$technikContactDataArray,
			$subject,
			$messageBody,
			$attachmentDataArray
		);
             $result = TrelloControler::create($campaignEntity->getK_name(), $campaignEntity->getK_id());
             $this->processCampaignTrello($campaignEntity, $result['id']);
		$this->logger->endGroup();
		
		if (\count($failedRecipients) > 0) {
			$this->addErrorMessages(
				array(
					__FUNCTION__ => 'Die Email konnte an folgende Addressen nicht gesendet werden:' . \implode(\chr(13), $failedRecipients)
				)
			);
		} else {
			$this->addSuccessMessages(
				array(
					__FUNCTION__ => 'Die Email wurde an TCM versandt.'
				)
			);
		}
	}
	
	/**
	 * processLogCampaignActions
	 * 
	 * @param integer $actionId
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param integer $campaignStatus
	 * @param boolean $addLogData
	 * @return integer
	 */
	protected function processLogCampaignActions($actionId, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, $campaignStatus = 0, $addLogData = TRUE) {
		if ($addLogData === TRUE) {
			$logDataCampaignEntity = $campaignEntity->_getCleanProperties();
			
			$campaignAdvertisings = array();
			if (isset($logDataCampaignEntity['campaignAdvertisings'])) {
				$campaignAdvertisingsItems = $logDataCampaignEntity['campaignAdvertisings'];
				/* @var $campaignAdvertisingsItems \Packages\Core\Persistence\ObjectStorage */
				if ($campaignAdvertisingsItems->count() > 0) {
					foreach ($campaignAdvertisingsItems as $campaignAdvertising) {
						/* @var $campaignAdvertising \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
						$campaignAdvertisings[] = $campaignAdvertising->_getCleanProperties();
					}
				}
				
				unset($logDataCampaignEntity['campaignAdvertisings']);
				
				$logDataCampaignEntity['campaignAdvertisings'] = $campaignAdvertisings;
				unset($campaignAdvertisingsItems);
			}
			
			if (isset($logDataCampaignEntity['contactPersonEntity'])) {
				$contactPersonEntity = $logDataCampaignEntity['contactPersonEntity'];
				/* @var $contactPersonEntity \Modules\Customer\Domain\Model\ContactPersonEntity */
				
				$customerEntity = $contactPersonEntity->getCustomerEntity();
				/* @var $customerEntity \Modules\Customer\Domain\Model\CustomerEntity */
				
				unset($logDataCampaignEntity['contactPersonEntity']);
				$logDataCampaignEntity['contactPersonEntity'] = $contactPersonEntity->_getCleanProperties();
				$logDataCampaignEntity['contactPersonEntity']['customerEntity'] = $customerEntity->_getCleanProperties();
				unset($contactPersonEntity, $customerEntity);
			}
			$logData = \serialize($logDataCampaignEntity);
			unset($logDataCampaignEntity);
		} else {
			$logData = NULL;
		}
		
		$newLogEntity = GeneralUtility::makeInstance($this->settings['repositorySettings']['log']['logFetchClass']);
		/* @var $newLogEntity \Modules\Campaign\Domain\Model\LogEntity */
		$newLogEntity->setUserid($this->loggedUserEntity->getBenutzer_id());
		$newLogEntity->setAction($actionId);
		$newLogEntity->setKid($campaignEntity->getK_id());
		$newLogEntity->setTimestamp(new \DateTime());
		$newLogEntity->setStatus(((int) $campaignStatus > 0 ? (int) $campaignStatus : $campaignEntity->getStatus()));
		$newLogEntity->setLog_data($logData);
		
		return $this->repository->add(
			$newLogEntity,
			$this->settings['repositorySettings']['log']['logTable']
		);
	}
	
	/**
	 * processCampaignLooked
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return string
	 */
	protected function processCampaignLooked(\Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity) {
		$this->logger->beginGroup(__FUNCTION__);
		
		$result = '';

		$campaignEntity->_memorizeCleanState();
		
		if (\strlen($campaignEntity->getEdit_lock()) > 0 
			&& $campaignEntity->getEdit_lock() != $this->loggedUserEntity->getVorname() . ' ' . $this->loggedUserEntity->getNachname()
		) {
			$result = '
				<div style="color:#FFFFFF; background:red; width:327px; padding:3px; text-align:center; font-size:10px;">' 
					. 'Dieses Mailing wird bereits von <b>' . $campaignEntity->getEdit_lock() . '</b> bearbeitet.' 
				. '</div>'
			;
		} else {
			$campaignEntity->setEdit_lock($this->loggedUserEntity->getVorname() . ' ' . $this->loggedUserEntity->getNachname());
			$this->logger->log(TRUE, 'editLook');
		}
		
		if ($campaignEntity->_isDirty()) {
			$campaignUpdateResult = $this->repository->update($campaignEntity);
			$this->logger->log($campaignUpdateResult, '$campaignUpdateResult');
		}
		
		$this->logger->endGroup();
		
		return $result;
	}
	
	/**
	 * processCampaignUnlocked
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function processCampaignUnlocked(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['campaign']['table'])
			. ' SET ' . Query::masketField('edit_lock') . Query::$fieldEqual . $this->repository->quoteString('')
			. ' WHERE ' . Query::masketField($this->settings['repositorySettings']['campaign']['primaryField']) . Query::$fieldEqual . $campaignEntity->getK_id()
		;
		
		$result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);
	}
        /**
	 * processCampaignTrello
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
         * @param string $trelloId 
	 * @return void
	 */
	public function processCampaignTrello(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity,$trelloId) {
		$query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['campaign']['table'])
			. ' SET ' . Query::masketField('trello_id') . Query::$fieldEqual . $this->repository->quoteString($trelloId)
			. ' WHERE ' . Query::masketField($this->settings['repositorySettings']['campaign']['primaryField']) . Query::$fieldEqual . $campaignEntity->getK_id()
		;
		$result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);
	}
	
	/**
	 * resetUseCampaignStartDate
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return void
	 */
	protected function resetUseCampaignStartDate(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['campaign']['table'])
			. ' SET ' . Query::masketField('use_campaign_start_date') . Query::$fieldEqual . $this->repository->quoteString(0)
			. ' WHERE ' . Query::masketField($this->settings['repositorySettings']['campaign']['primaryField']) . Query::$fieldEqual . $campaignEntity->getK_id()
		;
		
		$result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);
	}
	
	/**
	 * sendMail
	 * 
	 * @param array $toDataArray
	 * @param string $subject
	 * @param string $messageBody
	 * @param array $attachmentDataArray
	 * @return array
	 */
	protected function sendMail(array $toDataArray, $subject, $messageBody, array $attachmentDataArray = array()) {
		$this->logger->beginGroup(__FUNCTION__);
		
		$swiftMessage = $this->mailEngine->createMessageObject();
		/* @var $swiftMessage \Swift_Message */
		
		$swiftMessage->setBody($messageBody)
			->setSubject($subject)
		;
		
		// im DEV-System email nur an die dev Email senden (MailUtility::$devEmailAddress)
		if (GeneralUtility::isDevSystem()) {
			$swiftMessage
				->setTo(MailUtility::$devEmailAddress)
			;
		} else {
			$swiftMessage->setTo($toDataArray);
		}
		
		if (\count($attachmentDataArray) > 0) {
			foreach ($attachmentDataArray as $fileItem) {
				$swiftMessage->attach(\Swift_Attachment::fromPath($fileItem));
			}
		}
		
		$mailerSendResults = $this->mailEngine->sendMessage($swiftMessage);
		$this->logger->log($mailerSendResults, '$mailerSendResults');
		
		$failedRecipients = $this->mailEngine->getFailedRecipients();
		$this->logger->log($failedRecipients, '$failedRecipients');
		$this->mailEngine->resetFailedRecipients();
		
		$this->logger->endGroup();
		
		return $failedRecipients;
	}
	
	/**
	 * createDefaultInfoMailContent
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
         * @param string $domain
	 * @return string
	 */
	protected function createDefaultInfoMailContent(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, $domain) {
		$messageBody = \chr(13) . \chr(13)
			. '========================================' . \chr(13)
			. 'Mandant: ' . $this->clientEntity->getMandant() . \chr(13)
			. 'Agentur: ' . $campaignEntity->getContactPersonEntity()->getCustomerEntity()->getFirma() . \chr(13)
			. 'KampagneId: ' . $campaignEntity->getK_id() . \chr(13)
			. 'Kampagne: ' . $campaignEntity->getK_name() . \chr(13)
			. 'Versanddatum: ' . $campaignEntity->getDatum()->format('d.m.Y') . \chr(13)
			. 'Uhrzeit: ' . $campaignEntity->getDatum()->format('H:i') . ' Uhr' . \chr(13)
                        . 'Versanddomain: ' . $domain. \chr(13)
		;
		
		if (\strlen($campaignEntity->getSender_email()) > 0) {
			$messageBody .= 'Absenderadresse: ' . $campaignEntity->getSender_email() . \chr(13);
		}
		
		if (\strlen($campaignEntity->getAbsendername()) > 0) {
			$messageBody .= 'Absendername: ' . $campaignEntity->getAbsendername() . \chr(13);
		}
		
		if (\strlen($campaignEntity->getBetreff()) > 0) {
			$messageBody .= 'Betreff: ' . $campaignEntity->getBetreff() . \chr(13);
		}
		
		$messageBody .= '========================================' . \chr(13) . \chr(13);
		
		return $messageBody;
	}
	
	/**
	 * createCampaignAdvertisingAndAddToCampaignEntity
	 * 
	 * @param string $destinationDir
	 * @param string $mimeType
	 * @param string $file
	 * @param \DateTime $crdate
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
         * @param boolean $move
	 * @return void
	 */
	protected function createCampaignAdvertisingAndAddToCampaignEntity($destinationDir, $mimeType, $file, \DateTime $crdate, \Modules\Campaign\Domain\Model\CampaignEntity &$campaignEntity, $move) {
		$newCampaignAdvertising = GeneralUtility::makeInstance($this->settings['repositorySettings']['campaignAdvertising']['campaignAdvertisingFetchClass']);
		/* @var $newCampaignAdvertising \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
		$newCampaignAdvertising->setCampaign_id($campaignEntity->getK_id());
		$newCampaignAdvertising->setMime_type($mimeType);
		$newCampaignAdvertising->setFile($file);
		$newCampaignAdvertising->setCrdate($crdate);
		$newCampaignAdvertising->setUser_id($this->loggedUserEntity->getBenutzer_id());
		
		$campaignAdvertisingId = $this->repository->add(
			$newCampaignAdvertising,
			$this->settings['repositorySettings']['campaignAdvertising']['campaignAdvertisingTable']
		);
		$newCampaignAdvertising->setId($campaignAdvertisingId);
		$newCampaignAdvertising->_memorizeCleanState();
		
		$campaignEntity->addCampaignAdvertisings($newCampaignAdvertising);
                if($move === TRUE){
		FileUtility::moveFile(
			$this->targetDir . $file,
			$destinationDir . $file
		 );
                }else{
                    $HvDestinationDir = $this->targetDir . $campaignEntity->getNv_id(). \DIRECTORY_SEPARATOR;
                    FileUtility::copyFile(
			$HvDestinationDir . $file,
			$destinationDir . $file
		 );
                }
	}
}