<?php
namespace Modules\Campaign\Controller;


/**
 * Description of AbstractController
 *
 * @author Cristian.Reus
 */
abstract class AbstractController extends \Packages\Core\Mvc\Controller\AbstractController {
	
	/**
	 * repository
	 * 
	 * @var \Modules\Campaign\Domain\Repository\CampaignRepository
	 */
	protected $repository = NULL;
	
	/**
	 * userRepository
	 * 
	 * @var \Packages\Core\Domain\Repository\UserRepository
	 */
	protected $userRepository = NULL;
	
	/**
	 * targetDir
	 * 
	 * @var string
	 */
	protected $targetDir = NULL;
	
	
	
	
	/**
	 * postInitAction
	 * 
	 * @return void
	 */
	protected function postInitAction() {
		$this->targetDir = \DIR_Uploads . $this->clientEntity->getAbkz() . \DIRECTORY_SEPARATOR;
	}
	
	/**
	 * initDefaultViewDataArray
	 * 
	 * @param array $actionRequestDataArray
	 * @return void
	 */
	protected function initDefaultViewDataArray(array $actionRequestDataArray) {
		$this->defaultViewDataArray['actionRequestDataArray'] = $actionRequestDataArray;
		$this->defaultViewDataArray['targetDir'] = $this->targetDir;
		$this->defaultViewDataArray['clientEntity'] = $this->clientEntity;
		$this->defaultViewDataArray['loggedUserEntity'] = $this->loggedUserEntity;
		$this->defaultViewDataArray['settingsDataArray'] = $this->settings;
	}

}