<?php
use Packages\Core\Utility\Format\NumberUtility;


/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */

/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
?>
<div id="tab1">
	<input type="hidden" id="mailingTyp" name="mailingTyp" value="<?php echo $mailing; ?>" />

	<?php echo $campaignIsLock; ?>

	<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2" align="right">
				<span class="required">*</span><span style="color:#666666;font-size:11px"> = Pflichtfelder</span>
			</td>
		</tr>
		<tr>
			<td align="right" class="info">Werbemittel:</td>
			<td align="left">
				<div class="floatLeft">
					<select name="campaign[advertising_materials]" id="campaign_advertising_materials" style="width: 170px;">
						<?php echo $campaignAdvertisingMaterialsOptions; ?>
					</select>
				</div>
				<div class="floatLeft spacerLeftWrap" style="margin-top: 5px;">
					<input type="checkbox" name="campaign[premium_campaign]" id="campaign_premium_campaign" value="1" <?php echo $campaignEntity->getPremium_campaign() == 1 ? 'checked="checked"' : ''; ?> /> 
					<label for="campaign_premium_campaign" class="widthAuto">Premium Kampagne</label>
				</div><br class="clearBoth" />
			</td>
		</tr>
		<tr>
			<td align="right" class="info">Kunde/Agentur: <span class="required">*</span></td>
			<td align="left">
				<?php
					require_once($partialViewPath . $mailing . \DIRECTORY_SEPARATOR . 'customer.php');
				?>
			</td>
		</tr>

		<?php
			require_once($partialViewPath . $mailing . \DIRECTORY_SEPARATOR . 'contactPerson.php');
		?>

		<tr>
			<td align="right" class="info">Kampagne: <span class="required">*</span></td>
			<td align="left">
				<input type="text" name="campaign[k_name]" id="k_name" value="<?php echo $campaignEntity->getK_name(); ?>" <?php echo $readOnly; ?> />
			</td>
		</tr>
		<tr>
			<td align="right" class="info"><span id="v_datum">Versanddatum</span>: <span class="required">*</span></td>
			<td align="left">
				<input type="hidden" id="date" name="campaign[date]" value="<?php echo $campaignDate; ?>" <?php echo $readOnly; ?> />
				<input type="hidden" id="nTag" value="<?php echo $campaignDay; ?>" <?php echo $readOnly; ?> />
				<input type="hidden" id="nMonat" value="<?php echo $campaignMonth; ?>" <?php echo $readOnly; ?> />
				<input type="hidden" id="nJahr" value="<?php echo $campaignYear; ?>" <?php echo $readOnly; ?> />

				<div class="field floatLeft" id="datefields">
					<select id="month" name="campaign[month]">
						<?php echo $campaignMonthOptions; ?>
					</select>
					<select id="day" name="campaign[day]">
						<?php echo $campaignDayOptions; ?>
					</select>
					<input type="text" id="year" name="campaign[year]" value="" />
				</div>
				<div style="margin-left: 10px;" class="floatLeft">
					<select name="campaign[versand_hour]" id="campaign_versandHour" class="widthAuto" onchange="validateAndCheckNvCampaignDate('<?php echo $mailing; ?>')" <?php echo $disabledCampaignDate; ?> >
						<?php echo $campaignHourOptions; ?>
					</select> <b>:</b>
					<select name="campaign[versand_min]" id="campaign_versandMin" style="margin-left: 2px;" class="widthAuto" onchange="validateAndCheckNvCampaignDate('<?php echo $mailing; ?>');" <?php echo $disabledCampaignDate; ?> >
						<?php echo $campaignMinutesOptions; ?>
					</select> Uhr
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" class="info">Kampagnen-Klickprofil: <span class="required">*</span></td>
			<td align="left">
				<select name="campaign[campaign_click_profiles_id]" id="campaign_click_profiles_id">
					<option value="">- Bitte ausw&auml;hlen -</option>
					<?php echo $campaignClickProfilesOptions; ?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" class="info">Gebuchtes Volumen:</td>
			<td align="left">
				<input onkeyup="calcNewUSQ(this.value, <?php echo $settingsDataArray['moduleSettings']['cmVorgabeUsq']; ?>);" onchange="createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');"  name="campaign[gebucht]" id="volumen" type="text" value="<?php echo NumberUtility::numberFormat($campaignEntity->getGebucht()); ?>" />
			</td>
		</tr>
		<tr>
			<td align="right" class="info" title="zu versendendes Volumen">Zu versend. Volumen:  <span class="required">*</span></td>
			<td align="left"> 
                                <select name="campaign[vorgabe_m]" id="vorgabe_m" style="width: 130px" onchange="calcNewUSQ2(this.value, <?php echo $settingsDataArray['moduleSettings']['cmVorgabeUsq']; ?>);">
					<option value="">- Bitte ausw&auml;hlen -</option>
					<?php echo $campaignVorgabeOptions; ?>
				</select>			       
				<input type="text" value="<?php echo $settingsDataArray['moduleSettings']['cmVorgabeUsq']; ?>" style="width:25px; text-align:right;" id="cm_vorgabe_usq" />% 
				<input type="button" value="&Uuml;SQ" title="&Uuml;bersendungsquote &uuml;bernehmen" onclick="calcNewV();" />
				<input type="button" value="Ziel eingeben" title="Zielvorgaben eingeben" onclick="toggleViewById('targetSettings');" class="floatRight" />
			</td>
		</tr>
		<tr class="toggleHide" id="targetSettings">
			<td colspan="2">
				<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
					<tr id="openingTarget">
						<td align="right" class="info">Ziel &Ouml;ffnungsrate:</td>
						<td align="left">
							<input style="width:100px" name="campaign[vorgabe_o]" id="vorgabe_o" maxlength="5" type="text" value="<?php echo $campaignEntity->getVorgabe_o(); ?>" <?php echo $readOnly; ?> /> % 
							<span style="font-style:italic; color:#999999;">(z.B. 10, 11.5)</span>
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Ziel Klickrate:</td>
						<td align="left">
							<input style="width:100px" name="campaign[vorgabe_k]" id="vorgabe_k" maxlength="5" type="text" value="<?php echo $campaignEntity->getVorgabe_k(); ?>" <?php echo $readOnly; ?> /> % 
							<span style="font-style:italic; color:#999999;">(z.B. 0.8, 1.5)</span>

							<input type="hidden" id="vorgabe_k_old" value="<?php echo $campaignEntity->getVorgabe_k(); ?>" <?php echo $readOnly; ?> />
							<input type="hidden" id="vorgabe_o_old" value="<?php echo $campaignEntity->getVorgabe_o(); ?>" <?php echo $readOnly; ?> />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<?php
		if ($this->moduleRequestDataArray['_action'] != 'addNv' 
			&& $mailing != 'NV'
		) {
			?>
			<tr>
				<td align="right" class="info">Broking Volumen:</td>
				<td align="left">
					<input name="campaign[broking_volume]" id="broking_volume" type="text" onchange="changeValueToNumber(this.value, this.id)" value="<?php echo NumberUtility::numberFormat($campaignEntity->getBroking_volume()); ?>" />
				</td>
			</tr>
			<?php
		}
		?>
		
		<tr class="<?php echo $priceClass; ?>">
			<td align="right" class="info topPaddingWrapper" valign="top">Abrechnungsart: <span class="required">*</span></td>
			<td align="left">
				<select name="campaign[abrechnungsart]" id="abrechnungsart" <?php echo $billingTypeSettings; ?>  onchange="showOrChangePriceByBillingType(this.value, 'priceContentBox', true);">
					<option value="">- Bitte ausw&auml;hlen -</option>
					<?php echo $campaignDeliverySystemOptions; ?>
				</select>
				<div id="priceContentBox">
					<div id="priceByBillingTypeContent"></div>
					
					Broking Price: <input name="campaign[broking_price]" id="broking_price" type="text" style="width: 100px;" value="<?php echo NumberUtility::sumFormat($campaignEntity->getBroking_price()); ?>" /> &euro;

					<input name="campaign[preis]" id="preis" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getPreis()); ?>" />
					<input name="campaign[preis2]" id="preis2" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getPreis2()); ?>" />
                                        <input name="campaign[hybrid_preis]" id="hybrid_preis" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getHybrid_preis()); ?>" />
                                        <input name="campaign[hybrid_preis2]" id="hybrid_preis2" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getHybrid_preis2()); ?>" />
					<input name="campaign[unit_price]" id="unit_price" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getUnit_price()); ?>" />
					<input name="campaign[preis_gesamt]" id="preis_gesamt" type="hidden" value="<?php echo NumberUtility::sumFormat($campaignEntity->getPreis_gesamt()); ?>" />
					<input name="campaign[preis_gesamt_verify]" id="preis_gesamt" type="hidden" value="<?php echo $campaignEntity->getPreis_gesamt_verify(); ?>" />
				</div>
			</td>
		</tr>

		<?php
			require_once($partialViewPath . $mailing . \DIRECTORY_SEPARATOR . 'useStartDate.php');
		?>

		<tr>
			<td align="right" class="info" valign="top">Notiz:</td>
			<td align="left" valign="top" >
				<textarea name="campaign[note]" id="note" rows="4" ><?php echo $campaignEntity->getNotiz(); ?></textarea>
				<div id="edit_true"></div>
			</td>
		</tr>
	</table>
</div>