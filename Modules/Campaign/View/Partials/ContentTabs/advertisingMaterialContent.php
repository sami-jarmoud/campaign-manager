<?php
use Packages\Core\Utility\Html\FormUtility;

use Modules\Campaign\Utility\CampaignAdvertisingsEntityUtility;

/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */

$updateCampaignIntoDeliverySystem = NULL;
if (\strlen($campaignEntity->getMail_id()) > 0 
	&& (
		$campaignEntity->getStatus() < 20 
		&& $campaignEntity->getStatus() !== 5
	)
) {
	// updateCampaignIntoDeliverySystem
	$updateCampaignIntoDeliverySystem = FormUtility::createHiddenField(
		'actions[updateCampaignIntoDeliverySystem]',
		1
	);
}
?>
<div id="tab3">
	<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">
				<?php
					echo $updateCampaignIntoDeliverySystem;
				?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
					<tr id="campaignAdvertisings">
						<td align="right" class="info">Werbemittel/Anlagen:</td>
						<td align="left">
							<div id="pluploadContainer">
								<input type="button" class="datei-button" onclick="javascript:;" id="pickfiles" value="Datei w&auml;hlen" />
								<input type="button" class="datei-button" onclick="javascript:;" id="uploadfiles" value="Hochladen" />
							</div>
							<div id="console" style="clear: both;"></div>
							<div id="filelist"></div>
						</td>
					</tr>
					
					<?php
					// nur für die technik
					if ($loggedUserEntity->getAbteilung() == 't') {
						?>
					<tr>
						<td align="right" class="info">Zeichensatz:</td>
						<td align="left">
							<select name="campaign[charset]" id="charset">
								<?php echo $campaignCharsetOptionItems; ?>
							</select>
						</td>
					</tr>
					<?php
						}else{
                                        ?>
                                        <input name="campaign[charset]" id="charset" type="hidden" value="<?php echo $campaignEntity->getCharset(); ?>"   
                                        <?php            
                                                }
					?>
					<tr>
						<td align="right" class="info">Mailformat:</td>
						<td align="left">
							<select name="campaign[mailtyp]" id="mailtyp" onchange="oHide(this.value);">
								<?php echo $campaignMailingTypeOptionItems; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="left" colspan="2">Kommentar:<br />
							<textarea name="campaign[advertisings_comment]" id="advertisings_comment" style="height:40px; width: 98%;"><?php echo $campaignEntity->getAdvertisings_comment(); ?></textarea>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<?php
		echo CampaignAdvertisingsEntityUtility::getCampaignAdvertisingsContentResult(
			$campaignEntity,
			$userDataArray,
			$targetDir
		);
		
		if ($loggedUserEntity->getAbteilung() != 't') {
			// für alle außer technik
			?>
			<tr>
				<td align="right" class="info">Werbemittel vorhanden:</td>
				<td align="left">
					<input type="checkbox" name="actions[sendNotificationEmailToTcm]" value="1" />
				</td>
			</tr>
			<?php
			// nur für die technik
		} elseif ($loggedUserEntity->getAbteilung() == 't') {
			if (\strlen($campaignEntity->getMail_id()) === 0 
				&& $campaignEntity->getCampaignAdvertisings()->count() > 0
			) {
				?>
				<tr>
					<td align="right" class="info">Im Versandsystem anlegen:</td>
					<td align="left">
						<input type="checkbox" id="campaign_addIntoDeliverySystem" name="actions[addIntoDeliverySystem]" value="1" />
					</td>
				</tr>
			<?php
			} elseif (\strlen($campaignEntity->getMail_id()) > 0 
				&& (
					$campaignEntity->getStatus() < 20 
					&& $campaignEntity->getStatus() !== 5
				)
				&& (
					$this->moduleRequestDataArray['_action'] != 'addNv' 
					&& $this->moduleRequestDataArray['_action'] != 'copy'
				)
				&& $campaignEntity->getUse_campaign_start_date() !== TRUE
			) {
				?>
				<tr>
					<td align="right" class="info">Werbemittel synchronisieren:</td>
					<td align="left">
						<input type="checkbox" id="campaign_updateCampaignAdvertisingsContent" name="actions[updateCampaignAdvertisingsContent]" value="1" />
					</td>
				</tr>
			<?php
			}
		}
		?>
	</table>
</div>