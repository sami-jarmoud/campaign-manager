<?php
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */

/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $domaineDeliverySystemsDataArray \Packages\Core\Persistence\ArrayIterator */

?>
<div id="tab2">
	<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="headerColumn">Selektion:</td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0" id="versandInformationen">
					<tr>
						<td align="right" class="info">Mandant:</td>
						<td align="left"><?php echo $clientEntity->getMandant(); ?></td>
					</tr>
					<tr>
						<td align="right" class="info">Versandsystem: <span class="required">*</span></td>
						<td align="left" id="deliverySystemItems">
							<select name="campaign[versandsystem]" id="deliverySystem" onchange="changeDeliverySystem(this.value, 'deliverySystemDistributor', 0, 0);">
								<option value="">- Bitte ausw&auml;hlen -</option>
								<?php echo $deliverySystemOptionItems; ?>
							</select>
						</td>
					</tr>
					<tr id="deliverySystemDistributorCell">
						<td align="right" class="info">Verteiler:</td>
						<td align="left">
							<div id="deliverySystemDistributorContent"></div>
							<div id="deliverySystemDistributor" class="floatLeft">
								<select name="campaign[deliverySystemDistributor]" id="deliverySystemDistributorItems" class="widthAuto" onchange="changeDeliverySystemDomain(this.value, 'deliverySystemDistributorDomain', 0, 0);">
									<?php echo $deliverySystemDistributorItem; ?>
								</select>
							</div>
						</td>
					</tr>
                                        <tr  id="deliverySystemDistributorDomainCell">
						<td align="right" class="info">Versanddomain: <span class="required">*</span></td>
						<td align="left">
							<div id="deliverySystemDistributorDomainContent"></div>
							<div id="deliverySystemDistributorDomain" class="floatLeft">
								<select name="campaign[dasp_id]" id="deliverySystemDistributorDomainItems" class="widthAuto">
									<?php echo $deliverySystemDistributorDomainItem; ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Selektion-Klickprofil:</td>
						<td align="left">
							<select name="campaign[selection_click_profiles_id]" id="selection_click_profiles_id">
								<option value="">- Bitte ausw&auml;hlen -</option>
								<?php echo $campaignSelectionClickProfilesOptions; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="right" class="info topPaddingWrapper" valign="top">Selektion optional:</td>
						<td align="left">
							<div class="spacerTopWrap">
								<div id="genderSelection" class="floatLeft ">
									<?php echo $campaignGenderOptionItems; ?>
								</div><br class="clearBoth" />
								<div class="floatLeft info spacerTopWrap">
									Alter von:<input type="text" name="campaign[alter_von]" id="alter_von" style="width:36px; font-weight: normal;" value="<?php echo $campaignEntity->getAlterVon(); ?>" <?php echo $readOnly; ?> />
									&nbsp;Alter bis:<input type="text" name="campaign[alter_bis]" id="alter_bis" style="width:36px; font-weight: normal;" value="<?php echo $campaignEntity->getAlterBis(); ?>" <?php echo $readOnly; ?> />
								</div><br class="clearBoth" />
								<div class="floatLeft info spacerTopWrap">
									PLZ von:&nbsp;&nbsp;<input type="text" name="campaign[plz_von]" id="plz_von" style="width:36px; font-weight: normal;" value="<?php echo $campaignEntity->getPlzVon(); ?>" <?php echo $readOnly; ?> />
									&nbsp;PLZ bis:&nbsp;&nbsp;<input type="text" name="campaign[plz_bis]" id="plz_bis" style="width:36px; font-weight: normal;" value="<?php echo $campaignEntity->getPlzBis(); ?>" <?php echo $readOnly; ?> />
								</div><br class="clearBoth" />
								<div class="floatLeft info spacerTopWrap">
									Letzte Reaktion innerhalb&nbsp;<input type="text" name="campaign[reagierer]" id="reagierer" style="width:36px; font-weight: normal;" value="<?php echo $campaignEntity->getReagierer(); ?>" <?php echo $readOnly; ?> />Tagen.
								</div><br class="clearBoth" />
							</div>

							<div class="spacerTopWrap">
								<div id="zgContent">
									<div class="info">Kommentar:
									</div>
									<input type="hidden"  name="campaign[zg]" id="zg" value="<?php echo $campaignEntity->getZielgruppe();?>">
									<textarea name="campaign[zgKommentar]" id="zgKommentar" style="width:220px; height:50px;" <?php echo $readOnly; ?>><?php echo $campaignEntity->getZgKommentar(); ?></textarea>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="headerColumn">Email-Parameter:</td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="neu_k extendedTable" cellpadding="0" cellspacing="0" border="0" id="emailParameter">
					<tr>
						<td align="right" class="info">Betreff:</td>
						<td align="left">
							<input name="campaign[betreff]" id="betreff" type="text"  value="<?php echo $campaignEntity->getBetreff(); ?>" <?php echo $readOnly; ?>/>
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Absendername:</td>
						<td align="left">
							<input name="campaign[absendername]" id="absendername" type="text" value="<?php echo $campaignEntity->getAbsendername(); ?>" <?php echo $readOnly; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Absenderemail(Prefix):</td>
						<td align="left">
							<input name="campaign[sender_email]" id="sender_email" type="text" value="<?php echo $campaignEntity->getSender_email(); ?>" <?php echo $readOnly; ?> />
						</td>
					</tr>
					<tr>
						<td align="right" class="info">Blacklist:</td>
						<td align="left">
							<input type="checkbox" name="campaign[bl]" id="bl" value="X" <?php echo $campaignEntity->getBlacklist() == 'X' ? 'checked="checked"' : ''; ?> />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>