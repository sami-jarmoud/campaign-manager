<?php
use Modules\Campaign\Utility\CampaignAndCustomerUtility;


/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */

if (\strlen($campaignEntity->getMail_id()) > 0) {
	if ($this->moduleRequestDataArray['_action'] == 'addNv'){
	?>	
        <tr>
		<td align="right" class="info">Im VS anlegen:</td>
		<td align="left">
			<input type="radio" name="actions[autoNv]" id="campaign_imvs_1" onchange="showDeliverySystemStartDate(this, 'campaign_use_campaign_start_date');enableOrDisableDeliverySystemByChangingImvs(this, '<?php echo $campaignEntity->getVersandsystem(); ?>', '<?php echo $campaignEntity->getDsd_id(); ?>');" value="1" checked/> <label for="campaign_imvs_1" >Ja</label>
			<input type="radio" name="actions[autoNv]" id="campaign_imvs_0" onchange="showDeliverySystemStartDate(this, 'campaign_use_campaign_start_date');enableOrDisableDeliverySystemByChangingImvs(this, '<?php echo $campaignEntity->getVersandsystem(); ?>', '<?php echo $campaignEntity->getDsd_id(); ?>');" value="0"  /> <label for="campaign_imvs_0" >Nein</label>
			
			<input type="hidden" name="campaignMailId" value="<?php echo $campaignEntity->getMail_id(); ?>" />
		</td>
	</tr>
        
        <tr id="campaign_use_campaign_start_date">
	<td align="right" class="info">Startdatum &uuml;bernehmen?</td>
	<td align="left">
		<input id="campaign_use_campaign_start_date1" name="campaign[use_campaign_start_date]" value="1" type="radio">
                <label for="campaign_use_campaign_start_date_1">Ja</label>
               <input id="campaign_use_campaign_start_date0" name="campaign[use_campaign_start_date]" value="0" checked="checked" type="radio">
               <label for="campaign_use_campaign_start_date_0">Nein</label>
	</td>
    </tr>
	<?php
	}
?>
<tr id="campaign_use_campaign_start_date">
	<td align="right" class="info">Startdatum &uuml;bernehmen?</td>
	<td align="left">
		<?php
			echo CampaignAndCustomerUtility::getCampaignDeliverySystemStartDateRadios(
				array(
					1 => 'Ja',
					0 => 'Nein'
				),
				$campaignEntity->getUse_campaign_start_date(),
				$statusCampaignStartDate
			);
		?>
	</td>
</tr>
<?php
}
?>