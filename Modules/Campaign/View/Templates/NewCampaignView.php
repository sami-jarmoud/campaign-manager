<?php
use Packages\Core\Utility\Html\FormUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;

/* @var $this \Modules\Campaign\Controller\IndexController */

/* @var $customersDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $domaineDeliverySystemsDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $clientDeliveries \Packages\Core\Persistence\ObjectStorage */
/* @var $firstDeliverySystemItemEntity \Packages\Core\Domain\Model\ClientDeliveryEntity */

/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $dateNow \DateTime */


/**
 * defaultSettings
 */
require_once('campaignViewVariable.php');

/**
 * newCampaignSettings
 */
$jsNewKampaign = 'true';

$campaignIsLock = NULL;

$campaignAdvertisingMaterialsOptions = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$advertisingMaterialsDataArray,
	''
);
$campaignDate = '';
$campaignDay = '';
$campaignMonth = '';
$campaignYear = '';
$campaignHourOptions = CampaignAndCustomerUtility::getHourOptionList(9);
$campaignMinutesOptions = CampaignAndCustomerUtility::getMinutesOptionList(0);
$campaignVorgabeValue = '';
$campaignBillingTypeValue = '';
$campaignDeliverySystemAspShortcut = $firstDeliverySystemItemEntity->getDeliverySystemEntity()->getAsp_abkz();
$campaignDeliveryDomainId = $campaignEntity->getDasp_id();
$deliverySystemDistributorItem = '';
$campaignGenderOptionItems = CampaignAndCustomerUtility::getGenderSelectionOptionList();
$campaignMailingTypeOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$mailtypDataArray,
	''
);
$campaignCharsetOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$charsetDataArray,
	''
);


// default SenderEmailPrefix
$campaignEntity->setSender_email('news');

$moduleRequestAction = 'create';

// htmlContent
require_once('campaignViewTemplate.php');
?>

<script type="text/javascript">
	YAHOO.util.Dom.get('deliverySystem').selectedIndex = 1;
	
	// changeDeliverySystem
	changeDeliverySystem(
		'<?php echo $firstDeliverySystemItemEntity->getDeliverySystemEntity()->getAsp_abkz(); ?>',
		'deliverySystemDistributor',
		0,
		0
	);
</script>