<?php
use Packages\Core\Utility\Html\FormUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;

/* @var $this \Modules\Campaign\Controller\IndexController */

/* @var $customersDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $domaineDeliverySystemsDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $clientDeliveries \Packages\Core\Persistence\ObjectStorage */
/* @var $campaignDeliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
/* @var $deliverySystemDistributorDataArray \Packages\Core\Persistence\ArrayIterator */

/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $dateNow \DateTime */


/**
 * defaultSettings
 */
require_once('campaignViewVariable.php');


/**
 * editCampaignSettings
 */
$campaignAdvertisingMaterialsOptions = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$advertisingMaterialsDataArray,
	$campaignEntity->getAdvertising_materials()
);

$campaignDate = $campaignEntity->getDatum()->format('Y-m-d');
$campaignDay = $campaignEntity->getDatum()->format('d');
$campaignMonth = $campaignEntity->getDatum()->format('m');
$campaignYear = $campaignEntity->getDatum()->format('Y');
$campaignHourOptions = CampaignAndCustomerUtility::getHourOptionList(\intval($campaignEntity->getDatum()->format('H')));
$campaignMinutesOptions = CampaignAndCustomerUtility::getMinutesOptionList(\intval($campaignEntity->getDatum()->format('i')));

$campaignDeliverySystemAspShortcut = $campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp_abkz();
$campaignDeliveryDomainId = $campaignEntity->getDasp_id();
$tmpBillingTypeDataArray = \array_flip(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray);
$campaignBillingTypeValue = $tmpBillingTypeDataArray[$campaignEntity->getAbrechnungsart()];
unset($tmpBillingTypeDataArray);
$campaignVorgabeValue = $campaignEntity->getVorgabe_m();

$deliverySystemDistributorItem = CampaignAndCustomerUtility::getDeliverySystemDistributorsItems(
	$deliverySystemDistributorDataArray,
	$campaignDeliverySystemDistributorEntity,
	($campaignEntity->getMail_id() ? TRUE : FALSE)
);

$campaignGenderOptionItems = CampaignAndCustomerUtility::getGenderSelectionOptionList(
	FALSE,
	$zielgruppeDataArray
);

$campaignMailingTypeOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$mailtypDataArray,
	$campaignEntity->getMailtyp()
);
$campaignCharsetOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$charsetDataArray,
	$campaignEntity->getCharset()
);

$performanceContentClass = 'toggleShow';

switch ($campaignEntity->getAbrechnungsart()) {
	case 'TKP':
	case 'Festpreis':
	case 'Hybrid':
		if ($mailing == 'NV') {
			$jsNewKampaign = 'true';

			$priceClass = $incomeContentClass = 'toggleHide';
		} else {
			$incomeContentClass = 'toggleShow';
		}
		break;

	default:
		$incomeContentClass = 'toggleShow';
		break;
}

if ($mailing == 'NV') {
	// clone campaignDatum to tmpCalcCampaignDate
	$tmpCalcCampaignDate = clone $campaignEntity->getDatum();
	$tmpCalcCampaignDate->sub(new \DateInterval('PT15M'));
	if ($campaignEntity->getUse_campaign_start_date() === TRUE 
		&& $dateNow->format('U') > $tmpCalcCampaignDate->format('U')
	) {
		$statusCampaignStartDate = TRUE;
	}
	unset($tmpCalcCampaignDate);
}

$moduleRequestAction = 'update';
// htmlContent
require_once('campaignViewTemplate.php');
?>

<input type="hidden" id="kid" name="campaign[k_id]" value="<?php echo $campaignEntity->getK_id(); ?>" />

<script type="text/javascript">
	<?php
		if ($mailing == 'HV') {
			?>
			// toggle - deliverySystemDistributorCell
			showViewById('deliverySystemDistributorCell');
                        showViewById('deliverySystemDistributorDomainCell');
                        
			// showOrChangePriceByBillingType
			showOrChangePriceByBillingType(
				'<?php echo $campaignEntity->getAbrechnungsart(); ?>',
				'priceContentBox',
				false
			);
			
			// createAndShowNewPerformanceContent
			createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');
			
			deliverySystem.disabled = true;
			<?php
		} else {
			?>
			// toggle - deliverySystemDistributorCell
			showViewById('deliverySystemDistributorCell');
		        showViewById('deliverySystemDistributorDomainCell');
                        
			// showOrChangePriceByBillingType
			showOrChangePriceByBillingType(
				'<?php echo $campaignEntity->getAbrechnungsart(); ?>',
				'priceContentBox',
				false
			);
	
			showViewById('campaign_use_campaign_start_date');
			
			if (!showOButton) {
				disableCampaign_useCampaignStartDate();
			}
			
			// createAndShowNewPerformanceContent
			createAndShowNewPerformanceContent('<?php echo $campaignEntity->getAbrechnungsart(); ?>');
			deliverySystem.disabled = true;
			<?php
		}
	?>
</script>