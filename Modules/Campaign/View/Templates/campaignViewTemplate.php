<?php
use Packages\Core\Utility\Html\FormUtility;
use Packages\Core\Utility\SystemHelperUtility;
use Packages\Core\Utility\Date\DatetimeUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;

/* @var $this \Modules\Campaign\Controller\IndexController */

/* @var $customersDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $domaineDeliverySystemsDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $clientDeliveries \Packages\Core\Persistence\ObjectStorage */
/* @var $campaignDeliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
/* @var $deliverySystemDistributorDataArray \Packages\Core\Persistence\ArrayIterator */

/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $dateNow \DateTime */

$customerOptions = CampaignAndCustomerUtility::getCustomerOptionList(
	$customersDataArray,
	$campaignEntity
);

$campaignMonthOptions = CampaignAndCustomerUtility::getOptionListByCount(12);
$campaignDayOptions = CampaignAndCustomerUtility::getOptionListByCount(
	DatetimeUtility::getDayCountByMonthAndYear(
		$dateNow->format('m'),
		$dateNow->format('Y')
	)
);

$campaignClickProfilesOptions = CampaignAndCustomerUtility::getClientClickProfilesOptionList(
	'campaign_click_profiles_id',
	$clientClickProfiles,
	$campaignEntity
);
$campaignDeliverySystemOptions = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$campaignBillingsTypeDataArray,
	$campaignBillingTypeValue
);

$deliverySystemOptionItems = SystemHelperUtility::getDeliverySystemsItemsOptionList(
	$clientDeliveries,
	$campaignDeliverySystemAspShortcut
);


$deliverySystemDistributorDomainItem = SystemHelperUtility::getDeliverySystemsItemsDomainOptionList(
	$domaineDeliverySystemsDataArray,
	$campaignDeliveryDomainId
);

$campaignSelectionClickProfilesOptions = CampaignAndCustomerUtility::getClientClickProfilesOptionList(
	'selection_click_profiles_id',
	$clientClickProfiles,
	$campaignEntity
);

$campaignVorgabeOptions = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$campaignVorgabeDataArray,
        CampaignAndCustomerUtility::numberFormat($campaignVorgabeValue)
);
$usesCheckboxItems = \nl2br(CampaignAndCustomerUtility::getUserCheckboxItems($userDataArray));

if ($campaignEntity->getVorgabe_o() === '') {
	$campaignEntity->setVorgabe_o($settingsDataArray['moduleSettings']['cmVorgabeO']);
}

if ($campaignEntity->getVorgabe_k() === '') {
	$campaignEntity->setVorgabe_k($settingsDataArray['moduleSettings']['cmVorgabeK']);
}

if ($campaignEntity->getMailtyp() == 't') {
	$campaignEntity->setVorgabe_o(0);
}

$performanceInfoContent = 'Bitte f&uuml;llen Sie die Daten manuell aus.';
$readOnlyPerformance = '';
if (\strlen($campaignEntity->getMail_id()) > 0 
	&& (
		$this->moduleRequestDataArray['_action'] != 'addNv' 
		&& $this->moduleRequestDataArray['_action'] != 'copy'
	)
) {
	$performanceInfoContent = 'Daten werden von ' . $campaignEntity->getVersandsystem() . ' automatisch synchronisiert.';
	$readOnlyPerformance = 'readonly="readonly" class="inactive"';
}

switch ($campaignEntity->getAbrechnungsart()) {
	case 'TKP':
	case 'Festpreis':
		$leadsContentClass = 'toggleHide';
		break;
	
	default:
		$leadsContentClass = 'toggleShow';
		break;
}

// js and input Settings
$readOnly = $disabledCampaignDate = '';
if ($campaignEntity->getStatus() === 5 
	|| $campaignEntity->getStatus() > 20
) {
	$jsShowOButton = 'false';
	#$readOnly = 'readonly="readonly"';
	#$disabledCampaignDate = 'disabled="disabled"';
} else {
	$jsShowOButton = 'true';
}

if ($campaignEntity->getContactPersonEntity() instanceof \Modules\Customer\Domain\Model\ContactPersonEntity) {
	echo '
		<script type="text/javascript">
			getAP(' . $campaignEntity->getContactPersonEntity()->getCustomerEntity()->getKunde_id() . ', ' . $campaignEntity->getContactPersonEntity()->getAp_id() . ');
		</script>
	';
} elseif ($campaignEntity->getAgentur_id() > 0) {
	/**
	 * @deprecated gilt nur für ältere kampagnen
	 */
	echo '
		<script type="text/javascript">
			getAP(' . $campaignEntity->getAgentur_id() . ', ' . $campaignEntity->getAp_id() . ');
		</script>
	';
}
?>
<input type="hidden" name="customer[firma_short]" id="kNewCustomerShortCompany" value="" />
<input type="hidden" name="customer[strasse]" id="kNewCustomerStreet" value="" />
<input type="hidden" name="customer[plz]" id="kNewCustomerZip" value="" />
<input type="hidden" name="customer[ort]" id="kNewCustomerCity" value="" />
<input type="hidden" name="customer[telefon]" id="kNewCustomerPhone" value="" />
<input type="hidden" name="customer[fax]" id="kNewCustomerFax" value="" />
<input type="hidden" name="customer[website]" id="kNewCustomerWebsite" value="" />
<input type="hidden" name="customer[email]" id="kNewCustomerEmail" value="" />
<input type="hidden" name="customer[geschaeftsfuehrer]" id="kNewCustomerCeo" value="" />
<input type="hidden" name="customer[registergericht]" id="kNewCustomerRegistergericht" value="" />
<input type="hidden" name="customer[vat_number]" id="kNewCustomerVatNumber" value="" />
<input type="hidden" name="customer[payment_deadline]" id="kNewCustomerPaymentDeadline" value="" />
<input type="hidden" name="customer[country_id]" id="kNewCustomerCountryId" value="" />
<input type="hidden" name="customer[data_selection]" id="kNewCustomerDataSelection" value="" />

<input type="hidden" name="contactPerson[anrede]" id="kNewContactPersonGender" value=""  />
<input type="hidden" name="contactPerson[vertriebler_id]" id="kNewContactPersonVertriebler" value="" />
<input type="hidden" name="contactPerson[titel]" id="kNewContactPersonTitle" value="" />
<input type="hidden" name="contactPerson[vorname]" id="kNewContactPersonFirstname" value="" />
<input type="hidden" name="contactPerson[nachname]" id="kNewContactPersonLastname" value="" />
<input type="hidden" name="contactPerson[email]" id="kNewContactPersonEmail" value="" />
<input type="hidden" name="contactPerson[telefon]" id="kNewContactPersonPhone" value="" />
<input type="hidden" name="contactPerson[fax]" id="kNewContactPersonFax" value="" />
<input type="hidden" name="contactPerson[mobil]" id="kNewContactPersonMobil" value="" />
<input type="hidden" name="contactPerson[position]" id="kNewContactPersonPosition" value="" />

<input type="hidden" name="_module" value="<?php echo $this->moduleRequestDataArray['_module']; ?>" />
<input type="hidden" name="_controller" value="<?php echo $this->moduleRequestDataArray['_controller']; ?>" />
<input type="hidden" name="_action" value="<?php echo $moduleRequestAction; ?>" />

<input type="hidden" id="status" name="campaignStatus" value="<?php echo $campaignEntity->getStatus(); ?>" />
<input type="hidden" id="campaign_mailId" value="<?php echo $campaignEntity->getMail_id(); ?>" />
<input type="hidden" name="oldAction" id="campaignAction" value="<?php echo $this->moduleRequestDataArray['_action']; ?>" />
<input type="hidden" id="kid" name="campaign[kid]" value="<?php echo $campaignEntity->getK_id(); ?>" />
<?php
if ($this->moduleRequestDataArray['_action'] != 'edit') {
	?>
<input type="hidden" name="campaign[bearbeiter]" value="<?php echo $loggedUserEntity->getVorname() . ' ' . $loggedUserEntity->getNachname(); ?>" />
	<?php
}
?>


<div id="tab_k_new" class="yui-navset" style="padding-top:7px; text-align:left; border:0px;">
    <ul class="yui-nav" style="margin-left:-1px;">
        <li class='selected'><a href="#tab1"><em>Kampagnendetails</em></a></li>
        <li><a href="#tab2"><em>Versanddetails</em></a></li>
		<li><a href="#tab3"><em>Dateien</em></a></li>
        <li><a href="#tab4"><em>Infomailing</em></a></li>
		<?php
		if ($this->moduleRequestDataArray['_action'] == 'edit' 
			|| $this->moduleRequestDataArray['_action'] == 'addNv'
		) {
			?>
			<li class="toggleShow"><a href="#tab5"><em>Auswertung</em></a></li>
			<?php
		}
		?>
    </ul>
    <div class="yui-content" style="margin:0px; padding:0px;">
		<?php
			$viewPath = \str_replace('Templates', '', __DIR__);
			$partialViewPath = $viewPath . 'Partials' .  \DIRECTORY_SEPARATOR;
			
			// tab1
			require_once($partialViewPath . 'ContentTabs' . \DIRECTORY_SEPARATOR . 'campaignContent.php');
			
			// tab2
			require_once($partialViewPath . 'ContentTabs' . \DIRECTORY_SEPARATOR . 'deliveryContent.php');
			
			// tab3
			require_once($partialViewPath . 'ContentTabs' . \DIRECTORY_SEPARATOR . 'advertisingMaterialContent.php');
			
			// tab4
			require_once($partialViewPath . 'ContentTabs' . \DIRECTORY_SEPARATOR . 'infomailingContent.php');
			
			// tab5
			if ($this->moduleRequestDataArray['_action'] == 'edit' 
				|| $this->moduleRequestDataArray['_action'] == 'addNv'
			) {
				require_once($partialViewPath . 'ContentTabs' . \DIRECTORY_SEPARATOR . 'analysisContent.php');
			}
		?>
	</div>
</div>

<script type="text/javascript">
	<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'system/javascript/Vendor/plupload/plupload.full.min.js');
	require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'system/javascript/Vendor/plupload/i18n/de.js');
	?>
	
	var deliverySystem = YAHOO.util.Dom.get('deliverySystem');
    var deliverySystemDistributor = YAHOO.util.Dom.get('deliverySystemDistributorItems');
    var volume = YAHOO.util.Dom.get('volumen').value;
    var vz = YAHOO.util.Dom.get('vorgabe_m').value;
	
	<?php
		if ($mailing == 'HV') {
        ?>
			if (volume !== '' 
				&& volume !== vz
			) {
				YAHOO.util.Dom.get('cm_vorgabe_usq').value = ((vz / volume) * 100) - 100;
			}
	<?php
		} else {
			echo "YAHOO.util.Dom.get('cm_vorgabe_usq').value = '';";
			echo 'var hvCampaignDateObject = new Date("' . $hvCampaignDate->format('c') . '");';
		}
		
		echo '
			var showOButton = ' . $jsShowOButton . ';
			var newKampaign = ' . $jsNewKampaign . ';
		';
    
		require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'system/javascript/Module/Campaign/newCampaign.js');
		
		require_once($_SERVER['DOCUMENT_ROOT'] . \DIRECTORY_SEPARATOR . 'system/javascript/Module/Campaign/uploader.js');
	?>
	
	if (YAHOO.util.Dom.get('apButton') !== null) {
		hideViewById('apButton');
    }
    
    /**
     * toggle - Settings
     */
	toggleViewById('priceContentBox');
    toggleViewById('deliverySystemDistributorContent');
    toggleViewById('deliverySystemDistributorDomainContent');
    /* openingTarget */
    oHide('<?php echo $campaignEntity->getMailtyp(); ?>');
	
	// download zip ausblenden
	toggleViewById('compressAndDownloadFile_1454414330');
	
	resetValueDataIfValueIsNull(
		vz,
		'vorgabe_m'
	);
</script>