<?php
use Packages\Core\Utility\Html\FormUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;

/* @var $this \Modules\Campaign\Controller\IndexController */

/* @var $customersDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $campaignEntity \Modules\Campaign\Domain\Model\CampaignEntity */
/* @var $domaineDeliverySystemsDataArray \Packages\Core\Persistence\ArrayIterator */
/* @var $clientDeliveries \Packages\Core\Persistence\ObjectStorage */
/* @var $campaignDeliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
/* @var $deliverySystemDistributorDataArray \Packages\Core\Persistence\ArrayIterator */

/* @var $clientEntity \Packages\Core\Domain\Model\ClientEntity */
/* @var $loggedUserEntity \Packages\Core\Domain\Model\UserEntity */
/* @var $dateNow \DateTime */


/**
 * defaultSettings
 */
require_once('campaignViewVariable.php');


/**
 * editCampaignSettings
 */
$campaignAdvertisingMaterialsOptions = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$advertisingMaterialsDataArray,
	$campaignEntity->getAdvertising_materials()
);

$campaignDate = $campaignEntity->getDatum()->format('Y-m-d');
$campaignDay = $campaignEntity->getDatum()->format('d');
$campaignMonth = $campaignEntity->getDatum()->format('m');
$campaignYear = $campaignEntity->getDatum()->format('Y');
$campaignHourOptions = CampaignAndCustomerUtility::getHourOptionList(\intval($campaignEntity->getDatum()->format('H')));
$campaignMinutesOptions = CampaignAndCustomerUtility::getMinutesOptionList(\intval($campaignEntity->getDatum()->format('i')));

$campaignDeliverySystemAspShortcut = $campaignDeliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp_abkz();
$campaignDeliveryDomainId = $campaignEntity->getDasp_id();
$tmpBillingTypeDataArray = \array_flip(CampaignAndCustomerUtility::$campaignBillingsTypeDataArray);
$campaignBillingTypeValue = $tmpBillingTypeDataArray[$campaignEntity->getAbrechnungsart()];
$campaignVorgabeValue = $campaignEntity->getVorgabe_m();
unset($tmpBillingTypeDataArray);

$deliverySystemDistributorItem = CampaignAndCustomerUtility::getDeliverySystemDistributorsItems(
	$deliverySystemDistributorDataArray,
	$campaignDeliverySystemDistributorEntity,
	FALSE
);

$campaignGenderOptionItems = CampaignAndCustomerUtility::getGenderSelectionOptionList(
	FALSE,
	$zielgruppeDataArray
);

$campaignMailingTypeOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$mailtypDataArray,
	$campaignEntity->getMailtyp()
);
$campaignCharsetOptionItems = FormUtility::createOptionListItemData(
	CampaignAndCustomerUtility::$charsetDataArray,
	$campaignEntity->getCharset()
);

$performanceContentClass = 'toggleShow';
$jsNewKampaign = 'true';

if ($mailing == 'NV') {
	if (($strPos = \strpos($campaignEntity->getK_name(), ' [NV')) !== FALSE) {
		$campaignTitle = \substr($campaignEntity->getK_name(), 0, $strPos);
		$campaignEntity->setK_name($campaignTitle . $mengeText);
	} else {
		$campaignEntity->setK_name($campaignEntity->getK_name() . ' [HV ' . $hvCampaignDate->format('d.m') . ']' . $mengeText);
	}
}

switch ($campaignEntity->getAbrechnungsart()) {
	case 'TKP':
	case 'Festpreis':
	case 'Hybrid':
		$priceClass = 'toggleHide';
		
		$campaignEntity->setPreis(0);
		$campaignEntity->setPreis2(0);
		$campaignEntity->setUnit_price(0);
		$campaignEntity->setPreis_gesamt(0);
		$campaignEntity->setPreis_gesamt_verify(0);
		break;

	default:
		$incomeContentClass = 'toggleShow';
		break;
}

$moduleRequestAction = 'create';

// htmlContent
require_once('campaignViewTemplate.php');
?>

<input type="hidden" name="campaign[nv_id]" value="<?php echo $campaignEntity->getNv_id(); ?>" />

<script type="text/javascript">
	<?php
		if ($mailing == 'NV') {
			?>
			// toggle - deliverySystemDistributorCell
			showViewById('deliverySystemDistributorCell');
                        
			// showOrChangePriceByBillingType
			showOrChangePriceByBillingType(
				'<?php echo $campaignEntity->getAbrechnungsart(); ?>',
				'priceContentBox',
				false
			);
			
			//deliverySystem.disabled = true;
			<?php
			if ($campaignEntity->getUse_campaign_start_date() === 1) {
				?>
				YAHOO.util.Dom.get('campaign_imvs_1').checked = true;
				showViewById('campaign_use_campaign_start_date');
				<?php
			} else {
				?>
				hideViewById('campaign_use_campaign_start_date');
				<?php
			}
		}
	?>
</script>