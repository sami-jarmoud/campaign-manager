<?php
namespace Modules\Campaign\Utility;

use Packages\Core\Utility\Html\FormUtility;


/**
 * Description of CampaignAndCustomerUtility
 *
 * @author Cristian.Reus
 */
final class CampaignAndCustomerUtility {


	/**
	 * hvCampaignsStatusDataArray
	 * 
	 * @var array
	 */
	public static $hvCampaignsStatusDataArray = array(
		0 => array(
			'status' => 0,
			'label' => 'Neu',
			'sublabel' => 'Neue Kampagne',
			'campaignViewLabel' => 'Neu',
			'campaignStatusColor' => 'red',
		),
		1 => array(
			'status' => 1,
			'label' => 'Werbemittel erhalten',
			'sublabel' => 'Werbemittel wurden geliefert',
			'campaignViewLabel' => 'Werbemittel',
			'campaignStatusColor' => '#FF9900',
		),
		2 => array(
			'status' => 2,
			'label' => 'Optimiert',
			'sublabel' => 'Werbemittel wurden optimiert und sind versandfertig',
			'campaignViewLabel' => 'Optimiert',
			'campaignStatusColor' => '#FF9900',
		),
		4 => array(
			'status' => 4,
			'label' => 'Freigabe',
			'sublabel' => 'Freigabe wurde erteilt',
			'campaignViewLabel' => 'Freigabe',
			'campaignStatusColor' => '#5DB00D',
		),
		5 => array(
			'status' => 5,
			'label' => 'Versendet',
			'sublabel' => 'Mailing wurde versendet',
			'campaignViewLabel' => 'versendet',
			'campaignStatusColor' => '#5DB00D',
		),
	);
	
	/**
	 * nvCampaignsStatusDataArray
	 * 
	 * @var array
	 */
	public static $nvCampaignsStatusDataArray = array(
		0 => array(
			'status' => 0,
			'label' => 'Neu',
			'sublabel' => 'Neue Kampagne',
			'campaignViewLabel' => 'Neu',
			'campaignStatusColor' => 'red',
		),
		1 => array(
			'status' => 1,
			'label' => 'Werbemittel erhalten',
			'sublabel' => 'Werbemittel wurden geliefert',
			'campaignViewLabel' => 'Werbemittel',
			'campaignStatusColor' => '#FF9900',
		),
		2 => array(
			'status' => 2,
			'label' => 'Optimiert',
			'sublabel' => 'Werbemittel wurden optimiert und sind versandfertig',
			'campaignViewLabel' => 'Optimiert',
			'campaignStatusColor' => '#FF9900',
		),
		4 => array(
			'status' => 4,
			'label' => 'Freigabe',
			'sublabel' => 'Freigabe wurde erteilt',
			'campaignViewLabel' => 'Freigabe',
			'campaignStatusColor' => '#5DB00D',
		),
		13 => array(
			'status' => 13,
			'label' => 'Programiert',
			'sublabel' => 'Programiert',
			'campaignViewLabel' => 'programiert',
			'campaignStatusColor' => '#FF9900',
		),
		5 => array(
			'status' => 5,
			'label' => 'Versendet',
			'sublabel' => 'Mailing wurde versendet',
			'campaignViewLabel' => 'versendet',
			'campaignStatusColor' => '#5DB00D',
		),
	);
	
	/**
	 * campaignsReportingStatusDataArray
	 * 
	 * @var array
	 */
	public static $campaignsReportingStatusDataArray = array(
		23 => array(
			'status' => 23,
			'label' => 'Reportfertig',
			'sublabel' => 'Das Mailing ist abgeschlossen und kann reportet werden.',
			'campaignViewLabel' => 'Reportfertig',
			'campaignStatusColor' => '#FF9900',
		),
		20 => array(
			'status' => 20,
			'label' => 'Report intern verschickt',
			'sublabel' => 'Das Reporting wurde intern verschickt',
			'campaignViewLabel' => 'Report intern<br />verschickt',
			'campaignStatusColor' => '#FF9900',
		),
		21 => array(
			'status' => 21,
			'label' => 'Erstreporting abgeschlossen',
			'sublabel' => 'Das Erstreporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Erstreporting<br />abgeschlossen',
			'campaignStatusColor' => '#FF9900',
		),
		22 => array(
			'status' => 22,
			'label' => 'Endreporting abgeschlossen',
			'sublabel' => 'Das finale Reporting wurde an die Agentur verschickt',
			'campaignViewLabel' => 'Endreporting<br />abgeschlossen',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	/**
	 * campaignsOrderStatusDataArray
	 * 
	 * @var array
	 */
	public static $campaignsOrderStatusDataArray = array(
		30 => array(
			'status' => 30,
			'label' => 'Rechnungsstellung freigegeben',
			'sublabel' => 'Die Rechnungsstellungsmail wurde an Buchhaltung verschickt.',
			'campaignViewLabel' => 'Rechnungsstellung<br />freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		32 => array(
			'status' => 32,
			'label' => 'Rechnung gestellt',
			'sublabel' => 'Die Rechnung wurde an den Kunden verschickt.',
			'campaignViewLabel' => 'Rechnung<br />gestellt',
			'campaignStatusColor' => '#FF9900',
		),
		33 => array(
			'status' => 33,
			'label' => 'Rechnung bezahlt',
			'sublabel' => 'Die Rechung wurde bezahlt.',
			'campaignViewLabel' => 'Rechnung<br />bezahlt',
			'campaignStatusColor' => '#5DB00D',
		),
		40 => array(
			'status' => 40,
			'label' => 'Gutschrift erhalten',
			'sublabel' => 'Gutschrift erhalten.',
			'campaignViewLabel' => 'Gutschrift erhalten',
			'campaignStatusColor' => '#5DB00D',
		)
	);
	
	/**
	 * campaignsOthersStatusDataArray
	 * 
	 * @var array
	 */
	public static $campaignsOthersStatusDataArray = array(
		6 => array(
			'status' => 6,
			'label' => 'senden',
			'sublabel' => 'senden',
			'campaignViewLabel' => 'senden',
			'campaignStatusColor' => 'yellow',
		),
		7 => array(
			'status' => 7,
			'label' => 'abgebrochen',
			'sublabel' => 'Abgebrochen',
			'campaignViewLabel' => 'Abgebrochen',
			'campaignStatusColor' => 'yellow',
		),
		24 => array(
			'status' => 24,
			'label' => 'Report freigegeben',
			'sublabel' => 'Report freigegeben.',
			'campaignViewLabel' => 'Report freigegeben',
			'campaignStatusColor' => '#FF9900',
		),
		31 => array(
			'status' => 31,
			'label' => 'Rechnung wird erstellt',
			'sublabel' => 'Der Rechnungserhalt wurde best&auml;tigt',
			'campaignViewLabel' => 'Rechnung<br />wird erstellt',
			'campaignStatusColor' => '#FF9900',
		),
	);
	
	/**
	 * campaignBillingsTypeDataArray
	 * 
	 * @var array
	 */
	public static $campaignBillingsTypeDataArray = array(
		'TKP' => 'TKP',
		'Hybri' => 'Hybrid',
		'CPL' => 'CPL',
		'CPO' => 'CPO',
		'CPC' => 'CPC',
		'Festp' => 'Festpreis'
	);
	
	/**
	 * genderDataArray
	 * 
	 * @var array
	 */
	public static $genderDataArray = array(
		1 => 'nur Herren',
		2 => 'nur Frauen',
		0 => 'beide'
	);
	
	/**
	 * mailtypDataArray
	 * 
	 * @var array
	 */
	public static $mailtypDataArray = array(
		'm' => 'Multipart',
		'h' => 'Html',
		't' => 'Text'
	);
	
	/**
	 * mimeTypeDataArray
	 * 
	 * @var array
	 */
	public static $mimeTypeDataArray = array(
		'm' => 'multipart/alternative',
		'h' => 'text/html',
		't' => 'text/plain'
	);
	
	/**
	 * allTabFieldsDataArray
	 * 
	 * @var array
	 */
	public static $allTabFieldsDataArray = array(
		'k_id' => array(
			'label' => 'Kampagnen ID',
			'sublabel' => 'ID'
		),
		'datum' => array(
			'label' => 'Versanddatum',
			'sublabel' => 'Start'
		),
		'bearbeiter' => array(
			'label' => 'Bearbeiter',
			'sublabel' => 'Bearbeiter'
		),
		'betreff' => array(
			'label' => 'Betreff',
			'sublabel' => 'Betreff'
		),
		'absender' => array(
			'label' => 'Absendername',
			'sublabel' => 'Absendername'
		),
		'gebucht' => array(
			'label' => 'Gebucht',
			'sublabel' => 'Gebucht'
		),
		'versendet' => array(
			'label' => 'Versendet',
			'sublabel' => 'Versendet'
		),
		'beendet' => array(
			'label' => 'Versandendzeit',
			'sublabel' => 'Beendet'
		),
		'openings' => array(
			'label' => '&Ouml;ffnungen (unique)',
			'sublabel' => '&Ouml; (u)'
		),
		'openings_a' => array(
			'label' => '&Ouml;ffnungen (alle)',
			'sublabel' => '&Ouml;'
		),
		'open_quote_a' => array(
			'label' => '&Ouml;ffnungsrate (alle in %)',
			'sublabel' => '&Ouml;R'
		),
		'open_ziel' => array(
			'label' => '&Ouml;ffnungszielrate',
			'sublabel' => '&Ouml; Ziel'
		),
		'open_quote' => array(
			'label' => '&Ouml;ffnungsrate (unique in %)',
			'sublabel' => '&Ouml;R (u)'
		),
		'klicks' => array(
			'label' => 'Klicks (unique)',
			'sublabel' => 'K (u)'
		),
		'klicks_a' => array(
			'label' => 'Klicks (alle)',
			'sublabel' => 'K'
		),
		'klicks_quote' => array(
			'label' => 'Klickrate (unique in %)',
			'sublabel' => 'KR (u)'
		),
		'klicks_ziel' => array(
			'label' => 'Klickzielrate',
			'sublabel' => 'K Ziel'
		),
		'klicks_quote_a' => array(
			'label' => 'Klickrate (alle in %)',
			'sublabel' => 'KR'
		),
		'hbounces' => array(
			'label' => 'Hardbounces (abs. und %)',
			'sublabel' => 'Hardbounces'
		),
		'sbounces' => array(
			'label' => 'Softbounces (abs. und %)',
			'sublabel' => 'Softbounces'
		),
		'hsbounces' => array(
			'label' => 'Bounces gesamt (abs. und %)',
			'sublabel' => 'Bounces gesamt'
		),
		'zielgruppe' => array(
			'label' => 'Zielgruppe',
			'sublabel' => 'Zielgruppe'
		),
		'bl' => array(
			'label' => 'Blacklist',
			'sublabel' => 'Blacklist'
		),
		'mailformat' => array(
			'label' => 'Mailformat',
			'sublabel' => 'Mime'
		),
		'asp' => array(
			'label' => 'Versandsystem (ASP)',
			'sublabel' => 'ASP'
		),
		'notiz' => array(
			'label' => 'Notiz',
			'sublabel' => 'Notiz'
		),
		'ap' => array(
			'label' => 'Ansprechpartner',
			'sublabel' => 'Ansprechpartner'
		),
		'abrechnungsart' => array(
			'label' => 'Abrechnungsart',
			'sublabel' => 'Abr.rt'
		),
		'preis' => array(
			'label' => 'Preis',
			'sublabel' => 'Preis'
		),
		'preis_gesamt' => array(
			'label' => 'Einnahmen',
			'sublabel' => 'Einnahmen'
		),
		'avg_tkp' => array(
			'label' => 'eTKP',
			'sublabel' => 'eTKP'
		),
		'leads' => array(
			'label' => 'Leads',
			'sublabel' => 'Leads'
		),
		'leads_u' => array(
			'label' => 'Leads validiert',
			'sublabel' => 'Leads validiert'
		),
		'report' => array(
			'label' => 'Report intern',
			'sublabel' => 'Report intern'
		),
		'rechnung' => array(
			'label' => 'Rechnung',
			'sublabel' => 'Rechnung'
		),
		'erstreporting' => array(
			'label' => 'Erstreporting f&auml;llig am',
			'sublabel' => 'Erstrep. f&auml;llig am'
		),
		'endreporting' => array(
			'label' => 'Endreporting f&auml;llig am',
			'sublabel' => 'Endrep. f&auml;llig am'
		),
		'menge_ziel' => array(
			'label' => 'Vorgabe Menge',
			'sublabel' => 'Menge Ziel'
		),
		'abmelder' => array(
			'label' => 'Abmelder',
			'sublabel' => 'Abmelder'
		),
		'rechnungstellung' => array(
			'label' => 'Rechnungstellung',
			'sublabel' => 'Re-St'
		),
		'zustellreport' => array(
			'label' => 'Zustellreport',
			'sublabel' => 'Z-Rep'
		),
		'infomail' => array(
			'label' => 'Infomail - Neues Mailing',
			'sublabel' => 'Infomail'
		),
		'unit_price' => array(
			'label' => 'Preis pauschal',
			'sublabel' => 'Preis pauschal'
		),
		'anfrage' => array(
			'label' => 'Anfrage details',
			'sublabel' => 'Anfrage details'
		),
		'dsd_id' => array(
			'label' => 'Verteiler',
			'sublabel' => 'Verteiler'
		),
		'mail_id' => array(
			'label' => 'MailId',
			'sublabel' => 'MailId'
		),
		'use_campaign_start_date' => array(
			'label' => 'in Asp programiert',
			'sublabel' => 'in Asp prog.'
		),
		'actionLogInfo' => array(
			'label' => 'LogInfo',
			'sublabel' => 'LogInfo'
		),
		'campaign_click_profile_id' => array(
			'label' => 'Kampagnen-Klickprofil',
			'sublabel' => 'K.-Klickprofil'
		),
		'selection_click_profile_id' => array(
			'label' => 'Selektion-Klickprofil',
			'sublabel' => 'S.-Klickprofil'
		),
		'broking_volume' => array(
			'label' => 'Broking Volumen',
			'sublabel' => 'B.Volumen'
		),
		'broking_price' => array(
			'label' => 'Broking Preis',
			'sublabel' => 'B.Preis'
		),
		'kosten' => array(
			'label' => 'Kosten',
			'sublabel' => 'Kosten'
		),
		'deckungsbeitrag' => array(
			'label' => 'Deckungsbeitrag',
			'sublabel' => 'Deckungsbeitrag'
		),
		// TODO: deprecated, entfernen
		#'crm_order_id' => array(
		#	'label' => 'CRM - Id',
		#	'sublabel' => 'CRM - Id'
		#),
		'cs_id' => array(
			'label' => 'Selektion Land',
			'sublabel' => 'Selektion Land'
		),
	);
	
	/**
	 * customerDataSelections
	 * 
	 * @var array
	 */
	public static $customerDataSelections = array(
		1 => 'DOI',
		2 => 'SOI',
		3 => 'DOI/SOI',
		0 => 'Unbekannt'
	);
	
	/**
	 * advertisingMaterialsDataArray
	 * 
	 * @var array
	 */
	public static $advertisingMaterialsDataArray = array(
		1 => 'Optimierung',
		2 => 'Erstellung',
		3 => 'Wechsel'
	);
	
	/**
	 * charsetDataArray
	 * 
	 * @var array
	 */
	public static $charsetDataArray = array(
		'UTF-8' => 'UTF-8',
		'ISO-8859-1' => 'ISO-8859-1'
	);
	
	public static $campaignVorgabeDataArray = array(
                '100.000' => '100.000',
		'150.000' => '150.000',
		'200.000' => '200.000',
		'250.000' => '250.000',
		'300.000' => '300.000',
		'350.000' => '350.000',
                '400.000' => '400.000',
                '500.000' => '500.000',
                '1.000.000' => '1.000.000'
        );

	/**
	 * numberFormat
	 * 
	 * @param float $number
	 * @return float
	 */
	public static function numberFormat($number) {
		return \number_format($number, 0, '', '.');
	}
        /**
	 * getDeliverySystemDistributorsItems
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray
	 * @param \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $selectedDeliverySystemDistributorEntity
	 * @param boolean $disableNotSelectedClientId
	 * @param boolean $selectDefaultValue
	 * @return string
	 */
	public static function getDeliverySystemDistributorsItems(\Packages\Core\Persistence\ArrayIterator $deliverySystemDistributorDataArray, \Packages\Core\Domain\Model\DeliverySystemDistributorEntity $selectedDeliverySystemDistributorEntity, $disableNotSelectedClientId = false, $selectDefaultValue = false) {
		$result = '';
		
		foreach ($deliverySystemDistributorDataArray as $key => $deliverySystemDistributorEntity) {
			/* @var $deliverySystemDistributorEntity \Packages\Core\Domain\Model\DeliverySystemDistributorEntity */
			
			$selected = $disabled = '';
			if ($deliverySystemDistributorEntity->getId() === $selectedDeliverySystemDistributorEntity->getId()) {
				$selected = 'selected';
			} elseif ($selectDefaultValue 
				&& $deliverySystemDistributorEntity->getSet_as_default()
			) {
				$selected = 'selected';
			}
			
			if ($disableNotSelectedClientId 
				&& $deliverySystemDistributorEntity->getM_asp_id() !== $selectedDeliverySystemDistributorEntity->getM_asp_id()
			) {
				$disabled = 'disabled';
			}
			
			if ($deliverySystemDistributorEntity->getId() === $deliverySystemDistributorEntity->getParent_id()) {
				$optgroupDataArray = FormUtility::createOptgroupDataArray(
					self::getParentClientDeliverySystemDistributorTitle($deliverySystemDistributorEntity->getTitle())
				);
				
				$result .= FormUtility::openSelectOptgroup(
					$key,
					$optgroupDataArray
				);
			}
		
			$result .= FormUtility::createOptionItem(
				array(
					'value' => $deliverySystemDistributorEntity->getId(),
					'selected' => $selected,
					'disabled' => $disabled
				),
				$deliverySystemDistributorEntity->getTitle()
			);
			
			if (\count($deliverySystemDistributorDataArray) === $key) {
				// closeSelectOptgroup
				$result .= FormUtility::closeSelectOptgroup($optgroupDataArray);
				unset($optgroupDataArray);
			}
		}
		
		return $result;
	}
	
	/**
	 * getParentClientDeliverySystemDistributorTitle
	 * 
	 * @param string $value
	 * @return string
	 */
	public static function getParentClientDeliverySystemDistributorTitle($value) {
		$tmpDataArray = \explode('Gesamtverteiler', $value);
		$result = \trim($tmpDataArray[0]);
		if (\strlen($result) === 0) {
			$result = $value;
		}
		unset($tmpDataArray);
	
		return \rtrim($result, '- ');
	}
	
	
	/**
	 * getOptionListByCount
	 * 
	 * @param integer $end
	 * @return string
	 */
	public static function getOptionListByCount($end) {
		$result = '';

		for ($i = 1; $i <= $end; $i++) {
			if ($i < 10) {
				$i = '0' . $i;
			}

			$result .= FormUtility::createOptionItem(
				array(
					'value' => $i
				),
				$i
			);
		}

		return $result;
	}

	/**
	 * getHourOptionList
	 * 
	 * @param integer $hour
	 * @return string
	 */
	public static function getHourOptionList($hour) {
		$result = '';

		for ($i = 0; $i <= 23; $i++) {
			$selected = '';

			if ($hour > 0 
				&& $hour === $i
			) {
				$selected = 'selected';
			} elseif ($hour === 0 
				&& $i == 9
			) {
				$selected = 'selected';
			}

			if ($i < 10) {
				$i = '0' . $i;
			}

			$result .= FormUtility::createOptionItem(
				array(
					'value' => $i,
					'selected' => $selected
				),
				$i
			);
		}

		return $result;
	}

	/**
	 * getMinutesOptionList
	 * 
	 * @param integer $min
	 * @return string
	 */
	public static function getMinutesOptionList($min) {
		$result = '';

		for ($i = 0; $i <= 59; $i += 10) {
			$selected = '';

			if ($min != '00' 
				&& $min === $i
			) {
				$selected = 'selected';
			} elseif ($i == 0) {
				$selected = 'selected';
			}

			if ($i < 10) {
				$i = '0' . $i;
			}

			$result .= FormUtility::createOptionItem(
				array(
					'value' => $i,
					'selected' => $selected
				),
				$i
			);
		}

		return $result;
	}
	
	/**
	 * getUserCheckboxItems
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $userDataArray
	 * @return string
	 */
	public static function getUserCheckboxItems(\Packages\Core\Persistence\ArrayIterator $userDataArray) {
		$userCheckboxItems = '';
		
		foreach ($userDataArray as $userItemEntity) {
			/* @var $userItemEntity \Packages\Core\Domain\Model\UserEntity */
			if (\strlen($userItemEntity->getEmail()) > 0 
				&& (
					\strlen($userItemEntity->getVorname()) > 0 
					|| \strlen($userItemEntity->getNachname()) > 0 
				)
			) {
				$userCheckboxItems .= FormUtility::createCheckboxItem(
					array(
						'name' => 'actions[sendNotification][emails][]',
						'value' => $userItemEntity->getEmail()
					),
					$userItemEntity->getVorname() . ' ' . $userItemEntity->getNachname()
				);
			}
		}
		
		return $userCheckboxItems;
	}
	
	/**
	 * getClientClickProfilesOptionList
	 * 
	 * @param string $fieldType
	 * @param \Packages\Core\Persistence\ObjectStorage $clientClickProfiles
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return string
	 */
	public static function getClientClickProfilesOptionList($fieldType, \Packages\Core\Persistence\ObjectStorage $clientClickProfiles, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$resultSelectionItems = '';
		
		foreach ($clientClickProfiles as $clientClickProfile) {
			/* @var $clientClickProfile \Packages\Core\Domain\Model\ClientClickProfileEntity */
			
			$resultSelectionItems .= FormUtility::createOptionItem(
				array(
					'value' => $clientClickProfile->getUid(),
					'selected' => self::getSelectedClientClickProfileFromFieldType(
						$campaignEntity,
						$clientClickProfile,
						$fieldType
					)
				),
				$clientClickProfile->getClickProfileEntity()->getTitle() . ' (' . $clientClickProfile->getClickProfileEntity()->getShortcut() . ')'
			);
		}
		
		return $resultSelectionItems;
	}
	
	/**
	 * getCustomerOptionList
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $customersDataArray
	 * \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @return string
	 */
	public static function getCustomerOptionList(\Packages\Core\Persistence\ArrayIterator $customersDataArray, \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity) {
		$resultSelectItem = '';
		
		foreach ($customersDataArray as $customerEntity) {
			/* @var $customerEntity \Modules\Customer\Domain\Model\CustomerEntity */
			
			$selected = '';
			if ($campaignEntity->getContactPersonEntity() instanceof \Modules\Customer\Domain\Model\ContactPersonEntity) {
				if ($customerEntity->getKunde_id() === $campaignEntity->getContactPersonEntity()->getCustomerEntity()->getKunde_id()) {
					$selected = 'selected';
				}
			} else {
				// nur für ältere kampagnen
				if ($customerEntity->getKunde_id() === $campaignEntity->getAgentur_id()) {
					$selected = 'selected';
				}
			}
			
			$resultSelectItem .= FormUtility::createOptionItem(
				array(
					'value' => $customerEntity->getKunde_id(),
					'selected' => $selected
				),
				$customerEntity->getFirma()
			);
		}
		
		return $resultSelectItem;
	}
	
	/**
	 * getGenderSelectionOptionList
	 * 
	 * @param boolean $isNewCampaign
	 * @param array $targetGroupDataArray
	 * @return string
	 */
	public static function getGenderSelectionOptionList($isNewCampaign = true, array $targetGroupDataArray = array()) {
		$resultSelectionItem = '';
		
		foreach (self::$genderDataArray as $genderKey => $genderItem) {
			$checked = '';
			if ($isNewCampaign === true 
				&& (int) $genderKey === 0
			) {
				$checked = 'checked';
			} else {
				if (\in_array($genderItem, $targetGroupDataArray) === true 
					|| (
						$genderKey === 0 
						&& \in_array('Damen und Herren', $targetGroupDataArray) === true
					)
				) {
					$checked = 'checked';
				}
			}
			
			$resultSelectionItem .= FormUtility::createRadioboxItemWidthLabel(
				array(
					'name' => 'campaign[genderSelection]',
					'id' => 'campaign_genderSelection_' . $genderKey,
					'value' => $genderKey,
					'checked' => $checked
				),
				$genderItem,
				array(
					'for' => 'campaign_genderSelection_' . $genderKey,
				)
			);
		}
		
		return $resultSelectionItem;
	}

	/**
	 * getCampaignDeliverySystemStartDateRadios
	 * 
	 * @param array $dataArray
	 * @param string $checkedItem
	 * @param boolean $disabled
	 * @return string
	 */
	public static function getCampaignDeliverySystemStartDateRadios(array $dataArray, $checkedItem, $disabled) {
		$result = '';

		foreach ($dataArray as $key => $value) {
			$checked = '';
			if ((int) $key === (int) $checkedItem) {
				$checked = 'checked';
			}

			$result .= FormUtility::createRadioboxItemWidthLabel(
				array(
					'name' => 'campaign[use_campaign_start_date]',
					'id' => 'campaign_use_campaign_start_date_' . $key,
					'value' => $key,
					'checked' => $checked,
					'disabled' => ($disabled ? 'disabled' : ''),
				),
				$value,
				array(
					'for' => 'campaign_use_campaign_start_date_' . $key,
				)
			);
		}

		return $result;
	}
	
	/**
	 * findGenderSelection
	 * 
	 * @param string $targetGroupSelection
	 * @return integer
	 */
	public static function findGenderSelection($targetGroupSelection) {
		$result = 0;
		
		$genderDataArray = self::$genderDataArray;
		$genderDataArray[0] = 'Damen und Herren';
		foreach ($genderDataArray as $key => $item) {
			if ((\strpos($targetGroupSelection, $item)) !== FALSE) {
				$result = $key;
				break;
			}
		}
		
		return $result;
	}
	

	/**
	 * getSelectedClientClickProfileFromFieldType
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param \Packages\Core\Domain\Model\ClientClickProfileEntity $clientClickProfile
	 * @param string $fieldType
	 * @return string
	 */
	protected static function getSelectedClientClickProfileFromFieldType(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, \Packages\Core\Domain\Model\ClientClickProfileEntity $clientClickProfile, $fieldType) {
		$result = '';

		switch ($fieldType) {
			case 'campaign_click_profiles_id':
				if ($campaignEntity->getCampaign_click_profiles_id() === $clientClickProfile->getUid()) {
					$result = 'selected';
				}
				break;

			case 'selection_click_profiles_id':
				if ($campaignEntity->getSelection_click_profiles_id() === $clientClickProfile->getUid()) {
					$result = 'selected';
				}
				break;
		}

		return $result;
	}

}