<?php
namespace Modules\Campaign\Utility;

use Packages\Core\Utility\SystemHelperUtility;
use Packages\Core\Utility\Html\TableUtility;
use Packages\Core\Utility\Html\HtmlUtility;
use Packages\Core\Utility\Html\FormUtility;

use Modules\Campaign\Utility\CampaignAndCustomerUtility;


/**
 * Description of CampaignAdvertisingsEntityUtility
 *
 * @author Cristian.Reus
 */
final class CampaignAdvertisingsEntityUtility {
	
	/**
	 * campaignAdvertisingsCssClass
	 * 
	 * @var string
	 */
	protected static $campaignAdvertisingsCssClass = 'campaignAdvertising_';
	
	
	
	/**
	 * getCampaignAdvertisingsByMimeType
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param string $mimeType
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public static function getCampaignAdvertisingsByMimeType(\Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, $mimeType) {
		$resultData = new \Packages\Core\Persistence\ArrayIterator();
		
		foreach ($campaignAdvertisings as $campaignAdvertising) {
			/* @var $campaignAdvertising \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
			
			if ($campaignAdvertising->getMime_type() === $mimeType) {
				$resultData->append($campaignAdvertising);
			}
		}
		
		return $resultData;
	}
	
	/**
	 * getLatestCampaignAdvertisingByMimeType
	 * 
	 * @param \Packages\Core\Persistence\ObjectStorage $campaignAdvertisings
	 * @param string $mimeType
	 * @return \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity
	 */
	public static function getLatestCampaignAdvertisingByMimeType(\Packages\Core\Persistence\ObjectStorage $campaignAdvertisings, $mimeType) {
		// TODO: besser lösen
		$result = self::getCampaignAdvertisingsByMimeType(
			$campaignAdvertisings,
			$mimeType
		)->getArrayCopy();
		
		return \current($result);
	}
	
	/**
	 * createCampaignAdvertisingsItemsResult
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $campaignAdvertisings
	 * @param string $advertisingType
	 * @param \Packages\Core\Persistence\ArrayIterator $userDataArray
	 * @param string $targetDir
	 * @return string
	 */
	public static function createCampaignAdvertisingsItemsResult(\Packages\Core\Persistence\ArrayIterator $campaignAdvertisings, $advertisingType, \Packages\Core\Persistence\ArrayIterator $userDataArray, $targetDir) {
		$tableDataArray = TableUtility::createTable(
			array(
				'class' => 'neu_k extendedTable advertisingsItems',
				'cellpadding' => 0,
				'cellspacing' => 0,
				'border' => 0
			)
		);
		$tableHeadDataArray = TableUtility::createTHeadTag();
		$tableFooterDataArray = TableUtility::createTableFooterRow();
		$tableBodyDataArray = TableUtility::createTBodyTag();
		
		$result = 
			$tableDataArray['begin'] 
				. $tableHeadDataArray['begin'] 
					. self::createCampaignAdvertisingsTableHeaderResult($advertisingType)
				. $tableHeadDataArray['end']
				. $tableFooterDataArray['begin']
					. TableUtility::createTableHeaderCellWidthContent(
						array(
							'colspan' => 5,
						),
						'&nbsp;'
					)
				. $tableFooterDataArray['end']
				. $tableBodyDataArray['begin']
					. self::createCampaignAdvertisingsTableBodyResult(
						$campaignAdvertisings,
						$userDataArray,
						$targetDir
					)
				. $tableBodyDataArray['end']
			. $tableDataArray['end']
		;
		
		return $result;
	}
	
	/**
	 * getCampaignAdvertisingsContentResult
	 * 
	 * @param \Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity
	 * @param \Packages\Core\Persistence\ArrayIterator $userDataArray
	 * @param string $targetDir
	 * @return string
	 */
	public static function getCampaignAdvertisingsContentResult(\Modules\Campaign\Domain\Model\CampaignEntity $campaignEntity, \Packages\Core\Persistence\ArrayIterator $userDataArray, $targetDir) {
		$result = '';
		
		if ($campaignEntity->getCampaignAdvertisings()->count() > 0) {
			$campaignAdvertisingsContent = '';
			
			$tableBodyRowDataArray = TableUtility::createTableRow();
			$divDataArray = HtmlUtility::createDiv(
				array(
					'style' => 'text-align: right; background: #E4E4E4; margin: -5px; padding: 5px;'
				)
			);
			
			$htmlCampaignAdvertisings = self::getCampaignAdvertisingsByMimeType(
				$campaignEntity->getCampaignAdvertisings(),
				CampaignAndCustomerUtility::$mailtypDataArray['h']
			);
			$campaignEntity->getCampaignAdvertisings()->rewind();

			$txtCampaignAdvertisings = self::getCampaignAdvertisingsByMimeType(
				$campaignEntity->getCampaignAdvertisings(),
				CampaignAndCustomerUtility::$mailtypDataArray['t']
			);
			$campaignEntity->getCampaignAdvertisings()->rewind();

			$imagesCampaignAdvertisings = self::getCampaignAdvertisingsByMimeType(
				$campaignEntity->getCampaignAdvertisings(),
				'Image'
			);
			$campaignEntity->getCampaignAdvertisings()->rewind();

			$blacklistCampaignAdvertisings = self::getCampaignAdvertisingsByMimeType(
				$campaignEntity->getCampaignAdvertisings(),
				'Blacklist'
			);
			$campaignEntity->getCampaignAdvertisings()->rewind();
			
			$othersCampaignAdvertisings = self::getCampaignAdvertisingsByMimeType(
				$campaignEntity->getCampaignAdvertisings(),
				'Others'
			);
			$campaignEntity->getCampaignAdvertisings()->rewind();
			
			if ($htmlCampaignAdvertisings->count() > 0) {
				$campaignAdvertisingsContent .= self::createCampaignAdvertisingsItemsResult(
					$htmlCampaignAdvertisings,
					CampaignAndCustomerUtility::$mailtypDataArray['h'],
					$userDataArray,
					$targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR
				);
			}

			if ($txtCampaignAdvertisings->count() > 0) {
				$campaignAdvertisingsContent .= self::createCampaignAdvertisingsItemsResult(
					$txtCampaignAdvertisings,
					CampaignAndCustomerUtility::$mailtypDataArray['t'],
					$userDataArray,
					$targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR
				);
			}
			
			if ($imagesCampaignAdvertisings->count() > 0) {
				$campaignAdvertisingsContent .= self::createCampaignAdvertisingsItemsResult(
					$imagesCampaignAdvertisings,
					'Bilder',
					$userDataArray,
					$targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR
				);
			}
			
			if ($blacklistCampaignAdvertisings->count() > 0) {
				$campaignAdvertisingsContent .= self::createCampaignAdvertisingsItemsResult(
					$blacklistCampaignAdvertisings,
					'Blacklist',
					$userDataArray,
					$targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR
				);
			}
			
			if ($othersCampaignAdvertisings->count() > 0) {
				$campaignAdvertisingsContent .= self::createCampaignAdvertisingsItemsResult(
					$othersCampaignAdvertisings,
					'Sonstiges',
					$userDataArray,
					$targetDir . $campaignEntity->getK_id() . \DIRECTORY_SEPARATOR
				);
			}
			
			$result = $tableBodyRowDataArray['begin'] 
				. TableUtility::createTableCellWidthContent(
					array(
						'colspan' => 2
					),
					'&nbsp;'
				) 
				. $tableBodyRowDataArray['end'] 
				. $tableBodyRowDataArray['begin'] 
				. TableUtility::createTableCellWidthContent(
					array(
						'colspan' => 2
					),
					$divDataArray['begin'] 
						. HtmlUtility::createHyperlink(
							array(
								'href' => 'javascript:selectCampaignAdvertisingDownloadsByClassname(\'' . self::$campaignAdvertisingsCssClass . 'all' . '\');',
								'title' => 'Alle wählen'
							),
							'<i class="fa fa-check-square fa-lg"></i>'
						) . ' / ' . HtmlUtility::createHyperlink(
							array(
								'href' => 'javascript:unselectCampaignAdvertisingDownloadsByClassname(\'' . self::$campaignAdvertisingsCssClass . 'all' . '\');',
								'title' => 'Alle abwählen'
							),
							'<i class="fa fa-square fa-lg"></i>'
						)
					. $divDataArray['end']
						. $campaignAdvertisingsContent 
						. $divDataArray['begin'] 
							. FormUtility::createInputField(
								array(
									'id' => 'compressAndDownloadFile_1454414330',
									'onclick' => 'compressAndDownloadFile(\'' . self::$campaignAdvertisingsCssClass . 'all' . '\');',
									'value' => 'Download ZIP'
								),
								'button'
							) 
						. $divDataArray['end'] 
				) 
				. $tableBodyRowDataArray['end'] 
			;
			unset($tableBodyRowDataArray);
		}
		
		return $result;
	}
	
	/**
	 * createCampaignAdvertisingsTableBodyResult
	 * 
	 * @param \Packages\Core\Persistence\ArrayIterator $campaignAdvertisings
	 * @param \Packages\Core\Persistence\ArrayIterator $userDataArray
	 * @param string $targetDir
	 * @return string
	 */
	protected static function createCampaignAdvertisingsTableBodyResult(\Packages\Core\Persistence\ArrayIterator $campaignAdvertisings, \Packages\Core\Persistence\ArrayIterator $userDataArray, $targetDir) {
		$tableBodyResult = '';
		
		$tableBodyRowDataArray = TableUtility::createTableRow();
		$i = $campaignAdvertisings->count();
		foreach ($campaignAdvertisings as $campaignAdvertising) {
			/* @var $campaignAdvertising \Modules\Campaign\Domain\Model\CampaignAdvertisingEntity */
			
			$userName = '';
			$userEntity = SystemHelperUtility::getUserEntityFromDataArray(
				$userDataArray,
				$campaignAdvertising->getUser_id()
			);
			if ($userEntity instanceof \Packages\Core\Domain\Model\UserEntity) {
				$userName = $userEntity->getVorname() . ' ' . $userEntity->getNachname();
			}
			
			$tableBodyResult .= 
				$tableBodyRowDataArray['begin']
					. TableUtility::createTableCellWidthContent(
						array(),
						$i
					) 
					. TableUtility::createTableCellWidthContent(
						array(),
						$campaignAdvertising->getCrdate()->format('d.m.Y H:i:s')
					) 
					. TableUtility::createTableCellWidthContent(
						array(),
						$userName
					) 
					. TableUtility::createTableCellWidthContent(
						array(),
						HtmlUtility::createHyperlink(
							array(
								'href' => SystemHelperUtility::getUploadDirForHyperlinks($targetDir) . $campaignAdvertising->getFile(),
								'target' => '_blank',
								'title' => 'Vorschau',
								'style' => 'margin-right: 10px;'
							),
							'<i class="fa fa-eye fa-lg"></i>'
						) . ' ' .  HtmlUtility::createHyperlink(
							array(
								'href' => 'download.php?download[file]=' . \base64_encode($targetDir . $campaignAdvertising->getFile()),
								'target' => '_blank',
								'title' => 'Download'
							),
							'<i class="fa fa-download fa-lg"></i>'
						)
					). TableUtility::createTableCellWidthContent(
						array(
							'class' => 'align right'
						),
						FormUtility::createCheckbox(
							array(
								'name' => 'campaignAdvertising[download][]',
								'value' => $campaignAdvertising->getFile(),
								'class' => self::$campaignAdvertisingsCssClass . 'all',
								'onclick' => 'toogleCompressAndDownloadFile(this)'
							)
						)
					) 
				. $tableBodyRowDataArray['end']
			;
			
			$i--;
		}
		unset($tableBodyRowDataArray);
		
		return $tableBodyResult;
	}
	
	/**
	 * createCampaignAdvertisingsTableHeaderResult
	 * 
	 * @param string $advertisingType
	 * @return string
	 */
	protected static function createCampaignAdvertisingsTableHeaderResult($advertisingType) {
		$tableHeaderRowDataArray = TableUtility::createTableRow();
		
		$headerDataArray = array(
			'Version' => array(
				'width' => '12%'
			),
			'Datum/Uhrzeit' => array(
				'width' => '30%'
			),
			'Benutzer' => array(
				'width' => '40%'
			),
			'Aktionen' => array(
				'colspan' => 2
			)
		);
		$defaultHeaderItem = '';
		foreach ($headerDataArray as $key => $item) {
			$defaultHeaderItem .= TableUtility::createTableHeaderCellWidthContent(
				$item,
				$key
			);
		}
		
		return $tableHeaderRowDataArray['begin']
				. TableUtility::createTableHeaderCellWidthContent(
					array(
						'colspan' => 5,
						'class' => 'headerColumn'
					),
					$advertisingType
				)
			. $tableHeaderRowDataArray['end']
			. $tableHeaderRowDataArray['begin']
				. $defaultHeaderItem
			. $tableHeaderRowDataArray['end']
		;
	}
}
