<script type="text/javascript">
              function auswertung(){
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' 
          				+ '&deliverySystem=' + YAHOO.util.Dom.get('ds').value 
          				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd').value 
          				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp').value 
          				+ '&genderSystem=' + YAHOO.util.Dom.get('gs').value
          				+ '&reagierer=' + YAHOO.util.Dom.get('r').value 
          				+ '&alterVon=' + YAHOO.util.Dom.get('av').value 
          				+ '&alterBis=' + YAHOO.util.Dom.get('ab').value 
          				+ '&plzVon=' + YAHOO.util.Dom.get('pv').value 
          				+ '&plzBis=' + YAHOO.util.Dom.get('pb').value 
          				+ '&auswertung=' + true
          			,
          			'st'
          		);
               }
              function verwenden(){
				  var wert = YAHOO.util.Dom.get('ergebnis').innerHTML;
				  var ergebnis = wert.replace("." , "");
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=verwenden' 
          				+ '&deliverySystem=' + YAHOO.util.Dom.get('ds_v').innerHTML 
          				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd_v').innerHTML 
          				+ '&datum=' + YAHOO.util.Dom.get('d_v').innerHTML 
          				+ '&benutzer=' + YAHOO.util.Dom.get('b_v').innerHTML 
          				+ '&clientClickProfileShort=' + YAHOO.util.Dom.get('ccps_v').value 
          				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp_v').value 
          				+ '&genderSystem=' + YAHOO.util.Dom.get('gs_v').innerHTML
          				+ '&reagierer=' + YAHOO.util.Dom.get('r_v').innerHTML 
          				+ '&alterVon=' + YAHOO.util.Dom.get('av_v').innerHTML 
          				+ '&alterBis=' + YAHOO.util.Dom.get('ab_v').innerHTML 
          				+ '&plzVon=' + YAHOO.util.Dom.get('pv_v').innerHTML 
          				+ '&plzBis=' + YAHOO.util.Dom.get('pb_v').innerHTML 
          				+ '&ergebnis=' + ergebnis
          				+ '&verwenden=' + true
          			,
          			'st'
          		);
               }
              function vorhanden(){
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' 
                		    + '&deliverySystem=' + YAHOO.util.Dom.get('ds').value 
            				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd').value 
            				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp').value 
            				+ '&genderSystem=' + YAHOO.util.Dom.get('gs').value
            				+ '&reagierer=' + YAHOO.util.Dom.get('r').value 
            				+ '&alterVon=' + YAHOO.util.Dom.get('av').value 
            				+ '&alterBis=' + YAHOO.util.Dom.get('ab').value 
            				+ '&plzVon=' + YAHOO.util.Dom.get('pv').value 
            				+ '&plzBis=' + YAHOO.util.Dom.get('pb').value 
          				    + '&vorhanden=' + true
          			,
          			'st'
          		);
               }
              function zuruecksetzen(){
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' 
                		    + '&deliverySystem=' + YAHOO.util.Dom.get('ds').value 
            				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd').value 
            				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp').value 
            				+ '&genderSystem=' + YAHOO.util.Dom.get('gs').value
            				+ '&reagierer=' + YAHOO.util.Dom.get('r').value 
            				+ '&alterVon=' + YAHOO.util.Dom.get('av').value 
            				+ '&alterBis=' + YAHOO.util.Dom.get('ab').value 
            				+ '&plzVon=' + YAHOO.util.Dom.get('pv').value 
            				+ '&plzBis=' + YAHOO.util.Dom.get('pb').value 
          				    + '&anzeigen=' + true
          			,
          			'st'
          		);
               }
              function verwerfen(){
				  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung'
					        + '&deliverySystem=' + YAHOO.util.Dom.get('ds').value 
            				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd').value 
					        + '&anzeigen=' + true
					,
          			'st'
          		);
			  }
              <?php foreach ($resultAuswertung['statistiken'] as $key => $statistiken){?>
              function aktualisieren<?php echo $key;?>(){
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' 
          				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd_a<?php echo $key;?>').innerHTML  
          				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp_a<?php echo $key;?>').innerHTML
          				+ '&genderSystem=' + YAHOO.util.Dom.get('gs_a<?php echo $key;?>').innerHTML
          				+ '&reagierer=' + YAHOO.util.Dom.get('r_a<?php echo $key;?>').innerHTML 
          				+ '&alterVon=' + YAHOO.util.Dom.get('av_a<?php echo $key;?>').innerHTML 
          				+ '&alterBis=' + YAHOO.util.Dom.get('ab_a<?php echo $key;?>').innerHTML 
          				+ '&plzVon=' + YAHOO.util.Dom.get('pv_a<?php echo $key;?>').innerHTML 
          				+ '&plzBis=' + YAHOO.util.Dom.get('pb_a<?php echo $key;?>').innerHTML
          				+ '&funktionName=' + YAHOO.util.Dom.get('fn_a<?php echo $key;?>').value
				        + '&key='+ YAHOO.util.Dom.get('id_a<?php echo $key;?>').innerHTML
          				+ '&aktualisieren=' + true
          			,
          			'st'
          		);
               }
              function anlegen<?php echo $key;?>(){
                  setRequest('dispatch.php?_module=Statistic&_controller=index&_action=anlegen' 
          				+ '&deliverySystem=' + YAHOO.util.Dom.get('ds_a<?php echo $key;?>').innerHTML  
          				+ '&deliverySystemDistributor=' + YAHOO.util.Dom.get('dsd_a<?php echo $key;?>').innerHTML  
          				+ '&clientClickProfile=' + YAHOO.util.Dom.get('ccp_a<?php echo $key;?>').innerHTML
          				+ '&genderSystem=' + YAHOO.util.Dom.get('gs_a<?php echo $key;?>').innerHTML
          				+ '&reagierer=' + YAHOO.util.Dom.get('r_a<?php echo $key;?>').innerHTML 
          				+ '&alterVon=' + YAHOO.util.Dom.get('av_a<?php echo $key;?>').innerHTML 
          				+ '&alterBis=' + YAHOO.util.Dom.get('ab_a<?php echo $key;?>').innerHTML 
          				+ '&plzVon=' + YAHOO.util.Dom.get('pv_a<?php echo $key;?>').innerHTML 
          				+ '&plzBis=' + YAHOO.util.Dom.get('pb_a<?php echo $key;?>').innerHTML
          				+ '&funktionName=' + YAHOO.util.Dom.get('fn_a<?php echo $key;?>').value
          				+ '&id=' + YAHOO.util.Dom.get('id_a<?php echo $key;?>').innerHTML  
          				+ '&anlegen=' + true
          			,
          			'st'
          		);
               }
YAHOO.namespace("example.container");

YAHOO.util.Event.onDOMReady(function () {
	
function validateStatisticData(){
	var statisticErrorMessage = '';
	if( YAHOO.util.Dom.get('titel<?php echo $key;?>').value === '')  {
		statisticErrorMessage = " Der Titel muss nicht leer sein\n";
	}
	return statisticErrorMessage;

}
function replaceUmlauts(string)
{
    value = string.toLowerCase();
    value = value.replace(/ä/g, 'ae');
    value = value.replace(/ö/g, 'oe');
    value = value.replace(/ü/g, 'ue');
    return value;
}
	// Define various event handlers for Dialog
	var handleSubmit = function() {
		var validateResult = validateStatisticData();

		if (validateResult.length > 0) {
			alert(validateResult);	
			return false;
		} else {
			var title = replaceUmlauts(YAHOO.util.Dom.get('titel<?php echo $key;?>').value);
		setRequest('dispatch.php?_module=Statistic&_controller=index&_action=update'
		                    + '&titel=' +title 
					        + '&key='+ YAHOO.util.Dom.get('id_h<?php echo $key;?>').value
					,
          			'st'
          		);
	     };
	}
	var handleCancel = function() {
		this.cancel();
	};
    
    // Remove progressively enhanced content class, just before creating the module
    YAHOO.util.Dom.removeClass("dialog<?php echo $key;?>", "yui-pe-content");

	// Instantiate the Dialog
	YAHOO.example.container.dialog<?php echo $key;?> = new YAHOO.widget.Dialog("dialog<?php echo $key;?>", 
							{	width: '500px',
		                        fixedcenter: true,
	                            visible: false,
								constraintoviewport: true,
							  buttons : [ { 
									  text:"speichern", handler:handleSubmit, isDefault:true 
								  },{ 
									  text:"abbrechen", handler:handleCancel } ]
							});

	YAHOO.example.container.dialog<?php echo $key;?>.render();

	YAHOO.util.Event.addListener("update<?php echo $key;?>", "click", YAHOO.example.container.dialog<?php echo $key;?>.show, YAHOO.example.container.dialog<?php echo $key;?>, true);
});


              <?php }?>

// Validiert eine Ganzzahl.
function is_integer(x , id) {
 
	if(x !== ''){
	var	intRegex = /^\d+$/;
    var y=parseInt(x);  
		if (isNaN(y) || !intRegex.test(y)){
			YAHOO.util.Dom.get(id).value = '';
			alert('Bitte Geben Sie eine Ganzzahl ein!!');
		} 
	}
}
</script>

