<?php
namespace Modules\Statistic\Controller;

use Packages\Core\Mvc\Controller\AbstractController;
use Packages\Core\Factory\DeliverySystem;
use Packages\Core\Persistence\Generic\Query;
use Modules\Statistic\Utility\StatisticUtility;

/**
 * Description of IndexController
 * @author MohamedSamiJarmoud
 *
 */
class IndexController extends AbstractController {
  
	/**
	 * repository
	 *
	 * @var \Modules\Statistic\Domain\Repository\StatistikAuswertungRepository
	 */
	protected $repository = NULL;
	
	/**
	 * postInitAction
	 *
	 * @return void
	 */
	protected function postInitAction() {
		
	}

	/**
	 * initDefaultViewDataArray
	 *
	 * @param array $actionRequestDataArray
	 * @return void
	 */
	protected function initDefaultViewDataArray(array $actionRequestDataArray) {
		$this->defaultViewDataArray['actionRequestDataArray'] = $actionRequestDataArray;
		$this->defaultViewDataArray['clientEntity'] = $this->clientEntity;
		$this->defaultViewDataArray['settingsDataArray'] = $this->settings;
	}

	public function auszaelungAction(array $actionRequestDataArray) {
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$clientDeliveries = $this->clientEntity->getClientDeliveries();
		$this->logger->log($clientDeliveries, '$clientDeliveries');
		$resultAuswertung ='';
         
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		if (isset($actionRequestDataArray ["deliverySystem"]) 
			&& (int) $actionRequestDataArray ["deliverySystem"] > 0
				
		) {
			if((int) $actionRequestDataArray ["deliverySystem"] === 4){
						?>
		<script type="text/javascript">
			alert('Kajomi ist zur Zeit nicht möglich');
			setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' + '&anzeigen=' + true, 'st');
		</script>
			<?php 
			}
			$deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
				$this->clientEntity->getId(),
				(int) $actionRequestDataArray ["deliverySystem"],
				$_SESSION['underClientIds'] //TODO: anders l�sen
			);
		
			if (isset($actionRequestDataArray['deliverySystemDistributor']) 
				&& (int) $actionRequestDataArray['deliverySystemDistributor'] > 0
			) {
				$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById((int) $actionRequestDataArray['deliverySystemDistributor']);			
				$deliverySystem = DeliverySystem::getAndInitDeliverySystemInstance(
					'Statistic',
					$deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp(),
					$deliverySystemDistributorEntity->getClientDeliveryEntity()
				);
				
				$statistiken = $this->repository->getStatistikAuswertung();	
				$versandSystem = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
				$benutzer = $this->loggedUserEntity->getName();
				
				/* @var $deliverySystem \Packages\Api\DeliverySystem\Statistic\AbstractStatistic */
				if(isset($actionRequestDataArray['auswertung'])){
				if( $actionRequestDataArray['auswertung'] == true) {
				
				$actionRequestDataArray['clientClickProfileItem'] = $this->getClickProfile($actionRequestDataArray['clientClickProfile']);
				$actionRequestDataArray['genderSystemItem'] = $this->getGender($actionRequestDataArray['genderSystem']);
				$filternStatistiken = $this->processFiltern($actionRequestDataArray);
				 if(count($filternStatistiken) > 0
					  ){
				        ?>
							<script type="text/javascript">
									var Check = confirm("Ihre statistische Auswertung ist schon vorhanden.\n Wollen Sie die Vorhandene Auswertung sehen?");
									if( Check == true){
									   vorhanden(); 
									}							
							</script>
		              <?php
				     }
				$resultAuswertung['result'] = $deliverySystem->processStatisticAuszaelung($actionRequestDataArray, $deliverySystemDistributorEntity->getDistributor_id() );	
				$resultAuswertung['result']['data'] = StatisticUtility::getNettoResult((int) $actionRequestDataArray['deliverySystemDistributor'], $resultAuswertung['result']['data']);
				$resultAuswertung['verteilerTitle'] = $deliverySystemDistributorEntity->getTitle();
				$resultAuswertung['versandSystem'] = $versandSystem;
				$resultAuswertung['benutzer'] = $benutzer;
				$resultAuswertung['statistiken'] = $statistiken;
				
				$this->logger->log($resultAuswertung, '$resultAuswertung');

				 }
				}
	            
				// disconnect
				unset($deliverySystem);
			}
		} else {
			$deliverySystemDistributorDataArray = NULL;
		}
		if(isset($actionRequestDataArray['vorhanden'])){
			if( $actionRequestDataArray['vorhanden'] == true) {
				$actionRequestDataArray['clientClickProfileItem'] = $this->getClickProfile($actionRequestDataArray['clientClickProfile']);
				$actionRequestDataArray['genderSystemItem'] = $this->getGender($actionRequestDataArray['genderSystem']);
				$filternStatistiken = $this->processFiltern($actionRequestDataArray);
				$resultAuswertung['verteilerTitle'] = $actionRequestDataArray["deliverySystemDistributor"];
				$resultAuswertung['versandSystem'] = $actionRequestDataArray["deliverySystem"];;
				$resultAuswertung['statistiken'] = $filternStatistiken;
				$this->logger->log($resultAuswertung, '$resultAuswertung');
				
				$clientDeliveries = $this->clientEntity->getClientDeliveries();
				$this->logger->log($clientDeliveries, '$clientDeliveries');
				$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
				$this->logger->log($clientClickProfiles, '$clientClickProfiles');
				
				if (isset($actionRequestDataArray ["deliverySystem"])
						&& (int) $actionRequestDataArray ["deliverySystem"] > 0
						) {
							$deliverySystemDistributorDataArray = $this->systemRepository->findDeliverySystemDistributorsDataArrayByClientIdAndDeliverySystemId(
									$this->clientEntity->getId(),
									(int) $actionRequestDataArray ["deliverySystem"],
									$_SESSION['underClientIds'] //TODO: anders l�sen
									);
				
				if (isset($actionRequestDataArray['deliverySystemDistributor'])
					   && (int) $actionRequestDataArray['deliverySystemDistributor'] > 0
									) {
							$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorById((int) $actionRequestDataArray['deliverySystemDistributor']);
							$deliverySystem = DeliverySystem::getAndInitDeliverySystemInstance(
							'Statistic',
							$deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp(),
							$deliverySystemDistributorEntity->getClientDeliveryEntity()
							);

					$versandSystem = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
					$benutzer = $this->loggedUserEntity->getName();
					/* @var $deliverySystem \Packages\Api\DeliverySystem\Statistic\AbstractStatistic */

										// disconnect
										unset($deliverySystem);
									}
						} else {
							$deliverySystemDistributorDataArray = NULL;
						}
			}
		}
		if(isset($actionRequestDataArray['anzeigen'])){
			if( $actionRequestDataArray['anzeigen'] == true) {
			$statistiken = $this->repository->getStatistikAuswertung();
			
			$resultAuswertung['statistiken'] = $statistiken;
			$this->logger->log($resultAuswertung, '$resultAuswertung');
		}
		}
		
		if(isset($actionRequestDataArray['aktualisieren'])){
			if( $actionRequestDataArray['aktualisieren'] == true) {
				$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorByTitle($actionRequestDataArray['deliverySystemDistributor']);
				$deliverySystem = DeliverySystem::getAndInitDeliverySystemInstance(
						'Statistic',
						$deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp(),
						$deliverySystemDistributorEntity->getClientDeliveryEntity()
						);
				$statistiken = $this->repository->getStatistikAuswertung();
				$versandSystem = $deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp();
				$benutzer = $this->loggedUserEntity->getName();
				$actionRequestDataArray['genderSystem'] = $this->getGenderByID($actionRequestDataArray['genderSystem']);
				
				$newResults = $deliverySystem->processStatisticAuszaelung($actionRequestDataArray, $deliverySystemDistributorEntity->getDistributor_id() );
			    $newNettoResults['data'] = StatisticUtility::getNettoResult($deliverySystemDistributorEntity->getId(), $newResults['data']);
				#\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributorEntity, '$deliverySystemDistributorEntity');
				$resultAuswertung['verteilerTitle'] = $deliverySystemDistributorEntity->getTitle();
				$resultAuswertung['versandSystem'] = $versandSystem;
				$resultAuswertung['benutzer'] = $benutzer;
			    $this->updateStatistikResult($actionRequestDataArray['key'], $benutzer, $newNettoResults['data']);
				$resultAuswertung['statistiken'] = $statistiken;
				$this->logger->log($resultAuswertung, '$resultAuswertung');	
				?>
		<script type="text/javascript">
			setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung' + '&anzeigen=' + true, 'st');
		</script>
			<?php 
			}
		}
		$this->initDefaultViewDataArray($actionRequestDataArray);
		$this->initView(
			array(
				'clientDeliveries' => $clientDeliveries,
				'deliverySystemDistributorDataArray' => $deliverySystemDistributorDataArray,
				'clientClickProfiles' => $clientClickProfiles,
				'resultAuswertung' => $resultAuswertung
			)
		);

		$this->logger->endGroup();
	}
    
	public function verwendenAction(array $actionRequestDataArray){
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');

		$this->initDefaultViewDataArray($actionRequestDataArray);
		$funktionName = $this->getFunktionName($actionRequestDataArray);
		
		$clientClickProfiles = $this->clientEntity->getClientClickProfiles();
		$this->logger->log($clientClickProfiles, '$clientClickProfiles');
		
		$clickProfile = StatisticUtility::getClientClickProfileByName(
				                                  $clientClickProfiles, 
				                                  $actionRequestDataArray['clientClickProfile']
				);
        $this->processSpeichern($actionRequestDataArray, $funktionName, $clickProfile);
		
		$this->logger->endGroup();
	}
	public function anlegenAction(array $actionRequestDataArray){
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$deliverySystemDistributorEntity = $this->systemRepository->findDeliverySystemDistributorByTitle($actionRequestDataArray['deliverySystemDistributor']);
		$deliverySystem = DeliverySystem::getAndInitDeliverySystemInstance(
				'Statistic',
				$deliverySystemDistributorEntity->getClientDeliveryEntity()->getDeliverySystemEntity()->getAsp(),
				$deliverySystemDistributorEntity->getClientDeliveryEntity()
				);
		
		if(isset($actionRequestDataArray['anlegen'])){
			if( $actionRequestDataArray['anlegen'] == true) {
				
				$client_id = $this->clientEntity->getId();
				if ( \intval($client_id)<= 0) {
			            throw new \LengthException('invalid client id', 2449492163);
		             }
				$resultAuswertung['result'] = $deliverySystem->processStatisticAnlegen($actionRequestDataArray, $deliverySystemDistributorEntity->getDistributor_id(), \intval($client_id) );		
			    
				$statistik = $this->repository->findStatistikById((int)$actionRequestDataArray['id']);
				$statistik->setIsApply(1);
				$statistik->setInternId($resultAuswertung['result']['zielgruppeID']);
				/**
				 * updateStatistik
				 */
				$this->updateStatistik(
						$statistik->getId(),
						$statistik->getInternId(),
						$statistik->getIsApply()
						);
				$this->logger->log($statistik, '$statistik -> after');
			   }
			}
		#\Packages\Core\Utility\DebugUtility::debug($deliverySystemDistributorEntity, '$deliverySystemDistributorEntity');
		#$resultAuswertung['result'] = $deliverySystem->processStatisticAuszaelung($actionRequestDataArray, $deliverySystemDistributorEntity->getDistributor_id(), $anlegen );
	}
	
	public function updateAction(array $actionRequestDataArray){
		
		$this->logger->beginGroup(__FUNCTION__);
		$this->logger->log($actionRequestDataArray, '$actionRequestDataArray');
		
		$auszaelungId = isset($actionRequestDataArray['key']) ? \intval($actionRequestDataArray['key']) : 0;
		if ($auszaelungId <= 0) {
			throw new \LengthException('invalid statistik id', 1447419557);
		}

		$titel = isset($actionRequestDataArray['titel']) ? \strval($actionRequestDataArray['titel']) : "";
        $statistik = $this->repository->findById($auszaelungId);
		$statistik->setTitel($titel);
		

	
		$this->updateTitel($statistik);
	}
	protected function processSpeichern(array $actionRequestDataArray , $funktionName, $clickProfile) {
	
			$query = 'INSERT INTO ' . Query::masketField($this->settings['repositorySettings']['auswertung']['table'])
			        . ' ( '
			        . Query::masketField('funktion_name').','
					. Query::masketField('titel').','
			        . Query::masketField('datum').','
			        . Query::masketField('benutzer').','
					. Query::masketField('klickprofil_id').','
			        . Query::masketField('klickprofil').','
			        . Query::masketField('geschlecht').','
			        . Query::masketField('reagierer').','
			        . Query::masketField('alter_von').','
			        . Query::masketField('alter_bis').','
			        . Query::masketField('plz_von').','
			        . Query::masketField('plz_bis').','
			        . Query::masketField('status').','
			        . Query::masketField('ergebnis').','
			        . Query::masketField('versandsystem').','
			        . Query::masketField('verteiler')
		            . ') VALUES'
		            . ' ( '
					.$this->repository->quoteString($funktionName).','
					.$this->repository->quoteString($funktionName).','
					.$this->repository->quoteString(StatisticUtility::mysqldatum($actionRequestDataArray["datum"])).','
					.$this->repository->quoteString($actionRequestDataArray["benutzer"]).','
					.$this->repository->quoteString($actionRequestDataArray["clientClickProfile"]).','
					.$this->repository->quoteString($clickProfile).','
					.$this->repository->quoteString($actionRequestDataArray["genderSystem"]).','
					.$this->repository->quoteString($actionRequestDataArray["reagierer"]).','
				    .$this->repository->quoteString($actionRequestDataArray["alterVon"]).','
				    .$this->repository->quoteString($actionRequestDataArray["alterBis"]).','
				    .$this->repository->quoteString($actionRequestDataArray["plzVon"]).','
				    .$this->repository->quoteString($actionRequestDataArray["plzBis"]).','
				    .$this->repository->quoteString("Erledigt").','
				    .$this->repository->quoteString($actionRequestDataArray["ergebnis"]).','
				    .$this->repository->quoteString($actionRequestDataArray["deliverySystem"]).','
					.$this->repository->quoteString($actionRequestDataArray["deliverySystemDistributor"])
					.');'
				;
        
	    $result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);
    ?>
			<script type="text/javascript">
			setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung'
					    + '&anzeigen=' + true
					    , 'st');
			</script>
	<?php 	

	}
	protected function updateTitel(\Modules\Statistic\Domain\Model\StatistikAuswertungEntity $statistik){
		
		$result = $this->repository->update($statistik);
		
		 $this->logger->log($result, __FUNCTION__);
	  ?>
			<script type="text/javascript">
			setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung'
					    + '&anzeigen=' + true
					    , 'st');
			</script>
    <?php 
		
	}
	protected function getFunktionName(array $actionRequestDataArray){
    	$funktionName = '';
    	   if(!empty($actionRequestDataArray["clientClickProfileShort"])){
    	   	$funktionName .= $actionRequestDataArray["clientClickProfileShort"];
    	   }
    	   if(!empty($actionRequestDataArray["genderSystem"]) && ($actionRequestDataArray["genderSystem"] != 'beide') ){
    	   	$funktionName .= ' '.$actionRequestDataArray["genderSystem"];   		
    	   }
    	   if(!empty($actionRequestDataArray["alterVon"])){
    	   	$funktionName .= ' alter '.$actionRequestDataArray["alterVon"];   	   		
    	   }
    	   if(!empty($actionRequestDataArray["alterBis"])){
    	   	$funktionName .= '-'.$actionRequestDataArray["alterBis"];    	   		
    	   }
    	   if(!empty($actionRequestDataArray["plzVon"])){
    	   	$funktionName .= ' plz '.$actionRequestDataArray["plzVon"];   	   		
    	   }
    	   if(!empty($actionRequestDataArray["plzBis"])){
    	   	$funktionName .= '-'.$actionRequestDataArray["plzBis"]; 	   		
    	   }
    	   if(!empty($actionRequestDataArray["reagierer"])){
    	   	$funktionName .= ' reagierer '.$actionRequestDataArray["reagierer"];
    	   }
 	   return $funktionName;         
    }
    protected function processFiltern(array $actionRequestDataArray){
    	$query = $this->repository->findStatistikenByParameter($actionRequestDataArray); 
    	return $query;
    }
	protected function getClickProfile($clickProfile){
    	switch ($clickProfile){
                case 0:
    			$clickProfileData = '';
    			break;
    		    case 1:
    			$clickProfileData = 'Wirtschaft, Finanzen & Politik';
    			break;
    		    case 2:
    			$clickProfileData = 'Lifestyle & Mode';
    			break;
                    case 3:
    			$clickProfileData = 'Telekommunikation';
    			break;
                    case 4:
    			$clickProfileData = 'Technik und Auto';
    			break;
                    case 5:
    			$clickProfileData = 'Reise, Urlaub, Wellness & Gesundheit ';
    			break;
                    case 6:
    			$clickProfileData = 'Unterhaltung & Hobby';
    			break;
                    case 7:
    			$clickProfileData = 'Spiel Glückspiel';
    			break;
                    case 8:
    			$clickProfileData = 'Luxus/ Essen & Trinken';
    			break;
                    case 9:
    			$clickProfileData = 'Tiere';
    			break;
                    case 10:
    			$clickProfileData = 'Familie & Soziales';
    			break;
                    case 11:
    			$clickProfileData = 'Dating';
    			break;
                    case 12:
    			$clickProfileData = 'Sonstiges';
    			break;
    		default:
    			$clickProfileData = '';
    	}
    	return $clickProfileData;
    }
    protected function getGender($gender){
    	switch ($gender){
    		case 0:
    			$genderData = '';
    			break;
    		case 1:
    			$genderData = 'Herr';
    			break;
    		case 2:
    			$genderData = 'Frau';
    			break;
    		default:
    			$genderData = '';
    	}
    	return $genderData;
    }	
    protected function getGenderByID($gender){
    	switch ($gender){
    		case '':
    			$genderData = 0;
    			break;
    		case 'Herr':
    			$genderData = 1;
    			break;
    		case 'Frau':
    			$genderData = 2;
    			break;
    		default:
    			$genderData = 0;
    	}
    	return $genderData;
    }
    protected function updateStatistik($id, $interId, $isApply){
    	
    	$query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['auswertung']['table'])
    	                   . 'SET '
					       . Query::masketField('intern_id').' = '
						   . '\''.$interId.'\','
						   . Query::masketField('isApply').' = '
						   . '\''.$isApply.'\''
						   . 'WHERE'
						   . Query::masketField('id').' = '
						   . '\''.$id.'\''
    	                   . ';'
						;
		$result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);	
		?>
		<script type="text/javascript">
			setRequest('dispatch.php?_module=Statistic&_controller=index&_action=auszaelung'
					    + '&deliverySystem  =' + '' 
 				        + '&deliverySystemDistributor =' + '' 
 				        + '&genderSystem =' + ''
					    + '&reagierer=' + '' 
    				    + '&alterVon=' + '' 
    				    + '&alterBis=' + '' 
    				    + '&plzVon=' + '' 
    				    + '&plzBis=' + ''  
					    + '&anzeigen=' + true
					    , 'st');
			</script>
			<?php 
    }
	protected function updateStatistikResult($id, $user, $result){
		$query = 'UPDATE ' . Query::masketField($this->settings['repositorySettings']['auswertung']['table'])
    	                   . 'SET '
					       . Query::masketField('benutzer').' = '
						   . '\''.$user.'\','
						   . Query::masketField('ergebnis').' = '
						   . '\''.$result.'\','
			               . Query::masketField('datum').' = '
						   . '\''.StatisticUtility::getMysqlDatum().'\''
						   . ' WHERE'
						   . Query::masketField('id').' = '
						   . '\''.$id.'\''
    	                   . ';'
						;
		$result = $this->repository->executeQuery($query);
		$this->logger->log($result, __FUNCTION__);	

	}
	public static function getStatistikenIsApply(){
    	$statistikEntity = new \Modules\Statistic\Domain\Repository\StatistikAuswertungRepository();
    	
    	return $statistikEntity;
    }
    	
    
}
