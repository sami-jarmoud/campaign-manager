<?php
namespace Modules\Statistic\Domain\Repository;

use Packages\Core\Persistence\AbstractRepository;
use Packages\Core\Persistence\Generic\Query;


/**
 * Description of StatistikAuswertungRepository
 *
 * @author MohamedSamiJarmoud
 */
class StatistikAuswertungRepository extends AbstractRepository {
	
	/**
	 * statistikAuswertungTable
	 * 
	 * @var string
	 */
	protected $statistikAuswertungTable;
	
	/**
	 * statistikAuswertungPrimaryField
	 * 
	 * @var string
	 */
	protected $statistikAuswertungPrimaryField;
	
	/**
	 * statistikAuswertungFetchClass
	 * 
	 * @var string
	 */
	protected $statistikAuswertungFetchClass;
	
	
	/**
	 * logTable
	 * 
	 * @var string
	 */
	protected $logTable;
	
	/**
	 * logPrimaryField
	 * 
	 * @var string
	 */
	protected $logPrimaryField;
	
	/**
	 * logFetchClass
	 * 
	 * @var string
	 */
	protected $logFetchClass;
	
	/**
	 * getStatistikAuswertung
	 *
	 * @return \Packages\Core\Domain\Model\statistikAuswertungEntity
	 */
	public function getStatistikAuswertung() {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->table),
				'ORDER_BY' => Query::masketField('id') . 'DESC'
		);
	
		return $this->query->getRowsByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->fetchClass
				);
	}
	
	/**
	 * getStatistikAuswertungIsApply
	 *
	 * @return \Packages\Core\Domain\Model\statistikAuswertungEntity
	 */
	public function getStatistikAuswertungIsApply() {
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->table),
				'WHERE' => array(
						'_isApply' => array(
								'sql' => Query::masketField('isApply'),
								'value' => '1',
								'comparison' => 'integerEqual'
						),
				),
				'ORDER_BY' => Query::masketField('id') . 'DESC'
		);
	
		return $this->query->getRowsByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->fetchClass
				);
	}
	
	/**
	 * findStatistikById
	 *
	 * @param integer $id
	 * @return \Packages\Core\DomainObject\AbstractEntity
	 * @throws \InvalidArgumentException
	 */
	public function findStatistikById($id) {
		if (!\is_int($id)) {
			throw new \InvalidArgumentException('invalid id.', 1446538291);
		}
	
		$queryPartsDataArray = array(
				'FROM' => Query::masketField($this->table),
				'WHERE' => array(
						'_id' => array(
								'sql' => Query::masketField('id'),
								'value' => $id,
								'comparison' => 'integerEqual'
						),
				),
		);
	
		return $this->query->getRowByQueryPartsDataArray(
				$queryPartsDataArray,
				$this->fetchClass
				);
	}
		public function findById($id) {
		$object = $this->findStatistikById($id);
		
		if ($object instanceof \Modules\Statistic\Domain\Model\StatistikAuswertungEntity) {
			/* @var $object \Modules\Statistic\Domain\Model\StatistikAuswertungEntity */
			$object->_resetCleanProperties();
			
			$object->_memorizeCleanState();
		}
		
		return $object;
	}
	/**
	 * findAll
	 * 
	 * @return \Packages\Core\Persistence\ArrayIterator
	 */
	public function findStatistikenByParameter(array $parameter) {
		$queryPartsDataArray = array(
			'FROM' => Query::masketField($this->table),
			'WHERE' => array(
				'_klickprofil' => array(
					'sql' => Query::masketField('klickprofil_id'),
					'value' => $parameter['clientClickProfile'],
					'comparison' => 'integerEqual'
				),
				'_geschlecht' => array(
						'sql' => Query::masketField('geschlecht'),
						'value' => $parameter['genderSystemItem'],
						'comparison' => 'integerEqual'
				),
				'_reagierer' => array(
						'sql' => Query::masketField('reagierer'),
						'value' => $parameter['reagierer'],
						'comparison' => 'integerEqual'
				),
				'_alter_von' => array(
						'sql' => Query::masketField('alter_von'),
						'value' => $parameter['alterVon'],
						'comparison' => 'integerEqual'
				),
				'_alter_bis' => array(
						'sql' => Query::masketField('alter_bis'),
						'value' => $parameter['alterBis'],
						'comparison' => 'integerEqual'
				),
				'_plz_von' => array(
						'sql' => Query::masketField('plz_von'),
						'value' => $parameter['plzVon'],
						'comparison' => 'integerEqual'
				),
				'_plz_bis' => array(
						'sql' => Query::masketField('plz_bis'),
						'value' => $parameter['plzBis'],
						'comparison' => 'integerEqual'
				),
			),
				
			'ORDER_BY' => Query::masketField('datum') . 'DESC'
		);
		
		return $this->query->getRowsByQueryPartsDataArray(
			$queryPartsDataArray,
			$this->fetchClass
		);
	}
		

	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 ****************************************************************************************** */
	public function setStatistikAuswertungTable($statistikAuswertungTable) {
		$this->statistikAuswertungTable = $statistikAuswertungTable;
	}

	public function setStatistikAuswertungPrimaryField($statistikAuswertungPrimaryField) {
		$this->statistikAuswertungPrimaryField = $statistikAuswertungPrimaryField;
	}

	public function setStatistikAuswertungFetchClass($statistikAuswertungFetchClass) {
		$this->statistikAuswertungFetchClass = $statistikAuswertungFetchClass;
	}

	
	public function setLogTable($logTable) {
		$this->logTable = $logTable;
	}

	public function setLogPrimaryField($logPrimaryField) {
		$this->logPrimaryField = $logPrimaryField;
	}

	public function setLogFetchClass($logFetchClass) {
		$this->logFetchClass = $logFetchClass;
	}

}
