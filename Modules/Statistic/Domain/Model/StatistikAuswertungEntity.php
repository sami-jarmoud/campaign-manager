<?php
namespace Modules\Statistic\Domain\Model;

use Packages\Core\DomainObject\AbstractEntity;


/**
 * @author MohamedSamiJarmoud
 *
 */
class StatistikAuswertungEntity extends AbstractEntity{
	
	/**
	 * id
	 *
	 * @var integer
	 */
	protected $id = NULL;
	
	/**
	 * funktion_name
	 * 
	 * @var string
	 */
	protected $funktion_name;
	
	/**
	 * titel
	 * 
	 * @var string
	 */
	protected $titel;
	
	/**
	 * 
	 * @var \DateTime
	 */
	protected $datum;
	
	/**
	 * benutzer
	 * 
	 * @var string
	 */
	protected $benutzer;
	
	/**
	 * versandsystem
	 *
	 * @var string
	 */
	protected $versandsystem;
	
	/**
	 * verteiler
	 *
	 * @var string
	 */
	protected $verteiler;
	
	/**
	 * klickprofil_id
	 * 
	 * @var int
	 */
	protected $klickprofil_id;
	
	/**
	 * klickprofil
	 * 
	 * @var string
	 */
	protected $klickprofil;
	
	/**
	 * geschlecht
	 *
	 * @var string
	 */
	protected $geschlecht;
	
	/**
	 * reagierer
	 *
	 * @var string
	 */
	protected $reagierer;
	
	/**
	 * alter_von
	 *
	 * @var string
	 */
	protected $alter_von;
	
	/**
	 * alter_bis
	 *
	 * @var string
	 */
	protected $alter_bis;
	
	/**
	 * plz_bis
	 *
	 * @var string
	 */
	protected $plz_von;
	
	/**
	 * plz_von
	 *
	 * @var string
	 */
	protected $plz_bis;
	
	/**
	 * status
	 * 
	 * @var string
	 */
	protected $status;
	
	/**
	 * ergebnis
	 *
	 * @var integer
	 */
	protected $ergebnis;
	
	/**
	 * intern_id
	 *
	 * @var string
	 */
	protected $intern_id;
	
	/**
	 * isApply
	 *
	 * @var integer
	 */
	protected $isApply;
	
	public function __construct($primaryFieldName = 'id') {
		parent::__construct($primaryFieldName);
	}
	

	/********************************************************************************************
	 *
	 *              setter and getter
	 *
	 *******************************************************************************************/
	public function getId() {
		return (int) $this->id;
	}
	public function setId($id) {
		$this->id = \intval($id);
	}
	
	public function getFunktionName() {
		return $this->funktion_name;
	}
	public function setFunktionName($funktion_name){
		$this->funktion_name = $funktion_name;
	}
	public function getTitel() {
		return $this->titel;
	}
	public function setTitel($titel){
		$this->titel = $titel;
	}
	public function getDatum() {
		return $this->datum;
	}
	public function setDatum($datum){
	    $this->datum = $datum;
	}
	public function getBenutzer() {
		return $this->benutzer;
	}
	public function setBenutzer($benutzer){
	    $this->benutzer = $benutzer;
	}
	public function getVersandSystem() {
		return $this->versandsystem;
	}
	public function setVersandSystem($versandsystem){
		$this->versandsystem = $versandsystem;
	}
	public function getVerteiler() {
		return $this->verteiler;
	}
	public function setVerteiler($verteiler){
		$this->verteiler = $verteiler;
	}
	public function getKlickProfilId() {
		return $this->klickprofil_id;
	}
	public function setKlickProfilId($klickprofil_id){
	   $this->klickprofil_id = $klickprofil_id;
	}
	public function getKlickProfil() {
		return $this->klickprofil;
	}
	public function setKlickProfil($klickprofil){
	   $this->klickprofil = $klickprofil;
	}
	public function getGeschlecht() {
		return $this->geschlecht;
	}
	public function setGeschlecht($geschlecht){
		$this->geschlecht = $geschlecht;
	}
	public function getReagierer() {
		return $this->reagierer;
	}
	public function setReagierer($reagierer){
		$this->reagierer = $reagierer;
	}
	public function getAlterVon() {
		return $this->alter_von;
	}
	public function setAlterVon($alter_von){
		$this->alter_von = $alter_von;
	}
	public function getAlterBis() {
		return $this->alter_bis;
	}
	public function setAlterBis($alter_bis){
		$this->alter_bis = $alter_bis;
	}
	public function getPlzVon() {
		return $this->plz_von;
	}
	public function setPlzVon($plz_von){
		$this->plz_von = $plz_von;
	}
	public function getPlzBis() {
		return $this->plz_bis;
	}
	public function setPlzBis($plz_bis){
		$this->plz_bis = $plz_bis;
	}
	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status){
	   $this->status = $status;
	}
	public function getErgebnis() {
		return $this->ergebnis;
	}
	public function setErgebnis($ergebnis){
	   $this->ergebnis = $ergebnis;
	}
	public function getInternId() {
		return $this->intern_id;
	}
	public function setInternId($intern_id){
		$this->intern_id = $intern_id;
	}
	public function getIsApply() {
		return $this->isApply;
	}
	public function setIsApply($isApply){
		$this->isApply = $isApply;
	}
}